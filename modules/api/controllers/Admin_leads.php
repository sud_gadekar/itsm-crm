<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require __DIR__.'/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 */
class Admin_leads extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Api_model');
    }

    public function data_get($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $this->load->model('leads_model');
        $assigned = $this->input->get('salesperson');
        $status = $this->input->get('status');
        $tagids = $this->input->get('tagids');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');

        $data = $this->db->select(db_prefix() . 'leads.*,' . db_prefix() . 'leads.name, ' . db_prefix() . 'leads.id,' . db_prefix() . 'leads_status.name as status_name,' . db_prefix() . 'leads_sources.name as source_name,' . db_prefix() . 'staff.firstname as assigned_fname,' . db_prefix() . 'staff.lastname as assigned_lname,' . db_prefix() . 'countries.short_name as country_name');
        $this->db->join(db_prefix() . 'leads_status', db_prefix() . 'leads_status.id=' . db_prefix() . 'leads.status', 'left');
        $this->db->join(db_prefix() . 'leads_sources', db_prefix() . 'leads_sources.id=' . db_prefix() . 'leads.source', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid=' . db_prefix() . 'leads.assigned', 'left');
        $this->db->join(db_prefix() . 'countries', db_prefix() . 'countries.country_id=' . db_prefix() . 'leads.country', 'left');

        if($user->admin==0){
            $this->db->where('assigned', $staffid); 
        }else{
            if($assigned){ $this->db->where('assigned', $assigned); }
        }
        if($status){ $this->db->where('status', $status); }
        //$this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'leads.id', $id);
            $lead = $this->db->get(db_prefix() . 'leads')->row();
            if ($lead) {
                if ($lead->from_form_id != 0) {
                    $lead->form_data = $this->get_form([
                        'id' => $lead->from_form_id,
                    ]);
                }
                $lead->attachments = $this->leads_model->get_lead_attachments($id);
                $lead->public_url  = leads_public_url($id);
            }
            $data = $lead;
        }else{
            if($limit){
                $this->db->limit($limit,$start);
            }
            $data = $this->db->get(db_prefix() . 'leads')->result_array();
        }

        if($tagids){
            $data = $this->db->select(db_prefix() . 'leads.*,' . db_prefix() . 'leads.id,' . db_prefix() . 'leads_status.name as status_name,' . db_prefix() . 'leads_sources.name as source_name,' . db_prefix() . 'staff.firstname as assigned_fname,' . db_prefix() . 'staff.lastname as assigned_lname,' . db_prefix() . 'countries.short_name as country_name');
            $this->db->join(db_prefix() . 'leads', db_prefix() . 'leads.id=' . db_prefix() . 'taggables.rel_id', 'left');
            $this->db->join(db_prefix() . 'leads_status', db_prefix() . 'leads_status.id=' . db_prefix() . 'leads.status', 'left');
            $this->db->join(db_prefix() . 'leads_sources', db_prefix() . 'leads_sources.id=' . db_prefix() . 'leads.source', 'left');
            $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid=' . db_prefix() . 'leads.assigned', 'left');
            $this->db->join(db_prefix() . 'countries', db_prefix() . 'countries.country_id=' . db_prefix() . 'leads.country', 'left');    
            $this->db->where(db_prefix() . 'taggables.rel_type','lead')->where_in(db_prefix() . 'taggables.tag_id', [$tagids]);
            if($user->admin==0){
                $this->db->where('assigned', $staffid); 
            }else{
                if($assigned){ $this->db->where('assigned', $assigned); }
            }
            if($status){ $this->db->where('status', $status); }
            $this->db->group_by(db_prefix() . 'leads.id');
            if($limit){
                $this->db->limit($limit,$start);
            }
            $data = $this->db->get(db_prefix() . 'taggables')->result_array();
        }
        //var_dump($this->db->last_query()); die();

        if ($data)
        {
            if(is_numeric($id)){
                $data->assigned_to_name = $data->assigned_fname.' '.$data->assigned_lname;
                $data->approval_status_name = 'Pending';
                if($data->approval_status == 1){ $data->approval_status_name = 'Approved'; }
                else if($data->approval_status == 2){ $data->approval_status_name = 'Rejected'; }

                $pre_sale_engineers = array();
                if($data->pre_sale_engineer != 0 ){
                    $res =  explode (",", $data->pre_sale_engineer);
                    foreach ($res as $value) {
                        array_push($pre_sale_engineers, get_staff_full_name($value));
                    }
                }
                $data->pre_sale_engineers = implode(', ', $pre_sale_engineers);

                $tag_list = $this->db->select('name')->where('rel_type','lead')->where('rel_id', $id);
                $this->db->join(db_prefix() . 'tags', db_prefix() . 'tags.id=' . db_prefix() . 'taggables.tag_id', 'left');
                $tag_list = $this->db->get(db_prefix() . 'taggables')->result_array();
                $lead_tags = array();
                if($tag_list){
                    foreach($tag_list as $taggables){
                        array_push($lead_tags, $taggables['name']);
                    }
                    $data->tags = implode(', ', $lead_tags);
                }else{
                    $data->tags = '';
                }
                // Get related notes
                $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid=' . db_prefix() . 'notes.addedfrom');
                $this->db->where('rel_type', 'lead');
                $this->db->where('rel_id', $id);
                $data->notes = $this->db->select(db_prefix() . 'notes.*,' . db_prefix() . 'staff.firstname,' . db_prefix() . 'staff.lastname')->get(db_prefix() . 'notes')->result_array();
            }else{
                foreach($data as $key=>$row){
                    $data[$key]['assigned_to_name'] = $row['assigned_fname'].' '.$row['assigned_lname'];
                    $data[$key]['approval_status_name'] = 'Pending';
                    if($row['approval_status'] == 1){ $data[$key]['approval_status_name'] = 'Approved'; }
                    else if($row['approval_status'] == 2){ $data[$key]['approval_status_name'] = 'Rejected'; }

                    $pre_sale_engineers = array();
                    if($row['pre_sale_engineer'] != 0 ){
                        $res =  explode (",", $row['pre_sale_engineer']);
                        foreach ($res as $value) {
                            array_push($pre_sale_engineers, get_staff_full_name($value));
                        }
                    }
                    $data[$key]['pre_sale_engineers'] = implode(', ', $pre_sale_engineers);

                    $tag_list = $this->db->select('name')->where('rel_type','lead')->where('rel_id', $row['id']);
                    $this->db->join(db_prefix() . 'tags', db_prefix() . 'tags.id=' . db_prefix() . 'taggables.tag_id', 'left');
                    $tag_list = $this->db->get(db_prefix() . 'taggables')->result_array();
                    $lead_tags = array();
                    if($tag_list){
                        foreach($tag_list as $taggables){
                            array_push($lead_tags, $taggables['name']);
                        }
                        $data[$key]['tags'] = implode(', ', $lead_tags);
                    }else{
                        $data[$key]['tags'] = '';
                    }
                }
            }
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->response([
                'data' => [],
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('lead', $key);
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"leads");

            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    public function data_post()
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        } 
    }

    public function data_delete($id = '')
    { 
        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Lead ID'
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->load->model('leads_model');
            $output = $this->leads_model->delete($id);
            if($output === TRUE){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Lead Delete Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Lead Delete Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function data_put($id = '')
    {
        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Lead ID'
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {

            $update_data = $this->input->post();
            // update data
            $this->load->model('leads_model');
            $output = $this->leads_model->update($update_data, $id);
            if($output > 0 && !empty($output)){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Lead Update Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Lead Update Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    function handle_lead_attachments_array($leadid, $index_name = 'file')
    {
        $path           = get_upload_path_by_type('lead') . $leadid . '/';
        $CI             = &get_instance();

        if (isset($_FILES[$index_name]['name'])
            && ($_FILES[$index_name]['name'] != '' || is_array($_FILES[$index_name]['name']) && count($_FILES[$index_name]['name']) > 0)) {
            if (!is_array($_FILES[$index_name]['name'])) {
                $_FILES[$index_name]['name']     = [$_FILES[$index_name]['name']];
                $_FILES[$index_name]['type']     = [$_FILES[$index_name]['type']];
                $_FILES[$index_name]['tmp_name'] = [$_FILES[$index_name]['tmp_name']];
                $_FILES[$index_name]['error']    = [$_FILES[$index_name]['error']];
                $_FILES[$index_name]['size']     = [$_FILES[$index_name]['size']];
            }

            _file_attachments_index_fix($index_name);
            for ($i = 0; $i < count($_FILES[$index_name]['name']); $i++) {
                // Get the temp file path
                $tmpFilePath = $_FILES[$index_name]['tmp_name'][$i];

                // Make sure we have a filepath
                if (!empty($tmpFilePath) && $tmpFilePath != '') {
                    if (_leometric_upload_error($_FILES[$index_name]['error'][$i])
                        || !_upload_extension_allowed($_FILES[$index_name]['name'][$i])) {
                        continue;
                    }

                    _maybe_create_upload_path($path);
                    $filename    = unique_filename($path, $_FILES[$index_name]['name'][$i]);
                    $newFilePath = $path . $filename;

                    // Upload the file into the temp dir
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $CI = & get_instance();
                        $CI->load->model('leads_model');
                        $data   = [];
                        $data[] = [
                            'file_name' => $filename,
                            'filetype'  => $_FILES[$index_name]['type'][$i],
                            ];
                        $CI->leads_model->add_attachment_to_database($leadid, $data, false);
                    }
                }
            }
        }
        return true;
    }

}
