<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Files extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //$this->load->model('clients_model');
        $files_where = 'visible_to_customer = 1 AND id IN (SELECT file_id FROM ' . db_prefix() . 'shared_customer_files WHERE contact_id =' . $contact_id . ')';
        $files_where = hooks()->apply_filters('customers_area_files_where', $files_where);
        $data = $this->clients_model->get_customer_files($client_id, $files_where);

        // Check if the data store contains
        if ($data)
        {
            // Set the response and exit
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_post($id = '')
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //print_r($_FILES["file"]); die;

        //Upload Profile Image
        //if (is_numeric($id)) {
            //$this->load->model('misc_model');
            // $file                        = $_FILES["file"]; //$this->input->post('file');
            // $file[0]['staffid']          = 0;
            // $file[0]['contact_id']       = $contact_id;
            // $file['visible_to_customer'] = 1;
            // $success                     = $this->misc_model->add_attachment_to_database(
            //     $client_id,
            //     'customer',
            //     $file
            // );

            // print_r($success);

            $target_dir = "uploads/clients/".$client_id."/";
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
            // if($check !== false) {
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    $data['dateadded'] = date('Y-m-d H:i:s');
                    $data['rel_id']    = $client_id;
                    $data['rel_type'] = 'customer';
                    $data['file_name'] = $_FILES["file"]["name"];
                    $data['filetype']  = $_FILES["file"]["type"];
                    $data['contact_id'] = $contact_id;
                    $data['visible_to_customer'] = 1;
                    $data['attachment_key'] = app_generate_hash();

                    $this->db->insert(db_prefix() . 'files', $data);
                    $insert_id = $this->db->insert_id();

                    $this->db->insert(db_prefix() . 'shared_customer_files', [
                        'file_id'    => $insert_id,
                        'contact_id' => $contact_id,
                    ]);
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'File uploaded successful.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                } else {
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Sorry, there was an error uploading your file.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                }
            // } else {
            //     // error
            //     $message = array(
            //         'status' => FALSE,
            //         'message' => 'File is not an image.'
            //     );
            //     $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            // }
        //}
    }
}
