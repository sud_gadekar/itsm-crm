<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Service_requests extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * @api {get} api/service_requests/:id Request Service_request information
     * @apiName GetService_request
     * @apiGroup Service_request
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {Number} id Service_request unique ID.
     *
     * @apiSuccess {Object} Service_request information.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *         "service_requestid": "7",
     *         "adminreplying": "0",
     *         "userid": "0",
     *         "contactid": "0",
     *         "email": null,
     *         "name": "Trung bình",
     *         "department": "1",
     *         "priority": "2",
     *         "status": "1",
     *         "service": "1",
     *         "service_requestkey": "8ef33d61bb0f26cd158d56cc18b71c02",
     *         "subject": "Service_request ER",
     *         "message": "Service_request ER",
     *         "admin": "5",
     *         "date": "2019-04-10 03:08:21",
     *         "project_id": "5",
     *         "lastreply": null,
     *         "clientread": "0",
     *         "adminread": "1",
     *         "assigned": "5",
     *         "line_manager": "8",
     *         "milestone": "27",
     *         ...
     *     }
     * @apiError {Boolean} status Request status.
     * @apiError {String} message The id of the Service_request was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "No data were found"
     *     }
     */
    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('service_requests', $id);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"service_requests", $id);
            if(is_numeric($id)){
                $data->replies = $this->Service_requests_model->get_service_request_replies($id);
            }

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @api {get} api/service_requests/search/:keysearch Search Service_request Information
     * @apiName GetService_requestSearch
     * @apiGroup Service_request
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {String} keysearch Search keywords.
     *
     * @apiSuccess {Object} Service_request information.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *         "service_requestid": "7",
     *         "adminreplying": "0",
     *         "userid": "0",
     *         "contactid": "0",
     *         "email": null,
     *         "name": "Trung bình",
     *         "department": "1",
     *         "priority": "2",
     *         "status": "1",
     *         "service": "1",
     *         "service_requestkey": "8ef33d61bb0f26cd158d56cc18b71c02",
     *         "subject": "Service_request ER",
     *         "message": "Service_request ER",
     *         "admin": "5",
     *         "date": "2019-04-10 03:08:21",
     *         "project_id": "5",
     *         "lastreply": null,
     *         "clientread": "0",
     *         "adminread": "1",
     *         "assigned": "5",
     *         "line_manager": "8",
     *         "milestone": "27",
     *         ...
     *     }
     * @apiError {Boolean} status Request status.
     * @apiError {String} message The id of the Service_request was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "No data were found"
     *     }
     */
    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('service_request', $key);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"service_requests");

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    /**
     * @api {post} api/service_requests Add New Service_request
     * @apiName PostService_request
     * @apiGroup Service_request
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {String} subject                       Mandatory Service_request name .
     * @apiParam {String} department                    Mandatory Service_request Department.
     * @apiParam {String} contactid                     Mandatory Service_request Contact.
     * @apiParam {String} userid                        Mandatory Service_request user.
     * @apiParam {String} [project_id]                  Optional Service_request Project.
     * @apiParam {String} [message]                     Optional Service_request message.
     * @apiParam {String} [service]                     Optional Service_request Service.
     * @apiParam {String} [assigned]                    Optional Assign service_request.
     * @apiParam {String} [cc]                          Optional Service_request CC.
     * @apiParam {String} [priority]                    Optional Priority.
     * @apiParam {String} [tags]                        Optional service_request tags.
     *
     * @apiParamExample {Multipart Form} Request-Example:
     *    array (size=11)
     *     'subject' => string 'service_request name' (length=11)
     *     'contactid' => string '4' (length=1)
     *     'userid' => string '5' (length=1)
     *     'department' => string '2' (length=1)
     *     'cc' => string '' (length=0)
     *     'tags' => string '' (length=0)
     *     'assigned' => string '8' (length=1)
     *     'priority' => string '2' (length=1)
     *     'service' => string '2' (length=1)
     *     'project_id' => string '' (length=0)
     *     'message' => string '' (length=0)
     *
     *
     * @apiSuccess {Boolean} status Request status.
     * @apiSuccess {String} message Service_request add successful.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "status": true,
     *       "message": "Service_request add successful."
     *     }
     *
     * @apiError {Boolean} status Request status.
     * @apiError {String} message Service_request add fail.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "Service_request add fail."
     *     }
     * 
     */
    public function data_post($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Add replay
        if (is_numeric($id)) {
            $this->form_validation->set_rules('message', _l('Reply message'), 'required');
            if ($this->form_validation->run() == FALSE)
            {
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            else
            {
                // insert data
                $attachments = $_FILES["attachments"];
                $data['service_requestid'] = $id;
                $data['date']     = date('Y-m-d H:i:s');
                $data['message']  = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['contactid'] = $contact_id;
                $data['userid'] = $client_id;
                $this->db->insert(db_prefix() . 'service_request_replies', $data);
                $insert_id = $this->db->insert_id();

                if($insert_id > 0 && !empty($insert_id)){
                    log_activity('New Service Request Reply [ReplyID: ' . $insert_id . ']');
                    //upload service request attachment
                    if($attachments){
                        $target_dir = "uploads/service_request_attachments/".$id."/";
                        if (!file_exists($target_dir)) {
                            mkdir($target_dir, 0755);
                            $fp = fopen($target_dir . 'index.html', 'w');
                            fclose($fp);
                        }
                        $target_file = $target_dir . basename($_FILES["attachments"]["name"]);
                        if (move_uploaded_file($_FILES["attachments"]["tmp_name"], $target_file)) {
                            $attachment['dateadded'] = date('Y-m-d H:i:s');
                            $attachment['file_name'] = $_FILES["attachments"]["name"];
                            $attachment['filetype']  = $_FILES["attachments"]["type"];

                            $attachment['service_requestid']  = $id;
                            $attachment['replyid'] = $insert_id;
                            $this->db->insert(db_prefix() . 'service_request_attachments', $attachment);
                        }
                        else{
                            // attachment error
                            $message = array(
                                'status' => FALSE,
                                'message' => 'Sorry, there was an error uploading your file.'
                            );
                            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                        }
                    }
                    // success
                    $message = array(
                    'status' => TRUE,
                    'message' => 'Comment added successfully.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }else{
                    // error
                    $message = array(
                    'status' => FALSE,
                    'message' => 'Comment add failed.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

        // form validation
        $this->form_validation->set_rules('subject', _l('customer_ticket_subject'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'subject' => $this->input->post('subject', TRUE),
                'service' => $this->input->post('service', TRUE),
                'request_for' => $this->input->post('request_for', TRUE),
                'message' => $this->Api_model->value($this->input->post('message', TRUE)),
                'assigned' => $this->Api_model->value($this->input->post('assigned', TRUE)),
                'contactid' => $contact_id,
                'userid'    => $client_id
                // 'department' => $this->input->post('department', TRUE),
                // 'contactid' => $this->input->post('contactid', TRUE),
                // 'userid' => $this->input->post('userid', TRUE),
                // 'cc' => $this->Api_model->value($this->input->post('cc', TRUE)),
                // 'tags' => $this->Api_model->value($this->input->post('tags', TRUE)),
                // 'priority' => $this->Api_model->value($this->input->post('priority', TRUE)),
                // 'service' => $this->Api_model->value($this->input->post('service', TRUE)),
                // 'project_id' => $this->Api_model->value($this->input->post('project_id', TRUE)),
             ];
            if (!empty($this->input->post('custom_fields', TRUE))) {
                $insert_data['custom_fields'] = $this->Api_model->value($this->input->post('custom_fields', TRUE));
            }
               
            // insert data
            $this->load->model('service_requests_model');
            $output = $this->service_requests_model->add($insert_data);
            if($output > 0 && !empty($output)){
                // success
                $data = [];
                $data['message'] = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['status'] = 1;
                $data['userid'] = $client_id;
                $data['contactid'] = $contact_id;
                $reply_id = $this->service_requests_model->add_reply($data, $output, null);

                if ($reply_id > 0 && !empty($reply_id)) {
                    // $this->db->where('service_requestid', $output);
                    // $this->db->update(db_prefix() . 'service_request_attachments', [
                    //     'replyid' => $reply_id,
                    // ]);
                    if($_FILES["attachments"]){
                        $target_dir = "uploads/service_request_attachments/".$output."/";
                        if (!file_exists($target_dir)) {
                            mkdir($target_dir, 0755);
                            $fp = fopen($target_dir . 'index.html', 'w');
                            fclose($fp);
                        }
                        $target_file = $target_dir . basename($_FILES["attachments"]["name"][0]);
                        if (move_uploaded_file($_FILES["attachments"]["tmp_name"][0], $target_file)) {
                            $attachment['dateadded'] = date('Y-m-d H:i:s');
                            $attachment['file_name'] = $_FILES["attachments"]["name"][0];
                            $attachment['filetype']  = $_FILES["attachments"]["type"][0];
                            $attachment['service_requestid']  = $output;
                            $attachment['replyid'] = $reply_id;
                            $this->db->insert(db_prefix() . 'service_request_attachments', $attachment);
                        }
                        else{
                            // attachment error
                            $message = array(
                                'status' => FALSE,
                                'message' => 'Sorry, there was an error uploading your file.'
                            );
                            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                        }
                    }
                }

                $message = array(
                'status' => TRUE,
                'message' => 'Service Request add successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Service Request add fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }


    /**
     * @api {delete} api/delete/service_requests/:id Delete a Service_request
     * @apiName DeleteService_request
     * @apiGroup Service_request
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {Number} id Service_request unique ID.
     *
     * @apiSuccess {Boolean} status Request status.
     * @apiSuccess {String} message Service_request Delete Successful.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "status": true,
     *       "message": "Service_request Delete Successful."
     *     }
     *
     * @apiError {Boolean} status Request status.
     * @apiError {String} message Service_request Delete Fail.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "Service_request Delete Fail."
     *     }
     */
    public function data_delete($id = '')
    {
        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Service_request ID'
        );
        $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->load->model('service_requests_model');
            $output = $this->service_requests_model->delete($id);
            if($output === TRUE){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Service_request Delete Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Service_request Delete Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }


    /**
     * @api {put} api/service_requests/:id Update a service_request
     * @apiName PutService_request
     * @apiGroup Service_request
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {String} subject                       Mandatory Service_request name .
     * @apiParam {String} department                    Mandatory Service_request Department.
     * @apiParam {String} contactid                     Mandatory Service_request Contact.
     * @apiParam {String} userid                        Mandatory Service_request user.
     * @apiParam {String} priority                      Mandatory Priority.
     * @apiParam {String} [project_id]                  Optional Service_request Project.
     * @apiParam {String} [message]                     Optional Service_request message.
     * @apiParam {String} [service]                     Optional Service_request Service.
     * @apiParam {String} [assigned]                    Optional Assign service_request.
     * @apiParam {String} [tags]                        Optional service_request tags.
     *
     *
     * @apiParamExample {json} Request-Example:
     *  {
     *       "subject": "Service_request ER",
     *       "department": "1",
     *       "contactid": "0",
     *       "service_requestid": "7",
     *       "userid": "0",
     *       "project_id": "5",
     *       "message": "Service_request ER",
     *       "service": "1",
     *       "assigned": "5",
     *       "priority": "2",
     *       "tags": ""
     *   }
     *
     * @apiSuccess {Boolean} status Request status.
     * @apiSuccess {String} message Service_request Update Successful.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "status": true,
     *       "message": "Service_request Update Successful."
     *     }
     *
     * @apiError {Boolean} status Request status.
     * @apiError {String} message Service_request Update Fail.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "Service_request Update Fail."
     *     }
     */
    public function data_put($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Service Request ID'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {

            $update_data = $this->input->post();
            // update data
            $this->load->model('service_requests_model');
            $update_data['service_requestid'] = $id;
            //$update_data['approved_by'] = $contact_id;
            $output = $this->service_requests_model->service_request_approval($update_data);
            if($output > 0 && !empty($output)){
                // success
                $sr_msg = 'Success';
                if($this->input->post('is_approved')==1){ $sr_msg = 'Service Request Approval Successful.'; }
                else if($this->input->post('is_approved')==0){ $sr_msg = 'Service Request Rejected.'; }
                $message = array(
                'status' => TRUE,
                'message' => $sr_msg
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Service Request Approval Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
