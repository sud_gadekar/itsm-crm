<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_ticket_notes extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        //Not Applicable
    }
    
    public function data_post($id = '')
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Update Note
        if (is_numeric($id)) {
            $this->form_validation->set_rules('note', _l('Note Description'), 'required');
            if ($this->form_validation->run() == FALSE){
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                // update data
                $data['description']  = trim($this->Api_model->value($this->input->post('note', TRUE)));
                $this->db->where('id', $id);
                $this->db->update(db_prefix() . 'notes', $data);
                if ($this->db->affected_rows() > 0) { // success
                    log_activity('Update Ticket Note [NoteID: ' . $id . ']');
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Ticket Note updated successfully.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                } else { // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Ticket Note update failed.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }
            }
        }
        else //Add New Note
        {   
            $this->form_validation->set_rules('ticketid', _l('Ticket ID'), 'required');
            $this->form_validation->set_rules('note', _l('Note Description'), 'required');
            if ($this->form_validation->run() == FALSE){
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                // Insert data
                $data['rel_id']         = $this->Api_model->value($this->input->post('ticketid', TRUE));
                $data['rel_type']       = 'ticket';
                $data['description']    = trim($this->Api_model->value($this->input->post('note', TRUE)));
                $data['addedfrom']      = $staffid;
                $data['dateadded']      = date('Y-m-d H:i:s');
                $this->db->insert(db_prefix() . 'notes', $data);
                $insert_id = $this->db->insert_id();

                if($insert_id > 0 && !empty($insert_id)){ // success
                    log_activity('New Ticket Note [NoteID: ' . $insert_id . ']');
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Ticket Note added successfully.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                } else { // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Ticket Note add failed.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }
            }
        }
    }

    //{delete} api/delete/admin_ticket_notes/:id Delete a Ticket Note
    public function data_delete($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Note ID'
        );
        $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->db->where('id', $id);
            $this->db->delete(db_prefix() . 'notes');
            if ($this->db->affected_rows() > 0) {
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Ticket Note Delete Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Ticket Note Delete Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }
}
