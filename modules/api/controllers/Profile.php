<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Profile extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('profile', $id);

        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            // Set the response and exit
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_post($id = '')
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Upload Profile Image
        if (is_numeric($id)) {
            // $file = basename($_FILES['profile_image']['name']); print_r($file); die;
            // handle_contact_profile_image_upload();

            $target_dir = "uploads/client_profile_images/"; //.$id."/";
            $target_file = $target_dir . basename($_FILES["profile_image"]["name"]);
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["profile_image"]["tmp_name"]);
            if($check !== false) {
                if (move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file)) {
                    $output = $this->clients_model->update_contact(['profile_image' => $_FILES["profile_image"]["name"],], $contact_id, true);
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Profile image update successful.',
                        'profile_image' => $target_file
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                } else {
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Sorry, there was an error uploading your file.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'File is not an image.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }

        //Change password
        if ($this->input->post('change_password')) {
            $this->form_validation->set_rules('oldpassword', _l('clients_edit_profile_old_password'), 'required');
            $this->form_validation->set_rules('newpassword', _l('clients_edit_profile_new_password'), 'required');
            $this->form_validation->set_rules('newpasswordr', _l('clients_edit_profile_new_password_repeat'), 'required|matches[newpassword]');
            if ($this->form_validation->run() !== false) {
                $success = $this->clients_model->change_contact_password(
                    $contact_id,
                    $this->input->post('oldpassword', false),
                    $this->input->post('newpasswordr', false)
                );

                if (is_array($success) && isset($success['old_password_not_match'])) {
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => _l('client_old_password_incorrect')
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                } elseif ($success == true) {
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => _l('client_password_changed')
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }
            }
        }
        
        //Update Profile
        $this->form_validation->set_rules('firstname', _l('client_firstname'), 'required');
        $this->form_validation->set_rules('lastname', _l('client_lastname'), 'required');

        //$this->form_validation->set_message('contact_email_profile_unique', _l('form_validation_is_unique'));
        //$this->form_validation->set_rules('email', _l('clients_email'), 'required|valid_email|callback_contact_email_profile_unique');
        
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $this->load->model('clients_model');
            $contact = $this->clients_model->get_contact($contact_id);

            $output = $this->clients_model->update_contact([
                'firstname'          => $this->input->post('firstname'),
                'lastname'           => $this->input->post('lastname'),
                'title'              => $this->input->post('title'),
                //'email'              => $this->input->post('email'),
                'phonenumber'        => $this->input->post('phonenumber'),
                'direction'          => $this->input->post('direction'),
                'invoice_emails'     => $this->input->post('invoice_emails'),
                'credit_note_emails' => $this->input->post('credit_note_emails'),
                'estimate_emails'    => $this->input->post('estimate_emails'),
                'ticket_emails'      => $this->input->post('ticket_emails'),
                'service_request_emails'=> $this->input->post('service_request_emails'),
                'contract_emails'    => $this->input->post('contract_emails'),
                'project_emails'     => $this->input->post('project_emails'),
                'task_emails'        => $this->input->post('task_emails'),
            ], $contact_id, true);

            // success
            $message = array(
                'status' => TRUE,
                'message' => 'Profile update successful.'
            );
            $this->response((object) $message, REST_Controller::HTTP_OK);
            
            // if($output > 0 && !empty($output)){
                
            // }else{
            //     // error
            //     $message = array(
            //     'status' => FALSE,
            //     'message' => 'Profile update fail.'
            //     );
            //     $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            // }
        }
    }


    /**
     * @api {delete} api/delete/tickets/:id Delete a Ticket
     * @apiName DeleteTicket
     * @apiGroup Ticket
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {Number} id Ticket unique ID.
     *
     * @apiSuccess {Boolean} status Request status.
     * @apiSuccess {String} message Ticket Delete Successful.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "status": true,
     *       "message": "Ticket Delete Successful."
     *     }
     *
     * @apiError {Boolean} status Request status.
     * @apiError {String} message Ticket Delete Fail.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "Ticket Delete Fail."
     *     }
     */
    public function data_delete($id = '')
    {
        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Ticket ID'
        );
        $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->load->model('tickets_model');
            $output = $this->tickets_model->delete($id);
            if($output === TRUE){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Ticket Delete Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Ticket Delete Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }


    /**
     * @api {put} api/tickets/:id Update a ticket
     * @apiName PutTicket
     * @apiGroup Ticket
     *
     * @apiHeader {String} Authorization Basic Access Authentication token.
     *
     * @apiParam {String} subject                       Mandatory Ticket name .
     * @apiParam {String} department                    Mandatory Ticket Department.
     * @apiParam {String} contactid                     Mandatory Ticket Contact.
     * @apiParam {String} userid                        Mandatory Ticket user.
     * @apiParam {String} priority                      Mandatory Priority.
     * @apiParam {String} [project_id]                  Optional Ticket Project.
     * @apiParam {String} [message]                     Optional Ticket message.
     * @apiParam {String} [service]                     Optional Ticket Service.
     * @apiParam {String} [assigned]                    Optional Assign ticket.
     * @apiParam {String} [tags]                        Optional ticket tags.
     *
     *
     * @apiParamExample {json} Request-Example:
     *  {
     *       "subject": "Ticket ER",
     *       "department": "1",
     *       "contactid": "0",
     *       "ticketid": "7",
     *       "userid": "0",
     *       "project_id": "5",
     *       "message": "Ticket ER",
     *       "service": "1",
     *       "assigned": "5",
     *       "priority": "2",
     *       "tags": ""
     *   }
     *
     * @apiSuccess {Boolean} status Request status.
     * @apiSuccess {String} message Ticket Update Successful.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "status": true,
     *       "message": "Ticket Update Successful."
     *     }
     *
     * @apiError {Boolean} status Request status.
     * @apiError {String} message Ticket Update Fail.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message": "Ticket Update Fail."
     *     }
     */
    public function data_put($id = '')
    {
        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Ticket ID'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {

            $update_data = $this->input->post();
            // update data
            $this->load->model('tickets_model');
            $update_data['ticketid'] = $id;
            $output = $this->tickets_model->update_single_ticket_settings($update_data);
            if($output > 0 && !empty($output)){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Ticket Update Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Ticket Update Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
