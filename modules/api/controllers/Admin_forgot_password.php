<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_forgot_password extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        
    }
    
    public function data_post($id = '')
    {
        $this->form_validation->set_rules('email', _l('Email'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $this->load->model('Authentication_model');
            $email = $this->Api_model->value($this->input->post('email', TRUE));
            $output = $this->Authentication_model->forgot_password($email, true);

            if($output > 0 && !empty($output)){ // success
                $message = array(
                    'status' => TRUE,
                    'message' => 'Check your email for further instructions resetting your password'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{ // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Sorry, something went wrong.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }
}
