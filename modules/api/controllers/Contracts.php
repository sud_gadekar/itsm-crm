<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Contracts extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('contracts', $id);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"contracts", $id);

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('contract', $key);

        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"contracts");

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    
    public function data_post($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Signed Contract
        if (is_numeric($id)) {
            $this->form_validation->set_rules('signed', _l('Sign'), 'required');
            if ($this->form_validation->run() == FALSE)
            {
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            else
            {
                $signed = $this->input->post('signed', TRUE);
                $this->db->where('id', $id);
                $output = $this->db->update('contracts', ['marked_as_signed' => $signed, 'signed' => $signed]);

                if($output > 0 && !empty($output)){
                    // success
                    $message = array(
                    'status' => TRUE,
                    'message' => 'Contract signed successful.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }else{
                    // error
                    $message = array(
                    'status' => FALSE,
                    'message' => 'Contract signed fail.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

        // form validation
        $this->form_validation->set_rules('contract_id', 'Contract ID', 'required');
        $this->form_validation->set_rules('content', 'Comment', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'contract_id' => $this->input->post('contract_id', TRUE),
                'content' => $this->input->post('content', TRUE),
                //'contactid' => $contact_id,
                //'userid'    => $client_id
             ];
            if (!empty($this->input->post('custom_fields', TRUE))) {
                $insert_data['custom_fields'] = $this->Api_model->value($this->input->post('custom_fields', TRUE));
            }
               
            // insert data
            $this->load->model('contracts_model');
            $output = $this->contracts_model->add_app_comment($insert_data);
            if($output > 0 && !empty($output)){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Contract comment add successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Contract comment add fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
