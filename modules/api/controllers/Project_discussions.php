<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Project_discussions extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        //$data = $this->Api_model->get_table('project_discussions', $id);
        $this->load->model('projects_model');
        $data = $this->projects_model->get_project_discussion($id);
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"project_discussions", $id);
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_post($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Add discussion comment
        if (is_numeric($id)) {
            $this->form_validation->set_rules('content', _l('Content'), 'required');
            if ($this->form_validation->run() == FALSE)
            {
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            else
            {
                $type = 'regular';
                $insert_data = [
                    'content' => $this->Api_model->value($this->input->post('content', TRUE)),
                    'contact_id' => $contact_id
                ];
                // insert data
                $this->load->model('projects_model');
                $output = $this->projects_model->add_discussion_comment($insert_data, $id, $type);
                if($output > 0 && !empty($output)){
                    // success
                    $message = array(
                    'status' => TRUE,
                    'message' => 'Discussion comment add successful.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }else{
                    // error
                    $message = array(
                    'status' => FALSE,
                    'message' => 'Discussion comment add fail.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

        \modules\api\core\Apiinit::check_url('api');
        // form validation
        // $this->form_validation->set_rules('name', 'Discussion Name', 'trim|required|max_length[600]', array('is_unique' => 'This %s already exists please enter another Discussion Name'));
        // $this->form_validation->set_rules('startdate', 'Discussion Start Date', 'trim|required', array('is_unique' => 'This %s already exists please enter another Discussion Start Date'));
        //$this->form_validation->set_rules('is_public', 'Publicly available discussion', 'trim', array('is_unique' => 'Public state can be 1. Skip it completely to set it at non-public'));
        $this->form_validation->set_rules('subject', _l('Subject'), 'required');
        $this->form_validation->set_rules('description', _l('Description'), 'required');

        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'subject' => $this->input->post('subject', TRUE),
                'description' => $this->input->post('description', TRUE),
                'contact_id' => $contact_id,
                'project_id' => $this->input->post('project_id', TRUE),
            ];

            if (!empty($this->input->post('custom_fields', TRUE))) {
                $insert_data['custom_fields'] = $this->Api_model->value($this->input->post('custom_fields', TRUE));
            }
            // insert data
            $this->load->model('projects_model');
            $output = $this->projects_model->add_discussion($insert_data);
            if($output > 0 && !empty($output)){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Discussion add successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Discussion add failed.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function data_delete($id = '')
    {
        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Discussion ID'
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->load->model('projects_model');
            $output = $this->projects_model->delete_discussion($id);
            if($output === TRUE){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Discussion Delete Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Discussion Delete Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function data_put($id = '')
    {
        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Discussion ID'
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {

            $update_data = $this->input->post();
            // update data
            $this->load->model('projects_model');
            $output = $this->projects_model->update($update_data, $id);
            if($output > 0 && !empty($output)){
                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Discussion Update Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Discussion Update Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

}