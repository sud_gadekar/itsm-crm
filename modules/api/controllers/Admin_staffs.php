<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_staffs extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        // $headers = apache_request_headers();
        // $this->db->where('token', $headers['token']);
        // $user = $this->db->get(db_prefix() . 'staff')->row();
        // if($user){
        //     $staffid = $user->staffid;
        // }else{
        //     $this->response([
        //         'status' => FALSE,
        //         'message' => "Unauthorized Access"
        //     ], REST_Controller::HTTP_UNAUTHORIZED);
        // }
        
        //All Staff
        $data = $this->db->select('staffid, email, firstname, lastname, concat(firstname, " ", lastname) as full_name')
        ->from(db_prefix() . 'staff')->where('active',1)->get()->result_array();

        //assigned owners
        if($id==1){
            $data  = $this->db->select('tblstaff.*, CONCAT(tblstaff.firstname,\' \',tblstaff.lastname) as full_name')
                    ->from(db_prefix() . 'staff')
                    ->join(db_prefix() . 'staff_departments','tblstaff.staffid = tblstaff_departments.staffid')
                    ->where('tblstaff.is_not_staff',0)->where('tblstaff.active',1)->where('tblstaff_departments.departmentid',6)->order_by('tblstaff.firstname', 'desc')->get()->result_array();
        }
        
        //Pre Sales Engineers
        if($id==2){ 
            $data = $this->db->select('staffid, email, firstname, lastname, concat(firstname, " ", lastname) as full_name')
            ->from(db_prefix() . 'staff')->where(['is_not_staff' => 0, 'active' => 1])->get()->result_array();
        }

        //Sales Managers
        if($id==3){ 
            $data = $this->db->select('staffid, email, firstname, lastname, concat(firstname, " ", lastname) as full_name')
            ->from(db_prefix() . 'staff')->where(['is_not_staff' => 0, 'active' => 1, 'role' => 6])->get()->result_array();
        }

        if ($data)
        {
            // Set the response and exit
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
