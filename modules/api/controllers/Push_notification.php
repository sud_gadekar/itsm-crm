<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Push_notification extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        
    }

    public function data_post()
    {   
        //Send Push Notofication
        $this->form_validation->set_rules('devicetoken', _l('Device Token'), 'required');
        $this->form_validation->set_rules('body', _l('Notification Body'), 'required');
        $this->form_validation->set_rules('title', _l('Notification Title'), 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $fields = array(
                        'to' => $this->input->post('devicetoken'),
                        'notification' => [
                            "body" => $this->input->post('body'),
                            "title" => $this->input->post('title')
                        ],
                        'data' => [
                            "body" => $this->input->post('body'),
                            "title" => $this->input->post('title'),
                            "key_1" => "Value for key_1",
                            "key_2" => "Value for key_2"
                        ]
                    );

            //firebase server url to send the curl request
            $url = 'https://fcm.googleapis.com/fcm/send';
            $serverKey = 'AAAAHWjWvkE:APA91bEEElElGzhiO_LTYT-Ml9wv-oeH7dEpE2oT3rjMKrMlMg1rDkw474-ok0nzmqZNYJhPtm_RdXPusxEComjhZnAGJOsZtUwUgy79LcAUNBADMgPFdPRwPNg-o4LUZlDiCtoMZqm_';
            
            //building headers for the request
            $headers = array(
                'Authorization: key=' .$serverKey,
                'Content-Type: application/json'
            );

            //Initializing curl to open a connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);

            //adding headers 
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            //disabling ssl support
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    
            //adding the fields in json format 
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            
            //finally executing the curl request 
            $result = curl_exec($ch);
            if ($result === FALSE) {
                $message = array(
                    'status' => TRUE,
                    'message' => 'Push notification send failed.',
                    'data' => curl_error($ch)
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            curl_close($ch);

            // success
            $message = array(
                'status' => TRUE,
                'message' => 'Push notification send successful.',
                'data' => $result
            );
            $this->response((object) $message, REST_Controller::HTTP_OK);
        }
    }
}
