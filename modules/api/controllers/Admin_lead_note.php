<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require __DIR__.'/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 */
class Admin_lead_note extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Api_model');
    }

    public function data_get($id = '')
    {
        //
    }
    
    public function data_post()
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Add Lead Note
        $this->form_validation->set_rules('description', _l('Description'), 'required');
        $this->form_validation->set_rules('leadid', _l('Lead ID'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // insert data
            $id = $_POST['leadid'];
            if($_POST['date_contacted']){ $data['date_contacted'] = $_POST['date_contacted']; }
            $data['dateadded']   = date('Y-m-d H:i:s');
            $data['addedfrom']   = $staffid;
            $data['rel_type']    = 'lead';
            $data['rel_id']      = $id;
            $data['description'] = nl2br($_POST['description']);

            $data = hooks()->apply_filters('create_note_data', $data, 'lead', $id);

            $this->db->insert(db_prefix() . 'notes', $data);
            $note_id = $this->db->insert_id();

            if ($note_id) {
                if (isset($_POST['date_contacted'])) {
                    $this->db->where('id', $id);
                    $this->db->update(db_prefix() . 'leads', [
                        'lastcontact' => $_POST['date_contacted'],
                    ]);
                }else{
                    $this->db->where('id', $id);
                    $this->db->update(db_prefix() . 'leads', [
                        'last_updated_at' => date('Y-m-d h:i:s'),
                    ]);
                }
                $message = array(
                    'status' => TRUE,
                    'message' => 'Lead Note added successfully.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
            else { // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Lead Note add failed.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            } 
        }
    }

    public function data_delete($id = '')
    { 
        $id = $this->security->xss_clean($id);
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Lead ID'
        );
        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            // delete data
            $this->db->where('id', $id);
            $this->db->delete(db_prefix() . 'notes');
            if ($this->db->affected_rows() > 0) {
                // success
                $message = array(
                    'status' => TRUE,
                    'message' => 'Lead Note Delete Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Lead Note Delete Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function data_put($id = '')
    {
        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Lead Note ID'
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'notes', $data = [
                'description' => nl2br($_POST['description']),
            ]);

            if ($this->db->affected_rows() > 0) {
                // success
                $message = array(
                    'status' => TRUE,
                    'message' => 'Lead Note Update Successful.'
                );
                $this->response($message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Lead Note Update Fail.'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

}
