<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_lead_status extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();
        if($user){
            $staffid = $user->staffid;
        }else{
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $assigned = $this->input->get('salesperson');
        $tagids = $this->input->get('tagids');
        $data = $this->db->select('*')->from(db_prefix() . 'leads_status')->get()->result_array();

        if ($data)
        {
            foreach($data as $key=>$row){
                $leads = $this->db->select('*')->where('status', $row['id']);
                if($user->admin==0){ $this->db->where('assigned', $staffid); }
                else{ if($assigned){ $this->db->where('assigned', $assigned); } }
                $leads = $this->db->get(db_prefix() . 'leads')->result_array();
                
                if($tagids){
                    $leads = $this->db->select('DISTINCT(id)');
                    $this->db->join(db_prefix() . 'leads', db_prefix() . 'leads.id=' . db_prefix() . 'taggables.rel_id', 'left');
                    $this->db->where('status', $row['id'])->where(db_prefix() . 'taggables.rel_type','lead')->where_in(db_prefix() . 'taggables.tag_id', [$tagids]);
                    if($user->admin==0){ $this->db->where('assigned', $staffid); }
                    else{ if($assigned){ $this->db->where('assigned', $assigned); } }
                    //$this->db->group_by(db_prefix() . 'leads.id');
                    $leads = $this->db->get(db_prefix() . 'taggables')->result_array();
                }
                //var_dump($this->db->last_query()); die();
                // if($user->admin==0){ $leads = $this->db->where('status', $row['id'])->where('assigned', $staffid)->get(db_prefix() . 'leads')->result_array(); }
                // else{ $leads = $this->db->where('status', $row['id'])->get(db_prefix() . 'leads')->result_array(); }
                $data[$key]['leads_count'] = count($leads);
            }

            // Set the response and exit
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    //Change Lead Status
    public function data_post($id = '')
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        // form validation
        $this->form_validation->set_rules('leadid', _l('Lead ID'), 'required');
        $this->form_validation->set_rules('statusid', _l('Status ID'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $this->load->model('leads_model');
            $data = array();
            $data['status'] = $_POST['statusid'];
            $this->db->where('id', $_POST['leadid']);
            $this->db->update(db_prefix() . 'leads', $data);
            if ($this->db->affected_rows() > 0) {
                // success
                $message = array(
                    'status' => TRUE,
                    'message' => 'Lead Status Update Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Lead Status Update Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }
}
