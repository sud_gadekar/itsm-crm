<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Renewals extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('renewals', $id);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"renewals", $id);

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('renewal', $key);

        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"renewals");

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
