<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Fcm_token_add extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        
    }

    public function data_post()
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'contacts')->row();

        if($user){
            $contact_id = $user->id; 
            $client_id = $user->userid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        
        //Update FCM Token
        $this->form_validation->set_rules('fcm_token', _l('FCM Token'), 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $this->load->model('clients_model');
            $contact = $this->clients_model->get_contact($contact_id);

            $output = $this->clients_model->update_contact([
                'fcm_token'          => $this->input->post('fcm_token'),
            ], $contact_id, true);

            // success
            $message = array(
                'status' => TRUE,
                'message' => 'FCM Token update successful.'
            );
            $this->response((object) $message, REST_Controller::HTTP_OK);
        }
    }
}
