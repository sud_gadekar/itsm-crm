<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_tickets extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $status_id = $this->input->get('status_id');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');
        $userid = $this->input->get('customerid');
        $assigned = $this->input->get('staffid');
        $data = $this->Api_model->get_table('admin_tickets', $id, false, false, false, $status_id, 'admin', $start, $limit, $userid, $assigned);

        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $headers = apache_request_headers();
            $this->db->where('token', $headers['token']);
            $user = $this->db->get(db_prefix() . 'staff')->row();

            $data = $this->Api_model->get_api_custom_data($data,"tickets", $id);
            if(is_numeric($id)){
                $admin_staff = $this->db->select('firstname, lastname')->where('staffid',$data->admin)->get(db_prefix() . 'staff')->row();
                $data->edit_incident = true;
                $data->addedby_name = $admin_staff->firstname.' '.$admin_staff->lastname;
                if($user->admin==0){
                    $permissions = $this->db->select('capability')->where('staff_id', $user->staffid)->where('feature', 'incidents')->where('capability', 'edit')->get(db_prefix() . 'staff_permissions')->row();
                    if($permissions && $data->assigned == $user->staffid){ $data->edit_incident = true; }
                    else{ $data->edit_incident = false; }
                }

                $data->replies = $this->Tickets_model->get_ticket_replies($id);
                $ticket_replies_mail = $this->db->like('subject','[Ticket ID: '.$id.']')->like('subject','Ticket')->get(db_prefix().'mail_inbox')->result_array();

                foreach($ticket_replies_mail as $key=>$mail_reply){
                    $reply['id'] = $mail_reply['id'];
                    $reply['from_name'] = $mail_reply['sender_name'];
                    $reply['reply_email'] = 1;
                    $reply['admin'] = 0;
                    $reply['userid'] = 0;
                    $reply['message'] = $mail_reply['body'];
                    $reply['date'] = $mail_reply['date_received'];
                    $reply['contactid'] = 0;
                    $reply['submitter'] = trim($mail_reply['from_email'],'\'"');
                    $reply['attachments'] = [];
                    array_push($data->replies, $reply);
                }

                // Get related notes
                $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid=' . db_prefix() . 'notes.addedfrom');
                $this->db->where('rel_type', 'ticket');
                $this->db->where('rel_id', $id);
                $data->notes = $this->db->select(db_prefix() . 'notes.*,' . db_prefix() . 'staff.firstname,' . db_prefix() . 'staff.lastname')->get(db_prefix() . 'notes')->result_array();
            }else{
                foreach($data as $key=>$row){
                    $data[$key]['edit_incident'] = true;
                    if($user->admin==0){
                        $permissions = $this->db->select('capability')->where('staff_id', $user->staffid)->where('feature', 'incidents')->where('capability', 'edit')->get(db_prefix() . 'staff_permissions')->row();
                        if($permissions && $row['assigned'] == $user->staffid){ $data[$key]['edit_incident'] = true; }
                        else{ $data[$key]['edit_incident'] = false; }
                    }
                }
            }

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'data' => [],
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('ticket', $key);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"tickets");

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    public function data_post($id = '')
    {   
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Add replay
        if (is_numeric($id)) {
            $this->form_validation->set_rules('message', _l('Reply message'), 'required');
            if ($this->form_validation->run() == FALSE)
            {
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            else
            {
                // insert data
                $data['ticketid'] = $id;
                $data['date']     = date('Y-m-d H:i:s');
                $data['message']  = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['admin'] = $staffid;
                $this->db->insert(db_prefix() . 'ticket_replies', $data);
                $insert_id = $this->db->insert_id();

                if($insert_id > 0 && !empty($insert_id)){ // success

                    //send reply added mail 
                    $ticket    = $this->Tickets_model->get_ticket_by_id($id);
                    $email     = $this->clients_model->get_contact($ticket->contactid)->email;
                    $_attachments = null;
                    $cc = null;
                    $sendEmail = true;
                    if ($isContact && total_rows(db_prefix() . 'contacts', ['ticket_emails' => 1, 'id' => $ticket->contactid]) == 0) {
                        $sendEmail = false;
                    }
                    if ($sendEmail) {
                        send_mail_template('ticket_new_reply_to_customer', $ticket, $email, $_attachments, $cc);
                    }

                    //send push notification
                    $contactid = $ticket->contactid;
                    $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
                    $token = $user_fcm->fcm_token;
                    $notification_data['module']    = 'incident';
                    $notification_data['id']        = $id;
                    $notification_data['title']     = 'New Incident Ticket Reply';
                    $notification_data['body']      = 'New Incident Ticket Reply Added [ReplyID: ' . $insert_id . ']';
                    push_notification($token, $notification_data);

                    log_activity('New Ticket Reply [ReplyID: ' . $insert_id . ']');
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Comment added successfully.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }else{ // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Comment add failed.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }
            }
        }

        // form validation
        $this->form_validation->set_rules('subject', _l('customer_ticket_subject'), 'required');
        $this->form_validation->set_rules('staffid', _l('Staff ID'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'subject' => $this->input->post('subject', TRUE),
                'service' => $this->input->post('service', TRUE),
                'message' => $this->Api_model->value($this->input->post('message', TRUE)),
                'assigned' => $this->input->post('staffid', TRUE), //$staffid,
                'email' => $this->clients_model->get_contact($this->input->post('contactid', TRUE))->email,
                'contactid' => $this->input->post('contactid', TRUE),
                'userid'    => $this->db->where('id', $this->input->post('contactid', TRUE))->get(db_prefix() . 'contacts')->row()->userid
             ];
            if (!empty($this->input->post('custom_fields', TRUE))) {
                $insert_data['custom_fields'] = $this->Api_model->value($this->input->post('custom_fields', TRUE));
            }
               
            // insert data
            $this->load->model('tickets_model');
            $output = $this->tickets_model->add($insert_data);
            if($output > 0 && !empty($output)){
                // success
                $data = [];
                $data['message'] = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['status'] = 1;
                $data['admin'] = $staffid;
                $reply_id = $this->tickets_model->add_reply($data, $output, null);

                if ($reply_id > 0 && !empty($reply_id)) {
                    $this->db->where('ticketid', $output);
                    $this->db->update(db_prefix() . 'ticket_attachments', [
                        'replyid' => $reply_id,
                    ]);
                }

                $message = array(
                'status' => TRUE,
                'message' => 'Ticket add successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Ticket add fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }

    public function data_put($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Ticket ID'
            );
            $this->response((object) $message, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->load->model('tickets_model');
            $data = array();
            $data['status'] = $_POST['status_id'];
            //$data['message'] = $_POST['note'];
            $this->db->where('ticketid', $id);
            $this->db->update(db_prefix() . 'tickets', $data);
            if ($this->db->affected_rows() > 0) {

                $this->db->where('ticketstatusid', $_POST['status_id']);
                $status = $this->db->get(db_prefix() . 'tickets_status')->row();

                $ticket = $this->tickets_model->get_ticket_by_id($id);
                if($_POST['status_id'] == 5)
                {
                    $admin ='';
                    $cc = ''; 
                    send_mail_template('ticket_closed_to_customer', $ticket, $ticket->email, $admin == null ? [] : $_attachments, $cc);
                }
                // success
                //send push notification
                $contactid = $ticket->contactid;
                $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
                $token = $user_fcm->fcm_token;
                $notification_data['module']    = 'incident';
                $notification_data['id']        = $id;
                $notification_data['title']     = 'Incident Ticket Status Updated';
                $notification_data['body']      = 'Ticket status marked to '.$status->name.' [TicketID: ' . $ticket->ticketpattern.$id . ']';
                push_notification($token, $notification_data);

                $message = array(
                    'status' => TRUE,
                    'message' => 'Ticket Status Update Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Ticket Status Update Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }
}
