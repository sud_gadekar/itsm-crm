<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Login extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_post($id = '')
    {
        //echo "Hello"; die;
        //header("Access-Control-Allow-Origin: *");
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // generate a token
        $login_token = md5(uniqid(rand(), true));
    
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('login', $id, $email, $password, $login_token);

        // Check if the data store contains
        if ($data)
        {
            $this->db->where('token', $login_token);
            $user = $this->db->get(db_prefix() . 'contacts')->row();
            $user->profile_image = "uploads/client_profile_images/".$user->profile_image;
            $permissions = $this->db->where('userid', $user->id)->get(db_prefix() . 'contact_permissions')->result_array();
            $per_array = array();
            foreach($permissions as $permission){
                array_push($per_array, $permission['permission_id']);
            }
            $per = implode(",", $per_array);
            // return data
            $this->response(
                [
                    'status' => true,
                    'message' => 'Login Successfull',
                    'contact_permissions' => $per,
                    'result' => $user,
                ],
                REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Invalid Login Details'
            ], REST_Controller::HTTP_OK); // HTTP_NOT_FOUND NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        // return data
        $this->api_return(
            [
                'status' => true,
                "result" => [
                    'user_data' => $user_data['token_data']
                ],
            ],
        200);
    }

    public function api_key()
    {
        $this->_APIConfig([
            'methods' => ['POST'],
            'key' => ['header', 'Set API Key'],
        ]);
    }
}
