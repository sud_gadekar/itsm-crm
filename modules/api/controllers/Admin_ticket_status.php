<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_ticket_status extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();
        if($user){
            $staffid = $user->staffid;
        }else{
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $userid = $this->input->get('customerid');
        $assigned = $this->input->get('staffid');

        // if($user->admin!=1){
        //     $this->db->where("(name NOT LIKE '%close%')", NULL, FALSE);
        // }
        $this->db->order_by('statusorder', 'asc');
        $data = $this->db->get(db_prefix() . 'tickets_status')->result_array();
        // Check if the data store contains
        foreach($data as $key=>$row){
            $ticket_count = $this->db->select('status')->where('status', $row['ticketstatusid']);
            if($user->admin==0){ $this->db->where('assigned', $staffid); }
            else{ if($assigned){ $this->db->where('assigned', $assigned); } }
            if($userid){ $this->db->where('userid', $userid); }
            $ticket_count = $this->db->get(db_prefix() . 'tickets')->result_array();

            // if($user->admin==1){
            //     $ticket_count = $this->db->select('status')->where('status', $row['ticketstatusid'])->get(db_prefix() . 'tickets')->result_array();
            // }else{
            //     $ticket_count = $this->db->select('status')->where('assigned', $staffid)->where('status', $row['ticketstatusid'])->get(db_prefix() . 'tickets')->result_array();
            // }
            $data[$key]['count'] = count($ticket_count);
            $data[$key]['isSelected'] = false;
        }
        if ($data)
        {
            // Set the response and exit
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
