<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_service_requests extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // If the id parameter doesn't exist return all the
        $status_id = $this->input->get('status_id');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');
        $userid = $this->input->get('customerid');
        $assigned = $this->input->get('staffid');
        $data = $this->Api_model->get_table('admin_service_requests', $id, false, false, false, $status_id, 'admin', $start, $limit, $userid, $assigned);

        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $headers = apache_request_headers();
            $this->db->where('token', $headers['token']);
            $user = $this->db->get(db_prefix() . 'staff')->row();

            $data = $this->Api_model->get_api_custom_data($data,"service_requests", $id);
            if(is_numeric($id)){
                $admin_staff = $this->db->select('firstname, lastname')->where('staffid',$data->admin)->get(db_prefix() . 'staff')->row();
                $data->edit_sr = true;
                $data->addedby_name = $admin_staff->firstname.' '.$admin_staff->lastname;
                if($user->admin==0){
                    $permissions = $this->db->select('capability')->where('staff_id', $user->staffid)->where('feature', 'service_requests')->where('capability', 'edit')->get(db_prefix() . 'staff_permissions')->row();
                    if($permissions && $data->assigned == $user->staffid){ $data->edit_sr = true; }
                    else{ $data->edit_sr = false; }
                }

                $data->replies = $this->Service_requests_model->get_service_request_replies($id);
                $service_request_replies_mail = $this->db->like('subject','[service_request ID: '.$id.']')->like('subject','Service request')->get(db_prefix().'mail_inbox')->result_array();

                foreach($service_request_replies_mail as $key=>$mail_reply){
                    $reply['id'] = $mail_reply['id'];
                    $reply['from_name'] = $mail_reply['sender_name'];
                    $reply['reply_email'] = 1;
                    $reply['admin'] = 0;
                    $reply['userid'] = 0;
                    $reply['message'] = $mail_reply['body'];
                    $reply['date'] = $mail_reply['date_received'];
                    $reply['contactid'] = 0;
                    $reply['submitter'] = trim($mail_reply['from_email'],'\'"');
                    $reply['attachments'] = [];
                    array_push($data->replies, $reply);
                }

                // Get related notes
                $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid=' . db_prefix() . 'notes.addedfrom');
                $this->db->where('rel_type', 'service_request');
                $this->db->where('rel_id', $id);
                $data->notes = $this->db->select(db_prefix() . 'notes.*,' . db_prefix() . 'staff.firstname,' . db_prefix() . 'staff.lastname')->get(db_prefix() . 'notes')->result_array();
            }else{
                foreach($data as $key=>$row){
                    $data[$key]['edit_sr'] = true;
                    if($user->admin==0){
                        $permissions = $this->db->select('capability')->where('staff_id', $user->staffid)->where('feature', 'service_requests')->where('capability', 'edit')->get(db_prefix() . 'staff_permissions')->row();
                        if($permissions && $row['assigned'] == $user->staffid){ $data[$key]['edit_sr'] = true; }
                        else{ $data[$key]['edit_sr'] = false; }
                    }
                }
            }

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'data' => [],
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function data_search_get($key = '')
    {
        $data = $this->Api_model->search('service_request', $key);
        
        if ($data == 'UNAUTHORIZED'){
            $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }
        // Check if the data store contains
        if ($data)
        {
            $data = $this->Api_model->get_api_custom_data($data,"service_requests");

            // Set the response and exit
            //$this->response((object) $data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response([
                'data' => $data,
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    public function data_post($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        //Add replay
        if (is_numeric($id)) {
            $this->form_validation->set_rules('message', _l('Reply message'), 'required');
            if ($this->form_validation->run() == FALSE)
            {
                // form validation error
                $message = array(
                    'status' => FALSE,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors() 
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
            else
            {
                // insert data
                $data['service_requestid'] = $id;
                $data['date']     = date('Y-m-d H:i:s');
                $data['message']  = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['admin'] = $staffid;
                $this->db->insert(db_prefix() . 'service_request_replies', $data);
                $insert_id = $this->db->insert_id();

                if($insert_id > 0 && !empty($insert_id)){ // success

                    // send reply added mail 
                    $this->load->model('service_requests_model');
                    $this->load->model('clients_model');
                    $service_request    = $this->Service_requests_model->get_service_request_by_id($id);
                    $email              = $this->clients_model->get_contact($service_request->contactid)->email;
                    $_attachments = null;
                    $cc = null;
                    $sendEmail = true;
                    if ($isContact && total_rows(db_prefix() . 'contacts', ['service_request_emails' => 1, 'id' => $service_request->contactid]) == 0) {
                        $sendEmail = false;
                    }
                    if ($sendEmail) {
                        send_mail_template('service_request_new_reply_to_customer', $service_request, $email, $_attachments, $cc);
                    }

                    //send push notification
                    $contactid = $service_request->contactid;
                    $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
                    $token = $user_fcm->fcm_token;
                    $notification_data['module']    = 'service_request';
                    $notification_data['id']        = $id;
                    $notification_data['title']     = 'New SR Ticket Reply';
                    $notification_data['body']      = 'New SR Ticket Reply Added [ReplyID: ' . $insert_id . ']';
                    push_notification($token, $notification_data);

                    log_activity('New Service Request Reply [ReplyID: ' . $insert_id . ']');
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Comment added successfully.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }else{ // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'Comment add failed.'
                    );
                    $this->response((object) $message, REST_Controller::HTTP_OK);
                }
            }
        }

        // form validation
        $this->form_validation->set_rules('subject', _l('customer_ticket_subject'), 'required');
        $this->form_validation->set_rules('staffid', _l('Staff ID'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'subject' => $this->input->post('subject', TRUE),
                'service' => $this->input->post('service', TRUE),
                'request_for' => $this->input->post('request_for', TRUE),
                'message' => $this->Api_model->value($this->input->post('message', TRUE)),
                'assigned' => $this->input->post('staffid', TRUE), //$staffid,
                'email' => $this->clients_model->get_contact($this->input->post('contactid', TRUE))->email,
                'contactid' => $this->input->post('contactid', TRUE),
                'userid'    => $this->db->where('id', $this->input->post('contactid', TRUE))->get(db_prefix() . 'contacts')->row()->userid
             ];
            if (!empty($this->input->post('custom_fields', TRUE))) {
                $insert_data['custom_fields'] = $this->Api_model->value($this->input->post('custom_fields', TRUE));
            }
               
            // insert data
            $this->load->model('service_requests_model');
            $output = $this->service_requests_model->add($insert_data);
            if($output > 0 && !empty($output)){
                // success
                $data = [];
                $data['message'] = trim($this->Api_model->value($this->input->post('message', TRUE)));
                $data['status'] = 1;
                $data['userid'] = $client_id;
                $data['contactid'] = $contact_id;
                $reply_id = $this->service_requests_model->add_reply($data, $output, null);

                if ($reply_id > 0 && !empty($reply_id)) {
                    // $this->db->where('service_requestid', $output);
                    // $this->db->update(db_prefix() . 'service_request_attachments', [
                    //     'replyid' => $reply_id,
                    // ]);
                    if($_FILES["attachments"]){
                        $target_dir = "uploads/service_request_attachments/".$output."/";
                        if (!file_exists($target_dir)) {
                            mkdir($target_dir, 0755);
                            $fp = fopen($target_dir . 'index.html', 'w');
                            fclose($fp);
                        }
                        $target_file = $target_dir . basename($_FILES["attachments"]["name"][0]);
                        if (move_uploaded_file($_FILES["attachments"]["tmp_name"][0], $target_file)) {
                            $attachment['dateadded'] = date('Y-m-d H:i:s');
                            $attachment['file_name'] = $_FILES["attachments"]["name"][0];
                            $attachment['filetype']  = $_FILES["attachments"]["type"][0];
                            $attachment['service_requestid']  = $output;
                            $attachment['replyid'] = $reply_id;
                            $this->db->insert(db_prefix() . 'service_request_attachments', $attachment);
                        }
                        else{ // attachment error
                            $message = array(
                                'status' => FALSE,
                                'message' => 'Sorry, there was an error uploading your file.'
                            );
                            $this->response((object) $message, REST_Controller::HTTP_OK);
                        }
                    }
                }

                $message = array(
                'status' => TRUE,
                'message' => 'Service Request add successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Service Request add fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }

    public function data_put($id = '')
    {
        $headers = apache_request_headers();
        $this->db->where('token', $headers['token']);
        $user = $this->db->get(db_prefix() . 'staff')->row();

        if($user){
            $staffid = $user->staffid;
        }else{
            return $this->response([
                'status' => FALSE,
                'message' => "Unauthorized Access"
            ], REST_Controller::HTTP_UNAUTHORIZED);
        }

        $_POST = json_decode($this->security->xss_clean(file_get_contents("php://input")), true);
        if(empty($_POST ) || !isset($_POST ))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Data Not Acceptable OR Not Provided'
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
        $this->form_validation->set_data($_POST);
        
        if(empty($id) && !is_numeric($id))
        {
            $message = array(
            'status' => FALSE,
            'message' => 'Invalid Service Request ID'
            );
            $this->response((object) $message, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->load->model('service_requests_model');
            $data = array();
            $data['status'] = $_POST['status_id'];
            //$data['message'] = $_POST['note'];
            $this->db->where('service_requestid', $id);
            $this->db->update(db_prefix() . 'service_requests', $data);
            if ($this->db->affected_rows() > 0) {

                $this->db->where('service_requeststatusid', $_POST['status_id']);
                $status = $this->db->get(db_prefix() . 'service_requests_status')->row();

                $service_request = $this->service_requests_model->get_service_request_by_id($id);
                if($_POST['status_id'] == 5)
                {
                    $admin ='';
                    $cc = ''; 
                    send_mail_template('service_request_closed_to_customer', $service_request, $service_request->email, $admin == null ? [] : $_attachments, $cc);
                }
                // success
                //send push notification
                $contactid = $service_request->contactid;
                $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
                $token = $user_fcm->fcm_token;
                $notification_data['module']    = 'service_request';
                $notification_data['id']        = $id;
                $notification_data['title']     = 'SR Ticket Status Updated';
                $notification_data['body']      = 'Ticket status marked to '.$status->name.' [SRTicketID: ' . $service_request->servicerequestpattern.$id . ']';
                push_notification($token, $notification_data);
                $message = array(
                    'status' => TRUE,
                    'message' => 'Service Request Status Update Successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                    'status' => FALSE,
                    'message' => 'Service Request Status Update Fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }
        }
    }
}
