<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Guest_contact extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_get($id = '')
    {
        // Set the response and exit
        $this->response([
            'status' => FALSE,
            'message' => 'No data were found'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    public function data_post()
    {
        // form validation
        $this->form_validation->set_rules('subject', _l('Subject'), 'required');
        $this->form_validation->set_rules('name', _l('User Full Name'), 'required');
        $this->form_validation->set_rules('company', _l('Company Name'), 'required');
        $this->form_validation->set_rules('email', _l('Email'), 'required');
        $this->form_validation->set_rules('country_code', _l('Country Code'), 'required');
        $this->form_validation->set_rules('contact', _l('Contact No.'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            // form validation error
            $message = array(
                'status' => FALSE,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors() 
            );
            $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            $insert_data = [
                'subject' => $this->input->post('subject', TRUE),
                'name' => $this->input->post('name', TRUE),
                'company' => $this->input->post('company', TRUE),
                'email' => $this->input->post('email', TRUE),
                'country_code' => $this->input->post('country_code', TRUE),
                'contact' => $this->input->post('contact', TRUE),
                'comment' => $this->Api_model->value($this->input->post('comment', TRUE)),
            ];
               
            // insert data
            $email = 'rohit@decodingit.com';
            $subject = 'Guest Contact - '.$this->input->post('subject', TRUE);
            $message = '<span style="font-size: 12pt;">Hello,</span><br /><br /><span style="font-size: 12pt;">New contact enquiry has been received from app.</span><br /><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Subject:</strong> '.$this->input->post('subject', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>User Name:</strong> '.$this->input->post('name', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Company:</strong> '.$this->input->post('company', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Email:</strong> '.$this->input->post('email', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Country Code:</strong> '.$this->input->post('country_code', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Contact No:</strong> '.$this->input->post('contact', TRUE).'</span><br />';
            $message.= '<span style="font-size: 12pt;"><strong>Comment:</strong></span><br /><span style="font-size: 12pt;">'.$this->input->post('comment', TRUE).'</span>';

            $output = $this->db->insert(db_prefix() . 'guest_contacts', $insert_data);
            if($output > 0 && !empty($output)){

                $this->load->model('Emails_model');
                $mail_result = $this->Emails_model->send_simple_email($email, $subject, $message);

                // success
                $message = array(
                'status' => TRUE,
                'message' => 'Contact add successful.'
                );
                $this->response((object) $message, REST_Controller::HTTP_OK);
            }else{
                // error
                $message = array(
                'status' => FALSE,
                'message' => 'Contact add fail.'
                );
                $this->response((object) $message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
