<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require __DIR__.'/REST_Controller.php';
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Admin_login extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function data_post($id = '')
    {
        //echo "Hello"; die;
        //header("Access-Control-Allow-Origin: *");
        $email = $this->input->post('email');
        $password = $this->input->post('password'); 
        $code = $this->input->post('code');
        $type = $this->input->post('type');
        $code = trim($code);

        // generate a token
        $login_token = md5(uniqid(rand(), true));
    
        // If the id parameter doesn't exist return all the
        $data = $this->Api_model->get_table('admin_login', $id, $email, $password, $login_token);
        if($code){
            if ($type == 'email') {
                $result = $this->Authentication_model->is_two_factor_code_valid($code);
                if($result){
                    $this->db->where('token', $login_token);
                    $user = $this->db->get(db_prefix() . 'staff')->row();
                    $this->Authentication_model->clear_two_factor_auth_code($user->staffid);
                    if($user->profile_image){ 
                        $user->profile_image = "/uploads/staff_profile_images/".$user->staffid."/thumb_".$user->profile_image;
                    }
                    $permissions = $this->db->where('staff_id', $user->staffid)->get(db_prefix() . 'staff_permissions')->result_array();
                    // return data
                    $this->response(
                        [
                            'status' => true,
                            'message' => 'Login Successfull',
                            'contact_permissions' => $permissions,
                            'result' => $user,
                        ],
                        REST_Controller::HTTP_OK);
                }else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Invalid Code'
                    ], REST_Controller::HTTP_OK); 
                }
            } elseif ($type == 'app') {
                $result = $this->Authentication_model->is_google_two_factor_code_valid($code);
                if($result){
                    $this->db->where('token', $login_token);
                    $user = $this->db->get(db_prefix() . 'staff')->row();
                    if($user->profile_image){ 
                        $user->profile_image = "/uploads/staff_profile_images/".$user->staffid."/thumb_".$user->profile_image;
                    }
                    $permissions = $this->db->where('staff_id', $user->staffid)->get(db_prefix() . 'staff_permissions')->result_array();
                    // return data
                    $this->response(
                        [
                            'status' => true,
                            'message' => 'Login Successfull',
                            'contact_permissions' => $permissions,
                            'result' => $user,
                        ],
                        REST_Controller::HTTP_OK);
                }else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Invalid Passcode'
                    ], REST_Controller::HTTP_OK); 
                }
            }
        }

        // Check if the data store contains
        if (is_array($data) && isset($data['memberinactive'])) {
            $this->response([
                'status' => FALSE,
                'message' => _l('admin_auth_inactive_account')
            ], REST_Controller::HTTP_OK);
        } 
        elseif (is_array($data) && isset($data['two_factor_auth'])) {
            if ($data['user']->two_factor_auth_enabled == 1) {
                $this->Authentication_model->set_two_factor_auth_code($data['user']->staffid);
                $sent = send_mail_template('staff_two_factor_auth_key', $data['user']);

                if (!$sent) {
                    $this->response([
                        'status' => FALSE,
                        'message' => _l('two_factor_auth_failed_to_send_code')
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => TRUE,
                        'message' => _l('two_factor_auth_code_sent_successfully', $email),
                        'type' => 'email'
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => _l('enter_two_factor_auth_code_from_mobile'),
                    'type' => 'app'
                ], REST_Controller::HTTP_OK);
            }
        } elseif ($data == false) {
            //set_alert('danger', _l('admin_auth_invalid_email_or_password'));
            $this->response([
                'status' => FALSE,
                'message' => 'Invalid Login Details'
            ], REST_Controller::HTTP_OK);
        }

        $this->db->where('token', $login_token);
        $user = $this->db->get(db_prefix() . 'staff')->row();
        if($user->profile_image){
            $user->profile_image = "/uploads/staff_profile_images/".$user->staffid."/thumb_".$user->profile_image;
        }
        $permissions = $this->db->where('staff_id', $user->staffid)->get(db_prefix() . 'staff_permissions')->result_array();
        // return data
        $this->response(
            [
                'status' => true,
                'message' => 'Login Successfull',
                'contact_permissions' => $permissions,
                'result' => $user,
            ],
            REST_Controller::HTTP_OK);
        
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        $user_data = $this->_apiConfig([
            'methods' => ['POST'],
            'requireAuthorization' => true,
        ]);

        // return data
        $this->api_return(
            [
                'status' => true,
                "result" => [
                    'user_data' => $user_data['token_data']
                ],
            ],
        200);
    }

    public function api_key()
    {
        $this->_APIConfig([
            'methods' => ['POST'],
            'key' => ['header', 'Set API Key'],
        ]);
    }
}
