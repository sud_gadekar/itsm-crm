<div class="public-service_request mtop40">
	<?php hooks()->do_action('public_service_request_start', $service_request); ?>
	<?php if(is_staff_logged_in()) { ?>
		<div class="alert alert-warning mbot25">
			You are logged in a staff member, if you want to reply to the service_request as staff, you must make reply via the admin area.
		</div>
	<?php } ?>
	<h3 class="mbot25">
		#<?php echo $service_request->service_requestid; ?> - <?php echo $service_request->subject; ?>
	</h3>
	<?php echo $single_service_request_view; ?>
	<?php hooks()->do_action('public_service_request_end', $service_request); ?>
</div>
