<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if ($boq['status'] == $status) { ?>
<li data-boq-id="<?php echo $boq['id']; ?>" class="<?php if($boq['invoice_id'] != NULL || $boq['estimate_id'] != NULL){echo 'not-sortable';} ?>">
   <div class="panel-body">
      <div class="row">
         <div class="col-md-12">
            <h4 class="bold pipeline-heading">
               <a href="<?php echo admin_url('boqs/list_boqs/'.$boq['id']); ?>" data-toggle="tooltip" data-title="<?php echo $boq['subject']; ?>" onclick="boq_pipeline_open(<?php echo $boq['id']; ?>); return false;"><?php echo format_boq_number($boq['id']); ?></a>
               <?php if(has_permission('estimates','','edit')){ ?>
               <a href="<?php echo admin_url('boqs/boq/'.$boq['id']); ?>" target="_blank" class="pull-right"><small><i class="fa fa-pencil-square-o" aria-hidden="true"></i></small></a>
               <?php } ?>
            </h4>
            <span class="mbot10 inline-block full-width">
            <?php
               if($boq['rel_type'] == 'lead'){
                 echo '<a href="'.admin_url('leads/index/'.$boq['rel_id']).'" onclick="init_lead('.$boq['rel_id'].'); return false;" data-toggle="tooltip" data-title="'._l('lead').'">' .$boq['boq_to'].'</a><br />';
               } else if($boq['rel_type'] == 'customer'){
                 echo '<a href="'.admin_url('clients/client/'.$boq['rel_id']).'" data-toggle="tooltip" data-title="'._l('client').'">' .$boq['boq_to'].'</a><br />';
               }
               ?>
            </span>
         </div>
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-8">
                  <?php if($boq['total'] != 0){
                     ?>
                  <span class="bold"><?php echo _l('boq_total'); ?>:
                     <?php echo app_format_money($boq['total'], get_currency($boq['currency'])); ?>
                  </span>
                  <br />
                  <?php } ?>
                  <?php echo _l('boq_date'); ?>: <?php echo _d($boq['date']); ?>
                  <?php if(is_date($boq['open_till'])){ ?>
                  <br />
                  <?php echo _l('boq_open_till'); ?>: <?php echo _d($boq['open_till']); ?>
                  <?php } ?>
                  <br />
               </div>
               <div class="col-md-4 text-right">
                  <small><i class="fa fa-comments" aria-hidden="true"></i> <?php echo _l('boq_comments'); ?>: <?php echo total_rows(db_prefix().'boq_comments', array(
                     'boqid' => $boq['id']
                     )); ?></small>
               </div>
               <?php $tags = get_tags_in($boq['id'],'boq');
                  if(count($tags) > 0){ ?>
               <div class="col-md-12">
                  <div class="mtop5 kanban-tags">
                     <?php echo render_tags($tags); ?>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</li>
<?php } ?>
