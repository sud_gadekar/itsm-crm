<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active">
    <a href="#set_service_requests_general" aria-controls="set_service_requests_general" role="tab" data-toggle="tab"><?php echo _l('settings_group_general'); ?></a>
  </li>
  <li role="presentation">
    <a href="#set_service_requests_piping" aria-controls="set_service_requests_piping" role="tab" data-toggle="tab"><?php echo _l('service_requests_piping'); ?></a>
  </li>
  <li role="presentation">
    <a href="#service_request_form" aria-controls="service_request_form" role="tab" data-toggle="tab"><?php echo _l('service_request_form'); ?></a>
  </li>
</ul>
<div class="tab-content mtop30">
  <div role="tabpanel" class="tab-pane active" id="set_service_requests_general">
    <?php render_yes_no_option('services','settings_service_requests_use_services'); ?>
    <hr />
    <?php render_yes_no_option('staff_access_only_assigned_departments','settings_service_requests_allow_departments_access'); ?>
    <hr />
    <?php render_yes_no_option('receive_notification_on_new_service_request','receive_notification_on_new_service_request', 'receive_notification_on_new_service_request_help'); ?>
    <hr />
    <?php render_yes_no_option('receive_notification_on_new_service_request_replies','receive_notification_on_new_service_request_replies', 'receive_notification_on_new_service_request_reply_help'); ?>
    <hr />
    <?php render_yes_no_option('staff_members_open_service_requests_to_all_contacts','staff_members_open_service_requests_to_all_contacts','staff_members_open_service_requests_to_all_contacts_help'); ?>
    <hr />
    <?php render_yes_no_option('access_service_requests_to_none_staff_members','access_service_requests_to_none_staff_members'); ?>
    <hr />
    <?php render_yes_no_option('allow_non_admin_staff_to_delete_service_request_attachments','allow_non_admin_staff_to_delete_service_request_attachments'); ?>
    <hr />
    <?php render_yes_no_option('allow_customer_to_change_service_request_status','allow_customer_to_change_service_request_status'); ?>
    <hr />
    <?php render_yes_no_option('only_show_contact_service_requests','only_show_contact_service_requests'); ?>
    <hr />
    <?php render_yes_no_option('service_request_replies_order','service_request_replies_order','service_request_replies_order_notice',_l('order_ascending'),_l('order_descending'),'asc','desc'); ?>
    <hr />
    <?php
      $this->load->model('service_requests_model');
      $statuses = $this->service_requests_model->get_service_request_status();
      $statuses['callback_translate'] = 'service_request_status_translate';
      echo render_select('settings[default_service_request_reply_status]',$statuses,array('service_requeststatusid','name'),'default_service_request_reply_status',get_option('default_service_request_reply_status'),array(),array(),'','',false); ?>
    <hr />
    <?php echo render_input('settings[maximum_allowed_service_request_attachments]','settings_service_requests_max_attachments',get_option('maximum_allowed_service_request_attachments'),'number'); ?>
    <hr />
    <?php echo render_input('settings[service_request_attachments_file_extensions]','settings_service_requests_allowed_file_extensions',get_option('service_request_attachments_file_extensions')); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="set_service_requests_piping">
    cPanel forwarder path: <code><?php echo hooks()->apply_filters('cpanel_service_requests_forwarder_path', FCPATH .'pipe.php'); ?></code>
    <hr />
    <?php render_yes_no_option('email_piping_only_registered','email_piping_only_registered'); ?>
    <hr />
    <?php render_yes_no_option('email_piping_only_replies','email_piping_only_replies'); ?>
    <hr />
    <?php render_yes_no_option('service_request_import_reply_only','service_request_import_reply_only'); ?>
    <hr />
    <?php echo render_select('settings[email_piping_default_priority]',$service_request_priorities,array('priorityid','name'),'email_piping_default_priority',get_option('email_piping_default_priority')); ?>
  </div>
  <div role="tabpanel" class="tab-pane" id="service_request_form">
    <h4 class="bold">Form Info</h4>
    <p><b>Form url:</b>
     <span class="label label-default">
        <a href="<?php echo site_url('forms/service_request'); ?>" target="_blank">
        <?php echo site_url('forms/service_request'); ?>
      </a>
     </span>
    </p>
    <p><b>Form file location:</b> <code><?php echo hooks()->apply_filters('service_request_form_file_location_settings', VIEWPATH.'forms\service_request.php'); ?></code></p>
    <hr />
    <h4 class="bold">Embed form</h4>
    <p><?php echo _l('form_integration_code_help'); ?></p>
    <textarea class="form-control" rows="2"><iframe width="600" height="850" src="<?php echo site_url('forms/service_request'); ?>" frameborder="0" allowfullscreen></iframe></textarea>
    <p class="bold mtop15">When placing the iframe snippet code consider the following:</p>
    <p class="<?php if(strpos(site_url(),'http://') !== false){echo 'bold text-success';} ?>">1. If the protocol of your installation is http use a http page inside the iframe.</p>
    <p class="<?php if(strpos(site_url(),'https://') !== false){echo 'bold text-success';} ?>">2. If the protocol of your installation is https use a https page inside the iframe.</p>
    <p>None SSL installation will need to place the link in non ssl eq. landing page and backwards.</p>
    <hr />
    <h4 class="bold">Change form container column (Bootstrap)</h4>
    <p>
      <span class="label label-default">
      <a href="<?php echo site_url('forms/service_request?col=col-md-8'); ?>" target="_blank">
        <?php echo site_url('forms/service_request?col=col-md-8'); ?>
      </a>
    </span>
    </p>
    <p>
      <span class="label label-default">
      <a href="<?php echo site_url('forms/service_request?col=col-md-8+col-md-offset-2'); ?>" target="_blank"><?php echo site_url('forms/service_request?col=col-md-8+col-md-offset-2'); ?></a>
    </span>
    </p>
    <p>
      <span class="label label-default">
      <a href="<?php echo site_url('forms/service_request?col=col-md-5'); ?>" target="_blank">
        <?php echo site_url('forms/service_request?col=col-md-5'); ?>
      </a>
    </span>
    </p>
  </div>
