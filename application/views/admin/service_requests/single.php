<?php defined("BASEPATH") or exit("No direct script access allowed"); ?>
<?php init_head(); ?>
<?php set_service_request_open($service_request->adminread, $service_request->service_requestid); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
            <div class="horizontal-scrollable-tabs">
              <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
              <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
              <div class="horizontal-tabs">
                <ul class="nav nav-tabs no-margin nav-tabs-horizontal" role="tablist">
                  <li role="presentation" class="<?php if (!$this->session->flashdata("active_tab")) {
                    echo "active";
                    } ?>">
                    <a href="#addreply" aria-controls="addreply" role="tab" data-toggle="tab">
                      <?php echo _l("service_request_single_add_reply"); ?>
                    </a>
                  </li>
                  <li role="presentation">
                    <a href="#note" aria-controls="note" role="tab" data-toggle="tab">
                      <?php echo _l("service_request_single_add_note"); ?>
                    </a>
                  </li>
                  <li role="presentation">
                    <a href="#tab_reminders"
                      onclick="initDataTable('.table-reminders', admin_url + 'misc/get_reminders/' + <?php echo $service_request->service_requestid; ?> + '/' + 'service_request', undefined, undefined, undefined,[1,'asc']); return false;"
                      aria-controls="tab_reminders" role="tab" data-toggle="tab">
                      <?php echo _l("Reminders "); ?>
                      <?php
                      $total_reminders = total_rows(db_prefix() . "reminders", ["isnotified" => 0, "staff" => get_staff_user_id(), "rel_type" => "service_request", "rel_id" => $service_request->service_requestid, ]);
                      if ($total_reminders > 0) {
                      echo '<span class="badge">' . $total_reminders . "</span>";
                      }
                      ?>
                    </a>
                  </li>
                  <li role="presentation">
                    <a href="#otherservice_requests" onclick="init_table_service_requests(true);"
                      aria-controls="otherservice_requests" role="tab" data-toggle="tab">
                      <?php echo _l("service_request_single_other_user_service_requests"); ?>
                    </a>
                  </li>
                  <li role="presentation">
                    <a href="#tasks"
                      onclick="init_rel_tasks_table(<?php echo $service_request->service_requestid; ?>,'service_request'); return false;"
                      aria-controls="tasks" role="tab" data-toggle="tab">
                      <?php echo _l("tasks"); ?>
                    </a>
                  </li>
                  <li role="presentation" class="<?php if ($this->session->flashdata("active_tab_settings")) {
                    echo "active";
                    } ?>">
                    <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                      <?php echo _l("Details"); ?>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="panel_s">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8">
                <h3 class="mtop4 mbot20 pull-left">
                <span id="service_request_subject">
                  #<?php echo $service_request->service_requestid; ?> -
                  <?php echo $service_request->subject; ?>
                </span>
                <?php if ($service_request->project_id != 0) {
                echo "<br /><small>" . _l("service_request_linked_to_project", '<a href="' . admin_url("projects/view/" . $service_request->project_id) . '">' . get_project_name_by_id($service_request->project_id) . "</a>") . "</small>";
                } ?>
                </h3>
                <?php echo '<div
                class="label mtop5 mbot15' . (is_mobile() ? " " : " mleft15 ") . 'p8 pull-left single-service_request-status-label" style="background:' . $service_request->statuscolor . '">' . service_request_status_translate($service_request->service_requeststatusid) . "</div>"; ?>

                <?php if(isset($service_request->company)) { ?>
                    <a href="<?php echo admin_url('clients/client/'.$service_request->userid) ?>"
                        class="btn btn-info label mtop5 mbot15 p8 pull-left single-service_request-status-label"
                        style="margin-left: 3rem;">
                        <?php echo $service_request->company?>
                    </a>
                    <?php } ?>

                  <?php /* if($service_request->service_requeststatusid == 5 ) { ?>
                      <a href="#"
                        class="btn btn-info label mtop5 mbot15 p8 pull-left single-ticket-status-label"
                        style="margin-left: 3rem;">
                        <?php echo _l('Send SR Report')?>
                      </a>
                  <?php } */ ?>

                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 text-right">
                <div class="row">
                  <div class="col-md-6 col-md-offset-6">
                    <?php echo render_select("status_top", $statuses, ["service_requeststatusid", "name"], "", $service_request->status, [], [], "no-mbot", "", false); ?>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane <?php if (!$this->session->flashdata("active_tab")) {
                echo "active";
                } ?>" id="addreply">
                <hr class="no-mtop" />
                <?php $tags = get_tags_in($service_request->service_requestid, "service_request"); ?>
                <?php if (count($tags) > 0) { ?>
                <div class="row">
                  <div class="col-md-12">
                    <?php echo '<b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l("tags") . ":</b><br /><br /> " . render_tags($tags); ?>
                    <hr />
                  </div>
                </div>
                <?php
                } ?>
                <?php if (sizeof($service_request->service_request_notes) > 0) { ?>
                <div class="row">
                  <div class="col-md-12 mbot15">
                    <h4 class="bold"><?php echo _l("service_request_single_private_staff_notes"); ?></h4>
                    <div class="service_requeststaffnotes">
                      <div class="table-responsive">
                        <table>
                          <tbody>
                            <?php foreach ($service_request->service_request_notes as $note) { ?>
                            <tr>
                              <td>
                                <span class="bold">
                                  <?php echo staff_profile_image($note["addedfrom"], ["staff-profile-xs-image", ]); ?> <a href="<?php echo admin_url("staff/profile/" . $note["addedfrom"]); ?>"><?php echo _l("service_request_single_service_request_note_by", get_staff_full_name($note["addedfrom"])); ?>
                                  </a>
                                </span>
                                <?php if ($note["addedfrom"] == get_staff_user_id() || is_admin()) { ?>
                                <div class="pull-right">
                                  <a href="#" class="btn btn-default btn-icon"
                                    onclick="toggle_edit_note(<?php echo $note["id"]; ?>);return false;"><i
                                  class="fa fa-pencil-square-o"></i></a>
                                  <a href="<?php echo admin_url("misc/delete_note/" . $note["id"]); ?>" class="mright10 _delete btn btn-danger btn-icon">
                                    <i class="fa fa-remove"></i>
                                  </a>
                                </div>
                                <?php
                                } ?>
                                <hr class="hr-10" />
                                <div data-note-description="<?php echo $note["id"]; ?>">
                                  <?php echo check_for_links($note["description"]); ?>
                                </div>
                                <div data-note-edit-textarea="<?php echo $note["id"]; ?>" class="hide inline-block full-width">
                                  <textarea name="description" class="form-control"
                                  rows="4"><?php echo clear_textarea_breaks($note["description"]); ?></textarea>
                                  <div class="text-right mtop15">
                                    <button type="button" class="btn btn-default"
                                    onclick="toggle_edit_note(<?php echo $note["id"]; ?>);return false;"><?php echo _l("cancel"); ?></button>
                                    <button type="button" class="btn btn-info"
                                    onclick="edit_note(<?php echo $note["id"]; ?>);"><?php echo _l("update_note"); ?></button>
                                  </div>
                                </div>
                                <small class="bold">
                                <?php echo _l("service_request_single_note_added", _dt($note["dateadded"])); ?>
                                </small>
                              </td>
                            </tr>
                            <?php
                            } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                } ?>
                <div>
                  <?php echo form_open_multipart($this->uri->uri_string(), ["id" => "single-service_request-form", "novalidate" => true, ]); ?>
                  <a href="<?php echo admin_url("service_requests/delete/" . $service_request->service_requestid); ?>" class="btn btn-danger _delete btn-service_request-label mright5">
                    <i class="fa fa-remove"></i>
                  </a>
                  <?php if (!empty($service_request->priority_name)) { ?>
                  <span class="service_request-label label label-default inline-block">
                    <?php echo _l("service_request_single_priority", service_request_priority_translate($service_request->priorityid)); ?>
                  </span>
                  <?php
                  } ?>
                  <?php if (!empty($service_request->service_name)) { ?>
                  <span class="service_request-label label label-default inline-block">
                    <?php echo _l("service") . ": " . $service_request->service_name; ?>
                  </span>
                  <?php } ?>

                  <?php echo form_hidden('service_requestid',$service_request->service_requestid); ?>
                  <span class="service_request-label label label-default inline-block">
                      <?php echo _l('Company') . ': '. $service_request->company; ?>
                  </span>

                  <?php if ($service_request->assigned != 0) { ?>
                  <span class="service_request-label label label-info inline-block">
                    <?php echo _l("Assigned"); ?>: <?php echo get_staff_full_name($service_request->assigned); ?>
                  </span>
                  <?php
                  } ?>
                  <?php if ($service_request->lastreply !== null) { ?>
                  <span class="service_request-label label label-success inline-block"
                    data-toggle="tooltip" title="<?php echo _dt($service_request->lastreply); ?>">
                    <span class="text-has-action">
                      <?php echo _l("service_request_single_last_reply", time_ago($service_request->lastreply)); ?>
                    </span>
                  </span>
                  <?php
                  } ?>
                  <span class="service_request-label label label-info inline-block">
                    <a href="<?php echo get_service_request_public_url($service_request); ?>" target="_blank">
                      <?php echo _l("view_public_form"); ?>
                    </a>
                  </span>
                  <div class="mtop15">
                    <?php $use_knowledge_base = get_option("use_knowledge_base"); ?>
                    <div class="row mbot15">
                      <div class="col-md-6">
                        <select data-width="100%" id="insert_predefined_reply"
                          data-live-search="true" class="selectpicker" data-title="<?php echo _l("service_request_single_insert_predefined_reply"); ?>">
                          <?php foreach ($predefined_replies as $predefined_reply) { ?>
                          <option value="<?php echo $predefined_reply["id"]; ?>"><?php echo $predefined_reply["name"]; ?></option>
                          <?php
                          } ?>
                        </select>
                      </div>
                      <?php if ($use_knowledge_base == 1) { ?>
                      <div class="visible-xs">
                        <div class="mtop15"></div>
                      </div>
                      <div class="col-md-6">
                        <?php $groups = get_all_knowledge_base_articles_grouped(); ?>
                        <select data-width="100%" id="insert_knowledge_base_link"
                          class="selectpicker" data-live-search="true"
                          onchange="insert_service_request_knowledgebase_link(this);"
                          data-title="<?php echo _l("service_request_single_insert_knowledge_base_link"); ?>">
                          <option value=""></option>
                          <?php foreach ($groups as $group) { ?>
                          <?php if (count($group["articles"]) > 0) { ?>
                          <optgroup label="<?php echo $group["name"]; ?>">
                            <?php foreach ($group["articles"] as $article) { ?>
                            <option value="<?php echo $article["articleid"]; ?>">
                              <?php echo $article["subject"]; ?>
                            </option>
                            <?php
                            } ?>
                          </optgroup>
                          <?php
                          } ?>
                          <?php
                          } ?>
                        </select>
                      </div>
                      <?php
                      } ?>
                    </div>
                    <?php echo render_textarea("message", "", "", [], [], "", "tinymce"); ?>
                  </div>
                  <div class="panel_s service_request-reply-tools">
                    <div class="btn-bottom-toolbar text-right">
                      <button type="submit" class="btn btn-info"
                      data-form="#single-service_request-form" autocomplete="off"
                      data-loading-text="<?php echo _l("wait_text"); ?>">
                      <?php echo _l("service_request_single_add_response"); ?>
                      </button>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-5">
                          <?php echo render_select("status", $statuses, ["service_requeststatusid", "name"], "service_request_single_change_status", get_option("default_service_request_reply_status"), [], [], "", "", false); ?>
                          <?php //echo render_input('cc','CC'); ?>
                          <?php
                            $selected = array();
                            if(isset($service_request->cc_contacts)){
                              foreach(explode(',',$service_request->cc_contacts) as $member){
                                array_push($selected,$member);
                              }
                            } else {
                              array_push($selected,get_staff_user_id());
                            }
                            echo render_select('cc_contacts[]',$company_contacts,array('id','email'),'CC (Contacts)',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                          ?>
                          <?php
                            $selected = array();
                            if(isset($service_request->cc_staffs)){
                              foreach(explode(',',$service_request->cc_staffs) as $member){
                                array_push($selected,$member);
                              }
                            } else {
                              array_push($selected,get_staff_user_id());
                            }
                            echo render_select('cc_staffs[]',$company_staffs,array('staffid','email'),'CC (Staffs)',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                          ?>
                          <?php if ($service_request->assigned !== get_staff_user_id()) { ?>
                          <div class="checkbox">
                            <input type="checkbox" name="assign_to_current_user"
                            id="assign_to_current_user">
                            <label for="assign_to_current_user"><?php echo _l("service_request_single_assign_to_me_on_update"); ?></label>
                          </div>
                          <?php
                          } ?>
                          <div class="checkbox">
                            <input type="checkbox" <?php echo hooks()->apply_filters("service_request_add_response_and_back_to_list_default", "checked"); ?> name="service_request_add_response_and_back_to_list" value="1"
                            id="service_request_add_response_and_back_to_list">
                            <label for="service_request_add_response_and_back_to_list"><?php echo _l("service_request_add_response_and_back_to_list"); ?></label>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div class="row attachments">
                        <div class="attachment">
                          <div class="col-md-5 mbot15">
                            <div class="form-group">
                              <label for="attachment" class="control-label">
                                <?php echo _l("service_request_single_attachments"); ?>
                              </label>
                              <div class="input-group">
                                <input type="file" extension="<?php echo str_replace([".", " "], "", get_option("service_request_attachments_file_extensions")); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]"
                                accept="<?php echo get_service_request_form_accepted_mimes(); ?>">
                                <span class="input-group-btn">
                                  <button
                                  class="btn btn-success add_more_attachments p8-half"
                                  data-max="<?php echo get_option("maximum_allowed_service_request_attachments"); ?>" type="button"><i class="fa fa-plus"></i></button>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                </div>
                <div class="panel_s mtop20">
                  <div class="panel-body <?php if ($service_request->admin == null) {
                    echo "client-reply";
                    } ?>">
                    <div class="row">
                      <div
                        class="col-md-3 border-right service_request-submitter-info service_request-submitter-info">
                        <p>
                          <?php if ($service_request->admin == null || $service_request->admin == 0) { ?>
                          <?php if ($service_request->userid != 0) { ?>
                          <a href="<?php echo admin_url("clients/client/" . $service_request->userid . "?contactid=" . $service_request->contactid); ?>"><?php echo $service_request->submitter; ?>
                          </a>
                          <?php
                          } else {
                          echo $service_request->submitter; ?>
                          <br />
                          <a
                          href="mailto:<?php echo $service_request->service_request_email; ?>"><?php echo $service_request->service_request_email; ?></a>
                          <hr />
                          <?php if (total_rows(db_prefix() . "spam_filters", ["type" => "sender", "value" => $service_request->service_request_email, "rel_type" => "service_requests", ]) == 0) { ?>
                          <button type="button"
                          data-sender="<?php echo $service_request->service_request_email; ?>"
                          class="btn btn-danger block-sender btn-xs"> <?php echo _l("block_sender"); ?>
                          </button>
                          <?php
                          } else {
                          echo '<span class="label label-danger">' . _l("sender_blocked") . "</span>";
                          }
                          }
                          } else { ?>
                          <a href="<?php echo admin_url("profile/" . $service_request->admin); ?>"><?php echo $service_request->opened_by; ?></a>
                          <?php
                          } ?>
                        </p>
                        <p class="text-muted">
                          <?php if ($service_request->admin !== null || $service_request->admin != 0) {
                          echo _l("service_request_staff_string");
                          } else {
                          if ($service_request->userid != 0) {
                          echo _l("service_request_client_string");
                          }
                          } ?>
                        </p>
                        <?php if (has_permission("tasks", "", "create")) { ?>
                        <a href="#" class="btn btn-default btn-xs"
                        onclick="convert_service_request_to_task(<?php echo $service_request->service_requestid; ?>,'service_request'); return false;"><?php echo _l("convert_to_task"); ?></a>
                        <?php
                        } ?>
                      </div>
                      <div class="col-md-9">
                        <div class="row">
                          <div class="col-md-12 text-right">
                            <?php if (!empty($service_request->message)) { ?>
                            <a href="#"
                              onclick="print_service_request_message(<?php echo $service_request->service_requestid; ?>, 'service_request'); return false;"
                            class="mright5"><i class="fa fa-print"></i></a>
                            <?php
                            } ?>
                            <a href="#"
                              onclick="edit_service_request_message(<?php echo $service_request->service_requestid; ?>,'service_request'); return false;"><i
                            class="fa fa-pencil-square-o"></i></a>
                          </div>
                        </div>
                        <div data-service_request-id="<?php echo $service_request->service_requestid; ?>"
                          class="tc-content">
                          <?php echo check_for_links($service_request->message); ?>
                        </div>
                        <?php if (count($service_request->attachments) > 0) {
                        echo "<hr />";
                        foreach ($service_request->attachments as $attachment) {
                        $path = get_upload_path_by_type("service_request") . $service_request->service_requestid . "/" . $attachment["file_name"];
                        $is_image = is_image($path);
                        if ($is_image) {
                        echo '<div class="preview_image">';
                          }
                          ?>
                          <a href="<?php echo site_url("download/file/service_request/" . $attachment["id"]); ?>" class="display-block mbot5" <?php if ($is_image) { ?> data-lightbox="attachment-service_request-<?php echo $service_request->service_requestid; ?>" <?php
                            } ?>>
                            <i class="<?php echo get_mime_class($attachment["filetype"]); ?>"></i> <?php echo $attachment["file_name"]; ?>
                            <?php if ($is_image) { ?>
                            <img class="mtop5" src="<?php echo site_url("download/preview_image?path=" . protected_file_url_by_path($path) . "&type=" . $attachment["filetype"]); ?>">
                            <?php
                            } ?>
                          </a>
                          <?php
                          if ($is_image) {
                        echo "</div>";
                        }
                        if (is_admin() || (!is_admin() && get_option("allow_non_admin_staff_to_delete_service_request_attachments") == "1")) {
                        echo '<a href="' . admin_url("service_requests/delete_attachment/" . $attachment["id"]) . '" class="text-danger _delete">' . _l("delete") . "</a>";
                        }
                        echo "<hr />";
                        ?>
                        <?php
                        }
                        } ?>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer">
                    <?php echo _l("service_request_posted", _dt($service_request->date)); ?>
                  </div>
                </div>
                 <?php foreach ($service_request_replies as $reply) { ?>
                <div class="panel_s">
                  <div class="panel-body <?php if ($reply["admin"] == null) {
                    echo "client-reply";
                    } ?>">
                    <div class="row">
                      <div class="col-md-3 border-right service_request-submitter-info">
                        <p>
                          <?php if ($reply["admin"] == null || $reply["admin"] == 0) { ?>
                          <?php if ($reply["userid"] != 0) { ?>
                          <a href="<?php echo admin_url("clients/client/" . $reply["userid"] . "?contactid=" . $reply["contactid"]); ?>"><?php echo $reply["submitter"]; ?></a>
                          <?php
                          } else { ?>
                          <?php echo $reply["submitter"]; ?>
                          <br />
                          <a href="mailto:<?php echo $reply["reply_email"]; ?>"><?php echo $reply["reply_email"]; ?></a>
                          <?php
                          } ?>
                          <?php
                          } else { ?>
                          <a href="<?php echo admin_url("profile/" . $reply["admin"]); ?>"><?php echo $reply["submitter"]; ?></a>
                          <?php
                          } ?>
                        </p>
                        <p class="text-muted">
                          <?php if ($reply["admin"] !== null || $reply["admin"] != 0) {
                          echo _l("service_request_staff_string");
                          } else {
                          if ($reply["userid"] != 0) {
                          echo _l("service_request_client_string");
                          }
                          } ?>
                        </p>
                        <hr />
                        <a href="<?php echo admin_url("service_requests/delete_service_request_reply/" . $service_request->service_requestid . "/" . $reply["id"]); ?>" class="btn btn-danger pull-left _delete mright5 btn-xs"><?php echo _l("delete_service_request_reply"); ?></a>
                        <div class="clearfix"></div>
                        <?php if (has_permission("tasks", "", "create")) { ?>
                        <a href="#" class="pull-left btn btn-default mtop5 btn-xs" onclick="convert_service_request_to_task(<?php echo $reply["id"]; ?>,'reply'); return false;"><?php echo _l("convert_to_task"); ?>
                        </a>
                        <div class="clearfix"></div>
                        <?php
                        } ?>
                      </div>
                      <div class="col-md-9">
                        <div class="row">
                          <div class="col-md-12 text-right">
                            <?php if (!empty($reply["message"])) { ?>
                            <a href="#" onclick="print_service_request_message(<?php echo $reply["id"]; ?>, 'reply'); return false;" class="mright5"><i class="fa fa-print"></i></a>
                            <?php
                            } ?>
                            <a href="#" onclick="edit_service_request_message(<?php echo $reply["id"]; ?>,'reply'); return false;"><i class="fa fa-pencil-square-o"></i></a>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div data-reply-id="<?php echo $reply["id"]; ?>" class="tc-content">
                          <?php echo check_for_links($reply["message"]); ?>
                        </div>
                        <?php if (count($reply["attachments"]) > 0) {
                        echo "<hr />";
                        foreach ($reply["attachments"] as $attachment) {
                        $path = get_upload_path_by_type("service_request") . $service_request->service_requestid . "/" . $attachment["file_name"];
                        $is_image = is_image($path);
                        if ($is_image) {
                        echo '<div class="preview_image">';
                          }
                          ?>
                          <a href="<?php echo site_url("download/file/service_request/" . $attachment["id"]); ?>" class="display-block mbot5" <?php if ($is_image) { ?> data-lightbox="attachment-reply-<?php echo $reply["id"]; ?>" <?php
                            } ?>>
                            <i class="<?php echo get_mime_class($attachment["filetype"]); ?>"></i> <?php echo $attachment["file_name"]; ?>
                            <?php if ($is_image) { ?>
                            <img class="mtop5" src="<?php echo site_url("download/preview_image?path=" . protected_file_url_by_path($path) . "&type=" . $attachment["filetype"]); ?>">
                            <?php
                            } ?>
                          </a>
                          <?php
                          if ($is_image) {
                        echo "</div>";
                        }
                        if (is_admin() || (!is_admin() && get_option("allow_non_admin_staff_to_delete_service_request_attachments") == "1")) {
                        echo '<a href="' . admin_url("service_requests/delete_attachment/" . $attachment["id"]) . '" class="text-danger _delete">' . _l("delete") . "</a>";
                        }
                        echo "<hr />";
                        }
                        } ?>
                      </div>


                    </div>
                  </div>
                  <div class="panel-footer">
                    <span><?php echo _l("service_request_posted", _dt($reply["date"])); ?></span>
                  </div>


                </div>
                <?php
                } ?>
                 <!-- Start -->
                <?php foreach ($service_request_replies_mail as $reply) { ?>
                <div class="panel_s">
                  <div class="panel-body
                    echo 'client-reply';} ?>">
                    <div class="row">
                      <div class="col-md-3 border-right service_request-submitter-info">
                        <p>
                          <?php if ($reply["id"] != 0) { ?>
                          <?php echo $reply["sender_name"]; ?>
                          <?php
                          } ?>
                        </p>
                        <p class="text-muted">
                          <?php if ($reply["id"] != 0) {
                          echo $service_request->company;
                          } ?>
                        </p>
                        <hr />
                        <!-- <a href="<?php
                          /* echo admin_url(
                          'service_requests/delete_service_request_reply/' .
                          $service_request->service_requestid .
                          '/' .
                          $reply['id']
                          ); ?>" class="btn btn-danger pull-left _delete mright5 btn-xs"><?php echo _l(
                          'delete_service_request_reply'
                          );*/
                          ?>
                        </a> -->
                        <div class="clearfix"></div>
                        <?php if (has_permission("tasks", "", "create")) { ?>
                        <a href="#" class="pull-left btn btn-default mtop5 btn-xs" onclick="convert_service_request_to_task(<?php echo $reply["id"]; ?>,'reply'); return false;"><?php echo _l("convert_to_task"); ?>
                        </a>
                        <div class="clearfix"></div>
                        <?php
                        } ?>
                      </div>
                      <!-- col-9 -->
                      <div class="col-md-9">
                        <div class="row">
                          <div class="col-md-12 text-right">
                            <?php if (!empty($reply["body"])) { ?>
                            <a href="#" onclick="print_service_request_message(<?php echo $reply["id"]; ?>, 'reply'); return false;" class="mright5"><i class="fa fa-print"></i></a>
                            <?php
                            } ?>
                            <!-- <a href="#" onclick="edit_service_request_message(<?php
                              /* echo $reply[
                              'id'
                              ]; */
                            ?>,'reply'); return false;"><i class="fa fa-pencil-square-o"></i></a> -->
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div data-reply-id="<?php echo $reply["id"]; ?>" class="tc-content">
                          <?php echo check_for_links($reply["body"]); ?>
                        </div>
                        <?php
                        /*if (count($reply['has_attachment']) > 0) {
                        echo '<hr />';
                        foreach ($reply['has_attachment']as $attachment) {
                        
                        $path =get_upload_path_by_type('service_request') .$service_request->service_requestid .
                        '/' .$attachment['file_name'];
                        $is_image = is_image($path);
                        
                        if ($is_image) {
                        echo '<div class="preview_image">';
                          }
                          ?>
                          <a href="<?php echo site_url(
                            'download/file/service_request/' . $attachment['id']
                            ); ?>" class="display-block mbot5" <?php if ($is_image) { ?>
                            data-lightbox="attachment-reply-<?php echo $reply['id']; ?>" <?php } ?>>
                            <i class="<?php echo get_mime_class(
                            $attachment['filetype']
                            ); ?>"></i>
                            <?php echo $attachment['file_name']; ?>
                            <?php if ($is_image) { ?>
                            <img class="mtop5" src="<?php echo site_url(
                            'download/preview_image?path=' .
                            protected_file_url_by_path(
                            $path
                            ) .
                            '&type=' .
                            $attachment['filetype']
                            ); ?>">
                            <?php } ?>
                          </a>
                          <?php
                          if ($is_image) {
                        echo '</div>';
                        }
                        if (
                        is_admin() ||
                        (!is_admin() &&
                        get_option(
                        'allow_non_admin_staff_to_delete_service_request_attachments'
                        ) == '1')
                        ) {
                        echo '<a href="' .
                          admin_url(
                          'service_requests/delete_attachment/' .
                          $attachment['id']
                          ) .
                          '" class="text-danger _delete">' .
                          _l('delete') .
                        '</a>';
                        }
                        echo '<hr />';
                        
                        }
                        } */
                        ?>
                      </div>
                      <!-- col-9 End Here-->
                    </div>
                  </div>
                  <div class="panel-footer">
                    <span><?php echo _l("service_request_posted", _dt($reply["date_received"])); ?></span>
                  </div>
                </div>
                <?php
                } ?>
              </div>
              <div role="tabpanel" class="tab-pane" id="note">
                <hr class="no-mtop" />
                <div class="form-group">
                  <label for="note_description"><?php echo _l("service_request_single_note_heading"); ?></label>
                  <textarea class="form-control" name="note_description" rows="5"></textarea>
                </div>
                <a class="btn btn-info pull-right add_note_service_request"><?php echo _l("service_request_single_add_note"); ?></a>
              </div>
              <div role="tabpanel" class="tab-pane" id="tab_reminders">
                <a href="#" class="btn btn-info btn-xs" data-toggle="modal"
                  data-target=".reminder-modal-service_request-<?php echo $service_request->service_requestid; ?>"><i
                class="fa fa-bell-o"></i> <?php echo _l("SET SERVICE REQUEST REMINDER"); ?></a>
                <hr />
                <?php render_datatable([_l("reminder_description"), _l("reminder_date"), _l("reminder_staff"), _l("reminder_is_notified"), ], "reminders"); ?>
              </div>
              <div role="tabpanel" class="tab-pane" id="otherservice_requests">
                <hr class="no-mtop" />
                <div class="_filters _hidden_inputs hidden service_requests_filters">
                  <?php echo form_hidden("filters_service_request_id", $service_request->service_requestid); ?>
                  <?php echo form_hidden("filters_email", $service_request->email); ?>
                  <?php echo form_hidden("filters_userid", $service_request->userid); ?>
                </div>
                <?php echo AdminServiceRequestsTableStructure(); ?>
              </div>
              <div role="tabpanel" class="tab-pane" id="tasks">
                <hr class="no-mtop" />
                <?php init_relation_tasks_table(["data-new-rel-id" => $service_request->service_requestid, "data-new-rel-type" => "service_request", ]); ?>
              </div>
              <div role="tabpanel" class="tab-pane <?php if ($this->session->flashdata("active_tab_settings")) {
                echo "active";
                } ?>" id="settings">
                <hr class="no-mtop" />
                <div class="row">
                  <div class="col-md-6">
                    <?php echo render_input("subject", "service_request_settings_subject", $service_request->subject); ?>
                    <div class="form-group select-placeholder">
                      <label for="contactid" class="control-label"><?php echo _l("contact"); ?></label>
                      <select name="contactid" id="contactid" class="ajax-search"
                        data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l("dropdown_non_selected_tex"); ?>" <?php if (!empty($service_request->from_name) && !empty($service_request->service_request_email)) {
                        echo ' data-no-contact="true"';
                        } else {
                        echo ' data-service_request-emails="' . $service_request->service_request_emails . '"';
                        } ?>>
                        <?php
                        $rel_data = get_relation_data("contact", $service_request->contactid);
                        $rel_val = get_relation_values($rel_data, "contact");
                        echo '<option value="' . $rel_val["id"] . '" selected data-subtext="' . $rel_val["subtext"] . '">' . $rel_val["name"] . "</option>";
                        ?>
                      </select>
                      <?php echo form_hidden("userid", $service_request->userid); ?>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <?php echo render_input("name", "service_request_settings_to", $service_request->submitter, "text", ["readonly" => true]); ?>
                      </div>
                      <div class="col-md-6">
                        <?php if ($service_request->userid != 0) {
                        echo render_input("email", "service_request_settings_email", $service_request->email, "email", ["readonly" => true, ]);
                        } else {
                        echo render_input("email", "service_request_settings_email", $service_request->service_request_email, "email", ["readonly" => true]);
                        } ?>
                      </div>
                    </div>
                    <?php echo render_select("department", $departments, ["departmentid", "name"], "service_request_settings_departments", $service_request->department); ?>
                    <div class="form-group select-placeholder">
                      <label for="direction"><?php echo "Take approval"; ?></label>
                      <select class="selectpicker approval_select" data-none-selected-text="<?php echo _l("system_default_string"); ?>" data-width="100%" name="take_approval" id="direction" onchange="showDiv('hidden_div', this)">
                        <option value="1" name="take_approval" <?php echo $service_request->take_approval == 1 ? "selected" : ""; ?>>Yes</option>
                        <option value="0" name="take_approval" <?php echo $service_request->take_approval == 0 ? "selected" : ""; ?>>No</option>
                      </select>
                    </div>
                    <div class="form-group select-placeholder" id="hidden_div"
                      style="display: none">
                      <!-- <label for="" class="control-label">
                        
                      </label> -->
                      <!-- <select class="selectpicker" name="request_for" data-width="100%">
                        
                        
                        <?php
                        /*foreach($contacts as $contact) {?>
                        <option value="<?php echo $contact['id']?>">
                        <?php echo $contact['firstname'].' '.$contact['lastname'] ?></option>
                        <?php }*/
                        ?>
                        
                        
                        
                      </select> -->
                      <?php echo render_select("approval_assigned_to_staffid", $contacts, ["id", ["firstname", "lastname"]], "Assign service request to approval authority", $service_request->contactid); ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group mbot20">
                      <label for="tags" class="control-label"><i class="fa fa-tag"
                      aria-hidden="true"></i> <?php echo _l("tags"); ?></label>
                      <input type="text" class="tagsinput" id="tags" name="tags" value="<?php echo prep_tags_input(get_tags_in($service_request->service_requestid, "service_request")); ?>" data-role="tagsinput">
                    </div>
                    <div class="form-group select-placeholder">
                      <label for="assigned" class="control-label">
                        <?php echo _l("service_request_settings_assign_to"); ?>
                      </label>
                      <select name="assigned" data-live-search="true" id="assigned"
                        class="form-control selectpicker" data-none-selected-text="<?php echo _l("dropdown_non_selected_tex"); ?>">
                      
                        <?php foreach ($staff as $member) {
                        // service_request is assigned to member
                        // Member is set to inactive
                        // We should show the member in the dropdown too
                        // Otherwise, skip this member
                        if ($member["active"] == 0 && $service_request->assigned != $member["staffid"]) {
                        continue;
                        } ?>
                        <option value="<?php echo $member["staffid"]; ?>" <?php if ($service_request->assigned == $member["staffid"]) {
                          echo "selected";
                          } ?>>
                          <?php echo $member["firstname"] . " " . $member["lastname"]; ?>
                        </option>
                        <?php
                        } ?>
                      </select>
                    </div>
                    <div class="row">
                      <div class="col-md-<?php if (get_option("services") == 1) {
                        echo 6;
                        } else {
                        echo 12;
                        } ?>">
                        <?php
                        $priorities["callback_translate"] = "service_request_priority_translate";
                        echo render_select("priority", $priorities, ["priorityid", "name"], "service_request_settings_priority", $service_request->priority);
                        ?>
                      </div>
                      <?php if (get_option("services") == 1) { ?>
                      <div class="col-md-6">
                        <?php if (is_admin() || get_option("staff_members_create_inline_service_request_services") == "1") {
                        echo render_select_with_input_group("service", $services, ["serviceid", "name"], "service_request_settings_service", $service_request->service, '<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
                        } else {
                        echo render_select("service", $services, ["serviceid", "name"], "service_request_settings_service", $service_request->service);
                        } ?>
                      </div>
                      <?php
                      } ?>
                      <div class="col-md-6">
                        <?php echo render_select("request_for", $requests, ["requestid", "name"], "Request For", hooks()->apply_filters("new_service_request_request_for_selected", $service_request->request_for), ["required" => "true"]); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo render_select("delivery", $deliveries, ["deliveryid", "name"], "Delivery", hooks()->apply_filters("new_service_request_delivery_selected", $service_request->delivery), ["required" => "true"]); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo render_input("estimated_efforts", "Estimated Efforts", $service_request->estimated_efforts, "text", ["list" => "estimated_list"]); ?>
                        <datalist id="estimated_list">
                        <option value="15 Mins">
                          <option value="30 Mins">
                            <option value="60 Mins">
                              <option value="4 Hours">
                                </datalist>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group select-placeholder">
                                      <label for="billing_type"><?php echo _l('Billing Type'); ?></label>
                                      <select class="selectpicker" name="billing_type" id="billing_type"
                                          onchange="getBillingValue(this);" data-width="100%">
                                          <option value="contract"
                                              <?php echo ($service_request->billing_type == 'contract') ? 'selected' : '' ?>>
                                              Contract</option>
                                          <option value="additional_service"
                                              <?php echo ($service_request->billing_type == 'additional_service') ? 'selected' : '' ?>>
                                              Additional Service</option>
                                          <option value="lpo"
                                              <?php echo ($service_request->billing_type == 'lpo') ? 'selected' : '' ?>>
                                              LPO</option>
                                          <option value="free"
                                              <?php echo ($service_request->billing_type == 'free') ? 'selected' : '' ?>>
                                              Free</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="col-md-6 contractselect" style="display: none;">
                                  <div class="form-group">
                                      <label for="contract" class="control-label">
                                          <?php echo _l('Contract'); ?>
                                      </label>
                                      <?php if(isset($contracts)) { ?>
                                      <select class="form-control" name="contract">
                                          <?php  foreach($contracts as $contract) { ?>
                                          <option value="<?php echo $contract['id']; ?>"
                                              <?php echo ($contract['id'] == $service_request->contract) ? 'selected' : '' ?>>
                                              <?php echo $contract['subject']; ?></option>
                                          <?php }  ?>
                                      </select>
                                      <?php } else {?>
                                      <select name="contract" id="contract" class="form-control"></select>
                                      <?php }?>
                                  </div>
                              </div>

                              <div class="col-md-6 additional_charges_select" style="display: none;">
                                  <div class="form-group">
                                      <label for="additional_charge" class="control-label">
                                          <?php echo _l('Additional Charges'); ?>
                                      </label>
                                      <?php if(isset($addtional_charges_select)) { ?>
                                      <select class="form-control" name="additional_charge[]">
                                          <?php  foreach($addtional_charges_select as $charge) { ?>
                                          <option value="<?php echo $charge['id']; ?>">
                                              <?php echo $charge['chargetype']; ?></option>
                                          <?php }  ?>
                                      </select>
                                      <?php } else { ?>
                                      <select name="additional_charge[]" id="additional_charge" multiple
                                          class="form-control"></select>
                                      <?php } ?>
                                  </div>
                              </div>
                            </div>
                            <div class="form-group select-placeholder projects-wrapper<?php if ($service_request->userid == 0) {
                              echo " hide";
                              } ?>">
                              <label for="project_id"><?php echo _l("project"); ?></label>
                              <div id="project_ajax_search_wrapper">
                                <select name="project_id" id="project_id" class="projects ajax-search"
                                  data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l("dropdown_non_selected_tex"); ?>">
                                  <?php if ($service_request->project_id != 0) { ?>
                                  <option value="<?php echo $service_request->project_id; ?>"><?php echo get_project_name_by_id($service_request->project_id); ?></option>
                                  <?php
                                  } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <?php echo render_custom_fields("service_requests", $service_request->service_requestid); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 text-center">
                            <hr />
                            <a href="#" class="btn btn-info save_changes_settings_single_service_request">
                              <?php echo _l("submit"); ?>
                            </a>
                          </div>
                        </div>
                        <hr class="hr-panel-heading" />
                        <?php if (count($teams) > 0) { ?>
                        <div class="panel_s mtop20">
                          <div class="panel-body">
                            <div class="_buttons">
                              <span
                              class="btn btn-info pull-left display-block"><?php echo _l('Designated Team'); ?></span>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr-panel-heading" />
                            <?php if (count($teams) > 0) { ?>
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Role'); ?></th>
                                <th><?php echo _l('Staff Name'); ?></th>
                                <th><?php echo _l('Email'); ?></th>
                                <th><?php echo _l('Mobile Number'); ?></th>
                              </thead>
                              <tbody>
                                <?php foreach ($teams as $key => $team) { ?>
                                <tr>
                                  <td><?php echo $key + 1; ?></td>
                                  <td>
                                    <?php echo $this->db->where('roleid', $team['role'])->get('tblteamroles')->row()->name; ?>
                                  </td>
                                  <td>
                                    <?php echo $team['firstname'] . ' ' . $team['lastname']; ?>
                                  </td>
                                  <td>
                                    <?php echo $team['email']; ?>
                                  </td>
                                  <td>
                                    <?php echo $team['phonenumber']; ?>
                                  </td>
                                </tr>
                                <?php
                                } ?>
                              </tbody>
                            </table>
                            <?php
                            } else { ?>
                            <p class="no-margin"><?php echo _l('No Team found'); ?></p>
                            <?php
                            } ?>
                          </div>
                        </div>
                        <?php
                        } ?>
                        <?php if (count($notes) > 0) { ?>
                        <div class="panel_s mtop20">
                          <div class="panel-body">
                            <div class="_buttons">
                              <span
                              class="btn btn-info pull-left display-block"><?php echo _l('Notes'); ?></span>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr-panel-heading" />
                            <?php if (count($notes) > 0) { ?>
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Description'); ?></th>
                                <th><?php echo _l('Created At'); ?></th>
                              </thead>
                              <tbody>
                                <?php foreach ($notes as $key => $note) { ?>
                                <tr>
                                  <td><?php echo $key + 1; ?></td>
                                  <td><?php echo $note['description'] ?></td>
                                  <td><?php echo $note['dateadded'] ?></td>
                                </tr>
                                <?php
                                } ?>
                              </tbody>
                            </table>
                            <?php
                            } else { ?>
                            <p class="no-margin"><?php echo _l('No note found'); ?></p>
                            <?php
                            } ?>
                          </div>
                        </div>
                        <?php
                        } ?>
                      </div>
                    </div>
                  </div>
                </div>
                
                

                <!--                    End -->
               

              </div>
            </div>
            <div class="btn-bottom-pusher"></div>
            <?php if (count($service_request_replies) > 1) { ?>
            <a href="#top" id="toplink">↑</a>
            <a href="#bot" id="botlink">↓</a>
            <?php
            } ?>
          </div>
        </div>
        <!-- The reminders modal -->
        <?php $this->load->view("admin/includes/modals/reminder", ["id" => $service_request->service_requestid, "name" => "service_request", "members" => $staff, "reminder_title" => _l("service_request_set_reminder_title"), ]); ?>
        <!-- Edit service_request Messsage Modal -->
        <div class="modal fade" id="service_request-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <?php echo form_open(admin_url("service_requests/edit_message")); ?>
            <div class="modal-content">
              <div id="edit-service_request-message-additional"></div>
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo _l("service_request_message_edit"); ?></h4>
              </div>
              <div class="modal-body">
                <?php echo render_textarea("data", "", "", [], [], "", "tinymce-service_request-edit"); ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l("close"); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l("submit"); ?></button>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
        <div class="modal fade" id="closure-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <?php
            $attributes = ["id" => "myform"];
            echo form_open(admin_url("service_requests/before_closing_request/"));
            ?>
            <div class="modal-content">
              <div id="edit-service_request-message-additional"></div>
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Well Done - <?php echo $service_request->staff_firstname.' '.$service_request->staff_lastname?></h4>
            </div>
              <div class="modal-body">
                <!-- <div class="form-group">
                  <div class="checkbox checkbox-success">
                    <input type="hidden" name="service_requestid"
                    value="<?php echo $service_request->service_requestid; ?>">
                    <input type="checkbox" name="service_request_category" value="1" id="service_request_category">
                    <label for="service_request_category">Service requests Category is set</label>
                  </div>
                </div> -->
                <div class="form-group">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" name="worklog" value="1" id="worklog" <?php
                    /*echo ($service_request->worklog == 1) ? 'checked':'' */
                    ?>>
                    <label for="worklog">Work Log created</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" name="user_acceptance" value="1" id="user_acceptance">
                    <label for="user_acceptance">Customer has accpeted the resolution</label>
                    <input type="hidden" name="service_requestid" 
                        value="<?php if(isset($service_request->service_requestid)){echo $service_request->service_requestid;}?>">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l("close"); ?></button>
                <button disabled="" type="submit" id="btncheck" class="btn btn-info"><?php echo _l("submit"); ?></button>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>

        <div class="modal fade" id="close-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
              <?php $attributes = array('id' => '');
            echo form_open(admin_url('service_requests/before_close_status/'));
            ?>
              <div class="modal-content">
                  <div id="edit-service_request-message-additional"></div>
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                              aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                          <div class="checkbox checkbox-success">
                              <input type="checkbox" name="attach_service_report" value="1" id="attach_service_report">
                              <label for="user_acceptance">Attach Service Report</label>
                              <input type="hidden" name="service_requestid" 
                              value="<?php if(isset($service_request->service_requestid)){echo $service_request->service_requestid;}?>">
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l(
                  'close'
                  ); ?></button>
                      <button type="submit" class="btn btn-info"><?php echo _l(
                  'submit'
                  ); ?></button>
                  </div>
              </div>
              <?php echo form_close(); ?>
          </div>
      </div>

          <script>
          var _service_request_message;
          </script>
          <?php $this->load->view("admin/service_requests/services/service"); ?>
          <?php init_tail(); ?>
          <?php hooks()->do_action("service_request_admin_single_page_loaded", $service_request); ?>
        <script>
          $(document).ready(function() {
              if ($("#billing_type option:selected").val() == 'contract') {
                  $('.contractselect').css('display', 'block')
              }
              if ($("#billing_type option:selected").val() == 'additional_service') {
                  $('.additional_charges_select').css('display', 'block');
              }

          });
            $(function() {
                  $('#single-service_request-form').appFormValidator();
                  init_ajax_search('contact', '#contactid.ajax-search', {
                  service_requests_contacts: true
                  });
                  init_ajax_search('project', 'select[name="project_id"]', {
                  customer_id: function() {
                  return $('input[name="userid"]').val();
                  }
                  });
                  $('body').on('shown.bs.modal', '#_task_modal', function() {
                  if (typeof(_service_request_message) != 'undefined') {
                  // Init the task description editor
                  if (!is_mobile()) {
                  $(this).find('#description').click();
                  } else {
                  $(this).find('#description').focus();
                  }
                  setTimeout(function() {
                  tinymce.get('description').execCommand('mceInsertContent', false,
                  _service_request_message);
                  $('#_task_modal input[name="name"]').val($('#service_request_subject').text()
                  .trim());
                  }, 100);
                  }
                  });
            });
            var service_request_message_editor;
            var edit_service_request_message_additional = $('#edit-service_request-message-additional');
            function edit_service_request_message(id, type) {
                edit_service_request_message_additional.empty();
                // type is either service_request or reply
                _service_request_message = $('[data-' + type + '-id="' + id + '"]').html();
                init_service_request_edit_editor();
                tinyMCE.activeEditor.setContent(_service_request_message);
                $('#service_request-message').modal('show');
                edit_service_request_message_additional.append(hidden_input('type', type));
                edit_service_request_message_additional.append(hidden_input('id', id));
                edit_service_request_message_additional.append(hidden_input('main_service_request', $(
                'input[name="service_requestid"]').val()));
            }
            function init_service_request_edit_editor() {
                if (typeof(service_request_message_editor) !== 'undefined') {
                return true;
                }
                service_request_message_editor = init_editor('.tinymce-service_request-edit');
            }
            <?php if (has_permission("tasks", "", "create")) { ?>
            function convert_service_request_to_task(id, type) {
                if (type == 'service_request') {
                _service_request_message = $('[data-service_request-id="' + id + '"]').html();
                } else {
                _service_request_message = $('[data-reply-id="' + id + '"]').html();
                }
                var new_task_url = admin_url +
                'tasks/task?rel_id=<?php echo $service_request->service_requestid; ?>&rel_type=service_request&service_request_to_task=true';
                new_task(new_task_url);
            }
            <?php
            } ?>
            </script>
            <script type="text/javascript">
            $(function() {
                $("#worklog, #user_acceptance").change(function() {
                if ($("#worklog").is(':checked') && $(
                "#user_acceptance").is(':checked')) {
                $('#btncheck').attr('disabled', false);
                } else {
                $('#btncheck').attr('disabled', true);
                }
                });
            });
            $('#status_top').on('change', function() {
                if (this.value == 3) {
                $('#closure-message').modal({
                show: 'false'
                });
                $("#worklog, #user_acceptance").change(function() {
                if ($("#worklog").is(':checked') && $(
                "#user_acceptance").is(':checked')) {
                $('#btncheck').attr('disabled', false);
                } else {
                $('#btncheck').attr('disabled', true);
                }
                });
                }
                 else if(this.value == 5)
                {
                     $('#close-message').modal({
                        show: 'false',
                        backdrop: 'static'
                    });
                }
                

            });
            function set_contract(data) {
                $("#contract").empty();
                $.each(data, function(index, json) {
                $("#contract").append(`<option value='${data[index].id}'>${data[index].subject}</option>`);
                // console.log($("#contract").next().next().find('.dropdown-menu'));
                });
            }
            function set_cc_contacts(data) {
                $("#cc_contacts").empty();
                if(data.length > 0){
                    $.each(data, function(index, json) {
                        selected = false;

                        var text = `${data[index].email}`;
                        var newOption = new Option(text, `${data[index].id}`, selected, selected);

                        $('#cc_contacts').append(newOption).trigger('change');
                        $("#cc_contacts").selectpicker("refresh");
                    });
                }
            }
            function set_cc_staffs(data) {
                $("#cc_staffs").empty();
                if(data.length > 0){
                    $.each(data, function(index, json) {
                        selected = false;

                        var text = `${data[index].email}`;
                        var newOption = new Option(text, `${data[index].staffid}`, selected, selected);

                        $('#cc_staffs').append(newOption).trigger('change');
                        $("#cc_staffs").selectpicker("refresh");
                    });
                }
            }
            function showDivOnLoad(divId, element) {
                document.getElementById(divId).style.display = element == 1 ? 'block' : 'none';
            }
            function showDiv(divId, element) {
                document.getElementById(divId).style.display = element.value == 1 ? 'block' : 'none';
            }

            function set_additional_charge(data) {
                $("#additional_charge").empty();
                $.each(data, function(index, json) {
                    $("#additional_charge").append(
                        `<option value='${data[index].id}'>${data[index].chargetype}</option>`);
                    // console.log($("#contract").next().next().find('.dropdown-menu'));
                });
            }
        </script>

        <script>
        function getBillingValue(sel) {
            $('.contractselect').css("display", 'none');
            $('.additional_charges_select').css("display", 'none');
            if ((sel.value) == 'contract') {
                $('.contractselect').css("display", 'block');
            } else if ((sel.value) == 'additional_service') {
                $('.additional_charges_select').css("display", 'block');
            }
        }
        </script>
      </body>
    </html>