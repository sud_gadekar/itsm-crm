<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'new_service_request_form')); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<?php /*if($flag == '') { ?>
									<a href="#" id="service_request_no_contact">
										<span class="label label-default">
										<i class="fa fa-envelope"></i> <?php echo _l('service_request_create_no_contact'); ?>
										</span>
									</a>
									<a href="#" class="hide" id="service_request_to_contact">
										<span class="label label-default">
										<i class="fa fa-user-o"></i> <?php echo _l('service_request_create_to_contact'); ?>
										</span>
									</a>
									<div class="mbot15"></div>
								<?php } */ ?>
						<?php echo render_input('subject','service_request_settings_subject',$subject,'text',array('required'=>'true')); ?>
						<?php if($flag == 1 || $flag == '') { ?>
						<div class="form-group select-placeholder" id="service_request_contact_w">
							<label for="contactid"><?php echo _l('contact'); ?></label>
							<select  name="contactid" required="true" id="contactid" class="ajax-search" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
									<option value="<?php echo $contactid; ?>" selected ><?php echo $sender_name; ?></option>
								
								<option value=""></option>
							</select>
						</div>
					<?php } ?>
						<div class="row">
							<div class="col-md-6">
							<?php if($mailid) {?>
                                    <?php echo render_input('name','ticket_settings_to',$sender_name,'text',array('readonly'=>true)); ?>
                            <?php } else {?>
                                    <?php echo render_input('name','ticket_settings_to',$sender_name,'text',array('readonly'=>true)); ?>
                            <?php }?>
							</div>
							<div class="col-md-6">


								<?php /*echo render_input('email','service_request_settings_email',$from_email,'email',array('disabled'=>true)); */?>
								
								<?php if($mailid) {?>
									<?php echo render_input('email','service_request_settings_email',$from_email,'email',array('readonly'=>true)); ?>
								<?php } else {?>
									<?php echo render_input('email','service_request_settings_email',$from_email,'email',array('readonly'=>true)); ?>
								<?php }?>
								
								<?php/* echo form_hidden('userid',$mailid); */?>

								<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>">

								<input type="hidden" name="mailid" id="mailid" value="<?php echo $mailid; ?>">

								<input type="hidden" name="cntid" id="cntid" value="<?php echo $cntid; ?>">
							</div>
							<?php if(isset($company)){?>
	                            <div class="col-md-6">
	                                <?php echo render_input('company','Company',$company,'text',array('disabled'=>true)); ?>
	                            </div>
	                        <?php }?>
						</div>
					<?php  ?>
						<div class="row">
							<div class="col-md-6">
								<?php echo render_select('department',$departments,array('departmentid','name'),'service_request_settings_departments',(count($departments) == 1) ? $departments[0]['departmentid'] : '',array('required'=>'true')); ?>
							</div>
							<div class="col-md-6">
							<?php //echo render_input('cc','CC'); ?>
                            <div class="form-group">
                                <label for="cc_contacts" class="control-label">
                                    <?php echo _l('CC (Contacts)'); ?>
                                </label>
                                <select class="form-control selectpicker" name="cc_contacts[]" id="cc_contacts" data-width="100%" data-live-search="true" multiple></select>
                            </div>
							</div>
							<div class="col-md-12">
                            <div class="form-group">
                                <label for="cc_staffs" class="control-label">
                                    <?php echo _l('CC (Staffs)'); ?>
                                </label>
                                <select class="form-control selectpicker" name="cc_staffs[]" id="cc_staffs" data-width="100%" data-live-search="true" multiple></select>
                            </div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
							<input type="text" class="tagsinput" id="tags" name="tags" data-role="tagsinput">
						</div>

						<div class="form-group select-placeholder">
							<label for="assigned" class="control-label">
								<?php echo _l('service_request_settings_assign_to'); ?>
							</label>
							<select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
								<option value=""><?php echo _l('service_request_settings_none_assigned'); ?></option>
								<?php foreach($staff as $member){ ?>
									<option value="<?php echo $member['staffid']; ?>"
										<?php if(isset($service_request->assigned)){
											echo ($service_request->assigned == $member['staffid']) ? 'selected' : '';	
										}?>>
										<?php echo $member['firstname'] . ' ' . $member['lastname'] ; ?>
									</option>
								<?php } ?>
							</select>
						</div>
						<div class="row">
							<div class="col-md-<?php if(get_option('services') == 1){ echo 6; }else{echo 12;} ?>">
								<?php $priorities['callback_translate'] = 'service_request_priority_translate';
								echo render_select('priority', $priorities, array('priorityid','name'), 'service_request_settings_priority', hooks()->apply_filters('new_service_request_priority_selected', 2), array('required'=>'true')); ?>
							</div>
							<?php if(get_option('services') == 1){ ?>
								<div class="col-md-6">
									<?php if(is_admin() || get_option('staff_members_create_inline_service_request_services') == '1'){
										echo render_select_with_input_group('service',$services,array('serviceid','name'),'service_request_settings_service','','<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
									} else {
										echo render_select('service',$services,array('serviceid','name'),'service_request_settings_service');
									}
									?>
								</div>
							<?php } ?>

							<div class="col-md-6">
								<?php echo render_select('request_for', $requests, array('requestid','name'), 'Request For', hooks()->apply_filters('new_service_request_request_for_selected', 1), array('required'=>'true')); ?>

								<!--<div class="form-group select-placeholder">
			                        <label for="direction"><?php echo _l('Request For'); ?></label>
			                        <select class="selectpicker" name="request_for" data-width="100%">
			                           <option value="Add">Add</option>
			                           <option value="Remove">Remove</option>
			                           <option value="Change">Change</option>
			                           <option value="Upgrade">Upgrade</option>
			                        </select>
			                     </div>-->
							</div>

							<div class="col-md-6">
								<?php echo render_select('delivery', $deliveries, array('deliveryid','name'), 'Delivery', hooks()->apply_filters('new_service_request_delivery_selected', 1), array('required'=>'true')); ?>

								<!--<div class="form-group select-placeholder">
			                        <label for="direction"><?php echo _l('Delivery'); ?></label>
			                        <select class="selectpicker" name="delivery"  data-width="100%">
			                           <option value="Remove">Remove</option>
			                           <option value="Onsite">Onsite</option>
			                           <option value="Remote/Onsite"> Remote/Onsite</option>
			                        </select>
			                     </div>-->
							</div>

							<div class="col-md-6">
								<?php echo render_input('estimated_efforts','Estimated Efforts (Time)','','text',array('list'=>'estimated_list')); ?>
								<datalist id="estimated_list">
								    <option value="15 Mins">
								    <option value="30 Mins">
								    <option value="60 Mins">
								    <option value="4 Hours">
								</datalist>
							</div>

							
							<div class="col-md-6">
                                        <div class="form-group select-placeholder">
                                            <label for="direction"><?php echo _l('Billing Type'); ?></label>
                                            <select class="selectpicker" name="billing_type" id="billing_type"
                                                data-width="100%" onchange="getBillingValue(this);">>
                                                <option selected disabled>Select</option>
                                                <option value="contract">Contract</option>
                                                <option value="additional_service">Additional Service</option>
                                                <option value="lpo">LPO</option>
                                                <option value="free">Free</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-6 contractselect" style="display: none;">
                                        <div class="form-group">
                                            <label for="contract" class="control-label">
                                                <?php echo _l('Contract'); ?>
                                            </label>
                                            <select name="contract" id="contract" class="form-control"></select>
                                        </div>
                                    </div>

                                    <div class="col-md-6 additional_charges_select" style="display: none;">
                                        <div class="form-group">
                                            <label for="additional_charge" class="control-label">
                                                <?php echo _l('Additional Charges'); ?>
                                            </label>

                                            <!-- <select class="selectpicker"  name="additional_charge[]" multiple>
                                                <?php /*foreach($charges as $charge) { ?>
                                                <option value="<?php echo $charge['id']; ?>">
                                                    <?php echo $charge['chargetype']; ?></option>
                                                <?php } */ ?>
                                            </select> -->
											<select name="additional_charge[]" id="additional_charge" class="form-control"></select>
                                        </div>
                                    </div>
						</div>

						<div class="form-group projects-wrapper hide">
							<label for="project_id"><?php echo _l('project'); ?></label>
							<div id="project_ajax_search_wrapper">
								<select name="project_id" id="project_id" class="projects ajax-search" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"<?php if(isset($project_id)){ ?> data-auto-project="true" data-project-userid="<?php echo $userid; ?>"<?php } ?>>
									<?php if(isset($project_id)){ ?>
										<option value="<?php echo $project_id; ?>" selected><?php echo '#'.$project_id. ' - ' . get_project_name_by_id($project_id); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<?php echo render_custom_fields('service_requests'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-heading">
						<?php echo _l('service_request_add_body'); ?>
					</div>
					<div class="panel-body">
						<div class="btn-bottom-toolbar text-right">
							<button type="submit" data-form="#new_service_request_form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info"><?php echo _l('open_service_request'); ?></button>
						</div>
						<div class="row">
							<div class="col-md-12 mbot20 before-service_request-message">
								<div class="row">
									<div class="col-md-6">
										<select id="insert_predefined_reply" data-width="100%" data-live-search="true" class="selectpicker" data-title="<?php echo _l('service_request_single_insert_predefined_reply'); ?>">
											<?php foreach($predefined_replies as $predefined_reply){ ?>
												<option value="<?php echo $predefined_reply['id']; ?>"><?php echo $predefined_reply['name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<?php if(get_option('use_knowledge_base') == 1){ ?>
										<div class="visible-xs">
											<div class="mtop15"></div>
										</div>
										<div class="col-md-6">
											<?php $groups = get_all_knowledge_base_articles_grouped(); ?>
											<select id="insert_knowledge_base_link" data-width="100%" class="selectpicker" data-live-search="true" onchange="insert_service_request_knowledgebase_link(this);" data-title="<?php echo _l('service_request_single_insert_knowledge_base_link'); ?>">
												<option value=""></option>
												<?php foreach($groups as $group){ ?>
													<?php if(count($group['articles']) > 0){ ?>
														<optgroup label="<?php echo $group['name']; ?>">
															<?php foreach($group['articles'] as $article) { ?>
																<option value="<?php echo $article['articleid']; ?>">
																	<?php echo $article['subject']; ?>
																</option>
															<?php } ?>
														</optgroup>
													<?php } ?>
												<?php } ?>
											</select>
										</div>
									<?php } ?>
								</div>


							</div>
						</div>
						<div class="clearfix"></div>
						<?php echo render_textarea('message','',$body,array(),array(),'','tinymce'); ?>
					</div>
					<div class="panel-footer attachments_area">
						<div class="row attachments">
							<div class="attachment">
								<div class="col-md-4 col-md-offset-4 mbot15">
									<div class="form-group">
										<label for="attachment" class="control-label"><?php echo _l('service_request_add_attachments'); ?></label>
										<div class="input-group">
											<input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('service_request_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_service_request_form_accepted_mimes(); ?>">
											<span class="input-group-btn">
												<button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_service_request_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
</div>
<?php $this->load->view('admin/service_requests/services/service'); ?>
<?php init_tail(); ?>
<?php hooks()->do_action('new_service_request_admin_page_loaded'); ?>
<script>
	var contracts = '';
	$(function(){

		init_ajax_search('contact','#contactid.ajax-search',{
			service_requests_contacts:true,
			contact_userid:function(){
					// when service_request is directly linked to project only search project client id contacts
					var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
					if(uid){
						return uid;
					} else {
						return '';
					}
				}
			});

		validate_new_service_request_form();

		<?php if(isset($project_id) || isset($contact)){ ?>
			$('body.service_request select[name="contactid"]').change();
		<?php } ?>

		<?php if(isset($project_id)){ ?>
			$('body').on('selected.cleared.ajax.bootstrap.select','select[data-auto-project="true"]',function(e){
				$('input[name="userid"]').val('');
				$(this).parents('.projects-wrapper').addClass('hide');
				$(this).prop('disabled',false);
				$(this).removeAttr('data-auto-project');
				$('body.service_request select[name="contactid"]').change();
			});
		<?php } ?>
	});
	function getBillingValue(sel) {
        $('.contractselect').css("display", 'none');
        $('.additional_charges_select').css("display", 'none');
        if ((sel.value) == 'contract') {
            $('.contractselect').css("display", 'block');
        } else if ((sel.value) == 'additional_service') {
            $('.additional_charges_select').css("display", 'block');
        }
    }

	 function set_contract(data){
    	$("#contract").empty();
    	
    	$.each(data,function(index,json){            
				$("#contract").append(`<option value='${data[index].id}'>${data[index].subject}</option>`);		
				  // console.log($("#contract").next().next().find('.dropdown-menu'));
				 });
    }

    function set_cc_contacts(data) {
        $("#cc_contacts").empty();
        if(data.length > 0){
            $.each(data, function(index, json) {
                selected = false;

                var text = `${data[index].email}`;
                var newOption = new Option(text, `${data[index].id}`, selected, selected);

                $('#cc_contacts').append(newOption).trigger('change');
                $("#cc_contacts").selectpicker("refresh");
            });
        }
    }

    function set_cc_staffs(data) {
        $("#cc_staffs").empty();
        if(data.length > 0){
            $.each(data, function(index, json) {
                selected = false;

                var text = `${data[index].email}`;
                var newOption = new Option(text, `${data[index].staffid}`, selected, selected);

                $('#cc_staffs').append(newOption).trigger('change');
                $("#cc_staffs").selectpicker("refresh");
            });
        }
    }

	function set_additional_charge(data) {
		// print_r(data);die();
        $("#additional_charge").empty();
        $.each(data, function(index, json) {
            $("#additional_charge").append(`<option value='${data[index].id}'>${data[index].chargetype}</option>`);
            // console.log($("#contract").next().next().find('.dropdown-menu'));
        });
    }

</script>
</body>
</html>
