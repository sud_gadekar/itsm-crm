<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="_buttons">
                            <a href="<?php echo admin_url('service_requests/add'); ?>"
                                class="btn btn-info pull-left display-block mright5">
                                <?php echo _l('new_service_request'); ?>
                            </a>
                            <a href="#" class="btn btn-default btn-with-tooltip" data-toggle="tooltip"
                                data-placement="bottom"
                                data-title="<?php echo _l('service_requests_chart_weekly_opening_stats'); ?>"
                                onclick="slideToggle('.weekly-service_request-opening',init_service_requests_weekly_chart); return false;"><i
                                    class="fa fa-bar-chart"></i></a>
                            <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                                data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-filter" aria-hidden="true"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right width300">
                                    <li>
                                        <a href="#" data-cview="all"
                                            onclick="dt_custom_view('','.service_requests-table',''); return false;">
                                            <?php echo _l('task_list_all'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" data-cview="my_service_requests"
                                            onclick="dt_custom_view('my_service_requests','.service_requests-table','my_service_requests'); return false;"><?php echo _l('my_service_requests_assigned'); ?></a>
                                    </li>
                                    <li class="divider"></li>
                                    <?php foreach($statuses as $status){ ?>
                                    <li
                                        class="<?php if($status['service_requeststatusid'] == $chosen_service_request_status || $chosen_service_request_status == '' && in_array($status['service_requeststatusid'], $default_service_requests_list_statuses)){echo 'active';} ?>">
                                        <a href="#"
                                            data-cview="service_request_status_<?php echo $status['service_requeststatusid']; ?>"
                                            onclick="dt_custom_view('service_request_status_<?php echo $status['service_requeststatusid']; ?>','.service_requests-table','service_request_status_<?php echo $status['service_requeststatusid']; ?>'); return false;">
                                            <?php echo service_request_status_translate($status['service_requeststatusid']); ?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <?php if(count($service_request_assignees) > 0 && is_admin()){ ?>
                                    <div class="clearfix"></div>
                                    <li class="divider"></li>
                                    <li class="dropdown-submenu pull-left">
                                        <a href="#" tabindex="-1"><?php echo _l('filter_by_assigned'); ?></a>
                                        <ul class="dropdown-menu dropdown-menu-left">
                                            <?php foreach($service_request_assignees as $as){ ?>
                                            <li>
                                                <a href="#"
                                                    data-cview="service_request_assignee_<?php echo $as['assigned']; ?>"
                                                    onclick="dt_custom_view(<?php echo $as['assigned']; ?>,'.service_requests-table','service_request_assignee_<?php echo $as['assigned']; ?>'); return false;"><?php echo get_staff_full_name($as['assigned']); ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php } ?>

                                    <?php if(count($clients) > 0 && is_admin()){ ?>
                                    <div class="clearfix"></div>
                                    <li class="divider"></li>
                                    <li class="dropdown-submenu pull-left">
                                        <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                        <ul class="dropdown-menu dropdown-menu-left">
                                            <?php foreach($clients as $client){ ?>
                                            <li>
                                                <a href="#" data-cview="service_request_client_<?php echo $client['userid']; ?>"
                                                    onclick="dt_custom_view(<?php echo $client['userid']; ?>,'.service_requests-table','service_request_client_<?php echo $client['userid']; ?>'); return false;"><?php echo $client['company']; ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </div>
                        <div class="panel_s weekly-service_request-opening no-shadow" style="display:none;">
                            <hr class="hr-panel-heading" />
                            <div class="clearfix"></div>
                            <div class="panel-heading-bg">
                                <?php echo _l('home_weekend_service_request_opening_statistics'); ?>
                            </div>
                            <div class="panel-body">
                                <div class="relative" style="max-height:350px;">
                                    <canvas class="chart" id="weekly-service_request-openings-chart"
                                        height="350"></canvas>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-panel-heading" />
                        <?php hooks()->do_action('before_render_service_requests_list_table'); ?>
                        <?php $this->load->view('admin/service_requests/summary'); ?>
                        <a href="#" data-toggle="modal" data-target="#service_requests_bulk_actions"
                            class="bulk-actions-btn table-btn hide"
                            data-table=".table-service_requests"><?php echo _l('bulk_actions'); ?></a>
                        <div class="clearfix"></div>
                        <?php echo AdminServiceRequestsTableStructure('', true); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bulk_actions" id="service_requests_bulk_actions" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo _l('bulk_actions'); ?></h4>
            </div>
            <div class="modal-body">
                <?php if(is_admin()){ ?>
                <div class="checkbox checkbox-danger">
                    <input type="checkbox" name="mass_delete" id="mass_delete">
                    <label for="mass_delete"><?php echo _l('mass_delete'); ?></label>
                </div>
                <hr class="mass_delete_separator" />
                <?php } ?>
                <div id="bulk_change">
                    <?php echo render_select('move_to_status_service_requests_bulk',$statuses,array('service_requeststatusid','name'),'service_request_single_change_status'); ?>
                    <?php echo render_select('move_to_department_service_requests_bulk',$departments,array('departmentid','name'),'department'); ?>
                    <?php echo render_select('move_to_priority_service_requests_bulk',$priorities,array('priorityid','name'),'service_request_priority'); ?>
                    <div class="form-group">
                        <?php echo '<p><b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b></p>'; ?>
                        <input type="text" class="tagsinput" id="tags_bulk" name="tags_bulk" value=""
                            data-role="tagsinput">
                    </div>
                    <?php if(get_option('services') == 1){ ?>
                    <?php echo render_select('move_to_service_service_requests_bulk',$services,array('serviceid','name'),'service'); ?>
                    <?php } ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <a href="#" class="btn btn-info"
                    onclick="service_requests_bulk_action(this); return false;"><?php echo _l('confirm'); ?></a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
var chart;
var chart_data = <?php echo $weekly_service_requests_opening_statistics; ?>;

function init_service_requests_weekly_chart() {
    if (typeof(chart) !== 'undefined') {
        chart.destroy();
    }
    // Weekly service_request openings statistics
    chart = new Chart($('#weekly-service_request-openings-chart'), {
        type: 'line',
        data: chart_data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    });
}
</script>

</body>

</html>