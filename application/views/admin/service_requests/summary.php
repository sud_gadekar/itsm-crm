<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
 <?php
 $statuses = $this->service_requests_model->get_service_request_status();
 ?>
 <div class="_filters _hidden_inputs hidden service_requests_filters">
  <?php
  echo form_hidden('my_service_requests');
  if(is_admin()){
    $service_request_assignees = $this->service_requests_model->get_service_requests_assignes_disctinct();
    foreach($service_request_assignees as $assignee){
      echo form_hidden('service_request_assignee_'.$assignee['assigned']);
    }
  }

  if(is_admin()){
   $clients = $this->db->select('tblclients.userid, tblclients.company')->get('tblclients')->result_array();
    foreach($clients as $client){
      echo form_hidden('service_request_client_'.$client['userid']);
    }
  }

  foreach($statuses as $status){
    $val = '';
    if($chosen_service_request_status != ''){
      if($chosen_service_request_status == $status['service_requeststatusid']){
        $val = $chosen_service_request_status;
      }
    } else {
      if(in_array($status['service_requeststatusid'], $default_service_requests_list_statuses)){
        $val = 1;
      }
    }
    echo form_hidden('service_request_status_'.$status['service_requeststatusid'],$val);
  } ?>
</div>
<div class="col-md-12">
  <h4 class="no-margin"><?php echo _l('service_requests_summary'); ?></h4>
  </div>
  <?php
  $where = '';
  if (!is_admin()) {
    if (get_option('staff_access_only_assigned_departments') == 1) {
      $departments_ids = array();
      if (count($staff_deparments_ids) == 0) {
        $departments = $this->departments_model->get();
        foreach($departments as $department){
          array_push($departments_ids,$department['departmentid']);
        }
      } else {
       $departments_ids = $staff_deparments_ids;
     }
     if(count($departments_ids) > 0){
      $where = 'AND department IN (SELECT departmentid FROM '.db_prefix().'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="'.get_staff_user_id().'")';
    }
  }
}
foreach($statuses as $status){
  $_where = '';
  if($where == ''){
    $_where = 'status='.$status['service_requeststatusid'];
  } else{
    $_where = 'status='.$status['service_requeststatusid'] . ' '.$where;
  }
  if(isset($project_id)){
    $_where = $_where . ' AND project_id='.$project_id;
  }
  ?>
  <div class="col-md-2 col-xs-6 mbot15 border-right">
    <a href="#" data-cview="service_request_status_<?php echo $status['service_requeststatusid']; ?>" onclick="dt_custom_view('service_request_status_<?php echo $status['service_requeststatusid']; ?>','.service_requests-table','service_request_status_<?php echo $status['service_requeststatusid']; ?>',true); return false;">
      <h3 class="bold"><?php echo total_rows(db_prefix().'service_requests',$_where); ?></h3>
      <span style="color:<?php echo $status['statuscolor']; ?>">
        <?php echo service_request_status_translate($status['service_requeststatusid']); ?>
      </span>
    </a>
  </div>
  <?php } ?>
</div>
 <hr class="hr-panel-heading" />
