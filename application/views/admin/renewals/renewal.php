<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'renewal_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Renewal </h4>
                  <div class="row">
                    <div class="additional"></div>
                     <div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="clientid" class="control-label">
                                <?php echo _l('Customer'); ?>
                            </label>
                            <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select Customer</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>" 
                                    <?php if(isset($renewals_details->clientid)){
                                        echo ($renewals_details->clientid == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="id" id="id" 
                            value="<?php if(isset($renewals_details->id)){echo $renewals_details->id;}?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Renewal name</label>
                            <input type="text" name="name" id="name" 
                            value="<?php  if(isset($renewals_details->name))
                              {echo $renewals_details->name;}?>" class="form-control">
                        </div>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="renewal_type"><?php echo _l('Type'); ?></label>
                            <select class="selectpicker" name="renewal_type" id="renewal_type"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Type</option>
                                <?php foreach($renewal_types as  $renewal_type){ ?>
                                    <option value="<?php echo $renewal_type['id']?>" 
                                      <?php if(isset($renewals_details->renewal_type)){
                                        echo ($renewals_details->renewal_type == $renewal_type['id']) ? 'selected' : '';
                                      }?>>
                                      <?php echo $renewal_type['type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="vendor"><?php echo _l('Vendor'); ?></label>
                          <select class="selectpicker" name="vendor" 
                                  id="vendor"data-width="100%" data-live-search="true">
                                <option value="" disabled selected="">Select Vendor</option>
                                <?php foreach($renewal_vendors as  $renewal_vendor){ ?>
                                    <option value="<?php echo $renewal_vendor['id']?>"
                                      <?php if(isset($renewals_details->vendor)) {
                                        echo ($renewals_details->vendor == $renewal_vendor['id']) ? 'selected' : '';
                                      }?>>
                                      <?php echo $renewal_vendor['vendor_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="start_date" class="control-label">Start Date</label>
                          <div class="input-group date">
                            <input type="text" id="start_date" name="start_date" class="form-control datepicker" 
                                   value="<?php if(isset($renewals_details->start_date)){
                                    echo $renewals_details->start_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="end_date" class="control-label">End Date</label>
                          <div class="input-group date">
                            <input type="text" id="end_date" name="end_date" class="form-control datepicker" value="<?php  if(isset($renewals_details->end_date)){
                                    echo $renewals_details->end_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                          <label for="note" class="control-label">Note</label>
                          <textarea id="note" name="note" class="form-control" rows="4">
                            <?php  if(isset($renewals_details->note)){
                              echo $renewals_details->note; 
                            }?>
                          </textarea>
                        </div>
                    </div>
                   <!--  <div class="col-md-4">
                        <div class="form-group">
                            <label for="notify_to">Notify To</label>
                            <input type="text" name="notify_to" id="notify_to" 
                            value="<?php  
                                        if(isset($renewals_details->notify_to)){
                                            echo $renewals_details->notify_to; 
                                        }
                                  ?>" class="form-control">
                        </div>
                    </div>
 -->
                    <div class="col-md-4">
                        <div class="form-group">
                         
                            <label for="notify_to" class="control-label">
                                <?php echo _l('Notify To'); ?>
                            </label>

                            <select class="selectpicker" id="notify_to" name="notify_to[]" multiple="multiple"  data-width="100%" data-none-selected-text="Nothing selected" data-live-search="true" >
                              
                            </select>

                       
                            

                           <!--  <select multiple data-live-search="true" name="notify_to" id="notify_to" 
                            value="<?php if(isset($inventory_details->notify_to)){
                              echo $inventory_details->notify_to;
                            }?>" class="form-control"></select> -->

                        </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
           <?php echo form_close(); ?>

        <?php if(isset($renewals_details)){?>
        <div class="panel_s">
        <div class="panel-body">
        <div class="tab-content">
          <h4 class="customer-profile-group-heading">Renewal Notes</h4>

          <?php echo form_open(admin_url('renewals/add_note/'.$renewals_details->id),array('id'=>'renewal-notes')); ?>
          <div class="form-group">
            <textarea id="renewal_note_description" name="renewal_note_description" class="form-control" rows="4"></textarea>
          </div>
          <button type="submit" class="btn btn-info pull-right"><?php echo _l('lead_add_edit_add_note'); ?></button>
          <?php echo form_close(); ?>
          <div class="clearfix"></div>
          <hr />
          <div class="panel_s no-shadow">
              <?php
                $len = count($notes);
                $i = 0;
                foreach($notes as $note){ ?>
              <div class="media renewal-note">
                <a href="<?php echo admin_url('profile/'.$note["addedfrom"]); ?>" target="_blank">
                <?php echo staff_profile_image($note['addedfrom'],array('staff-profile-image-small','pull-left mright10')); ?>
                </a>
                <div class="media-body">
                    <?php if($note['addedfrom'] == get_staff_user_id() || is_admin()){ ?>
                    <a href="#" class="pull-right text-danger" onclick="delete_renewal_note(this,<?php echo $note['id']; ?>, <?php echo $renewals_details->id; ?>);return false;"><i class="fa fa fa-times"></i></a>
                    <a href="#" class="pull-right mright5" onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><i class="fa fa-pencil-square-o"></i></a>
                    <?php } ?>
                    <?php if(!empty($note['date_contacted'])){ ?>
                    <span data-toggle="tooltip" data-title="<?php echo _dt($note['date_contacted']); ?>">
                    <i class="fa fa-phone-square text-success font-medium valign" aria-hidden="true"></i>
                    </span>
                    <?php } ?>
                    <small><?php echo _l('lead_note_date_added',_dt($note['dateadded'])); ?></small>
                    <a href="<?php echo admin_url('profile/'.$note["addedfrom"]); ?>" target="_blank">
                      <h5 class="media-heading bold"><?php echo get_staff_full_name($note['addedfrom']); ?></h5>
                    </a>
                    <div data-note-description="<?php echo $note['id']; ?>" class="text-muted">
                      <?php echo check_for_links(app_happy_text($note['description'])); ?>
                    </div>
                    <div data-note-edit-textarea="<?php echo $note['id']; ?>" class="hide mtop15">
                      <?php echo render_textarea('note','',$note['description']); ?>
                      <div class="text-right">
                          <button type="button" class="btn btn-default" onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><?php echo _l('cancel'); ?></button>
                          <button type="button" class="btn btn-info" onclick="edit_note(<?php echo $note['id']; ?>);"><?php echo _l('update_note'); ?></button>
                      </div>
                    </div>
                </div>
                <?php if ($i >= 0 && $i != $len - 1) {
                    echo '<hr />';
                    }
                    ?>
              </div>
              <?php $i++; } ?>
          </div>

        </div>
        </div>
        </div>
        <?php } ?>

      </div>

    <div class="btn-bottom-pusher"></div>
  </div>
  <?php /* $this->load->view('admin/inventory_details/inventory_detail_category'); /*/?>
  <?php init_tail(); ?>

<script>
   $(function(){
     appValidateForm($('#renewal_form'),
      {clientid:'required',renewal_type:'required',start_date:'required',end_date:'required',
      });
   });
</script>
<script type="text/javascript">
   jQuery(function($) {
    $('#clientid').change(function(){ 
        var value = $(this).val();
        // var typeValue = $.trim($("#type option:selected").text());
        if(value){
          $.ajax({
                   url:'<?=admin_url()?>renewals/get_notify_me',
                   method:"POST",
                   data:{id:value},
                   success:function(data){
                    $("#notify_to").empty();
                      if(JSON.parse(data).length > 0){
                          $.each(JSON.parse(data), function(index, json) {
                              $("#notify_to").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id==$('#subtypeid').val())?'selected' : ''}>${JSON.parse(data)[index].firstname}</option>`);
                          });
                          $('.selectpicker').selectpicker('refresh')
                        }
                   }
          });
        }
    }).trigger('change');
  });

 
</script>
</body>
</html>