
    <?php if(count($renewals_lists) > 0){?>
    <?php foreach ($renewals_lists as $key => $renewals_list) { ?>
    <tr class="has-row-options" data-href="<?php echo admin_url('renewals/renewal/').$renewals_list['id'] ?>">
      <td><?php echo $key+1 ?>
        <div class="row-options">
          
          <a href="<?php echo admin_url('renewals/renewal/').$renewals_list['id'] ?>">Edit </a>
          <span class="text-dark"> | </span>
          <a
          href="<?php echo admin_url('renewals/delete/').$renewals_list['id'] ?>" class="text-danger _delete">Delete </a>
        </td>
        <td><?php echo $renewals_list['company']; ?></td>
        <td><?php echo $renewals_list['name'] ?></td>
        <td><?php echo $renewals_list['type_name'] ?></td>
        <td><?php echo $renewals_list['vendor'] ?></td>
        <td><?php echo $renewals_list['start_date'] ?></td>
        <td><?php echo $renewals_list['end_date'] ?></td>
      </tr>
      <?php }  ?>
      <?php } else{ ?>
      <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
      <?php }?>
   