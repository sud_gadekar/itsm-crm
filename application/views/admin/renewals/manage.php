<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('renewals','','create')){ ?>
                  <a href="<?php echo admin_url('renewals/renewal'); ?>" class="btn btn-info">
                    <?php echo 'ADD'; ?></a>
                  <?php }?>
                    
            
                   <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>


                    </div>
                    <hr class="hr-panel-heading">
                     <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Renewal Summary</h4>
                        </div>
                          <div class="col-md-3 col-xs-6 mbot15 border-right">
                            <a href="#" onclick="filterbyexpiry(1);return false;">
                              <h3 class="bold "><?php echo $active_renewal; ?></h3>
                              <span class="text-info">
                                <?php echo 'Active' ?>
                              </span>
                            </a>
                          </div>
                           <div class="col-md-3 col-xs-6 mbot15 border-right">
                            <a href="#" onclick="filterbyexpiry(2);return false;">
                              <h3 class="bold"><?php echo $expired_renewal; ?></h3>
                              <span class="text-danger">
                                <?php echo 'Expired' ?>
                              </span>
                            </a>
                          </div>
                           <div class="col-md-3 col-xs-6 mbot15 border-right">
                            <a href="#" onclick="filterbyexpiry(3);return false;">
                              <h3 class="bold"><?php echo $expired_in_one_month_renewal; ?></h3>
                              <span class="text-warning">
                                <?php echo 'Expiring in 1 Month' ?>
                              </span>
                            </a>
                          </div>
                           <div class="col-md-3 col-xs-6 mbot15 border-right">
                            <a href="#" onclick="filterbyexpiry(4);return false;">
                              <h3 class="bold"><?php echo $expired_in_two_month_renewal; ?></h3>
                              <span style="color:#2d2d2d;">
                                <?php echo 'Expiring in 2 Months' ?>
                              </span>
                            </a>
                          </div>

                    </div>

                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="inventory_details_total"></div>
                  </div>
               </div>
          <!--   </div>
            <div class="row"> -->
             <div class="panel-body">
               <div class="renewals_table" id="small-table ">
                
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                         
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Contact'); ?></th>
                                <th><?php echo _l('Name'); ?></th>
                                <th><?php echo _l('Type'); ?></th>
                                <th><?php echo _l('Vendor'); ?></th>
                                <th><?php echo _l('Start Date'); ?></th>
                                <th><?php echo _l('End Date'); ?></th>
                                <!-- <th><?php echo _l('Notify To'); ?></th> -->
                              </thead>
                              <tbody>
                                <?php if(count($renewals_lists) > 0){?>

                                <?php foreach ($renewals_lists as $key => $renewals_list) { ?>
                                <tr class="has-row-options" data-href="<?php echo admin_url('renewals/renewal/').$renewals_list['id'] ?>">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('renewals/renewal/').$renewals_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('renewals/delete/').$renewals_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td><?php echo $renewals_list['company']; ?></td>
                                  <td><?php echo $renewals_list['name'] ?></td>
                                  <td><?php echo $renewals_list['type_name'] ?></td>
                                  <td><?php echo $renewals_list['vendor_name'] ?></td>
                                  <td><?php echo $renewals_list['start_date'] ?></td>
                                  <td><?php echo $renewals_list['end_date'] ?></td>
                                  <!-- <td><?php echo $renewals_list['notify_to'] ?></td> -->
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                         
                        </div>
                        <?php /*}*/?>

                     
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // inventory_details additional server params
             var inventory_details_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               inventory_details_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             //initDataTable('.table-inventory_details', admin_url+'inventory_details/table', 'undefined', 'undefined', inventory_details_ServerParams, <?php echo hooks()->apply_filters('inventory_details_table_default_order', json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

            // init_inventory_detail();

             $('#inventory_detail_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_inventory_detail').attr('data-empty-note');
                var emptyName = $('#tab_inventory_detail').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#inventory_detail_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="inventory_detail_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'inventory_details/convert_to_invoice/'+$('body').find('.inventory_detail_convert_btn').attr('data-id'), parameters);
            });
           });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".has-row-options").click(function() {
          window.location = $(this).data("href");
      });
  });

  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>renewals/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.table tbody').empty();
              $('.table tbody').html(data);
            }
           }
      });
  }
</script>

 <script type="text/javascript">
    function filterbyexpiry($id) {
       if($id){
          $.ajax({
                   url:'<?=admin_url()?>renewals/index',
                   method:"POST",
                   data:{id:$id},
                   success:function(data){
                    if(data){
                      $('.table tbody').empty();
                      $('.table tbody').html(data);
                    }
                   }
          });
        }
    }
  </script>

</body>
</html>
