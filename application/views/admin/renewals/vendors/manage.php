<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_category(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New Renewal vendor'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($renewals_vendors) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Vendor'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($renewals_vendors as $key => $renewals_vendor){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td><a href="#" onclick="edit_category(this,<?php echo $renewals_vendor['id']; ?>);return false;" data-name="<?php echo $renewals_vendor['vendor_name']; ?>"><?php echo $renewals_vendor['vendor_name']; ?></a></td>
									<td>
										<a href="#" onclick="edit_category(this,<?php echo $renewals_vendor['id']; ?>); return false" data-name="<?php echo $renewals_vendor['vendor_name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<!-- <a href="<?php echo admin_url('gaps/delete_category/'.$renewals_vendor['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> -->
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No renewal vendor found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="category" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('renewals/vendor'), array('id' => 'vendor_form')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button vendor="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Renewal vendor Edit'); ?></span>
					<span class="add-title"><?php echo _l('Renewal vendor Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('vendor_name','Renewal vendor Title'); ?>
				</div>
			</div>
			<div class="modal-footer">
				<button vendor="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button vendor="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_gap_categories);
		$('#category').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#category input[name="vendor_name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_gap_categories(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_category(){
		$('#category').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_category(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#category input[name="vendor_name"]').val(name);
		$('#category').modal('show');
		$('.add-title').addClass('hide');
	}
</script>

  <script>
   $(function(){
     appValidateForm($('#vendor_form'),{vendor_name:'required'});
   });
  </script>
</body>
</html>
