<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <h3>Guest Enquiries</h3>
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="enquiries_total"></div>
                  </div>
               </div>
            <!-- </div>
            <div class="row"> -->
               <div class="panel-body">
               <div class="enquiries_table" id="small-table ">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                        
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('Sr.No.'); ?></th>
                                <th><?php echo _l('Subject'); ?></th>
                                <th><?php echo _l('Name'); ?></th>
                                <th><?php echo _l('Company'); ?></th>
                                <th><?php echo _l('Email'); ?></th>
                                <th><?php echo _l('Contact No.'); ?></th>
                                <th><?php echo _l('Comment'); ?></th>
                                <th><?php echo _l('Action'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($enquiries) > 0){?>

                                <?php foreach ($enquiries as $key => $enquiry) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1; ?></td>
                                  <td><?php echo $enquiry['subject'] ?></td>
                                  <td><?php echo $enquiry['name'] ?></td>
                                  <td><?php echo $enquiry['company'] ?></td>
                                  <td><?php echo $enquiry['email'] ?></td>
                                  <td><?php echo $enquiry['contact'] ?></td>
                                  <td><?php echo $enquiry['comment'] ?></td>
                                  <td>
                                    <a href="<?php echo admin_url('tickets/guest_delete/').$enquiry['id'] ?>"><span class="fa fa-trash" title="delete"></span></a> | <a href="<?php echo admin_url('clients/client?enquiry=').$enquiry['id'] ?>" target="_blank"><span class="fa fa-phone" title="Create Company & Contact"></span></a>
                                  </td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                         
                          </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script type="text/javascript">
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>enquiries/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.enquiries_table').html(data);
            }
           }
      });
  }
</script>
</body>
</html>
