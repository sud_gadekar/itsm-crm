<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<?php set_ticket_open($ticket->adminread,$ticket->ticketid); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="horizontal-scrollable-tabs">
                            <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
                            <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
                            <div class="horizontal-tabs">
                                <ul class="nav nav-tabs no-margin nav-tabs-horizontal" role="tablist">
                                    <li role="presentation"
                                        class="<?php if(!$this->session->flashdata('active_tab')){echo 'active';} ?>">
                                        <a href="#addreply" aria-controls="addreply" role="tab" data-toggle="tab">
                                            <?php echo _l('ticket_single_add_reply'); ?>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#note" aria-controls="note" role="tab" data-toggle="tab">
                                            <?php echo _l('ticket_single_add_note'); ?>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab_reminders"
                                            onclick="initDataTable('.table-reminders', admin_url + 'misc/get_reminders/' + <?php echo $ticket->ticketid ;?> + '/' + 'ticket', undefined, undefined, undefined,[1,'asc']); return false;"
                                            aria-controls="tab_reminders" role="tab" data-toggle="tab">
                                            <?php echo _l('ticket_reminders'); ?>
                                            <?php
                                                $total_reminders = total_rows(db_prefix().'reminders',
                                                array(
                                                'isnotified'=>0,
                                                'staff'=>get_staff_user_id(),
                                                'rel_type'=>'ticket',
                                                'rel_id'=>$ticket->ticketid
                                                )
                                                );
                                                if($total_reminders > 0){
                                                echo '<span class="badge">'.$total_reminders.'</span>';
                                                }
                                                ?>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#othertickets" onclick="init_table_tickets(false);"
                                            aria-controls="othertickets" role="tab" data-toggle="tab">
                                            <?php echo _l('ticket_single_other_user_tickets'); ?>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tasks"
                                            onclick="init_rel_tasks_table(<?php echo $ticket->ticketid; ?>,'ticket'); return false;"
                                            aria-controls="tasks" role="tab" data-toggle="tab">
                                            <?php echo _l('tasks'); ?>
                                        </a>
                                    </li>
                                    <li role="presentation"
                                        class="<?php if($this->session->flashdata('active_tab_settings')){echo 'active';} ?>">
                                        <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                                            <?php echo _l('Details'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="mtop4 mbot20 pull-left">
                                    <span id="ticket_subject">
                                        #<?php echo $ticket->ticketid; ?> - <?php echo $ticket->subject; ?>
                                    </span>
                                    <?php if($ticket->project_id != 0){
                        echo '<br /><small>'._l('ticket_linked_to_project','<a href="'.admin_url('projects/view/'.$ticket->project_id).'">'.get_project_name_by_id($ticket->project_id).'</a>') .'</small>';
                        } ?>
                                </h3>
                                <?php echo '<div class="label mtop5 mbot15'.(is_mobile() ? ' ' : ' mleft15 ').'p8 pull-left single-ticket-status-label" style="background:'.$ticket->statuscolor.'">'.ticket_status_translate($ticket->ticketstatusid).'</div>'; ?>

                                <?php if(isset($ticket->company)) { ?>
                                <a href="<?php echo admin_url('clients/client/'.$ticket->userid) ?>"
                                    class="btn btn-info label mtop5 mbot15 p8 pull-left single-ticket-status-label"
                                    style="margin-left: 3rem;">
                                    <?php echo $ticket->company?>
                                </a>
                                <?php } ?>


                                <?php /*if($ticket->ticketstatusid == 5 ) { ?>
                                <a href="#"
                                    class="btn btn-info label mtop5 mbot15 p8 pull-left single-ticket-status-label"
                                    style="margin-left: 3rem;">
                                    <?php echo _l('Send SR Report')?>
                                </a>
                                <?php } */?>


                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <?php echo render_select('status_top',$statuses,array('ticketstatusid','name'),'',$ticket->status,array(),array(),'no-mbot','',false); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel"
                                class="tab-pane <?php if(!$this->session->flashdata('active_tab')){echo 'active';} ?>"
                                id="addreply">
                                <hr class="no-mtop" />
                                <?php $tags = get_tags_in($ticket->ticketid,'ticket'); ?>
                                <?php if(count($tags) > 0){ ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo '<b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b><br /><br /> ' . render_tags($tags); ?>
                                        <hr />
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(sizeof($ticket->ticket_notes) > 0){ ?>
                                <div class="row">
                                    <div class="col-md-12 mbot15">
                                        <h4 class="bold"><?php echo _l('ticket_single_private_staff_notes'); ?></h4>
                                        <div class="ticketstaffnotes">
                                            <div class="table-responsive">
                                                <table>
                                                    <tbody>
                                                        <?php foreach($ticket->ticket_notes as $note){ ?>
                                                        <tr>
                                                            <td>
                                                                <span class="bold">
                                                                    <?php echo staff_profile_image($note['addedfrom'],array('staff-profile-xs-image')); ?>
                                                                    <a
                                                                        href="<?php echo admin_url('staff/profile/'.$note['addedfrom']); ?>"><?php echo _l('ticket_single_ticket_note_by',get_staff_full_name($note['addedfrom'])); ?>
                                                                    </a>
                                                                </span>
                                                                <?php
                                                if($note['addedfrom'] == get_staff_user_id() || is_admin()){ ?>
                                                                <div class="pull-right">
                                                                    <a href="#" class="btn btn-default btn-icon"
                                                                        onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><i
                                                                            class="fa fa-pencil-square-o"></i></a>
                                                                    <a href="<?php echo admin_url('misc/delete_note/'.$note["id"]); ?>"
                                                                        class="mright10 _delete btn btn-danger btn-icon">
                                                                        <i class="fa fa-remove"></i>
                                                                    </a>
                                                                </div>
                                                                <?php } ?>
                                                                <hr class="hr-10" />
                                                                <div data-note-description="<?php echo $note['id']; ?>">
                                                                    <?php echo check_for_links($note['description']); ?>
                                                                </div>
                                                                <div data-note-edit-textarea="<?php echo $note['id']; ?>"
                                                                    class="hide inline-block full-width">
                                                                    <textarea name="description" class="form-control"
                                                                        rows="4"><?php echo clear_textarea_breaks($note['description']); ?></textarea>
                                                                    <div class="text-right mtop15">
                                                                        <button type="button" class="btn btn-default"
                                                                            onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><?php echo _l('cancel'); ?></button>
                                                                        <button type="button" class="btn btn-info"
                                                                            onclick="edit_note(<?php echo $note['id']; ?>);"><?php echo _l('update_note'); ?></button>
                                                                    </div>
                                                                </div>
                                                                <small class="bold">
                                                                    <?php echo _l('ticket_single_note_added',_dt($note['dateadded'])); ?>
                                                                </small>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div>
                                    <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'single-ticket-form','novalidate'=>true)); ?>
                                    <a href="<?php echo admin_url('tickets/delete/'.$ticket->ticketid); ?>"
                                        class="btn btn-danger _delete btn-ticket-label mright5">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                    <?php if(!empty($ticket->priority_name)){ ?>
                                    <span class="ticket-label label label-default inline-block">
                                        <?php echo _l('ticket_single_priority',ticket_priority_translate($ticket->priorityid)); ?>
                                    </span>
                                    <?php } ?>
                                    <?php if(!empty($ticket->service_name)){ ?>
                                    <span class="ticket-label label label-default inline-block">
                                        <?php echo _l('service'). ': ' . $ticket->service_name; ?>
                                    </span>
                                    <?php } ?>

                                    <?php echo form_hidden('ticketid',$ticket->ticketid); ?>
                                    <span class="ticket-label label label-default inline-block">
                                        <?php echo _l('Company') . ': '. $ticket->company; ?>
                                    </span>
                                    
                                    <?php if($ticket->assigned != 0){ ?>
                                    <span class="ticket-label label label-info inline-block">
                                        <?php echo _l('ticket_assigned'); ?>:
                                        <?php echo get_staff_full_name($ticket->assigned); ?>
                                    </span>
                                    <?php } ?>
                                    <?php if($ticket->lastreply !== NULL){ ?>
                                    <span class="ticket-label label label-success inline-block" data-toggle="tooltip"
                                        title="<?php echo _dt($ticket->lastreply); ?>">
                                        <span class="text-has-action">
                                            <?php echo _l('ticket_single_last_reply',time_ago($ticket->lastreply)); ?>
                                        </span>
                                    </span>
                                    <?php } ?>
                                    <span class="ticket-label label label-info inline-block">
                                        <a href="<?php echo get_ticket_public_url($ticket); ?>" target="_blank">
                                            <?php echo _l('view_public_form'); ?>
                                        </a>
                                    </span>
                                    <div class="mtop15">
                                        <?php
                              $use_knowledge_base = get_option('use_knowledge_base');
                              ?>
                                        <div class="row mbot15">
                                            <div class="col-md-6">
                                                <select data-width="100%" id="insert_predefined_reply"
                                                    data-live-search="true" class="selectpicker"
                                                    data-title="<?php echo _l('ticket_single_insert_predefined_reply'); ?>">
                                                    <?php foreach($predefined_replies as $predefined_reply){ ?>
                                                    <option value="<?php echo $predefined_reply['id']; ?>">
                                                        <?php echo $predefined_reply['name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php if($use_knowledge_base == 1){ ?>
                                            <div class="visible-xs">
                                                <div class="mtop15"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $groups = get_all_knowledge_base_articles_grouped(); ?>
                                                <select data-width="100%" id="insert_knowledge_base_link"
                                                    class="selectpicker" data-live-search="true"
                                                    onchange="insert_ticket_knowledgebase_link(this);"
                                                    data-title="<?php echo _l('ticket_single_insert_knowledge_base_link'); ?>">
                                                    <option value=""></option>
                                                    <?php foreach($groups as $group){ ?>
                                                    <?php if(count($group['articles']) > 0){ ?>
                                                    <optgroup label="<?php echo $group['name']; ?>">
                                                        <?php foreach($group['articles'] as $article) { ?>
                                                        <option value="<?php echo $article['articleid']; ?>">
                                                            <?php echo $article['subject']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </optgroup>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php echo render_textarea('message','','',array(),array(),'','tinymce'); ?>
                                    </div>
                                    <div class="panel_s ticket-reply-tools">
                                        <div class="btn-bottom-toolbar text-right">
                                            <button type="submit" class="btn btn-info" data-form="#single-ticket-form"
                                                autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>">
                                                <?php echo _l('ticket_single_add_response'); ?>
                                            </button>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <?php echo render_select('status',$statuses,array('ticketstatusid','name'),'ticket_single_change_status',get_option('default_ticket_reply_status'),array(),array(),'','',false); ?>
                                                    <?php //echo render_input('cc','CC'); ?>
                                                    <?php
                                                    $selected = array();
                                                    if(isset($ticket->cc_contacts)){
                                                        foreach(explode(',',$ticket->cc_contacts) as $member){
                                                            array_push($selected,$member);
                                                        }
                                                    } else {
                                                        array_push($selected,get_staff_user_id());
                                                    }
                                                    echo render_select('cc_contacts[]',$company_contacts,array('id','email'),'CC (Contacts)',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                                                    ?>
                                                    <?php
                                                    $selected = array();
                                                    if(isset($ticket->cc_staffs)){
                                                        foreach(explode(',',$ticket->cc_staffs) as $member){
                                                            array_push($selected,$member);
                                                        }
                                                    } else {
                                                        array_push($selected,get_staff_user_id());
                                                    }
                                                    echo render_select('cc_staffs[]',$company_staffs,array('staffid','email'),'CC (Staffs)',$selected,array('multiple'=>true,'data-actions-box'=>true),array(),'','',false);
                                                    ?>
                                                    <?php if($ticket->assigned !== get_staff_user_id()){ ?>
                                                    <div class="checkbox">
                                                        <input type="checkbox" name="assign_to_current_user"
                                                            id="assign_to_current_user">
                                                        <label
                                                            for="assign_to_current_user"><?php echo _l('ticket_single_assign_to_me_on_update'); ?></label>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="checkbox">
                                                        <input type="checkbox"
                                                            <?php echo hooks()->apply_filters('ticket_add_response_and_back_to_list_default','checked'); ?>
                                                            name="ticket_add_response_and_back_to_list" value="1"
                                                            id="ticket_add_response_and_back_to_list">
                                                        <label
                                                            for="ticket_add_response_and_back_to_list"><?php echo _l('ticket_add_response_and_back_to_list'); ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row attachments">
                                                <div class="attachment">
                                                    <div class="col-md-5 mbot15">
                                                        <div class="form-group">
                                                            <label for="attachment" class="control-label">
                                                                <?php echo _l('ticket_single_attachments'); ?>
                                                            </label>
                                                            <div class="input-group">
                                                                <input type="file"
                                                                    extension="<?php echo str_replace(['.', ' '], '', get_option('ticket_attachments_file_extensions')); ?>"
                                                                    filesize="<?php echo file_upload_max_size(); ?>"
                                                                    class="form-control" name="attachments[0]"
                                                                    accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
                                                                <span class="input-group-btn">
                                                                    <button
                                                                        class="btn btn-success add_more_attachments p8-half"
                                                                        data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>"
                                                                        type="button"><i
                                                                            class="fa fa-plus"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>

                                </div>
                                <div class="panel_s mtop20">
                    <div class="panel-body <?php if($ticket->admin == NULL){echo 'client-reply';} ?>">
                        <div class="row">
                            <div class="col-md-3 border-right ticket-submitter-info ticket-submitter-info">
                                <p>
                                    <?php if($ticket->admin == NULL || $ticket->admin == 0){ ?>
                                    <?php if($ticket->userid != 0){ ?>
                                    <a
                                        href="<?php echo admin_url('clients/client/'.$ticket->userid.'?contactid='.$ticket->contactid); ?>"><?php echo $ticket->submitter; ?>
                                    </a>
                                    <?php } else {
                           echo $ticket->submitter;
                           ?>
                                    <br />
                                    <a
                                        href="mailto:<?php echo $ticket->ticket_email; ?>"><?php echo $ticket->ticket_email; ?></a>
                                    <hr />
                                    <?php
                           if(total_rows(db_prefix().'spam_filters',array('type'=>'sender','value'=>$ticket->ticket_email,'rel_type'=>'tickets')) == 0){ ?>
                                    <button type="button" data-sender="<?php echo $ticket->ticket_email; ?>"
                                        class="btn btn-danger block-sender btn-xs"> <?php echo _l('block_sender'); ?>
                                    </button>
                                    <?php
                           } else {
                           echo '<span class="label label-danger">'._l('sender_blocked').'</span>';
                           }
                           }
                           } else {  ?>
                                    <a
                                        href="<?php echo admin_url('profile/'.$ticket->admin); ?>"><?php echo $ticket->opened_by; ?></a>
                                    <?php } ?>
                                </p>
                                <p class="text-muted">
                                    <?php /* if($ticket->admin !== NULL || $ticket->admin != 0){
                                        echo _l('ticket_staff_string');
                                        } else {
                                          if($ticket->userid != 0){
                                             echo _l('ticket_client_string');
                                            }
                                        }
                                    */ ?>
                                </p>
                                <?php if(has_permission('tasks','','create')){ ?>
                                <a href="#" class="btn btn-default btn-xs"
                                    onclick="convert_ticket_to_task(<?php echo $ticket->ticketid; ?>,'ticket'); return false;"><?php echo _l('convert_to_task'); ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?php if(!empty($ticket->message)) { ?>
                                        <a href="#"
                                            onclick="print_ticket_message(<?php echo $ticket->ticketid; ?>, 'ticket'); return false;"
                                            class="mright5"><i class="fa fa-print"></i></a>
                                        <?php } ?>
                                        <a href="#"
                                            onclick="edit_ticket_message(<?php echo $ticket->ticketid; ?>,'ticket'); return false;"><i
                                                class="fa fa-pencil-square-o"></i></a>
                                    </div>
                                </div>
                                <div data-ticket-id="<?php echo $ticket->ticketid; ?>" class="tc-content">
                                    <?php echo check_for_links($ticket->message); ?>
                                </div>
                                <?php if(count($ticket->attachments) > 0){
                        echo '<hr />';
                        foreach($ticket->attachments as $attachment){
                        $path = get_upload_path_by_type('ticket').$ticket->ticketid.'/'.$attachment['file_name'];
                        $is_image = is_image($path);
                        if($is_image){
                        echo '<div class="preview_image">';
                           }
                           ?>
                                <a href="<?php echo site_url('download/file/ticket/'. $attachment['id']); ?>"
                                    class="display-block mbot5" <?php if($is_image){ ?>
                                    data-lightbox="attachment-ticket-<?php echo $ticket->ticketid; ?>" <?php } ?>>
                                    <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i>
                                    <?php echo $attachment['file_name']; ?>
                                    <?php if($is_image){ ?>
                                    <img class="mtop5"
                                        src="<?php echo site_url('download/preview_image?path='.protected_file_url_by_path($path).'&type='.$attachment['filetype']); ?>">
                                    <?php } ?>
                                </a>
                                <?php if($is_image){
                        echo '</div>';
                        }
                        if(is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_ticket_attachments') == '1')){
                        echo '<a href="'.admin_url('tickets/delete_attachment/'.$attachment['id']).'" class="text-danger _delete">'._l('delete').'</a>';
                        }
                        echo '<hr />';
                        ?>
                                <?php }
                        } ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <?php echo _l('ticket_posted',_dt($ticket->date)); ?>
                    </div>
                </div>
                <?php foreach($ticket_replies as $reply) { ?>
                <div class="panel_s">
                    <div class="panel-body <?php if($reply['admin'] == NULL){echo 'client-reply';} ?>">
                        <div class="row">
                            <div class="col-md-3 border-right ticket-submitter-info">
                                <p>
                                    <?php if($reply['admin'] == NULL || $reply['admin'] == 0){ ?>
                                    <?php if($reply['userid'] != 0){ ?>
                                    <a
                                        href="<?php echo admin_url('clients/client/'.$reply['userid'].'?contactid='.$reply['contactid']); ?>"><?php echo $reply['submitter']; ?></a>
                                    <?php } else { ?>
                                    <?php echo $reply['submitter']; ?>
                                    <br />
                                    <a
                                        href="mailto:<?php echo $reply['reply_email']; ?>"><?php echo $reply['reply_email']; ?></a>
                                    <?php } ?>
                                    <?php }  else { ?>
                                    <a
                                        href="<?php echo admin_url('profile/'.$reply['admin']); ?>"><?php echo $reply['submitter']; ?></a>
                                    <?php } ?>
                                </p>
                                <p class="text-muted">
                                    <?php if($reply['admin'] !== NULL || $reply['admin'] != 0){
                                        echo _l('ticket_staff_string');
                                        } else {
                                        if($reply['userid'] != 0){
                                        echo _l('ticket_client_string');
                                        }
                                        }
                                        ?>
                                </p>
                                <hr />
                                <a href="<?php echo admin_url('tickets/delete_ticket_reply/'.$ticket->ticketid .'/'.$reply['id']); ?>"
                                    class="btn btn-danger pull-left _delete mright5 btn-xs"><?php echo _l('delete_ticket_reply'); ?></a>
                                <div class="clearfix"></div>
                                <?php if(has_permission('tasks','','create')){ ?>
                                <a href="#" class="pull-left btn btn-default mtop5 btn-xs"
                                    onclick="convert_ticket_to_task(<?php echo $reply['id']; ?>,'reply'); return false;"><?php echo _l('convert_to_task'); ?>
                                </a>
                                <div class="clearfix"></div>
                                <?php } ?>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?php if(!empty($reply['message'])) { ?>
                                        <a href="#"
                                            onclick="print_ticket_message(<?php echo $reply['id']; ?>, 'reply'); return false;"
                                            class="mright5"><i class="fa fa-print"></i></a>
                                        <?php } ?>
                                        <a href="#"
                                            onclick="edit_ticket_message(<?php echo $reply['id']; ?>,'reply'); return false;"><i
                                                class="fa fa-pencil-square-o"></i></a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div data-reply-id="<?php echo $reply['id']; ?>" class="tc-content">
                                    <?php echo check_for_links($reply['message']); ?>
                                </div>
                                <?php if(count($reply['attachments']) > 0){
                        echo '<hr />';
                        foreach($reply['attachments'] as $attachment){
                        $path = get_upload_path_by_type('ticket').$ticket->ticketid.'/'.$attachment['file_name'];
                        $is_image = is_image($path);
                        if($is_image){
                        echo '<div class="preview_image">';
                           }
                           ?>
                                <a href="<?php echo site_url('download/file/ticket/'. $attachment['id']); ?>"
                                    class="display-block mbot5" <?php if($is_image){ ?>
                                    data-lightbox="attachment-reply-<?php echo $reply['id']; ?>" <?php } ?>>
                                    <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i>
                                    <?php echo $attachment['file_name']; ?>
                                    <?php if($is_image){ ?>
                                    <img class="mtop5"
                                        src="<?php echo site_url('download/preview_image?path='.protected_file_url_by_path($path).'&type='.$attachment['filetype']); ?>">
                                    <?php } ?>
                                </a>
                                <?php if($is_image){
                        echo '</div>';
                        }
                        if(is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_ticket_attachments') == '1')){
                        echo '<a href="'.admin_url('tickets/delete_attachment/'.$attachment['id']).'" class="text-danger _delete">'._l('delete').'</a>';
                        }
                        echo '<hr />';
                        }
                        } ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span><?php echo _l('ticket_posted',_dt($reply['date'])); ?></span>
                    </div>
                </div>
                <?php } ?>
                <!-- Start -->
                <?php foreach ($ticket_replies_mail as $reply) { ?>
                <div class="panel_s">
                    <div class="panel-body
                  echo 'client-reply';} ?>">
                        <div class="row">
                            <div class="col-md-3 border-right ticket-submitter-info">
                                <p>
                                    <?php if ($reply['id'] != 0) { ?>
                                    <?php echo $reply['sender_name']; ?>
                                    <?php }  ?>
                                </p>
                                <p class="text-muted">
                                    <?php echo $ticket->company ?>
                                </p>
                                <hr />
                                <!-- <a href="<?php /* echo admin_url(
                           'tickets/delete_ticket_reply/' .
                           $ticket->ticketid .
                           '/' .
                           $reply['id']
                           ); ?>" class="btn btn-danger pull-left _delete mright5 btn-xs"><?php echo _l(
                           'delete_ticket_reply'
                           );*/ ?>
                        </a> -->
                                <div class="clearfix"></div>
                                <?php if (has_permission('tasks', '', 'create')
                        ) { ?>
                                <a href="#" class="pull-left btn btn-default mtop5 btn-xs" onclick="convert_ticket_to_task(<?php echo $reply[
                           'id'
                           ]; ?>,'reply'); return false;"><?php echo _l('convert_to_task'); ?>
                                </a>
                                <div class="clearfix"></div>
                                <?php } ?>
                            </div>
                            <!-- col-9 -->
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?php if (
                              !empty($reply['body'])
                              ) { ?>
                                        <a href="#" onclick="print_ticket_message(<?php echo $reply[
                                 'id'
                                 ]; ?>, 'reply'); return false;" class="mright5"><i class="fa fa-print"></i></a>
                                        <?php } ?>
                                        <!-- <a href="#" onclick="edit_ticket_message(<?php /* echo $reply[
                                 'id'
                              ]; */ ?>,'reply'); return false;"><i class="fa fa-pencil-square-o"></i></a> -->
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div data-reply-id="<?php echo $reply[
                           'id'
                           ]; ?>" class="tc-content">
                                    <?php echo check_for_links(
                           $reply['body']
                           ); ?>
                                </div>
                                <?php /*if (count($reply['has_attachment']) > 0) {
                        echo '<hr />';
                        foreach ($reply['has_attachment']as $attachment) {
                        $path =get_upload_path_by_type('ticket') .$ticket->ticketid .
                        '/' .$attachment['file_name'];
                        $is_image = is_image($path);
                        if ($is_image) {
                        echo '<div class="preview_image">';
                           }
                           ?>
                                <a href="<?php echo site_url(
                              'download/file/ticket/' . $attachment['id']
                              ); ?>" class="display-block mbot5" <?php if ($is_image) { ?>
                                    data-lightbox="attachment-reply-<?php echo $reply['id']; ?>" <?php } ?>>
                                    <i class="<?php echo get_mime_class(
                              $attachment['filetype']
                              ); ?>"></i>
                                    <?php echo $attachment['file_name']; ?>
                                    <?php if ($is_image) { ?>
                                    <img class="mtop5" src="<?php echo site_url(
                              'download/preview_image?path=' .
                              protected_file_url_by_path(
                              $path
                              ) .
                              '&type=' .
                              $attachment['filetype']
                              ); ?>">
                                    <?php } ?>
                                </a>
                                <?php
                           if ($is_image) {
                        echo '</div>';
                        }
                        if (
                        is_admin() ||
                        (!is_admin() &&
                        get_option(
                        'allow_non_admin_staff_to_delete_ticket_attachments'
                        ) == '1')
                        ) {
                        echo '<a href="' .
                           admin_url(
                           'tickets/delete_attachment/' .
                           $attachment['id']
                           ) .
                           '" class="text-danger _delete">' .
                           _l('delete') .
                        '</a>';
                        }
                        echo '<hr />';
                        }
                        } */?>
                            </div>
                            <!-- col-9 End Here-->
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span><?php echo _l(
                     'ticket_posted',
                     _dt($reply['date_received'])
                  ); ?></span>
                    </div>
                </div>
                <?php } ?>
                <!--                    End -->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="note">
                                <hr class="no-mtop" />
                                <div class="form-group">
                                    <label
                                        for="note_description"><?php echo _l('ticket_single_note_heading'); ?></label>
                                    <textarea class="form-control" name="note_description" rows="5"></textarea>
                                </div>
                                <a
                                    class="btn btn-info pull-right add_note_ticket"><?php echo _l('ticket_single_add_note'); ?></a>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab_reminders">
                                <a href="#" class="btn btn-info btn-xs" data-toggle="modal"
                                    data-target=".reminder-modal-ticket-<?php echo $ticket->ticketid; ?>"><i
                                        class="fa fa-bell-o"></i> <?php echo _l('ticket_set_reminder_title'); ?></a>
                                <hr />
                                <?php render_datatable(array( _l( 'reminder_description'), _l( 'reminder_date'), _l( 'reminder_staff'), _l( 'reminder_is_notified')), 'reminders'); ?>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="othertickets">
                                <hr class="no-mtop" />
                                <div class="_filters _hidden_inputs hidden tickets_filters">
                                    <?php echo form_hidden('filters_ticket_id',$ticket->ticketid); ?>
                                    <?php echo form_hidden('filters_email',$ticket->email); ?>
                                    <?php echo form_hidden('filters_userid',$ticket->userid); ?>
                                </div>
                                <?php echo AdminTicketsTableStructure(); ?>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tasks">
                                <hr class="no-mtop" />
                                <?php init_relation_tasks_table(array('data-new-rel-id'=>$ticket->ticketid,'data-new-rel-type'=>'ticket')); ?>
                            </div>
                            <div role="tabpanel"
                                class="tab-pane <?php if($this->session->flashdata('active_tab_settings')){echo 'active';} ?>"
                                id="settings">
                                <hr class="no-mtop" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo render_input('subject','ticket_settings_subject',$ticket->subject); ?>
                                        <div class="form-group select-placeholder">
                                            <label for="contactid"
                                                class="control-label"><?php echo _l('contact'); ?></label>
                                            <select name="contactid" id="contactid" class="ajax-search"
                                                data-width="100%" data-live-search="true"
                                                data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                                <?php if(!empty($ticket->from_name) && !empty($ticket->ticket_email)){echo ' data-no-contact="true"';} else {echo ' data-ticket-emails="'.$ticket->ticket_emails.'"';} ?>>
                                                <?php
                                                        $rel_data = get_relation_data('contact',$ticket->contactid);
                                                        $rel_val = get_relation_values($rel_data,'contact');
                                                        echo '<option value="'.$rel_val['id'].'" selected data-subtext="'.$rel_val['subtext'].'">'.$rel_val['name'].'</option>';
                                                        ?>
                                            </select>
                                            <?php echo form_hidden('userid',$ticket->userid); ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php echo render_input('name','ticket_settings_to',$ticket->submitter,'text',array('readonly'=>true)); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                        if($ticket->userid != 0){
                                                        echo render_input('email','ticket_settings_email',$ticket->email,'email',array('readonly'=>true));
                                                        } else {
                                                        echo render_input('email','ticket_settings_email','','email',array('readonly'=>true));
                                                        }
                                                        ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo render_input('company','Company',$ticket->company,'text',array('disabled'=>true)); ?>
                                            </div>
                                        </div>
                                        <?php echo render_select('department',$departments,array('departmentid','name'),'ticket_settings_departments',$ticket->department); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mbot20">
                                            <label for="tags" class="control-label"><i class="fa fa-tag"
                                                    aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                                            <input type="text" class="tagsinput" id="tags" name="tags"
                                                value="<?php echo prep_tags_input(get_tags_in($ticket->ticketid,'ticket')); ?>"
                                                data-role="tagsinput">
                                        </div>
                                        <div class="form-group select-placeholder">
                                            <label for="assigned" class="control-label">
                                                <?php echo _l('ticket_settings_assign_to'); ?>
                                            </label>
                                            <select name="assigned" data-live-search="true" id="assigned"
                                                class="form-control selectpicker"
                                                data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                
                                                <option>
                                                    <?php foreach($staff as $member) { ?>
                                                <option value="<?php echo $member['staffid']; ?>"
                                                    <?php echo($ticket->assigned == $member['staffid']) ? 'selected' : ''?>>
                                                    <?php echo $member['firstname'] . ' ' . $member['lastname'] ; ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="row">
                                            <div
                                                class="col-md-<?php if(get_option('services') == 1){ echo 6; }else{echo 12;} ?>">
                                                <?php
                                    $priorities['callback_translate'] = 'ticket_priority_translate';
                                    echo render_select('priority',$priorities,array('priorityid','name'),'ticket_settings_priority',$ticket->priority); ?>
                                            </div>
                                            <?php if(get_option('services') == 1){ ?>
                                            <div class="col-md-6">
                                                <?php if(is_admin() || get_option('staff_members_create_inline_ticket_services') == '1'){
                                    echo render_select_with_input_group('service',$services,array('serviceid','name'),'ticket_settings_service',$ticket->service,'<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
                                    } else {
                                    echo render_select('service',$services,array('serviceid','name'),'ticket_settings_service',$ticket->service);
                                    }
                                    ?>
                                            </div>
                                            <?php } ?>
                                            <div class="col-md-6">
                                                <div class="form-group select-placeholder">
                                                    <label for="direction"><?php echo _l('Billing Type'); ?></label>
                                                    <select class="selectpicker" name="billing_type" id="billing_type"
                                                        onchange="getBillingValue(this);" data-width="100%">
                                                        <option value="contract"
                                                            <?php echo ($ticket->billing_type == 'contract') ? 'selected' : '' ?>>
                                                            Contract</option>
                                                        <option value="additional_service"
                                                            <?php echo ($ticket->billing_type == 'additional_service') ? 'selected' : '' ?>>
                                                            Additional Service</option>
                                                        <option value="lpo"
                                                            <?php echo ($ticket->billing_type == 'lpo') ? 'selected' : '' ?>>
                                                            LPO</option>
                                                        <option value="free"
                                                            <?php echo ($ticket->billing_type == 'free') ? 'selected' : '' ?>>
                                                            Free</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6 contractselect" style="display: none;">
                                                <div class="form-group">
                                                    <label for="contract" class="control-label">
                                                        <?php echo _l('Contract'); ?>
                                                    </label>
                                                    <?php if(isset($contracts)) { ?>
                                                    <select class="form-control" name="contract">
                                                        <?php  foreach($contracts as $contract) { ?>
                                                        <option value="<?php echo $contract['id']; ?>"
                                                            <?php echo ($contract['id'] == $ticket->contract) ? 'selected' : '' ?>>
                                                            <?php echo $contract['subject']; ?></option>
                                                        <?php }  ?>
                                                    </select>
                                                    <?php } else {?>
                                                    <select name="contract" id="contract" class="form-control"></select>
                                                    <?php }?>
                                                </div>
                                            </div>

                                            <div class="col-md-6 additional_charges_select" style="display: none;">
                                                <div class="form-group">
                                                    <label for="additional_charge" class="control-label">
                                                        <?php echo _l('Additional Charges'); ?>
                                                    </label>
                                                    <?php if(isset($addtional_charges_select)) { ?>
                                                    <select class="form-control" name="additional_charge[]">
                                                        <?php  foreach($addtional_charges_select as $charge) { ?>
                                                        <option value="<?php echo $charge['id']; ?>">
                                                            <?php echo $charge['chargetype']; ?></option>
                                                        <?php }  ?>
                                                    </select>
                                                    <?php } else { ?>
                                                    <select name="additional_charge[]" id="additional_charge" multiple
                                                        class="form-control"></select>
                                                    <?php } ?>
                                                </div>
                                            </div>


                                        </div>
                                        <div
                                            class="form-group select-placeholder projects-wrapper<?php if($ticket->userid == 0){echo ' hide';} ?>">
                                            <label for="project_id"><?php echo _l('project'); ?></label>
                                            <div id="project_ajax_search_wrapper">
                                                <select name="project_id" id="project_id" class="projects ajax-search"
                                                    data-live-search="true" data-width="100%"
                                                    data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                    <?php if($ticket->project_id != 0){ ?>
                                                    <option value="<?php echo $ticket->project_id; ?>">
                                                        <?php echo get_project_name_by_id($ticket->project_id); ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <?php echo render_custom_fields('tickets',$ticket->ticketid); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <hr />
                                        <a href="#" class="btn btn-info save_changes_settings_single_ticket">
                                            <?php echo _l('submit'); ?>
                                        </a>
                                    </div>
                                </div>
                                <hr class="hr-panel-heading" />

                                <?php if(count($teams) > 0){ ?>
                                <div class="panel_s mtop20">
                                    <div class="panel-body">
                                        <div class="_buttons">
                                            <span class="btn btn-info pull-left display-block"><?php echo _l('Designated Team'); ?></span>
                                            <a href="<?php echo admin_url('clients/client/'.$ticket->userid.'?group=team') ?>"><span class="btn btn-info pull-right display-block"><?php echo _l('Add New'); ?></span></a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr class="hr-panel-heading" />
                                        <?php if(count($teams) > 0){ ?>

                                        <table class="table dt-table scroll-responsive">
                                            <thead>
                                                <th><?php echo _l('id'); ?></th>
                                                <th><?php echo _l('Role'); ?></th>
                                                <th><?php echo _l('Staff Name'); ?></th>
                                                <th><?php echo _l('Email'); ?></th>
                                                <th><?php echo _l('Mobile Number'); ?></th>
                                            </thead>
                                            <tbody>
                                                <?php foreach($teams as $key => $team){ ?>
                                                <tr>
                                                    <td><?php echo $key+1; ?></td>

                                                    <td>
                                                        <?php echo $this->db->where('roleid',$team['role'])->get('tblteamroles')->row()->name;?>
                                                    </td>

                                                    <td>
                                                        <?php echo $team['firstname'].' '.$team['lastname']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $team['email']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $team['phonenumber']; ?>
                                                    </td>

                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php } else { ?>
                                        <p class="no-margin"><?php echo _l('No Team found'); ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if(is_admin()){ ?>

                                    <?php if(count($notes) > 0){ ?>
                                    <div class="panel_s mtop20">
                                        <div class="panel-body">
                                            <div class="_buttons">
                                                <span
                                                    class="btn btn-info pull-left display-block"><?php echo _l('Notes'); ?></span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr class="hr-panel-heading" />
                                            <?php if(count($notes) > 0){ ?>

                                            <table class="table dt-table scroll-responsive">
                                                <thead>
                                                    <th><?php echo _l('id'); ?></th>
                                                    <th><?php echo _l('Description'); ?></th>
                                                    <th><?php echo _l('Created At'); ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($notes as $key => $note){ ?>
                                                    <tr>
                                                        <td><?php echo $key+1; ?></td>

                                                        <td><?php echo $note['description'] ?></td>
                                                        <td><?php echo $note['dateadded'] ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <?php } else { ?>
                                            <p class="no-margin"><?php echo _l('No note found'); ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>

                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>


                <!-- Start Notes -->



                <!-- End Notes -->
               
            </div>
        </div>
        <div class="btn-bottom-pusher"></div>
        <?php if(count($ticket_replies) > 1){ ?>
        <a href="#top" id="toplink">↑</a>
        <a href="#bot" id="botlink">↓</a>
        <?php } ?>
    </div>
</div>
<!-- The reminders modal -->
<?php $this->load->view('admin/includes/modals/reminder',array(
'id'=>$ticket->ticketid,
'name'=>'ticket',
'members'=>$staff,
'reminder_title'=>_l('ticket_set_reminder_title'))
); ?>
<!-- Edit ticket Messsage Modal -->
<div class="modal fade" id="ticket-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <?php echo form_open(admin_url('tickets/edit_message')); ?>
        <div class="modal-content">
            <div id="edit-ticket-message-additional"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo _l('ticket_message_edit'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo render_textarea('data','','',array(),array(),'','tinymce-ticket-edit'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<div class="modal fade" id="closure-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <?php $attributes = array('id' => 'myform');
      echo form_open(admin_url('tickets/before_closing_request/'));
      ?>
        <div class="modal-content">
            <div id="edit-ticket-message-additional"></div>
           


           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Well Done - <?php echo $ticket->firstname.' '.$ticket->lastname?></h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" name="worklog" value="1" id="worklog"
                            <?php /*echo ($ticket->worklog == 1) ? 'checked':'' */?>>
                        <label for="worklog">Work Log created</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" name="user_acceptance" value="1" id="user_acceptance">
                        <label for="user_acceptance">Customer has accpeted the resolution</label>
                        <input type="hidden" name="ticketid" 
                        value="<?php if(isset($ticket->ticketid)){echo $ticket->ticketid;}?>">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l(
            'close'
            ); ?></button>
                <button disabled="" type="submit" id="btncheck" class="btn btn-info"><?php echo _l(
            'submit'
            ); ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>


<div class="modal fade" id="close-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <?php $attributes = array('id' => '');
      echo form_open(admin_url('tickets/before_close_status/'));
      ?>
        <div class="modal-content">
            <div id="edit-ticket-message-additional"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" name="attach_service_report" value="1" id="attach_service_report">
                        <label for="user_acceptance">Attach Service Report</label>
                        <input type="hidden" name="ticketid" 
                        value="<?php if(isset($ticket->ticketid)){echo $ticket->ticketid;}?>">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l(
            'close'
            ); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l(
            'submit'
            ); ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>


<script>
var _ticket_message;
</script>
<?php $this->load->view('admin/tickets/services/service'); ?>
<?php init_tail(); ?>
<?php hooks()->do_action('ticket_admin_single_page_loaded', $ticket); ?>
<script>
$(document).ready(function() {
    if ($("#billing_type option:selected").val() == 'contract') {
        $('.contractselect').css('display', 'block')
    }
    if ($("#billing_type option:selected").val() == 'additional_service') {
        $('.additional_charges_select').css('display', 'block');
    }

});


$(function() {
    $('#single-ticket-form').appFormValidator();
    init_ajax_search('contact', '#contactid.ajax-search', {
        tickets_contacts: true
    });
    init_ajax_search('project', 'select[name="project_id"]', {
        customer_id: function() {
            return $('input[name="userid"]').val();
        }
    });
    $('body').on('shown.bs.modal', '#_task_modal', function() {
        if (typeof(_ticket_message) != 'undefined') {
            // Init the task description editor
            if (!is_mobile()) {
                $(this).find('#description').click();
            } else {
                $(this).find('#description').focus();
            }
            setTimeout(function() {
                tinymce.get('description').execCommand('mceInsertContent', false,
                    _ticket_message);
                $('#_task_modal input[name="name"]').val($('#ticket_subject').text()
                    .trim());
            }, 100);
        }
    });
});
var ticket_message_editor;
var edit_ticket_message_additional = $('#edit-ticket-message-additional');

function edit_ticket_message(id, type) {
    edit_ticket_message_additional.empty();
    // type is either ticket or reply
    _ticket_message = $('[data-' + type + '-id="' + id + '"]').html();
    init_ticket_edit_editor();
    tinyMCE.activeEditor.setContent(_ticket_message);
    $('#ticket-message').modal('show');
    edit_ticket_message_additional.append(hidden_input('type', type));
    edit_ticket_message_additional.append(hidden_input('id', id));
    edit_ticket_message_additional.append(hidden_input('main_ticket', $('input[name="ticketid"]').val()));
}

function init_ticket_edit_editor() {
    if (typeof(ticket_message_editor) !== 'undefined') {
        return true;
    }
    ticket_message_editor = init_editor('.tinymce-ticket-edit');
}
<?php if (has_permission('tasks', '', 'create')) { ?>

function convert_ticket_to_task(id, type) {
    if (type == 'ticket') {
        _ticket_message = $('[data-ticket-id="' + id + '"]').html();
    } else {
        _ticket_message = $('[data-reply-id="' + id + '"]').html();
    }
    var new_task_url = admin_url +
        'tasks/task?rel_id=<?php echo $ticket->ticketid; ?>&rel_type=ticket&ticket_to_task=true';
    new_task(new_task_url);
}
<?php } ?>
</script>
<script type="text/javascript">
$(function() {
    $("#worklog, #user_acceptance").change(function() {
        if ($("#worklog").is(':checked') && $(
                "#user_acceptance").is(':checked')) {
            $('#btncheck').attr('disabled', false);
        } else {
            $('#btncheck').attr('disabled', true);
        }
    });
});
$('#status_top').on('change', function() {
    if (this.value == 3) {
        $('#closure-message').modal({
            show: 'false',
            backdrop: 'static'
        });
        $("#worklog, #user_acceptance").change(function() {
            if ($("#worklog").is(':checked') && $(
                    "#user_acceptance").is(':checked')) {
                $('#btncheck').attr('disabled', false);
            } else {
                $('#btncheck').attr('disabled', true);
            }
        });
    }
    else if(this.value == 5)
    {
         $('#close-message').modal({
            show: 'false',
            backdrop: 'static'
        });
    }
});

function set_contract(data) {
    $("#contract").empty();
    $.each(data, function(index, json) {
        $("#contract").append(`<option value='${data[index].id}'>${data[index].subject}</option>`);
        // console.log($("#contract").next().next().find('.dropdown-menu'));
    });
}

function set_cc_contacts(data) {
    $("#cc_contacts").empty();
    if(data.length > 0){
        $.each(data, function(index, json) {
            selected = false;

            var text = `${data[index].email}`;
            var newOption = new Option(text, `${data[index].id}`, selected, selected);

            $('#cc_contacts').append(newOption).trigger('change');
            $("#cc_contacts").selectpicker("refresh");
        });
    }
}

function set_cc_staffs(data) {
    $("#cc_staffs").empty();
    if(data.length > 0){
        $.each(data, function(index, json) {
            selected = false;

            var text = `${data[index].email}`;
            var newOption = new Option(text, `${data[index].staffid}`, selected, selected);

            $('#cc_staffs').append(newOption).trigger('change');
            $("#cc_staffs").selectpicker("refresh");
        });
    }
}

function set_additional_charge(data) {
    $("#additional_charge").empty();
    $.each(data, function(index, json) {
        $("#additional_charge").append(
            `<option value='${data[index].id}'>${data[index].chargetype}</option>`);
        // console.log($("#contract").next().next().find('.dropdown-menu'));
    });
}
</script>

<script>
function getBillingValue(sel) {
    $('.contractselect').css("display", 'none');
    $('.additional_charges_select').css("display", 'none');
    if ((sel.value) == 'contract') {
        $('.contractselect').css("display", 'block');
    } else if ((sel.value) == 'additional_service') {
        $('.additional_charges_select').css("display", 'block');
    }
}
</script>
</body>

</html>