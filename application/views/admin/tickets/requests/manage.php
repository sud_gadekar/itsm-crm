<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_request_for(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New Request For'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($requests) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Request For Name'); ?></th>
								<th><?php echo _l('options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($requests as $request_for){ ?>
								<tr>
									<td><?php echo $request_for['requestid']; ?></td>
									<td><a href="#" onclick="edit_request_for(this,<?php echo $request_for['requestid']; ?>);return false;" data-name="<?php echo $request_for['name']; ?>"><?php echo $request_for['name']; ?></a></td>
									<td>
										<a href="#" onclick="edit_request_for(this,<?php echo $request_for['requestid']; ?>); return false" data-name="<?php echo $request_for['name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('tickets/delete_request_for/'.$request_for['requestid']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No Request For Found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="request_for" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('tickets/request_for')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Request For Edit'); ?></span>
					<span class="add-title"><?php echo _l('Request For Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('name','Request For Name'); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_ticket_requests);
		$('#request_for').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#request_for input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_ticket_requests(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_request_for(){
		$('#request_for').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_request_for(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#request_for input[name="name"]').val(name);
		$('#request_for').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
