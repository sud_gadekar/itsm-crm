<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    db_prefix() . 'inventory_details.id as id',
    'type',
    'manufacture',
    'product_name',
    'processor',
    'ip_address',
    'serial_no',
    'ip_address',
];

$custom_fields = get_table_custom_fields('inventory_details');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN ' . db_prefix() . 'customfieldsvalues as ctable_' . $key . ' ON ' . db_prefix() . 'inventory_details.id = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}
$join = [];
$where  = [];
$filter = [];
//include_once(APPPATH . 'views/admin/tables/includes/inventory_details_filter.php');

if ($clientid != '') {
    array_push($where, 'AND ' . db_prefix() . 'inventory_details.clientid=' . $this->ci->db->escape_str($clientid));
}

if (!has_permission('inventory_details', '', 'view')) {
    array_push($where, 'AND ' . db_prefix() . 'inventory_details.addedfrom=' . get_staff_user_id());
}

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'inventory_details';

$aColumns = hooks()->apply_filters('inventory_details_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);
$output  = $result['output'];
$rResult = $result['rResult'];

$this->ci->load->model('payment_modes_model');

foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['id'];

    $categoryOutput = '';

    if (is_numeric($clientid)) {
        $categoryOutput = '<a href="' . admin_url('inventory_details/list_inventory_details/' . $aRow['id']) . '">' . $aRow['category_name'] . '</a>';
    } else {
        $categoryOutput = '';
    }

    $categoryOutput .= '<div class="row-options">';


    $categoryOutput .= '<a href="' . admin_url('inventory_details/list_inventory_details/' . $aRow['id']) . '" onclick="init_inventory_detail(' . $aRow['id'] . ');return false;">' . _l('view') . '</a>';

    if (has_permission('inventory_details', '', 'edit')) {
        $categoryOutput .= ' | <a href="' . admin_url('inventory_details/inventory_detail/' . $aRow['id']) . '">' . _l('edit') . '</a>';
    }

    if (has_permission('inventory_details', '', 'delete')) {
        $categoryOutput .= ' | <a href="' . admin_url('inventory_details/delete/' . $aRow['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
    }

    $categoryOutput .= '</div>';
    $row[] = $categoryOutput;

//    $row[] = '<a href="' . admin_url('inventory_details/list_inventory_details/' . $aRow['id']) . '" onclick="init_inventory_detail(' . $aRow['id'] . ');return false;">' . $aRow['inventory_detail_name'] . '</a>';

    $outputReceipt = '';

    if (!empty($aRow['file_name'])) {
        $outputReceipt = '<a href="' . site_url('download/file/inventory_detail/' . $aRow['id']) . '">' . $aRow['file_name'] . '</a>';
    }

    $row[] = $outputReceipt;

//    $row[] = '<a href="' . admin_url('projects/view/' . $aRow['project_id']) . '">' .$aRow['project_name'].'</a>';

//    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';


    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $row['DT_RowClass'] = 'has-row-options';

    $row = hooks()->apply_filters('inventory_details_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}
