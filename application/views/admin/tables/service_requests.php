<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    '1', // bulk actions
   'service_requestid',
    'subject',
    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name',
    // '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM ' . db_prefix() . 'taggables JOIN ' . db_prefix() . 'tags ON ' . db_prefix() . 'taggables.tag_id = ' . db_prefix() . 'tags.id WHERE rel_id = ' . db_prefix() . 'service_requests.service_requestid and rel_type="service_request" ORDER by tag_order ASC) as tags',
    // db_prefix() . 'departments.name as department_name',
    'assigned',
    db_prefix() . 'services.name as service_name',
    'request_for',
    'status',
    'priority',
    'lastreply',
    'is_approved',
    db_prefix() . 'service_requests.date',
    
    ];

$contactColumn = 3;
$tagsColumns   = 3;

$additionalSelect = [
    'adminread',
    'service_requestkey',
    db_prefix() . 'service_requests.userid',
    'statuscolor',
    db_prefix() . 'service_requests.name as service_request_opened_by_name',
    db_prefix() . 'service_requests.email',
    db_prefix() . 'service_requests.userid',
    'assigned',
    db_prefix() . 'clients.company',
    'is_approved','approval_assigned_to_staffid'
    ];

$join = [
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid',
    'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service',
    'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department',
    'LEFT JOIN ' . db_prefix() . 'service_requests_status ON ' . db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status',
    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid',
    'LEFT JOIN ' . db_prefix() . 'service_requests_priorities ON ' . db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority',
    ];

$custom_fields = get_table_custom_fields('service_requests');
foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, 'ctable_' . $key . '.value as ' . $selectAs);
    array_push($join, 'LEFT JOIN ' . db_prefix() . 'customfieldsvalues as ctable_' . $key . ' ON ' . db_prefix() . 'service_requests.service_requestid = ctable_' . $key . '.relid AND ctable_' . $key . '.fieldto="' . $field['fieldto'] . '" AND ctable_' . $key . '.fieldid=' . $field['id']);
}

$where  = [];
$filter = [];

if (isset($userid) && $userid != '') {
    array_push($where, 'AND ' . db_prefix() . 'service_requests.userid = ' . $this->ci->db->escape_str($userid));
} elseif (isset($by_email)) {
    array_push($where, 'AND ' . db_prefix() . 'service_requests.email = "' . $this->ci->db->escape_str($by_email) . '"');
}

if (isset($where_not_service_request_id)) {
    array_push($where, 'AND ' . db_prefix() . 'service_requests.service_requestid != ' . $this->ci->db->escape_str($where_not_service_request_id));
}

if ($this->ci->input->post('project_id')) {
    array_push($where, 'AND project_id = ' . $this->ci->db->escape_str($this->ci->input->post('project_id')));
}

$statuses  = $this->ci->service_requests_model->get_service_request_status();
$_statuses = [];
foreach ($statuses as $__status) {
    if ($this->ci->input->post('service_request_status_' . $__status['service_requeststatusid'])) {
        array_push($_statuses, $__status['service_requeststatusid']);
    }
}
if (count($_statuses) > 0) {
    array_push($filter, 'AND status IN (' . implode(', ', $_statuses) . ')');
}

if ($this->ci->input->post('my_service_requests')) {
    array_push($where, 'OR assigned = ' . get_staff_user_id());
}

$assignees  = $this->ci->service_requests_model->get_service_requests_assignes_disctinct();

$clients    = $this->ci->db->select('tblclients.userid, tblclients.company')
                           ->get('tblclients')->result_array();

$_assignees = [];
$_clients   = [];

$SessionId = $this->ci->session->userdata('staff_user_id');

foreach ($assignees as $__assignee) {
    if ($this->ci->input->post('service_request_assignee_' . $__assignee['assigned'])) {
        array_push($_assignees, $__assignee['assigned']);
    }
}
if (count($_assignees) > 0) {
    array_push($filter, 'AND assigned IN (' . implode(', ', $_assignees) . ')');
}


foreach ($clients as $__client)
{
    if ($this->ci->input->post('service_request_client_' . $__client['userid']))
    {
        array_push($_clients, $__client['userid']);
    }
}
if (count($_clients) > 0)
{
    array_push($filter, 'AND tblclients.userid IN (' . implode(', ', $_clients) . ')');
}



if (count($filter) > 0) {
    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
}

// If userid is set, the the view is in client profile, should be shown all service_requests
if (!is_admin()) {
    if (get_option('staff_access_only_assigned_departments') == 1) {
        $this->ci->load->model('departments_model');
        $staff_deparments_ids = $this->ci->departments_model->get_staff_departments(get_staff_user_id(), true);
        $departments_ids      = [];
        if (count($staff_deparments_ids) == 0) {
            $departments = $this->ci->departments_model->get();
            foreach ($departments as $department) {
                array_push($departments_ids, $department['departmentid']);
            }
        } else {
            $departments_ids = $staff_deparments_ids;
        }
        if (count($departments_ids) > 0) {
            array_push($where, 'AND department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
        }
    }
}

$sIndexColumn = 'service_requestid';
$sTable       = db_prefix() . 'service_requests';

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
            $_data = $aRow[strafter($aColumns[$i], 'as ')];
        } else {
            $_data = $aRow[$aColumns[$i]];
        }

        if ($aColumns[$i] == '1') {
            // print_r($aRow['service_requestid']);die();
            $_data = '<div class="checkbox"><input type="checkbox" value="' . $aRow['service_requestid'] . '"><label></label></div>';
        } 
        
        elseif ($aColumns[$i] == 'service_requestid')
        {
            $url   = admin_url('service_requests/service_request/' . $aRow['service_requestid']);
            $_data = '<a href="' . $url . '" class="valign">' .'SR'.$_data . '</a>';
        }
        elseif ($aColumns[$i] == 'subject' || $aColumns[$i] == 'service_requestid') 
        {
            // service_request is assigned
            if ($aRow['assigned'] != 0) 
            {
                if ($aColumns[$i] != 'service_requestid') {
                    $_data .= '<a href="' . admin_url('profile/' . $aRow['assigned']) . '" data-toggle="tooltip" title="' . get_staff_full_name($aRow['assigned']) . '" class="pull-left mright5">' . staff_profile_image($aRow['assigned'], [
                        'staff-profile-image-xs',
                        ]) . '</a>';
                }
            }
            $url   = admin_url('service_requests/service_request/' . $aRow['service_requestid']);
            $_data = '<a href="' . $url . '" class="valign">'.$_data . '</a>';
            if ($aColumns[$i] == 'subject') 
            {
                $_data .= '<div class="row-options">';
                $_data .= '<a href="' . $url . '">' . _l('view') . '</a>';
                $_data .= ' <span class="text-dark"> | </span><a href="' . $url . '?tab=settings">' . _l('edit') . '</a>';
                $_data .= ' <span class="text-dark"> | </span><a href="' . get_service_request_public_url($aRow) . '" target="_blank">' . _l('view_public_form') . '</a>';
                $_data .= ' <span class="text-dark"> | </span><a href="' . admin_url('service_requests/delete/' . $aRow['service_requestid']) . '" class="text-danger _delete">' . _l('delete') . '</a>';

                

                // if ($SessionId == $aRow['approval_assigned_to_staffid'] && is_null($aRow['is_approved']))
                // {
                //     $_data .= '  <span class="text-dark"> | </span><a title="Approve" href="' . admin_url('service_requests/approve/' . $aRow['service_requestid']) . '">' . '<i class="fa fa-check text-success"></i>' . '</a>';
                    
                //     $_data .= '  <span class="text-dark"> | </span><a title="Reject" href="' . admin_url('service_requests/reject/' . $aRow['service_requestid']) . '">' . '<i class="fa fa-times text-danger"></i>' . '</a>';
                // }

                if(is_staff_logged_in())
                {
                    $_data .= '  <span class="text-dark"> | </span><a title="SR Report" href="' . admin_url('service_requests/convert_to_pdf/' . $aRow['service_requestid']) . '">' . 'SR Report' . '</a>';
                }


                $_data .= '</div>';
            }
        } 

        elseif ($i == $contactColumn) 
        {
            if ($aRow['userid'] != 0) 
            {
                $_data = '<a href="' . admin_url('clients/client/' . $aRow['userid'] . '?group=contacts') . '">' . $aRow['contact_full_name'];
                if (!empty($aRow['company'])) 
                {
                    $_data .= ' (' . $aRow['company'] . ')';
                }
                $_data .= '</a>';
            } 
            else 
            {
                $_data = $aRow['service_request_opened_by_name'];
            }
        }

        elseif ($aColumns[$i] == 'assigned')
        {
                if($aRow['assigned'] != 0)
                {
                    $assigned = $this->ci->db->where('staffid',$aRow['assigned'])->get('tblstaff')->row();
                    $_data = $assigned->firstname.' '.$assigned->lastname;
                }
                else
                {
                     $_data = '';
                }
        }

        elseif ($i == $tagsColumns) {
            $_data = render_tags($_data);
        }  elseif ($aColumns[$i] == 'status') {
            $_data = '<span class="label inline-block service_request-status-' . $aRow['status'] . '" style="border:1px solid ' . $aRow['statuscolor'] . '; color:' . $aRow['statuscolor'] . '">' . service_request_status_translate($aRow['status']) . '</span>';
        } 
        // elseif ($aColumns[$i] == db_prefix() . 'service_requests.date') 
        // {
        //     $_data = 'red';//_dt($_data);
        // } 
       
        elseif ($aColumns[$i] == 'request_for') 
        {
            if(isset($aRow['request_for']))
            {
                $_data = $this->ci->db->where('requestid',$aRow['request_for'])
                                       ->get('tbltickets_request_for')->row()->name;
            }
            else
            {
                $_data = '';
            }    
        }
        elseif ($aColumns[$i] == 'priority') 
        {
            if($aRow['priority'] !=0 )
            {
                 $_data = service_request_priority_translate($aRow['priority']);
            }
            else
            {
                $_data = '';
            }
           
        }   
        elseif ($aColumns[$i] == 'lastreply') 
        {
            if ($aRow[$aColumns[$i]] == null) {
                $_data = _l('service_request_no_reply_yet');
            } else {
                $_data = _dt($aRow[$aColumns[$i]]);
            }
        } 
        elseif($aColumns[$i] == 'is_approved' ) 
        {
            if($aRow['is_approved'] == 1 )
            {
                $_data = '<span class="text-success">Approve</span>';
            }
            elseif($aRow['is_approved'] == null && $aRow['is_approved'] == '')
            {
                $_data = '';
            }
            elseif($aRow['is_approved'] == 0){
                $_data = '<span class="text-danger">Reject</span>';
            }
        }
        else {
            if (strpos($aColumns[$i], 'date_picker_') !== false) {
                $_data = (strpos($_data, ' ') !== false ? _dt($_data) : _d($_data));
            }
        }

        $row[] = $_data;

        if ($aRow['adminread'] == 0) {
            $row['DT_RowClass'] = '';
        }
    }

    if (isset($row['DT_RowClass'])) 
    {
        $row['DT_RowClass'] .= ' has-row-options';
    } 
    else 
    {
        $row['DT_RowClass'] = 'has-row-options';
    }

    $output['aaData'][] = $row;
}
