<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('inventories','','create')){ ?>
                  <a href="<?php echo admin_url('inventory_details/inventory_detail'); ?>" class="btn btn-info">
                    <?php echo 'New Inventory'; ?></a>
                  <?php } else{ ?>
                     <a href="#" class="btn btn-info">
                    <?php echo 'Inventory Details'; ?></a>
                  <?php } ?>
                  <?php /*$this->load->view('admin/inventory_details/filter_by_template'); */?>
                  <a href="#" onclick="slideToggle('#stats-top'); return false;" class="pull-right btn btn-default mleft5 btn-with-tooltip" data-toggle="tooltip" title="<?php echo _l('view_stats_tooltip'); ?>"><i class="fa fa-bar-chart"></i></a>
                  <a href="#" class="btn btn-default pull-right btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-expenses','#expense'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a>
 

                   <?php if(count($clients) > 0 && is_admin()){ ?>
                   <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                    <?php } ?>
                   <hr class="hr-panel-heading" />
                   <div class="row">
                      <div class="col-md-12">
                        <h4 class="no-margin"><?php echo _l('Inventory by Devices'); ?></h4>
                      </div>
                   </div>
                   <div class="row">
                      <?php
                          $where = '';
                          foreach($subtypes as $subtype){
                            $_where = '';
                            if($where == ''){
                              $_where = 'sub_type='.$subtype['id'];
                            } else{
                              $_where = 'sub_type='.$subtype['id'] . ' '.$where;
                            }
                            ?>
                            <?php if(total_rows(db_prefix().'inventory_details',$_where) >0 ){ ?>
                            <div class="col-md-2 col-xs-6 mbot15 border-right">
                              <a href="#" onclick="filterbytype(<?php echo $subtype['id'];?>);return false;">
                                <h3 class="bold"><?php 
                                 echo total_rows(db_prefix().'inventory_details',$_where); 
                                 ?></h3>
                                <span >
                                  <?php echo $subtype['type_name']; ?>
                                </span>
                              </a>
                            </div>
                            <?php } ?>
                    <?php } ?>

                   </div>
                    
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="inventory_details_total"></div>
                  </div>
               </div>
          <!--   </div>
            <div class="row"> -->
               <div class="panel-body">
               <div class="inventory_details_table" id="small-table ">
                 
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                         
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th style="width: 100px;"><?php echo _l(' Inventory Label'); ?></th>
                                <th><?php echo _l('Contact'); ?></th>
                                <th><?php echo _l('Type'); ?></th>
                                <th><?php echo _l('Sub Type'); ?></th>
                                <th><?php echo _l('Product Name'); ?></th>
                                <th><?php echo _l('Host Name'); ?></th>
                                <th><?php echo _l('Action'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($inventory_details_lists) > 0){?>

                                <?php foreach ($inventory_details_lists as $key => $inventory_details_list) { ?>
                                <tr class="has-row-options" data-href="<?php echo admin_url('inventory_details/inventory_detail/').$inventory_details_list['id'] ?>">
                                  <td><?php echo $inventory_details_list['customer_prefix']; ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('inventory_details/inventory_detail/').$inventory_details_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('inventory_details/delete/').$inventory_details_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td><?php echo $inventory_details_list['company']; ?></td>
                                  <td><?php echo $inventory_details_list['type']; ?></td>
                                  <td><?php echo $inventory_details_list['subtype_name']; ?></td>
                                  <td><?php echo $inventory_details_list['product_name'] ?></td>
                                  <td><?php echo $inventory_details_list['hostname'] ?></td>
                                  <td> 
                                        <a href = "#" 
                                          data_company = "<?php echo $inventory_details_list['company']?>"
                                          data_type = "<?php echo $inventory_details_list['type']?>"
                                          data_subtype = "<?php echo $inventory_details_list['subtype_name']?>" 
                                          data_manufacture_name = "<?php echo $inventory_details_list['manufacture_name']?>" 
                                          data_product_name = "<?php echo $inventory_details_list['product_name']?>"
                                          data_hostname = "<?php echo $inventory_details_list['hostname']?>"
                                          data_location = "<?php echo $inventory_details_list['location']?>"
                                          data_status = "<?php echo $inventory_details_list['status_name']?>"
                                          data_processor = "<?php echo $inventory_details_list['processor']?>"
                                          data_ram = "<?php echo $inventory_details_list['ram']?>"
                                          data_storage = "<?php echo $inventory_details_list['storage']?>"
                                          data_ip_address = "<?php echo $inventory_details_list['ip_address']?>" 
                                          data_serial_no = "<?php echo $inventory_details_list['serial_no']?>" 
                                          data_operating_system = "<?php echo $inventory_details_list['operating_system']?>"
                                          data_sw_valid_from = "<?php echo $inventory_details_list['sw_valid_from']?>"
                                          data_sw_valid_to = "<?php echo $inventory_details_list['sw_valid_to']?>" 
                                          data_sw_amount = "<?php echo $inventory_details_list['sw_amount']?>"
                                          data_sw_renewal = "<?php echo $inventory_details_list['sw_renewal']?>"
                                          data_backup_required = "<?php echo $inventory_details_list['backup_required']?>" 
                                          data_subject = "<?php echo $inventory_details_list['subject']?>" 
                                          data-purpose = "<?php echo $inventory_details_list['purpose']?>"
                                          
                                          class="showAssetsOnModal btn btn-default btn-icon">
                                        <i class="fa fa-eye"></i></a> 
                                    </td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                          
                        </div>
                        <?php /*}*/?>

                    
               </div>
             <!--   <div class="col-md-7 small-table-right-col">
                  <div id="inventory_detail" class="hide">
                  </div>
               </div> -->
             </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="inventory_detail_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="inventory_detail_convert_invoice_type_1" value="save_as_draft_false" name="inventory_detail_convert_invoice_type">
               <label for="inventory_detail_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="inventory_detail_convert_invoice_type_2" value="save_as_draft_true" name="inventory_detail_convert_invoice_type">
               <label for="inventory_detail_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('inventory_detail_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('inventory_detail_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('inventory_detail'); ?> <?php echo _l('inventory_detail_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('inventory_detail'); ?> <?php echo _l('inventory_detail_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="inventory_detail_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="assetmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <div class="assetDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 



<?php init_tail(); ?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".has-row-options").click(function() {
          window.location = $(this).data("href");
      });
  });
  
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>inventory_details/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
                $('.table tbody').empty();
                $('.table tbody').html(data);
            }
           }
      });
  }
</script>



<!-- Show Asset Data On Modal -->
  <script type="text/javascript">
    $(".showAssetsOnModal").on("click", function(){
        var data_company = $(this).attr("data_company");
        var data_type = $(this).attr("data_type");
        var data_subtype = $(this).attr("data_subtype");
        var data_manufacture_name = $(this).attr("data_manufacture_name");
        var data_product_name = $(this).attr("data_product_name");
        var data_hostname = $(this).attr("data_hostname");
        var data_location = $(this).attr("data_location");
        var data_processor =$(this).attr("data_processor");
        var data_status = $(this).attr("data_status");
        var data_ram = $(this).attr("data_ram");
        var data_storage = $(this).attr("data_storage");
        var data_ip_address = $(this).attr("data_ip_address");
        var data_serial_no = $(this).attr("data_serial_no");
        var data_operating_system = $(this).attr("data_operating_system");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");
        var data_sw_valid_to = $(this).attr("data_sw_valid_to");
        var data_sw_amount = $(this).attr("data_sw_amount");
        var data_sw_renewal = $(this).attr("data_sw_renewal");
        var data_backup_required = $(this).attr("data_backup_required");
        var data_subject = $(this).attr("data_subject");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");         
        var data_firstname = $(this).attr("data_firstname");
        var data_lastname = $(this).attr("data_lastname");
        var data_purpose = $(this).attr("data_purpose");
        //var contact  =  data_firstname +' '+ data_lastname;           
        var BackupStatus = '';
        if(data_backup_required == 1){
          BackupStatus = "Yes";
        }   
        else if(data_backup_required == 0)
        {
          BackupStatus = "No";
        }
                              
          var table_header = "<table class='table table-striped table-bordered'><tbody>";
          var table_footer = "</tbody></table>";
          var html ="";

          html += "<tr><td>"+'Customer'+"</td><td>"+data_company+"</td></tr>";
          html += "<tr><td>"+'Type'+"</td><td>"+data_type+"</td></tr>";
          html += "<tr><td>"+'Sub type'+"</td><td>"+data_subtype+"</td></tr>";
          html += "<tr><td>"+'Manufacture'+"</td><td>"+data_manufacture_name+"</td></tr>";
          html += "<tr><td>"+'Product'+"</td><td>"+data_product_name+"</td></tr>";
          html += "<tr><td>"+'Status'+"</td><td>"+data_status+"</td></tr>";
          html += "<tr><td>"+'Host name'+"</td><td>"+data_hostname+"</td></tr>";
          html += "<tr><td>"+'Location'+"</td><td>"+data_location+"</td></tr>";
          if(data_type != 'Software')
          {
            html += "<tr><td>"+'Processor'+"</td><td>"+data_processor+"</td></tr>";
            html += "<tr><td>"+'RAM'+"</td><td>"+data_ram+"</td></tr>";
            html += "<tr><td>"+'Storage'+"</td><td>"+data_storage+"</td></tr>";
            html += "<tr><td>"+'IP'+"</td><td>"+data_ip_address+"</td></tr>";
            html += "<tr><td>"+'Serial number'+"</td><td>"+data_serial_no+"</td></tr>";
            html += "<tr><td>"+'OS'+"</td><td>"+data_operating_system+"</td></tr>";
            html += "<tr><td>"+'Backup required'+"</td><td>"+BackupStatus+"</td></tr>";
            html += "<tr><td>"+'Contract'+"</td><td>"+data_subject+"</td></tr>";
            html += "<tr><td>"+'Purpose'+"</td><td>"+data_purpose+"</td></tr>";
          }
          if(data_type == 'Software')
          {
            html += "<tr><td>"+'From '+"</td><td>"+data_sw_valid_from+"</td></tr>";
            html += "<tr><td>"+'To'+"</td><td>"+data_sw_valid_to+"</td></tr>";
            html += "<tr><td>"+'Amount'+"</td><td>"+data_sw_amount+"</td></tr>";
            html += "<tr><td>"+'Renewal'+"</td><td>"+data_sw_renewal+"</td></tr>";
          }
          //html += "<tr><td>"+'Contact'+"</td><td>"+contact+"</td></tr>";    
          var all = table_header +html+ table_footer;

          $('#assetmodal .assetDetailsData').html(all); 
          $('#assetmodal').modal('show'); 
        
    });
  </script>
  <script type="text/javascript">
    function filterbytype($id) {
       if($id){
          $.ajax({
                   url:'<?=admin_url()?>inventory_details/index',
                   method:"POST",
                   data:{id:$id},
                   success:function(data){
                    if(data){
                      $('.table tbody').empty();
                      $('.table tbody').html(data);
                    }
                   }
          });
        }
    }
  </script>

</body>
</html>
