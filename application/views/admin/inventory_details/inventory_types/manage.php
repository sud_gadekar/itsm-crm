<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_inventory_type(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New inventory type'); ?></a>
							&nbsp;&nbsp;&nbsp;
							<a href="#" style="margin-left: 5px;" onclick="new_inventory_subtype(); return false;" class="btn btn-info pull-left display-block ml-3"><?php echo _l('New inventory subtype'); ?></a></br></br></br>

						</div>

						
						<?php if(count($inventory_types) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Inventory Type Name'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($inventory_types as $inventory_type){ ?>
								<tr>
									<td><?php echo $inventory_type['id']; ?></td>
									<td>
									  <a href="#" 
										onclick="edit_inventory_type(this,<?php echo $inventory_type['id']; ?>);return false;" data-name="<?php echo $inventory_type['type_name']; ?>">
											<?php echo $inventory_type['type_name']; ?>
									  </a>
									</td>
									<td>
										<a href="#" onclick="edit_inventory_type(this,<?php echo $inventory_type['id']; ?>); return false" data-name="<?php echo $inventory_type['type_name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No inventory type found'); ?></p>
						<?php } ?>


						<!-- Inventory sub type -->
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($inventory_subtypes) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Inventory Sub Type Name'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($inventory_subtypes as $key => $inventory_subtype){ ?>
								<tr>
									<td><?php echo $key + 1; ?></td>
									<td>
									  <a href="#" 
										onclick="edit_inventory_type(this,<?php echo $inventory_subtype['id']; ?>);return false;" data-name="<?php echo $inventory_subtype['type_name']; ?>"
										
										
										>
											<?php echo $inventory_subtype['type_name']; ?>
									  </a>
									</td>
									<td>
										<a href="#" 
										onclick="edit_inventory_subtype(this,<?php echo $inventory_subtype['id']; ?>); return false" data-name="<?php echo $inventory_subtype['type_name']; ?>" 
										<?php foreach ($inventory_types as $key => $value) {
											if($value['id'] == $inventory_subtype['parent_id'])
											{
												?>data-type="<?php echo $value['id'];?>"

											<?php }	
										}?>
										class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('inventory_details/delete_inventory_subtype/'.$inventory_subtype['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else{ ?>
							<p class="no-margin"><?php echo _l('No inventory sub type found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Type Modal -->

<div class="modal fade" id="inventory_type" tabindex="-1" inventory_type="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('inventory_details/inventory_type')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Inventory type edit'); ?></span>
					<span class="add-title"><?php echo _l('Inventory type add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group">
							<label for="type_name" class="control-label"> 
								<small class="req text-danger">* </small>Inventory Type Name</label>
								<input type="text" id="type_name" name="type_name" class="form-control" value="" required>
							</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- SubType Modal -->

<div class="modal fade" id="inventory_subtype" tabindex="-1" inventory_type="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('inventory_details/inventory_subtype')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Inventory sub type edit'); ?></span>
					<span class="add-title"><?php echo _l('Inventory sub type add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group select-placeholder">
	                        <label for=""><?php echo _l('Inventory type'); ?></label>
	                        <select class="selectpicker" name="parent_id" data-width="100%" data-live-search="true" required>
	                          <?php foreach ($inventory_types as $inventory_type): ?>
			                    <option value="<?= $inventory_type['id']; ?>" ><?= $inventory_type['type_name']; ?></option>
			                  <?php endforeach; ?>
	                        </select>
	                     </div>
						<div class="form-group">
							<label for="type_name" class="control-label"> Inventory sub type name</label>
								<input type="text" id="type_name" name="type_name" class="form-control" value="" required>
								 <input type="hidden" id="id" name="id" class="form-control" 
								value="<?php if(isset($inventory_subtype['id'])){
									echo $inventory_subtype['id']; }?>"> 
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_inventory_types);
		$('#inventory_type').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#inventory_type input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_inventory_types(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_inventory_type(){
		$('#inventory_type').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_inventory_type(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#inventory_type input[name="type_name"]').val(name);
		$('#inventory_type').modal('show');
		$('.add-title').addClass('hide');
	}
</script>

<!-- Sub Type Script -->
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_inventory_subtypes);
		$('#inventory_subtype').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#inventory_subtype input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_inventory_subtypes(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_inventory_subtype(){
		$('#inventory_subtype').modal('show');
		$('.edit-title').addClass('hide');
		$('#id').val('');
	}
	function edit_inventory_subtype(invoker,id){
		var name = $(invoker).data('name');
		var type = $(invoker).data('type');

		console.log(id);

		$('#additional').append(hidden_input('id',id));
		$('#inventory_subtype input[name="type_name"]').val(name);
		$('#inventory_subtype select > option').filter('[value='+type+']').attr('selected','selected');
		$('#inventory_subtype #id').val(id);
		$('.selectpicker').selectpicker('refresh')
		//$('#inventory_subtype > option:selected').text(type);

		$('#inventory_subtype').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
