<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'inventory_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
          <!-- <button class="btn btn-info save-and-add-contact customer-form-submiter">Save </button> -->
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Inventory </h4>
                  <div class="row">
                    <div class="additional"></div>
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="type"><?php echo _l('Type'); ?></label>
                            <input type="hidden" name="id" 
                            value=<?php if(isset($inventory_details->id)) {
                             echo $inventory_details->id; } ?>>
                            <select class="selectpicker" name="type" id="type"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Type</option>
                                <?php foreach($parent_types as  $parent_type){ ?>
                                    <option value="<?php echo $parent_type['id']?>" 
                                      <?php if(isset($inventory_details->type)){
                                        echo ($inventory_details->type == $parent_type['id']) ? 'selected' : '';}?>>
                                      <?php echo $parent_type['type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="sub_type" class="control-label">
                                <?php echo _l('Sub Type'); ?>
                            </label>
                            <select name="sub_type" id="sub_type" 
                            value="<?php if(isset($inventory_details->sub_type)){
                              echo $inventory_details->sub_type;
                            }?>" class="form-control"></select>
                        </div>
                    </div>
                      <input type="hidden" name="subtypeid" id="subtypeid" disabled value="<?php if(isset($inventory_details->sub_type)){
                              echo $inventory_details->sub_type;
                            }?>">
                  </div>
                  <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group select-placeholder">
                            <label for="type"><?php echo _l('Manufacture'); ?></label>
                            <select class="selectpicker" name="manufacture" id="manufacture" data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Manufacture</option>
                                <?php foreach($inventory_manufactures as $inventory_manufacture){ ?>
                                    <option value="<?php echo $inventory_manufacture['id']?>"
                                      <?php if(isset($inventory_details->manufacture)){
                                        echo ($inventory_details->manufacture == $inventory_manufacture['id']) ? 'selected' : '';}?>>
                                      <?php echo $inventory_manufacture['manufacture_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="product_name" class="control-label">
                                <?php echo _l('Product name'); ?>
                            </label>
                            <div class="input-group">
                              <select class="form-control" name="product_name" id="product_name">
                          
                              </select>            
                              <!-- <span class="input-group-btn">
                                <a href="#" onclick="new_inventory_product();return false;" class="btn btn-default" type="button" tabindex="-1">
                                  <span class="fa fa-plus" aria-hidden="true"></span>
                                </a>
                              </span> -->
                              <div class="input-group-addon" style="opacity: 1;">
                                <a href="#" onclick="new_inventory_product();return false;">
                                  <i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="productid" id="productid" disabled value="<?php if(isset($inventory_details->product_name)){
                              echo $inventory_details->product_name;
                            }?>"> 


                  <!-- Product Add -->
                  <!-- <div class="col-md-6">
                    <div class="form-group form-group-select-input-service input-group-select">
                      <label for="product_name" class="control-label">Product</label>
                        <div class="input-group input-group-select select-service">
                        <div class="dropdown bootstrap-select input-group-btn _select_input_group bs3 bs3-has-addon" style="width: 100%;">
                            <select id="product_name" name="product_name" class="selectpicker _select_input_group" data-width="100%"           data-none-selected-text="Nothing selected" data-live-search="true" tabindex="-98">
                            </select>
                        </div>
                        <div class="input-group-addon" style="opacity: 1;">
                          <a href="#" onclick="new_inventory_product();return false;">
                            <i class="fa fa-plus"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
 -->


                  <!-- End -->


                  </div>
                  <div class="row">          
                   <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="status"><?php echo _l('Status'); ?></label>
                            <select class="selectpicker" name="status" id="status"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Status</option>
                                <?php foreach($inventory_statuses as  $inventory_status){ ?>
                                    <option value="<?php echo $inventory_status['id']?>" 
                                      <?php if(isset($inventory_details->status)){
                                        echo ($inventory_details->status == $inventory_status['id']) ? 'selected' : '';}?>>
                                      <?php echo $inventory_status['status_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="hostname"><?php echo _l('Hostname'); ?></label>
                            <input type="text" name="hostname" id="hostname" 
                            value="<?php if(isset($inventory_details->hostname)){
                              echo $inventory_details->hostname;}?>" class="form-control" required>
                        </div>
                    </div>

                   
                  </div>
                  <div class="row">
                     <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="processor"><?php echo _l('Processor'); ?></label>
                            <input type="text" name="processor" id="processor" 
                            value="<?php if(isset($inventory_details->processor)){
                              echo $inventory_details->processor; }?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="ram"><?php echo _l('RAM'); ?></label>
                            <input type="text" name="ram" id="ram" 
                            value="<?php if(isset($inventory_details->ram)){ 
                              echo $inventory_details->ram; } ?>" class="form-control">
                        </div>
                    </div>

                    
                  </div>
                  <div class="row">

                    <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="ip_address"><?php echo _l('IP Address'); ?></label>
                            <input type="text" name="ip_address" id="ip_address" 
                            value="<?php if(isset($inventory_details->ip_address)){
                              echo $inventory_details->ip_address;}?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="serial_no"><?php echo _l('Serial No'); ?></label>
                            <input type="text" name="serial_no" id="serial_no" 
                            value="<?php if(isset($inventory_details->serial_no)){
                              echo $inventory_details->serial_no;}?>" class="form-control">
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="storage"><?php echo _l('Storage'); ?></label>
                            <input type="text" name="storage" id="storage" 
                            value="<?php if(isset($inventory_details->storage)){ 
                              echo $inventory_details->storage;}?>" class="form-control">
                        </div>
                    </div>


                    <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="operating_system"><?php echo _l('Operating System'); ?></label>
                            <input type="text" name="operating_system" id="operating_system" 
                            value="<?php if(isset($inventory_details->operating_system)){
                              echo $inventory_details->operating_system;}?>" class="form-control">
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                              <label for="clientid" class="control-label">
                                  <?php echo _l('Assigned to ( Customer )'); ?>
                                </label>
                             
                              <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Customer</option>
                                <?php foreach($clients as  $client){ ?>
                                    <option value="<?php echo $client['userid']?>" 
                                      <?php if(isset($inventory_details->clientid)){
                                        echo ($inventory_details->clientid == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                    <span class="">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label for="location" class="control-label">
                                    <?php echo _l('Location'); ?>
                                </label>
                                <select name="location" id="location" class="form-control"></select>
                            </div>
                        </div>
                        <input type="hidden" name="locationid" id="locationid" disabled 
                               value="<?php if(isset($inventory_details->location)){
                                  echo $inventory_details->location;
                              }?>">
                    </span>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="support_contract" class="control-label">
                                <?php echo _l('Contract'); ?>
                            </label>
                            <select name="support_contract" id="support_contract" class="form-control"></select>
                        </div>
                    </div>

                     <input type="hidden" name="contractid" id="contractid" disabled 
                         value="<?php if(isset($inventory_details->support_contract)){
                            echo $inventory_details->support_contract;
                     }?>">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="support_contact" class="control-label">
                                <?php echo _l('Contact'); ?>
                            </label>
                            <select name="contact" id="contact" class="form-control"></select>
                        </div>
                    </div>

                     <input type="hidden" name="contactid" id="contactid" disabled 
                         value="<?php if(isset($inventory_details->contact)){
                            echo $inventory_details->contact;
                     }?>">
                   </div>
                   <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="purpose">Purpose</label>
                            <input type="text" name="purpose" id="purpose" 
                            value="<?php if(isset($inventory_details->purpose)){echo $inventory_details->purpose;}?>" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="notes" class="control-label">Note</label>
                            <textarea id="notes" name="notes" class="form-control" rows="4"><?php if(isset($inventory_details->notes)){echo $inventory_details->notes;}?>
                            </textarea>
                          </div>                    
                      </div>
                   </div>

                   <div class="row">
                    <!--  <div class="col-md-6 show_software_type_hide_other">
                        <div class="form-group">
                            <label for="customer_prefix">Customer Prefix</label>
                            <input type="text" name="customer_prefix" id="customer_prefix" 
                              value="<?php if(isset($inventory_details->customer_prefix)){
                                        echo $inventory_details->customer_prefix;                                
                              }?>" class="form-control" readonly>
                        </div>
                    </div> -->
                     <input type="hidden" name="customer_prefix" id="customer_prefix" 
                              value="<?php if(isset($inventory_details->customer_prefix)){
                                        echo $inventory_details->customer_prefix;                                
                              }?>" class="form-control" readonly>

                    <span class="" >
                        <div class="col-md-6 show_software_type_hide_other">
                           <div class="form-check form-check-inline">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="backup_required">Backup Required</label>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-check-input" type="radio" name="backup_required"
                                            id="backup_required" value="1"
                                            <?php if(isset($inventory_details->backup_required)){
                                              echo ($inventory_details->backup_required == 1 )?'checked' : ''; }?>>&nbsp;&nbsp;Yes
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-check-input" type="radio" name="backup_required"
                                            id="backup_required" value="0"
                                            <?php if(isset($inventory_details->backup_required)){
                                              echo ($inventory_details->backup_required == 0 )?'checked' : ''; }?>>&nbsp;&nbsp;No
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                    <div class="col-md-6 show_software_type" style="display: none">
                      <div class="form-group">
                        <label for="sw_valid_from" class="control-label">From</label>
                          <div class="input-group date">
                            <input type="text" id="sw_valid_from" name="sw_valid_from" class="form-control datepicker" 
                                   value="<?php if(isset($inventory_details->sw_valid_from)){
                                    echo $inventory_details->sw_valid_from;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="col-md-6 show_software_type" style="display: none;">
                      <div class="form-group">
                        <label for="sw_valid_to" class="control-label">To</label>
                          <div class="input-group date">
                            <input type="text" id="sw_valid_to" name="sw_valid_to" class="form-control datepicker" 
                                   value="<?php if(isset($inventory_details->sw_valid_to)){
                                    echo $inventory_details->sw_valid_to;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>

                     <div class="col-md-6 show_software_type" style="display: none;">
                        <div class="form-group">
                            <label for="sw_amount"><?php echo _l('Amount'); ?></label>
                            <input type="text" name="sw_amount" id="sw_amount" 
                            value="<?php if(isset($inventory_details->sw_amount)){
                              echo $inventory_details->sw_amount;}?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6 show_software_type" style="display: none;">
                      <div class="form-group">
                        <label for="sw_renewal" class="control-label">Renewal</label>
                          <div class="input-group date">
                            <input type="text" id="sw_renewal" name="sw_renewal" class="form-control datepicker" 
                                   value="<?php if(isset($inventory_details->sw_renewal)){
                                    echo $inventory_details->sw_renewal;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
           <?php echo form_close(); ?>
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>
  <?php /* $this->load->view('admin/inventory_details/inventory_detail_category'); /*/?>
  <?php init_tail(); ?>


  <script type="text/javascript">
  jQuery(function($) {
    $('#type').change(function(){ 
        var value = $(this).val();
        var typeValue = $.trim($("#type option:selected").text());
        if(value){
          $('.show_software_type_hide_other').css('display','block');
          $('.show_software_type').css('display','none');
          $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_sub_type',
                   method:"POST",
                   data:{id:value},
                   success:function(data){
                    $("#sub_type").empty();
                      if(JSON.parse(data).length > 0){
                      $('.sub_typeselect').css('display','block');
                          $.each(JSON.parse(data), function(index, json) {
                              $("#sub_type").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id==$('#subtypeid').val())?'selected' : ''}>${JSON.parse(data)[index].type_name}</option>`);
                          });
                      }
                      else{
                        $('.sub_typeselect').css('display','none');
                      }
                   }
          });
          //alert(typeValue);
          if(typeValue == 'Software' ){

            $('.show_software_type_hide_other').css('display','none');
            $('.show_software_type').css('display','block');
          }
        }
    }).trigger('change');

    //Get Product against manufacturer

    $('#manufacture').change(function(){ 
        var value = $(this).val();
        if(value){
          $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_product',
                   method:"POST",
                   data:{id:value},
                   success:function(data){
                    $("#product_name").empty();
                      if(JSON.parse(data).length > 0){
                      $('.sub_typeselect').css('display','block');
                          $.each(JSON.parse(data), function(index, json) {
                              $("#product_name").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id==$('#productid').val())?'selected' : ''}>${JSON.parse(data)[index].product_name}</option>`);
                          });
                      }
                      else{
                        $('.sub_typeselect').css('display','none');
                      }
                   }
          });

          // if(value == 5 ){
          //   $('.show_software_type_hide_other').css('display','none');
          //   $('.show_software_type').css('display','block');
          // }
        }
    }).trigger('change');

  });

 jQuery(function($) {
    $('body.inventory_detail select[name="clientid"]').on('change', function() {
         var clientid = $(this).val();
         if(clientid != ''){
              $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_client_location',
                   method:"POST",
                   data:{id:clientid},
                   success:function(data){
                    $("#location").empty();
                      if(JSON.parse(data).length > 0){
                          $.each(JSON.parse(data), function(index, json) {
                              $("#location").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id==$('#locationid').val())?'selected' : ''}>${JSON.parse(data)[index].location}</option>`);
                          });
                      }
                   }
              });

              $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_client_contract',
                   method:"POST",
                   data:{id:clientid},
                   success:function(data){
                    $("#support_contract").empty();
                      if(JSON.parse(data).length > 0){
                          $.each(JSON.parse(data), function(index, json) {
                              $("#support_contract").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id==$('#contractid').val())?'selected' : ''}>${JSON.parse(data)[index].subject}</option>`);
                          });
                      }
                   }
              });

               $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_client_contact',
                   method:"POST",
                   data:{id:clientid},
                   success:function(data){
                    $("#contact").empty();
                      if(JSON.parse(data).length > 0){
                          $.each(JSON.parse(data), function(index, json) {
                              $("#contact").append(`<option value='${JSON.parse(data)[index].id}'
                                ${(JSON.parse(data)[index].id == $('#contactid').val())?'selected' : ''}>${JSON.parse(data)[index].firstname +' '+ JSON.parse(data)[index].lastname}</option>`);
                          });
                      }
                   }
              });
         }
    }).trigger('change');

     
  });


  jQuery(function($) {
     $('body.inventory_detail select[name="clientid"]').on('change', function() {
         var clientid = $(this).val();
         if(clientid != '')
         {
             $.ajax({
                   url:'<?=admin_url()?>inventory_details/get_client_prefix',
                   method:"POST",
                   data:{id:clientid},
                   success:function(data){
                      if(JSON.parse(data).length > 0){
                          $.each(JSON.parse(data), function(index, json) {
                              console.log(JSON.parse(data));
                              $('#customer_prefix').val(JSON.parse(data)[index].company_prefix);
                          });
                      }
                   }
              });
         }
      });   
  });


  </script>


  <script type="text/javascript">
    function new_product(){
        $('#product-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
  </script>

  <script>
   $(function(){
     appValidateForm($('#inventory_form'),
      {
        type:'required',
        manufacture:'required',
        clientid:'required'
      });
   });
</script>


<script type="text/javascript">
  window.onbeforeunload = null;
  function new_inventory_product(){
    $('#inventory_product').modal('show');
    $('.edit-title').addClass('hide');
  }
</script>
<script>
   $(function(){
     appValidateForm($('#inventory_product_form'),
      { manufacture_id:'required',
        product_name:'required',
      });
   });
</script>
<script type="text/javascript">
  $("#inventory_product_form").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               window.location.reload();
           }
         });

    
});
</script>



<div class="modal fade" id="product-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('expenses/category'),array('id'=>'expense-category-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('edit_expense_category'); ?></span>
                    <span class="add-title"><?php echo _l('new_expense_category'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('name','expense_add_edit_name'); ?>
                        <?php echo render_textarea('description','expense_add_edit_description'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- product Modal -->

<div class="modal fade" id="inventory_product" tabindex="-1" inventory_product="dialog">
  <div class="modal-dialog">
    <?php echo form_open(admin_url('inventory_details/inventory_product_from_add'),array('id'=>'inventory_product_form')); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button product="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
          <span class="edit-title"><?php echo _l('Inventory product edit'); ?></span>
          <span class="add-title"><?php echo _l('Inventory product add'); ?></span>
        </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group select-placeholder">
                <label for="manufacture_id"><?php echo _l('Inventory manufacture'); ?></label>
                <select class="selectpicker" id="manufacture_id" name="manufacture_id" data-width="100%">
                  <option disabled value="" selected="">Select Manufature</option>
                  <?php foreach ($inventory_manufactures as $inventory_manufacture): ?>
                <option 
                value="<?= $inventory_manufacture['id']; ?>" ><?= $inventory_manufacture['manufacture_name']; ?></option>
              <?php endforeach; ?>
                </select>
             </div>
          </div>

          <div class="col-md-12">
            <div id="additional"></div>
            <div class="form-group">
              <label for="product_name" class="control-label"> 
                <small class="req text-danger">* </small>Inventory product Name</label>
                <input product="text" id="product_name" name="product_name" class="form-control" value="">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button product="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
        <button product="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
      </div>
    </div><!-- /.modal-content -->
    <?php echo form_close(); ?>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>