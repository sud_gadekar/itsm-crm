
  
    <?php if(count($inventory_details_lists) > 0){?>
    <?php foreach ($inventory_details_lists as $key => $inventory_details_list) { ?>
    <tr class="has-row-options">
      <td><?php echo $inventory_details_list['customer_prefix'];  ?>
        <div class="row-options">
          
          <a href="<?php echo admin_url('inventory_details/inventory_detail/').$inventory_details_list['id'] ?>">Edit </a>
          <span class="text-dark"> | </span>
          <a
          href="<?php echo admin_url('inventory_details/delete/').$inventory_details_list['id'] ?>" class="text-danger _delete">Delete </a>
        </td>
        <td><?php echo $inventory_details_list['company']; ?></td>
        <td><?php echo $inventory_details_list['type'] ?></td>
        <td><?php echo $inventory_details_list['subtype_name'] ?></td>
        <td><?php echo $inventory_details_list['product_name'] ?></td>
        <td><?php echo $inventory_details_list['hostname'] ?></td>
        <td> 
              <a href = "#" 
                data_company = "<?php echo $inventory_details_list['company']?>"
                data_type = "<?php echo $inventory_details_list['type']?>"
                data_subtype = "<?php echo $inventory_details_list['subtype_name']?>" 
                
                data_product_name = "<?php echo $inventory_details_list['product_name']?>"
                data_hostname = "<?php echo $inventory_details_list['hostname']?>"
                data_location = "<?php echo $inventory_details_list['location']?>"
                
                data_processor = "<?php echo $inventory_details_list['processor']?>"
                data_ram = "<?php echo $inventory_details_list['ram']?>"
                data_storage = "<?php echo $inventory_details_list['storage']?>"
                data_ip_address = "<?php echo $inventory_details_list['ip_address']?>" 
                data_serial_no = "<?php echo $inventory_details_list['serial_no']?>" 
                data_operating_system = "<?php echo $inventory_details_list['operating_system']?>"
                data_sw_valid_from = "<?php echo $inventory_details_list['sw_valid_from']?>"
                data_sw_valid_to = "<?php echo $inventory_details_list['sw_valid_to']?>" 
                data_sw_amount = "<?php echo $inventory_details_list['sw_amount']?>"
                data_sw_renewal = "<?php echo $inventory_details_list['sw_renewal']?>"
                data_backup_required = "<?php echo $inventory_details_list['backup_required']?>" 
                data_subject = "<?php echo $inventory_details_list['subject']?>" 
                data-purpose = "<?php echo $inventory_details_list['purpose']?>"
                
                class="showAssetsOnModal btn btn-default btn-icon">
              <i class="fa fa-eye"></i></a> 
          </td>
      </tr>
      <?php }  ?>
      <?php } else { ?>
      <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No entries found</td></tr>
      <?php }?>
    

  
<div class="modal fade" id="assetmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <div class="assetDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

  <script type="text/javascript">
    $(".showAssetsOnModal").on("click", function(){
        var data_company = $(this).attr("data_company");
        var data_type = $(this).attr("data_type");
        var data_subtype = $(this).attr("data_subtype");
        var data_manufacture_name = $(this).attr("data_manufacture_name");
        var data_product_name = $(this).attr("data_product_name");
        var data_hostname = $(this).attr("data_hostname");
        var data_location = $(this).attr("data_location");
        var data_processor =$(this).attr("data_processor");
        var data_status = $(this).attr("data_status");
        var data_ram = $(this).attr("data_ram");
        var data_storage = $(this).attr("data_storage");
        var data_ip_address = $(this).attr("data_ip_address");
        var data_serial_no = $(this).attr("data_serial_no");
        var data_operating_system = $(this).attr("data_operating_system");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");
        var data_sw_valid_to = $(this).attr("data_sw_valid_to");
        var data_sw_amount = $(this).attr("data_sw_amount");
        var data_sw_renewal = $(this).attr("data_sw_renewal");
        var data_backup_required = $(this).attr("data_backup_required");
        var data_subject = $(this).attr("data_subject");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");         
        var data_firstname = $(this).attr("data_firstname");
        var data_lastname = $(this).attr("data_lastname");
        var data_purpose = $(this).attr("data_purpose");
        //var contact  =  data_firstname +' '+ data_lastname;           
        var BackupStatus = '';
        if(data_backup_required == 1){
          BackupStatus = "Yes";
        }   
        else if(data_backup_required == 0)
        {
          BackupStatus = "No";
        }
                              
          var table_header = "<table class='table table-striped table-bordered'><tbody>";
          var table_footer = "</tbody></table>";
          var html ="";

          html += "<tr><td>"+'Customer'+"</td><td>"+data_company+"</td></tr>";
          html += "<tr><td>"+'Type'+"</td><td>"+data_type+"</td></tr>";
          html += "<tr><td>"+'Sub type'+"</td><td>"+data_subtype+"</td></tr>";
          //html += "<tr><td>"+'Manufacture'+"</td><td>"+data_manufacture_name+"</td></tr>";
          html += "<tr><td>"+'Product'+"</td><td>"+data_product_name+"</td></tr>";
          //html += "<tr><td>"+'Status'+"</td><td>"+data_status+"</td></tr>";
          html += "<tr><td>"+'Host name'+"</td><td>"+data_hostname+"</td></tr>";
          html += "<tr><td>"+'Location'+"</td><td>"+data_location+"</td></tr>";
          if(data_type != 'Software')
          {
            html += "<tr><td>"+'Processor'+"</td><td>"+data_processor+"</td></tr>";
            html += "<tr><td>"+'RAM'+"</td><td>"+data_ram+"</td></tr>";
            html += "<tr><td>"+'Storage'+"</td><td>"+data_storage+"</td></tr>";
            html += "<tr><td>"+'IP'+"</td><td>"+data_ip_address+"</td></tr>";
            html += "<tr><td>"+'Serial number'+"</td><td>"+data_serial_no+"</td></tr>";
            html += "<tr><td>"+'OS'+"</td><td>"+data_operating_system+"</td></tr>";
            html += "<tr><td>"+'Backup required'+"</td><td>"+BackupStatus+"</td></tr>";
            html += "<tr><td>"+'Contract'+"</td><td>"+data_subject+"</td></tr>";
            //html += "<tr><td>"+'Purpose'+"</td><td>"+data_purpose+"</td></tr>";
          }
          if(data_type == 'Software')
          {
            html += "<tr><td>"+'From '+"</td><td>"+data_sw_valid_from+"</td></tr>";
            html += "<tr><td>"+'To'+"</td><td>"+data_sw_valid_to+"</td></tr>";
            html += "<tr><td>"+'Amount'+"</td><td>"+data_sw_amount+"</td></tr>";
            html += "<tr><td>"+'Renewal'+"</td><td>"+data_sw_renewal+"</td></tr>";
          }
          //html += "<tr><td>"+'Contact'+"</td><td>"+contact+"</td></tr>";    
          var all = table_header +html+ table_footer;

          $('#assetmodal .assetDetailsData').html(all); 
          $('#assetmodal').modal('show'); 
        
    });
  </script>
  
