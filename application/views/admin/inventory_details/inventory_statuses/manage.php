<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_inventory_statuse(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New inventory status'); ?></a>
							

						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($inventory_statuses) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Inventory status'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($inventory_statuses as $key => $inventory_statuse){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td>
									  <a href="#" 
										onclick="edit_inventory_statuse(this,<?php echo $inventory_statuse['id']; ?>);return false;" data-name="<?php echo $inventory_statuse['status_name']; ?>">
											<?php echo $inventory_statuse['status_name']; ?>
									  </a>
									</td>
									<td>
										<a href="#" onclick="edit_inventory_statuse(this,<?php echo $inventory_statuse['id']; ?>); return false" data-name="<?php echo $inventory_statuse['status_name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										 <a href="<?php  echo admin_url('inventory_details/delete_inventory_statuse/'.$inventory_statuse['id']);  ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> 
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- statuse Modal -->

<div class="modal fade" id="inventory_statuse" tabindex="-1" inventory_statuse="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('inventory_details/inventory_statuse')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button statuse="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Inventory Status Edit'); ?></span>
					<span class="add-title"><?php echo _l('Inventory Status Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group">
							<label for="status_name" class="control-label"> 
								<small class="req text-danger">* </small>Inventory status</label>
								<input statuse="text" id="status_name" name="status_name" class="form-control" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button statuse="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button statuse="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_inventory_statuses);
		$('#inventory_statuse').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#inventory_statuse input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_inventory_statuses(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_inventory_statuse(){
		$('#inventory_statuse').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_inventory_statuse(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#inventory_statuse input[name="status_name"]').val(name);
		$('#inventory_statuse').modal('show');
		$('.add-title').addClass('hide');
	}
</script>

</body>
</html>
