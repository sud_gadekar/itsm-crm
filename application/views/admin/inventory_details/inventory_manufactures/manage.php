<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_inventory_manufacture(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New inventory manufacture'); ?></a>
							
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($inventory_manufactures) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Inventory manufacture Name'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($inventory_manufactures as $inventory_manufacture ){ ?>
								<tr>
									<td><?php echo $inventory_manufacture['id']; ?></td>
									<td>
									  <a href="#" 
										onclick="edit_inventory_manufacture(this,<?php echo $inventory_manufacture['id']; ?>);return false;" data-name="<?php echo $inventory_manufacture['manufacture_name']; ?>">
											<?php echo $inventory_manufacture['manufacture_name']; ?>
									  </a>
									</td>
									<td>
										<a href="#" onclick="edit_inventory_manufacture(this,<?php echo $inventory_manufacture['id']; ?>); return false" data-name="<?php echo $inventory_manufacture['manufacture_name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<!-- <a href="<?php /* echo admin_url('inventory_details/delete_inventory_manufacture/'.$inventory_manufacture['id']);*/  ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> -->
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No inventory manufacture found'); ?></p>
						<?php } ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- manufacture Modal -->

<div class="modal fade" id="inventory_manufacture" tabindex="-1" inventory_manufacture="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('inventory_details/inventory_manufacture')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button manufacture="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Inventory manufacture edit'); ?></span>
					<span class="add-title"><?php echo _l('Inventory manufacture add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group">
							<label for="manufacture_name" class="control-label"> 
								<small class="req text-danger">* </small>Inventory manufacture Name</label>
								<input manufacture="text" id="manufacture_name" name="manufacture_name" class="form-control" value="">
							</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button manufacture="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button manufacture="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_inventory_manufactures);
		$('#inventory_manufacture').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#inventory_manufacture input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_inventory_manufactures(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_inventory_manufacture(){
		$('#inventory_manufacture').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_inventory_manufacture(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#inventory_manufacture input[name="manufacture_name"]').val(name);
		$('#inventory_manufacture').modal('show');
		$('.add-title').addClass('hide');
	}
</script>

</body>
</html>
