<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_inventory_product(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New inventory product'); ?></a>
							

						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($inventory_products) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Inventory product Name'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($inventory_products as $inventory_product){ ?>
								<tr>
									<td><?php echo $inventory_product['id']; ?></td>
									<td>
									  <a href="#" 
										onclick="edit_inventory_product(this,<?php echo $inventory_product['id']; ?>);return false;" data-name="<?php echo $inventory_product['product_name']; ?>">
											<?php echo $inventory_product['product_name']; ?>

											<input type="hidden" name="" id="manufacturename"
											value="<?php echo $inventory_product['id']; ?>">
									  </a>
									</td>
									<td>
										<a href="#" onclick="edit_inventory_product(this,<?php echo $inventory_product['id']; ?>); return false" 
											data-name="<?php echo $inventory_product['product_name']; ?>" 
											class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<!-- <a href="<?php /* echo admin_url('inventory_details/delete_inventory_product/'.$inventory_product['id']);*/  ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> -->
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- product Modal -->

<div class="modal fade" id="inventory_product" tabindex="-1" inventory_product="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('inventory_details/inventory_product'),array('id'=>'inventory_product_form')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button product="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Inventory product edit'); ?></span>
					<span class="add-title"><?php echo _l('Inventory product add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group select-placeholder">
	                        <label for=""><?php echo _l('Inventory manufacture'); ?></label>
	                        <select class="selectpicker" name="manufacture_id" data-width="100%">
	                        	<option disabled value="" selected="">Select Manufature</option>
	                          <?php foreach ($inventory_manufactures as $inventory_manufacture): ?>
			                    <option 
			                    value="<?= $inventory_manufacture['id']; ?>" ><?= $inventory_manufacture['manufacture_name']; ?></option>
			                  <?php endforeach; ?>
	                        </select>
	                     </div>
					</div>

					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group">
							<label for="product_name" class="control-label"> 
								<small class="req text-danger">* </small>Inventory product Name</label>
								<input product="text" id="product_name" name="product_name" class="form-control" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button product="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button product="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php init_tail(); ?>
<script>

	   $(function(){
	     appValidateForm($('#inventory_product_form'),
	      {manufacture_id:'required'
	      });
	   });

	function manage_client_inventory_products(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_inventory_product(){
		$('#inventory_product').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_inventory_product(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));

		$('#manufacturename')
		
		$('#inventory_product input[name="product_name"]').val(name);
		$('#inventory_product').modal('show');
		$('.add-title').addClass('hide');
	}
</script>

</body>
</html>
