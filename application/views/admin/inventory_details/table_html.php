<?php defined('BASEPATH') or exit('No direct script access allowed');


$table_data = [
  _l('the_number_sign'),
  _l('inventory_detail_dt_table_heading_category'),
  _l('inventory_detail_dt_table_heading_amount'),
  _l('inventory_detail_name'),
  _l('inventory_detail_receipt'),
  _l('inventory_detail_dt_table_heading_date'),
  _l('inventory_detail_name'),
  _l('inventory_detail_receipt'),
 
];

// if (!isset($project)) {
//   array_push($table_data, _l('project'));
//   array_push($table_data, [
//     'name'     => _l('inventory_detail_dt_table_heading_customer'),
//     'th_attrs' => ['class' => (isset($client) ? 'not_visible' : '')],
//   ]);
// }

// $table_data = array_merge($table_data, [
//   _l('invoice'),
//   _l('inventory_detail_dt_table_heading_reference_no'),
//   _l('inventory_detail_dt_table_heading_payment_mode'),
// ]);

$custom_fields = get_custom_fields('inventory_details', ['show_on_table' => 1]);

foreach ($custom_fields as $field) {
  array_push($table_data, $field['name']);
}


$table_data = hooks()->apply_filters('inventory_details_table_columns', $table_data);
// print_r($table_data);die();
render_datatable($table_data, (isset($class) ? $class : 'inventory_details'), [], [
  'data-last-order-identifier' => 'inventory_details',
  'data-default-order'         => get_table_last_order('inventory_details'),
]);
