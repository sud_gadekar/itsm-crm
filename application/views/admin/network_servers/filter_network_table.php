
    <?php if(count($network_servers) > 0){?>
    <?php foreach ($network_servers as $key => $network_server) { ?>
    <tr class="has-row-options">
      <td><?php echo $network_server['company'] ?>
        <div class="row-options">
          
          <a href="<?php echo admin_url('network_servers/network_server/').$network_server['id'].'#details' ?>">Edit </a>
          <span class="text-dark"> | </span>
          <a
          href="<?php echo admin_url('network_servers/delete_it_overview/').$network_server['id'] ?>" class="text-danger _delete">Delete </a>
        </td>
        <td><?php echo ucfirst($network_server['type_name']) ?></td>
        <td><?php echo $network_server['title'] ?></td>
      </tr>
      <?php }  ?>
      <?php } else {?>
      <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No entries found</td></tr>
      <?php }?>
   