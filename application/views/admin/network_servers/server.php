<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('expenses','','create')){ ?>
                  <a href="<?php echo admin_url('network_servers/network_server/server'); ?>" class="btn btn-info">
                    <?php echo 'New server'; ?></a>
                  <?php } ?>
                  <?php /*$this->load->view('admin/network_servers/filter_by_template'); */?>
                  
                  <div class="btn-group pull-right .mright5 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle mleft5" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="network_servers_total"></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 servers_table" id="small-table">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <!-- if network_serverid found in url -->
                        <?php /* echo form_hidden('id',$network_serverid); */ ?>
                        <?php /*$this->load->view('admin/network_servers/table_html'); */ ?>
            
                        <?php /* if (count($network_servers_lists) > 0) {  */?>
                        <div class="panel_s mtop20">
                          <div class="panel-body">
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                
                                <th><?php echo _l('Company'); ?></th>
                                <th><?php echo _l('Title'); ?></th>
                               
                              </thead>
                              <tbody>
                                <?php foreach ($network_servers as $key => $network_server) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $network_server['company']; ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('network_servers/network_server/').$network_server['id'].'#details' ?>">Edit </a> 
                                        <!--   <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('network_servers/delete/').$network_server['id'] ?>" class="text-danger _delete">Delete </a>  -->
                                  </td>
                                  <td><?php echo $network_server['title'] ?></td>
                                </tr>
                                <?php }  ?>
                              </tbody>
                            </table>
                            
                
                          </div>
                        </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="network_server" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="network_server_convert_helper_modal" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo _l('additional_action_required'); ?></h4>
         </div>
         <div class="modal-body">
            <div class="radio radio-primary">
               <input type="radio" checked id="network_server_convert_invoice_type_1" value="save_as_draft_false" name="network_server_convert_invoice_type">
               <label for="network_server_convert_invoice_type_1"><?php echo _l('convert'); ?></label>
            </div>
            <div class="radio radio-primary">
               <input type="radio" id="network_server_convert_invoice_type_2" value="save_as_draft_true" name="network_server_convert_invoice_type">
               <label for="network_server_convert_invoice_type_2"><?php echo _l('convert_and_save_as_draft'); ?></label>
            </div>
            <div id="inc_field_wrapper">
               <hr />
               <p><?php echo _l('network_server_include_additional_data_on_convert'); ?></p>
               <p><b><?php echo _l('network_server_add_edit_description'); ?> +</b></p>
               <div class="checkbox checkbox-primary inc_note">
                  <input type="checkbox" id="inc_note">
                  <label for="inc_note"><?php echo _l('network_server'); ?> <?php echo _l('network_server_add_edit_note'); ?></label>
               </div>
               <div class="checkbox checkbox-primary inc_name">
                  <input type="checkbox" id="inc_name">
                  <label for="inc_name"><?php echo _l('network_server'); ?> <?php echo _l('network_server_name'); ?></label>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-info" id="network_server_confirm_convert"><?php echo _l('confirm'); ?></button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // network_servers additional server params
             var network_servers_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               network_servers_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             //initDataTable('.table-network_servers', admin_url+'network_servers/table', 'undefined', 'undefined', network_servers_ServerParams, <?php echo hooks()->apply_filters('network_servers_table_default_order', json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

            // init_network_server();

             $('#network_server_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_network_server').attr('data-empty-note');
                var emptyName = $('#tab_network_server').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#network_server_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="network_server_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'network_servers/convert_to_invoice/'+$('body').find('.network_server_convert_btn').attr('data-id'), parameters);
            });
           });
</script>
<script type="text/javascript">
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>network_servers/filter_customer_by_server',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.servers_table').html(data);
            }
           }
      });
  }
</script>
</body>
</html>
