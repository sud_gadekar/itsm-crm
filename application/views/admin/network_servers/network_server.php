<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
      <div class="row">
        <div class="col-md-12"></div>
        <?php if(isset($id)){?> 
            <div class="col-md-3">
            <ul class="nav navbar-pills navbar-pills-flat nav-tabs nav-stacked customer-tabs" role="tablist">
              <li class="customer_tab_tasks service_overview">
                <a data-group="gaps" href="#service_overview" onClick="return false;">
                    <i class="fa fa-cog menu-icon" aria-hidden="true"></i>
                      Service Overview and Files
                </a>
              </li>
              <li class="customer_tab_tasks assets">
                <a data-group="assets" href="#assets" onClick="return false;">
                    <i class="fa fa-book menu-icon" aria-hidden="true"></i>
                      Dependencies
                </a>
              </li>
              <li class="customer_tab_tasks diagrams">
                <a data-group="diagrams" href="#diagrams" onClick="return false;">
                    <i class="fa fa-cubes menu-icon" aria-hidden="true"></i>
                      Diagram
                </a>
              </li>
              <li class="customer_tab_tasks urls">
                <a data-group="urls" href="#urls" onClick="return false;">
                    <i class="fa fa-tasks menu-icon" aria-hidden="true"></i>
                      Network Monitoring
                </a>
              </li>
              <li class="customer_tab_tasks gaps">
                <a data-group="gaps" href="#gaps" onClick="return false;">
                    <i class="fa fa-exchange menu-icon" aria-hidden="true"></i>
                      GAP
                </a>
              </li>
              <li class="customer_tab_tasks software_licenses">
                <a data-group="software_licenses" href="#software_licenses" onClick="return false;">
                    <i class="fa fa-retweet menu-icon" aria-hidden="true"></i>
                      Software & License
                </a>
              </li>
              
              <li class="customer_tab_projects details">
                <a data-group="details" href="#details" onClick="return false;">
                    <i class="fa fa-bars menu-icon" aria-hidden="true"></i>
                      Overview Title
                </a>
              </li>
         </div>
        <?php }?>
        <div class="<?php if(isset($id)){ echo ($id) ? 'col-md-9':'col-md-12'; } ?>">    
            <!-- Details  -->
              <?php echo form_open($this->uri->uri_string(),array('id'=>'inventory_form')); ?>
                <div class="panel_s" id="details">
                  <div class="btn-bottom-toolbar btn-toolbar-container-out text-right" style="width: 60.5%;">
                    <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
                  </div>
                 <div class="panel-body">
                  <div>
                    <div class="tab-content">
                      <h4 class="customer-profile-group-heading">
                        <?php if(isset($title))
                          {
                            echo ucfirst($title);
                          } 
                          elseif(isset($network_server_edit_data->type))
                          {
                            echo $this->db->where('id',$network_server_edit_data->type)->get('tblinventory_types')->row()->type_name;
                          } ?>
                      </h4>
                      <div class="row">
                        <div class="additional"></div>
                        <div class="col-md-4">
                          <div class="form-group select-placeholder f_client_id">
                                <label for="clientid" class="control-label">
                                    <span class="text-danger">*</span><?php echo _l('Assigned to ( Customer )'); ?></label>
                                <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                                  <option value="" disabled selected=""> Select </option>
                                  <?php foreach($clients as  $client){ ?>
                                      <option value="<?php if(isset($client['userid'])){echo $client['userid'];}?>" 
                                        <?php if(isset($network_server_edit_data->clientid)){
                                          echo ($network_server_edit_data->clientid == $client['userid']) ? 'selected' : '';
                                        }
                                        ?>>
                                        <?php echo $client['company']; ?></option>
                                  <?php } ?>
                              </select>
                              <input type="hidden" name="type" id="type" 
                              value="<?php if(isset($title)){ echo $title; } 
                                           elseif(isset($network_server_edit_data)){echo $network_server_edit_data->type;}?>">
                          </div>
                      </div>

                      
                      <div class="col-md-4">
                          <div class="form-group select-placeholder f_client_id">
                                <label for="type" class="control-label">
                                    <span class="text-danger">*</span><?php echo _l('Type'); ?></label>
                                <select class="selectpicker" name="type" id="type"data-width="100%" data-live-search="true">
                                
                                  <?php foreach ($inventory_types as $key => $value) { ?>
                                        <option 
                                              value="<?php if(isset( $value['id'])){echo $value['id'];}?>"
                                              <?php if(isset($value['id']) && isset($network_server_edit_data->type)){
                                                      echo ($value['id'] == $network_server_edit_data->type) ? 'selected' : '';
                                              }  ?>>
                                          <?php echo $value['type_name'];?>
                                        </option>                                  

                                  <?php } ?>

                                  <!-- /*<option value="network" <?php /* if(isset($network_server_edit_data->type)){ echo ($network_server_edit_data->type == "network") ? 'selected' : ''; } ?>>Network</option>
                                  <option value="server" <?php if(isset($network_server_edit_data->type)){ echo ($network_server_edit_data->type == "server") ? 'selected' : ''; } */?>>Server</option>*/ -->
                              </select>
                          </div>
                      </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="sub_type" class="control-label">
                                    <?php echo _l('Title'); ?>
                                </label>
                                <input type="text" name="title" value="<?php if(isset($network_server_edit_data->title)){
                                  echo $network_server_edit_data->title; 
                                }?>" id="title" class="form-control">
                                <input type="hidden" name="id" value="<?php if(isset($network_server_edit_data->id)){
                                  echo $network_server_edit_data->id; 
                                }?>" id="title" class="form-control">
                                
                            </div>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  
              <?php echo form_close(); ?>

            <!-- Asset  -->
            <div class="panel_s" id="assets" style="display: none;">
             <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Dependencies</h4>
                  <div class="inline-block new-contact-wrapper">
                     <a href="#" onclick="asset(); return false;" class="btn btn-info new-contact mbot25">Link Dependencies</a>
                  </div>
                  <div class="row">
                    <div class="additional"></div>
                       <table class="table dt-table scroll-responsive">
                        <thead>
                          <th><?php echo _l('Dependency'); ?></th>
                          <th><?php echo _l('Type'); ?></th>
                          <th><?php echo _l('Sub-type'); ?></th>   
                          <th><?php echo _l('Manufacture'); ?></th>
                          <th><?php echo _l('Product name'); ?></th>
                          <th><?php echo _l('Host name'); ?></th>
                          <th><?php echo _l('Location'); ?></th>
                          <th><?php echo _l('Options'); ?></th>
                        </thead>
                        <tbody>
                          <?php foreach ($network_server_assets as $key => $network_server_asset) { ?>
                          <tr class="has-row-options">
                            <td> <?php echo $network_server_asset['product_name']?>
                              <div class="row-options">
                                  <a 
                                    href="
                                    <?php echo admin_url('network_servers/network_server_asset_delete/').$network_server_asset['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                              </div> 
                            </td>
                            <td> <?php echo $network_server_asset['type']?> </td>
                            <td> <?php echo $network_server_asset['subtype']?> </td>
                            <td> <?php echo $network_server_asset['manufacture_name']?> </td>
                            <td> <?php echo $network_server_asset['product_name']?> </td>
                            <td> <?php echo $network_server_asset['hostname']?> </td>
                            <td> <?php echo $network_server_asset['location']?> </td>
                            <td> 
                                <a href = "#" 
                                  data_company = "<?php echo $network_server_asset['company']?>"
                                  data_type = "<?php echo $network_server_asset['type']?>"
                                  data_subtype = "<?php echo $network_server_asset['subtype']?>" 
                                  data_manufacture_name = "<?php echo $network_server_asset['manufacture_name']?>" 
                                  data_product_name = "<?php echo $network_server_asset['product_name']?>"
                                  data_hostname = "<?php echo $network_server_asset['hostname']?>"
                                  data_location = "<?php echo $network_server_asset['location']?>"
                                  data_status = "<?php echo $network_server_asset['status_name']?>"
                                  data_processor = "<?php echo $network_server_asset['processor']?>"
                                  data_ram = "<?php echo $network_server_asset['ram']?>"
                                  data_storage = "<?php echo $network_server_asset['storage']?>"
                                  data_ip_address = "<?php echo $network_server_asset['ip_address']?>" 
                                  data_serial_no = "<?php echo $network_server_asset['serial_no']?>" 
                                  data_operating_system = "<?php echo $network_server_asset['operating_system']?>"
                                  data_sw_valid_from = "<?php echo $network_server_asset['sw_valid_from']?>"
                                  data_sw_valid_to = "<?php echo $network_server_asset['sw_valid_to']?>" 
                                  data_sw_amount = "<?php echo $network_server_asset['sw_amount']?>"
                                  data_sw_renewal = "<?php echo $network_server_asset['sw_renewal']?>"
                                  data_backup_required = "<?php echo $network_server_asset['backup_required']?>" 
                                  data_subject = "<?php echo $network_server_asset['subject']?>" 
                                  data_firstname = "<?php echo $network_server_asset['firstname']?>"
                                  data_lastname = "<?php echo $network_server_asset['lastname']?>"
                                  
                                  class="showAssetsOnModal btn btn-default btn-icon">
                                <i class="fa fa-eye"></i></a> 
                            </td>
                          </tr>
                          <?php  } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>  

             <!-- Diagram  -->
            <div class="panel_s" id="diagrams" style="display: none;">
              <div class="panel-body">
                <div>
                  <div class="tab-content">
                    <h4 class="customer-profile-group-heading">Diagram </h4>
                    <div class="inline-block new-contact-wrapper">
                       <a href="#" onclick="diagram(); return false;" class="btn btn-info new-contact mbot25">link diagram</a>
                    </div>
                    <div class="row">
                      <div class="additional"></div>
                      <table class="table dt-table scroll-responsive">
                        <thead>
                         <th><?php echo _l('id'); ?></th>
                          <th><?php echo _l('Diagram Title'); ?></th>
                          <th><?php echo _l('Diagram File'); ?></th>
                        </thead>
                        <tbody>
                          <?php foreach ($network_server_diagrams as $key => $diagram) { ?>
                            <tr class="has-row-options">
                              <td> <?php echo $key+1;?>
                                <div class="row-options">
                                  <a 
                                  href="
                                  <?php echo admin_url('network_servers/network_server_diagram_delete/').$diagram['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                                </div> 
                              </td>

                              <td><?php echo $diagram['title']?></td>
                              <td>
                                  <a href="<?php echo base_url('uploads/diagrams/'.$diagram['diagramsid'].'/thumb_'.$diagram['diagram_file']); ?>" target="_blank">
                                      <img width="100px" height="60px" class="" src="<?php echo base_url('uploads/diagrams/'.$diagram['diagramsid'].'/thumb_'.$diagram['diagram_file']); ?>" alt=''>      
                                  </a>
                              </td>
                            </tr>
                        <?php  } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>  


              <!-- URL  -->
            <div class="panel_s" id="urls" style="display: none;">
             <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Network monitoring </h4>
                  <div class="inline-block new-contact-wrapper">
                       <a href="#" onclick="url(); return false;" class="btn btn-info new-contact mbot25">Add Monitoring</a>
                  </div>
                  <div class="row">
                      <div class="additional"></div>
                      <table class="table dt-table scroll-responsive">
                        <thead>
                         <th><?php echo _l('id'); ?></th>
                          <th><?php echo _l('Title'); ?></th>
                          <th><?php echo _l('Option'); ?></th>
                        </thead>
                        <tbody>
                          <?php foreach ($network_server_urls as $key => $url) { ?>
                            <tr class="has-row-options">
                              <td> <?php echo $key+1;?>
                                <div class="row-options">
                                  <a 
                                  href="
                                  <?php echo admin_url('network_servers/network_server_url_delete/').$url['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                                </div> 
                              </td>

                              <td><?php echo $url['monitoring_title']?></td>
                              <td><a href = "#" data-url="<?php echo $url['url']; ?>" 
                                class=" view_url_in_frame btn btn-default btn-icon">
                                <i class="fa fa-eye"></i></a></td>
                            </tr>
                        <?php  } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div> 


              <!-- GAP  -->

            <div class="panel_s" id="gaps" style="display: none;">
             <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">GAP </h4>
                  <div class="inline-block new-contact-wrapper">
                       <a href="#" onclick="gap(); return false;" class="btn btn-info new-contact mbot25">Link GAP</a>
                  </div>
                  <div class="row">
                      <div class="additional"></div>
                      <table class="table dt-table scroll-responsive">
                        <thead>
                         <th><?php echo _l('Customer'); ?></th>
                         <th><?php echo _l('Name'); ?></th>
                         <th><?php echo _l('Description'); ?></th>
                         <th><?php echo _l('Category'); ?></th>
                         <th><?php echo _l('Impact'); ?></th>
                         <th><?php echo _l('Status'); ?></th>
                         <th><?php echo _l('Date'); ?></th>
                        </thead>
                        <tbody>
                          <?php foreach ($network_server_gaps as $key => $gap) { ?>
                            <tr class="has-row-options">
                              <td> <?php echo $gap['company']; ?>
                                <div class="row-options">
                                  <a 
                                  href="
                                  <?php echo admin_url('network_servers/network_server_gap_delete/').$gap['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                                </div> 
                              </td>

                              <td><?php echo $gap['name']?></td>
                              <td><?php echo $gap['description']?></td>
                              <td><?php echo $gap['category_title']?></td>
                              <td><?php echo $gap['impact_title']?></td>
                              <td><?php echo $gap['status_title']?></td>
                              <td><?php echo $gap['created_at']?></td>
                            </tr>
                        <?php  } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div> 




              <!-- Software and License  -->

            <div class="panel_s" id="software_licenses" style="display: none;">
             <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Software & License</h4>
                  <div class="inline-block new-contact-wrapper">
                       <a href="#" onclick="software_license(); return false;" class="btn btn-info new-contact mbot25">Link Software & License</a>
                  </div>
                  <div class="row">
                      <div class="additional"></div>
                      <table class="table dt-table scroll-responsive">
                        <thead>
                         <th><?php echo _l('Customer'); ?></th>
                         <th><?php echo _l('Name'); ?></th>
                         <th><?php echo _l('Vendor'); ?></th>
                         <th><?php echo _l('Type'); ?></th>
                         <th><?php echo _l('Start Date'); ?></th>
                         <th><?php echo _l('End Date'); ?></th>
                        
                        </thead>
                        <tbody>
                          <?php foreach ($network_servers_software_licenses as $key => $software_license) { ?>
                            <tr class="has-row-options">
                              <td> <?php echo $software_license['company']; ?>
                                <div class="row-options">
                                  <a 
                                  href="
                                  <?php echo admin_url('network_servers/network_server_software_license_delete/').$software_license['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                                </div> 
                              </td>

                              <td><?php echo $software_license['name']?></td>
                              <td><?php echo $software_license['vendor_name']?></td>
                              <td><?php echo $software_license['type_name']?></td>
                              <td><?php echo $software_license['start_date']?></td>
                              <td><?php echo $software_license['end_date']?></td>
                              
                            </tr>
                        <?php  } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div> 


               <!-- Service overview  -->

            <div class="panel_s" id="service_overview" style="display: none;">
             <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Service Overview </h4>
              
                  <?php 
                  echo form_open_multipart(admin_url('network_servers/service_overview'),array('id'=>'service_overview_form')); ?>
          
                    <div class="btn-bottom-toolbar btn-toolbar-container-out text-right" style="width: 60.5%;">
                      <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
                    </div>
                    <div class="row">
                      <div class="additional"></div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="summary" class="control-label">Brief Summary</label>
                            <textarea id="summary" name="summary" class="form-control" rows="4" style="line-height: 21px;"><?php if(isset($service_overviews[0]['summary'])){print_r($service_overviews[0]['summary']);} ?>
                            </textarea>
                          </div>                    
                        </div>

                        <input type="hidden" name="overview_id" 
                              value="<?php if(isset($network_server_edit_data->id)){
                                     echo $network_server_edit_data->id; 
                              }?>" id="title" class="form-control">


                        <div class="col-md-12">
                          <div class="row attachments">
                              <div class="attachment">
                                  <div class="col-md-6 col-md-offset-3 mbot15">
                                      <div class="form-group">
                                          <label for="attachment"
                                              class="control-label"><?php echo _l('Attachments'); ?></label>
                                          <div class="input-group">
                                              <input type="file"
                                                  extension="<?php echo str_replace(['.', ' '], '', get_option('ticket_attachments_file_extensions')); ?>"
                                                  filesize="<?php echo file_upload_max_size(); ?>"
                                                  class="form-control" name="files[]" multiple
                                                  accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
                                              <!-- <span class="input-group-btn">
                                                  <button class="btn btn-success add_more_attachments p8-half"
                                                      data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>"
                                                      type="button"><i class="fa fa-plus"></i></button>
                                              </span> -->
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                           
                    </div>
               
              <?php echo form_close(); ?>

              <hr>
              <?php if(count($service_overviews) > 0 ){ ?>
                 <table class="table dt-table scroll-responsive">
                        <thead>
                           <th><?php echo _l('File'); ?></th>
                           <th><?php echo _l('Option'); ?></th>
                        </thead>
                        <tbody>
                          <?php foreach ($service_overviews as $key => $service_overview) { ?>
                            <tr class="has-row-options">
                              <?php if(isset($service_overview['file_name'])){ ?>
                              <td> <?php echo $service_overview['file_name']; ?>
                                <div class="row-options">
                                  <a 
                                  href="
                                  <?php echo admin_url('network_servers/delete_service_overview_file/').$service_overview['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  
                                </div> 
                              </td>

                              <td>
                                <a target="_blank"
                                href = "<?php echo base_url('/uploads/network_servers/'.$service_overview['service_overviewid'].'/'.$service_overview['file_name'])?>" 
                                  class="btn btn-default btn-icon">
                                  <i class="fa fa-eye"></i>
                                </a>
                              </td>
                            <?php } ?>
                            </tr>
                        <?php  } ?>
                        </tbody>

                      </table>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div> 

        </div>
           
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>
  <?php /* $this->load->view('admin/network_servers/network_server_category'); /*/?>
  <?php init_tail(); ?>


<!-- Product Modal -->

  <div class="modal fade" id="product-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('expenses/category'),array('id'=>'expense-category-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('edit_expense_category'); ?></span>
                    <span class="add-title"><?php echo _l('new_expense_category'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('name','expense_add_edit_name'); ?>
                        <?php echo render_textarea('description','expense_add_edit_description'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
  </div>

<!-- /.modal -->


<!-- Asset Modal -->

  <div class="modal fade" id="asset-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('network_servers/assets/').$id); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('Inventory'); ?></span>
                    <span class="add-title"><?php echo _l('Inventory'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group select-placeholder">
                              <label for="type"><?php echo _l('Product'); ?></label>
                              <select class="selectpicker" name="inventory_id[]" id="inventory_id"data-width="100%" multiple data-live-search="true">
                                <?php foreach($assets as $asset) {?>
                                  <option value="<?php echo $asset['id']?>" >
                                    <?php echo $asset['product_name'].' ( '.$asset['hostname'].' | '.$asset['purpose'].' ) '; ?>
                                  </option>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="network_id" value="<?php if(isset($id)){ echo $id;} ?>">
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
  </div>

<!-- /.modal -->

<!-- Diagram Modal -->

  <div class="modal fade" id="diagram-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('network_servers/diagrams/').$id); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('Diagram'); ?></span>
                    <span class="add-title"><?php echo _l('Diagram'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group select-placeholder">
                              <label for="type"><?php echo _l('Diagram'); ?></label>
                              <select class="selectpicker" name="diagram_id" id="diagram_id"data-width="100%" data-live-search="true">
                                <?php foreach($diagrams as $diagram) {?>
                                  <option value="<?php echo $diagram['id']?>" ><?php echo $diagram['title']?></option>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="network_id" value="<?php if(isset($id)){ echo $id;} ?>">
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
  </div>

<!-- /.modal -->

<!-- URL Modal -->

  <div class="modal fade" id="url-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog ">
        <?php echo form_open(admin_url('network_servers/urls/').$id,array('id' => 'monitoring_form', )); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('URL'); ?></span>
                    <span class="add-title"><?php echo _l('URL'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group ">
                              <label for="monitoring_title"><?php echo _l('Monitoring Title'); ?></label>
                              <input type="text" name="monitoring_title" class="form-control" value="" id="monitoring_title">
                          </div>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group ">
                              <label for="url"><?php echo _l('URL'); ?></label>
                              <textarea  type="url" id="url" name="url" class="form-control" rows="4"></textarea>
                              <input type="hidden" name="network_id" value="<?php if(isset($id)){ echo $id;} ?>">
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
  </div>

<!-- /.modal -->


<!-- GAP Modal -->

  <div class="modal fade" id="gap-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('network_servers/gaps/').$id); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('GAP'); ?></span>
                    <span class="add-title"><?php echo _l('GAP'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group select-placeholder">
                              <label for="gap_id"><?php echo _l('GAP'); ?></label>
                              <select class="selectpicker" name="gap_id" id="gap_id"data-width="100%" data-live-search="true">
                                <?php foreach($gaps as $gap) {?>
                                  <option value="<?php echo $gap['id']?>" ><?php echo $gap['name']?></option>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="network_id" value="<?php if(isset($id)){ echo $id;} ?>">
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
  </div>

<!-- /.modal -->


<!-- Software and license Modal -->

  <div class="modal fade" id="software_license-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('network_servers/software_licenses/').$id); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('Software & License'); ?></span>
                    <span class="add-title"><?php echo _l('Software & License'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                          <div class="form-group select-placeholder">
                              <label for="renewal_id"><?php echo _l('Software and License Name'); ?></label>
                              <select class="selectpicker" name="renewal_id" id="renewal_id" data-width="100%" data-live-search="true">
                                <?php foreach($software_licenses as $software_license) {?>
                                  <option value="<?php echo $software_license['id']?>" ><?php echo $software_license['name']?></option>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="network_id" value="<?php if(isset($id)){ echo $id;} ?>">
                          </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
  </div>

<!-- /.modal -->


<!-- Service Overview Modal -->

  

<!-- /Modal -->


<!-- Modal to show image  -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;height: 70vh;" >
      </div>
    </div>
  </div>
</div>  


<div class="modal fade" id="imageurlmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close closeiframemodal" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <iframe class="myframe" src="" width="560" style="height: 75vh !important; width: 100% !important;" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div> 


<div class="modal fade" id="assetmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <div class="assetDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 



  <script type="text/javascript">
    function new_product(){
        $('#product-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
    function asset(){
        $('#asset-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
    function diagram(){
        $('#diagram-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
    function url(){
        $('#url-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
    function gap(){
        $('#gap-modal').modal('show');
        $('.edit-title').addClass('hide');
    }
    function software_license(){
        $('#software_license-modal').modal('show');
        $('.edit-title').addClass('hide');
    }

  </script>

  <script type="text/javascript">
    $('ul li').click(function(e) 
       { 
        if($.trim($(this).text()) == 'Overview Title')
        {
          $('#gaps, #assets, #urls,#diagrams, #service_overview, #software_licenses').css('display','none');
          $('#details').css('display','block');
          $('.gaps, .assets, .urls,.diagrams, .service_overview, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'Dependencies')
        {
          $('#details, #diagrams, #urls, #gaps, #service_overview, #software_licenses').css('display','none');
          $('#assets').css('display','block');
          $('.details, .diagrams, .urls, .gaps, .service_overview, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'Diagram')
        {
          $('#details, #assets, #urls, #gaps, #service_overview, #software_licenses').css('display','none');
          $('#diagrams').css('display','block');
          $('.details, .assets, .urls, .gaps,.service_overview, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'Network Monitoring')
        {
          $('#details,#diagrams, #assets, #gaps, #service_overview, #software_licenses').css('display','none');
          $('#urls').css('display','block');
          $('.details, .assets, .diagrams, .gaps, .service_overview, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'GAP')
        {
          $('#details, #assets, #urls,#diagrams, #service_overview, #software_licenses').css('display','none');
          $('#gaps').css('display','block');
          $('.details, .assets, .urls, .diagrams, .service_overview, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'Software & License')
        {
          $('#details, #assets, #urls,#diagrams, #service_overview, #gaps').css('display','none');
          $('#software_licenses').css('display','block');
          $('.details, .assets, .urls, .diagrams, .service_overview, .gaps').removeClass('active');
          $(this).addClass('active');
        }
        else if($.trim($(this).text()) == 'Service Overview and Files')
        {
          $('#details, #assets, #urls,#diagrams,#gaps, #software_licenses').css('display','none');
          $('#service_overview').css('display','block');
          $('.details, .assets, .urls, .diagrams, .gaps, .software_licenses').removeClass('active');
          $(this).addClass('active');
        }
        
       });
  </script>

  <script type="text/javascript">
    $( document ).ready(function() {
      
      var url = $(location).attr("href"); 
      var arguments = url.split('#')[1];
      //alert(arguments);
      if(arguments == 'details')
      {
        $('#gaps, #assets, #urls,#diagrams,#service_overview, #software_licenses').css('display','none');
        $('#details').css('display','block');
        $('.gaps, .assets, .urls,.diagrams, .software_licenses').removeClass('active');
        $('.details').addClass('active');
      }
      else if(arguments == 'assets')
      {
        $('#details, #diagrams, #urls, #gaps, #service_overview, #software_licenses').css('display','none');
        $('#assets').css('display','block');
        $('.details, .diagrams, .urls, .gaps, .software_licenses').removeClass('active');
        $('.assets').addClass('active');
      }
      else if(arguments == 'diagrams')
      {
        $('#details, #assets, #urls, #gaps, #service_overview, #software_licenses').css('display','none');
        $('#diagrams').css('display','block');
        $('.details, .assets, .urls, .gaps, .software_licenses').removeClass('active');
        $('.diagrams').addClass('active');
      }
      else if(arguments == 'urls')
      {
        $('#details,#diagrams, #assets, #gaps, #service_overview, #software_licenses').css('display','none');
        $('#urls').css('display','block');
        $('.details, .assets, .diagrams, .gaps, .software_licenses').removeClass('active');
        $('.urls').addClass('active');
      }
      else if(arguments == 'gaps')
      {
        $('#details, #assets, #urls,#diagrams, #service_overview, #software_licenses').css('display','none');
        $('#gaps').css('display','block');
        $('.details, .assets, .urls, .diagrams, .software_licenses').removeClass('active');
        $('.gaps').addClass('active');
      }
      else if(arguments == 'software_licenses')
      {
        $('#details, #assets, #urls,#diagrams, #service_overview, #gaps').css('display','none');
        $('#software_licenses').css('display','block');
        $('.details, .assets, .urls, .diagrams, .gaps').removeClass('active');
        $('.software_licenses').addClass('active');
      }
      else if(arguments == 'service_overview')
      {
        $('#details, #assets, #urls,#diagrams, #gaps, #software_licenses').css('display','none');
        $('#service_overview').css('display','block');
        $('.details, .assets, .urls, .diagrams,.gaps, .software_licenses').removeClass('active');
        $('.service_overview').addClass('active');
      }
      

    });
  </script>

<!-- //Script for Image show on modal -->
  <script type="text/javascript">
    $(function() {
        $('.pop').on('click', function() {
          $('.imagepreview').attr('src', $(this).attr('src'));
          $('#imagemodal').modal('show');   
        });   
    });
  </script>

  <script type="text/javascript">
    $(function() {
      $('.view_url_in_frame').click(function() {

            $('#imageurlmodal').modal('show'); 
            var url = $(this).attr('data-url');
            
            if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) 
            {
                  iframe = $('iframe');
                  iframe.attr('src', url);
            }
            else
            {
              var prefix = 'https://';
              if (url.substr(0, prefix.length) !== prefix)
              {
                  url = prefix + url;
                  iframe = $('iframe');
                  iframe.attr('src', url);
              }
            }   
      });
    });

    $('.closeiframemodal').click(function(e) {
         $('iframe').attr('src', ''); 
      });
  </script>

<!-- Show Asset Data On Modal -->
  <script type="text/javascript">
    $(".showAssetsOnModal").on("click", function(){
        var data_company = $(this).attr("data_company");
        var data_type = $(this).attr("data_type");
        var data_subtype = $(this).attr("data_subtype");
        var data_manufacture_name = $(this).attr("data_manufacture_name");
        var data_product_name = $(this).attr("data_product_name");
        var data_hostname = $(this).attr("data_hostname");
        var data_location = $(this).attr("data_location");
        var data_status = $(this).attr("data_status");
        var data_ram = $(this).attr("data_ram");
        var data_storage = $(this).attr("data_storage");
        var data_ip_address = $(this).attr("data_ip_address");
        var data_serial_no = $(this).attr("data_serial_no");
        var data_operating_system = $(this).attr("data_operating_system");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");
        var data_sw_valid_to = $(this).attr("data_sw_valid_to");
        var data_sw_amount = $(this).attr("data_sw_amount");
        var data_sw_renewal = $(this).attr("data_sw_renewal");
        var data_backup_required = $(this).attr("data_backup_required");
        var data_subject = $(this).attr("data_subject");
        var data_sw_valid_from = $(this).attr("data_sw_valid_from");         
        var data_firstname = $(this).attr("data_firstname");
        var data_lastname = $(this).attr("data_lastname");
        var contact  =  data_firstname +' '+ data_lastname;           
        var BackupStatus = '';
        if(data_backup_required == 1){
          BackupStatus = "Yes";
        }   
        else if(data_backup_required == 0)
        {
          BackupStatus = "No";
        }
                              
          var table_header = "<table class='table table-striped table-bordered'><tbody>";
          var table_footer = "</tbody></table>";
          var html ="";

          html += "<tr><td>"+'Customer'+"</td><td>"+data_company+"</td></tr>";
          html += "<tr><td>"+'Type'+"</td><td>"+data_type+"</td></tr>";
          html += "<tr><td>"+'Sub type'+"</td><td>"+data_subtype+"</td></tr>";
          html += "<tr><td>"+'Manufacture'+"</td><td>"+data_manufacture_name+"</td></tr>";
          html += "<tr><td>"+'Product'+"</td><td>"+data_product_name+"</td></tr>";
          html += "<tr><td>"+'Host name'+"</td><td>"+data_hostname+"</td></tr>";
          html += "<tr><td>"+'Location'+"</td><td>"+data_location+"</td></tr>";
          html += "<tr><td>"+'Status'+"</td><td>"+data_status+"</td></tr>";
          html += "<tr><td>"+'RAM'+"</td><td>"+data_ram+"</td></tr>";
          html += "<tr><td>"+'Storage'+"</td><td>"+data_storage+"</td></tr>";
          html += "<tr><td>"+'IP'+"</td><td>"+data_ip_address+"</td></tr>";
          html += "<tr><td>"+'Serial number'+"</td><td>"+data_serial_no+"</td></tr>";
          html += "<tr><td>"+'OS'+"</td><td>"+data_operating_system+"</td></tr>";
          html += "<tr><td>"+'Backup required'+"</td><td>"+BackupStatus+"</td></tr>";
          html += "<tr><td>"+'Contract'+"</td><td>"+data_subject+"</td></tr>";
          html += "<tr><td>"+'Contact'+"</td><td>"+contact+"</td></tr>";
           
          var all = table_header +html+ table_footer;

          $('#assetmodal .assetDetailsData').html(all); 
          $('#assetmodal').modal('show'); 
        
    });
  </script>


<script>
   $(function(){
     appValidateForm($('#monitoring_form '),
      {
        monitoring_title:'required',
        url:'required'
      });
   });
</script>

</body>
</html>