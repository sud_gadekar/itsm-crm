<div class="col-md-12 servers_table" id="small-table">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <!-- if network_serverid found in url -->
                        <?php /* echo form_hidden('id',$network_serverid); */ ?>
                        <?php /*$this->load->view('admin/network_servers/table_html'); */ ?>
            
                        <?php /* if (count($network_servers_lists) > 0) {  */?>
                        <div class="panel_s mtop20">
                          <div class="panel-body">
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive
                            <?php echo (count($network_servers) <= 0) ?'dt-inline dataTable no-footer' : ''?>">
                              <thead>
                                
                                <th><?php echo _l('Company'); ?></th>
                                <th><?php echo _l('Title'); ?></th>
                               
                              </thead>
                              <tbody>
                                <?php if(count($network_servers) > 0){?>
                                <?php foreach ($network_servers as $key => $network_server) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $network_server['company']; ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('network_servers/network_server/').$network_server['id'].'#details' ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('network_servers/delete/').$network_server['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td><?php echo $network_server['title'] ?></td>
                                </tr>
                                <?php }  ?>
                                <?php } else {?>
                                  <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No entries found</td></tr>
                                <?php }?>
                              </tbody>
                            </table>
                            
                
                          </div>
                        </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>