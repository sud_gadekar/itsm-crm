<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style type="text/css">
    table th{
        padding-bottom: 20px !important; 
        font-size: 14px !important;
        color: white !important;
        font-weight: bold !important;
        letter-spacing: 1px !important;
    }
    table td{
            font-size: 14px;
            font-weight: 500;
            background-color: #80808087;
    }
</style>
<div id="wrapper">
    <div class="screen-options-area"></div>
    <div class="content">
        <!-- ITSM DashBoard -->
        <div class="row" >
            <div class="col-md-12">
                 <div class="panel_s">
                <div class="panel-body">
                    <h4 class="customer-profile-group-heading"><?php echo get_option('companyname');?> IT Services Dashboard</h4>
                    
                    <div class="row">
                      
                        <?php if(count($tickets_status_no_json['datasets'][0]['data']) > 0){ ?>
                          <div class="col-md-4">
                          <div class="panel_s">
                            <div class="panel-body padding-10">
                              <div class="row">
                                <div class="col-md-12 mbot10">
                                  <p class="padding-5"> <?php echo _l('Support Request by status'); ?></p>
                                  <hr class="hr-panel-heading-dashboard">
                                  <canvas height="220" id="tickets-by-status"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div>
                        <?php } ?>
                         

                      
                        <?php if(count($service_requests_status_no_json['datasets'][0]['data']) > 0){ ?>
                          <div class="col-md-4">
                          <div class="panel_s">
                            <div class="panel-body padding-10">
                              <div class="row">
                                <div class="col-md-12 mbot10">
                                  <p class="padding-5"> <?php echo _l('Service request by status'); ?></p>
                                  <hr class="hr-panel-heading-dashboard">
                                  <canvas height="220" id="service_requests-by-status"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                           </div> 
                        <?php } ?>
                     
                      
                          <?php if(count($tasks_status_no_json['datasets'][0]['data']) > 0){ ?>
                            <div class="col-md-4">
                            <div class="panel_s">
                              <div class="panel-body padding-10">
                                <div class="row">
                                  <div class="col-md-12 mbot10">
                                    <p class="padding-5"> <?php echo _l('Task by Status'); ?></p>
                                    <hr class="hr-panel-heading-dashboard">
                                    <canvas height="220" id="tasks-by-status"></canvas>
                                  </div>
                                </div>
                              </div>
                            </div>
                             </div> 
                          <?php } ?>
                       



                        <?php /*if(is_admin()) { */?>
                          <?php if(count($projects_status_no_json['datasets'][0]['data']) > 0){ ?>
                            <div class="col-md-4">
                            <div class="panel_s">
                              <div class="panel-body padding-10">
                                <div class="row">
                                  <div class="col-md-12 mbot10">
                                    <p class="padding-5"> <?php echo _l('Projects by status'); ?></p>
                                    <hr class="hr-panel-heading-dashboard">
                                    <canvas height="220" id="projects-by-status"></canvas>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </div> 
                          <?php } ?>
                        <?php/* } */?>
                      
                    <!-- </div>
                     <div class="row">   -->
                     
                     
                        <?php if(count($gaps_status_no_json['datasets'][0]['data']) > 0){ ?>
                           <div class="col-md-4">
                          <div class="panel_s">
                            <div class="panel-body padding-10">
                              <div class="row">
                                <div class="col-md-12 mbot10">
                                  <p class="padding-5"> <?php echo _l('GAP by status'); ?></p>
                                  <hr class="hr-panel-heading-dashboard">
                                  <canvas height="220" id="gaps-by-status"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                           </div> 
                        <?php } ?>
                     
                     
                      <?php if(is_admin()) { ?>
                        <?php if(count($inventorys_status_no_json['datasets'][0]['data']) > 0){ ?>
                          <div class="col-md-4">
                          <div class="panel_s">
                            <div class="panel-body padding-10">
                              <div class="row">
                                <div class="col-md-12 mbot10">
                                  <p class="padding-5"> <?php echo _l('Inventory by Types'); ?></p>
                                  <hr class="hr-panel-heading-dashboard">
                                  <canvas height="220" id="inventorys-by-status"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div> 
                        <?php } ?>
                      
                      
                        <?php if(count($inventorysubtypes_status_no_json['datasets'][0]['data']) > 0){ ?>
                           <div class="col-md-4">
                          <div class="panel_s">
                            <div class="panel-body padding-10">
                              <div class="row">
                                <div class="col-md-12 mbot10">
                                  <p class="padding-5"> <?php echo _l('Inventory by Subtypes'); ?></p>
                                  <hr class="hr-panel-heading-dashboard">
                                  <canvas height="220" id="inventorysubtypes-by-status"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                          </div> 
                        <?php } ?>
                      <?php } ?>
                      
                    </div>
                    <div class="clearfix"></div>
                    <!-- Support Request -->

                   <!--  <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Support Request by status</h4>
                        </div>
                        <?php foreach($statuses as $status){
                          $_where = '';
                          $where = '';
                          if($where == ''){
                            $_where = 'status='.$status['ticketstatusid'];
                          } else{
                            $_where = 'status='.$status['ticketstatusid'] . ' '.$where;
                          } ?>
                          <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="<?php echo admin_url('tickets');?>">
                              <h3 class="bold"><?php echo total_rows(db_prefix().'tickets',$_where); ?></h3>
                              <span style="color:<?php echo $status['statuscolor']; ?>">
                                <?php echo ticket_status_translate($status['ticketstatusid']); ?>
                              </span>
                            </a>
                          </div>
                        <?php } ?>
                    </div>
                    <hr class="hr-panel-heading"> -->
                    
                    <!-- Service Request -->

                  <!--   <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Service request by status</h4>
                        </div>
                        <?php foreach($statuses as $status){
                          $_where = '';
                          $where = '';
                          if($where == ''){
                            $_where = 'status='.$status['ticketstatusid'];
                          } else{
                            $_where = 'status='.$status['ticketstatusid'] . ' '.$where;
                          } ?>
                          <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="<?php echo admin_url('service_requests');?>">
                              <h3 class="bold"><?php echo total_rows(db_prefix().'service_requests',$_where); ?></h3>
                              <span style="color:<?php echo $status['statuscolor']; ?>">
                                <?php echo ticket_status_translate($status['ticketstatusid']); ?>
                              </span>
                            </a>
                          </div>
                        <?php } ?>
                    </div>
                    <hr class="hr-panel-heading"> -->

                    <!-- Projects -->

                  <!--   <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Projects by status</h4>
                        </div>
                        <?php $_where = '';
                          if(!has_permission('projects','','view')){
                            $_where = 'id IN (SELECT project_id FROM '.db_prefix().'project_members WHERE staff_id='.get_staff_user_id().')';
                          }
                          ?>
                        <?php foreach($project_statuses as $project_status){
                        ?>
                       <div class="col-md-2 col-xs-6 border-right">
                        <?php $where = ($_where == '' ? '' : $_where.' AND ').'status = '.$project_status['id']; ?>
                        <a href="<?php echo admin_url('projects');?>">
                         <h3 class="bold"><?php echo total_rows(db_prefix().'projects',$where); ?></h3>
                         <span style="color:<?php echo $project_status['color']; ?>" project-status-<?php echo $project_status['id']; ?>>
                         <?php echo $project_status['name']; ?>
                         </span>
                       </a>
                     </div>
                     <?php } ?>
                    </div>
                    <hr class="hr-panel-heading"> -->

                    <!-- Deals -->

                  <!--   <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Deals by status</h4>
                        </div>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="#" data-cview="" onclick="dt_custom_view('ticket_status_1','.tickets-table','ticket_status_1',true); return false;">
                                <h3 class="bold">1</h3>
                                <span style="color:#ff2d42">
                                New      </span>
                            </a>
                        </div>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="#" data-cview="" onclick="dt_custom_view('ticket_status_2','.tickets-table','ticket_status_2',true); return false;">
                                <h3 class="bold">0</h3>
                                <span style="color:#84c529">
                                In progress      </span>
                            </a>
                        </div>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="#" data-cview="" onclick="dt_custom_view('ticket_status_3','.tickets-table','ticket_status_3',true); return false;">
                                <h3 class="bold">1</h3>
                                <span style="color:#0000ff">
                                Resolved      </span>
                            </a>
                        </div>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="#" data-cview="" onclick="dt_custom_view('ticket_status_4','.tickets-table','ticket_status_4',true); return false;">
                                <h3 class="bold">0</h3>
                                <span style="color:#c0c0c0">
                                On Hold      </span>
                            </a>
                        </div>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                            <a href="#" data-cview="" onclick="dt_custom_view('ticket_status_5','.tickets-table','ticket_status_5',true); return false;">
                                <h3 class="bold">0</h3>
                                <span style="color:#03a9f4">
                                Closed      </span>
                            </a>
                        </div>
                    </div>
                    <hr class="hr-panel-heading"> -->

                     <!-- Tasks -->

                   <!--  <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">Tasks by status</h4>
                        </div>
                          <?php foreach(tasks_summary_data((isset($rel_id) ? $rel_id : null),(isset($rel_type) ? $rel_type : null)) as $summary){ ?>
                            <div class="col-md-2 col-xs-6 border-right">
                              <a href="<?php echo admin_url('tasks');?>">
                                <h3 class="bold"><?php echo $summary['total_tasks']; ?></h3>
                                <span style="color:<?php echo $summary['color']; ?>" class="">
                                  <?php echo $summary['name']; ?>
                                </span>
                              </a>
                             
                            </div>
                            <?php } ?>
                         
                    </div> -->
                    <hr class="hr-panel-heading">

                    <!-- <div class="row">
                        <div class="col-md-12">
                            <h4 class="no-margin">GAP by status</h4>
                        </div>
                        <?php 
                        foreach($gap_statuses as $status){
                        $_where = '';
                        
                        $_where = 'status_id='.$status['id'];
                        
                        ?>

                        data-cview="ticket_status_<?php echo $status['ticketstatusid']; ?>" 
                             onclick="dt_custom_view('ticket_status_<?php echo $status['ticketstatusid']; ?>','.tickets-table','ticket_status_<?php echo $status['ticketstatusid']; ?>',true); return false;"
                             
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                          <a href="<?php echo admin_url('gaps');?>">
                            <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
                            <span style="">
                              <?php echo $status['status_title']; ?>
                            </span>
                          </a>
                        </div>
                      <?php } ?>
                    </div> 
                    <hr class="hr-panel-heading">
                  -->



                    
                </div>
            </div>
            </div>
           
        </div>
        <div class="row">
            <div class="col-md-12" data-container="left-8">
                <?php render_dashboard_widgets('left-8'); ?>
            </div>
        </div>
    </div>

</div>
 <script>
    app.calendarIDs = '<?php echo json_encode($google_ids_calendars); ?>';
 </script>
<?php init_tail(); ?>
 <?php $this->load->view('admin/utilities/calendar_template'); ?>
 <?php $this->load->view('admin/dashboard/dashboard_js'); ?> 
</body>
</html>