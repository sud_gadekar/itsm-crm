<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_region(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New Item Region'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($regions) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Item Region Title'); ?></th>
								<th><?php echo _l('options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($regions as $region){ ?>
								<tr>
									<td><?php echo $region['id']; ?></td>
									<td><a href="#" onclick="edit_region(this,<?php echo $region['id']; ?>);return false;" data-name="<?php echo $region['region_title']; ?>"><?php echo $region['region_title']; ?></a></td>
									<td>
										<a href="#" onclick="edit_region(this,<?php echo $region['id']; ?>); return false" data-name="<?php echo $region['region_title']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('invoice_items/delete_region/'.$region['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No Items Regions Found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="region" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('invoice_items/region')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Item Region Edit'); ?></span>
					<span class="add-title"><?php echo _l('Item Region Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('region_title','Item Region Title'); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_items_regions);
		$('#region').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#region input[name="region_title"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_items_regions(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_region(){
		$('#region').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_region(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#region input[name="region_title"]').val(name);
		$('#region').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
