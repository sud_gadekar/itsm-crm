<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'gap_report_form')); ?>
<div id="gaps-report" class="hide">
   <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label for="gap_status"><?php echo _l('status'); ?></label>
            <select name="gap_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <?php foreach($gap_statuses as $status){  ?>
               <option value="<?php echo $status['id']; ?>"><?php echo $status['status_title'] ?></option>
            <?php } ?>
            </select>
         </div>
      </div>
      <!-- <div class="col-md-3">
         <div class="form-group">
              <label for="gap_clientid">Customer</label>
               <select name="gap_clientid" class="selectpicker"  data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($clients as $client){  ?>
                  <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="email">Send Mail</label>
            <input type="email" name="email" id="email" class="form-control" required>
            <input type="hidden" name="pdfname" value="GAP Report">
         </div>
      </div>

      <input type="hidden" name="tableData" id="input_hidden_field" value="">
      
      <div class="col-md-3"  style="text-align: center;margin-top: 2.3rem;">
            <button type="submit" class="btn btn-info incident_mail_send">Send Mail</button>
      </div>
  
</div>
<table class="table table-gaps-report scroll-responsive">
   <thead>
      <tr>
         <th><?php echo _l('#'); ?></th>
         <th><?php echo _l('Company'); ?></th>
         <th><?php echo _l('GAP Name'); ?></th>
         <th><?php echo _l('GAP Owner'); ?></th>
         <th><?php echo _l('GAP Category'); ?></th>
         <th><?php echo _l('GAP Impact'); ?></th>
         <th><?php echo _l('GAP Status'); ?></th>
         <th><?php echo _l('Created Date'); ?></th>
      </tr>
   </thead>
   <tbody></tbody>
   
</table>
</div>
<?php echo form_close();  ?>