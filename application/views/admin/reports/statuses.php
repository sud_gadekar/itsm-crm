<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
            <div class="row">
                <div class="col-md-12 border-right"> 
                  <h4 class="no-margin font-medium"><i class="fa fa-area-chart" aria-hidden="true"></i> <?php echo _l('Helpdesk Report'); ?></h4>
                  <hr />
                </div>
                <div class="col-md-4 border-right">
                  <!-- <p><a href="#" class="font-medium" onclick="init_report(this,'inventorys-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('Inventory'); ?></a></p>
                  <hr class="hr-10" />
                  <p><a href="#" class="font-medium" onclick="init_report(this,'backups-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('BackUp'); ?></a></p>
                  <hr class="hr-10" />
                  <p><a href="#" class="font-medium" onclick="init_report(this,'gaps-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('GAP'); ?></a></p>
                  <hr class="hr-10" />
                  <p><a href="#" class="font-medium" onclick="init_report(this,'tasks-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('Task'); ?></a></p> -->
                  <p><a href="#" class="font-medium" onclick="init_report(this,'incidents-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('Support Request'); ?></a></p>
                  <hr class="hr-10" />
                  <p><a href="#" class="font-medium" onclick="init_report(this,'service_requests-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('Service Request'); ?></a></p>
                  <hr class="hr-10" />
                </div>
                <div class="col-md-4 border-right">
                  <p><a href="#" class="font-medium" onclick="init_report(this,'all_tickets-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('All Requests'); ?></a></p>
                  <hr class="hr-10" />
                  <p><a href="#" class="font-medium" onclick="init_report(this,'staff_tickets-report'); return false;"><i class="fa fa-caret-down" aria-hidden="true"></i> <?php echo _l('Staff Total Tickets'); ?></a></p>
                  <hr class="hr-10" />
                </div>
                <div class="col-md-12 bg-light-gray border-radius-4">
                  <div class="bg-light-gray border-radius-4">
                    <div class="p8">
                      <?php /*if(isset($currencies)){ ?>
                      <div id="currency" class="form-group hide">
                        <label for="currency"><i class="fa fa-question-circle" data-toggle="tooltip" title="<?php echo _l('report_sales_base_currency_select_explanation'); ?>"></i> <?php echo _l('currency'); ?></label><br />
                        <select class="selectpicker" name="currency" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                          <?php foreach($currencies as $currency){
                          $selected = '';
                          if($currency['isdefault'] == 1){
                          $selected = 'selected';
                          }
                          ?>
                          <option value="<?php echo $currency['id']; ?>" <?php echo $selected; ?>><?php echo $currency['name']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <?php }*/ ?>
                      <div id="income-years" class="hide mbot15">
                        <label for="payments_years"><?php echo _l('year'); ?></label><br />
                        <select class="selectpicker" name="payments_years" data-width="100%" onchange="total_income_bar_report();" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                          <?php foreach($payments_years as $year) { ?>
                          <option value="<?php echo $year['year']; ?>"<?php if($year['year'] == date('Y')){echo 'selected';} ?>>
                            <?php echo $year['year']; ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                      <div id="customers-list" class="hide form-group col-sm-4">
                        <label for="report_clientid">Customer</label>
                        <select name="report_clientid" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                          <option value="" selected=""><?php echo 'All' ?></option>
                          <?php foreach($clients as $client){  ?>
                              <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div id="category-list" class="hide form-group col-sm-4">
                        <label for="service_category"><?php echo _l('Service Category'); ?></label>
                        <select name="service_category" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
                            <?php foreach($services as $service){ ?>
                            <option value="<?php echo $service['serviceid']; ?>"><?php echo $service['name'] ?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <div id="status-list" class="hide form-group col-sm-4">
                        <label for="incident_status"><?php echo _l('Status'); ?></label>
                        <select name="incident_status[]" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
                            <?php foreach($incident_statuses as $status){ if($status ==5){continue;} ?>
                            <option value="<?php echo $status['ticketstatusid']; ?>"><?php echo $status['name'] ?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <div id="sr-status-list" class="hide form-group col-sm-4">
                        <label for="service_request_status"><?php echo _l('Status (Service Request)'); ?></label>
                        <select name="service_request_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
                          <?php foreach($service_request_statuses as $status){ if($status ==5){continue;} ?>
                          <option value="<?php echo $status['service_requeststatusid']; ?>"><?php echo $status['name'] ?></option>
                        <?php } ?>
                        </select>
                      </div>
                      <div id="staff-list" class="hide form-group col-sm-4">
                        <label for="staff_ticket_staffid">Staff</label>
                        <select name="staff_ticket_staffid" class="selectpicker" multiple data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                          <option value="" selected=""><?php echo 'All' ?></option>
                          <?php foreach($staffs as $staff){  ?>
                              <option value="<?php echo $staff['staffid']; ?>"><?php echo $staff['firstname'].' '.$staff['lastname'] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div id="last-reply-list" class="hide form-group col-sm-4">
                        <label for="staff_ticket_lastrply">Last Reply</label>
                        <select name="staff_ticket_lastrply" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
                          <option value="" selected=""><?php echo 'All' ?></option>
                          <option value="8">8 Hrs</option>
                          <option value="12">12 Hrs</option>
                          <option value="24">24 Hrs</option>
                          <option value="48">48 Hrs</option>
                          <option value="72">72 Hrs</option>
                        </select>
                      </div>
                      <div class="form-group hide col-sm-4" id="report-time">
                        <label for="months-report"><?php echo _l('period_datepicker'); ?></label><br />
                        <select class="selectpicker" name="months-report" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                          <option value=""><?php echo _l('report_sales_months_all_time'); ?></option>
                          <option value="this_month"><?php echo _l('this_month'); ?></option>
                          <option value="1"><?php echo _l('last_month'); ?></option>
                          <option value="this_year"><?php echo _l('this_year'); ?></option>
                          <option value="last_year"><?php echo _l('last_year'); ?></option>
                          <option value="3" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-2 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_three_months'); ?></option>
                          <option value="6" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-5 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_six_months'); ?></option>
                          <option value="12" data-subtext="<?php echo _d(date('Y-m-01', strtotime("-11 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_twelve_months'); ?></option>
                          <option value="custom"><?php echo _l('period_datepicker'); ?></option>
                        </select>
                      </div>
                      <div id="date-range" class="hide mbot15 col-sm-4">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                            <div class="input-group date">
                              <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                            <div class="input-group date">
                              <input type="text" class="form-control datepicker" disabled="disabled" id="report-to" name="report-to">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="report" class="hide">
                <hr class="hr-panel-heading" />
                <h4 class="no-mtop"><?php echo _l('reports_sales_generated_report'); ?></h4>
                <hr class="hr-panel-heading" />
                <?php $this->load->view('admin/reports/includes/sales_income'); ?>
                <?php $this->load->view('admin/reports/includes/sales_payment_modes'); ?>
                <?php $this->load->view('admin/reports/includes/sales_customers_groups'); ?>
                <?php $this->load->view('admin/reports/includes/sales_customers'); ?>
                <?php $this->load->view('admin/reports/includes/sales_invoices'); ?>
                <?php $this->load->view('admin/reports/incidents'); ?>
                <?php $this->load->view('admin/reports/tasks'); ?>
                <?php $this->load->view('admin/reports/service_requests'); ?>
                <?php $this->load->view('admin/reports/inventorys'); ?>
                <?php $this->load->view('admin/reports/gaps'); ?>
                <?php $this->load->view('admin/reports/backups'); ?>
                <?php $this->load->view('admin/reports/includes/sales_credit_notes'); ?>
                <?php $this->load->view('admin/reports/includes/sales_items'); ?>
                <?php $this->load->view('admin/reports/includes/sales_estimates'); ?>
                <?php $this->load->view('admin/reports/includes/sales_payments'); ?>
                <?php $this->load->view('admin/reports/includes/sales_proposals'); ?>
                <?php $this->load->view('admin/reports/all_tickets'); ?>
                <?php $this->load->view('admin/reports/staff_tickets'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php init_tail(); ?>
  <?php $this->load->view('admin/reports/includes/sales_js'); ?>

<!-- <script>
      $('select[name="months-report"]').on('change', function() {
         alert( this.value ); 
      });
 </script> -->

</body>
</html>