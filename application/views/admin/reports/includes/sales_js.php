<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
 var salesChart;
 var groupsChart;
 var paymentMethodsChart;
 var customersTable;
 var report_from = $('input[name="report-from"]');
 var report_to = $('input[name="report-to"]');
 var report_customers = $('#customers-report');
 var report_customers_groups = $('#customers-group');
 var report_invoices = $('#invoices-report');
 var report_incidents = $('#incidents-report');
 var report_backups = $('#backups-report');
 var report_tasks = $('#tasks-report');
 var report_service_requests = $('#service_requests-report');
 var report_all_tickets = $('#all_tickets-report');
 var report_staff_tickets = $('#staff_tickets-report');

 var report_inventorys = $('#inventorys-report');

 var report_gaps = $('#gaps-report');

 var report_estimates = $('#estimates-report');
 var report_proposals = $('#proposals-reports');
 var report_items = $('#items-report');
 var report_credit_notes = $('#credit-notes');
 var report_payments_received = $('#payments-received-report');
 var date_range = $('#date-range');
 var report_from_choose = $('#report-time');
 var customers_list_choose = $('#customers-list');
 var category_list_choose = $('#category-list');
 var status_list_choose = $('#status-list');
 var sr_status_list_choose = $('#sr-status-list');
 var staff_list_choose = $('#staff-list');
 var last_reply_list_choose = $('#last-reply-list');
 var fnServerParams = {
   "report_months": '[name="months-report"]',
   "report_from": '[name="report-from"]',
   "report_to": '[name="report-to"]',
   "report_currency": '[name="currency"]',
   "invoice_status": '[name="invoice_status"]',
   "incident_status": '[name="incident_status[]"]',
   "incident_clientid": '[name="incident_clientid"]',
   "report_clientid": '[name="report_clientid"]',
   "length":'[name="DataTables_Table_0_length"]',
   "service_request_status": '[name="service_request_status"]',
   "request_for": '[name="request_for"]',
   "service_request_clientid": '[name="service_request_clientid"]',
   "all_ticket_status": '[name="all_ticket_status"]',
   "all_ticket_clientid": '[name="all_ticket_clientid"]',

   "staff_ticket_status": '[name="staff_ticket_status"]',
   "staff_ticket_staffid": '[name="staff_ticket_staffid"]',
   "staff_ticket_clientid": '[name="staff_ticket_clientid"]',
   "staff_ticket_reqtype": '[name="staff_ticket_reqtype"]',
   "staff_ticket_lastrply": '[name="staff_ticket_lastrply"]',
   "service_category": '[name="service_category"]',

   "inventory_status": '[name="inventory_status"]',
   "inventory_clientid": '[name="inventory_clientid"]',

   "task_status": '[name="task_status"]',
   "task_clientid": '[name="task_clientid"]',

   "task_assigned": '[name="task_assigned"]',

   "gap_status": '[name="gap_status"]',
   "gap_clientid": '[name="gap_clientid"]',

   "backup_status": '[name="backup_status"]',
   "backup_clientid": '[name="backup_clientid"]',

   "estimate_status": '[name="estimate_status"]',
   "sale_agent_invoices": '[name="sale_agent_invoices"]',
   "sale_agent_incidents": '[name="sale_agent_incidents"]',
   "sale_agent_incidents": '[name="sale_agent_incidents"]',

   "sale_agent_inventorys": '[name="sale_agent_inventorys"]',
   "sale_agent_inventorys": '[name="sale_agent_inventorys"]',

   "sale_agent_service_requests": '[name="sale_agent_service_requests"]',
   "sale_agent_service_requests": '[name="sale_agent_service_requests"]',

   "sale_agent_all_tickets": '[name="sale_agent_all_tickets"]',
   "sale_agent_all_tickets": '[name="sale_agent_all_tickets"]',

   "sale_agent_staff_tickets": '[name="sale_agent_staff_tickets"]',
   "sale_agent_staff_tickets": '[name="sale_agent_staff_tickets"]',

   "sale_agent_tasks": '[name="sale_agent_tasks"]',
   "sale_agent_tasks": '[name="sale_agent_tasks"]',

    "sale_agent_gaps": '[name="sale_agent_gaps"]',
   "sale_agent_gaps": '[name="sale_agent_gaps"]',

   "sale_agent_backups": '[name="sale_agent_backups"]',
   "sale_agent_backups": '[name="sale_agent_backups"]',

   "sale_agent_items": '[name="sale_agent_items"]',
   "sale_agent_estimates": '[name="sale_agent_estimates"]',
   "proposals_sale_agents": '[name="proposals_sale_agents"]',
   "proposal_status": '[name="proposal_status"]',
   "credit_note_status": '[name="credit_note_status"]',
 }
 $(function() {
   $('select[name="currency"],select[name="invoice_status"], select[name="incident_status[]"], select[name="incident_clientid"], select[name="report_clientid"], select[name="service_request_status"], select[name="request_for"], select[name="service_request_clientid"], select[name="all_ticket_status"], select[name="all_ticket_clientid"], select[name="staff_ticket_status"], select[name="staff_ticket_staffid"], select[name="staff_ticket_clientid"], select[name="staff_ticket_reqtype"], select[name="staff_ticket_lastrply"], select[name="service_category"], select[name="task_status"], select[name="task_assigned"], select[name="task_clientid"], select[name="inventory_status"], select[name="inventory_clientid"], select[name="gap_status"], select[name="gap_clientid"], select[name="backup_status"], select[name="backup_clientid"], select[name="estimate_status"],select[name="sale_agent_invoices"], select[name="sale_agent_invoices"],select[name="sale_agent_items"],select[name="sale_agent_estimates"],select[name="payments_years"],select[name="proposals_sale_agents"],select[name="proposal_status"],select[name="credit_note_status"]').on('change', function() {
     gen_reports();
   });

   report_from.on('change', function() {
     var val = $(this).val();
     var report_to_val = report_to.val();
     if (val != '') {
       report_to.attr('disabled', false);
       if (report_to_val != '') {
         gen_reports();
       }
     } else {
       report_to.attr('disabled', true);
     }
   });

   report_to.on('change', function() {
     var val = $(this).val();
     if (val != '') {
       gen_reports();
     }
   });

   $('select[name="months-report"]').on('change', function() {
     var val = $(this).val();
     report_to.attr('disabled', true);
     report_to.val('');
     report_from.val('');
     if (val == 'custom') {
       date_range.addClass('fadeIn').removeClass('hide');
       return;
     } else {
       if (!date_range.hasClass('hide')) {
         date_range.removeClass('fadeIn').addClass('hide');
       }
     }
     gen_reports();
   });

   $('.table-payments-received-report').on('draw.dt', function() {
     var paymentReceivedReportsTable = $(this).DataTable();
     var sums = paymentReceivedReportsTable.ajax.json().sums;
     $(this).find('tfoot').addClass('bold');
     $(this).find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?> (<?php echo _l('per_page'); ?>)");
     $(this).find('tfoot td.total').html(sums.total_amount);
   });

   $('.table-proposals-report').on('draw.dt', function() {
     var proposalsReportTable = $(this).DataTable();
     var sums = proposalsReportTable.ajax.json().sums;
      add_common_footer_sums($(this), sums);
      <?php foreach($proposal_taxes as $key => $tax){ ?>
        $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
     <?php } ?>
   });

   $('.table-invoices-report').on('draw.dt', function() {
     var invoiceReportsTable = $(this).DataTable();
     var sums = invoiceReportsTable.ajax.json().sums;
     add_common_footer_sums($(this),sums);
     $(this).find('tfoot td.amount_open').html(sums.amount_open);
     $(this).find('tfoot td.applied_credits').html(sums.applied_credits);
     <?php foreach($invoice_taxes as $key => $tax){ ?>
        $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
     <?php } ?>
   });

    $('.table-incidents-report').on('draw.dt', function() {
         var incidentReportsTable = $(this).DataTable();
    });

    $('.table-service_requests-report').on('draw.dt', function() {
         var service_requestReportsTable = $(this).DataTable();
    });

    $('.table-all_tickets-report').on('draw.dt', function() {
         var all_ticketReportsTable = $(this).DataTable();
    });

    $('.table-staff_tickets-report').on('draw.dt', function() {
         var staff_ticketReportsTable = $(this).DataTable();
    });

    $('.table-tasks-report').on('draw.dt', function() {
         var taskReportsTable = $(this).DataTable();
    });

    $('.table-inventorys-report').on('draw.dt', function() {
         var inventoryReportsTable = $(this).DataTable();
    });

    $('.table-gaps-report').on('draw.dt', function() {
         var gapReportsTable = $(this).DataTable();
    });

    $('.table-backups-report').on('draw.dt', function() {
         var backupReportsTable = $(this).DataTable();
    });

    $('.table-credit-notes-report').on('draw.dt', function() {
       var creditNotesTable = $(this).DataTable();
       var sums = creditNotesTable.ajax.json().sums;
       add_common_footer_sums($(this),sums);
       $(this).find('tfoot td.remaining_amount').html(sums.remaining_amount);
       <?php foreach($credit_note_taxes as $key => $tax){ ?>
          $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
       <?php } ?>
   });

   $('.table-estimates-report').on('draw.dt', function() {
     var estimatesReportsTable = $(this).DataTable();
     var sums = estimatesReportsTable.ajax.json().sums;
     add_common_footer_sums($(this),sums);
     <?php foreach($estimate_taxes as $key => $tax){ ?>
        $(this).find('tfoot td.total_tax_single_<?php echo $key; ?>').html(sums['total_tax_single_<?php echo $key; ?>']);
     <?php } ?>
   });

   $('.table-items-report').on('draw.dt', function() {
     var itemsTable = $(this).DataTable();
     var sums = itemsTable.ajax.json().sums;
     $(this).find('tfoot').addClass('bold');
     $(this).find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?> (<?php echo _l('per_page'); ?>)");
     $(this).find('tfoot td.amount').html(sums.total_amount);
     $(this).find('tfoot td.qty').html(sums.total_qty);
   });

 });

  function add_common_footer_sums(table,sums) {
       table.find('tfoot').addClass('bold');
       table.find('tfoot td').eq(0).html("<?php echo _l('invoice_total'); ?> (<?php echo _l('per_page'); ?>)");
       table.find('tfoot td.subtotal').html(sums.subtotal);
       table.find('tfoot td.total').html(sums.total);
       table.find('tfoot td.total_tax').html(sums.total_tax);
       table.find('tfoot td.discount_total').html(sums.discount_total);
       table.find('tfoot td.adjustment').html(sums.adjustment);
  }

 function init_report(e, type) {
   var report_wrapper = $('#report');

   if (report_wrapper.hasClass('hide')) {
        report_wrapper.removeClass('hide');
   }

   $('head title').html($(e).text());
   $('.customers-group-gen').addClass('hide');

   report_credit_notes.addClass('hide');
   report_customers_groups.addClass('hide');
   report_customers.addClass('hide');
   report_invoices.addClass('hide');
   report_incidents.addClass('hide');
   report_service_requests.addClass('hide');
   report_all_tickets.addClass('hide');
   report_staff_tickets.addClass('hide');
   report_tasks.addClass('hide');
   report_inventorys.addClass('hide');
   report_gaps.addClass('hide');
   report_backups.addClass('hide');
   report_estimates.addClass('hide');
   report_payments_received.addClass('hide');
   report_items.addClass('hide');
   report_proposals.addClass('hide');

   $('#income-years').addClass('hide');
   $('.chart-income').addClass('hide');
   $('.chart-payment-modes').addClass('hide');


   report_from_choose.addClass('hide');
   customers_list_choose.addClass('hide');
   category_list_choose.addClass('hide');
   status_list_choose.addClass('hide');
   sr_status_list_choose.addClass('hide');
   staff_list_choose.addClass('hide');
   last_reply_list_choose.addClass('hide');

   $('select[name="months-report"]').selectpicker('val', 'this_month');
   // Clear custom date picker
       report_to.val('');
       report_from.val('');
       $('#currency').removeClass('hide');

       if (type != 'total-income' && type != 'payment-modes') {
         report_from_choose.removeClass('hide');
         customers_list_choose.removeClass('hide');
         category_list_choose.removeClass('hide');
         status_list_choose.removeClass('hide');
         staff_list_choose.removeClass('hide');
         last_reply_list_choose.removeClass('hide');
       }

       if (type == 'total-income') {
         $('.chart-income').removeClass('hide');
         $('#income-years').removeClass('hide');
         date_range.addClass('hide');
       } else if (type == 'customers-report') {
         report_customers.removeClass('hide');
       } else if (type == 'customers-group') {
         $('.customers-group-gen').removeClass('hide');
       } else if (type == 'invoices-report') {
         report_invoices.removeClass('hide');
       } else if (type == 'incidents-report') {
         report_incidents.removeClass('hide');
       } else if (type == 'service_requests-report') {
         report_service_requests.removeClass('hide');
        //  status_list_choose.addClass('hide');
        //  sr_status_list_choose.removeClass('hide');
       } else if (type == 'all_tickets-report') {
         report_all_tickets.removeClass('hide');
       } else if (type == 'staff_tickets-report') {
         customers_list_choose.addClass('hide');
         category_list_choose.addClass('hide');
         status_list_choose.addClass('hide');
         report_staff_tickets.removeClass('hide');
       } else if (type == 'tasks-report') {
         report_tasks.removeClass('hide');
       } else if (type == 'inventorys-report') {
         report_inventorys.removeClass('hide');
       } else if (type == 'gaps-report') {
         report_gaps.removeClass('hide');
       } else if (type == 'backups-report') {
         report_backups.removeClass('hide');
       } else if (type == 'credit-notes') {
         report_credit_notes.removeClass('hide');
       } else if (type == 'payment-modes') {
         $('.chart-payment-modes').removeClass('hide');
         $('#income-years').removeClass('hide');
       } else if (type == 'payments-received') {
         report_payments_received.removeClass('hide');
       } else if (type == 'estimates-report') {
         report_estimates.removeClass('hide');
       } else if(type == 'proposals-report'){
        report_proposals.removeClass('hide');
      } else if(type == 'items-report'){
          report_items.removeClass('hide');
      }
      gen_reports();
    }


   // Generate total income bar
   function total_income_bar_report() {
     if (typeof(salesChart) !== 'undefined') {
       salesChart.destroy();
     }
     var data = {};
     data.year = $('select[name="payments_years"]').val();
     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/total_income_report', data).done(function(response) {
       response = JSON.parse(response);
       salesChart = new Chart($('#chart-income'), {
         type: 'bar',
         data: response,
         options: {
           responsive: true,
           maintainAspectRatio:false,
           legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          },
        }
      })
     });
   }

   function report_by_payment_modes() {
     if (typeof(paymentMethodsChart) !== 'undefined') {
       paymentMethodsChart.destroy();
     }
     var data = {};
     data.year = $('select[name="payments_years"]').val();
     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/report_by_payment_modes', data).done(function(response) {
       response = JSON.parse(response);
       paymentMethodsChart = new Chart($('#chart-payment-modes'), {
         type: 'bar',
         data: response,
         options: {
           responsive: true,
           maintainAspectRatio:false,
           scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          },
        }
      })
     });
   }
   // Generate customers report
   function customers_report() {
     if ($.fn.DataTable.isDataTable('.table-customers-report')) {
       $('.table-customers-report').DataTable().destroy();
     }
     initDataTable('.table-customers-report', admin_url + 'reports/customers_report', false, false, fnServerParams, [0, 'asc']);
   }

   

   function report_by_customer_groups() {
     if (typeof(groupsChart) !== 'undefined') {
       groupsChart.destroy();
     }
     var data = {};
     data.months_report = $('select[name="months-report"]').val();
     data.report_from = report_from.val();
     data.report_to = report_to.val();

     var currency = $('#currency');
     if (currency.length > 0) {
       data.report_currency = $('select[name="currency"]').val();
     }
     $.post(admin_url + 'reports/report_by_customer_groups', data).done(function(response) {
       response = JSON.parse(response);
       groupsChart = new Chart($('#customers-group-gen'), {
         type: 'line',
         data: response,
         options:{
          maintainAspectRatio:false,
          legend: {
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
              }
            }]
          }}
        });
     });
   }
   function invoices_report() {
     if ($.fn.DataTable.isDataTable('.table-invoices-report')) {
       $('.table-invoices-report').DataTable().destroy();
     }
     initDataTable('.table-invoices-report', admin_url + 'reports/invoices_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]).column(2).visible(false, false).columns.adjust();
   }

   function incidents_report() {

    if ($.fn.DataTable.isDataTable('.table-incidents-report')) 
    {
      $('.table-incidents-report').DataTable().destroy();
    }
    initDataTable('.table-incidents-report', admin_url + 'reports/incidents_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);

      // var clientid= $("select[name='incident_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#incidents-report #email").val(primary_mail[0].email); 
              $('#incidents-report #input_hidden_field').val($('.table-incidents-report').prop('outerHTML'));
            }
        });
      }

      $.ajax({
           url:'<?=admin_url()?>reports/get_incident_service_count',
           method:"POST",
           //data:{piechart:true},
           success:function(response_data){
            if(response_data)
            {
              console.log(response_data);
              var keys = Object.keys(JSON.parse(response_data));
              var values = Object.values(JSON.parse(response_data));

              var ctx = document.getElementById("incident_report_status_pie-chart").getContext('2d');
              Chart.defaults.global.legend.display = false;
              if(window.myChart  !== undefined && window.myChart !== null)
              {
                  window.myChart.destroy();
              }
              window.myChart = new Chart(ctx, {
                  type: 'bar',
                  responsive: true,
                  maintainAspectRatio: false,
                  data: {
                    labels: keys,
                    datasets: [{
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                      data: values
                    }]
                  },

                  options: {
                    animation: {
                      onComplete: function() {
                        //window.myChart.options.animation.onComplete = null; //disable after first render
                        console.log(window.myChart.toBase64Image());
                        // $.ajax({
                        //     url: '<?=admin_url()?>reports/store_incident_chart_image',
                        //     type: 'POST',
                        //     data:{data:window.myChart.toBase64Image()},
                        //     success: function (result){
                             
                        //     }
                        //  })
                      }
                    }
                  }
              });
            }
           }
      })

      $.ajax({
           url:'<?=admin_url()?>reports/get_incident_staff_count',
           method:"POST",
           //data:{piechart:true},
           success:function(staff_response_data){
            if(staff_response_data)
            {
              console.log(staff_response_data);
              var staff_keys = Object.keys(JSON.parse(staff_response_data));
              var staff_values = Object.values(JSON.parse(staff_response_data));

              var staff_ctx = document.getElementById("incident_report_staff_pie-chart").getContext('2d');
              Chart.defaults.global.legend.display = false;
              if(window.staff_myChart  !== undefined && window.staff_myChart !== null)
              {
                  window.staff_myChart.destroy();
              }
              window.staff_myChart = new Chart(staff_ctx, {
                  type: 'bar',
                  responsive: true,
                  maintainAspectRatio: false,
                  data: {
                    labels: staff_keys,
                    datasets: [{
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                      data: staff_values
                    }]
                  },

                  options: {
                    animation: {
                      onComplete: function() {
                        //window.staff_myChart.options.animation.onComplete = null; //disable after first render
                        console.log(window.staff_myChart.toBase64Image());
                        // $.ajax({
                        //     url: '<?=admin_url()?>reports/store_incident_chart_image',
                        //     type: 'POST',
                        //     data:{data:window.myChart.toBase64Image()},
                        //     success: function (result){
                             
                        //     }
                        //  })
                      }
                    }
                  }
              });
            }
           }
      })
   }


  function service_requests_report() {

     if ($.fn.DataTable.isDataTable('.table-service_requests-report')) {
       $('.table-service_requests-report').DataTable().destroy();
     }
     initDataTable('.table-service_requests-report', admin_url + 'reports/service_requests_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);

      // var clientid= $("select[name='service_request_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#service_requests-report #email").val(primary_mail[0].email); 
              $('#service_requests-report #input_hidden_field').val($('.table-service_requests-report').prop('outerHTML'));
            }
        });
      }

    $.ajax({  
       url:'<?=admin_url()?>reports/get_service_request_service_count',
       method:"POST",
       //data:{piechart:true},
       success:function(response_data){
        if(response_data)
        {
          console.log(response_data);
          var keys = Object.keys(JSON.parse(response_data));
          var values = Object.values(JSON.parse(response_data));

          var ctx1 = document.getElementById("service_request_report_status_pie-chart").getContext('2d');
          Chart.defaults.global.legend.display = false;
          if(window.myChart1  !== undefined && window.myChart1 !== null)
            {
                  window.myChart1.destroy();
            }
          window.myChart1 = new Chart(ctx1, {
              type: 'bar',
              responsive: true,
              maintainAspectRatio: true,
              data: {
                labels: keys,
                datasets: [{
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                  data: values
                }]
              },
              options: {
                    animation: {
                      onComplete: function() {
                       // window.myChart1.options.animation.onComplete = null; 
                        console.log(window.myChart1.toBase64Image());
                        $.ajax({
                            url: '<?=admin_url()?>reports/store_service_request_chart_image',
                            type: 'POST',
                            data:{data:window.myChart1.toBase64Image()}
                         })
                      }
                    }
                  }
          });
        }
       }
    });

    $.ajax({  
       url:'<?=admin_url()?>reports/get_service_request_requestfor_count',
       method:"POST",
       success:function(requestfor_response_data){
        if(requestfor_response_data)
        {
          console.log(requestfor_response_data);
          var requestfor_keys = Object.keys(JSON.parse(requestfor_response_data));
          var requestfor_values = Object.values(JSON.parse(requestfor_response_data));

          var ctx2 = document.getElementById("service_request_report_requestfor_pie-chart").getContext('2d');
          Chart.defaults.global.legend.display = false;
          if(window.myChart2  !== undefined && window.myChart2 !== null)
            {
                  window.myChart2.destroy();
            }
          window.myChart2 = new Chart(ctx2, {
              type: 'bar',
              responsive: true,
              maintainAspectRatio: true,
              data: {
                labels: requestfor_keys,
                datasets: [{
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                  data: requestfor_values
                }]
              },
              options: {
                animation: {
                  onComplete: function() {
                    //window.myChart2.options.animation.onComplete = null; 
                    console.log(window.myChart2.toBase64Image());
                    $.ajax({
                        url: '<?=admin_url()?>reports/store_service_request_requestfor_chart_image',
                        type: 'POST',
                        
                        data:{data:window.myChart2.toBase64Image()}
                     })
                  }
                }
              }
          });
        }
       }
    });

    $.ajax({
        url:'<?=admin_url()?>reports/get_service_request_staff_count',
        method:"POST",
        //data:{piechart:true},
        success:function(staff_response_data){
            if(staff_response_data)
            {
              console.log(staff_response_data);
              var staff_keys = Object.keys(JSON.parse(staff_response_data));
              var staff_values = Object.values(JSON.parse(staff_response_data));

              var staff_ctx = document.getElementById("service_request_report_staff_pie-chart").getContext('2d');
              Chart.defaults.global.legend.display = false;
              if(window.staff_myChart  !== undefined && window.staff_myChart !== null)
              {
                  window.staff_myChart.destroy();
              }
              window.staff_myChart = new Chart(staff_ctx, {
                  type: 'bar',
                  responsive: true,
                  maintainAspectRatio: false,
                  data: {
                    labels: staff_keys,
                    datasets: [{
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                      data: staff_values
                    }]
                  },

                  options: {
                    animation: {
                      onComplete: function() {
                        //window.staff_myChart.options.animation.onComplete = null; //disable after first render
                        console.log(window.staff_myChart.toBase64Image());
                        // $.ajax({
                        //     url: '<?=admin_url()?>reports/store_incident_chart_image',
                        //     type: 'POST',
                        //     data:{data:window.myChart.toBase64Image()},
                        //     success: function (result){
                             
                        //     }
                        //  })
                      }
                    }
                  }
              });
            }
        }
    })
  }

  function all_tickets_report() {

     if ($.fn.DataTable.isDataTable('.table-all_tickets-report')) {
       $('.table-all_tickets-report').DataTable().destroy();
     }
     initDataTable('.table-all_tickets-report', admin_url + 'reports/all_tickets_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);

      // var clientid= $("select[name='all_ticket_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#all_tickets-report #email").val(primary_mail[0].email); 
              $('#all_tickets-report #input_hidden_field').val($('.table-all_tickets-report').prop('outerHTML'));
            }
        });
      }

      $.ajax({  
        url:'<?=admin_url()?>reports/get_tickets_type_count',
        method:"POST",
        //data:{piechart:true},
        success:function(response_data){
          if(response_data){
            console.log(response_data);
            var keys = Object.keys(JSON.parse(response_data));
            var values = Object.values(JSON.parse(response_data));

            var ctx1 = document.getElementById("tickets_type_count_pie-chart").getContext('2d');
            Chart.defaults.global.legend.display = false;
            if(window.myChart1  !== undefined && window.myChart1 !== null)
              {
                    window.myChart1.destroy();
              }
            window.myChart1 = new Chart(ctx1, {
                type: 'bar',
                responsive: true,
                maintainAspectRatio: true,
                data: {
                  labels: keys,
                  datasets: [{
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                    data: values
                  }]
                },
                options: {
                      animation: {
                        onComplete: function() {
                        // window.myChart1.options.animation.onComplete = null; 
                          console.log(window.myChart1.toBase64Image());
                          // $.ajax({
                          //     url: '<?=admin_url()?>reports/store_service_request_chart_image',
                          //     type: 'POST',
                          //     data:{data:window.myChart1.toBase64Image()}
                          // })
                        }
                      }
                    }
            });
          }
        }
      });

      $.ajax({
        url:'<?=admin_url()?>reports/get_all_tickets_staff_count',
        method:"POST",
        //data:{piechart:true},
        success:function(staff_response_data){
            if(staff_response_data)
            {
              console.log(staff_response_data);
              var staff_keys = Object.keys(JSON.parse(staff_response_data));
              var staff_values = Object.values(JSON.parse(staff_response_data));

              var staff_ctx = document.getElementById("all_tickets_report_staff_pie-chart").getContext('2d');
              Chart.defaults.global.legend.display = false;
              if(window.staff_myChart  !== undefined && window.staff_myChart !== null)
              {
                  window.staff_myChart.destroy();
              }
              window.staff_myChart = new Chart(staff_ctx, {
                  type: 'bar',
                  responsive: true,
                  maintainAspectRatio: false,
                  data: {
                    labels: staff_keys,
                    datasets: [{
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                      data: staff_values
                    }]
                  },

                  options: {
                    animation: {
                      onComplete: function() {
                        //window.staff_myChart.options.animation.onComplete = null; //disable after first render
                        console.log(window.staff_myChart.toBase64Image());
                        // $.ajax({
                        //     url: '<?=admin_url()?>reports/store_incident_chart_image',
                        //     type: 'POST',
                        //     data:{data:window.myChart.toBase64Image()},
                        //     success: function (result){
                             
                        //     }
                        //  })
                      }
                    }
                  }
              });
            }
        }
      })
  }

  function staff_tickets_report() {

     if ($.fn.DataTable.isDataTable('.table-staff_tickets-report')) {
       $('.table-staff_tickets-report').DataTable().destroy();
     }
     initDataTable('.table-staff_tickets-report', admin_url + 'reports/staff_tickets_report', false, false, fnServerParams, []);
  }

   function tasks_report() {

     if ($.fn.DataTable.isDataTable('.table-tasks-report')) {
       $('.table-tasks-report').DataTable().destroy();
     }
     initDataTable('.table-tasks-report', admin_url + 'reports/tasks_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);
   }

   function inventorys_report() {

     if ($.fn.DataTable.isDataTable('.table-inventorys-report')) {
       $('.table-inventorys-report').DataTable().destroy();
     }
     initDataTable('.table-inventorys-report', admin_url + 'reports/inventorys_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);

      // var clientid= $("select[name='inventory_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#inventorys-report #email").val(primary_mail[0].email); 
              $('#inventorys-report #input_hidden_field').val($('.table-inventorys-report').prop('outerHTML'));
            }
        });
      }
   }

    function gaps_report() {

     if ($.fn.DataTable.isDataTable('.table-gaps-report')) {
       $('.table-gaps-report').DataTable().destroy();
     }
     initDataTable('.table-gaps-report', admin_url + 'reports/gaps_report', false, false, fnServerParams, [
       [2, 'desc'],
       [0, 'desc']
       ]);
      // var clientid= $("select[name='gap_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#gaps-report #email").val(primary_mail[0].email); 
              $('#gaps-report #input_hidden_field').val($('.table-gaps-report').prop('outerHTML'));
            }
        });
      }
   }

  function backups_report() {

     if ($.fn.DataTable.isDataTable('.table-backups-report')) {
       $('.table-backups-report').DataTable().destroy();
     }
    
     initDataTable('.table-backups-report', admin_url + 'reports/backups_report', false, false, fnServerParams,[
       [2, 'desc'],
       [0, 'desc']
       ]);
    
     $.ajax({
             url:'<?=admin_url()?>reports/get_backup_count',
             method:"POST",
             data:{piechart:true},
             success:function(data){
              if(data)
              {
                var nameArr = data.split(',');
                console.log(nameArr);

                new Chart(document.getElementById("backup_report_status_pie-chart"), {
                    type: 'pie',
                    data: {
                      labels: ["Success", "Failure", "Warning"],
                      datasets: [{
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                        data: [nameArr[0], nameArr[1], nameArr[2]]
                      }]
                    },
                });
                
                $('#success_count').val(nameArr[0]);
                $('#failure_count').val(nameArr[1]);
                $('#warning_count').val(nameArr[2]);
              }
             }
          });


      // var clientid= $("select[name='backup_clientid'] option:selected").val();
      var clientid= $("select[name='report_clientid'] option:selected").val();
      if(clientid)
      {
        $.ajax({
            url: '<?=admin_url()?>reports/get_primary_contact_mails',
            type: "POST",
            data:{
                  clientid    : clientid,
            },
            success: function (data){
              var primary_mail = JSON.parse(data);
              $("#backups-report #email").val(primary_mail[0].email); 
              $('#backups-report #input_hidden_field').val($('.table-backups-report').prop('outerHTML'));
            }
        });
      }
   }

  function credit_notes_report(){

     if ($.fn.DataTable.isDataTable('.table-credit-notes-report')) {
       $('.table-credit-notes-report').DataTable().destroy();
     }
     initDataTable('.table-credit-notes-report', admin_url + 'reports/credit_notes', false, false, fnServerParams,[1, 'desc']);

  }

 function estimates_report() {
   if ($.fn.DataTable.isDataTable('.table-estimates-report')) {
     $('.table-estimates-report').DataTable().destroy();
   }
   initDataTable('.table-estimates-report', admin_url + 'reports/estimates_report', false, false, fnServerParams, [
     [3, 'desc'],
     [0, 'desc']
     ]).column(3).visible(false, false).columns.adjust();
 }

   function payments_received_reports() {
     if ($.fn.DataTable.isDataTable('.table-payments-received-report')) {
       $('.table-payments-received-report').DataTable().destroy();
     }
     initDataTable('.table-payments-received-report', admin_url + 'reports/payments_received', false, false, fnServerParams, [1, 'desc']);
   }

   function proposals_report(){
   if ($.fn.DataTable.isDataTable('.table-proposals-report')) {
     $('.table-proposals-report').DataTable().destroy();
   }

   initDataTable('.table-proposals-report', admin_url + 'reports/proposals_report', false, false, fnServerParams, [0, 'desc']);
 }

 function items_report(){
   if ($.fn.DataTable.isDataTable('.table-items-report')) {
     $('.table-items-report').DataTable().destroy();
   }
   initDataTable('.table-items-report', admin_url + 'reports/items', false, false, fnServerParams, [0, 'asc']);
 }

   // Main generate report function
   function gen_reports() {

     if (!$('.chart-income').hasClass('hide')) {
       total_income_bar_report();
     } else if (!$('.chart-payment-modes').hasClass('hide')) {
       report_by_payment_modes();
     } else if (!report_customers.hasClass('hide')) {
       customers_report();
     } else if (!$('.customers-group-gen').hasClass('hide')) {
       report_by_customer_groups();
     } else if (!report_invoices.hasClass('hide')) {
       invoices_report();
     } else if (!report_incidents.hasClass('hide')) {
       incidents_report();
       //get_service_count();
     } else if (!report_service_requests.hasClass('hide')) {
       service_requests_report();
     } else if (!report_all_tickets.hasClass('hide')) {
       all_tickets_report();
     } else if (!report_staff_tickets.hasClass('hide')) {
       staff_tickets_report();
     } else if (!report_tasks.hasClass('hide')) {
       tasks_report();
     } else if (!report_inventorys.hasClass('hide')) {
       inventorys_report();
     } else if (!report_gaps.hasClass('hide')) {
       gaps_report();
     } else if (!report_backups.hasClass('hide')) {
       backups_report();
     } else if (!report_payments_received.hasClass('hide')) {
       payments_received_reports();
     } else if (!report_estimates.hasClass('hide')) {
       estimates_report();
     } else if(!report_proposals.hasClass('hide')){
      proposals_report();
    } else if(!report_items.hasClass('hide')) {
      items_report();
    } else if(!report_credit_notes.hasClass('hide')) {
      credit_notes_report();
    }
  }
</script>

<script type="text/javascript">
  
  $('#incidents-report #email').on('keyup', function() {
    $('#incidents-report #input_hidden_field').val($('.table-incidents-report').prop('outerHTML'));
  });

  $('#service_requests-report #email').on('keyup', function() {
      $('#service_requests-report #input_hidden_field').val($('.table-service_requests-report').prop('outerHTML'));
  });

  $('#all_tickets-report #email').on('keyup', function() {
      $('#all_tickets-report #input_hidden_field').val($('.table-all_tickets-report').prop('outerHTML'));
  });

  $('#staff_tickets-report #email').on('keyup', function() {
      $('#staff_tickets-report #input_hidden_field').val($('.table-staff_tickets-report').prop('outerHTML'));
  });

  $('#tasks-report #email').on('keyup', function() {
      $('#tasks-report #input_hidden_field').val($('.table-tasks-report').prop('outerHTML'));
  });

  $('#inventorys-report #email').on('keyup', function() {
      $('#inventorys-report #input_hidden_field').val($('.table-inventorys-report').prop('outerHTML'));
  });

  $('#backups-report #email').on('keyup', function() {
      $('#backups-report #input_hidden_field').val($('.table-backups-report').prop('outerHTML'));
  });

  $('#gaps-report #email').on('keyup', function() {
      $('#gaps-report #input_hidden_field').val($('.table-gaps-report').prop('outerHTML'));
  });


</script>

