<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'service_request_report_form')); ?>
<div id="service_requests-report" class="hide">
   <div class="row">
      <!-- <div class="col-md-3">
         <div class="form-group">
            <label for="service_request_status"><?php echo _l('Status'); ?></label>
            <select name="service_request_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <?php foreach($service_request_statuses as $status){ if($status ==5){continue;} ?>
               <option value="<?php echo $status['service_requeststatusid']; ?>"><?php echo $status['name'] ?></option>
            <?php } ?>
            </select>
         </div>
      </div> -->
      <!-- <div class="col-md-3">
         <div class="form-group">
              <label for="service_request_clientid">Customer</label>
               <select name="service_request_clientid" class="selectpicker"  data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($clients as $client){  ?>
                  <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="request_for"><?php echo _l('Request For'); ?></label>
            <select name="request_for" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($request_for as $request){ ?>
               <option value="<?php echo $request['requestid']; ?>"><?php echo $request['name'] ?></option>
            <?php } ?>
            </select>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="email">Send Mail</label>
            <input type="text" name="email" id="email" class="form-control">
            <input type="hidden" name="pdfname" value="Service Report">
         </div>
      </div>
      <input type="hidden" name="tableData" id="input_hidden_field" value="">   
      <div class="col-md-3"  style="text-align: center;margin-top: 2.3rem;">
            <button type="submit" class="btn btn-info incident_mail_send">Send Mail</button>
      </div>
</div>
<table class="table table-service_requests-report scroll-responsive">
   <thead>
      <tr>
         <th><?php echo _l('Request No'); ?></th>
         <th><?php echo _l('Subject'); ?></th>
         <th><?php echo _l('Contact'); ?></th>
         <!-- <th><?php/* echo _l('Assigned To');*/ ?></th> -->
         <!-- <th><?php/* echo _l('Email');*/ ?></th> -->
         <!-- <th><?php/* echo _l('Department');*/ ?></th> -->
         <th><?php echo _l('Request For'); ?></th> 
        <!--  <th><?php echo _l('Priority'); ?></th> -->
         <th><?php echo _l('Status'); ?></th>
          <th><?php echo _l('Service'); ?></th>
         <th><?php echo _l('Creation Date'); ?></th>
         <th><?php echo _l('Resolved Date'); ?></th>
         <th><?php echo _l('Resolved By'); ?></th>
      </tr>
   </thead>
   <tbody></tbody>
   
</table>
<div class="row">
      <div class="col-md-4 d-bl0ck m-auto">
       <div class="panel_s">
         <div class="panel-body padding-10">
           <div class="row">
             <div class="col-md-12 mbot10">
               <p class="padding-5"> <?php echo _l('Service request by service'); ?></p>
               <hr class="hr-panel-heading-dashboard">
               <canvas height="220" id="service_request_report_status_pie-chart"></canvas>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="col-md-4 d-bl0ck m-auto">
       <div class="panel_s">
         <div class="panel-body padding-10">
           <div class="row">
             <div class="col-md-12 mbot10">
               <p class="padding-5"> <?php echo _l('Service request by request for'); ?></p>
               <hr class="hr-panel-heading-dashboard">
               <canvas height="220" id="service_request_report_requestfor_pie-chart"></canvas>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="col-md-4 d-bl0ck m-auto">
       <div class="panel_s">
         <div class="panel-body padding-10">
           <div class="row">
             <div class="col-md-12 mbot10">
               <p class="padding-5"> <?php echo _l('Service Request by Staff'); ?></p>
               <hr class="hr-panel-heading-dashboard">
               <canvas height="220" id="service_request_report_staff_pie-chart"></canvas>
             </div>
           </div>
         </div>
       </div>
      </div>
  </div>
</div>
<?php echo form_close();  ?>
