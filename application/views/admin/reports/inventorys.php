<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'incident_report_form')); ?>
<div id="inventorys-report" class="hide">
   <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label for="inventory_status"><?php echo _l('Type'); ?></label>
            <select name="inventory_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <?php foreach($inventory_types as $type){  ?>
               <option value="<?php echo $type['id']; ?>"><?php echo $type['type_name'] ?></option>
            <?php } ?>
            </select>
         </div>
      </div>
      <!-- <div class="col-md-3">
         <div class="form-group">
              <label for="inventory_clientid">Customer</label>
               <select name="inventory_clientid" class="selectpicker"  data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($clients as $client){  ?>
                  <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="email">Send Mail</label>
            <input type="email" name="email" id="email" class="form-control" required>
            <input type="hidden" name="pdfname" value="Inventory Report">
         </div>
      </div>

      <input type="hidden" name="tableData" id="input_hidden_field" value="">
      
       <div class="col-md-3"  style="text-align: center;margin-top: 2.3rem;">
            <button type="submit" class="btn btn-info incident_mail_send">Send Mail</button>
       </div>
</div>
<table class="table table-inventorys-report scroll-responsive">
   <thead>
      <tr>
         <th><?php echo _l('Inventory Label'); ?></th>
         <th><?php echo _l('Contact'); ?></th>
         <th><?php echo _l('Type'); ?></th>
         <th><?php echo _l('Sub Type'); ?></th>
         <th><?php echo _l('Product Name'); ?></th>
         <th><?php echo _l('Host Name'); ?></th>
         <th><?php echo _l('Serial No'); ?></th>
         <th><?php echo _l('Purpose'); ?></th>
         <th><?php echo _l('CPU'); ?></th>
         <th><?php echo _l('RAM'); ?></th>
         <th><?php echo _l('Storage'); ?></th>
         <th><?php echo _l('Status'); ?></th>
         <th><?php echo _l('Location'); ?></th>
      </tr>
   </thead>
   <tbody></tbody>
   
</table>
</div>
<?php echo form_close();  ?>

