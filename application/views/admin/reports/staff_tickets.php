<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'staff_ticket_report_form')); ?>
<div id="staff_tickets-report" class="hide">
   <div class="row">
      <!-- <div class="col-md-3">
         <div class="form-group">
            <label for="staff_ticket_status"><?php echo _l('Status'); ?></label>
            <select name="staff_ticket_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <?php foreach($incident_statuses as $status){ if($status ==5){continue;} ?>
                  <option value="<?php echo $status['ticketstatusid']; ?>"><?php echo $status['name'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <!-- <div class="col-md-3">
         <div class="form-group">
              <label for="staff_ticket_clientid">Customer</label>
               <select name="staff_ticket_clientid" class="selectpicker"  data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($clients as $client){  ?>
                  <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="staff_ticket_reqtype">Request Type</label>
            <select name="staff_ticket_reqtype" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <option value="all" selected=""><?php echo 'All' ?></option>
               <option value="support">Support Request</option>
               <option value="service">Service Request</option>
            </select>
         </div>
      </div>
      <!-- <div class="col-md-3">
         <div class="form-group">
            <label for="staff_ticket_staffid">Staff</label>
            <select name="staff_ticket_staffid" class="selectpicker" multiple data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($staffs as $staff){  ?>
                  <option value="<?php echo $staff['staffid']; ?>"><?php echo $staff['firstname'].' '.$staff['lastname'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="staff_ticket_lastrply">Last Reply</label>
            <select name="staff_ticket_lastrply" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <option value="" selected=""><?php echo 'All' ?></option>
               <option value="8">8 Hrs</option>
               <option value="12">12 Hrs</option>
               <option value="24">24 Hrs</option>
               <option value="48">48 Hrs</option>
               <option value="72">72 Hrs</option>
            </select>
         </div>
      </div> -->
</div>
<table class="table table-staff_tickets-report scroll-responsive">
   <thead>
      <tr>
         <th><?php echo _l('Staff Name'); ?></th>
         <?php foreach($incident_statuses as $status){ if($status ==5){continue;} ?>
            <th id="<?php echo $status['ticketstatusid']; ?>"><?php echo _l($status['name']); ?></th>
         <?php } ?>
      </tr>
   </thead>
   <tbody></tbody>
   
</table>
</div>
<?php echo form_close();  ?>