<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'task_report_form')); ?>
<div id="tasks-report" class="hide">
   <div class="row">
      <div class="col-md-3">
         <div class="form-group">
            <label for="task_status"><?php echo _l('Status'); ?></label>
            <select name="task_status" class="selectpicker" multiple data-width="100%" data-none-selected-text="<?php echo _l('All'); ?>">
               <?php foreach($task_statuses as $status){ if($status ==5){continue;} ?>
               <option value="<?php echo $status['id']; ?>"><?php echo $status['name'] ?></option>
            <?php } ?>
            </select>
         </div>
      </div>
      <!-- <div class="col-md-3">
         <div class="form-group">
            <label for="task_assigned"><?php echo _l('Assigned To'); ?></label>
            <select name="task_assigned" class="selectpicker" data-width="100%" data-live-search="true">
               <option value="" selected><?php echo 'All' ?></option>
               <?php foreach($staffs as $staff){  ?>
               <option value="<?php echo $staff['staffid']; ?>">
                  <?php echo $staff['firstname'].' '.$staff['lastname'] ?>   
               </option>
            <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="email">Send Mail</label>
            <input type="email" name="email" id="email" class="form-control" required>
            <input type="hidden" name="pdfname" value="Task Report">
         </div>
      </div>

      <input type="hidden" name="tableData" id="input_hidden_field" value="">
      
      <div class="col-md-3"  style="text-align: center;margin-top: 2.3rem;">
            <button type="submit" class="btn btn-info incident_mail_send">Send Mail</button>
      </div>
     
</div>
<table class="table table-tasks-report scroll-responsive">
   <thead>
      <tr>
         
         <th><?php echo _l('#'); ?></th>
         <th><?php echo _l('Name'); ?></th>
         <th><?php echo _l('Status'); ?></th>
         <th><?php echo _l('Start Date'); ?></th>
         <th><?php echo _l('Due Date'); ?></th>
         <th><?php echo _l('Assigned To'); ?></th>
         <th><?php echo _l('Tags'); ?></th>
         <th><?php echo _l('Priority'); ?></th>
         
      </tr>
   </thead>
   <tbody></tbody>
   
</table>
</div>
<?php echo form_close();  ?>
