<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open(admin_url('reports/send_pdf_to_mail'),array('id'=>'backup_report_form')); ?>
<div id="backups-report" class="hide">
   <div class="row">
      
      <!-- <div class="col-md-4">
         <div class="form-group">
              <label for="backup_clientid">Customer</label>
               <select name="backup_clientid" class="selectpicker"  data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
               
               <option value="" selected=""><?php echo 'All' ?></option>
               <?php foreach($clients as $client){  ?>
                  <option value="<?php echo $client['userid']; ?>"><?php echo $client['company'] ?></option>
               <?php } ?>
            </select>
         </div>
      </div> -->
      <div class="col-md-3">
         <div class="form-group">
            <label for="email">Send Mail</label>
            <input type="email" name="email" id="email" class="form-control" required>
            <input type="hidden" name="pdfname" value="Backup Report">
         </div>
      </div>

      <input type="hidden" name="backup_id[]" value="">

      <input type="hidden" name="tableData" id="input_hidden_field" value="">
      
      <input type="hidden" name="success_count" id="success_count" value="">
      <input type="hidden" name="failure_count" id="failure_count" value="">
      <input type="hidden" name="warning_count" id="warning_count" value="">

       <div class="col-md-3"  style="text-align: center;margin-top: 2.3rem;">
            <button type="submit" class="btn btn-info incident_mail_send">Send Mail</button>
       </div>
 
   <div class="clearfix"></div>
</div>
<table class="table table-backups-report scroll-responsive">
   <thead>
      <tr>
         <th><?php echo _l('#'); ?></th>
         <th><?php echo _l('Customer'); ?></th>
         <th><?php echo _l('Backup policy'); ?></th>
         
         <th><?php echo _l('Inventory Product'); ?></th>
         <th><?php echo _l('Status'); ?></th>
         <th><?php echo _l('Remark'); ?></th>
         
         <th><?php echo _l('Restore Point'); ?></th>
         <th><?php echo _l('Archive'); ?></th>
         <th><?php echo _l('Archive Retention Period'); ?></th>
         <th><?php echo _l('Full Backup'); ?></th>

         <th><?php echo _l('Incremental Backup'); ?></th>
         <th><?php echo _l('First Backup Destination'); ?></th>
         <th><?php echo _l('Second Backup Destination'); ?></th>
         <th><?php echo _l('Third Backup Destination'); ?></th>
         <th><?php echo _l('Recovery Time Objective'); ?></th>
         <th><?php echo _l('Recovery Point Objective'); ?></th>
      
      </tr>
   </thead>
   <tbody></tbody>
   
</table>

  <div class="row">
     <div class="col-md-4 d-bl0ck m-auto">
       <div class="panel_s">
         <div class="panel-body padding-10">
           <div class="row">
             <div class="col-md-12 mbot10">
               <p class="padding-5"> <?php echo _l('Backup by status'); ?></p>
               <hr class="hr-panel-heading-dashboard">
               <canvas height="220" id="backup_report_status_pie-chart"></canvas>
             </div>
           </div>
         </div>
       </div>
       </div>
  </div>
</div>



<?php echo form_close();  ?>
