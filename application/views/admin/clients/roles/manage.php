<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_role(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New role'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($roles) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Role Name'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($roles as $role){ ?>
								<tr>
									<td><?php echo $role['roleid']; ?></td>
									<td><a href="#" onclick="edit_role(this,<?php echo $role['roleid']; ?>);return false;" data-name="<?php echo $role['name']; ?>"><?php echo $role['name']; ?></a></td>
									<td>
										<a href="#" onclick="edit_role(this,<?php echo $role['roleid']; ?>); return false" data-name="<?php echo $role['name']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('clients/delete_role/'.$role['roleid']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No role found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="role" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('clients/role')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Role Edit'); ?></span>
					<span class="add-title"><?php echo _l('Role Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('name','Role Name'); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_roles);
		$('#role').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#role input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_roles(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_role(){
		$('#role').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_role(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#role input[name="name"]').val(name);
		$('#role').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
