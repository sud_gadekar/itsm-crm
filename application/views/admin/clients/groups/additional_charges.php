<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="">
						<div class="_buttons">
							<a href="#" onclick="new_additional_charge(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('new billing rate'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($additional_service_charges) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Charge Type'); ?></th>
								<th><?php echo _l('Rates'); ?></th>
								<th><?php echo _l('Option'); ?></th>
								 
							</thead>
							<tbody>
								<?php foreach($additional_service_charges as $key => $additional_charge){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>

									<td>
										<?php echo $this->db->where('id', $additional_charge['id'])->get('tbladditional_service_charges')->row()->chargetype;?>
									</td>
									<td>
										<?php echo $additional_charge['rate'].' '.$additional_charge['currency']; ?>		
									</td>
									<td>
										<a href="#" onclick="edit_additional_charge(this,<?php echo $additional_charge['id']; ?>); return false"
										 data-name="<?php echo $additional_charge['chargetype']; ?>" 
										 data-rate="<?php echo $additional_charge['rate']; ?>" 
										 data-currency="<?php echo $additional_charge['currency']; ?>" 
										 class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>

										<a href="<?php echo admin_url('clients/delete_additional_charge/'.$additional_charge['id'].'/'.$additional_charge['customer_id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
									
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No billing rate found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	

<div class="modal fade" id="additional_charge" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('clients/charges')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Additional charge edit'); ?></span>
					<span class="add-title"><?php echo _l('Additional charge add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('chargetype','Charges Type','','text',array('list'=>'chargestype_list')); ?>
								<datalist id="chargestype_list">
								    <option value="Per Hour">
								    <option value="Per 2 Hour">
								    <option value="Per 4 Hour">
								    <option value="Per 8 Hour/Day">
								    <option value="Per Ticket">
								</datalist>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('rate','Rate'); ?>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('currency','Currency'); ?>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group">
							
	                        <input type="hidden" name="customer_id" 
	                        value="<?php echo $client->userid ?>">
	                     </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>

	$(function(){
		appValidateForm($('form'),{name:'required'},manage_ticket_priorities);
		$('#additional_charge').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#additional_charge input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_ticket_priorities(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_additional_charge(){
		$('#additional_charge').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_additional_charge(invoker,id){
		var name = $(invoker).data('name');
		var rate = $(invoker).data('rate');
		var currency = $(invoker).data('currency');
		$('#additional').append(hidden_input('id',id));
		$('#additional_charge input[name="chargetype"]').val(name);
		$('#additional_charge input[name="rate"]').val(rate);
		$('#additional_charge input[name="currency"]').val(currency);
		$('#additional_charge').modal('show');
		$('.add-title').addClass('hide');
	}

	$('select').selectpicker();
</script>