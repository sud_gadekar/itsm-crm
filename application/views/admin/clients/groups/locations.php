<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="">
						<div class="_buttons">
							<a href="#" onclick="new_location(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('new location'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						 <?php if(count($locations) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Location'); ?></th>
								<th><?php echo _l('Action'); ?></th>
							</thead>
							<tbody>
								<?php foreach($locations as $key => $location){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>

									<td>
										<?php 
										echo $location['location']; ?>	
									</td>

									
									<td>
										<a href="#" 
										 onclick="edit_location(this,<?php echo $location['id']; ?>); return false" 
										 data-name="<?php echo $location['location']; ?>" 
										 class="btn btn-default btn-icon"

										 >
										 <i class="fa fa-pencil-square-o"></i>
										</a>
										<a href="<?php echo admin_url('clients/delete_location/'.$location['id'].'/'.$location['customer_id']); ?>" class="btn btn-danger btn-icon _delete">
											<i class="fa fa-remove"></i>
										</a> 
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No location found'); ?></p>
						<?php } ?> 
					</div>
				</div>
			</div>
		</div>
	

<div class="modal fade" id="location" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('clients/locations')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Location Edit'); ?></span>
					<span class="add-title"><?php echo _l('Location Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group ">
	                        <label for="location"><?php echo _l('Location'); ?></label>

	                        <input type="text" name="location" value="" id="location" class="form-control" required>
	                        <input type="hidden" name="customer_id" value="<?php echo $client->userid ?>">
	                       

	                     </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>

	$(function(){
		appValidateForm($('form'),{name:'required'},manage_ticket_priorities);
		$('#location').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#location input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_ticket_priorities(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_location(){
		$('#location').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_location(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#location input[name="location"]').val(name);
		$('#location').modal('show');
		$('.add-title').addClass('hide');
	}

	$('select').selectpicker();
</script>