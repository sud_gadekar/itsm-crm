<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="">
						<div class="_buttons">
							<a href="#" onclick="new_team(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('new Team'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($teams) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Role'); ?></th>
								<th><?php echo _l('Staff Name'); ?></th>
								<th><?php echo _l('Action'); ?></th>
							</thead>
							<tbody>
								<?php foreach($teams as $key => $team){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>

									<td>
										<?php 
										echo $this->db->select('name')->where('roleid',$team['role'])->get('tblteamroles')->row()->name?>	
									</td>

									<td>
									
										<?php echo $team['firstname'].' '.$team['lastname']; ?>		
										
									</td>
									<td>
										
										<a href="<?php echo admin_url('clients/delete_team/'.$team['id'].'/'.$team['customer_id']); ?>" class="btn btn-danger btn-icon _delete">
											<i class="fa fa-remove"></i>
										</a> 
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No Team found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	

<div class="modal fade" id="team" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('clients/teams'),array('id'=>'team_form')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Team Edit'); ?></span>
					<span class="add-title"><?php echo _l('Team Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group select-placeholder">
	                        <label for=""><?php echo _l('Role'); ?></label>
	                        <select class="selectpicker" name="role" data-width="100%">
	                          <?php foreach ($roles as $role): ?>
			                    <option value="<?= $role['roleid']; ?>" ><?= $role['name']; ?></option>
			                  <?php endforeach; ?>
	                        </select>
	                     </div>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<div class="form-group select-placeholder">
	                        <label for="staffid"><?php echo _l('Employee'); ?></label>
	                        <select class="selectpicker" name="staffid[]" multiple data-width="100%" required="" data-live-search="true">
	                        	<?php foreach($staffs as $staff){?>
	                           <option value="<?php echo $staff['staffid'] ?>">
	                           		<?php echo $staff['firstname'].' '.$staff['lastname'] ?>
	                       	   </option>
	                           <?php } ?>
	                        </select>
	                        <input type="hidden" name="customer_id" 
	                        value="<?php echo $client->userid ?>">
	                       <!--  <select class="selectpicker" multiple data-live-search="true">
							  <option>Mustard</option>
							  <option>Ketchup</option>
							  <option>Relish</option>
							</select> -->


	                     </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>

<script>

	$(function(){
		appValidateForm($('form'),{name:'required'},manage_ticket_priorities);
		$('#team').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#team input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_ticket_priorities(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_team(){
		$('#team').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_team(invoker,id){
		var name = $(invoker).data('name');
		$('#additional').append(hidden_input('id',id));
		$('#team input[name="name"]').val(name);
		$('#team').modal('show');
		$('.add-title').addClass('hide');
	}

	$('select').selectpicker();
</script>