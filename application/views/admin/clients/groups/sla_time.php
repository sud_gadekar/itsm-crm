<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel_s">
            <div class="">
                <div class="_buttons">
                    <a href="#"
                        class="btn btn-info pull-left display-block"><?php echo _l('SLA Time'); ?></a>
                </div>
                <div class="clearfix"></div>
                <hr class="hr-panel-heading" />
                <?php echo form_open(admin_url('clients/sla_time')); ?>
                <div class="row">
                    <div class="col-md-6">
                        <h5>Support Request SLA</h5>
                        <div class="row">
                            <div class="col-md-4">Priority</div>
                            <div class="col-md-6">Response Time</div>
                        </div>
                        <hr class="hr-panel-heading" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="high_incident_sla" class="control-label" style="">High</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="high_incident_sla" name="high_incident_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->high_incident_sla)){
                                                    echo $sla_time->high_incident_sla;
                                                }?>" 
                                                list="high_incident_sla_list">
                                            <input type="hidden" name="customer_id" value="<?php echo $client_id ;?>">
                                            <datalist id="high_incident_sla_list">
                                                <option value="1">
                                                <option value="2">
                                                <option value="3">
                                            </datalist>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="medium_incident_sla" class="control-label"
                                                style="">Medium</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="medium_incident_sla" name="medium_incident_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->medium_incident_sla)){
                                                    echo $sla_time->medium_incident_sla;
                                                }?>" 
                                                list="high_incident_sla_list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="low_incident_sla" class="control-label" style="">Low</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="low_incident_sla" name="low_incident_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->low_incident_sla)){
                                                    echo $sla_time->low_incident_sla;
                                                }?>" 
                                                list="high_incident_sla_list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- SR SLA -->

                    <div class="col-md-6">
                        <h5>Service Request SLA</h5>
                        <div class="row">
                            <div class="col-md-4">Priority</div>
                            <div class="col-md-6">Response Time</div>
                        </div>
                        <hr class="hr-panel-heading" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="high_service_request_sla" class="control-label" style="">High</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="high_service_request_sla" name="high_service_request_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->high_service_request_sla)){
                                                    echo $sla_time->high_service_request_sla;
                                                }?>" 
                                                list="high_service_request_sla_list">
                                            <datalist id="high_service_request_sla_list">
                                                <option value="24">
                                                <option value="48">
                                                <option value="76">
                                            </datalist>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="medium_service_request_sla" class="control-label"
                                                style="">Medium</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="medium_service_request_sla" name="medium_service_request_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->medium_service_request_sla)){
                                                    echo $sla_time->medium_service_request_sla;
                                                }?>" 
                                                list="high_service_request_sla_list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="low_service_request_sla" class="control-label" style="">Low</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" id="low_service_request_sla" name="low_service_request_sla"
                                                class="form-control" 
                                                value="<?php if(isset($sla_time->low_service_request_sla)){
                                                    echo $sla_time->low_service_request_sla;
                                                }?>" 
                                                list="high_service_request_sla_list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo _l('submit'); ?></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="additional_charge" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('clients/charges')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('Additional charge edit'); ?></span>
                    <span class="add-title"><?php echo _l('Additional charge add'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <div class="form-group select-placeholder">
                            <label for=""><?php echo _l('Additional charges type'); ?></label>
                            <select class="selectpicker" name="chargestype" data-width="100%">
                                <?php foreach ($additional_charges as $additional_charge): ?>
                                <option value="<?= $additional_charge['id']; ?>">
                                    <?= $additional_charge['chargetype']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <div class="form-group">
                            <?php if(isset($additional_charge['rate'])) {?>
                            <?php /*echo render_input('rate','Rate',$additional_charge['rate']); */?>

                            <?php }?>
                            <input type="hidden" name="customer_id" value="<?php echo $client->userid ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
$(function() {
    appValidateForm($('form'), {
        name: 'required'
    }, manage_ticket_priorities);
    $('#additional_charge').on('hidden.bs.modal', function(event) {
        $('#additional').html('');
        $('#additional_charge input[name="name"]').val('');
        $('.add-title').removeClass('hide');
        $('.edit-title').removeClass('hide');
    });
});

function manage_ticket_priorities(form) {
    var data = $(form).serialize();
    var url = form.action;
    $.post(url, data).done(function(response) {
        // window.location.reload();
    });
    return false;
}

function new_additional_charge() {
    $('#additional_charge').modal('show');
    $('.edit-title').addClass('hide');
}

function edit_additional_charge(invoker, id) {
    var name = $(invoker).data('name');
    $('#additional').append(hidden_input('id', id));
    $('#additional_charge input[name="name"]').val(name);
    $('#additional_charge').modal('show');
    $('.add-title').addClass('hide');
}

$('select').selectpicker();
</script>