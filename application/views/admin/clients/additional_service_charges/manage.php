<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_additional_service_charge(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New additional service charges'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($additional_service_charges) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('Id'); ?></th>
								<th><?php echo _l('Charges Type'); ?></th>
								<th><?php echo _l('Rate'); ?></th>
								<th><?php echo _l('Currency'); ?></th>
								<th><?php echo _l('Action'); ?></th>

							</thead>
							<tbody>
								<?php foreach($additional_service_charges as $additional_service_charge){ ?>
								<tr>
									<td><?php echo $additional_service_charge['id']; ?></td>
									<td><a href="#" onclick="edit_additional_service_charge(this,<?php echo $additional_service_charge['id']; ?>);return false;" data-name="<?php echo $additional_service_charge['chargetype']; ?>"><?php echo $additional_service_charge['chargetype']; ?></a></td>
									<td>
										<?php echo $additional_service_charge['rate']; ?>
									</td>

									<td><?php echo $additional_service_charge['currency']; ?></td>				<td>
										<a href="#" onclick="edit_additional_service_charge(this,<?php echo $additional_service_charge['id']; ?>); return false"
										 data-name="<?php echo $additional_service_charge['chargetype']; ?>" 
										 data-rate="<?php echo $additional_service_charge['rate']; ?>" 
										 data-currency="<?php echo $additional_service_charge['currency']; ?>" 
										 class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('clients/delete_additional_service_charge/'.$additional_service_charge['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No additional service charge found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="additional_service_charge" tabindex="-1" additional_service_charge="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('clients/additional_service_charge')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Additional service charge edit'); ?></span>
					<span class="add-title"><?php echo _l('Additional service charge add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('chargetype','Charges Type','','text',array('list'=>'chargestype_list')); ?>
								<datalist id="chargestype_list">
								    <option value="Per Hour">
								    <option value="Per 2 Hour">
								    <option value="Per 4 Hour">
								    <option value="Per 8 Hour/Day">
								    <option value="Per Ticket">
								</datalist>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('rate','Rate'); ?>
					</div>
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('currency','Currency'); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_client_additional_service_charges);
		$('#additional_service_charge').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#additional_service_charge input[name="name"]').val('');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_client_additional_service_charges(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_additional_service_charge(){
		$('#additional_service_charge').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_additional_service_charge(invoker,id){
		var name = $(invoker).data('name');
		var rate = $(invoker).data('rate');
		var currency = $(invoker).data('currency');
		$('#additional').append(hidden_input('id',id));
		$('#additional_service_charge input[name="chargetype"]').val(name);
		$('#additional_service_charge input[name="rate"]').val(rate);
		$('#additional_service_charge input[name="currency"]').val(currency);
		$('#additional_service_charge').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
