<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('checklists','','create')){ ?>
                  <a href="<?php echo admin_url('checklists/health_checklist'); ?>" class="btn btn-info">
                    <?php echo 'New Checklist'; ?></a>
                  <a href="<?php echo admin_url('checklists/checklist_logs'); ?>" class="btn btn-info">
                    <?php echo 'Checklist Logs'; ?></a>
                  <?php } ?>
            
                   <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="checklists_total"></div>
                  </div>
               </div>
            <!-- </div>
            <div class="row"> -->
               <div class="panel-body">
               <div class="checklists_table" id="small-table ">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                        
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('Sr.No.'); ?></th>
                                <th><?php echo _l('View Logs'); ?></th>
                                <th><?php echo _l('Customer'); ?></th>
                                <th><?php echo _l('Checklist Title'); ?></th>
                                <th><?php echo _l('Frequency'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($checklists_lists) > 0){?>

                                <?php foreach ($checklists_lists as $key => $checklists_list) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('checklists/health_checklist/').$checklists_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('checklists/delete/').$checklists_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td>
                                    <a href="<?php echo admin_url('checklists/checklist_logs/'.$checklists_list['id']); ?>" class="btn btn-info btn-icon _view" title="View Logs"><i class="fa fa-eye"></i></a>
                                    <!--<a href="#" onclick="checklist_log(this,<?php echo $checklists_list['id']; ?>); return false" data-name="<?php echo $checklists_list['id']; ?>" class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>-->
                                  </td>
                                  <td><?php echo $checklists_list['company']; ?></td>
                                  <td><?php echo $checklists_list['checklist_title'] ?></td>
                                  <td>
                                    <?php 
                                      if($checklists_list['frequency']==1){ echo 'Daily'; }
                                      else if($checklists_list['frequency']==2){ echo 'Weekly'; }
                                      else if($checklists_list['frequency']==3){ echo 'Monthly'; }
                                    ?>
                                  </td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                         
                        </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Health Checklist Logs</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<!-- Show Asset Data On Modal -->
<script type="text/javascript">
    function checklist_log(url,id){
      $.ajax({
           url:'<?=admin_url()?>checklists/checklist_log_data/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              //alert(data)
              $('.logDetailsData').html(data);
            }
           }
      });
                              
      $('#logmodal').modal('show');   
    }
  </script>

<script type="text/javascript">
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>checklists/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.checklists_table').html(data);
            }
           }
      });
  }
</script>
</body>
</html>
