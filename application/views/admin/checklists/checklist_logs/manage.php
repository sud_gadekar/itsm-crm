
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($checklist_logs) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Health Checklist'); ?></th>
								<th><?php echo _l('Date'); ?></th>
								<th><?php echo _l('Inventory Product'); ?></th>
								<th><?php echo _l('Status'); ?></th>
								<th><?php echo _l('Remark'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($checklist_logs as $key => $checklist_log){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td><a href="<?php echo admin_url('checklists/health_checklist/'.$checklist_log['checklist_id']); ?>" data-name="<?php echo $checklist_log['checklist_id']; ?>"><?php echo $checklist_log['checklist_title']; ?></a></td>
									<td><?php echo date('d-m-Y',strtotime($checklist_log['log_date'])); ?></td>
									<td><?php echo $checklist_log['product_name'].'('.$checklist_log['hostname'].'|'.$checklist_log['purpose'].')'; ?></td>
									<td><?php echo $checklist_log['status']; ?></td>
									<td><?php echo $checklist_log['remark']; ?></td>
									<td>
										<a href="#" onclick="edit_checklist_log(this,<?php echo $checklist_log['id']; ?>,<?php echo $checklist_log['log_date']; ?>,'<?php echo $checklist_log['checklist_title']; ?>','<?php echo $checklist_log['product_name']; ?>','<?php echo $checklist_log['remark']; ?>'); return false" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<!-- <a href="<?php echo admin_url('gaps/delete_checklist_log/'.$checklist_log['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> -->
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No health checklist logs found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="checklist_log" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="width: 800px;">
		<?php echo form_open(admin_url('checklists/log')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Checklist Log Edit'); ?></span>
					<span class="add-title"><?php echo _l('Checklist Log Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="clientid" class="control-label">
                                <?php echo _l('Select Client'); ?>
                            </label>
                            <select class="selectpicker" name="client_id" id="client_id"data-width="100%" data-live-search="true" onchange="client_checklists(this); return false">
                              <option value="" disabled selected=""> Select Client</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>"><?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label for="log_date" class="control-label">Checklist Log Date</label>
                          <div class="input-group date">
                            <input type="text" id="log_date" name="log_date" class="form-control datepicker" 
                                   value="<?php if(isset($checklist_log->log_date)){
                                    echo $checklist_log->log_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="checklist_id" class="control-label">
                                <?php echo _l('Health Checklist'); ?>
                            </label>
                            <select name="checklist_id" id="checklist_id" class="form-control" required onchange="get_checklist_inventories(this); return false">
							</select>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div id="additional"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit_checklist_log" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('checklists/log')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Checklist Log Edit'); ?></span>
					<span class="add-title"><?php echo _l('Checklist Log Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="policy" class="control-label">
                                <?php echo _l('Health Checklist'); ?>
                            </label>
                            <input type="text" name="policy" id="policy" class="form-control" readonly>
                        </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
                            <label for="inventory_product" class="control-label">
                                <?php echo _l('Health Checklist(Inventory Product)'); ?>
                            </label>
                            <input type="text" name="inventory_product" id="inventory_product" class="form-control" readonly>
                        </div>
                    </div>
					<div id="edit_additional"></div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label for="log_date" class="control-label">Log Date</label>
                          <div class="input-group date">
                            <input type="text" id="log_date" name="log_date" class="form-control datepicker" 
                                   value="<?php if(isset($checklist_log->log_date)){
                                    echo $checklist_log->log_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="status"><?php echo _l('Checklist Status'); ?></label>
                            <select class="selectpicker" name="checklist_status" id="checklist_status"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
								<?php foreach($statuses as $status){ ?>
                                	<option value="<?php echo $status['statusid']; ?>" style="color: <?php echo $status['statuscolor']; ?>"><?php echo $status['name']; ?></option>';
								<?php }?>
                            </select>
                        </div>
                    </div>
					<div class="col-md-12">
                      	<div class="form-group">
                        	<label for="remark" class="control-label">Remark</label>
                            <input type="text" id="remark" name="remark" class="form-control" value="" autocomplete="off">
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script type="text/javascript">
    function client_checklists(url){
	  var id = $('#client_id').val();
      $.ajax({
           url:'<?=admin_url()?>checklists/client_checklists/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
				$("#checklist_id").empty();
				$('#additional').empty();
                if(JSON.parse(data).length > 0){
                    $('.checklist_idselect').css('display','block');
					$("#checklist_id").append(`<option value='' selected>Select Health Checklist</option>`);
                    $.each(JSON.parse(data), function(index, json) {
                        $("#checklist_id").append(`<option value='${JSON.parse(data)[index].id}'
                        ${(JSON.parse(data)[index].id==$('#checklist_id').val())?'selected' : ''}>${JSON.parse(data)[index].checklist_title}</option>`);
                    });
                }
           }
      });  
    }

	function get_checklist_inventories(url){
	  var id = $('#checklist_id').val();
      $.ajax({
           url:'<?=admin_url()?>checklists/get_checklist_inventories/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
				$('#additional').empty();
				$('#additional').append(data);
           }
      });  
    }
</script>
<script>
	function new_checklist_log(){
		$('#additional').empty();
		$('#additional').append(hidden_input('id',0));
		$('#checklist_log').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_checklist_log(invoker,id,date,policy,product,remark){
		var name = $(invoker).data('name');
		$('#edit_additional').empty();
		$('#edit_additional').append(hidden_input('id',id));
		$('#policy').val(policy);
		$('#inventory_product').val(product);
		$('#remark').val(remark);
		//$('#checklist_log select[name^="checklist_id"] option[value="1"]').attr("selected","selected");
		//$('#checklist_log input[name="log_date"]').val(date);
		$('#edit_checklist_log').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
