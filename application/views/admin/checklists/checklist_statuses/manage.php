<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_status(); return false;" class="btn btn-info pull-left display-block">
								<?php echo _l('New Checklist Status'); ?>
							</a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($statuses) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Status Name'); ?></th>
								<th><?php echo _l('options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($statuses as $status){ ?>
								<tr>
									<td><?php echo $status['statusid']; ?></td>
									<td><a href="#" onclick="edit_status(this,<?php echo $status['statusid']; ?>); return false" data-name="<?php echo $status['name']; ?>" data-color="<?php echo $status['statuscolor']; ?>" ><?php echo $status['name']; ?></a><br />
										<?php echo _l('Total',total_rows(db_prefix().'checklist_logs',array('checklist_status'=>$status['statusid']))); ?>
									</td>
									<td>
										<a href="#" onclick="edit_status(this,<?php echo $status['statusid']; ?>); return false" data-name="<?php echo $status['name']; ?>" data-color="<?php echo $status['statuscolor']; ?>"  class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<a href="<?php echo admin_url('checklists/delete_checklist_status/'.$status['statusid']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No Checklist Statuses Found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="checklist_status" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('checklists/status')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Edit Checklist Status'); ?></span>
					<span class="add-title"><?php echo _l('New Checklist Status'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_input('name','Status'); ?>
						<?php echo render_color_picker('statuscolor',_l('Status Color')); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script>
	$(function(){
		appValidateForm($('form'),{name:'required'},manage_checklist_statuses);
		$('#checklist_status').on('hidden.bs.modal', function(event) {
			$('#additional').html('');
			$('#checklist_status input[name="name"]').val('');
			$('#checklist_status .colorpicker-input').colorpicker('setValue','');
			$('.add-title').removeClass('hide');
			$('.edit-title').removeClass('hide');
		});
	});
	function manage_checklist_statuses(form) {
		var data = $(form).serialize();
		var url = form.action;
		$.post(url, data).done(function(response) {
			window.location.reload();
		});
		return false;
	}
	function new_status(){
		$('#checklist_status').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_status(invoker,id){
		var name = $(invoker).data('name');
		var color = $(invoker).data('color');
		$('#additional').append(hidden_input('id',id));
		$('#checklist_status input[name="name"]').val(name);
		$('#checklist_status .colorpicker-input').colorpicker('setValue',color);
		$('#checklist_status').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
