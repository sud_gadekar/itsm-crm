<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'backup_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Health Checklist</h4>

                  <div class="row">
                    <div class="additional"></div>
                     <div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="client_id" class="control-label">
                                <?php echo _l('Customer'); ?>
                            </label>
                            <select class="selectpicker" name="client_id" id="client_id"data-width="100%" data-live-search="true" onchange="get_inventories(this); return false" required>
                              <option value="" disabled selected=""> Select Customer</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>" 
                                    <?php if(isset($checklists_details->client_id)){
                                        echo ($checklists_details->client_id == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Checklist Title</label>
                            <input type="text" name="checklist_title" id="checklist_title" 
                            value="<?php  if(isset($checklists_details->checklist_title))
                              {echo $checklists_details->checklist_title;}?>" class="form-control" required>
                        </div>
                    </div>

                     <div class="col-md-6">
                        <div class="form-group select-placeholder"> <!-- select-placeholder -->
                            <label for="inventory" class="control-label">
                                <?php echo _l('Inventory Products'); ?>
                            </label>
                            <select class="form-control selectpicker" name="inventory[]" id="inventory" data-width="100%" data-live-search="true" multiple> <!--selectpicker-->
                            </select>
                            
                            <input type="hidden" name="inventory_ids" id="inventory_ids" value="<?php if(isset($checklists_details->inventory)){echo $checklists_details->inventory;}?>">
                        </div>
                        <input type="hidden" name="id" id="id" value="<?php if(isset($checklists_details->id)){echo $checklists_details->id;}?>">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="frequency"><?php echo _l('Schedule'); ?></label>
                            <select onchange="new_task(); return false;" class="selectpicker" name="frequency" id="frequency"data-width="100%" data-live-search="true" required>
                                <option value="" disabled selected=""> Select </option>
                                <option value="1" <?php if(isset($checklists_details->frequency)){ echo ($checklists_details->frequency == 1) ? 'selected' : '';}?>>Daily</option>
                                <option value="2" <?php if(isset($checklists_details->frequency)){ echo ($checklists_details->frequency == 2) ? 'selected' : '';}?>>Weekly</option>
                                <option value="3" <?php if(isset($checklists_details->frequency)){ echo ($checklists_details->frequency == 3) ? 'selected' : '';}?>>Monthly</option>
                            </select>
                        </div>
                    </div>
                  </div>
                  
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
           <?php echo form_close(); ?>
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>
  <?php /* $this->load->view('admin/inventory_details/inventory_detail_category'); /*/?>
  <?php init_tail(); ?>
<script>
   $(function(){
     appValidateForm($('#backup_form'),
      {clientid:'required',backup_type:'required',start_date:'required',end_date:'required',
        notify_to: {
          email:true,
          required:true
        }
      });
   });

   $('document').ready(function(){
    var id = $('#client_id').val();
    if(id){
      get_inventories(this);
    }
   });

   function get_inventories(url){
	  var id = $('#client_id').val();
    var str = $('#inventory_ids').val();
      $.ajax({
           url:'<?=admin_url()?>backups/get_client_inventories/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
              $("#inventory").empty();
              if(JSON.parse(data).length > 0){
                $.each(JSON.parse(data), function(index, json) {
                  // $("#inventory").append(`<option value='${JSON.parse(data)[index].id}'
                  // ${(str.indexOf(JSON.parse(data)[index].id) != -1)?'selected' : ''}>${JSON.parse(data)[index].product_name} (${JSON.parse(data)[index].hostname} | ${JSON.parse(data)[index].purpose})</option>`);

                  if(str.indexOf(JSON.parse(data)[index].id) != -1){ selected = true; }
                  else{ selected = false; }

                  var text = `${JSON.parse(data)[index].product_name} (${JSON.parse(data)[index].hostname} | ${JSON.parse(data)[index].purpose})`;
                  var newOption = new Option(text, `${JSON.parse(data)[index].id}`, selected, selected);

                  $('#inventory').append(newOption).trigger('change');
                  $("#inventory").selectpicker("refresh");
                });
              }
           }
      });  
    }
</script>
</body>
</html>