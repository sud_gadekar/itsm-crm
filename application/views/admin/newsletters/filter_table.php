
<div class="clearfix"></div>
<div class="row form-inline dataTables_wrapper">
<div class="col-md-6">
  <div class="dataTables_length" id="table-diagram_details_length">
    <label>
      <select name="table-diagram_details_length" aria-controls="table-diagram_details" class="form-control input-sm">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="-1">All</option>
      </select>
    </label>
  </div>
  <div class="dt-buttons btn-group">
    <button class="btn btn-default buttons-collection btn-default-dt-options" tabindex="0" aria-controls="table-diagram_details" type="button" aria-haspopup="true" aria-expanded="false"><span>Export</span>
    </button>
  </div>
</div>
<div class="col-md-6">
  <div id="table-diagram_details_filter" class="dataTables_filter">
    <label>
      <div class="input-group">
        <span class="input-group-addon">
          <span class="fa fa-search"></span>
          </span><input type="search" class="form-control input-sm" placeholder="Search..." aria-controls="table-diagram_details">
        </div>
      </label>
    </div>
  </div>
  <div id="table-diagram_details_processing" class="dataTables_processing panel panel-default" style="display: none;"><div class="dt-loader">
    
  </div>
</div>
</div>
<table class="table dt-table dataTable scroll-responsive
<?php echo (count($diagrams_lists) <= 0) ?'dt-inline dataTable no-footer' : ''?>">
<thead>
  <th><?php echo _l('id'); ?></th>
  <th><?php echo _l('Company'); ?></th>
  <th><?php echo _l('Title'); ?></th>
  <th><?php echo _l('Diagram File'); ?></th>
  <th><?php echo _l('Description'); ?></th>
  <th><?php echo _l('Created On'); ?></th>
</thead>
<tbody>
  <?php if(count($diagrams_lists) > 0){?>
  <?php foreach ($diagrams_lists as $key => $diagrams_list) { ?>
  <tr class="has-row-options">
    <td><?php echo $key+1; ?>
      <div class="row-options">
        
        <a href="<?php echo admin_url('diagrams/diagram/').$diagrams_list['id'] ?>">Edit </a>
        <span class="text-dark"> | </span>
        <a
        href="<?php echo admin_url('diagrams/delete/').$diagrams_list['id'] ?>" class="text-danger _delete">Delete </a>
      </td>
      <td><?php echo $diagrams_list['company'] ?></td>
      <td><?php echo $diagrams_list['title'] ?></td>
      <td>
        <a href="<?php echo base_url('uploads/diagrams/'.$diagrams_list['id'].'/thumb_'.$diagrams_list['diagram_file']); ?>" target="_blank"><?php echo $diagrams_list['diagram_file'] ?></a>
      </td>
      <td><?php echo $diagrams_list['description'] ?></td>
      <td><?php echo $diagrams_list['created_at'] ?></td>
    </tr>
    <?php }  ?>
    <?php } else{ ?>
    <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No entries found</td></tr>
    <?php }?>
  </tbody>
</table>
