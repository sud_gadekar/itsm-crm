<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'newsletter-form')); ?>
      <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <div class="panel_s">
               <div class="panel-body">
                  <h4 class="no-margin">
                     <?php echo $title; ?>
                  </h4>

                  <hr class="hr-panel-heading" />
                  <div class="clearfix"></div>
                  <?php $value = (isset($newsletter) ? $newsletter->title : ''); ?>
                  <?php $attrs = (isset($newsletter) ? array() : array('autofocus'=>true)); ?>

                  <div class="form-group select-placeholder f_client_id">
                        <label for="type" class="control-label"><?php echo _l('Type'); ?></label>
                        <select class="selectpicker" name="type" id="type" data-width="100%" data-live-search="true">
                           <option value="1" <?php if(isset($newsletter->type)){ echo ($newsletter->type == 1) ? 'selected' : '';}?>>Newsletter</option>
                           <option value="2" <?php if(isset($newsletter->type)){ echo ($newsletter->type == 2) ? 'selected' : '';}?>>Educational Content</option>
                      </select>
                  </div>
                  <?php echo render_input('title','Title',$value,'text',$attrs); ?>
                  
                  <!--<div class="form-group select-placeholder f_client_id">
                        <label for="clientid" class="control-label"><?php echo _l('Assigned to ( Customer )'); ?></label>
                        <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                          <option value="" disabled selected=""> Select Customer</option>
                          <?php foreach($clients as  $client){ ?>
                              <option value="<?php echo $client['userid']?>" 
                                <?php if(isset($newsletter->clientid)){
                                  echo ($newsletter->clientid == $client['userid']) ? 'selected' : '';}?>>
                                <?php echo $client['company']; ?></option>
                          <?php } ?>
                      </select>
                  </div>-->

                  <div class="form-group">
                     <label for="nl_file" class="profile-image"><?php echo _l('Newsletter / Educational Content File'); ?></label>
                     <input type="file" name="nl_file" class="form-control" id="nl_file">
                  </div>
                  <?php if(isset($newsletter) && $newsletter->nl_file != NULL){ ?>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-9">
                              <a href="<?php echo base_url('uploads/newsletters/'.$newsletter->id.'/'.$newsletter->nl_file); ?>" target="_blank"><?php echo $newsletter->nl_file ?></a>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
               </div>
            </div>
         </div>
         <?php if((has_permission('newsletters','','create') && !isset($newsletter)) || has_permission('newsletters','','edit') && isset($newsletter)){ ?>
         <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
            <button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
         </div>
         <?php } ?>
      </div>
      <?php echo form_close(); ?>
   </div>
</div>
<?php init_tail(); ?>
<script>
   $(function(){
     init_editor('#description', {append_plugins: 'stickytoolbar'});
     appValidateForm($('#newsletter-form'),{title:'required',nl_file:'required',type:'required'});
   });
</script>
</body>
</html>
