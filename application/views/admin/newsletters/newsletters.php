<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head();
   $has_permission_edit = has_permission('newsletters','','edit');
   $has_permission_create = has_permission('newsletters','','create');
   ?>
<div id="wrapper">
<div class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="panel_s mtop5">
            <div class="panel-body">
               <div class="_buttons">

                  <?php if($has_permission_create){ ?>
                  <a href="<?php echo admin_url('newsletters/newsletter'); ?>" class="btn btn-info mright5"><?php echo _l('New Newsletter / Educational Content'); ?></a>
                  <?php } else { ?>
                    <a href="#" class="btn btn-info mright5"><?php echo _l('Newsletter'); ?></a>
                  <?php } ?>
                  <?php if(count($clients) > 0 && is_admin()){ ?>
                  <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                    
                        </ul>
                    </div>
                    <?php } ?>
                  <div class="_hidden_inputs _filters">
                     
                  </div>
               </div>
              </div>
               <div class="panel-body">
               <div class="newsletters_table" id="small-table">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                       
                        <div class="mtop20">
                         
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <!--<th><?php echo _l('Company'); ?></th>-->
                                <th><?php echo _l('Type'); ?></th>
                                <th><?php echo _l('Title'); ?></th>
                                <th><?php echo _l('File'); ?></th>
                                <th><?php echo _l('Created On'); ?></th>
                              </thead>
                              <tbody>
                                <?php foreach ($newsletters_lists as $key =>  $newsletters_list) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key + 1; ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('newsletters/newsletter/').$newsletters_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('newsletters/delete/').$newsletters_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <!--<td><?php echo $newsletters_list['company'] ?></td>-->
                                  <td><?php echo ($newsletters_list['type']==2)?'Educational Content':'Newsletter'; ?></td>
                                  <td><?php echo $newsletters_list['title'] ?></td>
                                  <td>
                                    <a href="<?php echo base_url('uploads/newsletters/'.$newsletters_list['id'].'/'.$newsletters_list['nl_file']); ?>" target="_blank"><?php echo $newsletters_list['nl_file'] ?></a>
                                  </td>
                                  <td><?php echo $newsletters_list['created_at'] ?></td>
                                </tr>
                                <?php }  ?>
                              </tbody>
                            </table>
                          
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="newsletters" class="hide">
                  </div>
               </div>
             </div>
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // newsletters additional server params
             var newsletters_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               newsletters_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             //initDataTable('.table-newsletters', admin_url+'newsletters/table', 'undefined', 'undefined', newsletters_ServerParams, <?php echo hooks()->apply_filters('newsletters_table_default_order', json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

            // init_newsletter();

             $('#newsletter_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_newsletter').attr('data-empty-note');
                var emptyName = $('#tab_newsletter').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#newsletter_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="newsletter_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'newsletters/convert_to_invoice/'+$('body').find('.newsletter_convert_btn').attr('data-id'), parameters);
            });
           });
</script>

<script type="text/javascript">
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>newsletters/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              console.log(data);
              $('.newsletters_table').html(data);
            }
           }
      });
  }
</script>

</body>
</html>
