<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'gap_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
          <!-- <button class="btn btn-info save-and-add-contact customer-form-submiter">Save </button> -->
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading"><?php echo $title?></h4>
                  <div class="row">
                    <div class="additional"></div>
                    <div class="col-md-6">
                         <div class="form-group select-placeholder f_client_id">
                              <label for="clientid" class="control-label">
                                  <?php echo _l('Assigned to ( Customer )'); ?>
                                </label>
                              <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Type</option>
                                <?php foreach($clients as  $client){ ?>
                                    <option value="<?php echo $client['userid']?>" 
                                      <?php if(isset($gaps->clientid)){
                                        echo ($gaps->clientid == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name"><?php echo _l('GAP Name'); ?></label>
                            <input type="text" name="name" id="name" value="<?php if(isset($gaps->name)){ echo $gaps->name; } ?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                         <div class="form-group select-placeholder f_client_id">
                              <label for="staffid" class="control-label">
                                  <?php echo _l('GAP Owner'); ?>
                                </label>
                              <select class="selectpicker" name="staffid" id="staffid"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Owner</option>
                                <?php foreach($staffs as  $staff){ ?>
                                    <option value="<?php if(isset($staff['staffid'])){echo $staff['staffid'];}?>" 
                                      <?php if(isset($gaps->staffid)){
                                        echo ($gaps->staffid == $staff['staffid']) ? 'selected' : '';}?>>
                                      <?php echo $staff['firstname'].' '.$staff['lastname']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="type"><?php echo _l('Status'); ?></label>
                            <select class="selectpicker" name="status_id" id="status_id" data-width="100%">
                                <option value="" disabled selected=""> Select Status</option>
                                <?php foreach($statuses as $status){ ?>
                                    <option value="<?php echo $status['id']?>"
                                      <?php if(isset($gaps->status_id)){
                                        echo ($gaps->status_id == $status['id']) ? 'selected' : '';}?>>
                                      <?php echo $status['status_title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="type"><?php echo _l('Category'); ?></label>
                            <input type="hidden" name="id" value="<?php if(isset($gaps->id)) { echo $gaps->id; } ?>">

                            <select class="selectpicker" name="category_id" id="category_id" data-width="100%">
                                <option value="" disabled selected=""> Select Category</option>
                                <?php foreach($categories as  $category){ ?>
                                    <option value="<?php echo $category['id']?>" 
                                      <?php if(isset($gaps->category_id)){
                                        echo ($gaps->category_id == $category['id']) ? 'selected' : '';}?>>
                                      <?php echo $category['category_title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="type"><?php echo _l('Impact'); ?></label>
                            <select class="selectpicker" name="impact_id" id="impact_id" data-width="100%">
                                <option value="" disabled selected=""> Select Impact</option>
                                <?php foreach($impacts as $impact){ ?>
                                    <option value="<?php echo $impact['id']?>"
                                      <?php if(isset($gaps->impact_id)){
                                        echo ($gaps->impact_id == $impact['id']) ? 'selected' : '';}?>>
                                      <?php echo $impact['impact_title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                      <?php $value = (isset($gaps) ? $gaps->description : ''); ?>
                      <?php echo render_textarea('description','Description',$value,array('rows'=>4),array()); ?>
                    </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?>

		<?php if(isset($notes)){ ?>
			<div class="panel_s">
				<div class="panel-body">
					<div class="row _buttons">
                     	<div class="col-md-12">
							<h4 class="customer-profile-group-heading">GAP Notes</h4>
							<a href="#" onclick="new_gap_note(<?php echo $gaps->id; ?>); return false;" class="btn btn-info pull-left new"><?php echo _l('New Note'); ?></a>
						</div>
					</div>
                  	<hr class="hr-panel-heading hr-10" />
                  	<div class="clearfix"></div>

					<div class="row">
                     	<div class="col-md-12">
						<table class="table dt-table scroll-responsive">
                            <thead>
                                <th><?php echo _l('#'); ?></th>
                                <th><?php echo _l('Note'); ?></th>
                                <th><?php echo _l('Added On'); ?></th>
                                <th><?php echo _l('Action'); ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($notes as $key=>$note) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?></td>
                                  <td><?php echo $note['gap_note'] ?></td>
                                  <td><?php echo $note['created_at'] ?></td>
								  <td>
                                    <a href="#" onclick="edit_gap_note(this,<?php echo $note['id']; ?>,<?php echo $gaps->id; ?>); return false" data-name="<?php echo $note['gap_note']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
                                  </td>
                                </tr>
                                <?php }  ?>
                            </tbody>
                        </table>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>

  <div class="modal fade" id="gap_note" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('gaps/gap_note')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Gap Note Edit'); ?></span>
					<span class="add-title"><?php echo _l('Gap Note Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_textarea('gap_note','GAP Note',null,array('rows'=>4),array()); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <?php init_tail(); ?>

  <script>
   $(function(){
     appValidateForm($('#gap_form'),{clientid:'required',name:'required'});
   });
  </script>


  <script type="text/javascript">
	function new_gap_note(gap_id){
		$('#additional').html('');
		$('#additional').append(hidden_input('gap_id',gap_id));
		$("textarea#gap_note").val('');
		$('#gap_note').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_gap_note(invoker,id,gap_id){
		var name = $(invoker).data('name');
		$('#additional').html('');
		$('#additional').append(hidden_input('id',id));
		$('#additional').append(hidden_input('gap_id',gap_id));
		$("textarea#gap_note").val(name);
		$('#gap_note').modal('show');
		$('.add-title').addClass('hide');
	}
  </script>
</body>
</html>