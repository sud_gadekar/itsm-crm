<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
 <?php
 $statuses = $this->gaps_model->get_gap_status();
 ?>
 <div class="_filters _hidden_inputs hidden gaps_filters">
  <?php
  echo form_hidden('my_gaps');
  if(is_admin()){
    $gap_assignees = $this->gaps_model->get_gaps_assignes_disctinct();
    foreach($gap_assignees as $assignee){
      echo form_hidden('gap_assignee_'.$assignee['assigned']);
    }
  }
  foreach($statuses as $status){
    $val = '';
    if($chosen_gap_status != ''){
      if($chosen_gap_status == $status['id']){
        $val = $chosen_gap_status;
      }
    } else {
      if(in_array($status['id'], $default_gaps_list_statuses)){
        $val = 1;
      }
    }
    echo form_hidden('gap_status_'.$status['id'],$val);
  } ?>
</div>
<div class="col-md-12">
  <h4 class="no-margin"><?php echo _l('gaps_summary'); ?></h4>
  </div>
  <?php
  $where = '';
  if (!is_admin()) {
    if (get_option('staff_access_only_assigned_departments') == 1) {
      $departments_ids = array();
      if (count($staff_deparments_ids) == 0) {
        $departments = $this->departments_model->get();
        foreach($departments as $department){
          array_push($departments_ids,$department['departmentid']);
        }
      } else {
       $departments_ids = $staff_deparments_ids;
     }
     if(count($departments_ids) > 0){
      $where = 'AND department IN (SELECT departmentid FROM '.db_prefix().'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="'.get_staff_user_id().'")';
    }
  }
}
foreach($statuses as $status){
  $_where = '';
  if($where == ''){
    $_where = 'status='.$status['id'];
  } else{
    $_where = 'status='.$status['id'] . ' '.$where;
  }
  if(isset($project_id)){
    $_where = $_where . ' AND project_id='.$project_id;
  }
  ?>
  <div class="col-md-2 col-xs-6 mbot15 border-right">
    <a href="#" data-cview="gap_status_<?php echo $status['id']; ?>" onclick="dt_custom_view('gap_status_<?php echo $status['id']; ?>','.gaps-table','gap_status_<?php echo $status['id']; ?>',true); return false;">
      <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
      <span style="color:<?php echo $status['statuscolor']; ?>">
        <?php echo gap_status_translate($status['id']); ?>
      </span>
    </a>
  </div>
  <?php } ?>
  <hr class="hr-panel-heading">
</div>
 <hr class="hr-panel-heading" />
