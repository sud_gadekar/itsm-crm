<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="_buttons">
                            <a href="<?php echo admin_url('gaps/add'); ?>"
                                class="btn btn-info pull-left display-block mright5">
                                <?php echo _l('New Gap'); ?>
                            </a>
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                             <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                                data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-filter" aria-hidden="true"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right width300">
                                    
                                    
                                    <div class="clearfix"></div>
                                    
                                    <li class="dropdown-submenu pull-left">
                                        <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                        <ul class="dropdown-menu dropdown-menu-left">
                                            <?php foreach($clients as $client){ ?>
                                            <li>
                                                <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                   
                                </ul>
                            </div>
                             <?php } ?>
                        </div>

                      </div>
                     


                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                          <h4 class="no-margin">GAP by Status</h4>
                        </div>
                          <?php 
                        foreach($statuses as $status){
                        $_where = '';
                        
                        $_where = 'status_id='.$status['id'];
                        
                        ?>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                          <a href="#" onclick="filterbystatus(<?php echo $status['id'];?>);return false;">
                            <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
                            <span style="">
                              <?php echo $status['status_title']; ?>
                            </span>
                          </a>
                        </div>
                      <?php } ?>
                      </div>
                      
                      <hr class="">

                      <?php if(!is_admin()) { ?>

                      <div class="row">
                         <div class="col-md-12">
                          <h4 class="no-margin">GAP by Category</h4>
                        </div>
                          <?php 
                        foreach($categories as $category){
                        $_where = '';
                        
                        $_where = 'category_id='.$category['id'];
                        
                        ?>
                        <div class="col-md-2 col-xs-6 mbot15 border-right">
                          <a href="#">
                            <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
                            <span style="">
                              <?php echo $category['category_title']; ?>
                            </span>
                          </a>
                        </div>
                      <?php } ?>
                      </div>
                      <hr class="">

                    <?php } ?>

                   <div class="gaps_table" id="small-table ">
                      <div class="">
                         <div class="">
                            <div class="clearfix"></div>
                            <div class="mtop20">
                             
                                <div class="clearfix"></div>
                               
                                <table class="table dt-table scroll-responsive">
                                  <thead>
                                    <th><?php echo _l('#'); ?></th>
                                    <th><?php echo _l('Company'); ?></th>
                                    <th><?php echo _l('Name'); ?></th>
                                    <th><?php echo _l('Owner'); ?></th>
                                    <th><?php echo _l('Category'); ?></th>
                                    <th><?php echo _l('Impact'); ?></th>
                                    <th><?php echo _l('Status'); ?></th>
                                  </thead>
                                  <tbody>
                                    <?php foreach ($gaps_lists as $key => $gaps_list) { ?>
                                    <tr class="has-row-options" data-href='<?php echo admin_url('gaps/add/').$gaps_list['id'] ?>'>
                                      <td><?php echo $key+1 ?>
                                        <div class="row-options">
                                            <a href="<?php echo admin_url('gaps/add/').$gaps_list['id'] ?>">Edit </a> 
                                            <span class="text-dark"> | </span>
                                            <a href="<?php echo admin_url('gaps/delete/').$gaps_list['id'] ?>" class="text-danger _delete">Delete </a> 
                                      </td>
                                      <td><?php echo $gaps_list['company'] ?></td>
                                      <td><?php echo $gaps_list['name'] ?></td>
                                      <td><?php echo $gaps_list['firstname'].' '.$gaps_list['lastname'] ?></td>
                                      <td><?php echo $gaps_list['category_title'] ?></td>
                                      <td><?php echo $gaps_list['impact_title'] ?></td>
                                      <td><?php echo $gaps_list['status_title'] ?></td>
                                    </tr>
                                    <?php }  ?>
                                  </tbody>
                                </table>
                                
                    
                             
                            </div>
                        </div>
                    </div></div></div>
                        
                     </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bulk_actions" id="gaps_bulk_actions" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo _l('bulk_actions'); ?></h4>
            </div>
            <div class="modal-body">
                <?php if(is_admin()){ ?>
                <div class="checkbox checkbox-danger">
                    <input type="checkbox" name="mass_delete" id="mass_delete">
                    <label for="mass_delete"><?php echo _l('mass_delete'); ?></label>
                </div>
                <hr class="mass_delete_separator" />
                <?php } ?>
                <div id="bulk_change">
                    <?php echo render_select('move_to_status_gaps_bulk',$statuses,array('id','name'),'gap_single_change_status'); ?>
                    <?php echo render_select('move_to_department_gaps_bulk',$departments,array('departmentid','name'),'department'); ?>
                    <?php echo render_select('move_to_priority_gaps_bulk',$priorities,array('priorityid','name'),'gap_priority'); ?>
                    <div class="form-group">
                        <?php echo '<p><b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b></p>'; ?>
                        <input type="text" class="tagsinput" id="tags_bulk" name="tags_bulk" value=""
                            data-role="tagsinput">
                    </div>
                    <?php if(get_option('services') == 1){ ?>
                    <?php echo render_select('move_to_service_gaps_bulk',$services,array('serviceid','name'),'service'); ?>
                    <?php } ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <a href="#" class="btn btn-info"
                    onclick="gaps_bulk_action(this); return false;"><?php echo _l('confirm'); ?></a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".has-row-options").click(function() {
          window.location = $(this).data("href");
      });
  });

  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>gaps/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.table tbody').empty();
              $('.table tbody').html(data);
            }
           }
      });
  }
</script>
  <script type="text/javascript">
    function filterbystatus($id) {
       if($id){
          $.ajax({
                   url:'<?=admin_url()?>gaps/index',
                   method:"POST",
                   data:{id:$id},
                   success:function(data){
                    if(data){
                      $('.table tbody').empty();
                      $('.table tbody').html(data);
                    }
                   }
          });
        }
    }
  </script>

</body>

</html>