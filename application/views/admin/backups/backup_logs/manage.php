
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">
						<div class="_buttons">
							<a href="#" onclick="new_backup_log(); return false;" class="btn btn-info pull-left display-block"><?php echo _l('New Backup Log'); ?></a>
						</div>
						<div class="clearfix"></div>
						<hr class="hr-panel-heading" />
						<?php if(count($backup_logs) > 0){ ?>

						<table class="table dt-table scroll-responsive">
							<thead>
								<th><?php echo _l('id'); ?></th>
								<th><?php echo _l('Backup Policy'); ?></th>
								<th><?php echo _l('Date'); ?></th>
								<th><?php echo _l('Inventory Product'); ?></th>
								<th><?php echo _l('Status'); ?></th>
								<th><?php echo _l('Remark'); ?></th>
								<th><?php echo _l('Options'); ?></th>
							</thead>
							<tbody>
								<?php foreach($backup_logs as $key => $backup_log){ ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td><a href="<?php echo admin_url('backups/backup/'.$backup_log['backup_id']); ?>" data-name="<?php echo $backup_log['backup_id']; ?>"><?php echo $backup_log['policy_title']; ?></a></td>
									<td><?php echo date('d-m-Y',strtotime($backup_log['backup_date'])); ?></td>
									<td><?php echo $backup_log['product_name'].'('.$backup_log['hostname'].'|'.$backup_log['purpose'].')'; ?></td>
									<td><?php echo $backup_log['status']; ?></td>
									<td><?php echo $backup_log['remark']; ?></td>
									<td>
										<a href="#" onclick="edit_backup_log(this,<?php echo $backup_log['id']; ?>,<?php echo $backup_log['backup_date']; ?>,'<?php echo $backup_log['policy_title']; ?>','<?php echo $backup_log['product_name']; ?>'); return false" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
										<!-- <a href="<?php echo admin_url('gaps/delete_backup_log/'.$backup_log['id']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a> -->
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php } else { ?>
						<p class="no-margin"><?php echo _l('No backup logs found'); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="backup_log" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('backups/log')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Backup Log Edit'); ?></span>
					<span class="add-title"><?php echo _l('Backup Log Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="clientid" class="control-label">
                                <?php echo _l('Select Client'); ?>
                            </label>
                            <select class="selectpicker" name="client_id" id="client_id"data-width="100%" data-live-search="true" onchange="client_backup_policy(this); return false">
                              <option value="" disabled selected=""> Select Client</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>"><?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
					<div class="col-md-6">
                      <div class="form-group">
                        <label for="backup_date" class="control-label">Backup Date</label>
                          <div class="input-group date">
                            <input type="text" id="backup_date" name="backup_date" class="form-control datepicker" 
                                   value="<?php if(isset($backup_log->backup_date)){
                                    echo $backup_log->backup_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="backup_id" class="control-label">
                                <?php echo _l('Backup Policy'); ?>
                            </label>
                            <select name="backup_id" id="backup_id" class="form-control" required onchange="get_backup_inventories(this); return false">
							</select>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div id="additional"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit_backup_log" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('backups/log')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Backup Log Edit'); ?></span>
					<span class="add-title"><?php echo _l('Backup Log Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="policy" class="control-label">
                                <?php echo _l('Backup Policy'); ?>
                            </label>
                            <input type="text" name="policy" id="policy" class="form-control" readonly>
                        </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
                            <label for="inventory_product" class="control-label">
                                <?php echo _l('Backup of(Inventory Product)'); ?>
                            </label>
                            <input type="text" name="inventory_product" id="inventory_product" class="form-control" readonly>
                        </div>
                    </div>
					<div id="edit_additional"></div>
					<div class="col-md-12">
                      <div class="form-group">
                        <label for="backup_date" class="control-label">Backup Date</label>
                          <div class="input-group date">
                            <input type="text" id="backup_date" name="backup_date" class="form-control datepicker" 
                                   value="<?php if(isset($backup_log->backup_date)){
                                    echo $backup_log->backup_date;
                                   }?>" autocomplete="off">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                          </div>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group select-placeholder">
                            <label for="status"><?php echo _l('Backup Status'); ?></label>
                            <select class="selectpicker" name="status" id="status"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Success">Success</option>
                                <option value="Failure">Failure</option>
								<option value="Warning">Warning</option>
                            </select>
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>
<script type="text/javascript">
    function client_backup_policy(url){
	  var id = $('#client_id').val();
      $.ajax({
           url:'<?=admin_url()?>backups/client_backup_policy/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
				$("#backup_id").empty();
				$('#additional').empty();
                if(JSON.parse(data).length > 0){
                    $('.backup_idselect').css('display','block');
					$("#backup_id").append(`<option value='' selected>Select Policy</option>`);
                    $.each(JSON.parse(data), function(index, json) {
                        $("#backup_id").append(`<option value='${JSON.parse(data)[index].id}'
                        ${(JSON.parse(data)[index].id==$('#backup_id').val())?'selected' : ''}>${JSON.parse(data)[index].policy_title}</option>`);
                    });
                }
           }
      });  
    }

	function get_backup_inventories(url){
	  var id = $('#backup_id').val();
      $.ajax({
           url:'<?=admin_url()?>backups/get_backup_inventories/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
				$('#additional').empty();
				$('#additional').append(data);
           }
      });  
    }
</script>
<script>
	function new_backup_log(){
		$('#additional').empty();
		$('#additional').append(hidden_input('id',0));
		$('#backup_log').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_backup_log(invoker,id,date,policy,product){
		var name = $(invoker).data('name');
		$('#edit_additional').empty();
		$('#edit_additional').append(hidden_input('id',id));
		$('#policy').val(policy);
		$('#inventory_product').val(product);
		//$('#backup_log select[name^="backup_id"] option[value="1"]').attr("selected","selected");
		//$('#backup_log input[name="backup_date"]').val(date);
		$('#edit_backup_log').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>
