<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'backup_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Backup Log</h4>
                  <div class="row">
                    <div class="additional"></div>
                     <div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="client_id" class="control-label">
                                <?php echo _l('Customer'); ?>
                            </label>
                            <select class="selectpicker" name="client_id" id="client_id"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select Customer</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>" 
                                    <?php if(isset($backups_details->client_id)){
                                        echo ($backups_details->client_id == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Policy Title</label>
                            <input type="text" name="policy_title" id="policy_title" 
                            value="<?php  if(isset($backups_details->policy_title))
                              {echo $backups_details->policy_title;}?>" class="form-control">
                        </div>
                    </div>

                     <div class="col-md-6">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="server" class="control-label">
                                <?php echo _l('Inventory Products'); ?>
                            </label>
                            <select class="selectpicker" name="server[]" multiple id="server" data-width="100%" data-live-search="true">
                              <!--<option value="" disabled selected=""> Select Products</option>-->
                                <?php foreach($inventory as  $product){ ?>
                                  <option value="<?php echo $product['id']?>" 
                                    <?php if(isset($backups_details->server)){
                                        echo (strpos($backups_details->server, $product['id']) !== false) ? 'selected' : '';}?>>
                                      <?php echo $product['product_name']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="id" id="id" 
                            value="<?php if(isset($backups_details->id)){echo $backups_details->id;}?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Restore Points</label>
                            <input type="number" name="restore_point" id="restore_point" 
                            value="<?php  if(isset($backups_details->restore_point))
                              {echo $backups_details->restore_point;}?>" class="form-control">
                        </div>
                    </div>
                  </div>
                  <div class="row"> 
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="archive"><?php echo _l('Archive'); ?></label>
                            <select class="selectpicker" name="archive" id="archive"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($backups_details->archive)){ echo ($backups_details->archive == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($backups_details->archive)){ echo ($backups_details->archive == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Archive Retention Period (In Months)</label>
                            <input type="number" name="archive_retention_period" id="archive_retention_period" 
                            value="<?php  if(isset($backups_details->archive_retention_period))
                              {echo $backups_details->archive_retention_period;}?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="full_backup"><?php echo _l('Full Backup'); ?></label>
                            <select class="selectpicker" name="full_backup" id="full_backup"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Weekly" <?php if(isset($backups_details->full_backup)){ echo ($backups_details->full_backup == 'Weekly') ? 'selected' : '';}?>>Weekly</option>
                                <option value="Daily" <?php if(isset($backups_details->full_backup)){ echo ($backups_details->full_backup == 'Daily') ? 'selected' : '';}?>>Daily</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group select-placeholder">
                            <label for="incremental_backup"><?php echo _l('Incremental Backup'); ?></label>
                            <select class="selectpicker" name="incremental_backup" id="incremental_backup"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Weekly" <?php if(isset($backups_details->incremental_backup)){ echo ($backups_details->incremental_backup == 'Weekly') ? 'selected' : '';}?>>Weekly</option>
                                <option value="Daily" <?php if(isset($backups_details->incremental_backup)){ echo ($backups_details->incremental_backup == 'Daily') ? 'selected' : '';}?>>Daily</option>
                            </select>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_backup_destination">First Backup Destination</label>
                            <input type="text" name="first_backup_destination" id="first_backup_destination" 
                            value="<?php  if(isset($backups_details->first_backup_destination)){
                              echo $backups_details->first_backup_destination;
                            } ?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="second_backup_destination">Second Backup Destination</label>
                            <input type="text" name="second_backup_destination" id="second_backup_destination" 
                            value="<?php  if(isset($backups_details->second_backup_destination)){
                              echo $backups_details->second_backup_destination;
                            } ?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="third_backup_destination">Third Backup Destination</label>
                            <input type="text" name="third_backup_destination" id="third_backup_destination" 
                            value="<?php  if(isset($backups_details->third_backup_destination)){
                              echo $backups_details->third_backup_destination;
                            } ?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Recovery Time Objective (Days)</label>
                            <input type="number" name="recovery_time_obj" id="recovery_time_obj" 
                            value="<?php  if(isset($backups_details->recovery_time_obj))
                              {echo $backups_details->recovery_time_obj;}?>" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Recovery Point Objective (Days)</label>
                            <input type="number" name="recovery_point_obj" id="recovery_point_obj" 
                            value="<?php  if(isset($backups_details->recovery_point_obj))
                              {echo $backups_details->recovery_point_obj;}?>" class="form-control">
                        </div>
                    </div>


                  </div>
                  
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
           <?php echo form_close(); ?>
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>
  <?php /* $this->load->view('admin/inventory_details/inventory_detail_category'); /*/?>
  <?php init_tail(); ?>

<script>
   $(function(){
     appValidateForm($('#backup_form'),
      {clientid:'required',backup_type:'required',start_date:'required',end_date:'required',
        notify_to: {
          email:true,
          required:true
        }
      });
   });
</script>
</body>
</html>