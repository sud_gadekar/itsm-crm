
                           

                           <div class="row form-inline dataTables_wrapper">
  <div class="col-md-6">
    <div class="dataTables_length" id="table-gap_details_length">
      <label>
        <select name="table-gap_details_length" aria-controls="table-gap_details" class="form-control input-sm">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
          <option value="-1">All</option>
        </select>
      </label>
    </div>
    <div class="dt-buttons btn-group">
      <button class="btn btn-default buttons-collection btn-default-dt-options" tabindex="0" aria-controls="table-gap_details" type="button" aria-haspopup="true" aria-expanded="false"><span>Export</span>
      </button>
    </div>
  </div>
  <div class="col-md-6">
    <div id="table-gap_details_filter" class="dataTables_filter">
      <label>
        <div class="input-group">
          <span class="input-group-addon">
            <span class="fa fa-search"></span>
            </span><input type="search" class="form-control input-sm" placeholder="Search..." aria-controls="table-gap_details">
          </div>
        </label>
      </div>
    </div>
    <div id="table-gap_details_processing" class="dataTables_processing panel panel-default" style="display: none;"><div class="dt-loader">
      
    </div>
  </div>
</div>
                            <table class="table dataTable dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Customer'); ?></th>
                                <th><?php echo _l('Restore Point'); ?></th>
                                <th><?php echo _l('Archive'); ?></th>
                                <th><?php echo _l('Archive Retention Period'); ?></th>
                                <th><?php echo _l('Full Backup'); ?></th>
                                <th><?php echo _l('Incremental Backup'); ?></th>
                                <th><?php echo _l('First Backup Destination'); ?></th>
                                <th><?php echo _l('Second Backup Destination'); ?></th>
                                <th><?php echo _l('Third Backup Destination'); ?></th>
                                <th><?php echo _l('Recovery Time Objective'); ?></th>
                                <th><?php echo _l('Recovery Point Objective'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($backups_lists) > 0){?>

                                <?php foreach ($backups_lists as $key => $backups_list) { ?>
                                <tr class="has-row-options" data-href="<?php echo admin_url('backups/backup/').$backups_list['id'] ?>">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('backups/backup/').$backups_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('backups/delete/').$backups_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td><?php echo $backups_list['company']; ?></td>
                                  <td><?php echo $backups_list['restore_point'] ?></td>
                                  <td><?php echo $backups_list['archive'] ?></td>
                                  <td><?php echo $backups_list['archive_retention_period'].' Months' ?></td>
                                  <td><?php echo $backups_list['full_backup'] ?></td>
                                  <td><?php echo $backups_list['incremental_backup'] ?></td>
                                  <td><?php echo $backups_list['first_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['second_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['third_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['recovery_time_obj'].' Days' ?></td>
                                  <td><?php echo $backups_list['recovery_point_obj'].' Days' ?></td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                      