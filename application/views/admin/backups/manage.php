<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('backups','','create')){ ?>
                  <a href="<?php echo admin_url('backups/backup'); ?>" class="btn btn-info">
                    <?php echo 'New Policy'; ?></a>
                  <a href="<?php echo admin_url('backups/backup_logs'); ?>" class="btn btn-info">
                    <?php echo 'Backup Logs'; ?></a>
                  <?php } ?>
            
                   <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="backups_total"></div>
                  </div>
               </div>
            <!-- </div>
            <div class="row"> -->
               <div class="panel-body">
               <div class="backups_table" id="small-table ">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                        
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('Sr.No.'); ?></th>
                                <th><?php echo _l('View Logs'); ?></th>
                                <th><?php echo _l('Customer'); ?></th>
                                <th><?php echo _l('Policy Title'); ?></th>
                                <th><?php echo _l('Restore Point'); ?></th>
                                <th><?php echo _l('Archive'); ?></th>
                                <th><?php echo _l('Archive Retention Period'); ?></th>
                                <th><?php echo _l('Full Backup'); ?></th>
                                <th><?php echo _l('Incremental Backup'); ?></th>
                                <th><?php echo _l('First Backup Destination'); ?></th>
                                <th><?php echo _l('Second Backup Destination'); ?></th>
                                <th><?php echo _l('Third Backup Destination'); ?></th>
                                <th><?php echo _l('Recovery Time Objective'); ?></th>
                                <th><?php echo _l('Recovery Point Objective'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($backups_lists) > 0){?>

                                <?php foreach ($backups_lists as $key => $backups_list) { ?>
                                <tr class="has-row-options" data-href="<?php echo admin_url('backups/backup/').$backups_list['id'] ?>">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('backups/backup/').$backups_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('backups/delete/').$backups_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                  <td>
                                    <a href="#" onclick="backup_log(this,<?php echo $backups_list['id']; ?>); return false" data-name="<?php echo $backups_list['id']; ?>" class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>
                                  </td>
                                  <td><?php echo $backups_list['company']; ?></td>
                                  <td><?php echo $backups_list['policy_title'] ?></td>
                                  <td><?php echo $backups_list['restore_point'] ?></td>
                                  <td><?php echo $backups_list['archive'] ?></td>
                                  <td><?php echo $backups_list['archive_retention_period'].' Months' ?></td>
                                  <td><?php echo $backups_list['full_backup'] ?></td>
                                  <td><?php echo $backups_list['incremental_backup'] ?></td>
                                  <td><?php echo $backups_list['first_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['second_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['third_backup_destination'] ?></td>
                                  <td><?php echo $backups_list['recovery_time_obj'].' Hours' ?></td>
                                  <td><?php echo $backups_list['recovery_point_obj'].' Days' ?></td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                         
                        </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Backup Logs</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<!-- Show Asset Data On Modal -->
<script type="text/javascript">
    function backup_log(url,id){
      $.ajax({
           url:'<?=admin_url()?>backups/backup_log_data/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              //alert(data)
              $('.logDetailsData').html(data);
            }
           }
      });
                              
      $('#logmodal').modal('show');   
    }
  </script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".has-row-options").click(function() {
          window.location = $(this).data("href");
      });
  });
  
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>backups/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.backups_table').html(data);
            }
           }
      });
  }
</script>
</body>
</html>
