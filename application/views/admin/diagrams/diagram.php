<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'diagram-form')); ?>
      <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <div class="panel_s">
               <div class="panel-body">
                  <h4 class="no-margin">
                     <?php echo $title; ?>
                  </h4>

                  <hr class="hr-panel-heading" />
                  <div class="clearfix"></div>
                  <?php $value = (isset($diagram) ? $diagram->title : ''); ?>
                  <?php $attrs = (isset($diagram) ? array() : array('autofocus'=>true)); ?>

                  <?php echo render_input('title','Title',$value,'text',$attrs); ?>
                  
                   <div class="form-group select-placeholder f_client_id">
                        <label for="clientid" class="control-label">
                            
                            <?php echo _l('Assigned to ( Customer )'); ?>
                          </label>
               
                        <select class="selectpicker" name="clientid" id="clientid"data-width="100%" data-live-search="true">
                          <option value="" disabled selected=""> Select Type</option>
                          <?php foreach($clients as  $client){ ?>
                              <option value="<?php echo $client['userid']?>" 
                                <?php if(isset($diagram->clientid)){
                                  echo ($diagram->clientid == $client['userid']) ? 'selected' : '';}?>>
                                <?php echo $client['company']; ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <?php if((isset($diagram) && $diagram->diagram_file == NULL) || !isset($diagram)){ ?>
                     <div class="form-group">
                        <label for="diagram_file" class="profile-image"><?php echo _l('Diagram File'); ?></label>
                        <input type="file" name="diagram_file" class="form-control" id="diagram_file">
                     </div>
                     <?php } ?>
                     <?php if(isset($diagram) && $diagram->diagram_file != NULL){ ?>
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-9">
                              <?php //diagram_file($diagram->id,array('img','img-responsive','staff-profile-image-thumb'),'thumb'); 
                              
                                 $url = base_url('assets/images/user-placeholder.jpg');
                                 $id = trim($diagram->id);
                                 $classes = array('img','img-responsive');
                                 $type = 'thumb';
                                 $img_attrs = array('thumb');
                                 $diagram_file = '';
                        
                                 $_attributes = '';
                                 foreach ($img_attrs as $key => $val) {
                                    $_attributes .= $key . '=' . '"' . html_escape($val) . '" ';
                                 }
                        
                                 $blankImageFormatted = '<img src="' . $url . '" ' . $_attributes . ' class="' . implode(' ', $classes) . '" />';
                        
                                 
                                 $CI     = & get_instance();
                                 $result = $CI->app_object_cache->get('diagram-image-data-' . $id);
                        
                                 if (!$result) {
                                    $CI->db->select('diagram_file');
                                    $CI->db->where('id', $id);
                                    $result = $CI->db->get(db_prefix() . 'diagrams')->row();
                                    $CI->app_object_cache->add('diagram-image-data-' . $id, $result);
                                 }
                        
                                 if (!$result) {
                                    echo $blankImageFormatted;
                                 }
                        
                                 if ($result && $result->diagram_file !== null) {
                                    $profileImagePath = 'uploads/diagrams/' . $id . '/' . $type . '_' . $result->diagram_file;

                                    if (file_exists($profileImagePath)) {
                                       $diagram_file = '<img ' . $_attributes . ' src="' . base_url($profileImagePath) . '" class="' . implode(' ', $classes) . '" />';
                                    } else {
                                       echo $blankImageFormatted;
                                    }
                                 } else {
                                    $diagram_file = '<img src="' . $url . '" ' . $_attributes . ' class="' . implode(' ', $classes) . '" />';
                                 }

                                 echo $diagram_file;
                              
                              ?>
                           </div>
                           <div class="col-md-3 text-right">
                              <a href="<?php echo admin_url('diagrams/remove_diagram_file/'.$diagram->id); ?>"><i class="fa fa-remove"></i></a>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
                  
                  <p class="bold"><?php echo _l('Description'); ?></p>
                  <?php $contents = ''; if(isset($diagram)){$contents = $diagram->description;} ?>
                  <?php echo render_textarea('description','',$contents,array(),array(),'','tinymce tinymce-manual'); ?>
               </div>
            </div>
         </div>
         <?php if((has_permission('diagrams','','create') && !isset($diagram)) || has_permission('diagrams','','edit') && isset($diagram)){ ?>
         <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
            <button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
         </div>
         <?php } ?>
      </div>
      <?php echo form_close(); ?>
   </div>
</div>
<?php init_tail(); ?>
<script>
   $(function(){
     init_editor('#description', {append_plugins: 'stickytoolbar'});
     appValidateForm($('#diagram-form'),{title:'required',diagram_file:'required',clientid:'required'});
   });
</script>
</body>
</html>
