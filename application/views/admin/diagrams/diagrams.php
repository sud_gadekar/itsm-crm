<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head();
   $has_permission_edit = has_permission('diagrams','','edit');
   $has_permission_create = has_permission('diagrams','','create');
   ?>
<div id="wrapper">
<div class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="panel_s mtop5">
            <div class="panel-body">
               <div class="_buttons">

                  <?php if($has_permission_create){ ?>
                  <a href="<?php echo admin_url('diagrams/diagram'); ?>" class="btn btn-info mright5"><?php echo _l('New Daigram'); ?></a>
                  <?php } else { ?>
                    <a href="#" class="btn btn-info mright5"><?php echo _l('Diagram'); ?></a>
                  <?php } ?>
                  <?php if(count($clients) > 0 && is_admin()){ ?>
                  <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter(<?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                    
                        </ul>
                    </div>
                    <?php } ?>
                  <div class="_hidden_inputs _filters">
                     
                  </div>
               </div>
              </div>
               <div class="panel-body">
               <div class="diagrams_table" id="small-table">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                       
                        <div class="mtop20">
                         
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Company'); ?></th>
                                <th><?php echo _l('Title'); ?></th>
                                <th><?php echo _l('Diagram File'); ?></th>
                                <th><?php echo _l('Description'); ?></th>
                                <th><?php echo _l('Created On'); ?></th>
                              </thead>
                              <tbody>
                                <?php foreach ($diagrams_lists as $key =>  $diagrams_list) { ?>
                                <tr class="has-row-options" data-href="<?php echo admin_url('diagrams/diagram/').$diagrams_list['id'] ?>">
                                  <td><?php echo $key + 1; ?>
                                    <div class="row-options">
                                        
                                        <a href="<?php echo admin_url('diagrams/diagram/').$diagrams_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('diagrams/delete/').$diagrams_list['id'] ?>" class="text-danger _delete">Delete </a>   
                                  </td>
                                   <td><?php echo $diagrams_list['company'] ?></td>
                                  <td><?php echo $diagrams_list['title'] ?></td>
                                  <td>
                                    <a href="<?php echo base_url('uploads/diagrams/'.$diagrams_list['id'].'/thumb_'.$diagrams_list['diagram_file']); ?>" target="_blank"><?php echo $diagrams_list['diagram_file'] ?></a>
                                  </td>
                                  <td><?php echo $diagrams_list['description'] ?></td>
                                  <td><?php echo $diagrams_list['created_at'] ?></td>
                                </tr>
                                <?php }  ?>
                              </tbody>
                            </table>
                          
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="diagrams" class="hide">
                  </div>
               </div>
             </div>
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   Dropzone.autoDiscover = false;
   $(function(){
             // diagrams additional server params
             var diagrams_ServerParams = {};
             $.each($('._hidden_inputs._filters input'),function(){
               diagrams_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
             });
             //initDataTable('.table-diagrams', admin_url+'diagrams/table', 'undefined', 'undefined', diagrams_ServerParams, <?php echo hooks()->apply_filters('diagrams_table_default_order', json_encode(array(5,'desc'))); ?>).column(0).visible(false, false).columns.adjust();

            // init_diagram();

             $('#diagram_convert_helper_modal').on('show.bs.modal',function(){
                var emptyNote = $('#tab_diagram').attr('data-empty-note');
                var emptyName = $('#tab_diagram').attr('data-empty-name');
                if(emptyNote == '1' && emptyName == '1') {
                    $('#inc_field_wrapper').addClass('hide');
                } else {
                    $('#inc_field_wrapper').removeClass('hide');
                    emptyNote === '1' && $('.inc_note').addClass('hide') || $('.inc_note').removeClass('hide')
                    emptyName === '1' && $('.inc_name').addClass('hide') || $('.inc_name').removeClass('hide')
                }
             });

             $('body').on('click','#diagram_confirm_convert',function(){
              var parameters = new Array();
              if($('input[name="diagram_convert_invoice_type"]:checked').val() == 'save_as_draft_true'){
                parameters['save_as_draft'] = 'true';
              }
              parameters['include_name'] = $('#inc_name').prop('checked');
              parameters['include_note'] = $('#inc_note').prop('checked');
              window.location.href = buildUrl(admin_url+'diagrams/convert_to_invoice/'+$('body').find('.diagram_convert_btn').attr('data-id'), parameters);
            });
           });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
      $(".has-row-options").click(function() {
          window.location = $(this).data("href");
      });
  });
  
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>diagrams/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              console.log(data);
              $('.diagrams_table').html(data);
            }
           }
      });
  }
</script>

</body>
</html>
