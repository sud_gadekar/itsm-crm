<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="_buttons">
                            <a href="<?php echo admin_url('team_availiabilities/team_availiable'); ?>"
                                class="btn btn-info pull-left display-block mright5">
                                <?php echo _l('Add'); ?>
                            </a>
                        </div>
                      </div>
                    <div class="panel-body">
                   <div class="gaps_table" id="small-table ">
                      <div class="">
                         <div class="">
                            <div class="clearfix"></div>
                            <div class="mtop20">
                                <div class="clearfix"></div>
                                <table class="table dt-table scroll-responsive">
                                  <thead>
                                    <th><?php echo _l('Staff'); ?></th>
                                    <th><?php echo _l('Status'); ?></th>
                                    <th><?php echo _l('From - To Date'); ?></th>
                                    <!-- <th><?php echo _l('Project'); ?></th> -->
                                  </thead>
                                  <tbody>
                                   
                                    <?php if(!empty($team_availiables)){



                                      foreach ($team_availiables as $key => $team_availiable) { ?>
                                    <tr class="has-row-options">
                                      

                                      </td>
                                       <td>
                                        <?php echo staff_profile_image($team_availiable['staff_id'],['staff-profile-image-small']); ?>
                                        <?php echo get_staff_full_name($team_availiable['staff_id']) ;?>
                                          
                                          <div class="row-options">
                                            <a href="<?php echo admin_url('team_availiabilities/team_availiable/').$team_availiable['id'] ?>">Edit </a> 
                                            <span class="text-dark"> | </span>
                                            <a href="<?php echo admin_url('team_availiabilities/delete/').$team_availiable['id'] ?>" class="text-danger _delete">Delete </a>
                                          </div> 
                                        </td>
                                      <td>
                                        <?php if($team_availiable['status_id'] == 1 ){
                                              echo "On Leave";
                                        } elseif ($team_availiable['status_id'] == 2 ) {
                                              echo "On Project";
                                        } elseif ($team_availiable['status_id'] == 3 ) {
                                              echo "On Support Request";
                                        } elseif ($team_availiable['status_id'] == 4 ) {
                                              echo "On Service Report";
                                        } elseif ($team_availiable['status_id'] == 5 ) {
                                              echo "Availiable";
                                        } else {
                                              echo "NA";
                                        }

                                           ?>
                                          
                                        </td>
                                      <td>
                                        <?php 
                                          if($team_availiable['from_date'] != 0 && $team_availiable['to_date'] != 0){
                                            echo date('Y-m-d', strtotime($team_availiable['from_date'])).' - '.date('Y-m-d', strtotime($team_availiable['to_date'])); 
                                          } 
                                        ?></td>
                                      <!-- <td><?php echo get_project_name_by_id($team_availiable['project_id']);?></td> -->
                                      
                                    </tr>
                                    <?php }


                                    }?>


                                  </tbody>
                                </table>
                                
                    
                             
                            </div>
                        </div>
                    </div></div>
                  </div>
                        
                     </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bulk_actions" id="gaps_bulk_actions" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo _l('bulk_actions'); ?></h4>
            </div>
            <div class="modal-body">
                <?php if(is_admin()){ ?>
                <div class="checkbox checkbox-danger">
                    <input type="checkbox" name="mass_delete" id="mass_delete">
                    <label for="mass_delete"><?php echo _l('mass_delete'); ?></label>
                </div>
                <hr class="mass_delete_separator" />
                <?php } ?>
                <div id="bulk_change">
                    <?php echo render_select('move_to_status_gaps_bulk',$statuses,array('id','name'),'gap_single_change_status'); ?>
                    <?php echo render_select('move_to_department_gaps_bulk',$departments,array('departmentid','name'),'department'); ?>
                    <?php echo render_select('move_to_priority_gaps_bulk',$priorities,array('priorityid','name'),'gap_priority'); ?>
                    <div class="form-group">
                        <?php echo '<p><b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b></p>'; ?>
                        <input type="text" class="tagsinput" id="tags_bulk" name="tags_bulk" value=""
                            data-role="tagsinput">
                    </div>
                    <?php if(get_option('services') == 1){ ?>
                    <?php echo render_select('move_to_service_gaps_bulk',$services,array('serviceid','name'),'service'); ?>
                    <?php } ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <a href="#" class="btn btn-info"
                    onclick="gaps_bulk_action(this); return false;"><?php echo _l('confirm'); ?></a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php init_tail(); ?>

<script type="text/javascript">
  function filter(id){
     $.ajax({
           url:'<?=admin_url()?>gaps/filter_by_customer',
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              $('.table tbody').empty();
              $('.table tbody').html(data);
            }
           }
      });
  }
</script>
  <script type="text/javascript">
    function filterbystatus($id) {
       if($id){
          $.ajax({
                   url:'<?=admin_url()?>gaps/index',
                   method:"POST",
                   data:{id:$id},
                   success:function(data){
                    if(data){
                      $('.table tbody').empty();
                      $('.table tbody').html(data);
                    }
                   }
          });
        }
    }
  </script>

</body>

</html>