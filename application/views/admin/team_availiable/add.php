<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'team_availiable_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
          <!-- <button class="btn btn-info save-and-add-contact customer-form-submiter">Save </button> -->
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading"><?php echo $title?></h4>
                  <div class="row">
                    <div class="additional"></div>
                       
                    <div class="col-md-6">
                      <input type="hidden" name="id" value="<?php if(isset($team_availiables->id)){echo $team_availiables->id;}?>">
                         <div class="form-group select-placeholder">
                              <label for="staff_id" class="control-label">
                                  <?php echo _l('Staff'); ?>
                                </label>
                              
                              <?php if(is_staff_logged_in() && !is_admin()) {?>
                                <select class="selectpicker" name="staff_id" id="staff_id"data-width="100%" data-live-search="true" disabled>                                 
                                  <option value="<?php echo get_staff_user_id(); ?>" disabled selected=""><?php echo get_staff_full_name(get_staff_user_id()); ?></option>
                                </select>
                              <?php } else { ?>  
                                <select class="selectpicker" name="staff_id" id="staff_id"data-width="100%" data-live-search="true">
                                  <option value="" disabled selected=""> Select Staff</option>
                                  <?php foreach($staffs as  $staff){ ?>
                                      <option value="<?php echo $staff['staffid']?>" 
                                        <?php if(isset($team_availiables->staff_id)){
                                          echo ($team_availiables->staff_id == $staff['staffid']) ? 'selected' : '';}?>>
                                        <?php echo $staff['firstname']." ".$staff['lastname']; ?></option>
                                  <?php } ?>
                                </select>
                              <?php } ?>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                         <div class="form-group select-placeholder">
                              <label for="status_id" class="control-label">
                                  <?php echo _l('Status'); ?>
                                </label>
                              <select class="selectpicker" name="status_id" id="status_id"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Status</option>
                                  
                                  <option value="1" <?php if(isset($team_availiables->status_id)){
                                      echo ($team_availiables->status_id == 1 ) ? 'selected' : '';
                                    } ?>>
                                  <?php echo "On Leave"; ?></option>
                                  
                                  <option value="2" <?php if(isset($team_availiables->status_id)){
                                      echo ($team_availiables->status_id == 2 ) ? 'selected' : '';
                                    } ?>>
                                  <?php echo "On Project"; ?></option>

                                  <option value="3" <?php if(isset($team_availiables->status_id)){
                                      echo ($team_availiables->status_id == 3 ) ? 'selected' : '';
                                    } ?>>
                                  <?php echo "On Support Request"; ?></option>

                                  <option value="4" <?php if(isset($team_availiables->status_id)){
                                      echo ($team_availiables->status_id == 4 ) ? 'selected' : '';
                                    } ?>>
                                  <?php echo "On Service Request"; ?></option>

                                  <option value="5" <?php if(isset($team_availiables->status_id)){
                                      echo ($team_availiables->status_id == 5 ) ? 'selected' : '';
                                    } ?>>
                                  <?php echo "Availiable"; ?></option>
                            </select>
                        </div>
                    </div>

                    
                    <div class="leave_from_to">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="from_date" class="control-label">From</label>
                              <div class="input-group date">
                                <input type="text" id="from_date" name="from_date" class="form-control datepicker" 
                                      value="<?php if(isset($team_availiables->from_date)){echo $team_availiables->from_date;}?>" autocomplete="off">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar calendar-icon"></i>
                                </div>
                              </div>
                            </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="to_date" class="control-label">To</label>
                            <div class="input-group date">
                              <input type="text" id="to_date" name="to_date" class="form-control datepicker" 
                                     value="<?php if(isset($team_availiables->to_date)){echo $team_availiables->to_date;}?>" autocomplete="off">
                              <div class="input-group-addon">
                                  <i class="fa fa-calendar calendar-icon"></i>
                              </div>
                            </div>
                          </div>
                      </div>
                      
                    </div>
                      
                  
                     

                  <div class="show_project" style="display: none;">
                    <div class="col-md-6">
                         <div class="form-group select-placeholder">
                              <label for="project_id" class="control-label">
                                  <?php echo _l('Project'); ?>
                                </label>
                              <select class="selectpicker" name="project_id" id="project_id" data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select Project</option>
                                  <?php foreach($projects as  $project){ ?>
                                    <option value="<?php echo $project['id']?>"

                                    <?php if(isset($team_availiables->project_id)){
                                        echo ($team_availiables->project_id == $project['id']) ? 'selected' : '';}?>

                                      >
                                      <?php echo $project['name']; ?></option>
                                  <?php } ?>
                            </select>
                        </div>
                    </div>

                  </div>

                  <div class="leave_types" style="display: none;">
                     <div class="col-md-6">
                        <div class="form-group">
                          <label for="leave_type" class="control-label">Leave Type</label>
                            <input type="text" id="leave_type" name="leave_type" class="form-control" 
                                     value="<?php if(isset($team_availiables->leave_type)){echo $team_availiables->leave_type;}?>" autocomplete="off">
                            
                          </div>
                    </div>

                  </div>

                  <div class="on_tickets" style="display: none;">

                    <div class="col-md-6">
                         <div class="form-group select-placeholder">
                              <label for="ticket_subject" class="control-label">
                                  <?php echo _l('Ticket Subject'); ?>
                                </label>
                              <select class="selectpicker" name="ticket_subject" id="ticket_subject" data-width="100%" data-live-search="true">
                              
                                 
                            </select>
                        </div>
                    </div>

                  </div>
                   
                    
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?>

		
      </div>
    <div class="btn-bottom-pusher"></div>
  </div>


  <?php init_tail(); ?>

  <script>
   $(function(){
     appValidateForm($('#team_availiable_form'),{staff_id:'required'});
   });
  </script>


  <script type="text/javascript">
    jQuery(function($) {
      $('#status_id').change(function(){ 
          var value = $(this).val();
          
          if(value == 1){
            $('.show_project, .on_tickets').hide();
            $('.leave_types, .leave_from_to').removeAttr('style');
            $("#project_id option:selected, #ticket_subject option:selected").val('');
          }
          if(value == 2){
            $('.leave_types, .on_tickets').hide();
            $('.show_project, .leave_from_to').removeAttr('style');
            $("#leave_type").val(''); 
            $("#ticket_subject option:selected").val('');
          }
          if(value == 3 || value == 4) {
            $('.show_project,.leave_types').hide();
            $('.on_tickets, .leave_from_to').removeAttr('style');
            $("#leave_type").val(''); 
            $("#project_id option:selected").val('');
          }
          if(value == 5) {
            $('.show_project, .leave_types, .on_tickets, .leave_from_to').hide();
            $("#leave_type").val('');
            $("#project_id option:selected, #ticket_subject option:selected, #from_date, #to_date").val('');
          }
           
      }).trigger('change');

    });
  </script>
  <!-- Get  Ticket Subject Based on Related To -->
  <script type="text/javascript">
    jQuery(function($) {
      $('#status_id').change(function(){ 
        var value = $(this).val();
        var related_to = '';
        if(value == 3) {
          related_to = 'ticket';
        } else if(value == 4) {
          related_to = 'service_request';
        }

        if(related_to != '') {
          $.ajax({
                   url:'<?=admin_url()?>team_availiabilities/get_ticket_subject',
                   method:"POST",
                   data:{table_name:related_to},
                   success:function(data){
                    $("#ticket_subject").empty();
                      if(JSON.parse(data).length > 0) {
                          $.each(JSON.parse(data), function(index, json) {
                              $("#ticket_subject").append(`<option value='${JSON.parse(data)[index].id}'
                                >${'#'+JSON.parse(data)[index].id+' - '+JSON.parse(data)[index].subject}</option>`);
                          });
                          $('.selectpicker').selectpicker('refresh')
                      }
                   }
          });
        }
          
      }).trigger('change');
        
    });
  </script>
</body>
</html>