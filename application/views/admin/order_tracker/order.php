<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
     <?php echo form_open($this->uri->uri_string(),array('id'=>'order_form')); ?>
      <div class="row">
        <div class="col-md-12"></div>
        <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
          <button type="submit" class="btn btn-info only-save customer-form-submiter">Save</button>
        </div>
        <div class="col-md-12">
          <div class="panel_s">
            <div class="panel-body">
              <div>
                <div class="tab-content">
                  <h4 class="customer-profile-group-heading">Order Details</h4>
                  <div class="row">
                    <div class="additional"></div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">SO Number</label>
                            <input type="text" name="so_number" id="so_number" 
                            value="<?php  if(isset($order_details->so_number))
                              {echo $order_details->so_number;}?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name">Description</label>
                            <input type="text" name="description" id="description" 
                            value="<?php  if(isset($order_details->description))
                              {echo $order_details->description;}?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="client_id" class="control-label">
                                <?php echo _l('Customer'); ?>
                            </label>
                            <select class="selectpicker" name="client_id" id="client_id"data-width="100%" data-live-search="true" onchange="get_contacts(this); return false" required>
                              <option value="" disabled selected=""> Select Customer</option>
                                <?php foreach($clients as  $client){ ?>
                                  <option value="<?php echo $client['userid']?>" 
                                    <?php if(isset($order_details->client_id)){
                                        echo ($order_details->client_id == $client['userid']) ? 'selected' : '';}?>>
                                      <?php echo $client['company']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group select-placeholder"> <!-- select-placeholder -->
                            <label for="contact_id" class="control-label">
                                <?php echo _l('Contacts for Notifications'); ?>
                            </label>
                            <select class="form-control selectpicker" name="contact_id[]" id="contact_id" data-width="100%" data-live-search="true" multiple> <!--selectpicker-->
                            </select>
                            
                            <input type="hidden" name="contact_ids" id="contact_ids" value="<?php if(isset($order_details->contact_id)){echo $order_details->contact_id;}?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">LPO Number</label>
                            <input type="text" name="lpo_no" id="lpo_no" 
                            value="<?php  if(isset($order_details->lpo_no))
                              {echo $order_details->lpo_no;}?>" class="form-control" required>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="name">PO Number</label>
                            <input type="text" name="po_no" id="po_no" 
                            value="" class="form-control" required>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <?php if(!isset($order_details)){ ?>
                          <div class="lead-select-date-contacted">
                              <?php echo render_date_input('order_date','Order Date','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                          </div>
                          <?php } else { ?>
                              <?php echo render_date_input('order_date','Order Date',_dt($order_details->order_date),array('data-date-start-date'=>date('Y-m-d'))); ?>
                        <?php } ?>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Vendor Name</label>
                            <input type="text" name="vendor_name" id="vendor_name" 
                            value="" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="items_received"><?php echo _l('Items Received'); ?></label>
                            <select class="selectpicker" name="items_received" id="items_received"data-width="100%" data-live-search="true" required>
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->items_received)){ echo ($order_details->items_received == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->items_received)){ echo ($order_details->items_received == 'No') ? 'selected' : '';}?>>No</option>
                                <option value="Partial" <?php if(isset($order_details->items_received)){ echo ($order_details->items_received == 'Partial') ? 'selected' : '';}?>>Partial</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <?php if(!isset($order_details)){ ?>
                          <div class="lead-select-date-contacted">
                              <?php echo render_date_input('expected_delivery','Expected Delivery','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                          </div>
                          <?php } else { ?>
                              <?php echo render_date_input('expected_delivery','Expected Delivery',_dt($order_details->expected_delivery),array('data-date-start-date'=>date('Y-m-d'))); ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <?php if(!isset($order_details)){ ?>
                          <div class="lead-select-date-contacted">
                              <?php echo render_date_input('delivery_date','Delivery Date','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                          </div>
                          <?php } else { ?>
                              <?php echo render_date_input('delivery_date','Delivery Date',_dt($order_details->delivery_date),array('data-date-start-date'=>date('Y-m-d'))); ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="items_delivered"><?php echo _l('Items Delivered'); ?></label>
                            <select class="selectpicker" name="items_delivered" id="items_delivered"data-width="100%" data-live-search="true" required>
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->items_delivered)){ echo ($order_details->items_delivered == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->items_delivered)){ echo ($order_details->items_delivered == 'No') ? 'selected' : '';}?>>No</option>
                                <option value="Partial" <?php if(isset($order_details->items_delivered)){ echo ($order_details->items_delivered == 'Partial') ? 'selected' : '';}?>>Partial</option>
                            </select>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="bill_created"><?php echo _l('Bill Created'); ?></label>
                            <select class="selectpicker" name="bill_created" id="bill_created"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->bill_created)){ echo ($order_details->bill_created == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->bill_created)){ echo ($order_details->bill_created == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="pi_attached"><?php echo _l('PI Attached'); ?></label>
                            <select class="selectpicker" name="pi_attached" id="pi_attached"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->pi_attached)){ echo ($order_details->pi_attached == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->pi_attached)){ echo ($order_details->pi_attached == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="service_request"><?php echo _l('Service Required'); ?></label>
                            <select class="selectpicker" name="service_request" id="service_request"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Project" <?php if(isset($order_details->service_request)){ echo ($order_details->service_request == 'Project') ? 'selected' : '';}?>>Project</option>
                                <option value="Service Request" <?php if(isset($order_details->service_request)){ echo ($order_details->service_request == 'Service Request') ? 'selected' : '';}?>>Service Request</option>
                                <option value="NA" <?php if(isset($order_details->service_request)){ echo ($order_details->service_request == 'NA') ? 'selected' : '';}?>>NA</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="sr_id" class="control-label">
                                <?php echo _l('Link SR'); ?>
                            </label>
                            <select class="selectpicker" name="sr_id" id="sr_id"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select SR</option>
                                <?php foreach($sr_requests as  $sr_request){ ?>
                                  <option value="<?php echo $sr_request['service_requestid']?>" 
                                    <?php if(isset($order_details->sr_id)){
                                        echo ($order_details->sr_id == $sr_request['service_requestid']) ? 'selected' : '';}?>>
                                      <?php echo $sr_request['subject']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="project_id" class="control-label">
                                <?php echo _l('Link Project'); ?>
                            </label>
                            <select class="selectpicker" name="project_id" id="project_id"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select Report</option>
                                <?php foreach($projects as  $project){ ?>
                                  <option value="<?php echo $project['id']?>" 
                                    <?php if(isset($order_details->project_id)){
                                        echo ($order_details->project_id == $project['id']) ? 'selected' : '';}?>>
                                      <?php echo $project['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="renewal_required"><?php echo _l('Renewal Required'); ?></label>
                            <select class="selectpicker" name="renewal_required" id="renewal_required"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->renewal_required)){ echo ($order_details->renewal_required == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->renewal_required)){ echo ($order_details->renewal_required == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="renewal_db_updated"><?php echo _l('Renewal DB Updated'); ?></label>
                            <select class="selectpicker" name="renewal_db_updated" id="renewal_db_updated"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->renewal_db_updated)){ echo ($order_details->renewal_db_updated == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->renewal_db_updated)){ echo ($order_details->renewal_db_updated == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="sales_staff_id" class="control-label">
                                <?php echo _l('Sales Person'); ?>
                            </label>
                            <select class="selectpicker" name="sales_staff_id" id="sales_staff_id"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select Sales Person</option>
                                <?php foreach($sales_staffs as  $sales_staff){ ?>
                                  <option value="<?php echo $sales_staff['staffid']?>" 
                                    <?php if(isset($order_details->sales_staff_id)){
                                        echo ($order_details->sales_staff_id == $sales_staff['staffid']) ? 'selected' : '';}?>>
                                      <?php echo $sales_staff['full_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="invoice_submited"><?php echo _l('Invoice Submitted'); ?></label>
                            <select class="selectpicker" name="invoice_submited" id="invoice_submited"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->invoice_submited)){ echo ($order_details->invoice_submited == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->invoice_submited)){ echo ($order_details->invoice_submited == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Invoice No</label>
                            <input type="text" name="invoice_no" id="invoice_no" value="<?php  if(isset($order_details->invoice_no)){echo $order_details->invoice_no;}?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Amount</label>
                            <input type="text" name="amount" id="amount" value="<?php  if(isset($order_details->amount)){echo $order_details->amount;}?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <?php if(!isset($order_details)){ ?>
                          <div class="lead-select-date-contacted">
                              <?php echo render_date_input('inv_submission_date','Invoice Submission Date','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                          </div>
                          <?php } else { ?>
                              <?php echo render_date_input('inv_submission_date','Invoice Submission Date',_dt($order_details->inv_submission_date),array('data-date-start-date'=>date('Y-m-d'))); ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="payment_received"><?php echo _l('Payment Received'); ?></label>
                            <select class="selectpicker" name="payment_received" id="payment_received"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->payment_received)){ echo ($order_details->payment_received == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->payment_received)){ echo ($order_details->payment_received == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="invoice_acknowledge"><?php echo _l('Invoice Acknolwedged'); ?></label>
                            <select class="selectpicker" name="invoice_acknowledge" id="invoice_acknowledge"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->invoice_acknowledge)){ echo ($order_details->invoice_acknowledge == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->invoice_acknowledge)){ echo ($order_details->invoice_acknowledge == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <?php if(!isset($order_details)){ ?>
                          <div class="lead-select-date-contacted">
                              <?php echo render_date_input('payment_date','Payment Date','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                          </div>
                          <?php } else { ?>
                              <?php echo render_date_input('payment_date','Payment Date',_dt($order_details->payment_date),array('data-date-start-date'=>date('Y-m-d'))); ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Payment Terms</label>
                            <input type="text" name="payment_received_in" id="payment_received_in" value="<?php  if(isset($order_details->payment_received_in)){echo $order_details->payment_received_in;}?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="status_id" class="control-label">
                                <?php echo _l('Status'); ?>
                            </label>
                            <select class="selectpicker" name="status_id" id="status_id"data-width="100%" data-live-search="true">
                              <option value="" disabled selected=""> Select Status</option>
                                <?php foreach($statuses as  $status){ ?>
                                  <option value="<?php echo $status['statusid']?>" 
                                    <?php if(isset($order_details->status_id)){
                                        echo ($order_details->status_id == $status['statusid']) ? 'selected' : '';}?>>
                                      <?php echo $status['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="order_completed"><?php echo _l('Order Completed'); ?></label>
                            <select class="selectpicker" name="order_completed" id="order_completed"data-width="100%" data-live-search="true">
                                <option value="" disabled selected=""> Select </option>
                                <option value="Yes" <?php if(isset($order_details->order_completed)){ echo ($order_details->order_completed == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                <option value="No" <?php if(isset($order_details->order_completed)){ echo ($order_details->order_completed == 'No') ? 'selected' : '';}?>>No</option>
                            </select>
                        </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12">
                    <a onclick="addItem()" class="btn btn-info"><?php echo 'Add New Item'; ?></a>
                    <table id="itemtable" class="table scroll-responsive">
                        <thead>
                            <th><?php echo _l('Item Name'); ?></th>
                            <th><?php echo _l('Quantity'); ?></th>
                            <th><?php echo _l('Vendor Name'); ?></th>
                            <th><?php echo _l('Expected Delivery Date'); ?></th>
                            <th><?php echo _l('Delivery Date'); ?></th>
                            <th><?php echo _l('Item Received'); ?></th>
                            <th><?php echo _l('Item Delivered'); ?></th>
                            <th><?php echo _l('Option'); ?></th>
                        </thead>
                        <tbody>
                            <?php if(count($order_item_lists) > 0){ ?>
                            <?php foreach ($order_item_lists as $key => $order_item) { ?>
                                <tr class="has-row-options">
                                  <td>
                                    <select class="selectpicker" name="item_id<?php echo $key?>" id="item_id<?php echo $key?>" data-width="100%" data-live-search="true">
                                    <option value="" disabled selected=""> Select Item</option>
                                        <?php foreach($items as  $item){ ?>
                                        <option value="<?php echo $item['id']?>" 
                                            <?php if(isset($order_item['item_id'])){
                                                echo ($order_item['item_id'] == $item['id']) ? 'selected' : '';}?>>
                                            <?php echo $item['description']; ?></option>
                                        <?php } ?>
                                    </select>
                                  </td>
                                  <td>
                                    <input type="text" name="quantity<?php echo $key?>" id="quantity<?php echo $key?>" value="<?php  if(isset($order_item['quantity'])){echo $order_item['quantity'];}?>" class="form-control">
                                  </td>
                                  <td>
                                    <input type="text" name="vendor_name<?php echo $key?>" id="vendor_name<?php echo $key?>" value="<?php  if(isset($order_item['vendor_name'])){echo $order_item['vendor_name'];}?>" class="form-control">
                                  </td>
                                  <td>
                                    <?php echo render_date_input('expected_delivery_date'.$key,'',_dt($order_item['expected_delivery_date']),array('data-date-start-date'=>date('Y-m-d'))); ?>
                                  </td>
                                  <td>
                                    <?php echo render_date_input('delivery_date'.$key,'',_dt($order_item['delivery_date']),array('data-date-start-date'=>date('Y-m-d'))); ?>
                                  </td>
                                  <td>
                                    <select class="selectpicker" name="item_received<?php echo $key?>" id="item_received<?php echo $key?>" data-width="100%" data-live-search="true">
                                        <option value="" disabled selected=""> Select </option>
                                        <option value="Yes" <?php if(isset($order_item['item_received'])){ echo ($order_item['item_received'] == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                        <option value="No" <?php if(isset($order_item['item_received'])){ echo ($order_item['item_received'] == 'No') ? 'selected' : '';}?>>No</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select class="selectpicker" name="item_delivered<?php echo $key?>" id="item_delivered<?php echo $key?>" data-width="100%" data-live-search="true">
                                        <option value="" disabled selected=""> Select </option>
                                        <option value="Yes" <?php if(isset($order_item['item_delivered'])){ echo ($order_item['item_delivered'] == 'Yes') ? 'selected' : '';}?>>Yes</option>
                                        <option value="No" <?php if(isset($order_item['item_delivered'])){ echo ($order_item['item_delivered'] == 'No') ? 'selected' : '';}?>>No</option>
                                    </select>
                                  </td>
                                  <td><a class="btn btn-danger removeitem"><i class="fa fa-remove"></i></a></td>
                                </tr>
                            <?php } ?> 
                            <input type="hidden" id="itemcount" name="itemcount" value="<?php echo $key;?>">
                            <?php } else { ?>
                                <tr class="has-row-options">
                                  <td>
                                    <select class="selectpicker" name="item_id0" id="item_id0"data-width="100%" data-live-search="true">
                                    <option value="0" disabled selected=""> Select Item</option>
                                        <?php foreach($items as  $item){ ?>
                                        <option value="<?php echo $item['id']?>"><?php echo $item['description']; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td>
                                  <td><input type="text" name="quantity0" id="quantity0" value="" class="form-control"></td>
                                  <td><input type="text" name="vendor_name0" id="vendor_name0" value="" class="form-control"></td>
                                  <td>
                                    <div class="lead-select-date-contacted">
                                        <?php echo render_date_input('expected_delivery_date0','','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                                    </div>
                                  </td>
                                  <td>
                                    <div class="lead-select-date-contacted">
                                        <?php echo render_date_input('delivery_date0','','',array('data-date-start-date'=>date('Y-m-d'))); ?>
                                    </div>
                                  </td>
                                  <td>
                                    <select class="selectpicker" name="item_received0" id="item_received0" data-width="100%" data-live-search="true">
                                        <option value="" disabled selected=""> Select </option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select class="selectpicker" name="item_delivered0" id="item_delivered0" data-width="100%" data-live-search="true">
                                        <option value="" disabled selected=""> Select </option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                  </td>
                                </tr> 
                                <input type="hidden" id="itemcount" name="itemcount" value="0">
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                  </div>
                  
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
           <?php echo form_close(); ?>

           <?php if(isset($notes)){ ?>
            <div class="col-md-12">
			<div class="panel_s">
				<div class="panel-body">
					<div class="row _buttons">
                     	<div class="col-md-12">
							<h4 class="customer-profile-group-heading">Order Notes</h4>
							<a href="#" onclick="new_order_note(<?php echo $order_details->id; ?>); return false;" class="btn btn-info pull-left new"><?php echo _l('New Note'); ?></a>
						</div>
					</div>
                  	<hr class="hr-panel-heading hr-10" />
                  	<div class="clearfix"></div>

					<div class="row">
                     	<div class="col-md-12">
						<table class="table dt-table scroll-responsive">
                            <thead>
                                <th><?php echo _l('#'); ?></th>
                                <th><?php echo _l('Note'); ?></th>
                                <th><?php echo _l('Added On'); ?></th>
                                <th><?php echo _l('Added By'); ?></th>
                                <th><?php echo _l('Updated By'); ?></th>
                                <th><?php echo _l('Action'); ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($notes as $key=>$note) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?></td>
                                  <td><?php echo $note['order_note'] ?></td>
                                  <td><?php echo $note['created_at'] ?></td>
                                  <td><?php echo $note['firstname'].' '.$note['lastname'] ?></td>
                                  <td><?php echo $note['fname'].' '.$note['lname'] ?></td>
								  <td>
                                    <a href="#" onclick="edit_order_note(this,<?php echo $note['id']; ?>,<?php echo $order_details->id; ?>,<?php echo $note['share_to_customer']; ?>); return false" data-name="<?php echo $note['order_note']; ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
                                  </td>
                                </tr>
                                <?php }  ?>
                            </tbody>
                        </table>
						</div>
					</div>
				</div>
			</div>
			</div>
		<?php } ?>

      </div>
    <div class="btn-bottom-pusher"></div>
  </div>

  <div class="modal fade" id="order_note" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<?php echo form_open(admin_url('order_tracker/order_note')); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">
					<span class="edit-title"><?php echo _l('Order Note Edit'); ?></span>
					<span class="add-title"><?php echo _l('Order Note Add'); ?></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div id="additional"></div>
						<?php echo render_textarea('order_note','Order Note',null,array('rows'=>4),array()); ?>
					</div>
					<div class="col-md-12">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox" name="share_to_customer" id="share_to_customer">
                            <label for="share_to_customer">
                                <?php echo _l('Share to Customer'); ?>
                            </label>
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
				<button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
			</div>
		</div><!-- /.modal-content -->
		<?php echo form_close(); ?>
	</div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <?php /* $this->load->view('admin/inventory_details/inventory_detail_category'); /*/?>
  <?php init_tail(); ?>
<script>
   $(function(){
     appValidateForm($('#order_form'),
      {clientid:'required',backup_type:'required',start_date:'required',end_date:'required',
        notify_to: {
          email:true,
          required:true
        }
      });
   });

    $('document').ready(function(){
        var id = $('#client_id').val();
        if(id){
            get_contacts(this);
        }
    });

    function get_contacts(url){
        var id = $('#client_id').val();
        var str = $('#contact_ids').val();
        $.ajax({
            url:'<?=admin_url()?>order_tracker/get_client_contacts/'+id,
            method:"POST",
            data:{id:id},
            success:function(data){
                $("#contact_id").empty();
                if(JSON.parse(data).length > 0){
                    $.each(JSON.parse(data), function(index, json) {
                        // $("#contact_id").append(`<option value='${JSON.parse(data)[index].id}'
                        // ${(str.indexOf(JSON.parse(data)[index].id) != -1)?'selected' : ''}>${JSON.parse(data)[index].firstname} ${JSON.parse(data)[index].lastname} (${JSON.parse(data)[index].email})</option>`);
                        
                        if(str.indexOf(JSON.parse(data)[index].id) != -1){ selected = true; }
                        else{ selected = false; }

                        var text = `${JSON.parse(data)[index].firstname} ${JSON.parse(data)[index].lastname} (${JSON.parse(data)[index].email})`;
                        var newOption = new Option(text, `${JSON.parse(data)[index].id}`, selected, selected);

                        $('#contact_id').append(newOption).trigger('change');
                        $("#contact_id").selectpicker("refresh");
                    });
                    //$("#contact_id").css("height","90px");
                }
            }
        });
    }

   function addItem(){
       var count = $('#itemcount').val();
       var total = (parseInt(count)+1);

       var row = '<tr class="has-row-options"><td><select name="item_id'+total+'" id="item_id'+total+'"data-width="100%" data-live-search="true"><option value="" disabled selected=""> Select Item</option>';
        <?php foreach($items as  $item){ ?>
            row += '<option value="<?php echo $item['id']?>"><?php echo $item['description']; ?></option>';
        <?php } ?>
        row += '</select></td><td><input type="text" name="quantity'+total+'" id="quantity'+total+'" value="" class="form-control"></td>';
        row += '<td><input type="text" name="vendor_name'+total+'" id="vendor_name'+total+'" value="" class="form-control"></td>';
        row += '<td><div class="form-group" app-field-wrapper="expected_delivery_date'+total+'"><div class="input-group date"><input type="text" id="expected_delivery_date'+total+'" name="expected_delivery_date'+total+'" class="form-control datepicker" data-date-start-date="2021-07-20" value="" autocomplete="off"><div class="input-group-addon"><i class="fa fa-calendar calendar-icon"></i></div></div></div></td>';
        row += '<td><div class="form-group" app-field-wrapper="delivery_date'+total+'"><div class="input-group date"><input type="text" id="delivery_date'+total+'" name="delivery_date'+total+'" class="form-control datepicker" data-date-start-date="2021-07-20" value="" autocomplete="off"><div class="input-group-addon"><i class="fa fa-calendar calendar-icon"></i></div></div></div></td>';
        row += '<td><select name="item_received'+total+'" id="item_received'+total+'" data-width="100%" data-live-search="true"><option value="" disabled selected=""> Select </option><option value="Yes">Yes</option><option value="No">No</option></select></td>';
        row += '<td><select name="item_delivered'+total+'" id="item_delivered'+total+'" data-width="100%" data-live-search="true"><option value="" disabled selected=""> Select </option><option value="Yes">Yes</option><option value="No">No</option></select></td><td><a class="btn btn-danger removeitem"><i class="fa fa-remove"></i></a></td></tr>';
        
        jQuery("#itemtable tbody").append(row);
        $('#itemcount').val(total);
   }

   $('#itemtable').on('click', '.removeitem', function(event) {
        event.preventDefault();
        $(this).closest("tr").remove();
   });
</script>
<script type="text/javascript">
	function new_order_note(order_id){
		$('#additional').html('');
		$('#additional').append(hidden_input('order_id',order_id));
		$("textarea#order_note").val('');
		$('#order_note').modal('show');
		$('.edit-title').addClass('hide');
	}
	function edit_order_note(invoker,id,order_id,share_to){
		var name = $(invoker).data('name');
		$('#additional').html('');
		$('#additional').append(hidden_input('id',id));
		$('#additional').append(hidden_input('order_id',order_id));
		$("textarea#order_note").val(name);
        if(share_to==1){ 
            $('#share_to_customer').prop('checked', true);
        }
		$('#order_note').modal('show');
		$('.add-title').addClass('hide');
	}
</script>
</body>
</html>