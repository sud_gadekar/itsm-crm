<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if(has_permission('sales_order','','create')){ ?>
                  <a href="<?php echo admin_url('order_tracker/order'); ?>" class="btn btn-info">
                    <?php echo 'New Order'; ?></a>
                  <?php } ?>
            
                   <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data"
                        data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right width300">
                            
                            <?php if(count($clients) > 0 && is_admin()){ ?>
                            <div class="clearfix"></div>
                            
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1"><?php echo _l('By Customer'); ?></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($clients as $client){ ?>
                                    <li>
                                        <a href="#" onclick="filter('cust', <?php echo $client['userid'];?>)"><?php echo $client['company']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <div class="clearfix"></div>
                            <li class="divider"></li>
                            <?php } ?>
                            <li><a href="#" onclick="filter('order', 'Yes')">Completed Orders</a></li>
                            <li><a href="#" onclick="filter('order', 'No')">Pending Orders</a></li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu pull-left">
                                <a href="#" tabindex="-1">Order Status</a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <?php foreach($statuses as $status){ ?>
                                    <li>
                                        <a href="#" onclick="filter('status', <?php echo $status['statusid'];?>)"><?php echo $status['name']; ?>    
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </div>

                  <div id="stats-top" class="hide">
                     <hr />
                     <div id="order_trackers_total"></div>
                  </div>
               </div>
            <!-- </div>
            <div class="row"> -->
               <div class="panel-body">

                <div class="row">
                  <div class="col-md-12">
                    <h4 class="no-margin">Order Status Summary</h4>
                  </div>
                  <?php foreach($statuses as $status){ ?>
                  <div class="col-md-2 col-xs-6 mbot15 border-right">
                    <a href="#" data-cview="ticket_status_6" onclick="filter('status', <?php echo $status['statusid'];?>)">
                      <?php 
                        $order_cnt=$this->db->select('id')->from(db_prefix() . 'order_tracker')->where('status_id', $status['statusid'])->get()->result_array();
                      ?>
                      <h3 class="bold"><?php echo count($order_cnt) ?></h3>
                      <span style="color:<?php echo $status['statuscolor']; ?>"><?php echo $status['name']; ?></span>
                    </a>
                  </div>
                  <?php } ?>
                </div>
                <hr class="hr-panel-heading">

                <div class="order_trackers_table" id="small-table ">
                  <div class="">
                     <div class="">
                        <div class="clearfix"></div>
                        <div class="mtop20 ">
                        
                            <div class="clearfix"></div>
                           
                            <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('Sr.No.'); ?></th>
                                <th><?php echo _l('Customer Name'); ?></th>
                                <th><?php echo _l('Description'); ?></th>
                                <th><?php echo _l('LPO No'); ?></th>
                                <th><?php echo _l('SO No'); ?></th>
                                <th><?php echo _l('SO Date'); ?></th>
                                <th><?php echo _l('Amount'); ?></th>
                                <th><?php echo _l('Material Status'); ?></th>
                                <th><?php echo _l('Implementation Status'); ?></th>
                                <th><?php echo _l('Service Request'); ?></th>
                                <th><?php echo _l('Project'); ?></th>
                                <th><?php echo _l('Invoice No'); ?></th>
                                <th><?php echo _l('Invoice Date'); ?></th>
                                <th><?php echo _l('Payment Date'); ?></th>
                                <th><?php echo _l('Payment Period (Days)'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($order_tracker_lists) > 0){?>

                                <?php foreach ($order_tracker_lists as $key => $order_tracker) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                      <a href="<?php echo admin_url('order_tracker/order/').$order_tracker['id'] ?>">Edit </a>
                                      <span class="text-dark"> | </span>
                                      <a href="<?php echo admin_url('order_tracker/delete/').$order_tracker['id'] ?>" class="text-danger _delete">Delete</a>   
                                  </td>
                                  <td><?php echo $order_tracker['company']; ?></td>
                                  <td><?php echo $order_tracker['description']; ?></td>
                                  <td><?php echo $order_tracker['lpo_no']; ?></td>
                                  <td><?php echo $order_tracker['so_number'] ?></td>
                                  <td><?php echo $order_tracker['order_date']; ?></td>
                                  <td><?php echo $order_tracker['amount']; ?></td>
                                  <td><?php echo $order_tracker['name']; ?></td>
                                  <td><?php echo $order_tracker['service_request']; ?></td>
                                  <td>
                                    <a href="<?php echo admin_url('service_requests/service_request/').$order_tracker['sr_id'] ?>"><?php echo $order_tracker['subject']; ?></a>
                                  </td>
                                  <td>
                                    <a href="<?php echo admin_url('projects/project/').$order_tracker['project_id'] ?>"><?php echo $order_tracker['project']; ?></a>
                                  </td>
                                  <td><?php echo $order_tracker['invoice_no']; ?></td>
                                  <td><?php echo $order_tracker['inv_submission_date']; ?></td>
                                  <td><?php echo $order_tracker['payment_date']; ?></td>
                                  <td><?php echo $order_tracker['payment_received_in']; ?></td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                         
                        </div>
                        <?php /*}*/?>

                     </div>
                  </div>
               </div>
             </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Order Tracking Details</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<!-- Show Asset Data On Modal -->
<script type="text/javascript">
    function order_tracker_log(url,id){          
      $('#logmodal').modal('show');   
    }
</script>

<script type="text/javascript">
  function filter(val, id){
     $.ajax({
           url:'<?=admin_url()?>order_tracker/filter_by_customer',
           method:"POST",
           data:{id:id, val:val},
           success:function(data){
            if(data){
              $('.order_trackers_table').html(data);
            }
           }
      });
  }
</script>
</body>
</html>
