
                           

                           <div class="row form-inline dataTables_wrapper">
  <div class="col-md-6">
    <div class="dataTables_length" id="table-gap_details_length">
      <label>
        <select name="table-gap_details_length" aria-controls="table-gap_details" class="form-control input-sm">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
          <option value="-1">All</option>
        </select>
      </label>
    </div>
    <div class="dt-buttons btn-group">
      <button class="btn btn-default buttons-collection btn-default-dt-options" tabindex="0" aria-controls="table-gap_details" type="button" aria-haspopup="true" aria-expanded="false"><span>Export</span>
      </button>
    </div>
  </div>
  <div class="col-md-6">
    <div id="table-gap_details_filter" class="dataTables_filter">
      <label>
        <div class="input-group">
          <span class="input-group-addon">
            <span class="fa fa-search"></span>
            </span><input type="search" class="form-control input-sm" placeholder="Search..." aria-controls="table-gap_details">
          </div>
        </label>
      </div>
    </div>
    <div id="table-gap_details_processing" class="dataTables_processing panel panel-default" style="display: none;"><div class="dt-loader">
      
    </div>
  </div>
</div>
                            <table class="table dataTable dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('Sr.No.'); ?></th>
                                <th><?php echo _l('Customer Name'); ?></th>
                                <th><?php echo _l('Description'); ?></th>
                                <th><?php echo _l('LPO No'); ?></th>
                                <th><?php echo _l('SO No'); ?></th>
                                <th><?php echo _l('SO Date'); ?></th>
                                <th><?php echo _l('Amount'); ?></th>
                                <th><?php echo _l('Material Status'); ?></th>
                                <th><?php echo _l('Implementation Status'); ?></th>
                                <th><?php echo _l('Service Request'); ?></th>
                                <th><?php echo _l('Project'); ?></th>
                                <th><?php echo _l('Invoice No'); ?></th>
                                <th><?php echo _l('Invoice Date'); ?></th>
                                <th><?php echo _l('Payment Date'); ?></th>
                                <th><?php echo _l('Payment Period (Days)'); ?></th>
                              </thead>
                              <tbody>
                                <?php if(count($order_tracker_lists) > 0){?>

                                <?php foreach ($order_tracker_lists as $key => $order_tracker) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                      <a href="<?php echo admin_url('order_tracker/order/').$order_tracker['id'] ?>">Edit </a>
                                      <span class="text-dark"> | </span>
                                      <a href="<?php echo admin_url('order_tracker/delete/').$order_tracker['id'] ?>" class="text-danger _delete">Delete</a>   
                                  </td>
                                  <td><?php echo $order_tracker['company']; ?></td>
                                  <td><?php echo $order_tracker['description']; ?></td>
                                  <td><?php echo $order_tracker['lpo_no']; ?></td>
                                  <td><?php echo $order_tracker['so_number'] ?></td>
                                  <td><?php echo $order_tracker['order_date']; ?></td>
                                  <td><?php echo $order_tracker['amount']; ?></td>
                                  <td><?php echo $order_tracker['name']; ?></td>
                                  <td><?php echo $order_tracker['service_request']; ?></td>
                                  <td>
                                    <a href="<?php echo admin_url('service_requests/service_request/').$order_tracker['sr_id'] ?>"><?php echo $order_tracker['subject']; ?></a>
                                  </td>
                                  <td>
                                    <a href="<?php echo admin_url('projects/project/').$order_tracker['project_id'] ?>"><?php echo $order_tracker['project']; ?></a>
                                  </td>
                                  <td><?php echo $order_tracker['invoice_no']; ?></td>
                                  <td><?php echo $order_tracker['inv_submission_date']; ?></td>
                                  <td><?php echo $order_tracker['payment_date']; ?></td>
                                  <td><?php echo $order_tracker['payment_received_in']; ?></td>
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="12" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
                            
                
                      