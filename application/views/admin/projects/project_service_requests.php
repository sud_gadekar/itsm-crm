<?php defined('BASEPATH') or exit('No direct script access allowed');
    $this->load->view('admin/service_requests/summary',array('project_id'=>$project->id));
    echo form_hidden('project_id',$project->id);
    echo '<div class="clearfix"></div>';
    if(((get_option('access_service_requests_to_none_staff_members') == 1 && !is_staff_member()) || is_staff_member())){
        echo '<a href="'.admin_url('service_requests/add?project_id='.$project->id).'" class="mbot20 btn btn-info">'._l('new_service_request').'</a>';
    }
    echo AdminServiceRequestsTableStructure('service_requests-table');
?>
