<?php defined('BASEPATH') or exit('No direct script access allowed');
echo theme_head_view();
get_template_part($navigationEnabled ? 'navigation' : '');
?>

<style type="text/css">
  
.sidebar {
  height: 100%;
  /*width: 0;*/
  position: absolute;
  z-index: 100;
  top: 0;
  left: 0;
  background-color: #262761;
  /*overflow-x: hidden;*/
  transition: 0.5s;
  /*padding-top: 60px;*/
}

#side-menu  a {
    display: block;
    color: #fff;
    text-transform: uppercase;
    padding: 11px 20px 11px 16px;
    font-size: 13px;
    font-family: Roboto;
}

.side-menu_anchor_hover:hover{
    background: #e3e8ee !important;
    border-bottom: 0!important;
    color: #323a45 !important;
    transition: ease-in-out .2s;
}


.sidebar .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

.openbtn {
  font-size: 20px;
  cursor: pointer;
  background-color: #262761;
  color: white;
  padding: 10px 15px;
  border: none;
}

.openbtn:hover {
  background-color: #444;
}

div.content {
  margin-left: 200px;
  padding: 1px 16px;
  height: 1000px;
}



   aside a i.menu-icon {
       margin-right: 16px;
       display: block;
       float: left;
       width: 18px;
       font-size: 17px;
   }
</style>


   <!-- <div class="<?php echo (is_client_logged_in()) ? 'col-md-12 col-sm-12 col-xs-6' :'col-12' ?>" 
        style="<?php echo (is_client_logged_in()) ? 'padding-top: 10px !important' :'' ?>"> -->

        

   <div id="main" style="<?php echo ($this->uri->segment(1) == 'authentication' || $this->uri->segment(1) == 'contract' || $this->uri->segment(1) == 'forms') ? '' : 'margin-left: 250px;'?>">
      <div id="content" style="padding: 2rem;">
        
               <?php get_template_part('alerts'); ?>
            
         <?php if(isset($knowledge_base_search)){ ?>
            <?php get_template_part('knowledge_base/search'); ?>
         <?php } ?>
       
            <?php hooks()->do_action('customers_content_container_start'); ?>
          
               <?php echo theme_template_view(); ?>
           
   </div>
   <?php
   echo theme_footer_view();
   ?>

   </div>
</div>
<?php
/* Always have app_customers_footer() just before the closing </body>  */
app_customers_footer();
   /**
   * Check for any alerts stored in session
   */
   app_js_alerts();
   ?>
</body>
</html>
