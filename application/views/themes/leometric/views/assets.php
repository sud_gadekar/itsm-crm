<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css">
  html {
    scroll-behavior: smooth;
  }
</style>

<!-- Asset -->
<div class="panel_s" id="asset_list" style="display: none;">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Inventory'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <hr />
    <?php if(count($assets) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('Id'); ?></th>
        <th><?php echo _l('Contact'); ?></th>
        <th><?php echo _l('Type'); ?></th>
        <th><?php echo _l('Product'); ?></th>
        <th><?php echo _l('Contract'); ?></th>
        
      </thead>
      <tbody>
        <?php foreach($assets as $key => $asset){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td><?php echo $asset['company'] ?></td>
          <td>
            <?php echo $asset['type_name'] ?>
          </td>
          <td>
            <?php echo $asset['product_name'] ?>
          </td>
          <td>
            <?php echo $asset['subject'] ?>
          </td>
          
          <!-- <td>
            <a href="<?php echo admin_url('inventory_details/inventory_detail/').$asset['id']?>"  class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i>
            </a> -->
            <!-- <a href="<?php echo admin_url('clients/delete_asset/'.$asset['id']); ?>" class="btn btn-danger btn-icon _delete">
              <i class="fa fa-remove"></i>
            </a>
          </td> -->
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No asset found'); ?></p>
    <?php } ?>
  </div>
</div>
<!-- Diagram -->
<div class="panel_s" id="diagram_list" style="display: none;">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Diagram'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <hr />
    <?php if(count($diagrams) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Title'); ?></th>
       
        <th><?php echo _l('Descriptiion'); ?></th>
        <th><?php echo _l('Created On'); ?></th>
      </thead>
      <tbody>
        <?php foreach($diagrams as $key => $diagram){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td>
            <?php
            echo $diagram['title']?>
          </td>
          <td>
            <?php
            echo $diagram['description']?>
          </td>
          <td>
            <?php
            echo $diagram['created_at']?>
          </td>
          <!-- <td>
            <a href="<?php echo admin_url('diagrams/diagram/').$diagram['id'];?>"  class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
            <a href="<?php echo admin_url('diagrams/delete/'.$diagram['id']); ?>" class="btn btn-danger btn-icon _delete">
              <i class="fa fa-remove"></i>
            </a>
          </td> -->
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No diagram found'); ?></p>
    <?php } ?>
  </div>
</div>
<!-- GAP -->
<div class="panel_s" id="gap_list" style="display: none;">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('GAP'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <hr />
    <?php if(count($gaps) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Name'); ?></th>
        <th><?php echo _l('Descriptiion'); ?></th>
        <th><?php echo _l('Category'); ?></th>
        <th><?php echo _l('Impact'); ?></th>
        <th><?php echo _l('Status'); ?></th>
        
      </thead>
      <tbody>
        <?php foreach($gaps as $key => $gap){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td>
            <?php
            echo $gap['name']?>
          </td>
          
          <!--  <td>
            <a href="<?php echo admin_url('gaps/add/').$gap['id'];?>"  class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
            <a href="<?php echo admin_url('gaps/delete/'.$gap['id']); ?>" class="btn btn-danger btn-icon _delete">
              <i class="fa fa-remove"></i>
            </a>
          </td> -->

          <td><?php echo $gap['description']?></td>
          <td><?php echo $gap['category_title']?></td>
          <td><?php echo $gap['impact_title']?></td>
          <td><?php echo $gap['status_title']?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No gap found'); ?></p>
    <?php } ?>
  </div>
</div>
<!-- Network -->
<div class="panel_s" id="network_list" style="display: block;">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('IT Overview'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <hr />
    <?php if(count($network_servers) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Title'); ?></th>
        <th><?php echo _l('Type')?></th>
        <th><?php echo _l('Action'); ?></th> 
      </thead>
      <tbody>
        <?php foreach($network_servers as $key => $network){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td>
            <?php
            echo $network['title']?>
          </td>
          <td>
            <?php
            echo ucfirst($network['type_name']);?>
          </td>
          
          <td>
            <a href="<?php echo site_url('clients/network_server/').$network['id'];?>"  class="btn btn-default btn-icon">Open</a>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No network found'); ?></p>
    <?php } ?>
  </div>
</div>
<!-- Server -->
<div class="panel_s" id="server_list" style="display: none;">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Server'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <hr />
    <?php if(count($servers) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Name'); ?></th>
        <th><?php echo _l('Action'); ?></th> 
      </thead>
      <tbody>
        <?php foreach($servers as $key => $server){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td>
            <?php
            echo $server['title']?>
          </td>
            <td>
            <a href="<?php echo site_url('clients/network_server/').$server['id'];?>"  class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>
            <!--<a href="<?php echo admin_url('network_servers/delete/'.$network_server['id']); ?>" class="btn btn-danger btn-icon _delete">
              <i class="fa fa-remove"></i>
            </a>-->
          </td> 
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No server found'); ?></p>
    <?php } ?>
  </div>
</div>

<script type="text/javascript">
  $('#asset').click(function(e) 
  {
    $('#diagram_list, #gap_list, #network_list, #server_list').css('display','none');
    $('#asset_list').css('display','block');
  });

  $('#diagram').click(function(e) 
  {
    $('#asset_list, #gap_list, #network_list, #server_list').css('display','none');
    $('#diagram_list').css('display','block');
  });

  $('#gap').click(function(e) 
  {
    $('#asset_list, #diagram_list, #network_list, #server_list').css('display','none');
    $('#gap_list').css('display','block');
  });

  $('#network').click(function(e) 
  {
    $('#asset_list, #diagram_list, #gap_list, #server_list').css('display','none');
    $('#network_list').css('display','block');
  });

  $('#server').click(function(e) 
  {
    $('#asset_list, #diagram_list, #gap_list, #network_list').css('display','none');
    $('#server_list').css('display','block');
  });

</script>