<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Backup'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Backup Logs').' : '.$backup_policy; ?></h3>
        <div class="clearfix"></div>
        <hr />
      </div>
  </div>
  <div class="clearfix"></div>
  <!-- <hr /> -->
  <div class="clearfix"></div>
      <table class="table dt-table table-backups" data-order-col="<?php echo (get_option('services') == 1 ? 1 : 2); ?>" data-order-type="desc">
      <thead>
        <th><?php echo _l('Sr.No.'); ?></th>
        <th><?php echo _l('Date'); ?></th>
        <th><?php echo _l('Inventory Product'); ?></th>
        <th><?php echo _l('Status'); ?></th>
        <th><?php echo _l('Remark'); ?></th>
       
        <?php
        $custom_fields = get_custom_fields('backups',array('show_on_client_portal'=>1));
        foreach($custom_fields as $field){ ?>
          <th><?php echo $field['name']; ?></th>
        <?php } ?>
      </thead>
      <tbody>
        <?php foreach($logs as $key => $log){ ?>
          <tr>
            <td data-order="<?php echo $log['id']; ?>"><?php echo $key+1; ?></td>
            <td><?php echo date('d-m-Y',strtotime($log['backup_date'])); ?></td>
            <td><?php echo $log['product_name'].' ('.$log['hostname'].' | '.$log['purpose'].')'; ?></td>
            <td><?php echo $log['status']; ?></td>
            <td><?php echo $log['remark']; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
</div>
</div>