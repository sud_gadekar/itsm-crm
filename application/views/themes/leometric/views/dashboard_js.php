<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    var weekly_payments_statistics;

    $(function() {
        /*$( "[data-container]" ).sortable({
            connectWith: "[data-container]",
            helper:'clone',
            handle:'.widget-dragger',
            tolerance:'pointer',
            forcePlaceholderSize: true,
            placeholder: 'placeholder-dashboard-widgets',
            start:function(event,ui) {
                $("body,#wrapper").addClass('noscroll');
                $('body').find('[data-container]').css('min-height','20px');
            },
            stop:function(event,ui) {
                $("body,#wrapper").removeClass('noscroll');
                $('body').find('[data-container]').removeAttr('style');
            },
            update: function(event, ui) {
                if (this === ui.item.parent()[0]) {
                    var data = {};
                    $.each($("[data-container]"),function(){
                        var cId = $(this).attr('data-container');
                        data[cId] = $(this).sortable('toArray');
                        if(data[cId].length == 0) {
                            data[cId] = 'empty';
                        }
                    });
                    $.post(admin_url+'staff/save_dashboard_widgets_order', data, "json");
                }
            }
        });*/

        // Read more for dashboard todo items
        /*$('.read-more').readmore({
            collapsedHeight:150,
            moreLink: "<a href=\"#\"><?php echo _l('read_more'); ?></a>",
            lessLink: "<a href=\"#\"><?php echo _l('show_less'); ?></a>",
        });*/

        $('body').on('click','#viewWidgetableArea',function(e){
            e.preventDefault();

            if(!$(this).hasClass('preview')) {
                $(this).html("<?php echo _l('hide_widgetable_area'); ?>");
                $('[data-container]').append('<div class="placeholder-dashboard-widgets pl-preview"></div>');
            } else {
                $(this).html("<?php echo _l('view_widgetable_area'); ?>");
                $('[data-container]').find('.pl-preview').remove();
            }

            $('[data-container]').toggleClass('preview-widgets');
            $(this).toggleClass('preview');
        });

        var $widgets = $('.widget');
        var widgetsOptionsHTML = '';
        widgetsOptionsHTML += '<div id="dashboard-options">';
        widgetsOptionsHTML += "<h4><i class='fa fa-question-circle' data-toggle='tooltip' data-placement=\"bottom\" data-title=\"<?php echo _l('widgets_visibility_help_text'); ?>\"></i> <?php echo _l('widgets'); ?></h4><a href=\"<?php echo admin_url('staff/reset_dashboard'); ?>\"><?php echo _l('reset_dashboard'); ?></a>";

        widgetsOptionsHTML += ' | <a href=\"#\" id="viewWidgetableArea"><?php echo _l('view_widgetable_area'); ?></a>';
        widgetsOptionsHTML += '<hr class=\"hr-10\">';

        $.each($widgets,function(){
            var widget = $(this);
            var widgetOptionsHTML = '';
            if(widget.data('name') && widget.html().trim().length > 0) {
                widgetOptionsHTML += '<div class="checkbox checkbox-inline">';
                var wID = widget.attr('id');
                wID = wID.split('widget-');
                wID = wID[wID.length-1];
                var checked= ' ';
                var db_result = $.grep(user_dashboard_visibility, function(e){ return e.id == wID; });
                if(db_result.length >= 0) {
                    // no options saved or really visible
                    if(typeof(db_result[0]) == 'undefined' || db_result[0]['visible'] == 1) {
                        checked = ' checked ';
                    }
                }
                widgetOptionsHTML += '<input type="checkbox" class="widget-visibility" value="'+wID+'"'+checked+'id="widget_option_'+wID+'" name="dashboard_widgets['+wID+']">';
                widgetOptionsHTML += '<label for="widget_option_'+wID+'">'+widget.data('name')+'</label>';
                widgetOptionsHTML += '</div>';
            }
            widgetsOptionsHTML += widgetOptionsHTML;
        });

        $('.screen-options-area').append(widgetsOptionsHTML);
        $('body').find('#dashboard-options input.widget-visibility').on('change',function(){
          if($(this).prop('checked') == false) {
            $('#widget-'+$(this).val()).addClass('hide');
        } else {
            $('#widget-'+$(this).val()).removeClass('hide');
        }

        var data = {};
        var options = $('#dashboard-options input[type="checkbox"]').map(function() {
            return { id: this.value, visible: this.checked ? 1 : 0 };
        }).get();

        data.widgets = options;
/*
        if (typeof(csrfData) !== 'undefined') {
            data[csrfData['token_name']] = csrfData['hash'];
        }
*/
        $.post(admin_url+'staff/save_dashboard_widgets_visibility',data).fail(function(data) {
            // Demo usage, prevent multiple alerts
            if($('body').find('.float-alert').length == 0) {
                alert_float('danger', data.responseText);
            }
        });
    });

        var tickets_chart_departments = $('#tickets-awaiting-reply-by-department');



        var tickets_chart_status = $('#tickets-by-status');

        var service_requests_chart_status = $('#service_requests-by-status');

        var projects_chart_status = $('#projects-by-status');

        var gaps_chart_status = $('#gaps-by-status');

        var inventorys_chart_status = $('#inventorys-by-status');

        var inventorysubtypes_chart_status = $('#inventorysubtypes-by-status');



        var leads_chart = $('#leads_status_stats');
        var projects_chart = $('#projects_status_stats');

       
        if (tickets_chart_status.length > 0) {
            // Tickets awaiting reply by department chart
            new Chart(tickets_chart_status, {
                type: 'pie',
                data: <?php echo $tickets_reply_by_status; ?>,
                options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            if (i == 3)
                            { // Darker text color for lighter background
                                ctx.fillStyle = '#444';
                            }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
        });
        }

        if (service_requests_chart_status.length > 0) {
            // service_requests awaiting reply by department chart
            new Chart(service_requests_chart_status, {
                    type: 'pie',
                    data: <?php echo $service_requests_reply_by_status; ?>,
                    options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            if (i == 3)
                            { // Darker text color for lighter background
                                ctx.fillStyle = '#444';
                            }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
            });
        }

        if (projects_chart_status.length > 0) {
            // service_requests awaiting reply by department chart
            new Chart(projects_chart_status, {
                    type: 'pie',
                    data: <?php echo $projects_reply_by_status; ?>,
                    options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            if (i == 3)
                            { // Darker text color for lighter background
                                ctx.fillStyle = '#444';
                            }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
            });
        }

        if (gaps_chart_status.length > 0) {
            // service_requests awaiting reply by department chart
            new Chart(gaps_chart_status, {
                    type: 'pie',
                    data: <?php echo $gaps_reply_by_status; ?>,
                    options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            if (i == 3)
                            { // Darker text color for lighter background
                                ctx.fillStyle = '#444';
                            }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
            });
        }

        if (inventorys_chart_status.length > 0) {
            // service_requests awaiting reply by department chart
            Chart.defaults.global.legend.display = false;
            new Chart(inventorys_chart_status, {
                    type: 'bar',
                    data: <?php echo $inventorys_reply_by_status; ?>,
                    options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            // if (i == 3)
                            // { // Darker text color for lighter background
                            //     ctx.fillStyle = '#444';
                            // }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
            });
        }

        if (inventorysubtypes_chart_status.length > 0) {
            // service_requests awaiting reply by department chart
            new Chart(inventorysubtypes_chart_status, {
                    type: 'bar',
                    data: <?php echo $inventorysubtypes_reply_by_status; ?>,
                    options:{
                    animation:{
                        onComplete: function () {
                          var ctx = this.chart.ctx;
                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                          ctx.textAlign = 'center';
                          ctx.textBaseline = 'bottom';
                          this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle)/2;

                              var x = mid_radius * Math.cos(mid_angle);
                              var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            // if (i == 3)
                            // { // Darker text color for lighter background
                            //     ctx.fillStyle = '#444';
                            // }
                             
                              ctx.fillText(dataset.data[i], model.x + x, model.y + y);  
                            }
                          });               
                        }
                    }
                }
            });
        } 

        
        /*if(projects_chart.length > 0){
            // Projects statuses
            new Chart(projects_chart, {
                type: 'pie',
                data: <?php echo $projects_status_stats; ?>,
                options: {
                    maintainAspectRatio:false,
                    onClick:function(evt){
                       onChartClickRedirect(evt,this);
                   }
               }
           });
        }*/ 

        if($(window).width() < 500) {
            // Fix for small devices weekly payment statistics
            $('#weekly-payment-statistics').attr('height', '250');
        }

        /* fix_user_data_widget_tabs();
        $(window).on('resize', function(){
            $('.horizontal-scrollable-tabs ul.nav-tabs-horizontal').removeAttr('style');
            fix_user_data_widget_tabs();
        });*/
        
        // Payments statistics
        /*init_weekly_payment_statistics( <?php echo $weekly_payment_stats; ?> );*/
        $('select[name="currency"]').on('change', function() {
           // init_weekly_payment_statistics();
        });
    });
   /* function fix_user_data_widget_tabs(){
        if((app.browser != 'firefox'
                && isRTL == 'false' && is_mobile()) || (app.browser == 'firefox'
                && isRTL == 'false' && is_mobile())){
                $('.horizontal-scrollable-tabs ul.nav-tabs-horizontal').css('margin-bottom','26px');
        }
    }
    function init_weekly_payment_statistics(data) {
        if ($('#weekly-payment-statistics').length > 0) {

            if (typeof(weekly_payments_statistics) !== 'undefined') {
                weekly_payments_statistics.destroy();
            }
            if (typeof(data) == 'undefined') {
                var currency = $('select[name="currency"]').val();
                $.get(admin_url + 'home/weekly_payments_statistics/' + currency, function(response) {
                    weekly_payments_statistics = new Chart($('#weekly-payment-statistics'), {
                        type: 'bar',
                        data: response,
                        options: {
                            responsive:true,
                            scales: {
                                yAxes: [{
                                  ticks: {
                                    beginAtZero: true,
                                }
                            }]
                        },
                    },
                });
                }, 'json');
            } else {
                weekly_payments_statistics = new Chart($('#weekly-payment-statistics'), {
                    type: 'bar',
                    data: data,
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                              ticks: {
                                beginAtZero: true,
                            }
                        }]
                    },
                },
            });
            }

        }
    }*/
</script>
