<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Health Checklist'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Health Checklist'); ?></h3>
        <!-- <a href="<?php echo site_url('clients/open_ticket'); ?>" class="btn btn-info new-ticket pull-right">
          <?php echo _l('clients_ticket_open_subject'); ?>
        </a> -->
        <div class="clearfix"></div>
        <hr />
      </div>
      <?php /*foreach($statuses as $status){
          $_where = '';  
          $_where = 'status_id='.$status['id'].' AND '.'clientid='.$session; 
          ?>
          <!-- data-cview="ticket_status_<?php echo $status['ticketstatusid']; ?>" 
               onclick="dt_custom_view('ticket_status_<?php echo $status['ticketstatusid']; ?>','.tickets-table','ticket_status_<?php echo $status['ticketstatusid']; ?>',true); return false;"
                -->
          <div class="col-md-2 col-xs-6 mbot15 border-right">
            <a href="#">
              <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
              <span style="">
                <?php echo $status['status_title']; ?>
              </span>
            </a>
          </div>
      <?php }*/ ?>

  </div>
  <div class="clearfix"></div>
  <!-- <hr /> -->
  <div class="clearfix"></div>
      <table class="table dt-table table-checklists" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
      <thead>
        <th><?php echo _l('Sr.No.'); ?></th>
        <th><?php echo _l('View Logs'); ?></th>
        <th><?php echo _l('Customer'); ?></th>
        <th><?php echo _l('Health Checklist Title'); ?></th>
        <th><?php echo _l('Frequency'); ?></th>
       
        <?php
        $custom_fields = get_custom_fields('checklists',array('show_on_client_portal'=>1));
        foreach($custom_fields as $field){ ?>
          <th><?php echo $field['name']; ?></th>
        <?php } ?>
      </thead>
      <tbody>
        <?php foreach($checklists as $key => $checklist){ ?>
        <tr>  
          <td data-order="<?php echo $checklist['id']; ?>">
            <a href="#">
              <?php echo $key+1; ?>
            </a>
          </td>
          <td>
            <!--onclick="checklist_log(this,<?php echo $checklist['id']; ?>); return false"-->
            <a href="<?php echo site_url('clients/checklist_logs/').$checklist['id'] ?>" data-name="<?php echo $checklist['id']; ?>" class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>
          </td>
          <td><?php echo $checklist['company']; ?></td>
          <td><?php echo $checklist['checklist_title'] ?></td>
          <td>
            <?php 
              if($checklist['frequency']==1){ echo 'Daily'; }
              else if($checklist['frequency']==2){ echo 'Weekly'; }
              else if($checklist['frequency']==3){ echo 'Monthly'; }
            ?>
          </td>
             <?php foreach($custom_fields as $field){ ?>
              <td>
                <?php echo get_custom_field_value($checklist['checklistid'],$field['id'],'checklists'); ?>
              </td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
</div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>checklist Logs</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script type="text/javascript">
    function checklist_log(url,id){
      $.ajax({
           url:'<?=admin_url()?>checklists/checklist_log_data/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              //alert(data)
              $('.logDetailsData').html(data);
            }
           }
      });
                              
      $('#logmodal').modal('show');   
    }
  </script>