<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop renewals-summary-heading"><?php echo _l('Team Available'); ?></h3>
        <div class="clearfix"></div>
        <hr />
      </div>
  </div>

  <div class="clearfix"></div>
        <table class="table dt-table scroll-responsive">
            <thead>
            <th><?php echo _l('Role'); ?></th>
            <th><?php echo _l('Name'); ?></th>
            <th><?php echo _l('Email'); ?></th>
            <th><?php echo _l('Availability '); ?></th>
            
            </thead>
            <tbody>
            <?php if(count($team_availiables) > 0){?>

            <?php foreach ($team_availiables as $key => $team_availiable) { ?>
            <tr class="has-row-options">
                <td><?php echo $team_availiable['role_name']; ?></td>
                <td><?php echo get_staff_full_name($team_availiable['staff_id']); ?></td>
                <td><?php echo get_staff_email_by_id($team_availiable['staff_id']); ?></td>
                <td>
                <?php 
                      if($team_availiable['status_id'] == 1) {
                        echo "On Leave".' ('.$team_availiable['from_date'].' - '.$team_availiable['to_date'].' )';
                      } else if($team_availiable['status_id'] == 2) {
                        echo "On Project".' ('.$team_availiable['from_date'].' - '.$team_availiable['to_date'].' )';
                      } else if($team_availiable['status_id'] == 3) {
                        echo "On Support Request".' ('.$team_availiable['from_date'].' - '.$team_availiable['to_date'].' )';
                      } else if($team_availiable['status_id'] == 4) {
                        echo "On Service Request".' ('.$team_availiable['from_date'].' - '.$team_availiable['to_date'].' )';
                      } else if($team_availiable['status_id'] == 5) {
                        echo "Availiable";
                      } else {
                        echo "-";
                      }
                ?>
                
                
                
                
                </td>
            </tr>
            <?php }  ?>
            <?php } else{ ?>
            <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
            <?php }?>
            </tbody>
        </table>
        </div>
    </div>
</div>
