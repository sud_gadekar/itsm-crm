<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Tickets Report'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Tickets Report'); ?></h3>
        <!-- <a href="<?php echo site_url('clients/open_ticket'); ?>" class="btn btn-info new-ticket pull-right">
          <?php echo _l('clients_ticket_open_subject'); ?>
        </a> -->
        <button type="button" class="pull-right btn btn-success ticket-report-charts"><i class="fa fa-bar-chart menu-icon"></i> Show Charts</button>
        <div class="clearfix"></div>
        <hr />
      </div>
      <div class="col-md-12 list-status ticket-status">
      <div class="bg-light-gray border-radius-4">
        <form id="form_ticket_report" action="<?php echo site_url('clients/ticket_reports')?>" enctype="multipart/form-data" method="get" accept-charset="utf-8">
          <div class="p8">
            <div id="customers-list" class="form-group col-sm-4">
              <label for="report_ticket_type">Type</label>
              <select name="report_ticket_type" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                  <option value="all" selected=""><?php echo 'All Tickets' ?></option>
                  <option value="support" <?php echo (isset($_GET['report_ticket_type']) && $_GET['report_ticket_type']=='support')?'selected':'' ?>>Support Request</option>
                  <option value="service" <?php echo (isset($_GET['report_ticket_type']) && $_GET['report_ticket_type']=='service')?'selected':'' ?>>Service Request</option>
              </select>
            </div>
            <div id="category-list" class="form-group col-sm-4">
              <label for="service_category"><?php echo _l('Service Category'); ?></label>
                <select name="service_category" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                  <option value="" selected=""><?php echo 'All' ?></option>
                  <?php foreach($services as $service){ ?>
                  <option value="<?php echo $service['serviceid']; ?>" <?php echo (isset($_GET['service_category']) && $_GET['service_category']== $service['serviceid'])?'selected':'' ?>><?php echo $service['name'] ?></option>
                  <?php } ?>
                </select>
            </div>
            <div id="staff-list" class="form-group col-sm-4">
              <label for="ticket_staffid">Staff Name (Assigned To)</label>
              <!-- <select name="ticket_staffid" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                <option value="" selected=""><?php echo 'All' ?></option>
                <?php foreach($staffs as $staff){  ?>
                <option value="<?php echo $staff['staffid']; ?>" <?php echo (isset($_GET['ticket_staffid']) && $_GET['ticket_staffid']== $staff['staffid'])?'selected':'' ?>><?php echo $staff['firstname'].' '.$staff['lastname'] ?></option>
                <?php } ?>
              </select> -->
              <input type="text" name="ticket_staffid" placeholder="Enter staff name" class="form-control" value="<?php echo (isset($_GET['ticket_staffid']))?$_GET['ticket_staffid']:'' ?>">
            </div>
            <div id="request-for-list" class="form-group col-sm-4">
              <label for="request_for"><?php echo _l('Request For'); ?></label>
                <select name="request_for" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                  <option value="" selected=""><?php echo 'All' ?></option>
                  <?php foreach($request_for as $req_for){ ?>
                  <option value="<?php echo $req_for['requestid']; ?>" <?php echo (isset($_GET['request_for']) && $_GET['request_for']== $req_for['requestid'])?'selected':'' ?>><?php echo $req_for['name'] ?></option>
                  <?php } ?>
                </select>
            </div>
            <div id="status-list" class="form-group col-sm-4">
              <label for="status"><?php echo _l('Status'); ?></label>
                <select name="status" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                  <option value="" selected=""><?php echo 'All' ?></option>
                  <?php foreach($statuses as $status){ ?>
                  <option value="<?php echo $status['ticketstatusid']; ?>" <?php echo (isset($_GET['status']) && $_GET['status']== $status['ticketstatusid'])?'selected':'' ?>><?php echo $status['name'] ?></option>
                  <?php } ?>
                </select>
            </div>
            <div id="contact-list" class="form-group col-sm-4">
              <label for="contact"><?php echo _l('Contacts'); ?></label>
                <select name="contact" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('All'); ?>">
                  <option value="" selected=""><?php echo 'All' ?></option>
                  <?php foreach($contacts as $contact){ ?>
                  <option value="<?php echo $contact['id']; ?>" <?php echo (isset($_GET['contact']) && $_GET['contact']== $contact['id'])?'selected':'' ?>><?php echo $contact['firstname'].' '.$contact['lastname'] ?></option>
                  <?php } ?>
                </select>
            </div>
            <div class="form-group col-sm-4" id="report-time">
              <label for="months-report"><?php echo _l('period_datepicker'); ?></label><br />
              <select class="selectpicker" name="months-report" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                <option value=""><?php echo _l('report_sales_months_all_time'); ?></option>
                <option value="this_month" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='this_month')?'selected':'' ?>><?php echo _l('this_month'); ?></option>
                <option value="1" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='1')?'selected':'' ?>><?php echo _l('last_month'); ?></option>
                <option value="this_year" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='this_year')?'selected':'' ?>><?php echo _l('this_year'); ?></option>
                <option value="last_year" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='last_year')?'selected':'' ?>><?php echo _l('last_year'); ?></option>
                <option value="3" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='3')?'selected':'' ?> data-subtext="<?php echo _d(date('Y-m-01', strtotime("-2 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_three_months'); ?></option>
                <option value="6" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='6')?'selected':'' ?> data-subtext="<?php echo _d(date('Y-m-01', strtotime("-5 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_six_months'); ?></option>
                <option value="12" <?php echo (isset($_GET['months-report']) && $_GET['months-report']=='12')?'selected':'' ?> data-subtext="<?php echo _d(date('Y-m-01', strtotime("-11 MONTH"))); ?> - <?php echo _d(date('Y-m-t')); ?>"><?php echo _l('report_sales_months_twelve_months'); ?></option>
                <!-- <option value="custom"><?php echo _l('period_datepicker'); ?></option> -->
              </select>
            </div>
            <div id="date-range" class="hide mbot15 col-sm-4">
              <div class="row">
                <div class="col-md-6">
                  <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                  <div class="input-group date">
                    <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                  <div class="input-group date">
                    <input type="text" class="form-control datepicker" disabled="disabled" id="report-to" name="report-to">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group col-sm-4" id="report-time">
              <label for="months-report">.</label><br />
              <a type="button" href="<?php echo site_url('clients/ticket_reports');?>" class="btn btn-default" style="width: 30%; padding: 7px;">Reset</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <hr />
  <div class="clearfix"></div>
  <?php get_template_part('ticket_reports_table'); ?>
</div>

  <div class="row ticket_report_charts">
    <div class="col-md-6">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Tickets by type'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="tickets_type_count_pie-chart"></canvas>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Tickets by service categories'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="ticket_category_report_status_pie-chart"></canvas>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Tickets by staff'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="all_tickets_report_staff_pie-chart"></canvas>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Tickets by service request types'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="service_request_report_requestfor_pie-chart"></canvas>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>


</div>
</div>

<script>
  $(function(){

    $('select[name="months-report"], select[name="report_ticket_type"], select[name="service_category"], input[name="ticket_staffid"], select[name="request_for"], select[name="status"], select[name="contact"]').on('change', function() {
        $('#form_ticket_report').submit();
        //tickets_report();
    });
  });

  function tickets_report() {

    $('.table-all_tickets-report').DataTable().destroy();

    var serverParams = {
      "report_months": $('select[name="months-report"]').val(),
      "report_from": $('input[name="report-from"]').val(),
      "report_to": $('input[name="report-to"]').val(),
      "report_ticket_type": $('select[name="report_ticket_type"]').val(),
      "service_category": $('select[name="service_category"]').val(),
      "ticket_staffid": $('input[name="ticket_staffid"]').val(),
      "request_for": $('select[name="request_for"]').val(),
      "status": $('select[name="status"]').val(),
      "contact": $('select[name="contact"]').val(),
    }

    $.ajax({
        url:site_url + 'clients/all_tickets_report',
        method:"POST",
        data:serverParams,
        success:function(response_data){
          console.log(response_data);
        }
    });
  }

  $(".ticket-report-charts").click(function() {
    $('html,body').animate({
      scrollTop: $(".ticket_report_charts").offset().top},
      'slow');
  });

  $(document).ready(function(){
    $('title').html("Tickets Report");
    $('.table-all_tickets-report').DataTable( {
        searching: true,
        "bLengthChange": true,
        "bInfo": true,
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
        ]
    } );

    $.ajax({  
        url:site_url + 'clients/get_tickets_type_count',
        method:"POST",
        //data:{piechart:true},
        success:function(response_data){
          if(response_data){
            console.log(response_data);
            var keys = Object.keys(JSON.parse(response_data));
            var values = Object.values(JSON.parse(response_data));

            var ctx1 = document.getElementById("tickets_type_count_pie-chart").getContext('2d');
            Chart.defaults.global.legend.display = false;
            if(window.myChart1  !== undefined && window.myChart1 !== null)
              {
                    window.myChart1.destroy();
              }
            window.myChart1 = new Chart(ctx1, {
                type: 'bar',
                responsive: true,
                maintainAspectRatio: true,
                data: {
                  labels: keys,
                  datasets: [{
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                    data: values
                  }]
                },
                options: {
                      animation: {
                        onComplete: function() {
                          console.log(window.myChart1.toBase64Image());
                        }
                      }
                    }
            });
          }
        }
    });

    $.ajax({
        url:site_url + 'clients/get_all_tickets_staff_count',
        method:"POST",
        //data:{piechart:true},
        success:function(staff_response_data){
            if(staff_response_data)
            {
              console.log(staff_response_data);
              var staff_keys = Object.keys(JSON.parse(staff_response_data));
              var staff_values = Object.values(JSON.parse(staff_response_data));

              var staff_ctx = document.getElementById("all_tickets_report_staff_pie-chart").getContext('2d');
              Chart.defaults.global.legend.display = false;
              if(window.staff_myChart  !== undefined && window.staff_myChart !== null)
              {
                  window.staff_myChart.destroy();
              }
              window.staff_myChart = new Chart(staff_ctx, {
                  type: 'bar',
                  responsive: true,
                  maintainAspectRatio: false,
                  data: {
                    labels: staff_keys,
                    datasets: [{
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#ff2d42", "#7962f3","#c8d940"],
                      data: staff_values
                    }]
                  },

                  options: {
                    animation: {
                      onComplete: function() {
                        console.log(window.staff_myChart.toBase64Image());
                      }
                    }
                  }
              });
            }
        }
    });

    $.ajax({  
       url:site_url + 'clients/get_service_request_requestfor_count',
       method:"POST",
       success:function(requestfor_response_data){
        if(requestfor_response_data)
        {
          console.log(requestfor_response_data);
          var requestfor_keys = Object.keys(JSON.parse(requestfor_response_data));
          var requestfor_values = Object.values(JSON.parse(requestfor_response_data));

          var ctx2 = document.getElementById("service_request_report_requestfor_pie-chart").getContext('2d');
          Chart.defaults.global.legend.display = false;
          if(window.myChart2  !== undefined && window.myChart2 !== null)
            {
                  window.myChart2.destroy();
            }
          window.myChart2 = new Chart(ctx2, {
              type: 'bar',
              responsive: true,
              maintainAspectRatio: true,
              data: {
                labels: requestfor_keys,
                datasets: [{
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#ff2d42", "#7962f3","#c8d940"],
                  data: requestfor_values
                }]
              },
              options: {
                animation: {
                  onComplete: function() {
                    console.log(window.myChart2.toBase64Image());
                  }
                }
              }
          });
        }
       }
    });

    $.ajax({  
       url:site_url + 'clients/get_tickets_service_count',
       method:"POST",
       //data:{piechart:true},
       success:function(category_response_data){
        if(category_response_data)
        {
          console.log(category_response_data);
          var category_keys = Object.keys(JSON.parse(category_response_data));
          var category_values = Object.values(JSON.parse(category_response_data));

          var category_ctx = document.getElementById("ticket_category_report_status_pie-chart").getContext('2d');
          Chart.defaults.global.legend.display = false;
          if(window.category_myChart  !== undefined && window.category_myChart !== null)
            {
                  window.category_myChart.destroy();
            }
          window.category_myChart = new Chart(category_ctx, {
              type: 'bar',
              responsive: true,
              maintainAspectRatio: true,
              data: {
                labels: category_keys,
                datasets: [{
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#ff2d42", "#7962f3","#c8d940"],
                  data: category_values
                }]
              },
              options: {
                    animation: {
                      onComplete: function() {
                        console.log(window.category_myChart.toBase64Image());
                      }
                    }
                  }
          });
        }
       }
    });

  });
</script>