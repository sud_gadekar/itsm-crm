<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-notifications">
    <div class="panel-body">
        <h4 class="no-margin section-text"><?php echo _l('notifications'); ?></h4>
    </div>
</div>
<div class="panel_s">
    <div class="panel-body">
        <?php if(count($notifications) > 0){ ?>
            <table class="table dt-table table-notifications" data-order-col="1" data-order-type="desc">
                <thead>
                    <tr>
                        <th class="th-notification-name"><?php echo _l('Description'); ?></th>
                        <th class="th-notification-date"><?php echo _l('Date'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($notifications as $notification){ ?>
                    <tr>
                        <td><a href="<?php echo site_url($notification['link']); ?>"><?php echo $notification['description']; ?></a></td>
                        <td data-order="<?php echo $notification['date']; ?>"><?php echo _d($notification['date']); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <p class="no-margin"><?php echo _l('no_notifications'); ?></p>
        <?php } ?>
    </div>
</div>
