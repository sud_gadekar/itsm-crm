<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open_multipart('clients/open_service_request',array('id'=>'open-new-service_request-form')); ?>
<div class="row">
   <div class="col-md-12">

      <?php hooks()->do_action('before_client_open_service_request_form_start'); ?>

      <div class="panel_s">
         <div class="panel-heading text-uppercase open-service_request-subject">
            <?php echo _l('clients_service_request_open_subject'); ?>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group open-service_request-subject-group">
                     <label for="subject"><?php echo _l('customer_service_request_subject'); ?> *</label>
                     <input type="text" class="form-control" name="subject" id="subject" value="<?php echo set_value('subject'); ?>">
                     <?php echo form_error('subject'); ?>
                  </div>
                  </div>
                  <div class="col-md-6">
                  <?php
                  if(get_option('services') == 1 && count($services) > 0){ ?>
                  <div class="form-group open-service_request-service-group">
                     <label for="service"><?php echo _l('clients_service_request_open_service'); ?></label>
                     <select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" name="service" id="service" class="form-control selectpicker">
                        <option value=""></option>
                        <?php foreach($services as $service){ ?>
                        <option value="<?php echo $service['serviceid']; ?>" <?php echo set_select('service',$service['serviceid'],(count($services) == 1 ? true : false)); ?>><?php echo $service['name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
                  <?php } ?>
                  <div class="custom-fields">
                     <?php echo render_custom_fields('service_requests','',array('show_on_client_portal'=>1)); ?>
                  </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group open-service_request-subject-group">
                    <label for="request_for"><?php echo _l('Request For'); ?></label>
                     <select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" name="request_for" id="request_for" class="form-control selectpicker">
                        <option value=""></option>
                        <?php foreach($requests as $request){ ?>
                        <option value="<?php echo $request['requestid']; ?>"><?php echo $request['name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12">
      <div class="panel_s">
         <div class="panel-body">
            <div class="form-group open-service_request-message-group">
               <label for=""><?php echo _l('Service request description'); ?> *</label>
               <textarea name="message" id="message" class="form-control" rows="15"><?php echo set_value('message'); ?></textarea>
               <?php echo form_error('message'); ?>
            </div>
         </div>
         <div class="panel-footer attachments_area open-service_request-attachments-area">
            <div class="row attachments">
               <div class="attachment">
                  <div class="col-md-6 col-md-offset-3">
                     <div class="form-group">
                        <label for="attachment" class="control-label"><?php echo _l('clients_service_request_attachments'); ?></label>
                        <div class="input-group">
                           <input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('service_request_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_service_request_form_accepted_mimes(); ?>">
                           <span class="input-group-btn">
                           <button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_service_request_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12 text-center mtop20">
      <button type="submit" class="btn btn-info" data-form="#open-new-service_request-form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('submit'); ?></button>
   </div>
   <div class="col-md-12 text-center mtop20"></div>
   <div class="col-md-12 text-center mtop20"></div>
</div>
<?php echo form_close(); ?>
