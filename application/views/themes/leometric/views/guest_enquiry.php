<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="">
  <div class="col-md-4 col-md-offset-4 text-center forgot-password-heading">
    <h1 class="text-uppercase mbot20"><?php echo _l('Sign Up Form'); ?></h1>
  </div>
  <div class="col-md-4 col-md-offset-4">
    <div class="panel_s">
      <div class="panel-body">
        <?php echo form_open($this->uri->uri_string(),['id'=>'guest-login-form']); ?>
        <?php //echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
        <?php if($this->session->flashdata('message-danger')){ ?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('message-danger'); ?>
        </div>
        <?php } ?>
        <div class="form-group select-placeholder">
          <label for="subject" class="control-label"><?php echo _l('Subject'); ?></label>
          <select name="subject" id="subject" class="form-control selectpicker" data-live-search="false">
            <option value="Create My Account">Create My Account</option>
            <option value="Need Urgent Support">Need Urgent Support</option>
            <option value="Ask For a Quote">Ask For a Quote</option>
          </select>
        </div>
        <div class="form-group">
          <label for="name"><?php echo _l('Full Name'); ?></label>
          <input type="text" autofocus="true" class="form-control" name="name" id="name">
          <?php echo form_error('name'); ?>
        </div>
        <div class="form-group">
          <label for="company"><?php echo _l('Company Name'); ?></label>
          <input type="text" autofocus="true" class="form-control" name="company" id="company">
          <?php echo form_error('company'); ?>
        </div>
        <div class="form-group">
          <label for="email"><?php echo _l('Email'); ?></label>
          <input type="text" autofocus="true" class="form-control" name="email" id="email">
          <?php echo form_error('email'); ?>
        </div>
        <div class="form-group">
          <label for="contact"><?php echo _l('Contact No.'); ?></label>
          <input type="text" autofocus="true" class="form-control" name="contact" id="contact">
          <?php echo form_error('contact'); ?>
        </div>
        <!--<div class="form-group">
          <label for="comment"><?php echo _l('Comments'); ?></label>
          <textarea type="text" autofocus="true" class="form-control" name="comment" id="comment"></textarea>
        </div>-->
        <div class="form-group">
          <label for="country"><?php echo _l('Country'); ?></label>
          <input type="text" autofocus="true" class="form-control" name="country" id="country">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info btn-block"><?php echo _l('customer_forgot_password_submit'); ?></button>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
