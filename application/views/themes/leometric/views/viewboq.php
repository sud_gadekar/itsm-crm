<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div id="boq-wrapper">
   <?php
      ob_start();
      $items = get_items_table_data($boq, 'boq')
      ->add_table_class('no-margin')
      ->set_headings('estimate');

      echo $items->table();
      ?>
   <div class="row mtop15">
      <div class="col-md-6 col-md-offset-6">
         <table class="table text-right">
            <tbody>
               <tr id="subtotal">
                  <td><span class="bold"><?php echo _l('estimate_subtotal'); ?></span>
                  </td>
                  <td class="subtotal">
                     <?php echo app_format_money($boq->subtotal, $boq->currency_name); ?>
                  </td>
               </tr>
               <?php if(is_sale_discount_applied($boq)){ ?>
               <tr>
                  <td>
                     <span class="bold"><?php echo _l('estimate_discount'); ?>
                     <?php if(is_sale_discount($boq,'percent')){ ?>
                     (<?php echo app_format_number($boq->discount_percent,true); ?>%)
                     <?php } ?></span>
                  </td>
                  <td class="discount">
                     <?php echo '-' . app_format_money($boq->discount_total, $boq->currency_name); ?>
                  </td>
               </tr>
               <?php } ?>
               <?php
                  foreach($items->taxes() as $tax){
                   echo '<tr class="tax-area"><td class="bold">'.$tax['taxname'].' ('.app_format_number($tax['taxrate']).'%)</td><td>'.app_format_money($tax['total_tax'], $boq->currency_name).'</td></tr>';
                  }
                  ?>
               <?php if((int)$boq->adjustment != 0){ ?>
               <tr>
                  <td>
                     <span class="bold"><?php echo _l('estimate_adjustment'); ?></span>
                  </td>
                  <td class="adjustment">
                     <?php echo app_format_money($boq->adjustment, $boq->currency_name); ?>
                  </td>
               </tr>
               <?php } ?>
               <tr>
                  <td><span class="bold"><?php echo _l('estimate_total'); ?></span>
                  </td>
                  <td class="total">
                     <?php echo app_format_money($boq->total, $boq->currency_name); ?>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
   <?php
      if(get_option('total_to_words_enabled') == 1){ ?>
   <div class="col-md-12 text-center boq-html-total-to-words">
      <p class="bold"><?php echo  _l('num_word').': '.$this->numberword->convert($boq->total,$boq->currency_name); ?></p>
   </div>
   <?php }
      $items = ob_get_contents();
      ob_end_clean();
      $boq->content = str_replace('{boq_items}',$items,$boq->content);
      ?>
   <div class="mtop15 preview-top-wrapper">
      <div class="row">
         <div class="col-md-3">
            <div class="mbot30">
               <div class="boq-html-logo">
                  <?php echo get_dark_company_logo(); ?>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="top" data-sticky data-sticky-class="preview-sticky-header">
         <div class="container preview-sticky-container">
            <div class="row">
               <div class="col-md-12">
                  <div class="pull-left">
                     <h4 class="bold no-mtop boq-html-number"># <?php echo format_boq_number($boq->id); ?><br />
                        <small class="boq-html-subject"><?php echo $boq->subject; ?></small>
                     </h4>
                  </div>
                  <div class="visible-xs">
                     <div class="clearfix"></div>
                  </div>
                  <?php if(($boq->status != 2 && $boq->status != 3)){
                     if(!empty($boq->open_till) && date('Y-m-d',strtotime($boq->open_till)) < date('Y-m-d')){
                       echo '<span class="warning-bg content-view-status">'._l('boq_expired').'</span>';
                     } else { ?>
                  <?php if($identity_confirmation_enabled == '1'){ ?>
                  <button type="button" id="accept_action" class="btn btn-success pull-right action-button mleft5">
                  <i class="fa fa-check"></i> <?php echo _l('boq_accept_info'); ?>
                  </button>
                  <?php } else { ?>
                  <?php echo form_open($this->uri->uri_string()); ?>
                  <button type="submit" data-loading-text="<?php echo _l('wait_text'); ?>" autocomplete="off" class="btn btn-success pull-right action-button mleft5"><i class="fa fa-check"></i> <?php echo _l('boq_accept_info'); ?></button>
                  <?php echo form_hidden('action','accept_boq'); ?>
                  <?php echo form_close(); ?>
                  <?php } ?>
                  <?php echo form_open($this->uri->uri_string()); ?>
                  <button type="submit" data-loading-text="<?php echo _l('wait_text'); ?>" autocomplete="off" class="btn btn-default pull-right action-button mleft5"><i class="fa fa-remove"></i> <?php echo _l('boq_decline_info'); ?></button>
                  <?php echo form_hidden('action','decline_boq'); ?>
                  <?php echo form_close(); ?>
                  <?php } ?>
                  <!-- end expired boq -->
                  <?php } else {
                     if($boq->status == 2){
                       echo '<span class="danger-bg content-view-status">'._l('boq_status_declined').'</span>';
                     } else if($boq->status == 3){
                       echo '<span class="success-bg content-view-status">'._l('boq_status_accepted').'</span>';
                     }
                     } ?>
                  <?php echo form_open($this->uri->uri_string()); ?>
                  <button type="submit" class="btn btn-default pull-right action-button mleft5"><i class="fa fa-file-pdf-o"></i> <?php echo _l('clients_invoice_html_btn_download'); ?></button>
                  <?php echo form_hidden('action','boq_pdf'); ?>
                  <?php echo form_close(); ?>
                  <?php if(is_client_logged_in() && has_contact_permission('boqs')){ ?>
                  <a href="<?php echo site_url('clients/boqs/'); ?>" class="btn btn-default mleft5 pull-right action-button go-to-portal">
                  <?php echo _l('client_go_to_dashboard'); ?>
                  </a>
                  <?php } ?>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-8 boq-left">
         <div class="panel_s mtop20">
            <div class="panel-body boq-content tc-content padding-30">
               <?php echo $boq->content; ?>
            </div>
         </div>
      </div>
      <div class="col-md-4 boq-right">
         <div class="inner mtop20 boq-html-tabs">
            <ul class="nav nav-tabs nav-tabs-flat mbot15" role="tablist">
               <li role="presentation" class="<?php if(!$this->input->get('tab') || $this->input->get('tab') === 'summary'){echo 'active';} ?>">
                  <a href="#summary" aria-controls="summary" role="tab" data-toggle="tab">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo _l('summary'); ?></a>
               </li>
               <?php if($boq->allow_comments == 1){ ?>
               <li role="presentation" class="<?php if($this->input->get('tab') === 'discussion'){echo 'active';} ?>">
                  <a href="#discussion" aria-controls="discussion" role="tab" data-toggle="tab">
                  <i class="fa fa-commenting-o" aria-hidden="true"></i> <?php echo _l('discussion'); ?>
                  </a>
               </li>
               <?php } ?>
            </ul>
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane<?php if(!$this->input->get('tab') || $this->input->get('tab') === 'summary'){echo ' active';} ?>" id="summary">
                  <address class="boq-html-company-info">
                     <?php echo format_organization_info(); ?>
                  </address>
                  <hr />
                  <p class="bold boq-html-information">
                     <?php echo _l('boq_information'); ?>
                  </p>
                  <address class="no-margin boq-html-info">
                     <?php echo format_boq_info($boq, 'html'); ?>
                  </address>
                  <div class="row mtop20">
                     <?php if($boq->total != 0){ ?>
                     <div class="col-md-12 boq-html-total">
                        <h4 class="bold mbot30"><?php echo _l('boq_total_info',app_format_money($boq->total, $boq->currency_name)); ?></h4>
                     </div>
                     <?php } ?>
                     <div class="col-md-4 text-muted boq-status">
                        <?php echo _l('boq_status'); ?>
                     </div>
                     <div class="col-md-8 boq-status">
                        <?php echo format_boq_status($boq->status,'', false); ?>
                     </div>
                     <div class="col-md-4 text-muted boq-date">
                        <?php echo _l('boq_date'); ?>
                     </div>
                     <div class="col-md-8 boq-date">
                        <?php echo _d($boq->date); ?>
                     </div>
                     <?php if(!empty($boq->open_till)){ ?>
                     <div class="col-md-4 text-muted boq-open-till">
                        <?php echo _l('boq_open_till'); ?>
                     </div>
                     <div class="col-md-8 boq-open-till">
                        <?php echo _d($boq->open_till); ?>
                     </div>
                     <?php } ?>
                  </div>
                  <?php if(count($boq->attachments) > 0 && $boq->visible_attachments_to_customer_found == true){ ?>
                  <div class="boq-attachments">
                     <hr />
                     <p class="bold mbot15"><?php echo _l('boq_files'); ?></p>
                     <?php foreach($boq->attachments as $attachment){
                        if($attachment['visible_to_customer'] == 0){continue;}
                        $attachment_url = site_url('download/file/sales_attachment/'.$attachment['attachment_key']);
                        if(!empty($attachment['external'])){
                          $attachment_url = $attachment['external_link'];
                        }
                        ?>
                     <div class="col-md-12 row mbot15">
                        <div class="pull-left"><i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i></div>
                        <a href="<?php echo $attachment_url; ?>"><?php echo $attachment['file_name']; ?></a>
                     </div>
                     <?php } ?>
                  </div>
                  <?php } ?>
               </div>
               <?php if($boq->allow_comments == 1){ ?>
               <div role="tabpanel" class="tab-pane<?php if($this->input->get('tab') === 'discussion'){echo ' active';} ?>" id="discussion">
                  <?php echo form_open($this->uri->uri_string()) ;?>
                  <div class="boq-comment">
                     <textarea name="content" rows="4" class="form-control"></textarea>
                     <button type="submit" class="btn btn-info mtop10 pull-right" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('boq_add_comment'); ?></button>
                     <?php echo form_hidden('action','boq_comment'); ?>
                  </div>
                  <?php echo form_close(); ?>
                  <div class="clearfix"></div>
                  <?php
                     $boq_comments = '';
                     foreach ($comments as $comment) {
                      $boq_comments .= '<div class="boq_comment mtop10 mbot20" data-commentid="' . $comment['id'] . '">';
                      if($comment['staffid'] != 0){
                        $boq_comments .= staff_profile_image($comment['staffid'], array(
                          'staff-profile-image-small',
                          'media-object img-circle pull-left mright10'
                        ));
                      }
                      $boq_comments .= '<div class="media-body valign-middle">';
                      $boq_comments .= '<div class="mtop5">';
                      $boq_comments .= '<b>';
                      if($comment['staffid'] != 0){
                        $boq_comments .= get_staff_full_name($comment['staffid']);
                      } else {
                        $boq_comments .= _l('is_customer_indicator');
                      }
                      $boq_comments .= '</b>';
                      $boq_comments .= ' - <small class="mtop10 text-muted">' . time_ago($comment['dateadded']) . '</small>';
                      $boq_comments .= '</div>';
                      $boq_comments .= '<br />';
                      $boq_comments .= check_for_links($comment['content']) . '<br />';
                      $boq_comments .= '</div>';
                      $boq_comments .= '</div>';
                     }
                     echo $boq_comments; ?>
               </div>
               <?php } ?>
            </div>
            <div style="margin-top:80px;"></div>
         </div>
      </div>
   </div>
</div>
<?php
   if($identity_confirmation_enabled == '1'){
        get_template_part('identity_confirmation_form',array('formData'=>form_hidden('action','accept_boq')));
   }
   ?>
<script>
   $(function(){
     new Sticky('[data-sticky]');
     $(".boq-left table").wrap("<div class='table-responsive'></div>");
         // Create lightbox for boq content images
         $('.boq-content img').wrap( function(){ return '<a href="' + $(this).attr('src') + '" data-lightbox="boq"></a>'; });
     });
</script>
</div>
