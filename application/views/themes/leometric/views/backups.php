<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Backup'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Backup'); ?></h3>
        <!-- <a href="<?php echo site_url('clients/open_ticket'); ?>" class="btn btn-info new-ticket pull-right">
          <?php echo _l('clients_ticket_open_subject'); ?>
        </a> -->
        <div class="clearfix"></div>
        <hr />
      </div>
      <?php /*foreach($statuses as $status){
          $_where = '';  
          $_where = 'status_id='.$status['id'].' AND '.'clientid='.$session; 
          ?>
          <!-- data-cview="ticket_status_<?php echo $status['ticketstatusid']; ?>" 
               onclick="dt_custom_view('ticket_status_<?php echo $status['ticketstatusid']; ?>','.tickets-table','ticket_status_<?php echo $status['ticketstatusid']; ?>',true); return false;"
                -->
          <div class="col-md-2 col-xs-6 mbot15 border-right">
            <a href="#">
              <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
              <span style="">
                <?php echo $status['status_title']; ?>
              </span>
            </a>
          </div>
      <?php }*/ ?>

  </div>
  <div class="clearfix"></div>
  <!-- <hr /> -->
  <div class="clearfix"></div>
      <table class="table dt-table table-backups" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
      <thead>
        <th><?php echo _l('Sr.No.'); ?></th>
        <th><?php echo _l('View Logs'); ?></th>
        <th><?php echo _l('Customer'); ?></th>
        <th><?php echo _l('Policy Title'); ?></th>
        <th><?php echo _l('Restore Point'); ?></th>
        <th><?php echo _l('Archive'); ?></th>
        <th><?php echo _l('Archive Retention Period'); ?></th>
        <th><?php echo _l('Full Backup'); ?></th>
        <th><?php echo _l('Incremental Backup'); ?></th>
        <th><?php echo _l('First Backup Destination'); ?></th>
        <th><?php echo _l('Second Backup Destination'); ?></th>
        <th><?php echo _l('Third Backup Destination'); ?></th>
        <th><?php echo _l('Recovery Time Objective'); ?></th>
        <th><?php echo _l('Recovery Point Objective'); ?></th>
       
        <?php
        $custom_fields = get_custom_fields('backups',array('show_on_client_portal'=>1));
        foreach($custom_fields as $field){ ?>
          <th><?php echo $field['name']; ?></th>
        <?php } ?>
      </thead>
      <tbody>
        <?php foreach($backups as $key => $backup){ ?>
          <tr>  <!-- class="<?php if($backup['clientread'] == 0){echo 'text-danger';} ?>" -->
            <td data-order="<?php echo $backup['id']; ?>">
              <!-- <a href="<?php echo site_url('clients/backup/'.$backup['id']); ?>"> -->
                <a href="#">
                <?php echo $key+1; ?>
              </a>
            </td>

            <td>
              <!--onclick="backup_log(this,<?php echo $backup['id']; ?>); return false"-->
              <a href="<?php echo site_url('clients/backup_logs/').$backup['id'] ?>" data-name="<?php echo $backup['id']; ?>" class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>
            </td>
          <td><?php echo $backup['company']; ?></td>
          <td><?php echo $backup['policy_title'] ?></td>
          <td><?php echo $backup['restore_point'] ?></td>
          <td><?php echo $backup['archive'] ?></td>
          <td><?php echo $backup['archive_retention_period'].' Months' ?></td>
          <td><?php echo $backup['full_backup'] ?></td>
          <td><?php echo $backup['incremental_backup'] ?></td>
          <td><?php echo $backup['first_backup_destination'] ?></td>
          <td><?php echo $backup['second_backup_destination'] ?></td>
          <td><?php echo $backup['third_backup_destination'] ?></td>
          <td><?php echo $backup['recovery_time_obj'].' Days' ?></td>
          <td><?php echo $backup['recovery_point_obj'].' Days' ?></td>
             <?php foreach($custom_fields as $field){ ?>
              <td>
                <?php echo get_custom_field_value($backup['backupid'],$field['id'],'backups'); ?>
              </td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
</div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Backup Logs</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script type="text/javascript">
    function backup_log(url,id){
      $.ajax({
           url:'<?=admin_url()?>backups/backup_log_data/'+id,
           method:"POST",
           data:{id:id},
           success:function(data){
            if(data){
              //alert(data)
              $('.logDetailsData').html(data);
            }
           }
      });
                              
      $('#logmodal').modal('show');   
    }
  </script>