<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css">
  html {
  scroll-behavior: smooth;
}
.panel_s {
  scroll-margin: 60px 0 0 50px;
}

@media (max-width: 480px){
  .hide-button-mobile{
    display: none !important;
  }
}

</style>
<div class="row">
  <div class="col-md-12">
      <h3 class="text-center">Overview Title: <?php echo $name->title; ?></h3>
  </div>
</div>

<?php if(count($service_overviews) <= 0 && count($network_server_assets) <= 0 && count($network_server_diagrams) <= 0 && count($network_server_urls)<= 0 && count($network_server_gaps) <= 0 && count($network_server_software_licenses) <= 0 ){ ?>
    <h3>No Data Found</h3>
<?php } ?>


<div class="panel_s section-heading section-service_requests hide-button-mobile" style="position: -webkit-sticky;position: sticky;top: 0;z-index: 99">
  <div class="panel-body">
    <a  href="#service_overviews" class="btn btn-info new-service_request">
      <?php echo _l(' Service overviews'); ?>
    </a>
    <a   href="#asset_list" class="btn btn-info new-service_request">
      <?php echo _l(' Dependencies'); ?>
    </a>
    <a  href="#diagram_list" class="btn btn-info new-service_request">
      <?php echo _l(' Diagrams and Images'); ?>
    </a>
    <a  href="#monitoring_list" class="btn btn-info new-service_request">
      <?php echo _l(' Network Monitoring'); ?>
    </a>
    <a href="#gap_list" class="btn btn-info new-service_request">
      <?php echo _l(' GAP'); ?>
    </a>
    <a href="#renewal_list" class="btn btn-info new-service_request">
      <?php echo _l(' Software and License'); ?>
    </a>

  </div>
</div>




<?php  if(count($service_overviews) > 0 ){  ?>
<div class="panel_s" id="service_overviews">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Service Overview and Files '); ?>
        </h3>
      </div>
    </div>
    <hr style="margin: 0 !important;">
    <div class="clearfix"></div>
    <?php if(count($service_overviews) > 0 ){ ?>
    <p style="padding-top: 10px;"><?php if(isset($service_overviews[0]['summary']) && !empty($service_overviews[0]['summary']))
      {
      print_r($service_overviews[0]['summary']);
      } else { ?>
      <p class="no-margin"><?php echo _l('No service overview summary '); ?></p>
      <?php }?>
    </p>
    <hr style="margin-top: 0 !important">
    <?php if(count($service_overviews) > 0 ){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('File'); ?></th>
        <th><?php echo _l('Option'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($service_overviews as $key => $service_overview) { ?>
          <?php if(isset($service_overview['file_name']) && !empty($service_overview['file_name'])){ ?>
        <tr class="has-row-options">
          
          <td> <?php echo $service_overview['file_name']; ?>
          </td>
          <td>
            <a target="_blank"
              href = "<?php echo base_url('/uploads/network_servers/'.$service_overview['service_overviewid'].'/'.$service_overview['file_name'])?>"
              class="btn btn-default btn-icon">
              <i class="fa fa-eye"></i>
            </a>
          </td>
          </tr>
          <?php } ?>
        
        <?php  } ?>
      </tbody>
    </table>
    <?php } ?>
    <?php } ?>
  </div>
</div>
<hr>
<?php } ?>
<!-- Asset -->
<?php if(count($network_server_assets) > 0){ ?>
<div class="panel_s" id="asset_list">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Dependencies'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <?php if(count($network_server_assets) > 0){ ?>
    <table class="table  dataTable dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('Dependency label'); ?></th>
        <th><?php echo _l('Type'); ?></th>
        <th><?php echo _l('Sub-type'); ?></th>
        <th><?php echo _l('Manufacture'); ?></th>
        <th><?php echo _l('Product name'); ?></th>
        <th><?php echo _l('Host name'); ?></th>
        <th><?php echo _l('Location'); ?></th>
        <th><?php echo _l('Options'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($network_server_assets as $key => $network_server_asset) { ?>
        <tr class="has-row-options">
          <td> <?php echo $network_server_asset['customer_prefix']?>
            <div class="row-options">
              <!-- <a
                href="
              <?php echo admin_url('network_servers/network_server_asset_delete/').$network_server_asset['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>  -->
            </div>
          </td>
          <td> <?php echo $network_server_asset['type']?> </td>
          <td> <?php echo $network_server_asset['subtype']?> </td>
          <td> <?php echo $network_server_asset['manufacture_name']?> </td>
          <td> <?php echo $network_server_asset['product_name']?> </td>
          <td> <?php echo $network_server_asset['hostname']?> </td>
          <td> <?php echo $network_server_asset['location']?> </td>
          <td>
            <a href = "#"
              data_company = "<?php echo $network_server_asset['company']?>"
              data_type = "<?php echo $network_server_asset['type']?>"
              data_subtype = "<?php echo $network_server_asset['subtype']?>"
              data_manufacture_name = "<?php echo $network_server_asset['manufacture_name']?>"
              data_product_name = "<?php echo $network_server_asset['product_name']?>"
              data_hostname = "<?php echo $network_server_asset['hostname']?>"
              data_location = "<?php echo $network_server_asset['location']?>"
              data_status = "<?php echo $network_server_asset['status_name']?>"
              data_processor = "<?php echo $network_server_asset['processor']?>"
              data_ram = "<?php echo $network_server_asset['ram']?>"
              data_storage = "<?php echo $network_server_asset['storage']?>"
              data_ip_address = "<?php echo $network_server_asset['ip_address']?>"
              data_serial_no = "<?php echo $network_server_asset['serial_no']?>"
              data_operating_system = "<?php echo $network_server_asset['operating_system']?>"
              data_sw_valid_from = "<?php echo $network_server_asset['sw_valid_from']?>"
              data_sw_valid_to = "<?php echo $network_server_asset['sw_valid_to']?>"
              data_sw_amount = "<?php echo $network_server_asset['sw_amount']?>"
              data_sw_renewal = "<?php echo $network_server_asset['sw_renewal']?>"
              data_backup_required = "<?php echo $network_server_asset['backup_required']?>"
              data_subject = "<?php echo $network_server_asset['subject']?>"
              data_firstname = "<?php echo $network_server_asset['firstname']?>"
              data_lastname = "<?php echo $network_server_asset['lastname']?>"
              
              class="showAssetsOnModal btn btn-default btn-icon">
            <i class="fa fa-eye"></i></a>
          </td>
        </tr>
        <?php  } ?>
      </tbody>
    </table>
    <?php } ?>
  </div>
</div>
<hr>
<?php }  ?>
<!-- Diagram -->
<?php if(count($network_server_diagrams) > 0){ ?>
<div class="panel_s" id="diagram_list">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Diagrams and Images'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <?php if(count($network_server_diagrams) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Diagrams and Images Title'); ?></th>
        <th><?php echo _l('Diagrams and Images File'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($network_server_diagrams as $key => $diagram) { ?>
        <tr class="has-row-options">
          <td> <?php echo $key+1;?>
            <div class="row-options">
              <!--  <a
                href="
              <?php echo admin_url('network_servers/network_server_diagram_delete/').$diagram['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>   -->
            </div>
          </td>
          <td><?php echo $diagram['title']?></td>
          <td>
            <a href="<?php echo base_url('uploads/diagrams/'.$diagram['diagramsid'].'/thumb_'.$diagram['diagram_file']); ?>" target="_blank">
              <img width="100px" height="60px" src="<?php echo base_url('uploads/diagrams/'.$diagram['diagramsid'].'/thumb_'.$diagram['diagram_file']); ?>" alt=''>
            </a>
          </td>
        </tr>
        <?php  } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No diagram found'); ?></p>
    <?php } ?>
  </div>
</div>
<hr>
<?php } ?>
<!-- Monitoring -->
<?php if(is_admin()) { ?>
<?php if(count($network_server_urls) > 0){ ?>
<div class="panel_s" id="monitoring_list" >
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Network Monitoring'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <?php if(count($network_server_urls) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Title'); ?></th>
        <th><?php echo _l('Option'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($network_server_urls as $key => $url) { ?>
        <tr class="has-row-options">
          <td> <?php echo $key+1;?>
            <div class="row-options">
              <!-- <a
                href="
              <?php echo admin_url('network_servers/network_server_url_delete/').$url['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>   -->
            </div>
          </td>
          <td><?php echo $url['monitoring_title']?></td>
          <td><a href = "#" data-url="<?php echo $url['url']; ?>"
            class=" view_url_in_frame btn btn-default btn-icon">
          <i class="fa fa-eye"></i></a></td>
        </tr>
        <?php  } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No monitoring found'); ?></p>
    <?php } ?>
  </div>
</div>
<hr>
<?php }  ?>
<?php } ?>
<!-- GAP -->
<?php if(count($network_server_gaps) > 0){ ?>
<div class="panel_s" id="gap_list">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('GAP'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <?php if(count($network_server_gaps) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('Customer'); ?></th>
        <th><?php echo _l('Name'); ?></th>
        <th><?php echo _l('Owner'); ?></th>
        <th><?php echo _l('Category'); ?></th>
        <th><?php echo _l('Impact'); ?></th>
        <th><?php echo _l('Status'); ?></th>
        <th><?php echo _l('Date'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($network_server_gaps as $key => $gap) { ?>
        <tr class="has-row-options">
          <td> <?php echo $gap['company']; ?>
            <div class="row-options">
              <!-- <a
                href="
              <?php echo admin_url('network_servers/network_server_gap_delete/').$gap['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>   -->
            </div>
          </td>
          <td><?php echo $gap['name']?></td>
          <td><?php echo $gap['description']?></td>
          <td><?php echo $gap['category_title']?></td>
          <td><?php echo $gap['impact_title']?></td>
          <td><?php echo $gap['status_title']?></td>
          <td><?php echo $gap['created_at']?></td>
        </tr>
        <?php  } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No gap found'); ?></p>
    <?php } ?>
  </div>
</div>
<hr>
<?php }  ?>



<!-- Software and License -->
<?php if(count($network_server_software_licenses) > 0){ ?>
<div class="panel_s" id="renewal_list">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo _l('Software and License'); ?>
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <?php if(count($network_server_software_licenses) > 0){ ?>
    <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('Customer'); ?></th>
        <th><?php echo _l('Name'); ?></th>
        <th><?php echo _l('Type'); ?></th>
        <th><?php echo _l('Vendor'); ?></th>
        <th><?php echo _l('Start Date'); ?></th>
        <th><?php echo _l('End Date'); ?></th>
      </thead>
      <tbody>
        <?php foreach ($network_server_software_licenses as $key => $software_license) { ?>
        <tr class="has-row-options">
          <td> <?php echo $software_license['company']; ?>
            <div class="row-options">
              <!-- <a
                href="
              <?php echo admin_url('network_servers/network_server_gap_delete/').$gap['id'].'/'.$id ?>" class="text-danger _delete">Delete </a>   -->
            </div>
          </td>
          <td><?php echo $software_license['name']?></td>
          <td><?php echo $software_license['type_name']?></td>
          <td><?php echo $software_license['vendor_name']?></td>
          <td><?php echo $software_license['start_date']?></td>
          <td><?php echo $software_license['end_date']?></td>
        </tr>
        <?php  } ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p class="no-margin"><?php echo _l('No gap found'); ?></p>
    <?php } ?>
  </div>
</div>
<hr>
<?php }  ?>


<div class="modal fade" id="assetmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <div class="assetDetailsData">
          
        </div>
        
      </div>
    </div>
  </div>
</div>
<!-- Modal to show image  -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;height: 70vh;" >
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="imageurlmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close closeiframemodal" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <iframe class="myframe" src="" style="height: 75vh !important;width:100% !important;" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
<!--
<script type="text/javascript">
$('#inventory').click(function(e)
{
$('#diagram_list, #monitoring_list, #gap_list').css('display','none');
$('#asset_list').css('display','block');
});
$('#diagram').click(function(e)
{
$('#asset_list, #monitoring_list, #gap_list').css('display','none');
$('#diagram_list').css('display','block');
});
$('#monitoring').click(function(e)
{
$('#asset_list, #diagram_list, #gap_list').css('display','none');
$('#monitoring_list').css('display','block');
});
$('#gap').click(function(e)
{
$('#asset_list, #diagram_list, #monitoring_list').css('display','none');
$('#gap_list').css('display','block');
});
</script> -->
<script type="text/javascript">
$(".showAssetsOnModal").on("click", function(){
var data_company = $(this).attr("data_company");
var data_type = $(this).attr("data_type");
var data_subtype = $(this).attr("data_subtype");
var data_manufacture_name = $(this).attr("data_manufacture_name");
var data_product_name = $(this).attr("data_product_name");
var data_hostname = $(this).attr("data_hostname");
var data_location = $(this).attr("data_location");
var data_status = $(this).attr("data_status");
var data_ram = $(this).attr("data_ram");
var data_storage = $(this).attr("data_storage");
var data_ip_address = $(this).attr("data_ip_address");
var data_serial_no = $(this).attr("data_serial_no");
var data_operating_system = $(this).attr("data_operating_system");
var data_sw_valid_from = $(this).attr("data_sw_valid_from");
var data_sw_valid_to = $(this).attr("data_sw_valid_to");
var data_sw_amount = $(this).attr("data_sw_amount");
var data_sw_renewal = $(this).attr("data_sw_renewal");
var data_backup_required = $(this).attr("data_backup_required");
var data_subject = $(this).attr("data_subject");
var data_sw_valid_from = $(this).attr("data_sw_valid_from");
var data_firstname = $(this).attr("data_firstname");
var data_lastname = $(this).attr("data_lastname");
var contact  =  data_firstname +' '+ data_lastname;
var BackupStatus = '';
if(data_backup_required == 1){
BackupStatus = "Yes";
}
else if(data_backup_required == 0)
{
BackupStatus = "No";
}
var table_header = "<table class='table table-striped table-bordered'><tbody>";
var table_footer = "</tbody></table>";
var html ="";
html += "<tr><td>"+'Customer'+"</td><td>"+data_company+"</td></tr>";
html += "<tr><td>"+'Type'+"</td><td>"+data_type+"</td></tr>";
html += "<tr><td>"+'Sub type'+"</td><td>"+data_subtype+"</td></tr>";
html += "<tr><td>"+'Manufacture'+"</td><td>"+data_manufacture_name+"</td></tr>";
html += "<tr><td>"+'Product'+"</td><td>"+data_product_name+"</td></tr>";
html += "<tr><td>"+'Host name'+"</td><td>"+data_hostname+"</td></tr>";
html += "<tr><td>"+'Location'+"</td><td>"+data_location+"</td></tr>";
html += "<tr><td>"+'Status'+"</td><td>"+data_status+"</td></tr>";
html += "<tr><td>"+'RAM'+"</td><td>"+data_ram+"</td></tr>";
html += "<tr><td>"+'Storage'+"</td><td>"+data_storage+"</td></tr>";
html += "<tr><td>"+'IP'+"</td><td>"+data_ip_address+"</td></tr>";
html += "<tr><td>"+'Serial number'+"</td><td>"+data_serial_no+"</td></tr>";
html += "<tr><td>"+'OS'+"</td><td>"+data_operating_system+"</td></tr>";
html += "<tr><td>"+'Backup required'+"</td><td>"+BackupStatus+"</td></tr>";
html += "<tr><td>"+'Contract'+"</td><td>"+data_subject+"</td></tr>";
html += "<tr><td>"+'Contact'+"</td><td>"+contact+"</td></tr>";
var all = table_header +html+ table_footer;
$('#assetmodal .assetDetailsData').html(all);
$('#assetmodal').modal('show');
});
</script>
<!-- //Script for Image show on modal -->
<script type="text/javascript">

// $(function() {
//   $('.pop').on('click', function() {
//     $('.imagepreview').attr('src', $(this).attr('src'));
//     $('#imagemodal').modal('show');
//   });
// });


</script>
<script type="text/javascript">
$(function() {
$('.view_url_in_frame').click(function() {
$('#imageurlmodal').modal('show');
var url = $(this).attr('data-url');
if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0)
{
iframe = $('iframe');
iframe.attr('src', url);
}
else
{
var prefix = 'https://';
if (url.substr(0, prefix.length) !== prefix)
{
url = prefix + url;
iframe = $('iframe');
iframe.attr('src', url);
}
}
});
});
$('.closeiframemodal').click(function(e) {
$('iframe').attr('src', '');
});
</script>