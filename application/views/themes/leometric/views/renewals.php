<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-renewals">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Software and License'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop renewals-summary-heading"><?php echo _l('Software and License'); ?></h3>
       <!--  <a href="<?php echo site_url('clients/open_renewal'); ?>" class="btn btn-info new-renewal pull-right">
          <?php echo _l('clients_renewal_open_subject'); ?>
        </a> -->
        <div class="clearfix"></div>
        <hr />
      </div>

    
      <div class="col-md-3 list-status ticket-status">
           <a href="#" class="active">
              <h3 class="bold renewals-status-heading"><?php echo $active_renewal; ?></h3>
              <span class="text-info">Active</span>
          </a>
      </div>

       <div class="col-md-3 list-status ticket-status">
           <a href="#" class="active">
              <h3 class="bold renewals-status-heading"><?php echo $expired_renewal; ?></h3>
              <span class="text-danger">Expired</span>
          </a>
      </div>

       <div class="col-md-3 list-status ticket-status">
           <a href="#" class="active">
              <h3 class="bold renewals-status-heading"><?php echo $expired_in_one_month_renewal; ?></h3>
              <span class="text-warning">Expiring in 1 Month</span>
          </a>
      </div>

       <div class="col-md-3 list-status ticket-status">
           <a href="#" class="active">
              <h3 class="bold renewals-status-heading"><?php echo $expired_in_two_month_renewal; ?></h3>
              <span style="color:#2d2d2d;">Expiring in 2 Months</span>
          </a>
      </div>
    

  </div>
  <div class="clearfix"></div>
  <hr />
  <div class="clearfix"></div>
   <table class="table dt-table scroll-responsive">
                              <thead>
                                <th><?php echo _l('id'); ?></th>
                                <th><?php echo _l('Contact'); ?></th>
                                <th><?php echo _l('Name'); ?></th>
                                <th><?php echo _l('Type'); ?></th>
                                <th><?php echo _l('Vendor'); ?></th>
                                <th><?php echo _l('Start Date'); ?></th>
                                <th><?php echo _l('End Date'); ?></th>
                                <!-- <th><?php echo _l('Notify To'); ?></th> -->
                              </thead>
                              <tbody>
                                <?php if(count($renewals) > 0){?>

                                <?php foreach ($renewals as $key => $renewals_list) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?>
                                    <div class="row-options">
                                        
                                     <!--    <a href="<?php echo admin_url('renewals/renewal/').$renewals_list['id'] ?>">Edit </a> 
                                          <span class="text-dark"> | </span>
                                        <a 
                                        href="<?php echo admin_url('renewals/delete/').$renewals_list['id'] ?>" class="text-danger _delete">Delete </a>    -->
                                  </td>
                                  <td><?php echo $renewals_list['company']; ?></td>
                                  <td><?php echo $renewals_list['name'] ?></td>
                                  <td><?php echo $renewals_list['type_name'] ?></td>
                                  <td><?php echo $renewals_list['vendor_name'] ?></td>
                                  <td><?php echo $renewals_list['start_date'] ?></td>
                                  <td><?php echo $renewals_list['end_date'] ?></td>
                                  <!-- <td><?php echo $renewals_list['notify_to'] ?></td> -->
                                </tr>
                                <?php }  ?>
                              <?php } else{ ?>
                                <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
                              <?php }?>
                              </tbody>
                            </table>
</div>
</div>
</div>
