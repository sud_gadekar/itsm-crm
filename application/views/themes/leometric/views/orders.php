<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Orders'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Sales Orders'); ?></h3>
        <!-- <a href="<?php echo site_url('clients/open_ticket'); ?>" class="btn btn-info new-ticket pull-right">
          <?php echo _l('clients_ticket_open_subject'); ?>
        </a> -->
        <div class="clearfix"></div>
        <hr />
      </div>
      <?php /*foreach($statuses as $status){
          $_where = '';  
          $_where = 'status_id='.$status['id'].' AND '.'clientid='.$session; 
          ?>
          <!-- data-cview="ticket_status_<?php echo $status['ticketstatusid']; ?>" 
               onclick="dt_custom_view('ticket_status_<?php echo $status['ticketstatusid']; ?>','.tickets-table','ticket_status_<?php echo $status['ticketstatusid']; ?>',true); return false;"
                -->
          <div class="col-md-2 col-xs-6 mbot15 border-right">
            <a href="#">
              <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
              <span style="">
                <?php echo $status['status_title']; ?>
              </span>
            </a>
          </div>
      <?php }*/ ?>

  </div>
  <div class="clearfix"></div>
  <!-- <hr /> -->
  <div class="clearfix"></div>
      <table class="table dt-table table-orders" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
      <thead>
        <th><?php echo _l('Sr.No.'); ?></th>
        <th><?php echo _l('View Details'); ?></th>
        <th><?php echo _l('Description'); ?></th>
        <th><?php echo _l('LPO No'); ?></th>
        <th><?php echo _l('SO No'); ?></th>
        <th><?php echo _l('SO Date'); ?></th>
        <th><?php echo _l('Amount'); ?></th>
        <th><?php echo _l('Material Status'); ?></th>
        <th><?php echo _l('Implementation Status'); ?></th>
        <th><?php echo _l('Service Request'); ?></th>
        <th><?php echo _l('Project'); ?></th>
        <th><?php echo _l('Invoice No'); ?></th>
        <th><?php echo _l('Invoice Date'); ?></th>
        <th><?php echo _l('Payment Date'); ?></th>
        <th><?php echo _l('Payment Period (Days)'); ?></th>
       
        <?php
        $custom_fields = get_custom_fields('orders',array('show_on_client_portal'=>1));
        foreach($custom_fields as $field){ ?>
          <th><?php echo $field['name']; ?></th>
        <?php } ?>
      </thead>
      <tbody>
        <?php foreach($orders as $key => $order_tracker){ ?>
        <tr>  
          <td data-order="<?php echo $order_tracker['id']; ?>">
            <a href="#">
              <?php echo $key+1; ?>
            </a>
          </td>
          <td>
            <a href="<?php echo site_url('clients/order_details/').$order_tracker['id'] ?>" data-name="<?php echo $order_tracker['id']; ?>" class="btn btn-default btn-icon"><i class="fa fa-eye"></i></a>
          </td>
          <td><?php echo $order_tracker['description']; ?></td>
          <td><?php echo $order_tracker['lpo_no']; ?></td>
          <td><?php echo $order_tracker['so_number'] ?></td>
          <td><?php echo $order_tracker['order_date']; ?></td>
          <td><?php echo $order_tracker['amount']; ?></td>
          <td><?php echo $order_tracker['name']; ?></td>
          <td><?php echo $order_tracker['service_request']; ?></td>
          <td><?php echo $order_tracker['subject']; ?></td>
          <td><?php echo $order_tracker['project']; ?></td>
          <td><?php echo $order_tracker['invoice_no']; ?></td>
          <td><?php echo $order_tracker['inv_submission_date']; ?></td>
          <td><?php echo $order_tracker['payment_date']; ?></td>
          <td><?php echo $order_tracker['payment_received_in']; ?></td>
             <?php foreach($custom_fields as $field){ ?>
              <td>
                <?php echo get_custom_field_value($order_tracker['id'],$field['id'],'orders'); ?>
              </td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
</div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Order Details</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script type="text/javascript">
    function order_details(){                  
      $('#logmodal').modal('show');   
    }
</script>