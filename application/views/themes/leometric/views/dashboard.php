<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .iframecontainer {
      position: relative;
      width: 100%;
      overflow: hidden;
      padding-top: 56.25%; /* 16:9 Aspect Ratio */
    }

    .responsive-iframe {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      width: 100%;
      height: 100%;
      border: none;
    }
    iframe html body img{
      width: 100% !important;
    }

   @media (max-width: 768px) {
    .it_health_status_mobile {
      display: none !important; 
    }
  }
  .circle-icon {
    background: #bfc7c1;
    padding:20px;
    border-radius: 100%;
}
</style>
<div class="panel_s" >
  <div class="panel-body">
  <h4 class="customer-profile-group-heading">
    <?php if($this->session->userdata('client_user_id')){
      echo $this->db->where('userid',$this->session->userdata('client_user_id'))
                    ->get('tblclients')->row()->company;
    }?> IT Services Dashboard

    <a href="#" class="btn btn-info it-health-status" style="float: right; margin-top: -10px;">IT Health Status</a>
  </h4>
    <!-- <hr> -->
    <div class="row">
     <a href="<?php echo site_url('clients/tickets');?>" class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row" align="center">
              <div class="col-md-12">
               <!--   <p> <i class="fa fa-wrench circle-icon" aria-hidden="true" style="font-size:25px " ></i></p> -->
                 <p>  <img height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_incident.png'); ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Support Requests" ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_no_json; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>

   <a href="<?php echo site_url('clients/service_requests');?>" class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
             <div class="row" align="center">
              <div class="col-md-12 ">
                 <p>  <img height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_service.png'); ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Service Req." ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_sr_no; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>
    <a href="<?php echo site_url('clients/projects');?>"  class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
             <div class="row" align="center">
              <div class="col-md-12 ">
                 <p>  <img height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_project.png'); ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Projects" ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_project_no; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>
<!--   </div>

  <div class="row"> -->
     <a href="<?php echo site_url('clients/gaps');?>" class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
             <div class="row" align="center">
              <div class="col-md-12 ">
                  <p>  <img height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_outage.png'); ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Risks" ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_gap_no; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>

   <a href="<?php echo site_url('clients/orders');?>" class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
             <div class="row" align="center">
              <div class="col-md-12 ">
                   <p>  <img height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_order.png'); ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Orders" ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_order_no; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>
    <a href="<?php echo site_url('knowledge-base');?>" class="col-md-2">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row" align="center">
              <div class="col-md-12 ">
                   <p>  <img  height=30 width="auto"  src="<?php echo site_url('/assets/images/ic_article.png');  ?>" ></p>
               <b> <p class="padding-5" style="font-size: 1.1vw;"> <?php echo "Guides" ?></p>
                <b> <p class="padding-5" style="color:#26a6bd;font-size:22px"> <?php echo $all_data_count_knowledge_no; ?></p>
              </div>
            </div>
          </div>
        </div>
  </a>
    
  </div>

    <div class="row">
      <div class="row">
        <?php if(count($tickets_status_no_json['datasets'][0]['data']) > 0){ ?>
      <div class="col-md-4">
        

        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Support Request by status'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="tickets-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
       
      </div>
       <?php } ?>
       <?php if(count($service_requests_status_no_json['datasets'][0]['data']) > 0){ ?>
      <div class="col-md-4">
        
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Service request by status'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="service_requests-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <?php } ?>
       <?php if(count($projects_status_no_json['datasets'][0]['data']) > 0){ ?>
      <div class="col-md-4">
       
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Projects by status'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="projects-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
       
      </div>
       <?php } ?>
            <?php if(count($gaps_status_no_json['datasets'][0]['data']) > 0){ ?>

      <div class="col-md-4">
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('GAP by status'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="gaps-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <?php } ?>
      <?php if(count($inventorys_status_no_json['datasets'][0]['data']) > 0){ ?>
      <div class="col-md-4">
        
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Inventory by Types'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="inventorys-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <?php } ?>
       <?php if(count($inventorysubtypes_status_no_json['datasets'][0]['data']) > 0){ ?>
      <div class="col-md-4">
       
        <div class="panel_s">
          <div class="panel-body padding-10">
            <div class="row">
              <div class="col-md-12 mbot10">
                <p class="padding-5"> <?php echo _l('Inventory by Subtypes'); ?></p>
                <hr class="hr-panel-heading-dashboard">
                <canvas height="170" id="inventorysubtypes-by-status"></canvas>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <?php } ?>
    </div> 
    <hr class="hr-panel-heading">
      <div class="col-md-12 it_health_status_mobile">
        <h4 class="no-margin">IT Health Status</h4></br>
      </div>
      <div class="col-md-12 col-xs-12 mbot15 border-right it_health_status_mobile">
        <div class="iframecontainer">
        <iframe scrolling="yes" src="<?php echo $network_map->network_map_url;?>" class="responsive-iframe" title="Network Map"></iframe>
      </div>
      </div>
    </div> 
   <!--  <hr class="hr-panel-heading">
      <div class="col-md-12">
        <h4 class="no-margin">Support Request by status</h4>
      </div>
      <?php foreach($statuses as $status){
          $_where = '';
          $where = '';
          if($where == ''){
              $_where = 'status='.$status['ticketstatusid'].' AND userid='.$clientid;
          } 
          else
          {
              $_where = 'status='.$status['ticketstatusid'] . ' '.$where;
          } ?>
          <div class="col-md-2 col-xs-6 mbot15 border-right">
            <a href="<?php echo base_url('clients/tickets');?>">
              <h3 class="bold"><?php echo total_rows(db_prefix().'tickets',$_where); ?></h3>
              <span style="color:<?php echo $status['statuscolor']; ?>">
                <?php echo ticket_status_translate($status['ticketstatusid']); ?>
              </span>
            </a>
          </div>
      <?php } ?>
    </div>
    <hr class="hr-panel-heading"> -->
    <!-- Service Request -->
    <!-- <div class="row">
      <div class="col-md-12">
        <h4 class="no-margin">Service request by status</h4>
      </div>
      <?php foreach($statuses as $status){
        $_where = '';
        $where = '';
        if($where == '')
        {
            $_where = 'status='.$status['ticketstatusid'].' AND userid='.$clientid;;
        } else
        {
            $_where = 'status='.$status['ticketstatusid'] . ' '.$where;
        } ?>
        <div class="col-md-2 col-xs-6 mbot15 border-right">
          <a href="<?php echo base_url('clients/service_requests');?>">
            <h3 class="bold"><?php echo total_rows(db_prefix().'service_requests',$_where); ?></h3>
            <span style="color:<?php echo $status['statuscolor']; ?>">
              <?php echo ticket_status_translate($status['ticketstatusid']); ?>
            </span>
          </a>
        </div>
      <?php } ?>
    </div> -->
    <!-- <hr class="hr-panel-heading"> -->
    <!-- Projects -->
   <!--  <div class="row">
      <div class="col-md-12">
        <h4 class="no-margin">Projects by status</h4>
      </div>
      <?php foreach($project_statuses as $project_status){
          $_where = '';
          $where = '';
          if($where == '')
          {
              $_where = 'status='.$project_status['id'].' AND clientid='.$clientid;;
          } else
          {
              $_where = 'status='.$project_status['id'] . ' '.$where;
          }
          ?>
          <div class="col-md-2 col-xs-6 border-right">
            <a href="<?php echo base_url('clients/projects');?>">
              <h3 class="bold"><?php echo total_rows(db_prefix().'projects',$_where); ?></h3>
              <span style="color:<?php echo $project_status['color']; ?>" project-status-<?php echo $project_status['id']; ?>>
                <?php echo $project_status['name']; ?>
              </span>
            </a>
          </div>
      <?php } ?>
    </div>
    <hr class="hr-panel-heading">
    <?php if (is_primary_contact(get_contact_user_id())) {  ?>
        <div class="row">
          <div class="col-md-12">
            <h4 class="no-margin">GAP by status</h4>
          </div>
          <?php
          foreach($gap_statuses as $status){
          $_where = '';
          $_where = 'status_id='.$status['id'].' AND clientid='.$clientid;
          ?>
          <div class="col-md-2 col-xs-6 mbot15 border-right">
            <a href="<?php echo base_url('clients/gaps');?>">
              <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
              <span style="">
                <?php echo $status['status_title']; ?>
              </span>
            </a>
          </div>
          <?php } ?>
        </div>
    <hr class="hr-panel-heading">
    
    <?php } ?> -->
  </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $("#iframe-id").contents().find("img").attr("style","width:100%;height:100%")
    });

    $(".it-health-status").click(function() {
        $('html,body').animate({
            scrollTop: $(".it_health_status_mobile").offset().top},
            'slow');
    });
</script>
<script type="text/javascript">
    // setTimeout(function(event){
    //   const features = "resizable";
    //   event.preventDefault();
    //   event.stopPropagation();
    //   window.open('https://ownewsfeed.hdfcbank.com/', '_blank');
    //   //window.focus();
    // }, 1500)
</script>
<?php $this->load->view('themes/leometric/views/dashboard_js'); ?>