<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-boqs">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('boqs'); ?></h4>
  </div>
</div>
<div class="panel_s">
  <div class="panel-body">
    <table class="table dt-table table-boqs" data-order-col="3" data-order-type="desc">
      <thead>
        <tr>
          <th class="th-boq-number"><?php echo _l('boq') . ' #'; ?></th>
          <th class="th-boq-subject"><?php echo _l('boq_subject'); ?></th>
          <th class="th-boq-total"><?php echo _l('boq_total'); ?></th>
          <th class="th-boq-open-till"><?php echo _l('boq_open_till'); ?></th>
          <th class="th-boq-date"><?php echo _l('boq_date'); ?></th>
          <th class="th-boq-status"><?php echo _l('boq_status'); ?></th>
          <?php
          $custom_fields = get_custom_fields('boq',array('show_on_client_portal'=>1));
          foreach($custom_fields as $field){ ?>
            <th><?php echo $field['name']; ?></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach($boqs as $boq){ ?>
          <tr>
            <td>
              <a href="<?php echo site_url('boq/'.$boq['id'].'/'.$boq['hash']); ?>" class="td-boq-url">
                <?php echo format_boq_number($boq['id']); ?>
                <?php
                if ($boq['invoice_id']) {
                  echo '<br /><span class="text-success boq-invoiced">' . _l('estimate_invoiced') . '</span>';
                }
                ?>
              </a>
              <td>
                <a href="<?php echo site_url('boq/'.$boq['id'].'/'.$boq['hash']); ?>" class="td-boq-url-subject">
                  <?php echo $boq['subject']; ?>
                </a>
                <?php
                if ($boq['invoice_id'] != NULL) {
                  $invoice = $this->invoices_model->get($boq['invoice_id']);
                  echo '<br /><a href="' . site_url('invoice/' . $invoice->id . '/' . $invoice->hash) . '" target="_blank" class="td-boq-invoice-url">' . format_invoice_number($invoice->id) . '</a>';
                } else if ($boq['estimate_id'] != NULL) {
                  $estimate = $this->estimates_model->get($boq['estimate_id']);
                  echo '<br /><a href="' . site_url('estimate/' . $estimate->id . '/' . $estimate->hash) . '" target="_blank" class="td-boq-estimate-url">' . format_estimate_number($estimate->id) . '</a>';
                }
                ?>
              </td>
              <td data-order="<?php echo $boq['total']; ?>">
                <?php
                if ($boq['currency'] != 0) {
                 echo app_format_money($boq['total'], get_currency($boq['currency']));
               } else {
                 echo app_format_money($boq['total'], get_base_currency());
               }
               ?>
             </td>
             <td data-order="<?php echo $boq['open_till']; ?>"><?php echo _d($boq['open_till']); ?></td>
             <td data-order="<?php echo $boq['date']; ?>"><?php echo _d($boq['date']); ?></td>
             <td><?php echo format_boq_status($boq['status']); ?></td>
             <?php foreach($custom_fields as $field){ ?>
               <td><?php echo get_custom_field_value($boq['id'],$field['id'],'boq'); ?></td>
             <?php } ?>
           </tr>
         <?php } ?>
       </tbody>
     </table>
   </div>
 </div>
