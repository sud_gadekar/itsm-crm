<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="row">
    <?php if($service_request->project_id != 0){ ?>
    <div class="col-md-12 single-service_request-project-area">
        <div class="alert alert-info">
            <?php echo _l('service_request_linked_to_project','<a href="'.site_url('clients/project/'.$service_request->project_id).'"><b>'.get_project_name_by_id($service_request->project_id).'</b></a>') ;?>
        </div>
    </div>
    <?php } ?>
    <?php set_service_request_open($service_request->clientread,$service_request->service_requestid,false); ?>
    <?php echo form_hidden('service_request_id',$service_request->service_requestid); ?>
    <div class="col-md-4 service_request-info">
        <div class="panel_s">
            <div class="panel-heading">
                <?php echo _l('clients_single_service_request_information_heading'); ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            #<?php echo $service_request->service_requestid; ?> - <?php echo $service_request->subject; ?>
                        </h4>
                        <hr class="hr-10" />
                        <p>
                            <?php echo _l('clients_service_request_single_department', '<span class="pull-right bold">'.$service_request->department_name.'</span>'); ?>
                        </p>
                        <hr class="hr-10" />
                        <p>
                            <?php echo _l('clients_service_request_single_submitted','<span class="pull-right bold">'._dt($service_request->date).'</span>'); ?>
                        </p>
                        <hr class="hr-10" />
                        <p>
                            <?php echo _l('service_request_dt_submitter'); ?>:
                            <span class="pull-right bold">
                                 <?php echo $service_request->submitter; ?>
                            </span>
                        </p>
                        <hr class="hr-10" />
                        <div class="row">
                            <div class="col-md-4">
                                <?php echo _l('clients_service_request_single_status'); ?>
                            </div>
                            <div class="col-md-8">
                                <div class="service_request-status-inline">
                                    <span class="label pull-right bold" style="background:<?php echo $service_request->statuscolor; ?>">
                                        <?php echo service_request_status_translate($service_request->service_requeststatusid); ?>
                                        <?php if(get_option('allow_customer_to_change_service_request_status') == 1){ ?>
                                        <i class="fa fa-pencil-square-o pointer toggle-change-service_request-status"></i></span>
                                        <?php } ?>
                                    </div>
                                    <?php if(can_change_service_request_status_in_clients_area()){ ?>
                                    <div class="service_request-status hide">
                                        <div class="input-group">
                                            <select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" id="service_request_status_single" class="form-control" name="service_request_status_single">
                                                <?php foreach($service_request_statuses as $status){
                                                    if(!can_change_service_request_status_in_clients_area($status['service_requeststatusid'])){
                                                        continue;
                                                    }
                                                    ?>
                                                    <option value="<?php echo $status['service_requeststatusid']; ?>" <?php if($status['service_requeststatusid'] == $service_request->service_requeststatusid){echo 'selected';}?>>
                                                        <?php echo service_request_status_translate($status['service_requeststatusid']); ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <span class="input-group-addon"><i class="fa fa-remove pointer toggle-change-service_request-status"></i></span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <hr class="hr-10" />
                                <p>
                                    <?php echo _l('clients_service_request_single_priority','<span class="pull-right bold">'.service_request_priority_translate($service_request->priorityid).'</span>'); ?>
                                </p>
                                <?php if(get_option('services') == 1 && !empty($service_request->service_name)){ ?>
                                     <hr class="hr-10" />
                                    <p>
                                        <?php echo _l('service') . ': <span class="pull-right bold">'.$service_request->service_name.'</span>'; ?>
                                    </p>
                                <?php } ?>
                                <?php
                                $custom_fields = get_custom_fields('service_requests',array('show_on_client_portal'=>1));
                                foreach($custom_fields as $field){
                                    $cfValue = get_custom_field_value($service_request->service_requestid,$field['id'],'service_requests');
                                    if(empty($cfValue)) {
                                        continue;
                                    }
                                 ?>
                                <hr class="hr-10" />
                                <p><?php echo $field['name']; ?>: <span class="pull-right bold"><?php echo $cfValue; ?></span></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'service_request-reply')); ?>
                <div class="panel_s single-service_request-reply-area">
                    <div class="panel-heading">
                        <?php echo _l('clients_service_request_single_add_reply_heading'); ?>
                    </div>
                    <div class="panel-body">
                        <textarea name="message" class="form-control" rows="8"></textarea>
                        <?php echo form_error('message'); ?>
                    </div>
                    <div class="panel-footer attachments_area">
                        <div class="row attachments">
                            <div class="attachment">
                                <div class="col-md-6 col-md-offset-3">
                                  <div class="form-group">
                                    <label for="attachment" class="control-label"><?php echo _l('clients_service_request_attachments'); ?></label>
                                    <div class="input-group">
                                     <input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('service_request_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_service_request_form_accepted_mimes(); ?>">
                                     <span class="input-group-btn">
                                        <button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_service_request_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mtop20 mbot20 text-center">
                        <button class="btn btn-info" type="submit" data-form="#service_request-reply" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('service_request_single_add_reply'); ?></button>
                    </div>
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>
        <div class="panel_s">
            <div class="panel-heading">
                <?php echo _l('clients_single_service_request_string'); ?>
            </div>
            <div class="panel-body <?php if($service_request->admin == NULL){echo 'client-reply';} ?>">
                <div class="row">
                    <div class="col-md-3 border-right">
                        <?php if($service_request->admin == NULL || $service_request->admin == 0){ ?>
                        <p><?php echo $service_request->submitter; ?></p>
                        <?php } else { ?>
                        <p><?php echo $service_request->opened_by; ?></p>
                        <p class="text-muted">
                            <?php echo _l('service_request_staff_string'); ?>
                        </p>
                        <?php } ?>
                    </div>
                    <div class="col-md-9">
                        <?php echo check_for_links($service_request->message); ?><br />
                        <p>-----------------------------</p>
                        <?php if(count($service_request->attachments) > 0){
                            echo '<hr />';
                            foreach($service_request->attachments as $attachment){ ?>
                            <?php
                            $path = get_upload_path_by_type('service_request').$service_request->service_requestid.'/'.$attachment['file_name'];
                            $is_image = is_image($path);

                            if($is_image){
                                echo '<div class="preview_image">';
                            }
                            ?>
                            <a href="<?php echo site_url('download/file/service_request/'. $attachment['id']); ?>" class="display-block mbot5">
                                <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i> <?php echo $attachment['file_name']; ?>
                                <?php if($is_image){ ?>
                                <img src="<?php echo site_url('download/preview_image?path='.protected_file_url_by_path($path).'&type='.$attachment['filetype']); ?>" class="mtop5">
                                <?php } ?>
                            </a>
                            <?php if($is_image){
                                echo '</div>';
                            }
                            echo '<hr />';
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <?php foreach($service_request_replies as $reply){ ?>
    <div class="panel_s">
        <div class="panel-body <?php if($reply['admin'] == NULL){echo 'client-reply';} ?>">
            <div class="row">
                <div class="col-md-3 border-right">
                    <p><?php echo $reply['submitter']; ?></p>
                    <p class="text-muted">
                        <?php if($reply['admin'] !== NULL){
                            echo _l('service_request_staff_string');
                        }
                        ?>
                    </p>
                </div>
                <div class="col-md-9">
                    <?php echo check_for_links($reply['message']); ?><br />
                    <p>-----------------------------</p>
                    <?php if(count($reply['attachments']) > 0){
                        echo '<hr />';
                        foreach($reply['attachments'] as $attachment){
                          $path = get_upload_path_by_type('service_request').$service_request->service_requestid.'/'.$attachment['file_name'];
                          $is_image = is_image($path);
                          if($is_image){
                            echo '<div class="preview_image">';
                        }
                        ?>
                        <a href="<?php echo site_url('download/file/service_request/'. $attachment['id']); ?>" class="inline-block mbot5">
                            <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i> <?php echo $attachment['file_name']; ?>
                            <?php if($is_image){ ?>
                            <img src="<?php echo site_url('download/preview_image?path='.protected_file_url_by_path($path).'&type='.$attachment['filetype']); ?>" class="mtop5">
                            <?php } ?>
                        </a>
                        <?php if($is_image){
                            echo '</div>';
                        }
                        echo '<hr />';
                    }
                } ?>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <span><?php echo _l('clients_single_service_request_replied',_dt($reply['date'])); ?></span>
    </div>
</div>
<?php } ?>
</div>

</div>
<?php if(count($service_request_replies) > 1){ ?>
<a href="#top" id="toplink">↑</a>
<a href="#bot" id="botlink">↓</a>
<?php } ?>
