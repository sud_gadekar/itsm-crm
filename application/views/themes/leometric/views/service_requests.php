<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-service_requests">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Service requests'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop service_requests-summary-heading"><?php echo $title_heading; ?></h3>
        <a href="<?php echo site_url('clients/open_service_request'); ?>" class="btn btn-info new-service_request pull-right">
          <?php echo _l('clients_service_request_open_subject'); ?>
        </a>
        <div class="clearfix"></div>
        <hr />
      </div>
      <?php foreach($service_request_statuses as $key => $status){
        $key++;
        $_where = '';
        $where = '';
        if($where == ''){
          $_where = 'status='.$status['service_requeststatusid'].' AND tblservice_requests.userid='.$clientid;
        } else{
          $_where = 'status='.$status['service_requeststatusid'] . ' '.$where.' AND tblservice_requests.userid='.$clientid;
        } ?>
        <div class="col-md-2 col-xs-6 mbot15 border-right list-status">
          <a href="<?php echo site_url('clients/service_requests/').$key;?>">
            <h3 class="bold"><?php echo total_rows(db_prefix().'service_requests',$_where); ?></h3>
            <span style="color:<?php echo $status['statuscolor']; ?>">
              <?php echo service_request_status_translate($status['service_requeststatusid']); ?>
            </span>
          </a>
        </div>
      <?php } ?>
</div>
  <div class="clearfix"></div>
  <hr />
  <div class="clearfix"></div>
  <?php get_template_part('service_requests_table'); ?>
</div>
</div>
</div>
