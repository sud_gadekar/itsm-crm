<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('GAP'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Risks'); ?></h3>
        <!-- <a href="<?php echo site_url('clients/open_ticket'); ?>" class="btn btn-info new-ticket pull-right">
          <?php echo _l('clients_ticket_open_subject'); ?>
        </a> -->
        <div class="clearfix"></div>
        <hr />
      </div>
      <?php foreach($statuses as $status){
          $_where = '';  
          $_where = 'status_id='.$status['id'].' AND '.'clientid='.$session; 
          ?>
          <!-- data-cview="ticket_status_<?php echo $status['ticketstatusid']; ?>" 
               onclick="dt_custom_view('ticket_status_<?php echo $status['ticketstatusid']; ?>','.tickets-table','ticket_status_<?php echo $status['ticketstatusid']; ?>',true); return false;"
                -->
          <div class="col-md-2 col-xs-6 mbot15 border-right list-status">
            <a href="#">
              <h3 class="bold"><?php echo total_rows(db_prefix().'gaps',$_where); ?></h3>
              <span style="">
                <?php echo $status['status_title']; ?>
              </span>
            </a>
          </div>
      <?php } ?>

  </div>
  <div class="clearfix"></div>
  <hr />
  <div class="clearfix"></div>
   <table class="table dt-table scroll-responsive">
      <thead>
        <th><?php echo _l('id'); ?></th>
        <th><?php echo _l('Name'); ?></th>
        <th><?php echo _l('Owner'); ?></th>
        <th><?php echo _l('Category'); ?></th>
        <th><?php echo _l('Impact'); ?></th>
        <th><?php echo _l('Status'); ?></th>
        
      </thead>
      <tbody>
        <?php foreach($gaps as $key => $gap){ ?>
        <tr>
          <td><?php echo $key+1; ?></td>
          <td>
            <?php
            echo $gap['name']?>
          </td>
          
          <!--  <td>
            <a href="<?php echo admin_url('gaps/add/').$gap['id'];?>"  class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
            <a href="<?php echo admin_url('gaps/delete/'.$gap['id']); ?>" class="btn btn-danger btn-icon _delete">
              <i class="fa fa-remove"></i>
            </a>
          </td> -->

          <td><?php echo $gap['firstname'].' '.$gap['lastname']; ?></td>
          <td><?php echo $gap['category_title']?></td>
          <td><?php echo $gap['impact_title']?></td>
          <td><?php echo $gap['status_title']?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
</div>
</div>
