<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- <div class="panel_s section-heading section-tickets">
  <div class="panel-body">
    <h4 class="no-margin section-text"><?php echo _l('Orders'); ?></h4>
  </div>
</div> -->
<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop tickets-summary-heading"><?php echo _l('Sales Order Details'); ?></h3>
        <div class="clearfix"></div>
        <hr />
      </div>
  </div>
  <div class="clearfix"></div>
  <!-- <hr /> -->
  <div class="clearfix"></div>
  <div class="row">
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>SO Number</strong></label>
                            <p><?php echo $order_details->so_number ?></p>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name"><strong>Description</strong></label>
                            <p><?php echo $order_details->description ?></p>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="client_id" class="control-label">
                            <strong><?php echo _l('Customer'); ?>
                            </strong></label>
                            <p><?php echo $order_details->company; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>PO Number</strong></label>
                            <p><?php echo $order_details->po_no; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Order Date</strong></label>
                            <p><?php echo $order_details->order_date; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="items_received"><strong><?php echo _l('Items Received'); ?></strong></label>
                            <p><?php echo $order_details->items_received; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Expected Delivery</strong></label>
                            <p><?php echo $order_details->expected_delivery; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Delivery Date</strong></label>
                            <p><?php echo $order_details->delivery_date; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="items_delivered"><strong><?php echo _l('Items Delivered'); ?></strong></label>
                            <p><?php echo $order_details->items_delivered; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="bill_created"><strong><?php echo _l('Bill Created'); ?></strong></label>
                            <p><?php echo $order_details->bill_created; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="pi_attached"><strong><?php echo _l('PI Attached'); ?></strong></label>
                            <p><?php echo $order_details->pi_attached; ?></p>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="service_request"><strong><?php echo _l('Services Required'); ?></strong></label>
                            <p><?php echo $order_details->service_request; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="sr_id" class="control-label">
                            <strong><?php echo _l('Service Type - SR'); ?>
                            </strong></label>
                            <p><?php echo $order_details->subject; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="project_id" class="control-label">
                            <strong><?php echo _l('Service Type - Project'); ?>
                            </strong></label>
                            <p><?php echo $order_details->project; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="order_completed"><strong><?php echo _l('Order Completed'); ?></strong></label>
                            <p><?php echo $order_details->order_completed; ?></p>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="renewal_required"><strong><?php echo _l('Renewal Required'); ?></strong></label>
                            <p><?php echo $order_details->renewal_required; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="renewal_db_updated"><strong><?php echo _l('Renewal DB Updated'); ?></strong></label>
                            <p><?php echo $order_details->renewal_db_updated; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="sales_staff_id" class="control-label">
                            <strong><?php echo _l('Sales Person'); ?>
                            </strong></label>
                            <p><?php echo $order_details->full_name; ?></p>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group select-placeholder">
                            <label for="invoice_submited"><strong><?php echo _l('Invoice Submitted'); ?></strong></label>
                            <p><?php echo $order_details->invoice_submited; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Invoice No</strong></label>
                            <p><?php echo $order_details->invoice_no; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Amount</strong></label>
                            <p><?php echo $order_details->amount; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Invoice Submission Date</strong></label>
                            <p><?php echo $order_details->inv_submission_date; ?></p>
                        </div>
                    </div>
                    <!--<div class="col-md-8">
                        <div class="form-group">
                            <label for="name"><strong>Invoice Acknolwedged</strong></label>
                            <p><?php echo $order_details->invoice_acknowledge; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Payment Date</strong></label>
                            <p><?php echo $order_details->payment_date; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><strong>Payment Received In (Days)</strong></label>
                            <p><?php echo $order_details->payment_received_in; ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group select-placeholder f_client_id">
                            <label for="status_id" class="control-label">
                            <strong><?php echo _l('Status'); ?>
                            </strong></label>
                            <p><?php echo $order_details->name; ?></p>
                        </div>
                    </div>-->
                  </div>
</div>
        <?php if(isset($order_items)){ ?>
			<div class="panel_s">
				<div class="panel-body">
					<div class="row _buttons">
                     	<div class="col-md-12">
							<h4 class="customer-profile-group-heading">Items</h4>
						</div>
					</div>
                  	<hr class="hr-panel-heading hr-10" />
                  	<div class="clearfix"></div>

					<div class="row">
                     	<div class="col-md-12">
						<table class="table dt-table scroll-responsive">
                            <thead>
                                <th><?php echo _l('#'); ?></th>
                                <th><?php echo _l('Item Name'); ?></th>
                                <th><?php echo _l('Quantity'); ?></th>
                                <th><?php echo _l('Expected Delivery Date'); ?></th>
                                <th><?php echo _l('Delivered'); ?></th>
                                <th><?php echo _l('Delivery Date'); ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($order_items as $key=>$item) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?></td>
                                  <td><?php echo $item['description'] ?></td>
                                  <td><?php echo $item['quantity'] ?></td>
                                  <td><?php echo $item['expected_delivery_date'] ?></td>
                                  <td><?php echo $item['item_delivered'] ?></td>
                                  <td><?php echo $item['delivery_date'] ?></td>
                                </tr>
                                <?php }  ?>
                            </tbody>
                        </table>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

        <?php if(isset($notes)){ ?>
			<div class="panel_s">
				<div class="panel-body">
					<div class="row _buttons">
                     	<div class="col-md-12">
							<h4 class="customer-profile-group-heading">Order Notes</h4>
						</div>
					</div>
                  	<hr class="hr-panel-heading hr-10" />
                  	<div class="clearfix"></div>

					<div class="row">
                     	<div class="col-md-12">
						<table class="table dt-table scroll-responsive">
                            <thead>
                                <th><?php echo _l('#'); ?></th>
                                <th><?php echo _l('Note'); ?></th>
                                <th><?php echo _l('Added On'); ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($notes as $key=>$note) { ?>
                                <tr class="has-row-options">
                                  <td><?php echo $key+1 ?></td>
                                  <td><?php echo $note['order_note'] ?></td>
                                  <td><?php echo $note['created_at'] ?></td>
                                </tr>
                                <?php }  ?>
                            </tbody>
                        </table>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
</div>
</div>

<div class="modal fade" id="logmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Order Details</h4>
        <div class="logDetailsData">
          
        </div>
          
      </div>
    </div>
  </div>
</div> 

<script type="text/javascript">
    function order_details(){                  
      $('#logmodal').modal('show');   
    }
</script>