<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="panel_s">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-success pull-left no-mtop renewals-summary-heading"><?php echo $title; ?></h3>
        <div class="clearfix"></div>
        <hr />
      </div>
  </div>

  <div class="clearfix"></div>
        <table class="table dt-table scroll-responsive">
            <thead>
            <th><?php echo _l('ID'); ?></th>
            <th><?php echo _l('Title'); ?></th>
            <th><?php echo _l('File'); ?></th>
            <th><?php echo _l('Added On'); ?></th>
            
            </thead>
            <tbody>
            <?php if(count($newsletters) > 0){?>

            <?php foreach ($newsletters as $key => $newsletter) { ?>
            <tr class="has-row-options">
                <td><?php echo $newsletter['id']; ?></td>
                <td><?php echo $newsletter['title']; ?></td>
                <td><a href="<?php echo base_url('uploads/newsletters/'.$newsletter['id'].'/'.$newsletter['nl_file']); ?>" target="_blank"><?php echo $newsletter['nl_file']; ?></a></td>
                <td><?php echo date('d-m-Y', strtotime($newsletter['created_at'])); ?></td>
            </tr>
            <?php }  ?>
            <?php } else{ ?>
            <tr class="odd"><td valign="top" colspan="2" class="dataTables_empty">No entries found</td></tr>
            <?php }?>
            </tbody>
        </table>
        </div>
    </div>
</div>
