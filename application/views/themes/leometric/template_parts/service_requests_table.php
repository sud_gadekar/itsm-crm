<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<table class="table dt-table table-service_requests" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
  <thead>
    <th width="10%" class="th-service_request-number"><?php echo _l('clients_service_requests_dt_number'); ?></th>
    <th class="th-service_request-subject"><?php echo _l('clients_service_requests_dt_subject'); ?></th>



    
    <?php if($show_submitter_on_table) { ?>
      <th class="th-service_request-submitter"><?php echo _l('service_request_dt_submitter'); ?></th>
    <?php } ?>
     <th class="th-service_request-approve">
        <?php echo _l('Approve/Reject'); ?>    
      </th>

    <!-- <th class="th-service_request-department"><?php echo _l('clients_service_requests_dt_department'); ?></th> -->
    <th class="th-service_request-project"><?php echo _l('Assigned To'); ?></th>
    
    <?php if(get_option('services') == 1){ ?>
      <th class="th-service_request-service"><?php echo _l('clients_service_requests_dt_service'); ?></th>
    <?php } ?>
    <th class="th-service_request-priority"><?php echo _l('priority'); ?></th>

    <th class="th-service_request-request_for"><?php echo _l('Request For'); ?></th>

    <th class="th-service_request-status"><?php echo _l('clients_service_requests_dt_status'); ?></th>
    <th class="th-service_request-last-reply"><?php echo _l('clients_service_requests_dt_last_reply'); ?></th>

    <th class="th-ticket-created-date"><?php echo _l('Created Date'); ?></th>
    <th class="th-ticket-closed-date"><?php echo _l('Closed Date'); ?></th>


    <?php
    $custom_fields = get_custom_fields('service_requests',array('show_on_client_portal'=>1));
    foreach($custom_fields as $field){ ?>
      <th><?php echo $field['name']; ?></th>
    <?php } ?>
  </thead>
  <tbody>
    <?php foreach($service_requests as $service_request){ ?>
      <tr class="<?php if($service_request['clientread'] == 0){echo '';} ?>">
        <td data-order="<?php echo $service_request['service_requestid']; ?>">
          <a href="<?php echo site_url('clients/service_request/'.$service_request['service_requestid']); ?>">
            #<?php echo $service_request['service_requestid']; ?>
          </a>
        </td>
        <td>
          <a href="<?php echo site_url('clients/service_request/'.$service_request['service_requestid']); ?>">
            <?php echo $service_request['subject']; ?>
            <div class="row-options">
                <a href="<?php echo site_url('clients/sr_convert_to_pdf/'.$service_request['service_requestid']); ?>">
                  <span class="text-dark"> | SR Report | </span>
                </a>
              </div>
          </a>
        </td>
        <?php if($show_submitter_on_table) { ?>
          <td>
            <?php echo $service_request['user_firstname'] . ' ' . $service_request['user_lastname'];  ?>
          </td>
        <?php } ?>

        <?php if ($session == $service_request['approval_assigned_to_staffid'] && is_null($service_request['is_approved'])){ ?>

                  <td>
                    <a title="Approve" href="<?php echo site_url('clients/approve/'.$service_request['service_requestid']); ?>"> 
                         <i class="fa fa-check text-success"></i>
                    </a> |  
                    <a title="Reject" href="<?php echo site_url('clients/reject/'.$service_request['service_requestid']); ?>">
                         <i class="fa fa-times text-danger"></i>
                    </a> 
                  </td>
                <?php } else {  ?>
                    <td>
                       <?php if($service_request['is_approved'] == NULL) { echo '-'; }?>    
                       <?php if($service_request['is_approved'] == 0 && $service_request['is_approved'] != NULL) { echo 'Rejected'; }?>   
                       <?php if($service_request['is_approved'] == 1 && $service_request['is_approved'] != NULL) { echo 'Approved'; }?>   
                    </td>
                <?php } ?> 
       <!--  <td>
          <?php echo $service_request['department_name']; ?>
        </td> -->
        

        
         <td>
          <?php if($service_request['assigned'] != 0) { ?>
          <?php echo $this->db->where('staffid',$service_request['assigned'])->get('tblstaff')->row()->firstname.' '.
                      $this->db->where('staffid',$service_request['assigned'])->get('tblstaff')->row()->lastname; ?>
          <?php } ?>
        </td>
        <?php if(get_option('services') == 1){ ?>
          <td>
            <?php echo $service_request['service_name']; ?>
          </td>
        <?php } ?>
        <td>
          <?php
          echo service_request_priority_translate($service_request['priority']);
          ?>
        </td>
        <td><?php if(isset($service_request['request_for'])){
                  echo $this->db->where('requestid',$service_request['request_for'])
                                ->get('tbltickets_request_for')->row()->name;
            } ?>
        </td>
        <td>
          <span class="label inline-block" style="background:<?php echo $service_request['statuscolor']; ?>">
            <?php echo service_request_status_translate($service_request['service_requeststatusid']); ?></span>
          </td>
          <td data-order="<?php echo $service_request['lastreply']; ?>">
            <?php
            if ($service_request['lastreply'] == NULL) {
             echo _l('client_no_reply');
           } else {
             echo _dt($service_request['lastreply']);
           }
           ?>
         </td>


          <td>
          <span>
            <?php echo $service_request['date']; ?></span>
        </td>
        <td>
          <span>
            <?php 
                $service_request_closed = $this->db->where('service_requestid',$service_request['service_requestid'])
                                                  ->where('status',5)->select('datetime')
                                                  ->get('tblservice_request_status_logs')->row();
                if(isset($service_request_closed)){
                  echo $service_request_closed->datetime; 
                } else {
                  echo ''; 
                }  
            ?></span>
        </td>

         <?php foreach($custom_fields as $field){ ?>
          <td>
            <?php echo get_custom_field_value($service_request['service_requestid'],$field['id'],'service_requests'); ?>
          </td>
        <?php } ?>
      </tr>
    <?php } ?>
  </tbody>
</table>
