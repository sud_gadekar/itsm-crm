<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<table class="table table-tickets table-all_tickets-report scroll-responsive" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
  <thead>
    <th width="10%" class="th-ticket-number"><?php echo _l('Ticket No'); ?></th>
    <th class="th-ticket-subject"><?php echo _l('Subject'); ?></th>
    <th class="th-ticket-submitter"><?php echo _l('Contact'); ?></th>
    <th class="th-ticket-project"><?php echo _l('Assigned To'); ?></th>
    <th class="th-ticket-priority"><?php echo _l('Priority'); ?></th>
    <th class="th-ticket-status"><?php echo _l('Status'); ?></th>
    <?php if(get_option('services') == 1){ ?>
      <th class="th-ticket-service"><?php echo _l('Service'); ?></th>
    <?php } ?>
    <th class="th-ticket-status"><?php echo _l('Request For'); ?></th>
    <th class="th-ticket-created-date"><?php echo _l('Created Date'); ?></th>
    <th class="th-ticket-last-reply"><?php echo _l('Last Reply'); ?></th>
    <th class="th-ticket-closed-date"><?php echo _l('Closed Date'); ?></th>
  </thead>
  <tbody>
    <?php foreach($support_tickets as $ticket){ ?>
      <tr class="has-row-options <?php if($ticket['clientread'] == 0){echo '';} ?>">
        <td data-order="<?php echo $ticket['ticketid']; ?>">
          <a href="<?php echo site_url('clients/ticket/'.$ticket['ticketid']); ?>">IN<?php echo $ticket['ticketid']; ?></a>
        </td>
        <td>
          <a href="<?php echo site_url('clients/ticket/'.$ticket['ticketid']); ?>"><?php echo $ticket['subject']; ?></a>
          <div class="row-options">
            <a href="<?php echo site_url('clients/incident_convert_to_pdf/'.$ticket['ticketid']); ?>"><span class="text-dark">| SR Report |</span></a>
          </div>
        </td>
        <td><?php echo $ticket['user_firstname'] . ' ' . $ticket['user_lastname'];  ?></td>
        <td>
          <?php if($ticket['assigned'] != 0) { ?>
          <?php echo $this->db->where('staffid',$ticket['assigned'])->get('tblstaff')->row()->firstname.' '.
                      $this->db->where('staffid',$ticket['assigned'])->get('tblstaff')->row()->lastname; ?>
          <?php } ?>
        </td>
        <td><?php echo ticket_priority_translate($ticket['priority']); ?></td>
        <td>
          <span class="label inline-block" style="background:<?php echo $ticket['statuscolor']; ?>"><?php echo ticket_status_translate($ticket['ticketstatusid']); ?></span>
        </td>
        <?php if(get_option('services') == 1){ ?>
        <td><?php echo $ticket['service_name']; ?></td>
        <?php } ?>
        <td></td>
        <td><span><?php echo $ticket['date']; ?></span></td>
        <td data-order="<?php echo $ticket['lastreply']; ?>">
          <?php if ($ticket['lastreply'] == NULL) {
              echo _l('client_no_reply');
            } else {
              echo _dt($ticket['lastreply']);
            }
          ?>
        </td>
        <td>
          <span>
            <?php 
                $ticket_closed = $this->db->where('ticketid',$ticket['ticketid'])->where('status',5)->select('datetime')->get('tblticket_status_logs')->row();
                if(isset($ticket_closed)){
                  echo $ticket_closed->datetime; 
                } else {
                  echo ''; 
                }  
            ?>
          </span>
        </td>
      </tr>
    <?php } ?>
    <?php foreach($service_tickets as $service_request){ ?>
      <tr class="has-row-options <?php if($service_request['clientread'] == 0){echo '';} ?>">
        <td data-order="<?php echo $service_request['service_requestid']; ?>">
          <a href="<?php echo site_url('clients/service_request/'.$service_request['service_requestid']); ?>">
            SR<?php echo $service_request['service_requestid']; ?>
          </a>
        </td>
        <td>
          <a href="<?php echo site_url('clients/service_request/'.$service_request['service_requestid']); ?>">
            <?php echo $service_request['subject']; ?>
            <div class="row-options">
                <a href="<?php echo site_url('clients/sr_convert_to_pdf/'.$service_request['service_requestid']); ?>">
                  <span class="text-dark"> | SR Report | </span>
                </a>
              </div>
          </a>
        </td>
        <td><?php echo $service_request['user_firstname'] . ' ' . $service_request['user_lastname'];  ?></td>
        <td>
          <?php if($service_request['assigned'] != 0) { ?>
          <?php echo $this->db->where('staffid',$service_request['assigned'])->get('tblstaff')->row()->firstname.' '.
                      $this->db->where('staffid',$service_request['assigned'])->get('tblstaff')->row()->lastname; ?>
          <?php } ?>
        </td>
        <td><?php echo service_request_priority_translate($service_request['priority']); ?></td>
        <td>
          <span class="label inline-block" style="background:<?php echo $service_request['statuscolor']; ?>"><?php echo service_request_status_translate($service_request['service_requeststatusid']); ?></span>
        </td>

        <?php if(get_option('services') == 1){ ?>
        <td><?php echo $service_request['service_name']; ?></td>
        <?php } ?>
        <td><?php echo $service_request['request_for_name']; ?></td>
        <td><span><?php echo $service_request['date']; ?></span></td>
        <td data-order="<?php echo $service_request['lastreply']; ?>">
          <?php if ($service_request['lastreply'] == NULL) {
            echo _l('client_no_reply');
           } else {
            echo _dt($service_request['lastreply']);
           }
          ?>
        </td>
        <td>
          <span>
            <?php 
                $service_request_closed = $this->db->where('service_requestid',$service_request['service_requestid'])
                                                  ->where('status',5)->select('datetime')
                                                  ->get('tblservice_request_status_logs')->row();
                if(isset($service_request_closed)){
                  echo $service_request_closed->datetime; 
                } else {
                  echo ''; 
                }  
            ?></span>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
