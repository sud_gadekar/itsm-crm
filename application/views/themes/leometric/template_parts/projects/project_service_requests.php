<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<a href="<?php echo site_url('clients/open_service_request?project_id='.$project->id); ?>" class="btn btn-info mbot15">
	<?php echo _l('clients_service_request_open_subject'); ?>
</a>
<?php get_template_part('service_requests_table'); ?>
