<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<table class="table dt-table table-tickets" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
  <thead>
    <th><?php echo _l('Sr.No.'); ?></th>
    <th><?php echo _l('Contact'); ?></th>
    <th><?php echo _l('Name'); ?></th>
    <th><?php echo _l('Type'); ?></th>
    <th><?php echo _l('Vendor'); ?></th>
    <th><?php echo _l('Start Date'); ?></th>
    <th><?php echo _l('End Date'); ?></th>
  </thead>
  <tbody>
    <?php foreach($renewals as $key => $renewal){ ?>
      <tr>  <!-- class="<?php if($renewal['clientread'] == 0){echo 'text-danger';} ?>" -->
        <td data-order="<?php echo $renewal['id']; ?>">
          <!-- <a href="<?php echo site_url('clients/renewal/'.$renewal['id']); ?>"> -->
            <a href="#">
            <?php echo $key+1; ?>
          </a>
        </td>
      <td><?php echo $renewal['company']; ?></td>
      <td><?php echo $renewal['name'] ?></td>
      <td><?php echo $renewal['type_name'] ?></td>
      <td><?php echo $renewal['vendor_name'] ?></td>
      <td><?php echo $renewal['start_date'] ?></td>
      <td><?php echo $renewal['end_date'] ?></td>
      
      </tr>
    <?php } ?>
  </tbody>
</table>
