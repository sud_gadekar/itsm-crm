<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css">
.hide-menu {
padding: 18px 14px 18px 14px;
font-size: 14px;
float: left;
color: #d0d0d0;
cursor: pointer;
line-height: 27px;
}
aside a i.menu-icon {
    margin-right: 10px !important;  
}
@media (max-width: 768px)
.hide-menu {
margin-right: 5px;
}

.hide-menu:hover {
opacity: .7;
}

#side-menu li a{
  text-transform: initial;
}

@media (max-width: 768px){
  #main{
    margin-left: 0 !important;
  }
  #mySidebar{
    /*//width: 0 !important;*/
    /*overflow: auto;*/
  }

}
@media (min-width: 769px){
  .main{
    margin-left: 250px !important;
  }
}


@media (max-width: 480px){
  .hide-menu-mobile{
    display: none !important;
  }
}

li{
    cursor: pointer;
    padding: 8px;
    
}
.navbar-brand {
  float: left;
  height: 50px;
  padding: 0px !important;
  font-size: 18px;
  line-height: 20px;
}
.bottom-line > li{
  border-bottom: 1px solid grey;
}
.bottom-line > li:last-child {
  border-bottom: none;
}
li > img {
  height: 32px;
    width: 32px;
    border-radius: 50%;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: -24px;
    margin-top: 8px;
}
</style>

<div id="header" style="margin-left: 250px;height: 110px">
  <!--<nav class="navbar navbar-default header admin" style="margin-bottom: 0 !important;height: 110px">-->
    <div id="navbar1" class="container" style="width: 100% !important;padding-left: 0px;" style="background-color: var(background-color);">
     
      <div class="collapse navbar-collapse" id="theme-navbar-collapse"> 
        <ul class="nav navbar-nav navbar-right">
          <?php //hooks()->do_action('customers_navigation_start'); ?>
          <?php //foreach($menu as $item_id => $item) { ?>
          <!--<li class="customers-nav-item-<?php echo $item_id; ?>"
            <?php echo _attributes_to_string(isset($item['li_attributes']) ? $item['li_attributes'] : []); ?>>
            <a style="color:white;" href="<?php echo $item['href']; ?>"
              <?php echo _attributes_to_string(isset($item['href_attributes']) ? $item['href_attributes'] : []); ?>>
              <?php
              if(!empty($item['icon'])){
              echo '<i class="'.$item['icon'].'"></i> ';
              }
              echo $item['name'];
              ?>
            </a>
          </li>-->
          <?php //} ?>
          <?php //hooks()->do_action('customers_navigation_end'); ?>

          <?php if(!is_client_logged_in()) { ?>
            <!--<li class="customers-nav-item-login">
              <a href="<?php echo site_url('/authentication/guest_enquiry'); ?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Guest Enquiry</a>
            </li>-->
          <?php } ?>
          
          <?php if(is_client_logged_in()) { ?>
          <li class="dropdown customers-nav-item-contact">
            <a href="#" class="dropdown-toggle hide-menu-mobile"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">        
              <img src="<?php echo site_url('/assets/images/contact.png'); ?>" data-toggle="tooltip" data-title="Contact Details" data-placement="bottom" style="max-width: 33px;"></a>
            <ul class="dropdown-menu animated fadeIn">
              <li class="customers-nav-item-edit-profile">
                <a href="#">
                  Email : itsm@decodingit.com<br><br>
                  Websites: <br>
                  www.decodingit.com<br>
                  www.powermyit.com<br><br>
                  Helpline:<?php 
                  $userid = $this->session->userdata('client_user_id'); 
                  if(isset($userid))
                  {
                        $country = $this->db->where('userid',$userid)->get('tblclients')->row()->country;
                        // if($country == 166)
                        // {
                        //   echo " +968 22844786";
                        // }
                        // elseif ($country == 234) 
                        // {
                        //   echo " +968 22844786";
                        // }
                        // elseif ($country == 102) 
                        // {
                        //   echo " +968 22844786";
                        // }
                        // else{
                        //   echo "";
                        // }
                        echo " +968 22844786";
                  }
                  else
                  {
                    echo " +968 22844786";
                  }
                 
                  ?>

                </a>
              </li>
            </ul>
          </li>
          <li data-toggle="tooltip" title="<?php echo _l('Click to Chat and Talk'); ?>">
              <a target="_blank" href="<?php echo get_option('chat_url') ?>" class="hide-menu-mobile">
              <img src="<?php echo site_url('/assets/images/chat.png'); ?>"
                data-toggle="tooltip"
                data-title="Click to Chat and Talk"
                data-placement="bottom"
                class="client-profile-image-small">
              </a>
          </li>
          <li class="dropdown notifications-wrapper header-notifications" data-toggle="tooltip" title="<?php echo _l('nav_notifications'); ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo site_url('/assets/images/notification.png'); ?>" data-toggle="tooltip" data-title="Notifications" data-placement="bottom" style="max-width: 33px;">
            </a>
            <ul class="dropdown-menu animated fadeIn">
              <?php if(!empty($notifications)){ 
                      foreach($notifications as $notification){ ?>
                        <li class="customers-nav-item-edit-profile">
                          <a href="<?php echo site_url($notification['link']); ?>">
                            <?php echo $notification['description']; ?></br>
                            <span style="font-size: 10px; font-weight: 700;"><?php echo $notification['date']; ?><span>
                          </a>
                        </li>
                      <?php } ?>
                <li class="text-center"><a href="<?php echo site_url('/clients/notifications'); ?>">View all notifications</a></li>
                <?php }  ?>
              </ul>
          </li>
          <li class="dropdown customers-nav-item-profile">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo contact_profile_image_url($contact->id,'thumb'); ?>" data-toggle="tooltip" data-title="<?php echo html_escape($contact->firstname . ' ' .$contact->lastname); ?>" data-placement="bottom" style="max-width: 33px;">
            </a>
            <ul class="dropdown-menu animated fadeIn">
              <li class="customers-nav-item-edit-profile">
                <a href="<?php echo site_url('clients/profile'); ?>">
                  <?php echo _l('clients_nav_profile'); ?>
                </a>
              </li>
              <?php if($contact->is_primary == 1){ ?>
              <?php if(can_loggged_in_user_manage_contacts()) { ?>
              <li class="customers-nav-item-edit-profile">
                <a href="<?php echo site_url('contacts'); ?>">
                  <?php echo _l('clients_nav_contacts'); ?>
                </a>
              </li>
              <?php } ?>
              <li class="customers-nav-item-company-info">
                <a href="<?php echo site_url('clients/company'); ?>">
                  <?php echo _l('client_company_info'); ?>
                </a>
              </li>
              <?php } ?>
              <?php if(can_logged_in_contact_update_credit_card()){ ?>
              <li class="customers-nav-item-stripe-card">
                <a href="<?php echo site_url('clients/credit_card'); ?>">
                  <?php echo _l('credit_card'); ?>
                </a>
              </li>
              <?php } ?>
              <?php if (is_gdpr() && get_option('show_gdpr_in_customers_menu') == '1') { ?>
              <li class="customers-nav-item-announcements">
                <a href="<?php echo site_url('clients/gdpr'); ?>">
                  <?php echo _l('gdpr_short'); ?>
                </a>
              </li>
              <?php } ?>
              <li class="customers-nav-item-announcements">
                <a href="<?php echo site_url('clients/announcements'); ?>">
                  <?php echo _l('announcements'); ?>
                  <?php if($total_undismissed_announcements != 0){ ?>
                  <span class="badge"><?php echo $total_undismissed_announcements; ?></span>
                  <?php } ?>
                </a>
              </li>
              <?php if(!is_language_disabled()) {
              ?>
              <li class="dropdown-submenu pull-left customers-nav-item-languages">
                <a href="#" tabindex="-1">
                  <?php echo _l('language'); ?>
                </a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class="<?php if(get_contact_language() == ""){echo 'active';} ?>">
                    <a href="<?php echo site_url('clients/change_language'); ?>">
                      <?php echo _l('system_default_string'); ?>
                    </a>
                  </li>
                  <?php foreach($this->app->get_available_languages() as $user_lang) { ?>
                  <li <?php if(get_contact_language() == $user_lang){echo 'class="active"';} ?>>
                    <a href="<?php echo site_url('clients/change_language/'.$user_lang); ?>">
                      <?php echo ucfirst($user_lang); ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </li>
              <?php } ?>
              <?php
              /**
              * Don't show calendar for invoices, estimates, proposals etc.. views where no navigation is included or in kb area
              */
              if(is_client_logged_in() && $subMenuEnabled && !isset($knowledge_base_search)){ ?>
              
              <?php hooks()->do_action('before_customers_area_sub_menu_start'); ?>
              <li class="customers-top-submenu-files"><a href="<?php echo site_url('clients/files'); ?>"><?php echo _l('IT Policies'); ?></a></li>
              <li class="customers-top-submenu-calendar"><a href="<?php echo site_url('clients/calendar'); ?>"><?php echo _l('calendar'); ?></a></li>
              <?php hooks()->do_action('after_customers_area_sub_menu_end'); ?>
              
              <?php } ?>
              <li class="customers-nav-item-logout">
                <a href="<?php echo site_url('authentication/logout'); ?>">
                  <?php echo _l('clients_nav_logout'); ?>
                </a>
              </li>
            </ul>
          </li>
          <?php } ?>
            
          <?php hooks()->do_action('customers_navigation_after_profile'); ?>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </div>
  <!-- //SideBar -->
  <div class="navbar-header" style="background-color: white;width: 250px;top:-50">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#theme-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <div style="position: absolute;top: 0px;font-size: 18px;">
          <?php get_company_logo('','navbar-brand logo'); ?>  
        </div>
        
      </div>
  <div id="wrapper" class="admin">
    <?php if(is_client_logged_in()){ ?>
    <div id="mySidebar" class="sidebar" style="width: 250px;">
      <aside id="side-menu" >
			<nav id="column_left">	
				<ul class="nav nav-list bottom-line">
					<li>
            <a style="pointer-events: none;font-weight: 500" href="#" onclick="return false">
              <i class="fa fa-user menu-icon"></i>
              <span>Welcome <?php echo $contact->firstname . ' ' .$contact->lastname?></span>
            </a>
          </li>	
          <li>
            <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/dashboard');?>">
              <i class="fa fa-home menu-icon"></i>
              <span>Dashboard</span>
            </a>
          </li>
					<li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#knowmyit">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
              <span class="nav-header-primary">Know My IT <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="knowmyit">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/inventories');?>" style="padding-left: 3em;">
                  <i class="fa fa-book menu-icon"></i>
                  <span>Inventory</span>
                </a>
              </li>
              <li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/renewals');?>" style="padding-left: 3em;">
                  <i class="fa fa-retweet menu-icon"></i>
                  <span>Software & License</span>
                </a>
							</li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/assets');?>" style="padding-left: 3em;">
                  <i class="fa fa-globe menu-icon"></i>
                  <span>Network & Servers Overview</span>
                </a>
              </li>
              <li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/backups');?>" style="padding-left: 3em;">
                  <i class="fa fa-hdd-o menu-icon"></i>
                  <span>Backup Logs</span>
                </a>
							</li>
              <li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/dashboard');?>" style="padding-left: 3em;">
                  <i class="fa fa-hdd-o menu-icon"></i>
                  <span>IT Health Status</span>
                </a>
							</li>
						</ul>
					</li>
					<li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#managemyit">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
							<span class="nav-header-primary">Manage My IT <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="managemyit">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/tickets');?>" style="padding-left: 3em;">
                  <i class="fa fa-ticket menu-icon"></i>
                  <span>Support Request</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/service_requests');?>" style="padding-left: 3em;">
                  <i class="fa fa-wrench menu-icon"></i>
                  <span>Service Requests</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/service_request_approvals');?>" style="padding-left: 3em;">
                  <i class="fa fa-wrench menu-icon"></i>
                  <span>SR Approvals</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/outages');?>" style="padding-left: 3em;">
                  <i class="fa fa-ticket menu-icon"></i>
                  <span>Outages</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/projects');?>" style="padding-left: 3em;">
                  <i class="fa fa-ticket menu-icon"></i>
                  <span>Projects</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/checklists');?>" style="padding-left: 3em;">
                  <i class="fa fa-heartbeat menu-icon"></i>
                  <span>Checklist</span>
                </a>
              </li>
						</ul>
					</li>
					<li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#improvemyit">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
							<span class="nav-header-primary">Improve My IT <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="improvemyit">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/gaps');?>" style="padding-left: 3em;">
                  <i class="fa fa-exchange menu-icon"></i>
                  <span>Risks</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('knowledge-base');?>" style="padding-left: 3em;">
                  <i class="fa fa-folder-open-o menu-icon"></i>
                  <span>Knowledge Base</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/files');?>" style="padding-left: 3em;">
                  <i class="fa fa-folder-open-o menu-icon"></i>
                  <span>IT Reports</span>
                </a>
              </li>
						</ul>
					</li>
					<li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#ordersandcontracts">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
							<span class="nav-header-primary">Order Tracking <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="ordersandcontracts">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/orders');?>" style="padding-left: 3em;">
                  <i class="fa fa-first-order menu-icon"></i>
                  <span>Sales Orders</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/contracts');?>" style="padding-left: 3em;">
                  <i class="fa fa-file menu-icon"></i>
                  <span>Contracts</span>
                </a>
              </li>
						</ul>
          </li>
          <li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#resources">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
							<span class="nav-header-primary">Resources <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="resources">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/newsletters');?>" style="padding-left: 3em;">
                  <i class="fa fa-newspaper-o menu-icon"></i>
                  <span>Tech Trendz Newsletters</span>
                </a>
              </li>
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/educational_contents');?>" style="padding-left: 3em;">
                <i class="fa fa-mortar-board menu-icon"></i>
                <span>Tech Aware Guides</span>
              </a>
              </li>
              <li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('knowledge-base');?>" style="padding-left: 3em;">
                  <i class="fa fa-folder-open-o menu-icon"></i>
                  <span>Knowledge Base</span>
                </a>
              </li>
						</ul>
          </li>
          <li>
            <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/announcements');?>">
              <i class="fa fa-bullhorn menu-icon"></i>
              <span>Announcements</span>
            </a>
          </li>
          <li>
						<a class="side-menu_anchor_hover" data-toggle="collapse" data-target="#reports">
              <i class="fa fa-chevron-circle-right menu-icon"></i>
							<span class="nav-header-primary">Reports <span class="pull-right"><b class="caret"></b></span></span>
						</a>
						<ul class="nav nav-list collapse" id="reports">
							<li>
                <a class="side-menu_anchor_hover" href="<?php echo site_url('clients/ticket_reports');?>" style="padding-left: 3em;">
                  <i class="fa fa-ticket menu-icon"></i>
                  <span>Tickets</span>
                </a>
              </li>
						</ul>
          </li>
				</ul>
			</nav>
      </aside>
    </div>
    <?php } ?>
  </div>
  <script type="text/javascript">

  
    function openNav() {

  
    if ($(window).width() > 768) {
      if($("#mySidebar").width() == 0 )
      {
        $('.sidebar').css({ overflow: ''});
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";

      }
      else
      {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        $('.sidebar').css({ overflow: 'hidden'});
       
      }
    }
    else 
    {
       if($("#mySidebar").width() == 0 )
      {
        $('.sidebar').css({ overflow: ''});
        document.getElementById("mySidebar").style.width = "250px";
        
        document.getElementById("main").style.marginLeft = "0px";

      }
      else
      {
        document.getElementById("mySidebar").style.width = "0px";

        document.getElementById("main").style.marginLeft= "0";
        $('#mySidebar').css({ overflow: 'hidden'});
       
      } 
    }
      
    }

     $(document).ready(function(){

      if ($(window).width() < 768) 
      {
        document.getElementById("mySidebar").style.width = "0px";
        $('#mySidebar').css({ overflow: 'hidden'});
      }

      /* if($(window).width() > 768)
      {
        var height = document.getElementById('content').offsetHeight;
        console.log(height);
        //document.getElementById("").style.height = height;
        if(height < 768)
        {
          document.getElementById('side-menu').setAttribute("style","height:100vh");
        }
        else
        {
           document.getElementById('side-menu').setAttribute("style","height:"+height+"px");
        }
      }
       */
    })

   
  </script>