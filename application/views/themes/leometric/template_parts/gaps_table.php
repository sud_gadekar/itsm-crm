<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<table class="table dt-table scroll-responsive" data-order-col="<?php echo (get_option('services') == 1 ? 7 : 6); ?>" data-order-type="desc">
  <thead>
    <th width="10%" class="th-gap-number"><?php echo _l('id'); ?></th>
    <th class="th-gap-subject"><?php echo _l('Name'); ?></th>
    <th class="th-gap-subject"><?php echo _l('Owner'); ?></th>
    <th class="th-gap-subject"><?php echo _l('Category'); ?></th>
    <th class="th-gap-subject"><?php echo _l('Impact'); ?></th>
    <th class="th-gap-subject"><?php echo _l('Status'); ?></th>
   
    <?php
    $custom_fields = get_custom_fields('gaps',array('show_on_client_portal'=>1));
    foreach($custom_fields as $field){ ?>
      <th><?php echo $field['name']; ?></th>
    <?php } ?>
  </thead>
  <tbody>
    <?php foreach($gaps as $gap){ ?>
      <tr>  <!-- class="<?php if($gap['clientread'] == 0){echo 'text-danger';} ?>" -->
        <td data-order="<?php echo $gap['id']; ?>">
          <!-- <a href="<?php echo site_url('clients/gap/'.$gap['id']); ?>"> -->
            <a href="#">
            <?php echo $gap['id']; ?>
          </a>
        </td>
        <td><?php echo $gap['name']; ?> </td>
        <td> <?php echo $gap['firstname'].' '.$gap['lastname']; ?> </td>
        <td> <?php echo $gap['category_title']; ?> </td>
        <td> <?php echo $gap['impact_title']; ?> </td>
        <td> <?php echo $gap['status_title']; ?> </td>
         <?php foreach($custom_fields as $field){ ?>
          <td>
            <?php echo get_custom_field_value($gap['gapid'],$field['id'],'gaps'); ?>
          </td>
        <?php } ?>
      </tr>
    <?php } ?>
  </tbody>
</table>
