<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backups_model extends App_Model
{
    private $piping = false;

    public function __construct()
    {
        parent::__construct(); 
    }

    
    public function get($id = '', $contact_id = '', $client_id = '', $where = [])
    {
        $this->db->select('tblbackups.*, tblclients.company, aa.product_name as first_backup_destination, bb.product_name as second_backup_destination, cc.product_name as third_backup_destination')
                ->from(db_prefix() . 'backups')
                ->join(db_prefix() . 'clients','tblbackups.client_id = tblclients.userid', 'left')
                ->join(db_prefix() . 'inventory_details a','tblbackups.first_backup_destination = a.id', 'left')
                ->join(db_prefix() . 'inventory_products aa','a.product_name = aa.id', 'left')
                ->join(db_prefix() . 'inventory_details b','tblbackups.second_backup_destination = b.id', 'left')
                ->join(db_prefix() . 'inventory_products bb','b.product_name = bb.id', 'left')
                ->join(db_prefix() . 'inventory_details c','tblbackups.third_backup_destination = c.id', 'left')
                ->join(db_prefix() . 'inventory_products cc','c.product_name = cc.id', 'left')
                ->where($where);
        if ($id) {
            $this->db->where('tblbackups.id', $id);
        }
        if ($client_id) {
            $this->db->where('tblbackups.client_id', $client_id);
        }
        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('tblbackups.*, tblclients.company')
                ->from(db_prefix() . 'backups')
                ->join(db_prefix() . 'clients','tblbackups.client_id = tblclients.userid')
                // ->join(db_prefix() . 'inventory_details','tblbackups.server = tblinventory_details.id')
                // ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('tblbackups.client_id',$id);

        return $this->db->get()->result_array();
    }

    /**
     * Add new gap
     * @param mixed $data All $_POST data
     * @return  mixed
     */
    public function add($data)
    {
        $this->db->insert(db_prefix() . 'backups', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New backup Added [' . $insert_id . ']');
            return $insert_id;
        }

        return false;
    }

    //categories
    /**
     * Get gap category by id
     * @param  mixed $id category id
     * @return mixed     if id passed return object else array
     */
    public function get_log($id = '')
    {
        if (is_numeric($id)) {
            return $this->db->select('tblbackup_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblbackups.policy_title, tblinventory_products.product_name')
                ->from(db_prefix() . 'backup_logs')
                ->join(db_prefix() . 'backups','tblbackup_logs.backup_id = tblbackups.id')
                
                ->join(db_prefix() . 'inventory_details','tblbackup_logs.server = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
            ->where('id', $id)->row();
        }

        return $this->db->select('tblbackup_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblbackups.policy_title, tblinventory_products.product_name')
                ->from(db_prefix() . 'backup_logs')
                ->join(db_prefix() . 'backups','tblbackup_logs.backup_id = tblbackups.id')
                ->join(db_prefix() . 'inventory_details','tblbackup_logs.server = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->get()->result_array();
    }

    public function backup_logs($id = '')
    {
        if ($id) {
            return $this->db->select('tblbackup_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblbackups.policy_title, tblinventory_products.product_name')
                ->from(db_prefix() . 'backup_logs')
                ->join(db_prefix() . 'backups','tblbackup_logs.backup_id = tblbackups.id')
                
                ->join(db_prefix() . 'inventory_details','tblbackup_logs.server = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('tblbackup_logs.backup_id', $id)->get()->result_array();
        }
    }
}