<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Service_requests_model extends App_Model
{
    private $piping = false;

    public function __construct()
    {
        parent::__construct(); 
    }

    public function insert_piped_service_request($data)
    {
        $data = hooks()->apply_filters('piped_service_request_data', $data);

        $this->piping = true;
        $attachments  = $data['attachments'];
        $subject      = $data['subject'];
        // Prevent insert service_request to database if mail delivery error happen
        // This will stop createing a thousand service_requests
        $system_blocked_subjects = [
            'Mail delivery failed',
            'failure notice',
            'Returned mail: see transcript for details',
            'Undelivered Mail Returned to Sender',
            ];

        $subject_blocked = false;

        foreach ($system_blocked_subjects as $sb) {
            if (strpos('x' . $subject, $sb) !== false) {
                $subject_blocked = true;

                break;
            }
        }

        if ($subject_blocked == true) {
            return;
        }

        $message = $data['body'];
        $name    = $data['fromname'];

        $email   = $data['email'];
        $to      = $data['to'];
        $subject = $subject;
        $message = $message;

        $this->load->model('spam_filters_model');
        $mailstatus = $this->spam_filters_model->check($email, $subject, $message, 'service_requests');

        // No spam found
        if (!$mailstatus) {
            $pos = strpos($subject, '[service_request ID: ');
            if ($pos === false) {
            } else {
                $tid = substr($subject, $pos + 12);
                $tid = substr($tid, 0, strpos($tid, ']'));
                $this->db->where('service_requestid', $tid);
                $data = $this->db->get(db_prefix() . 'service_requests')->row();
                $tid  = $data->service_requestid;
            }
            $to            = trim($to);
            $toemails      = explode(',', $to);
            $department_id = false;
            $userid        = false;
            foreach ($toemails as $toemail) {
                if (!$department_id) {
                    $this->db->where('email', trim($toemail));
                    $data = $this->db->get(db_prefix() . 'departments')->row();
                    if ($data) {
                        $department_id = $data->departmentid;
                        $to            = $data->email;
                    }
                }
            }
            if (!$department_id) {
                $mailstatus = 'Department Not Found';
            } else {
                if ($to == $email) {
                    $mailstatus = 'Blocked Potential Email Loop';
                } else {
                    $message = trim($message);
                    $this->db->where('active', 1);
                    $this->db->where('email', $email);
                    $result = $this->db->get(db_prefix() . 'staff')->row();
                    if ($result) {
                        if ($tid) {
                            $data            = [];
                            $data['message'] = $message;
                            $data['status']  = get_option('default_service_request_reply_status');

                            if (!$data['status']) {
                                $data['status'] = 3; // Answered
                            }

                            if ($userid == false) {
                                $data['name']  = $name;
                                $data['email'] = $email;
                            }

                            $reply_id = $this->add_reply($data, $tid, $result->staffid, $attachments);
                            if ($reply_id) {
                                $mailstatus = 'service_request Reply Imported Successfully';
                            }
                        } else {
                            $mailstatus = 'service_request ID Not Found';
                        }
                    } else {
                        $this->db->where('email', $email);
                        $result = $this->db->get(db_prefix() . 'contacts')->row();
                        if ($result) {
                            $userid    = $result->userid;
                            $contactid = $result->id;
                        }
                        if ($userid == false && get_option('email_piping_only_registered') == '1') {
                            $mailstatus = 'Unregistered Email Address';
                        } else {
                            $filterdate = date('Y-m-d H:i:s', strtotime('-15 minutes'));
                            $query      = 'SELECT count(*) as total FROM ' . db_prefix() . 'service_requests WHERE date > "' . $filterdate . '" AND (email="' . $this->db->escape($email) . '"';
                            if ($userid) {
                                $query .= ' OR userid=' . (int) $userid;
                            }
                            $query .= ')';
                            $result = $this->db->query($query)->row();
                            if (10 < $result->total) {
                                $mailstatus = 'Exceeded Limit of 10 service_requests within 15 Minutes';
                            } else {
                                if (isset($tid)) {
                                    $data            = [];
                                    $data['message'] = $message;
                                    $data['status']  = 1;
                                    if ($userid == false) {
                                        $data['name']  = $name;
                                        $data['email'] = $email;
                                    } else {
                                        $data['userid']    = $userid;
                                        $data['contactid'] = $contactid;

                                        $this->db->where('userid', $userid);
                                        $this->db->where('service_requestid', $tid);
                                        $t = $this->db->get(db_prefix() . 'service_requests')->row();
                                        if (!$t) {
                                            $abuse = true;
                                        }
                                    }
                                    if (!isset($abuse)) {
                                        $reply_id = $this->add_reply($data, $tid, null, $attachments);
                                        if ($reply_id) {
                                            // Dont change this line
                                            $mailstatus = 'service_request Reply Imported Successfully';
                                        }
                                    } else {
                                        $mailstatus = 'service_request ID Not Found For User';
                                    }
                                } else {
                                    if (get_option('email_piping_only_registered') == 1 && !$userid) {
                                        $mailstatus = 'Blocked service_request Opening from Unregistered User';
                                    } else {
                                        if (get_option('email_piping_only_replies') == '1') {
                                            $mailstatus = 'Only Replies Allowed by Email';
                                        } else {
                                            $data               = [];
                                            $data['department'] = $department_id;
                                            $data['subject']    = $subject;
                                            $data['message']    = $message;
                                            $data['contactid']  = $contactid;
                                            $data['priority']   = get_option('email_piping_default_priority');
                                            if ($userid == false) {
                                                $data['name']  = $name;
                                                $data['email'] = $email;
                                            } else {
                                                $data['userid'] = $userid;
                                            }
                                            $tid = $this->add($data, null, $attachments);
                                            // Dont change this line
                                            $mailstatus = 'service_request Imported Successfully';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($mailstatus == '') {
            $mailstatus = 'service_request Import Failed';
        }
        $this->db->insert(db_prefix() . 'service_requests_pipe_log', [
            'date'     => date('Y-m-d H:i:s'),
            'email_to' => $to,
            'name'     => $name,
            'email'    => $email,
            'subject'  => $subject,
            'message'  => $message,
            'status'   => $mailstatus,
        ]);

        return $mailstatus;
    }

    private function process_pipe_attachments($attachments, $service_request_id, $reply_id = '')
    {
    

        if (!empty($attachments)) {
            $service_request_attachments = [];
            $allowed_extensions = array_map(function ($ext) {
                return strtolower(trim($ext));
            }, explode(',', get_option('service_request_attachments_file_extensions')));

            $path = FCPATH . 'uploads/service_request_attachments' . '/' . $service_request_id . '/';

            foreach ($attachments as $attachment) {
                $filename      = $attachment['filename'];
                $filenameparts = explode('.', $filename);
                $extension     = end($filenameparts);
                $extension     = strtolower($extension);
                if (in_array('.' . $extension, $allowed_extensions)) {

                    $filename = implode(array_slice($filenameparts, 0, 0 - 1));
                    $filename = trim(preg_replace('/[^a-zA-Z0-9-_ ]/', '', $filename));

                    if (!$filename) {
                        $filename = 'attachment';
                    }

                    if (!file_exists($path)) {
                        mkdir($path, 0755);
                        $fp = fopen($path . 'index.html', 'w');
                        fclose($fp);
                    }

                    $filename = unique_filename($path, $filename . '.' . $extension);
                    file_put_contents($path.$filename, $attachment['data']);

                    array_push($service_request_attachments, [
                        'file_name' => $filename,
                        'filetype'  => get_mime_by_extension($filename),
                    ]);
                }
            }

            $this->insert_service_request_attachments_to_database($service_request_attachments, $service_request_id, $reply_id);
        }
    }

    public function get($id = '', $where = [], $contact_id = '', $client_id = '', $staffid = '', $status_id = '', $start = '', $limit = '', $userid='', $assigned='')
    {   //echo "string";die();
        $permissions = $this->db->select('capability')->where('staff_id', $staffid)->where('feature', 'service_requests')->where('capability', 'view')->get(db_prefix() . 'staff_permissions')->row();
        $is_staffadmin = $this->db->select('admin')->where('staffid', $staffid)->get(db_prefix() . 'staff')->row();
        $primary_contact = $this->db->select('is_primary,userid')->where('id', $contact_id)->get(db_prefix() . 'contacts')->row();

        $this->db->select('*,' . db_prefix() . 'service_requests.userid,' . db_prefix() . 'service_requests.name as from_name,' . db_prefix() . 'service_requests.email as service_request_email, ' . db_prefix() . 'departments.name as department_name, ' . db_prefix() . 'service_requests_priorities.name as priority_name, statuscolor, ' . db_prefix() . 'service_requests.admin, ' . db_prefix() . 'services.name as service_name, service, ' . db_prefix() . 'service_requests_status.name as status_name,' . db_prefix() . 'service_requests.service_requestid, ' . db_prefix() . 'contacts.firstname as user_firstname, ' . db_prefix() . 'contacts.lastname as user_lastname,' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'staff.lastname as staff_lastname,lastreply,message,' . db_prefix() . 'service_requests.status,subject,department,priority,' . db_prefix() . 'contacts.email,adminread,clientread,date,' . db_prefix() . 'tickets_request_for.name as request_for_name,' . db_prefix() . 'contacts.phonenumber');
        $this->db->join(db_prefix() . 'departments', db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department', 'left');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        $this->db->join(db_prefix() . 'services', db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid', 'left');
        $this->db->join(db_prefix() . 'contacts', db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_requests.assigned', 'left');
        $this->db->join(db_prefix() . 'service_requests_priorities', db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority', 'left');
        $this->db->join(db_prefix() . 'tickets_request_for', db_prefix() . 'tickets_request_for.requestid = ' . db_prefix() . 'service_requests.request_for', 'left');

        

        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'service_requests.service_requestid', $id);

            return $this->db->get(db_prefix() . 'service_requests')->row();
        }
        if($contact_id){
            if($primary_contact->is_primary==1){ $this->db->where(db_prefix() . 'service_requests.userid', $primary_contact->userid); }
            else{ $this->db->where(db_prefix() . 'service_requests.contactid', $contact_id); }
        }
        if($staffid && $is_staffadmin->admin==0){
            if(!$permissions){
                $this->db->where(db_prefix() . 'service_requests.assigned', $staffid);
            }
        }
        else{ if($assigned){ $this->db->where('service_requests.assigned', $assigned); } }

        if($userid){ $this->db->where('service_requests.userid', $userid); }
        if($status_id){
            $this->db->where(db_prefix() . 'service_requests.status', $status_id);
        }
        // $this->db->order_by('lastreply', 'asc');
        $this->db->order_by('date', 'desc');
        if($limit){
            $this->db->limit($limit,$start);
        }

        return $this->db->get(db_prefix() . 'service_requests')->result_array();
    }

    public function get_service_reports($where = [], $start = '', $limit = '')
    {   
        $this->db->select('*,' . db_prefix() . 'service_requests.userid,' . db_prefix() . 'service_requests.name as from_name,' . db_prefix() . 'service_requests.email as service_request_email, ' . db_prefix() . 'service_requests_priorities.name as priority_name, statuscolor, ' . db_prefix() . 'service_requests.admin, ' . db_prefix() . 'services.name as service_name, service, ' . db_prefix() . 'service_requests_status.name as status_name,' . db_prefix() . 'service_requests.service_requestid, ' . db_prefix() . 'contacts.firstname as user_firstname, ' . db_prefix() . 'contacts.lastname as user_lastname,' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'staff.lastname as staff_lastname,lastreply,message,' . db_prefix() . 'service_requests.status,subject,department,priority,' . db_prefix() . 'contacts.email,adminread,clientread,date,' . db_prefix() . 'tickets_request_for.name as request_for_name,' . db_prefix() . 'contacts.phonenumber');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        $this->db->join(db_prefix() . 'services', db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid', 'left');
        $this->db->join(db_prefix() . 'contacts', db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_requests.assigned', 'left');
        $this->db->join(db_prefix() . 'service_requests_priorities', db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority', 'left');
        $this->db->join(db_prefix() . 'tickets_request_for', db_prefix() . 'tickets_request_for.requestid = ' . db_prefix() . 'service_requests.request_for', 'left');

        $this->db->where($where);
        $this->db->order_by('date', 'desc');
        if($limit){
            $this->db->limit($limit,$start);
        }

        return $this->db->get(db_prefix() . 'service_requests')->result_array();
    }

    public function get_sr_approvals($id = '', $where = [], $contact_id = '', $client_id = '')
    {   //echo "string";die();

        $this->db->select('*,' . db_prefix() . 'service_requests.userid,' . db_prefix() . 'service_requests.name as from_name,' . db_prefix() . 'service_requests.email as service_request_email, ' . db_prefix() . 'departments.name as department_name, ' . db_prefix() . 'service_requests_priorities.name as priority_name, statuscolor, ' . db_prefix() . 'service_requests.admin, ' . db_prefix() . 'services.name as service_name, service, ' . db_prefix() . 'service_requests_status.name as status_name,' . db_prefix() . 'service_requests.service_requestid, ' . db_prefix() . 'contacts.firstname as user_firstname, ' . db_prefix() . 'contacts.lastname as user_lastname,' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'staff.lastname as staff_lastname,lastreply,message,' . db_prefix() . 'service_requests.status,subject,department,priority,' . db_prefix() . 'contacts.email,adminread,clientread,date,' . db_prefix() . 'tickets_request_for.name as request_for_name');
        $this->db->join(db_prefix() . 'departments', db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department', 'left');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        $this->db->join(db_prefix() . 'services', db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid', 'left');
        $this->db->join(db_prefix() . 'contacts', db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_requests.assigned', 'left');
        $this->db->join(db_prefix() . 'service_requests_priorities', db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority', 'left');
        $this->db->join(db_prefix() . 'tickets_request_for', db_prefix() . 'tickets_request_for.requestid = ' . db_prefix() . 'service_requests.request_for', 'left');

        

        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'service_requests.service_requestid', $id);
            return $this->db->get(db_prefix() . 'service_requests')->row();
        }
        // if($contact_id){
        //     $this->db->where(db_prefix() . 'service_requests.contactid', $contact_id);
        // }
        $this->db->where(db_prefix() . 'service_requests.take_approval', 1);
        $this->db->where(db_prefix() . 'service_requests.approval_assigned_to_staffid', $contact_id);
        $this->db->where(db_prefix() . 'service_requests.is_approved', null);

        $this->db->order_by('lastreply', 'asc');

        return $this->db->get(db_prefix() . 'service_requests')->result_array();
    }

    /**
     * Get service_request by id and all data
     * @param  mixed  $id     service_request id
     * @param  mixed $userid Optional - service_requests from USER ID
     * @return object
     */
    public function get_service_request_by_id($id, $userid = '')
    {
    

        $this->db->select('*, ' . db_prefix() . 'service_requests.userid, ' . db_prefix() . 'service_requests.name as from_name, ' . db_prefix() . 'service_requests.email as service_request_email, ' . db_prefix() . 'departments.name as department_name, ' . db_prefix() . 'service_requests_priorities.name as priority_name, statuscolor, ' . db_prefix() . 'service_requests.admin, ' . db_prefix() . 'services.name as service_name, service, ' . db_prefix() . 'service_requests_status.name as status_name, ' . db_prefix() . 'service_requests.service_requestid, ' . db_prefix() . 'contacts.firstname as user_firstname, ' . db_prefix() . 'contacts.lastname as user_lastname, ' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'staff.lastname as staff_lastname, lastreply, message, ' . db_prefix() . 'service_requests.status, subject, department, priority, ' . db_prefix() . 'contacts.email, adminread, clientread, date, staffassigned.firstname as staff_firstname,staffassigned.lastname as staff_lastname');
        $this->db->from(db_prefix() . 'service_requests');
        $this->db->join(db_prefix() . 'departments', db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department', 'left');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        $this->db->join(db_prefix() . 'services', db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_requests.admin', 'left');
        $this->db->join(db_prefix() . 'contacts', db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid', 'left');
        $this->db->join(db_prefix() . 'service_requests_priorities', db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority', 'left');

        $this->db->join(db_prefix() . 'staff as staffassigned', 'staffassigned.staffid = ' . db_prefix() . 'service_requests.assigned', 'left');

        if (strlen($id) === 32) {
            $this->db->or_where(db_prefix() . 'service_requests.service_requestkey', $id);
        } else {
            $this->db->where(db_prefix() . 'service_requests.service_requestid', $id);
        }
        if (is_numeric($userid)) {
            $this->db->where(db_prefix() . 'service_requests.userid', $userid);
        }
        $service_request = $this->db->get()->row();
        if ($service_request) {
            if ($service_request->admin == null || $service_request->admin == 0) {
                if ($service_request->contactid != 0) {
                    $service_request->submitter = $service_request->user_firstname . ' ' . $service_request->user_lastname;
                } else {
                    $service_request->submitter = $service_request->from_name;
                }
            } else {
                if ($service_request->contactid != 0) {
                    $service_request->submitter = $service_request->user_firstname . ' ' . $service_request->user_lastname;
                } else {
                    $service_request->submitter = $service_request->from_name;
                }
                $service_request->opened_by = $service_request->staff_firstname . ' ' . $service_request->staff_lastname;
            }

            $service_request->attachments = $this->get_service_request_attachments($id);
        }


        return $service_request;
    }

    /**
     * Insert service_request attachments to database
     * @param  array  $attachments array of attachment
     * @param  mixed  $service_requestid
     * @param  boolean $replyid If is from reply
     */
    public function insert_service_request_attachments_to_database($attachments, $service_requestid, $replyid = false)
    {

        foreach ($attachments as $attachment) {
            $attachment['service_requestid']  = $service_requestid;
            $attachment['dateadded'] = date('Y-m-d H:i:s');
            if ($replyid !== false && is_int($replyid)) {
                $attachment['replyid'] = $replyid;
            }
            $this->db->insert(db_prefix() . 'service_request_attachments', $attachment);
        }
    }

    /**
     * Get service_request attachments from database
     * @param  mixed $id      service_request id
     * @param  mixed $replyid Optional - reply id if is from from reply
     * @return array
     */
    public function get_service_request_attachments($id, $replyid = '')
    {

        $this->db->where('service_requestid', $id);
        if (is_numeric($replyid)) {
            $this->db->where('replyid', $replyid);
        } else {
            $this->db->where('replyid', null);
        }
        $this->db->where('service_requestid', $id);

        $attachments = $this->db->get(db_prefix() . 'service_request_attachments')->result_array();
        $i = 0;
        foreach ($attachments as $attachment) {
            $attachments[$i]['file_path'] = '/download/file/service_request/' . $attachment['id'];
            $i++;
        }
        return $attachments;
    }

    /**
     * Add new reply to service_request
     * @param mixed $data  reply $_POST data
     * @param mixed $id    service_request id
     * @param boolean $admin staff id if is staff making reply
     */
    public function add_reply($data, $id, $admin = null, $pipe_attachments = false)
    {

        if (isset($data['assign_to_current_user'])) {
            $assigned = get_staff_user_id();
            unset($data['assign_to_current_user']);
        }

        $unsetters = [
            'note_description',
            'department',
            'priority',
            'subject',
            'assigned',
            'project_id',
            'service',
            'status_top',
            'attachments',
            'DataTables_Table_0_length',
            'DataTables_Table_1_length',
            'custom_fields',
        ];

        foreach ($unsetters as $unset) {
            if (isset($data[$unset])) {
                unset($data[$unset]);
            }
        }

        if ($admin !== null) {
            $data['admin'] = $admin;
            $status        = $data['status'];
        } else {
            $status = 1;
        }

        if (isset($data['status'])) {
            unset($data['status']);
        }

        $cc = '';
        if (isset($data['cc'])) {
            $cc = $data['cc'];
            unset($data['cc']);
        }

        $cc_contacts = '';
        $cc_mail_array = array();
        if (isset($data['cc_contacts'])) {
            foreach($data['cc_contacts'] as $cc_mail){
                $mail = $this->db->select('email')->from(db_prefix() . 'contacts')->where('id',$cc_mail)->get()->row();
                array_push($cc_mail_array, $mail->email);
            }
            //$cc = implode(',', $cc_mail_array);
            $cc_contacts = implode(',', $data['cc_contacts']);
            unset($data['cc_contacts']);
        }

        $cc_staffs = '';
        if (isset($data['cc_staffs'])) {
            foreach($data['cc_staffs'] as $cc_mail){
                $mail = $this->db->select('email')->from(db_prefix() . 'staff')->where('staffid',$cc_mail)->get()->row();
                array_push($cc_mail_array, $mail->email);
            }
            $cc_staffs = implode(',', $data['cc_staffs']);
            unset($data['cc_staffs']);
        }
        $cc = implode(',', $cc_mail_array);

        $data['service_requestid'] = $id;
        $data['date']     = date('Y-m-d H:i:s');
        $data['message']  = trim($data['message']);

        if ($this->piping == true) {
            $data['message'] = preg_replace('/\v+/u', '<br>', $data['message']);
        }

        // admin can have html
        if ($admin == null && hooks()->apply_filters('service_request_message_without_html_for_non_admin', true)) {
            $data['message'] = _strip_tags($data['message']);
            $data['message'] = nl2br_save_html($data['message']);
        }

        if (!isset($data['userid'])) {
            $data['userid'] = 0;
        }

        $data['message'] = remove_emojis($data['message']);
        $data            = hooks()->apply_filters('before_service_request_reply_add', $data, $id, $admin);

        $this->db->insert(db_prefix() . 'service_request_replies', $data);

        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            /**
             * When a service_request is in status "In progress" and the customer reply to the service_request
             * it changes the status to "Open" which is not normal.
             *
             * The service_request should keep the status "In progress"
             */
            $this->db->select('status');
            $this->db->where('service_requestid', $id);
            $old_service_request_status = $this->db->get(db_prefix() . 'service_requests')->row()->status;

            $newStatus = hooks()->apply_filters(
                'service_request_reply_status',
                ($old_service_request_status == 2 && $admin == null ? $old_service_request_status : $status),
                ['service_request_id' => $id, 'reply_id' => $insert_id, 'admin' => $admin, 'old_status' => $old_service_request_status]
            );

            if (isset($assigned)) {
                $this->db->where('service_requestid', $id);
                $this->db->update(db_prefix() . 'service_requests', [
                    'assigned' => $assigned,
                ]);
            }

            if ($pipe_attachments != false) {
                $this->process_pipe_attachments($pipe_attachments, $id, $insert_id);
            } else {
                $attachments = handle_service_request_attachments($id);
                if ($attachments) {
                    $this->service_requests_model->insert_service_request_attachments_to_database($attachments, $id, $insert_id);
                }
            }

            $_attachments = $this->get_service_request_attachments($id, $insert_id);

            log_activity('New service_request Reply [ReplyID: ' . $insert_id . ']');

            $this->db->where('service_requestid', $id);
            $this->db->update(db_prefix() . 'service_requests', [
                'lastreply'  => date('Y-m-d H:i:s'),
                'status'     => $newStatus,
                'cc_contacts' => $cc_contacts,
                'cc_staffs'  => $cc_staffs,
                'adminread'  => 0,
                'clientread' => 0,
            ]);

            if ($old_service_request_status != $newStatus) {
                hooks()->do_action('after_service_request_status_changed', [
                    'id'     => $id,
                    'status' => $newStatus,
                ]);
            }

            $service_request    = $this->get_service_request_by_id($id);
            $userid    = $service_request->userid;
            $contactid = $service_request->contactid;

            //send push notification
            $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
            $token = $user_fcm->fcm_token;
            $notification_data['module']    = 'service_request';
            $notification_data['id']        = $id;
            $notification_data['title']     = 'New Service Request Ticket Reply';
            $notification_data['body']      = 'New Service Request Ticket Reply Added [ReplyID: ' . $insert_id . ']';
            push_notification($token, $notification_data);

            $isContact = false;
            if ($service_request->userid != 0 && $service_request->contactid != 0) {
                $email     = $this->clients_model->get_contact($service_request->contactid)->email;
                $isContact = true;
            } else {
                $email = $service_request->service_request_email;
            }
            if ($admin == null) {
                $this->load->model('departments_model');
                $this->load->model('staff_model');
                $staff = $this->staff_model->get('', ['active' => 1]);

                $notifiedUsers                           = [];
                $notificationForStaffMemberOnservice_requestReply = get_option('receive_notification_on_new_service_request_replies') == 1;

                foreach ($staff as $staff_key => $member) {
                    if (get_option('access_service_requests_to_none_staff_members') == 0
                         && !is_staff_member($member['staffid'])) {
                        continue;
                    }

                    $staff_departments = $this->departments_model->get_staff_departments($member['staffid'], true);

                    if (in_array($service_request->department, $staff_departments)) {
                        send_mail_template('service_request_new_reply_to_staff', $service_request, $member, $_attachments);

                        if ($notificationForStaffMemberOnservice_requestReply) {
                            $notified = add_notification([
                                    'description'     => 'not_new_service_request_reply',
                                    'touserid'        => $member['staffid'],
                                    'fromcompany'     => 1,
                                    'fromuserid'      => 0,
                                    'link'            => 'service_requests/service_request/' . $id,
                                    'additional_data' => serialize([
                                        $service_request->subject,
                                    ]),
                                ]);
                            if ($notified) {
                                array_push($notifiedUsers, $member['staffid']);
                            }
                        }
                    }
                }
                pusher_trigger_notification($notifiedUsers);
            } else {
                $sendEmail = true;
                if ($isContact && total_rows(db_prefix() . 'contacts', ['service_request_emails' => 1, 'id' => $service_request->contactid]) == 0) {
                    $sendEmail = false;
                }
                if ($sendEmail) {
                    send_mail_template('service_request_new_reply_to_customer', $service_request, $email, $_attachments, $cc);
                }
            }
            hooks()->do_action('after_service_request_reply_added', [
                'data'    => $data,
                'id'      => $id,
                'admin'   => $admin,
                'replyid' => $insert_id,
            ]);

            return $insert_id;
        }

        return false;
    }

    /**
     *  Delete service_request reply
     * @param   mixed $service_request_id    service_request id
     * @param   mixed $reply_id     reply id
     * @return  boolean
     */
    public function delete_service_request_reply($service_request_id, $reply_id)
    {

        hooks()->do_action('before_delete_service_request_reply', ['service_request_id' => $service_request_id, 'reply_id' => $reply_id]);

        $this->db->where('id', $reply_id);
        $this->db->delete(db_prefix() . 'service_request_replies');

        if ($this->db->affected_rows() > 0) {
            // Get the reply attachments by passing the reply_id to get_service_request_attachments method
            $attachments = $this->get_service_request_attachments($service_request_id, $reply_id);
            if (count($attachments) > 0) {
                foreach ($attachments as $attachment) {
                    $this->delete_service_request_attachment($attachment['id']);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Remove service_request attachment by id
     * @param  mixed $id attachment id
     * @return boolean
     */
    public function delete_service_request_attachment($id)
    {

        $deleted = false;
        $this->db->where('id', $id);
        $attachment = $this->db->get(db_prefix() . 'service_request_attachments')->row();
        if ($attachment) {
            if (unlink(get_upload_path_by_type('service_request') . $attachment->service_requestid . '/' . $attachment->file_name)) {
                $this->db->where('id', $attachment->id);
                $this->db->delete(db_prefix() . 'service_request_attachments');
                $deleted = true;
            }
            // Check if no attachments left, so we can delete the folder also
            $other_attachments = list_files(get_upload_path_by_type('service_request') . $attachment->service_requestid);
            if (count($other_attachments) == 0) {
                delete_dir(get_upload_path_by_type('service_request') . $attachment->service_requestid);
            }
        }

        return $deleted;
    }

    /**
     * Get service_request attachment by id
     * @param  mixed $id attachment id
     * @return mixed
     */
    public function get_service_request_attachment($id)
    {

        $this->db->where('id', $id);

        return $this->db->get(db_prefix() . 'service_request_attachments')->row();
    }

    /**
     * This functions is used when staff open client service_request
     * @param  mixed $userid client id
     * @param  mixed $id     service_requestid
     * @return array
     */
    public function get_user_other_service_request($userid, $id)
    {

        $this->db->select(db_prefix() . 'departments.name as department_name, ' . db_prefix() . 'services.name as service_name,' . db_prefix() . 'service_requests_status.name as status_name,' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'clients.lastname as staff_lastname,service_requestid,subject,firstname,lastname,lastreply');
        $this->db->from(db_prefix() . 'service_requests');
        $this->db->join(db_prefix() . 'departments', db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department', 'left');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        $this->db->join(db_prefix() . 'services', db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_requests.admin', 'left');
        $this->db->where(db_prefix() . 'service_requests.userid', $userid);
        $this->db->where(db_prefix() . 'service_requests.service_requestid !=', $id);
        $service_requests = $this->db->get()->result_array();
        $i       = 0;
        foreach ($service_requests as $service_request) {
            $service_requests[$i]['submitter'] = $service_request['firstname'] . ' ' . $service_request['lastname'];
            unset($service_request['firstname']);
            unset($service_request['lastname']);
            $i++;
        }

        return $service_requests;
    }

    /**
     * Get all service_request replies
     * @param  mixed  $id     service_requestid
     * @param  mixed $userid specific client id
     * @return array
     */
    public function get_service_request_replies($id)
    {

        $service_request_replies_order = get_option('service_request_replies_order');
        // backward compatibility for the action hook
        $service_request_replies_order = hooks()->apply_filters('service_request_replies_order', $service_request_replies_order);

        $this->db->select(db_prefix() . 'service_request_replies.id,' . db_prefix() . 'service_request_replies.name as from_name,' . db_prefix() . 'service_request_replies.email as reply_email, ' . db_prefix() . 'service_request_replies.admin, ' . db_prefix() . 'service_request_replies.userid,' . db_prefix() . 'staff.firstname as staff_firstname, ' . db_prefix() . 'staff.lastname as staff_lastname,' . db_prefix() . 'contacts.firstname as user_firstname,' . db_prefix() . 'contacts.lastname as user_lastname,message,date,contactid');
        $this->db->from(db_prefix() . 'service_request_replies');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'service_request_replies.userid', 'left');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'service_request_replies.admin', 'left');
        $this->db->join(db_prefix() . 'contacts', db_prefix() . 'contacts.id = ' . db_prefix() . 'service_request_replies.contactid', 'left');
        $this->db->where('service_requestid', $id);
        $this->db->order_by('date', $service_request_replies_order);
        $replies = $this->db->get()->result_array();
        $i       = 0;
        foreach ($replies as $reply) {
            if ($reply['admin'] !== null || $reply['admin'] != 0) {
                // staff reply
                $replies[$i]['submitter'] = $reply['staff_firstname'] . ' ' . $reply['staff_lastname'];
            } else {
                if ($reply['contactid'] != 0) {
                    $replies[$i]['submitter'] = $reply['user_firstname'] . ' ' . $reply['user_lastname'];
                } else {
                    $replies[$i]['submitter'] = $reply['from_name'];
                }
            }
            unset($replies[$i]['staff_firstname']);
            unset($replies[$i]['staff_lastname']);
            unset($replies[$i]['user_firstname']);
            unset($replies[$i]['user_lastname']);
            $replies[$i]['attachments'] = $this->get_service_request_attachments($id, $reply['id']);
            $i++;
        }

        return $replies;
    }

    /**
     * Add new service_request to database
     * @param mixed $data  service_request $_POST data
     * @param mixed $admin If admin adding the service_request passed staff id
     */
    public function add($data, $admin = null, $pipe_attachments = false)
    {
        // echo $admin  ;die;

        if ($admin !== null) {
            $data['admin'] = $admin;
            unset($data['service_request_client_search']);
        }

        if (isset($data['assigned']) && $data['assigned'] == '') {
            $data['assigned'] = 0;
        }

        if (isset($data['project_id']) && $data['project_id'] == '') {
            $data['project_id'] = 0;
        }

        if ($admin == null) {
            if (isset($data['email']) && empty($data['contactid'])) {
                $data['userid']    = 0;
                $data['contactid'] = 0;
            } else {
                // Opened from customer portal otherwise is passed from pipe or admin area
                if (!isset($data['userid']) && !isset($data['contactid'])) {
                    $data['userid']    = get_client_user_id();
                    $data['contactid'] = get_contact_user_id();
                }
            }
            $data['status'] = 1;
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        // CC is only from admin area
        $cc = '';
        if (isset($data['cc'])) {
            $cc = $data['cc'];
            unset($data['cc']);
        }

        $cc_contacts = '';
        $cc_mail_array = array();
        if (isset($data['cc_contacts'])) {
            foreach($data['cc_contacts'] as $cc_mail){
                $mail = $this->db->select('email')->from(db_prefix() . 'contacts')->where('id',$cc_mail)->get()->row();
                array_push($cc_mail_array, $mail->email);
            }
            //$cc = implode(',', $cc_mail_array);
            $cc_contacts = implode(',', $data['cc_contacts']);
            unset($data['cc_contacts']);
        }
        $cc_staffs = '';
        if (isset($data['cc_staffs'])) {
            foreach($data['cc_staffs'] as $cc_mail){
                $mail = $this->db->select('email')->from(db_prefix() . 'staff')->where('staffid',$cc_mail)->get()->row();
                array_push($cc_mail_array, $mail->email);
            }
            $cc_staffs = implode(',', $data['cc_staffs']);
            unset($data['cc_staffs']);
        }
        $cc = implode(',', $cc_mail_array);
        
        $data['cc_staffs']   = $cc_staffs;
        $data['cc_contacts']   = $cc_contacts;
        $data['date']      = date('Y-m-d H:i:s');
        $data['service_requestkey'] = app_generate_hash();
        $data['status']    = 1;
        $data['message']   = trim($data['message']);
        $data['subject']   = trim($data['subject']);
        if ($this->piping == true) {
            $data['message'] = preg_replace('/\v+/u', '<br>', $data['message']);
        }

        // Admin can have html
        if ($admin == null && hooks()->apply_filters('service_request_message_without_html_for_non_admin', true)) {
            $data['message'] = _strip_tags($data['message']);
            $data['subject'] = _strip_tags($data['subject']);
            $data['message'] = nl2br_save_html($data['message']);
        }

        if (!isset($data['userid'])) {
            $data['userid'] = 0;
        }
        if (isset($data['priority']) && $data['priority'] == '' || !isset($data['priority'])) {
            $data['priority'] = 0;
        }

        $tags = '';
        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tags']);
        }
        
        $data['message'] = remove_emojis($data['message']);
        $data            = hooks()->apply_filters('before_service_request_created', $data, $admin);
       
        $this->db->insert(db_prefix() . 'service_requests', $data);
        
        $service_requestid = $this->db->insert_id();
        //echo $service_requestid;die;
        //echo '$tags';die;
        if ($service_requestid) {
            $this->db->insert(db_prefix() . 'service_request_status_logs', array('service_requestid' => $service_requestid ,
                'status'=>1,'datetime'=>$data['date']));

            handle_tags_save($tags, $service_requestid, 'service_request');

            if (isset($custom_fields)) {
                handle_custom_fields_post($service_requestid, $custom_fields);
            }

            $service_request    = $this->get_service_request_by_id($service_requestid);
            $contactid = $service_request->contactid;
            //send push notification
            $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
            $token = $user_fcm->fcm_token;
            $notification_data['module']    = 'service_request';
            $notification_data['id']        = $service_requestid;
            $notification_data['title']     = 'New Service Request Ticket';
            $notification_data['body']      = 'New Service Request Ticket Created [SR-ID: ' . $service_request->servicerequestpattern . $service_requestid . ']';
            push_notification($token, $notification_data);

            if (isset($data['assigned']) && $data['assigned'] != 0) {
                if ($data['assigned'] != get_staff_user_id()) {
                    $notified = add_notification([
                        'description'     => 'not_service_request_assigned_to_you',
                        'touserid'        => $data['assigned'],
                        'fromcompany'     => 1,
                        'fromuserid'      => 0,
                        'link'            => 'service_requests/service_request/' . $service_requestid,
                        'additional_data' => serialize([
                            $data['subject'],
                        ]),
                    ]);

                    if ($notified) {
                        pusher_trigger_notification([$data['assigned']]);
                    }

                   send_mail_template('service_request_assigned_to_staff', get_staff($data['assigned'])->email, $data['assigned'], $service_requestid, $data['userid'], $data['contactid']);

                   // send_mail_template('new-service_request-opened-approval', get_staff($data['assigned'])->email, $data['assigned'], $service_requestid, $data['userid'], $data['contactid']);
                }
               
            }

            if ($pipe_attachments != false) {
                //echo 'ram';die;
                $this->process_pipe_attachments($pipe_attachments, $service_requestid);
            } else {
                $attachments = handle_service_request_attachments($service_requestid);
                if ($attachments) {
                    $this->insert_service_request_attachments_to_database($attachments, $service_requestid);
                }
            }
            $_attachments = $this->get_service_request_attachments($service_requestid);
            
            //print_r($data);die();

            $isContact = false;
            if (isset($data['userid']) && $data['userid'] != false && isset($data['contactid'])) {
                $email     = $this->clients_model->get_contact($data['contactid'])->email;
                $isContact = true;
            } else {
                $email = $data['email'];
            }

            $template = 'service_request_created_to_customer';
            if ($admin == null) {
                $template = 'service_request_autoresponse';

                $this->load->model('departments_model');
                $this->load->model('staff_model');
                $staff = $this->staff_model->get('', ['active' => 1]);

                $notifiedUsers                              = [];
                $notificationForStaffMemberOnservice_requestCreation = get_option('receive_notification_on_new_service_request') == 1;

                foreach ($staff as $member) {
                    if (get_option('access_service_requests_to_none_staff_members') == 0
                        && !is_staff_member($member['staffid'])) {
                        continue;
                    }
                    $staff_departments = $this->departments_model->get_staff_departments($member['staffid'], true);

                    if (in_array($data['department'], $staff_departments)) {
                         //print_r('helod');die;
                        send_mail_template('service_request_created_to_staff', $service_requestid, $data['userid'], $data['contactid'], $member, $_attachments);

                        // send_mail_template('new-service_request-opened-approval',  $service_requestid, $data['userid'], $data['contactid'], $member, $_attachments);

                        if ($notificationForStaffMemberOnservice_requestCreation) {
                            $notified = add_notification([
                                    'description'     => 'not_new_service_request_created',
                                    'touserid'        => $member['staffid'],
                                    'fromcompany'     => 1,
                                    'fromuserid'      => 0,
                                    'link'            => 'service_requests/service_request/' . $service_requestid,
                                    'additional_data' => serialize([
                                        $data['subject'],
                                    ]),
                                ]);
                            if ($notified) {
                                array_push($notifiedUsers, $member['staffid']);
                            }
                        }
                    }
                }
                pusher_trigger_notification($notifiedUsers);
            }
            
            $sendEmail = true;

            if ($isContact && total_rows(db_prefix() . 'contacts', ['service_request_emails' => 1, 'id' => $data['contactid']]) == 0) {
                $sendEmail = false;
            }

            if ($sendEmail) {
                $service_request = $this->get_service_request_by_id($service_requestid);
                // $admin == null ? [] : $_attachments - Admin opened service_request from admin area add the attachments to the email
                
                send_mail_template($template, $service_request, $email, $admin == null ? [] : $_attachments, $cc);
                // print_r('helo');die;
                // send_mail_template('service_request_assigned_to_approval_authority',$service_request, $email, $admin == null ? [] : $_attachments, $cc);
            }
                //echo $service_requestid;die;
            hooks()->do_action('service_request_created', $service_requestid);
            log_activity('New service_request Created [ID: ' . $service_requestid . ']');

            return $service_requestid;
        }

        return false;
    }

    /**
     * Get latest 5 client service_requests
     * @param  integer $limit  Optional limit service_requests
     * @param  mixed $userid client id
     * @return array
     */
    public function get_client_latests_service_request($limit = 5, $userid = '')
    {
        //echo 'hello';die;

        $this->db->select(db_prefix() . 'service_requests.userid, service_requeststatusid, statuscolor, ' . db_prefix() . 'service_requests_status.name as status_name,' . db_prefix() . 'service_requests.service_requestid, subject, date');
        $this->db->from(db_prefix() . 'service_requests');
        $this->db->join(db_prefix() . 'service_requests_status', db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status', 'left');
        if (is_numeric($userid)) {
            $this->db->where(db_prefix() . 'service_requests.userid', $userid);
        } else {
            $this->db->where(db_prefix() . 'service_requests.userid', get_client_user_id());
        }
        $this->db->limit($limit);

        return $this->db->get()->result_array();
    }

    /**
     * Delete service_request from database and all connections
     * @param  mixed $service_requestid service_requestid
     * @return boolean
     */
    public function delete($service_requestid)
    {
        //echo 'hello';die;

        $affectedRows = 0;
        hooks()->do_action('before_service_request_deleted', $service_requestid);
        // final delete service_request
        $this->db->where('service_requestid', $service_requestid);
        $this->db->delete(db_prefix() . 'service_requests');
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            $this->db->where('service_requestid', $service_requestid);
            $attachments = $this->db->get(db_prefix() . 'service_request_attachments')->result_array();
            if (count($attachments) > 0) {
                if (is_dir(get_upload_path_by_type('service_request') . $service_requestid)) {
                    if (delete_dir(get_upload_path_by_type('service_request') . $service_requestid)) {
                        foreach ($attachments as $attachment) {
                            $this->db->where('id', $attachment['id']);
                            $this->db->delete(db_prefix() . 'service_request_attachments');
                            if ($this->db->affected_rows() > 0) {
                                $affectedRows++;
                            }
                        }
                    }
                }
            }

            $this->db->where('relid', $service_requestid);
            $this->db->where('fieldto', 'service_requests');
            $this->db->delete(db_prefix() . 'customfieldsvalues');

            // Delete replies
            $this->db->where('service_requestid', $service_requestid);
            $this->db->delete(db_prefix() . 'service_request_replies');

            $this->db->where('rel_id', $service_requestid);
            $this->db->where('rel_type', 'service_request');
            $this->db->delete(db_prefix() . 'notes');

            $this->db->where('rel_id', $service_requestid);
            $this->db->where('rel_type', 'service_request');
            $this->db->delete(db_prefix() . 'taggables');

            $this->db->where('rel_type', 'service_request');
            $this->db->where('rel_id', $service_requestid);
            $this->db->delete(db_prefix() . 'reminders');

            // Get related tasks
            $this->db->where('rel_type', 'service_request');
            $this->db->where('rel_id', $service_requestid);
            $tasks = $this->db->get(db_prefix() . 'tasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }
        }
        if ($affectedRows > 0) {
            log_activity('service_request Deleted [ID: ' . $service_requestid . ']');

            return true;
        }

        return false;
    }

    /**
     * Update service_request data / admin use
     * @param  mixed $data service_request $_POST data
     * @return boolean
     */
    public function update_single_service_request_settings($data)
    {
        //echo 'hello';die;

        $affectedRows = 0;
        $data         = hooks()->apply_filters('before_service_request_settings_updated', $data);

        $service_requestBeforeUpdate = $this->get_service_request_by_id($data['service_requestid']);

        if (isset($data['custom_fields']) && count($data['custom_fields']) > 0) {
            if (handle_custom_fields_post($data['service_requestid'], $data['custom_fields'])) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        $tags = '';
        if (isset($data['tags'])) {
            $tags = $data['tags'];
            unset($data['tags']);
        }

        if (handle_tags_save($tags, $data['service_requestid'], 'service_request')) {
            $affectedRows++;
        }

        if (isset($data['priority']) && $data['priority'] == '' || !isset($data['priority'])) {
            $data['priority'] = 0;
        }

        if ($data['assigned'] == '') {
            $data['assigned'] = 0;
        }

        if (isset($data['project_id']) && $data['project_id'] == '') {
            $data['project_id'] = 0;
        }

        // if (isset($data['contactid']) && $data['contactid'] != '') {
        //     $data['name']  = null;
        //     $data['email'] = null;
        // }
        if($data['take_approval'] == 1) {
            $admin = '';
            $cc='';
            $service_request = $this->get_service_request_by_id($data['service_requestid']);
            //print_r($data['approval_assigned_to_staffid']);die();
            $contactMail  = $this->db->select('email')
                                 ->where('id',$data['approval_assigned_to_staffid'])
                                 ->get(db_prefix().'contacts')->row();
            
            if($contactMail->email) {
                send_mail_template('service_request_assigned_to_approval_authority',$service_request, $contactMail->email, $admin == null ? [] : $_attachments, $cc);
            }
        }
        //print_r($data);die();
        $this->db->where('service_requestid', $data['service_requestid']);
        $this->db->update(db_prefix() . 'service_requests', $data);
        if ($this->db->affected_rows() > 0) {
            hooks()->do_action(
                'service_request_settings_updated',
            [
                'service_request_id'       => $data['service_requestid'],
                'original_service_request' => $service_requestBeforeUpdate,
                'data'            => $data, ]
            );
            $affectedRows++;
        }

        $service_request    = $this->get_service_request_by_id($data['service_requestid']);
        $contactid = $service_request->contactid;
        //send push notification
        $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
        $token = $user_fcm->fcm_token;
        $notification_data['module']    = 'service_request';
        $notification_data['id']        = $data['service_requestid'];
        $notification_data['title']     = 'Service Request Ticket Updated';
        $notification_data['body']      = 'Service Request Ticket Settings Updated [TicketID: ' . $service_request->service_requestpattern . $data['service_requestid'] . ']';
        push_notification($token, $notification_data);

        $sendAssignedEmail = false;

        $current_assigned = $service_requestBeforeUpdate->assigned;
        if ($current_assigned != 0) {
            if ($current_assigned != $data['assigned']) {
                if ($data['assigned'] != 0 && $data['assigned'] != get_staff_user_id()) {
                    $sendAssignedEmail = true;
                    $notified          = add_notification([
                        'description'     => 'not_service_request_reassigned_to_you',
                        'touserid'        => $data['assigned'],
                        'fromcompany'     => 1,
                        'fromuserid'      => 0,
                        'link'            => 'service_requests/service_request/' . $data['service_requestid'],
                        'additional_data' => serialize([
                            $data['subject'],
                        ]),
                    ]);
                    if ($notified) {
                        pusher_trigger_notification([$data['assigned']]);
                    }
                }
            }
        } else {
            if ($data['assigned'] != 0 && $data['assigned'] != get_staff_user_id()) {
                $sendAssignedEmail = true;
                $notified          = add_notification([
                    'description'     => 'not_service_request_assigned_to_you',
                    'touserid'        => $data['assigned'],
                    'fromcompany'     => 1,
                    'fromuserid'      => 0,
                    'link'            => 'service_requests/service_request/' . $data['service_requestid'],
                    'additional_data' => serialize([
                        $data['subject'],
                    ]),
                ]);

                if ($notified) {
                    pusher_trigger_notification([$data['assigned']]);
                }
            }
        }
        if ($sendAssignedEmail === true) {
            $this->db->where('staffid', $data['assigned']);
            $assignedEmail = $this->db->get(db_prefix() . 'staff')->row()->email;

            send_mail_template('service_request_assigned_to_staff', $assignedEmail, $data['assigned'], $data['service_requestid'], $data['userid'], $data['contactid']);
        }
        if ($affectedRows > 0) {
            log_activity('service_request Updated [ID: ' . $data['service_requestid'] . ']');

            return true;
        }

        return false;
    }

    
    public function service_request_approval($data)
    {
        $affectedRows = 0;
        $data         = hooks()->apply_filters('before_service_request_settings_updated', $data);

        $service_requestBeforeUpdate = $this->get_service_request_by_id($data['service_requestid']);
        $this->db->where('service_requestid', $data['service_requestid']);
        $this->db->update(db_prefix() . 'service_requests', $data);
        if ($this->db->affected_rows() > 0) {
            hooks()->do_action(
                'service_request_settings_updated',
            [
                'service_request_id'       => $data['service_requestid'],
                'original_service_request' => $service_requestBeforeUpdate,
                'data'            => $data, ]
            );
            $affectedRows++;
        }

        // $sendAssignedEmail = false;
        // if ($sendAssignedEmail === true) {
        //     $this->db->where('staffid', $data['assigned']);
        //     $assignedEmail = $this->db->get(db_prefix() . 'staff')->row()->email;

        //     send_mail_template('service_request_assigned_to_staff', $assignedEmail, $data['assigned'], $data['service_requestid'], $data['userid'], $data['contactid']);
        // }
        if ($affectedRows > 0) {
            return true;
        }
        return false;
    }

    /**
     * C<ha></ha>nge service_request status
     * @param  mixed $id     service_requestid
     * @param  mixed $status status id
     * @return array
     */
    public function change_service_request_status($id, $status)
    {
        
        $data['client'] = $this->db->select('tblcontacts.userid')
        ->from(db_prefix() . 'service_requests')
        ->join(db_prefix() . 'contacts','tblservice_requests.contactid = tblcontacts.id')
        ->where('service_requestid',$id)->get()->row();

        $this->db->where('service_requeststatusid', $status);
        $data['status'] = $this->db->get(db_prefix() . 'service_requests_status')->row();

        $service_request    = $this->get_service_request_by_id($id);
        $contactid = $service_request->contactid;
        //send push notification
        $user_fcm = $this->db->select('fcm_token')->where('id', $contactid)->get(db_prefix() . 'contacts')->row();
        $token = $user_fcm->fcm_token;
        $notification_data['module']    = 'service_request';
        $notification_data['id']        = $id;
        $notification_data['title']     = 'Service Request Ticket Status Updated';
        $notification_data['body']      = 'Service Request Ticket status marked to '.$data['status']->name.' [SR-ID: ' . $service_request->service_requestpattern.$id . ']';
        push_notification($token, $notification_data);

        $client_notified = add_notification([
            'description'     => 'Service Request '.$id.' status marked to '.$data['status']->name,
            'touserid'        => 0,
            'toclientid'      => $data['client']->userid,
            'fromcompany'     => 1,
            'fromuserid'      => 0,
            'link'            => 'clients/service_request/' . $id,
            'additional_data' => null,
        ]);
        if ($client_notified) {
            pusher_trigger_notification([$data['client']->userid]);
        }

        if($status != 3 && $status != 5 ) {

            $this->db->where('service_requestid', $id)
                     ->update(db_prefix() . 'service_requests', ['status' => $status,]);
              
            if ($this->db->affected_rows() > 0) 
            {
                $this->db->insert(db_prefix() . 'service_request_status_logs', ['service_requestid' => $id,
                    'status' => $status]);
                if($status == 3)
                {
                     pusher_trigger_notification(1);
                }
               
                

                $alert   = 'success';
                $message = _l('service_request_status_changed_successfully');
                hooks()->do_action('after_service_request_status_changed', [
                    'id'     => $id,
                    'status' => $status,
                ]);
            }
                $data = $this->db->where('service_requestid', $id);
                $data = $this->db->get(db_prefix() . 'service_requests')->row();

                return [
                    'alert'   => $alert,
                    'message' => $message,
                ];
        }
        else
        {
            if(isset($id) && isset($status)) {
                if($status == 5)
                {
                    $service_request = $this->get_service_request_by_id($id);
                    $admin ='';
                    $cc = ''; 
                    send_mail_template('service_request_closed_to_customer', $service_request, $service_request->email, $admin == null ? [] : $_attachments, $cc);
                }
                else if($status == 3)
                {
                    if (isset($data->assigned) && $data->assigned != 0) {
                        if ($data->assigned != get_staff_user_id()) {
                            $notified = add_notification([
                                'description'     => 'service_request '.$id.' status marked to resolved',
                                'touserid'        => $data->assigned,
                                'fromcompany'     => 1,
                                'fromuserid'      => 0,
                                'link'            => 'service_requests/service_request/' . $id,
                                'additional_data' => serialize([
                                    $data->subject,
                                ]),
                            ]);
                            if ($notified) {
                                pusher_trigger_notification([$data->assigned]);
                            }
                        }
                    }
                }
            }
        }
    }

    // Priorities

    /**
     * Get service_request priority by id
     * @param  mixed $id priority id
     * @return mixed     if id passed return object else array
     */
    public function get_priority($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('priorityid', $id);

            return $this->db->get(db_prefix() . 'service_requests_priorities')->row();
        }

        return $this->db->get(db_prefix() . 'service_requests_priorities')->result_array();
    }

    /**
     * Add new service_request priority
     * @param array $data service_request priority data
     */
    public function add_priority($data)
    {
        $this->db->insert(db_prefix() . 'service_requests_priorities', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New service_request Priority Added [ID: ' . $insert_id . ', Name: ' . $data['name'] . ']');
        }

        return $insert_id;
    }

    /**
     * Update service_request priority
     * @param  array $data service_request priority $_POST data
     * @param  mixed $id   service_request priority id
     * @return boolean
     */
    public function update_priority($data, $id)
    {
        $this->db->where('priorityid', $id);
        $this->db->update(db_prefix() . 'service_requests_priorities', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('service_request Priority Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete service_request priorit
     * @param  mixed $id service_request priority id
     * @return mixed
     */
    public function delete_priority($id)
    {
        $current = $this->get($id);
        // Check if the priority id is used in service_requests table
        if (is_reference_in_table('priority', db_prefix() . 'service_requests', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $this->db->where('priorityid', $id);
        $this->db->delete(db_prefix() . 'service_requests_priorities');
        if ($this->db->affected_rows() > 0) {
            if (get_option('email_piping_default_priority') == $id) {
                update_option('email_piping_default_priority', '');
            }
            log_activity('service_request Priority Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    // Predefined replies

    /**
     * Get predefined reply  by id
     * @param  mixed $id predefined reply id
     * @return mixed if id passed return object else array
     */
    public function get_predefined_reply($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'service_requests_predefined_replies')->row();
        }

        return $this->db->get(db_prefix() . 'service_requests_predefined_replies')->result_array();
    }

    /**
     * Add new predefined reply
     * @param array $data predefined reply $_POST data
     */
    public function add_predefined_reply($data)
    {
        $this->db->insert(db_prefix() . 'service_requests_predefined_replies', $data);
        $insertid = $this->db->insert_id();
        log_activity('New Predefined Reply Added [ID: ' . $insertid . ', ' . $data['name'] . ']');

        return $insertid;
    }

    /**
     * Update predefined reply
     * @param  array $data predefined $_POST data
     * @param  mixed $id   predefined reply id
     * @return boolean
     */
    public function update_predefined_reply($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'service_requests_predefined_replies', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Predefined Reply Updated [ID: ' . $id . ', ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete predefined reply
     * @param  mixed $id predefined reply id
     * @return boolean
     */
    public function delete_predefined_reply($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'service_requests_predefined_replies');
        if ($this->db->affected_rows() > 0) {
            log_activity('Predefined Reply Deleted [' . $id . ']');

            return true;
        }

        return false;
    }

    // service_request statuses

    /**
     * Get service_request status by id
     * @param  mixed $id status id
     * @return mixed     if id passed return object else array
     */
    public function get_service_request_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('service_requeststatusid', $id);

            return $this->db->get(db_prefix() . 'service_requests_status')->row();
        }
        $this->db->order_by('statusorder', 'asc');

        return $this->db->get(db_prefix() . 'service_requests_status')->result_array();
    }

    /**
     * Add new service_request status
     * @param array service_request status $_POST data
     * @return mixed
     */
    public function add_service_request_status($data)
    {
        $this->db->insert(db_prefix() . 'service_requests_status', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New service_request Status Added [ID: ' . $insert_id . ', ' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Update service_request status
     * @param  array $data service_request status $_POST data
     * @param  mixed $id   service_request status id
     * @return boolean
     */
    public function update_service_request_status($data, $id)
    {
        $this->db->where('service_requeststatusid', $id);
        $this->db->update(db_prefix() . 'service_requests_status', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('service_request Status Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete service_request status
     * @param  mixed $id service_request status id
     * @return mixed
     */
    public function delete_service_request_status($id)
    {
        $current = $this->get_service_request_status($id);
        // Default statuses cant be deleted
        if ($current->isdefault == 1) {
            return [
                'default' => true,
            ];
        // Not default check if if used in table
        } elseif (is_reference_in_table('status', db_prefix() . 'service_requests', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $this->db->where('service_requeststatusid', $id);
        $this->db->delete(db_prefix() . 'service_requests_status');
        if ($this->db->affected_rows() > 0) {
            log_activity('service_request Status Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    // service_request services
    public function get_service($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('serviceid', $id);

            return $this->db->get(db_prefix() . 'services')->row();
        }

        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix() . 'services')->result_array();
    }

    public function get_request_for($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('requestid', $id);

            return $this->db->get(db_prefix() . 'tickets_request_for')->row();
        }

        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix() . 'tickets_request_for')->result_array();
    }

    public function add_service($data)
    {
        $this->db->insert(db_prefix() . 'services', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New service_request Service Added [ID: ' . $insert_id . '.' . $data['name'] . ']');
        }

        return $insert_id;
    }

    public function update_service($data, $id)
    {
        $this->db->where('serviceid', $id);
        $this->db->update(db_prefix() . 'services', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('service_request Service Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    public function delete_service($id)
    {
        if (is_reference_in_table('service', db_prefix() . 'service_requests', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $this->db->where('serviceid', $id);
        $this->db->delete(db_prefix() . 'services');
        if ($this->db->affected_rows() > 0) {
            log_activity('service_request Service Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * @return array
     * Used in home dashboard page
     * Displays weekly service_request openings statistics (chart)
     */
    public function get_weekly_service_requests_opening_statistics()
    {
        $departments_ids = [];
        if (!is_admin()) {
            if (get_option('staff_access_only_assigned_departments') == 1) {
                $this->load->model('departments_model');
                $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                $departments_ids      = [];
                if (count($staff_deparments_ids) == 0) {
                    $departments = $this->departments_model->get();
                    foreach ($departments as $department) {
                        array_push($departments_ids, $department['departmentid']);
                    }
                } else {
                    $departments_ids = $staff_deparments_ids;
                }
            }
        }

        $chart = [
            'labels'   => get_weekdays(),
            'datasets' => [
                [
                    'label'           => _l('home_weekend_service_request_opening_statistics'),
                    'backgroundColor' => 'rgba(197, 61, 169, 0.5)',
                    'borderColor'     => '#c53da9',
                    'borderWidth'     => 1,
                    'tension'         => false,
                    'data'            => [
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    ],
                ],
            ],
        ];

        $monday = new DateTime(date('Y-m-d', strtotime('monday this week')));
        $sunday = new DateTime(date('Y-m-d', strtotime('sunday this week')));

        $thisWeekDays = get_weekdays_between_dates($monday, $sunday);

        $byDepartments = count($departments_ids) > 0;
        if (isset($thisWeekDays[1])) {
            $i = 0;
            foreach ($thisWeekDays[1] as $weekDate) {
                $this->db->like('DATE(date)', $weekDate, 'after');
                if ($byDepartments) {
                    $this->db->where('department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                }
                $chart['datasets'][0]['data'][$i] = $this->db->count_all_results(db_prefix() . 'service_requests');

                $i++;
            }
        }

        return $chart;
    }

    public function get_service_requests_assignes_disctinct()
    {
        return $this->db->query('SELECT DISTINCT(assigned) as assigned FROM ' . db_prefix() . 'service_requests WHERE assigned != 0')->result_array();
    }

    /**
     * Check for previous service_requests opened by this email/contact and link to the contact
     * @param  string $email      email to check for
     * @param  mixed $contact_id the contact id to transfer the service_requests
     * @return boolean
     */
    public function transfer_email_service_requests_to_contact($email, $contact_id)
    {
        // Some users don't want to fill the email
        if (empty($email)) {
            return false;
        }

        $customer_id = get_user_id_by_contact_id($contact_id);

        $this->db->where('userid', 0)
                ->where('contactid', 0)
                ->where('admin IS NULL')
                ->where('email', $email);

        $this->db->update(db_prefix() . 'service_requests', [
                    'email'     => null,
                    'name'      => null,
                    'userid'    => $customer_id,
                    'contactid' => $contact_id,
                ]);

        $this->db->where('userid', 0)
                ->where('contactid', 0)
                ->where('admin IS NULL')
                ->where('email', $email);

        $this->db->update(db_prefix() . 'service_request_replies', [
                    'email'     => null,
                    'name'      => null,
                    'userid'    => $customer_id,
                    'contactid' => $contact_id,
                ]);

        return true;
    }
}