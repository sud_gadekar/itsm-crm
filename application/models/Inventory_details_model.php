<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Inventory_details_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get inventory_detail(s)
     * @param  mixed $id Optional inventory_detail id
     * @return mixed     object or array
     */
    public function get($id = '', $where = [], $contact_id = '', $client_id = '')
    {
             
        $this->db->select('*,tblinventory_details.id,tblinventory_manufacture.manufacture_name, tblinventory_statuses.status_name, type.type_name as type, subtype.type_name as subtype_name')
              ->from(db_prefix() . 'inventory_details')
              
              ->join('tblinventory_types as type','tblinventory_details.type = type.id','left')
              ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id','left')
              ->join(db_prefix() . 'client_locations','tblinventory_details.location = tblclient_locations.id','left')
              ->join(db_prefix() . 'contracts','tblinventory_details.support_contract = tblcontracts.id','left')
              ->join(db_prefix() . 'clients','tblinventory_details.clientid = tblclients.userid','left') 
              ->join('tblinventory_manufacture','tblinventory_details.manufacture = tblinventory_manufacture.id','left')
              
              ->join('tblinventory_statuses','tblinventory_details.status = tblinventory_statuses.id','left')
              ->join('tblinventory_types as subtype','tblinventory_details.sub_type = subtype.id','left')
              
              ->where($where);
        if ($id) {
            $this->db->where('tblinventory_details.id', $id);
        }
        if($client_id){
            $this->db->where(db_prefix() . 'inventory_details.clientid', $client_id);
        }

        return $this->db->get()->result_array();
    }

    


    public function get_by_customer($id)
    {
        //print_r($id);die();
        $this->db->select('*,tblinventory_details.id, type.type_name as type, subtype.type_name as subtype_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_types as type','tblinventory_details.type = type.id','left')
                ->join(db_prefix() . 'inventory_types as subtype','tblinventory_details.sub_type = subtype.id','left')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id','left')
                ->join(db_prefix() . 'client_locations','tblinventory_details.location = tblclient_locations.id','left')
                ->join(db_prefix() . 'contracts','tblinventory_details.support_contract = tblcontracts.id','left')
                ->join(db_prefix() . 'clients','tblinventory_details.clientid = tblclients.userid','left')
                ->where('tblinventory_details.clientid',$id);


        return $this->db->get()->result_array();
    }

    /**
     * Add new inventory_detail
     * @param mixed $data All $_POST data
     * @return  mixed
     */
    public function add($data)
    {
        //$data['date'] = to_sql_date($data['date']);
        //$data['note'] = nl2br($data['note']);
        // if (isset($data['billable'])) {
        //     $data['billable'] = 1;
        // } else {
        //     $data['billable'] = 0;
        // }
        // if (isset($data['create_invoice_billable'])) {
        //     $data['create_invoice_billable'] = 1;
        // } else {
        //     $data['create_invoice_billable'] = 0;
        // }
        // if (isset($data['custom_fields'])) {
        //     $custom_fields = $data['custom_fields'];
        //     unset($data['custom_fields']);
        // }
        // if (isset($data['send_invoice_to_customer'])) {
        //     $data['send_invoice_to_customer'] = 1;
        // } else {
        //     $data['send_invoice_to_customer'] = 0;
        // }

        // if (isset($data['repeat_every']) && $data['repeat_every'] != '') {
        //     $data['recurring'] = 1;
        //     if ($data['repeat_every'] == 'custom') {
        //         $data['repeat_every']     = $data['repeat_every_custom'];
        //         $data['recurring_type']   = $data['repeat_type_custom'];
        //         $data['custom_recurring'] = 1;
        //     } else {
        //         $_temp                    = explode('-', $data['repeat_every']);
        //         $data['recurring_type']   = $_temp[1];
        //         $data['repeat_every']     = $_temp[0];
        //         $data['custom_recurring'] = 0;
        //     }
        // } else {
        //     $data['recurring'] = 0;
        // }
        // unset($data['repeat_type_custom']);
        // unset($data['repeat_every_custom']);

        // if ((isset($data['project_id']) && $data['project_id'] == '') || !isset($data['project_id'])) {
        //     $data['project_id'] = 0;
        // }
        // $data['addedfrom'] = get_staff_user_id();
        // $data['dateadded'] = date('Y-m-d H:i:s');
        $this->db->insert(db_prefix() . 'inventory_details', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            if (isset($data['project_id']) && !empty($data['project_id'])) {
                $this->load->model('projects_model');
                $project_settings = $this->projects_model->get_project_settings($data['project_id']);
                $visible_activity = 0;
                foreach ($project_settings as $s) {
                    if ($s['name'] == 'view_finance_overview') {
                        if ($s['value'] == 1) {
                            $visible_activity = 1;

                            break;
                        }
                    }
                }
                $inventory_detail                  = $this->get($insert_id);
                $activity_additional_data = $inventory_detail->name . '<br />';
                $activity_additional_data .= app_format_money($inventory_detail->amount, $inventory_detail->currency_data->name);
                $this->projects_model->log_activity($data['project_id'], 'project_activity_recorded_inventory_detail', $activity_additional_data, $visible_activity);
            }
            log_activity('New inventory_detail Added [' . $insert_id . ']');

            return $insert_id;
        }

        return false;
    }

    public function get_category($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'inventory_details_categories')->row();
        }
        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix() . 'inventory_details_categories')->result_array();
    }

    public function get_child_inventory_details($id)
    {
        $this->db->select('id');
        $this->db->where('recurring_from', $id);
        $inventory_details = $this->db->get(db_prefix() . 'inventory_details')->result_array();

        $_inventory_details = [];
        foreach ($inventory_details as $inventory_detail) {
            $_inventory_details[] = $this->get($inventory_detail['id']);
        }

        return $_inventory_details;
    }

    public function get_inventory_details_total($data)
    {
        $this->load->model('currencies_model');
        $base_currency     = $this->currencies_model->get_base_currency()->id;
        $base              = true;
        $currency_switcher = false;
        if (isset($data['currency'])) {
            $currencyid        = $data['currency'];
            $currency_switcher = true;
        } elseif (isset($data['customer_id']) && $data['customer_id'] != '') {
            $currencyid = $this->clients_model->get_customer_default_currency($data['customer_id']);
            if ($currencyid == 0) {
                $currencyid = $base_currency;
            } else {
                if (total_rows(db_prefix() . 'inventory_details', [
                    'currency' => $base_currency,
                    'clientid' => $data['customer_id'],
                ])) {
                    $currency_switcher = true;
                }
            }
        } elseif (isset($data['project_id']) && $data['project_id'] != '') {
            $this->load->model('projects_model');
            $currencyid = $this->projects_model->get_currency($data['project_id'])->id;
        } else {
            $currencyid = $base_currency;
            if (total_rows(db_prefix() . 'inventory_details', [
                'currency !=' => $base_currency,
            ])) {
                $currency_switcher = true;
            }
        }

        $currency = get_currency($currencyid);

        $has_permission_view = has_permission('inventory_details', '', 'view');
        $_result             = [];

        for ($i = 1; $i <= 5; $i++) {
            $this->db->select('amount,tax,tax2,invoiceid');
            $this->db->where('currency', $currencyid);

            if (isset($data['years']) && count($data['years']) > 0) {
                $this->db->where('YEAR(date) IN (' . implode(', ', array_map(function ($year) {
                    return get_instance()->db->escape_str($year);
                }, $data['years'])) . ')');
            } else {
                $this->db->where('YEAR(date) = ' . date('Y'));
            }
            if (isset($data['customer_id']) && $data['customer_id'] != '') {
                $this->db->where('clientid', $data['customer_id']);
            }
            if (isset($data['project_id']) && $data['project_id'] != '') {
                $this->db->where('project_id', $data['project_id']);
            }

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }
            switch ($i) {
                case 1:
                    $key = 'all';

                    break;
                case 2:
                    $key = 'billable';
                    $this->db->where('billable', 1);

                    break;
                case 3:
                    $key = 'non_billable';
                    $this->db->where('billable', 0);

                    break;
                case 4:
                    $key = 'billed';
                    $this->db->where('billable', 1);
                    $this->db->where('invoiceid IS NOT NULL');
                    $this->db->where('invoiceid IN (SELECT invoiceid FROM ' . db_prefix() . 'invoices WHERE status=2 AND id=' . db_prefix() . 'inventory_details.invoiceid)');

                    break;
                case 5:
                    $key = 'unbilled';
                    $this->db->where('billable', 1);
                    $this->db->where('invoiceid IS NULL');

                    break;
            }
            $all_inventory_details = $this->db->get(db_prefix() . 'inventory_details')->result_array();
            $_total_all   = [];
            $cached_taxes = [];
            foreach ($all_inventory_details as $inventory_detail) {
                $_total = $inventory_detail['amount'];
                if ($inventory_detail['tax'] != 0) {
                    if (!isset($cached_taxes[$inventory_detail['tax']])) {
                        $tax                           = get_tax_by_id($inventory_detail['tax']);
                        $cached_taxes[$inventory_detail['tax']] = $tax;
                    } else {
                        $tax = $cached_taxes[$inventory_detail['tax']];
                    }
                    $_total += ($_total / 100 * $tax->taxrate);
                }
                if ($inventory_detail['tax2'] != 0) {
                    if (!isset($cached_taxes[$inventory_detail['tax2']])) {
                        $tax                            = get_tax_by_id($inventory_detail['tax2']);
                        $cached_taxes[$inventory_detail['tax2']] = $tax;
                    } else {
                        $tax = $cached_taxes[$inventory_detail['tax2']];
                    }
                    $_total += ($inventory_detail['amount'] / 100 * $tax->taxrate);
                }
                array_push($_total_all, $_total);
            }
            $_result[$key]['total'] = app_format_money(array_sum($_total_all), $currency);
        }
        $_result['currency_switcher'] = $currency_switcher;
        $_result['currencyid']        = $currencyid;

        // All inventory_details
        /*   $this->db->select('amount,tax');
        $this->db->where('currency',$currencyid);


        $this->db->select('amount,tax,invoiceid');
        $this->db->where('currency',$currencyid);
        $this->db->where('billable',1);
        $this->db->where('invoiceid IS NOT NULL');

        $all_inventory_details = $this->db->get(db_prefix().'inventory_details')->result_array();
        $_total_all = array();
        foreach($all_inventory_details as $inventory_detail){
        $_total = 0;
        if(total_rows(db_prefix().'invoices',array('status'=>2,'id'=>$inventory_detail['invoiceid'])) > 0){
        $_total = $inventory_detail['amount'];
        if($inventory_detail['tax'] != 0){
        $tax = get_tax_by_id($inventory_detail['tax']);
        $_total += ($_total / 100 * $tax->taxrate);
        }
        }
        array_push($_total_all,$_total);
        }
        $_result['billed']['total'] = app_format_money(array_sum($_total_all), $currency);

        $this->db->select('amount,tax,invoiceid');
        $this->db->where('currency',$currencyid);
        $this->db->where('billable',1);
        $this->db->where('invoiceid IS NOT NULL');

        $all_inventory_details = $this->db->get(db_prefix().'inventory_details')->result_array();
        $_total_all = array();
        foreach($all_inventory_details as $inventory_detail){
        $_total = 0;
        if(total_rows(db_prefix().'invoices','status NOT IN(2,5) AND id ='.$inventory_detail['invoiceid']) > 0){
        echo $this->db->last_query();
        $_total = $inventory_detail['amount'];
        if($inventory_detail['tax'] != 0){
        $tax = get_tax_by_id($inventory_detail['tax']);
        $_total += ($_total / 100 * $tax->taxrate);
        }
        }
        array_push($_total_all,$_total);
        }
        $_result['unbilled']['total'] = app_format_money(array_sum($_total_all), $currency);*/

        return $_result;
    }



    //Get Inventory

     public function get_inventory_type($id = '')
        {
            // print_r('Hi');die();
            if (is_numeric($id)) {
                $this->db->where('parent_id', $id);

                return $this->db->get(db_prefix() . 'inventory_types')->result_array();
            }

            return $this->db->get(db_prefix() . 'inventory_types')->result_array();
        }




    //Get Inventory

     public function get_inventory_subtype($id = '')
        {
            // print_r('Hi');die();
            if (is_numeric($id)) {
                $this->db->where('parent_id!=', $id);

                return $this->db->get(db_prefix() . 'inventory_types')->result_array();
            }

            return $this->db->get(db_prefix() . 'inventory_types')->result_array();
        }


      public function  get_inventory_manufacture($id = '')
      {
        if (is_numeric($id)) {
                $this->db->where('id', $id);

                return $this->db->get(db_prefix() . 'inventory_manufacture')->row();
            }
            return $this->db->get(db_prefix() . 'inventory_manufacture')->result_array();
      }

       public function  get_inventory_product($id = '')
      {
        if (is_numeric($id)) {
                $this->db->where('id', $id);

                return $this->db->get(db_prefix() . 'inventory_products')->row();
            }

            return $this->db->get(db_prefix() . 'inventory_products')->result_array();
      }

      public function  get_inventory_status($id = '')
      {
        if (is_numeric($id)) {
                $this->db->where('id', $id);

                return $this->db->get(db_prefix() . 'inventory_statuses')->row();
            }

            return $this->db->get(db_prefix() . 'inventory_statuses')->result_array();
      }

    /**
     * Update inventory_detail
     * @param  mixed $data All $_POST data
     * @param  mixed $id   inventory_detail id to update
     * @return boolean
     */
    public function update($data, $id)
    {
        $original_inventory_detail = $this->get($id);

        $affectedRows = 0;
        //$data['date'] = to_sql_date($data['date']);
        //$data['note'] = nl2br($data['note']);

        // Recurring inventory_detail set to NO, Cancelled
        // if ($original_inventory_detail->repeat_every != '' && $data['repeat_every'] == '') {
        //     $data['cycles']              = 0;
        //     $data['total_cycles']        = 0;
        //     $data['last_recurring_date'] = null;
        // }

        // if ($data['repeat_every'] != '') {
        //     $data['recurring'] = 1;
        //     if ($data['repeat_every'] == 'custom') {
        //         $data['repeat_every']     = $data['repeat_every_custom'];
        //         $data['recurring_type']   = $data['repeat_type_custom'];
        //         $data['custom_recurring'] = 1;
        //     } else {
        //         $_temp                    = explode('-', $data['repeat_every']);
        //         $data['recurring_type']   = $_temp[1];
        //         $data['repeat_every']     = $_temp[0];
        //         $data['custom_recurring'] = 0;
        //     }
        // } else {
        //     $data['recurring'] = 0;
        // }

        //$data['cycles'] = !isset($data['cycles']) || $data['recurring'] == 0 ? 0 : $data['cycles'];

        //unset($data['repeat_type_custom']);
        //unset($data['repeat_every_custom']);

        // if (isset($data['custom_fields'])) {
        //     $custom_fields = $data['custom_fields'];
        //     if (handle_custom_fields_post($id, $custom_fields)) {
        //         $affectedRows++;
        //     }
        //     unset($data['custom_fields']);
        // }

        // if (isset($data['create_invoice_billable'])) {
        //     $data['create_invoice_billable'] = 1;
        // } else {
        //     $data['create_invoice_billable'] = 0;
        // }

        // if (isset($data['billable'])) {
        //     $data['billable'] = 1;
        // } else {
        //     $data['billable'] = 0;
        // }

        // if (isset($data['send_invoice_to_customer'])) {
        //     $data['send_invoice_to_customer'] = 1;
        // } else {
        //     $data['send_invoice_to_customer'] = 0;
        // }

        // if (isset($data['project_id']) && $data['project_id'] == '' || !isset($data['project_id'])) {
        //     $data['project_id'] = 0;
        // }

        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'inventory_details', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('inventory_detail Updated [' . $id . ']');
            $affectedRows++;
        }

        if ($affectedRows > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param  integer ID
     * @return mixed
     * Delete inventory_detail from database, if used return
     */
    public function delete($id, $simpleDelete = false)
    {
        $_inventory_detail = $this->get($id);

        if ($_inventory_detail->invoiceid !== null && $simpleDelete == false) {
            return [
                'invoiced' => true,
            ];
        }

        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'inventory_details');

        if ($this->db->affected_rows() > 0) {
            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'inventory_details');
            $this->db->delete(db_prefix() . 'customfieldsvalues');
            // Get related tasks
            $this->db->where('rel_type', 'inventory_detail');
            $this->db->where('rel_id', $id);
            $tasks = $this->db->get(db_prefix() . 'tasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }

            $this->delete_inventory_detail_attachment($id);

            $this->db->where('recurring_from', $id);
            $this->db->update(db_prefix() . 'inventory_details', ['recurring_from' => null]);

            $this->db->where('rel_type', 'inventory_detail');
            $this->db->where('rel_id', $id);
            $this->db->delete(db_prefix() . 'reminders');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'inventory_detail');
            $this->db->delete(db_prefix() . 'related_items');

            log_activity('inventory_detail Deleted [' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Convert inventory_detail to invoice
     * @param  mixed  $id   inventory_detail id
     * @return mixed
     */
    public function convert_to_invoice($id, $draft_invoice = false, $params = [])
    {
        $inventory_detail          = $this->get($id);
        $new_invoice_data = [];
        $client           = $this->clients_model->get($inventory_detail->clientid);

        if ($draft_invoice == true) {
            $new_invoice_data['save_as_draft'] = true;
        }
        $new_invoice_data['clientid'] = $inventory_detail->clientid;
        $new_invoice_data['number']   = get_option('next_invoice_number');
        $invoice_date                 = (isset($params['invoice_date']) ? $params['invoice_date'] : date('Y-m-d'));
        $new_invoice_data['date']     = _d($invoice_date);

        if (get_option('invoice_due_after') != 0) {
            $new_invoice_data['duedate'] = _d(date('Y-m-d', strtotime('+' . get_option('invoice_due_after') . ' DAY', strtotime($invoice_date))));
        }

        $new_invoice_data['show_quantity_as'] = 1;
        $new_invoice_data['terms']            = get_option('predefined_terms_invoice');
        $new_invoice_data['clientnote']       = get_option('predefined_clientnote_invoice');
        $new_invoice_data['discount_total']   = 0;
        $new_invoice_data['sale_agent']       = 0;
        $new_invoice_data['adjustment']       = 0;
        $new_invoice_data['project_id']       = $inventory_detail->project_id;

        $new_invoice_data['subtotal'] = $inventory_detail->amount;
        $total                        = $inventory_detail->amount;

        if ($inventory_detail->tax != 0) {
            $total += ($inventory_detail->amount / 100 * $inventory_detail->taxrate);
        }
        if ($inventory_detail->tax2 != 0) {
            $total += ($inventory_detail->amount / 100 * $inventory_detail->taxrate2);
        }

        $new_invoice_data['total']     = $total;
        $new_invoice_data['currency']  = $inventory_detail->currency;
        $new_invoice_data['status']    = 1;
        $new_invoice_data['adminnote'] = '';
        // Since version 1.0.6
        $new_invoice_data['billing_street']  = clear_textarea_breaks($client->billing_street);
        $new_invoice_data['billing_city']    = $client->billing_city;
        $new_invoice_data['billing_state']   = $client->billing_state;
        $new_invoice_data['billing_zip']     = $client->billing_zip;
        $new_invoice_data['billing_country'] = $client->billing_country;
        if (!empty($client->shipping_street)) {
            $new_invoice_data['shipping_street']          = clear_textarea_breaks($client->shipping_street);
            $new_invoice_data['shipping_city']            = $client->shipping_city;
            $new_invoice_data['shipping_state']           = $client->shipping_state;
            $new_invoice_data['shipping_zip']             = $client->shipping_zip;
            $new_invoice_data['shipping_country']         = $client->shipping_country;
            $new_invoice_data['include_shipping']         = 1;
            $new_invoice_data['show_shipping_on_invoice'] = 1;
        } else {
            $new_invoice_data['include_shipping']         = 0;
            $new_invoice_data['show_shipping_on_invoice'] = 1;
        }

        $this->load->model('payment_modes_model');
        $modes = $this->payment_modes_model->get('', [
            'inventory_details_only !=' => 1,
        ]);
        $temp_modes = [];
        foreach ($modes as $mode) {
            if ($mode['selected_by_default'] == 0) {
                continue;
            }
            $temp_modes[] = $mode['id'];
        }

        $new_invoice_data['billed_inventory_details'][1] = [
            $inventory_detail->inventory_detailid,
        ];
        $new_invoice_data['allowed_payment_modes']           = $temp_modes;
        $new_invoice_data['newitems'][1]['description']      = _l('item_as_inventory_detail') . ' ' . $inventory_detail->name;
        $new_invoice_data['newitems'][1]['long_description'] = $inventory_detail->description;

        if (isset($params['include_note']) && $params['include_note'] == true && !empty($inventory_detail->note)) {
            $new_invoice_data['newitems'][1]['long_description'] .= PHP_EOL . $inventory_detail->note;
        }
        if (isset($params['include_name']) && $params['include_name'] == true && !empty($inventory_detail->inventory_detail_name)) {
            $new_invoice_data['newitems'][1]['long_description'] .= PHP_EOL . $inventory_detail->inventory_detail_name;
        }

        $new_invoice_data['newitems'][1]['unit']    = '';
        $new_invoice_data['newitems'][1]['qty']     = 1;
        $new_invoice_data['newitems'][1]['taxname'] = [];
        if ($inventory_detail->tax != 0) {
            $tax_data = get_tax_by_id($inventory_detail->tax);
            array_push($new_invoice_data['newitems'][1]['taxname'], $tax_data->name . '|' . $tax_data->taxrate);
        }
        if ($inventory_detail->tax2 != 0) {
            $tax_data = get_tax_by_id($inventory_detail->tax2);
            array_push($new_invoice_data['newitems'][1]['taxname'], $tax_data->name . '|' . $tax_data->taxrate);
        }

        $new_invoice_data['newitems'][1]['rate']  = $inventory_detail->amount;
        $new_invoice_data['newitems'][1]['order'] = 1;
        $this->load->model('invoices_model');

        $invoiceid = $this->invoices_model->add($new_invoice_data, true);
        if ($invoiceid) {
            $this->db->where('id', $inventory_detail->inventory_detailid);
            $this->db->update(db_prefix() . 'inventory_details', [
                'invoiceid' => $invoiceid,
            ]);

            if (is_custom_fields_smart_transfer_enabled()) {
                $this->db->where('fieldto', 'inventory_details');
                $this->db->where('active', 1);
                $cfinventory_details = $this->db->get(db_prefix() . 'customfields')->result_array();
                foreach ($cfinventory_details as $field) {
                    $tmpSlug = explode('_', $field['slug'], 2);
                    if (isset($tmpSlug[1])) {
                        $this->db->where('fieldto', 'invoice');
                        $this->db->group_start();
                        $this->db->like('slug', 'invoice_' . $tmpSlug[1], 'after');
                        $this->db->where('type', $field['type']);
                        $this->db->where('options', $field['options']);
                        $this->db->where('active', 1);
                        $this->db->group_end();

                        $cfTransfer = $this->db->get(db_prefix() . 'customfields')->result_array();

                        // Don't make mistakes
                        // Only valid if 1 result returned
                        // + if field names similarity is equal or more then CUSTOM_FIELD_TRANSFER_SIMILARITY%
                        if (count($cfTransfer) == 1 && ((similarity($field['name'], $cfTransfer[0]['name']) * 100) >= CUSTOM_FIELD_TRANSFER_SIMILARITY)) {
                            $value = get_custom_field_value($id, $field['id'], 'inventory_details', false);
                            if ($value == '') {
                                continue;
                            }
                            $this->db->insert(db_prefix() . 'customfieldsvalues', [
                                'relid'   => $invoiceid,
                                'fieldid' => $cfTransfer[0]['id'],
                                'fieldto' => 'invoice',
                                'value'   => $value,
                            ]);
                        }
                    }
                }
            }

            log_activity('inventory_detail Converted To Invoice [inventory_detailID: ' . $inventory_detail->inventory_detailid . ', InvoiceID: ' . $invoiceid . ']');

            hooks()->do_action('inventory_detail_converted_to_invoice', ['inventory_detail_id' => $inventory_detail->inventory_detailid, 'invoice_id' => $invoiceid]);

            return $invoiceid;
        }

        return false;
    }

    /**
     * Copy inventory_detail
     * @param  mixed $id inventory_detail id to copy from
     * @return mixed
     */
    public function copy($id)
    {
        $inventory_detail_fields   = $this->db->list_fields(db_prefix() . 'inventory_details');
        $inventory_detail          = $this->get($id);
        $new_inventory_detail_data = [];
        foreach ($inventory_detail_fields as $field) {
            if (isset($inventory_detail->$field)) {
                // We dont need these fields.
                if ($field != 'invoiceid' && $field != 'id' && $field != 'recurring_from') {
                    $new_inventory_detail_data[$field] = $inventory_detail->$field;
                }
            }
        }
        $new_inventory_detail_data['addedfrom']           = get_staff_user_id();
        $new_inventory_detail_data['dateadded']           = date('Y-m-d H:i:s');
        $new_inventory_detail_data['last_recurring_date'] = null;
        $new_inventory_detail_data['total_cycles']        = 0;

        $this->db->insert(db_prefix() . 'inventory_details', $new_inventory_detail_data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            // Get the old inventory_detail custom field and add to the new
            $custom_fields = get_custom_fields('inventory_details');
            foreach ($custom_fields as $field) {
                $value = get_custom_field_value($id, $field['id'], 'inventory_details', false);
                if ($value == '') {
                    continue;
                }
                $this->db->insert(db_prefix() . 'customfieldsvalues', [
                    'relid'   => $insert_id,
                    'fieldid' => $field['id'],
                    'fieldto' => 'inventory_details',
                    'value'   => $value,
                ]);
            }
            log_activity('inventory_detail Copied [inventory_detailID' . $id . ', Newinventory_detailID: ' . $insert_id . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Delete inventory_detail attachment
     * @param  mixed $id inventory_detail id
     * @return boolean
     */
    public function delete_inventory_detail_attachment($id)
    {
        if (is_dir(get_upload_path_by_type('inventory_detail') . $id)) {
            if (delete_dir(get_upload_path_by_type('inventory_detail') . $id)) {
                $this->db->where('rel_id', $id);
                $this->db->where('rel_type', 'inventory_detail');
                $this->db->delete(db_prefix() . 'files');
                log_activity('inventory_detail Receipt Deleted [inventory_detailID: ' . $id . ']');

                return true;
            }
        }

        return false;
    }

    public function get_inventory_details_years()
    {
        return $this->db->query('SELECT DISTINCT(YEAR(created_at)) as year FROM ' . db_prefix() . 'inventory_details ORDER by year DESC')->result_array();
    }
}
