<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Newsletters_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get newsletter by id
     * @param  string $id   newsletter ID
     * @param  string $slug if search by slug
     * @return mixed       if ID or slug passed return object else array
     */
    public function get($id = '')
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'newsletters');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result_array();
    }

    public function app_get($id = '')
    {
        $this->db->select('*, concat("/uploads/newsletters/",id,"/",nl_file) as file_path');
        $this->db->from(db_prefix() . 'newsletters');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }
        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'newsletters');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('tblnewsletters.clientid', $id);
        }
        return $this->db->get()->result_array();
    }

    /**
     * Get related artices based on newsletter id
     * @param  mixed $current_id current newsletter id
     * @return array
     */
    public function get_related_newsletters($current_id, $customers = true)
    {
        $total_related_newsletters = hooks()->apply_filters('total_related_newsletters', 5);

        $this->db->select('newslettergroup');
        $this->db->where('id', $current_id);
        $newsletter = $this->db->get(db_prefix() . 'newsletters')->row();

        $this->db->where('newslettergroup', $newsletter->newslettergroup);
        $this->db->where('id !=', $current_id);
        $this->db->where('active', 1);
        if ($customers == true) {
            $this->db->where('staff_newsletter', 0);
        } else {
            $this->db->where('staff_newsletter', 1);
        }
        $this->db->limit($total_related_newsletters);

        return $this->db->get(db_prefix() . 'newsletters')->result_array();
    }

    /**
     * Add new newsletter
     * @param array $data newsletter data
     */
    public function add_newsletter($data)
    {
        $data['created_by'] = get_staff_user_id();
        //$data = hooks()->apply_filters('before_add_kb_newsletter', $data);

        $this->db->insert(db_prefix() . 'newsletters', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Newsletter Added [id: ' . $insert_id . ']');
        }

        return $insert_id;
    }

    /**
     * Update newsletter
     * @param  array $data newsletter data
     * @param  mixed $id   id
     * @return boolean
     */
    public function update_newsletter($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'newsletters', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Newsletter Updated [id: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Change newsletter status
     * @param  mixed $id     newsletter id
     * @param  boolean $status is active or not
     */
    public function change_newsletter_status($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'newsletters', [
            'active' => $status,
        ]);
        log_activity('newsletter Status Changed [id: ' . $id . ' Status: ' . $status . ']');
    }

    public function update_groups_order()
    {
        $data = $this->input->post();
        foreach ($data['order'] as $group) {
            $this->db->where('groupid', $group[0]);
            $this->db->update(db_prefix() . 'newsletters_groups', [
                'group_order' => $group[1],
            ]);
        }
    }

    /**
     * Delete newsletter from database and all newsletter connections
     * @param  mixed $id newsletter ID
     * @return boolean
     */
    public function delete_newsletter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'newsletters');
        if ($this->db->affected_rows() > 0) {
            log_activity('Newsletter Deleted [id: ' . $id . ']');
            return true;
        }
        return false;
    }
}
