<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_tracker_model extends App_Model
{
    private $piping = false;

    public function __construct()
    {
        parent::__construct(); 
    }

    
    public function get($id = '', $where = [], $contact_id = '', $client_id = '')
    {
        $this->db->select('tblorder_tracker.*, tblclients.company, tblorder_status.name, tblservice_requests.subject, tblprojects.name as project')
                ->from(db_prefix() . 'order_tracker')
                ->join(db_prefix() . 'clients','tblorder_tracker.client_id = tblclients.userid', 'left')
                ->join(db_prefix() . 'order_status','tblorder_tracker.status_id = tblorder_status.statusid', 'left')
                ->join(db_prefix() . 'service_requests','tblorder_tracker.sr_id = tblservice_requests.service_requestid', 'left')
                ->join(db_prefix() . 'projects','tblorder_tracker.project_id = tblprojects.id', 'left')
                ->where($where);
        if($client_id){
            $this->db->where(db_prefix() . 'order_tracker.client_id', $client_id);
        }
        if ($id) {
            $data['order_details'] = $this->db->where('tblorder_tracker.id', $id)->get()->result_array();

            $data['notes'] = $this->db->where('order_id',$id)->where('share_to_customer',1)->select('*')->get('tblorder_tracker_notes')->result_array();
            $data['order_items'] = $this->db->select('tblorder_tracker_items.*, tblitems.description')
                    ->from(db_prefix() . 'order_tracker_items')
                    ->join(db_prefix() . 'items','tblorder_tracker_items.item_id = tblitems.id','left')
                    ->where('order_id', $id)->get()->result_array();

            return $data;
        }

        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('tblorder_tracker.*, tblclients.company, tblorder_status.name, tblservice_requests.subject, tblprojects.name as project')
                ->from(db_prefix() . 'order_tracker')
                ->join(db_prefix() . 'clients','tblorder_tracker.client_id = tblclients.userid', 'left')
                ->join(db_prefix() . 'order_status','tblorder_tracker.status_id = tblorder_status.statusid', 'left')
                ->join(db_prefix() . 'service_requests','tblorder_tracker.sr_id = tblservice_requests.service_requestid', 'left')
                ->join(db_prefix() . 'projects','tblorder_tracker.project_id = tblprojects.id', 'left')
                ->where('tblorder_tracker.client_id',$id);

        return $this->db->get()->result_array();
    }

    public function get_by_order($id)
    {
        $this->db->select('tblorder_tracker.*, tblclients.company, tblorder_status.name, tblservice_requests.subject, tblprojects.name as project')
                ->from(db_prefix() . 'order_tracker')
                ->join(db_prefix() . 'clients','tblorder_tracker.client_id = tblclients.userid', 'left')
                ->join(db_prefix() . 'order_status','tblorder_tracker.status_id = tblorder_status.statusid', 'left')
                ->join(db_prefix() . 'service_requests','tblorder_tracker.sr_id = tblservice_requests.service_requestid', 'left')
                ->join(db_prefix() . 'projects','tblorder_tracker.project_id = tblprojects.id', 'left')
                ->where('tblorder_tracker.order_completed',$id);

        return $this->db->get()->result_array();
    }

    public function get_by_status($id)
    {
        $this->db->select('tblorder_tracker.*, tblclients.company, tblorder_status.name, tblservice_requests.subject, tblprojects.name as project')
                ->from(db_prefix() . 'order_tracker')
                ->join(db_prefix() . 'clients','tblorder_tracker.client_id = tblclients.userid', 'left')
                ->join(db_prefix() . 'order_status','tblorder_tracker.status_id = tblorder_status.statusid', 'left')
                ->join(db_prefix() . 'service_requests','tblorder_tracker.sr_id = tblservice_requests.service_requestid', 'left')
                ->join(db_prefix() . 'projects','tblorder_tracker.project_id = tblprojects.id', 'left')
                ->where('tblorder_tracker.status_id',$id);

        return $this->db->get()->result_array();
    }

    /**
     * Add new order
     * @param mixed $data All $_POST data
     * @return  mixed
     */
    public function add($data)
    {
        for($i=0; $i<=$data['itemcount']; $i++){
            unset($data['item_id'.$i], $data['quantity'.$i], $data['vendor_name'.$i], $data['expected_delivery_date'.$i], $data['delivery_date'.$i], $data['item_received'.$i], $data['item_delivered'.$i]);
        }
        unset($data['itemcount']);

        $this->db->insert(db_prefix() . 'order_tracker', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New Order Added [' . $insert_id . ']');
            return $insert_id;
        }
        return false;
    }
    
    public function add_tracking_status($data)
    {
        $this->db->insert(db_prefix() . 'order_tracking_status', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New Order Status Added [' . $insert_id . ']');
            return $insert_id;
        }
        return false;
    }


    // order statuses
    /**
     * Get order status by id
     * @param  mixed $id status id
     * @return mixed     if id passed return object else array
     */
    public function get_order_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('statusid', $id);

            return $this->db->get(db_prefix() . 'order_status')->row();
        }

        return $this->db->get(db_prefix() . 'order_status')->result_array();
    }

    /**
     * Add new order status
     * @param array order status $_POST data
     * @return mixed
     */
    public function add_order_status($data)
    {
        $this->db->insert(db_prefix() . 'order_status', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Order Status Added [ID: ' . $insert_id . ', ' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Update order status
     * @param  array $data order status $_POST data
     * @param  mixed $id   order status id
     * @return boolean
     */
    public function update_order_status($data, $id)
    {
        $this->db->where('statusid', $id);
        $this->db->update(db_prefix() . 'order_status', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Order Status Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete order status
     * @param  mixed $id order status id
     * @return mixed
     */
    public function delete_order_status($id)
    {
        $current = $this->get_order_status($id);
        // Default statuses cant be deleted
        if ($current->isdefault == 1) {
            return [
                'default' => true,
            ];
        // Not default check if if used in table
        } elseif (is_reference_in_table('status', db_prefix() . 'order_tracker', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $this->db->where('statusid', $id);
        $this->db->delete(db_prefix() . 'order_status');
        if ($this->db->affected_rows() > 0) {
            log_activity('Order Status Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }
}