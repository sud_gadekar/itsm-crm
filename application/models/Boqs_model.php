<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boqs_model extends App_Model
{
    private $statuses;

    private $copy = false;

    public function __construct()
    {
        parent::__construct();
        $this->statuses = hooks()->apply_filters('before_set_boq_statuses', [
            6,
            4,
            1,
            5,
            2,
            3,
        ]);
    }

    public function get_statuses()
    {
        return $this->statuses;
    }

    public function get_sale_agents()
    {
        return $this->db->query('SELECT DISTINCT(assigned) as sale_agent FROM ' . db_prefix() . 'boqs WHERE assigned != 0')->result_array();
    }

    public function get_boqs_years()
    {
        return $this->db->query('SELECT DISTINCT(YEAR(date)) as year FROM ' . db_prefix() . 'boqs')->result_array();
    }

    public function do_kanban_query($status, $search = '', $page = 1, $sort = [], $count = false)
    {
        $default_pipeline_order      = get_option('default_boqs_pipeline_sort');
        $default_pipeline_order_type = get_option('default_boqs_pipeline_sort_type');
        $limit                       = get_option('boqs_pipeline_limit');

        $has_permission_view                 = has_permission('boqs', '', 'view');
        $has_permission_view_own             = has_permission('boqs', '', 'view_own');
        $allow_staff_view_boqs_assigned = get_option('allow_staff_view_boqs_assigned');
        $staffId                             = get_staff_user_id();

        $noPermissionQuery = get_boqs_sql_where_staff(get_staff_user_id());

        $this->db->select('id,invoice_id,estimate_id,subject,rel_type,rel_id,total,date,open_till,currency,boq_to,status');
        $this->db->from(db_prefix() . 'boqs');
        $this->db->where('status', $status);
        if (!$has_permission_view) {
            $this->db->where($noPermissionQuery);
        }
        if ($search != '') {
            if (!startsWith($search, '#')) {
                $this->db->where('(
                phone LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                zip LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                content LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                state LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                city LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                email LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                address LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                boq_to LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                total LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\'
                OR
                subject LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE \'!\')');
            } else {
                $this->db->where(db_prefix() . 'boqs.id IN
                (SELECT rel_id FROM ' . db_prefix() . 'taggables WHERE tag_id IN
                (SELECT id FROM ' . db_prefix() . 'tags WHERE name="' . $this->db->escape_str(strafter($search, '#')) . '")
                AND ' . db_prefix() . 'taggables.rel_type=\'boq\' GROUP BY rel_id HAVING COUNT(tag_id) = 1)
                ');
            }
        }

        if (isset($sort['sort_by']) && $sort['sort_by'] && isset($sort['sort']) && $sort['sort']) {
            $this->db->order_by($sort['sort_by'], $sort['sort']);
        } else {
            $this->db->order_by($default_pipeline_order, $default_pipeline_order_type);
        }

        if ($count == false) {
            if ($page > 1) {
                $page--;
                $position = ($page * $limit);
                $this->db->limit($limit, $position);
            } else {
                $this->db->limit($limit);
            }
        }

        if ($count == false) {
            return $this->db->get()->result_array();
        }

        return $this->db->count_all_results();
    }

    /**
     * Inserting new boq function
     * @param mixed $data $_POST data
     */
    public function add($data)
    {
        $data['allow_comments'] = isset($data['allow_comments']) ? 1 : 0;

        $save_and_send = isset($data['save_and_send']);

        $tags = isset($data['tags']) ? $data['tags'] : '';

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        $data['address'] = trim($data['address']);
        $data['address'] = nl2br($data['address']);

        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['addedfrom']   = get_staff_user_id();
        $data['hash']        = app_generate_hash();

        if (empty($data['rel_type'])) {
            unset($data['rel_type']);
            unset($data['rel_id']);
        } else {
            if (empty($data['rel_id'])) {
                unset($data['rel_type']);
                unset($data['rel_id']);
            }
        }

        $items = [];
        if (isset($data['newitems'])) {
            $items = $data['newitems'];
            unset($data['newitems']);
        }

        if ($this->copy == false) {
            $data['content'] = '{boq_items}';
        }

        $hook = hooks()->apply_filters('before_create_boq', [
            'data'  => $data,
            'items' => $items,
        ]);

        $data  = $hook['data'];
        $items = $hook['items'];

        unset($data['item_select']);
        unset($data['show_quantity_as']);
        unset($data['description']);
        unset($data['long_description']);
        unset($data['quantity']);
        unset($data['unit']);
        unset($data['rate']);
        $this->db->insert(db_prefix() . 'boqs', $data);
        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }

            handle_tags_save($tags, $insert_id, 'boq');

            foreach ($items as $key => $item) {
                if ($itemid = add_new_sales_item_post($item, $insert_id, 'boq')) {
                    _maybe_insert_post_item_tax($itemid, $item, $insert_id, 'boq');
                }
            }

            $boq = $this->get($insert_id);
            if ($boq->assigned != 0) {
                if ($boq->assigned != get_staff_user_id()) {
                    $notified = add_notification([
                        'description'     => 'not_boq_assigned_to_you',
                        'touserid'        => $boq->assigned,
                        'fromuserid'      => get_staff_user_id(),
                        'link'            => 'boqs/list_boqs/' . $insert_id,
                        'additional_data' => serialize([
                            $boq->subject,
                        ]),
                    ]);
                    if ($notified) {
                        pusher_trigger_notification([$boq->assigned]);
                    }
                }
            }

            if ($data['rel_type'] == 'lead') {
                $this->load->model('leads_model');
                $this->leads_model->log_lead_activity($data['rel_id'], 'not_lead_activity_created_boq', false, serialize([
                    '<a href="' . admin_url('boqs/list_boqs/' . $insert_id) . '" target="_blank">' . $data['subject'] . '</a>',
                ]));
            }

            update_sales_total_tax_column($insert_id, 'boq', db_prefix() . 'boqs');

            log_activity('New boq Created [ID: ' . $insert_id . ']');

            if ($save_and_send === true) {
                $this->send_boq_to_email($insert_id);
            }

            hooks()->do_action('boq_created', $insert_id);

            return $insert_id;
        }

        return false;
    }

    /**
     * Update boq
     * @param  mixed $data $_POST data
     * @param  mixed $id   boq id
     * @return boolean
     */
    public function update($data, $id)
    {
        $affectedRows = 0;

        $data['allow_comments'] = isset($data['allow_comments']) ? 1 : 0;

        $current_boq = $this->get($id);

        $save_and_send = isset($data['save_and_send']);

        if (empty($data['rel_type'])) {
            $data['rel_id']   = null;
            $data['rel_type'] = '';
        } else {
            if (empty($data['rel_id'])) {
                $data['rel_id']   = null;
                $data['rel_type'] = '';
            }
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            if (handle_custom_fields_post($id, $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }

        $items = [];
        if (isset($data['items'])) {
            $items = $data['items'];
            unset($data['items']);
        }

        $newitems = [];
        if (isset($data['newitems'])) {
            $newitems = $data['newitems'];
            unset($data['newitems']);
        }

        if (isset($data['tags'])) {
            if (handle_tags_save($data['tags'], $id, 'boq')) {
                $affectedRows++;
            }
        }

        $data['address'] = trim($data['address']);
        $data['address'] = nl2br($data['address']);

        $hook = hooks()->apply_filters('before_boq_updated', [
            'data'          => $data,
            'items'         => $items,
            'newitems'      => $newitems,
            'removed_items' => isset($data['removed_items']) ? $data['removed_items'] : [],
        ], $id);

        $data                  = $hook['data'];
        $data['removed_items'] = $hook['removed_items'];
        $newitems              = $hook['newitems'];
        $items                 = $hook['items'];

        // Delete items checked to be removed from database
        foreach ($data['removed_items'] as $remove_item_id) {
            if (handle_removed_sales_item_post($remove_item_id, 'boq')) {
                $affectedRows++;
            }
        }

        unset($data['removed_items']);
        unset($data['item_select']);
        unset($data['show_quantity_as']);
        unset($data['description']);
        unset($data['long_description']);
        unset($data['quantity']);
        unset($data['unit']);

        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'boqs', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            $boq_now = $this->get($id);
            if ($current_boq->assigned != $boq_now->assigned) {
                if ($boq_now->assigned != get_staff_user_id()) {
                    $notified = add_notification([
                        'description'     => 'not_boq_assigned_to_you',
                        'touserid'        => $boq_now->assigned,
                        'fromuserid'      => get_staff_user_id(),
                        'link'            => 'boqs/list_boqs/' . $id,
                        'additional_data' => serialize([
                            $boq_now->subject,
                        ]),
                    ]);
                    if ($notified) {
                        pusher_trigger_notification([$boq_now->assigned]);
                    }
                }
            }
        }

        foreach ($items as $key => $item) {
            if (update_sales_item_post($item['itemid'], $item)) {
                $affectedRows++;
            }

            if (isset($item['custom_fields'])) {
                if (handle_custom_fields_post($item['itemid'], $item['custom_fields'])) {
                    $affectedRows++;
                }
            }

            if (!isset($item['taxname']) || (isset($item['taxname']) && count($item['taxname']) == 0)) {
                if (delete_taxes_from_item($item['itemid'], 'boq')) {
                    $affectedRows++;
                }
            } else {
                $item_taxes        = get_boq_item_taxes($item['itemid']);
                $_item_taxes_names = [];
                foreach ($item_taxes as $_item_tax) {
                    array_push($_item_taxes_names, $_item_tax['taxname']);
                }
                $i = 0;
                foreach ($_item_taxes_names as $_item_tax) {
                    if (!in_array($_item_tax, $item['taxname'])) {
                        $this->db->where('id', $item_taxes[$i]['id'])
                        ->delete(db_prefix() . 'item_tax');
                        if ($this->db->affected_rows() > 0) {
                            $affectedRows++;
                        }
                    }
                    $i++;
                }
                if (_maybe_insert_post_item_tax($item['itemid'], $item, $id, 'boq')) {
                    $affectedRows++;
                }
            }
        }

        foreach ($newitems as $key => $item) {
            if ($new_item_added = add_new_sales_item_post($item, $id, 'boq')) {
                _maybe_insert_post_item_tax($new_item_added, $item, $id, 'boq');
                $affectedRows++;
            }
        }

        if ($affectedRows > 0) {
            update_sales_total_tax_column($id, 'boq', db_prefix() . 'boqs');
            log_activity('boq Updated [ID:' . $id . ']');
        }

        if ($save_and_send === true) {
            $this->send_boq_to_email($id);
        }

        if ($affectedRows > 0) {
            hooks()->do_action('after_boq_updated', $id);

            return true;
        }

        return false;
    }

    /**
     * Get boqs
     * @param  mixed $id boq id OPTIONAL
     * @return mixed
     */
    public function get($id = '', $where = [], $for_editor = false)
    {
        $this->db->where($where);

        if (is_client_logged_in()) {
            $this->db->where('status !=', 0);
        }

        $this->db->select('*,' . db_prefix() . 'currencies.id as currencyid, ' . db_prefix() . 'boqs.id as id, ' . db_prefix() . 'currencies.name as currency_name');
        $this->db->from(db_prefix() . 'boqs');
        $this->db->join(db_prefix() . 'currencies', db_prefix() . 'currencies.id = ' . db_prefix() . 'boqs.currency', 'left');

        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'boqs.id', $id);
            $boq = $this->db->get()->row();
            if ($boq) {
                $boq->attachments                           = $this->get_attachments($id);
                $boq->items                                 = get_items_by_type('boq', $id);
                $boq->visible_attachments_to_customer_found = false;
                foreach ($boq->attachments as $attachment) {
                    if ($attachment['visible_to_customer'] == 1) {
                        $boq->visible_attachments_to_customer_found = true;

                        break;
                    }
                }
                if ($for_editor == false) {
                    $boq = parse_boq_content_merge_fields($boq);
                }
            }

            return $boq;
        }

        return $this->db->get()->result_array();
    }

    public function clear_signature($id)
    {
        $this->db->select('signature');
        $this->db->where('id', $id);
        $boq = $this->db->get(db_prefix() . 'boqs')->row();

        if ($boq) {
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'boqs', ['signature' => null]);

            if (!empty($boq->signature)) {
                unlink(get_upload_path_by_type('boq') . $id . '/' . $boq->signature);
            }

            return true;
        }

        return false;
    }

    public function update_pipeline($data)
    {
        $this->mark_action_status($data['status'], $data['boqid']);
        foreach ($data['order'] as $order_data) {
            $this->db->where('id', $order_data[0]);
            $this->db->update(db_prefix() . 'boqs', [
                'pipeline_order' => $order_data[1],
            ]);
        }
    }

    public function get_attachments($boq_id, $id = '')
    {
        // If is passed id get return only 1 attachment
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        } else {
            $this->db->where('rel_id', $boq_id);
        }
        $this->db->where('rel_type', 'boq');
        $result = $this->db->get(db_prefix() . 'files');
        if (is_numeric($id)) {
            return $result->row();
        }

        return $result->result_array();
    }

    /**
     *  Delete boq attachment
     * @param   mixed $id  attachmentid
     * @return  boolean
     */
    public function delete_attachment($id)
    {
        $attachment = $this->get_attachments('', $id);
        $deleted    = false;
        if ($attachment) {
            if (empty($attachment->external)) {
                unlink(get_upload_path_by_type('boq') . $attachment->rel_id . '/' . $attachment->file_name);
            }
            $this->db->where('id', $attachment->id);
            $this->db->delete(db_prefix() . 'files');
            if ($this->db->affected_rows() > 0) {
                $deleted = true;
                log_activity('boq Attachment Deleted [ID: ' . $attachment->rel_id . ']');
            }
            if (is_dir(get_upload_path_by_type('boq') . $attachment->rel_id)) {
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('boq') . $attachment->rel_id);
                if (count($other_attachments) == 0) {
                    // okey only index.html so we can delete the folder also
                    delete_dir(get_upload_path_by_type('boq') . $attachment->rel_id);
                }
            }
        }

        return $deleted;
    }

    /**
     * Add boq comment
     * @param mixed  $data   $_POST comment data
     * @param boolean $client is request coming from the client side
     */
    public function add_comment($data, $client = false)
    {
        if (is_staff_logged_in()) {
            $client = false;
        }

        if (isset($data['action'])) {
            unset($data['action']);
        }
        $data['dateadded'] = date('Y-m-d H:i:s');
        if ($client == false) {
            $data['staffid'] = get_staff_user_id();
        }
        $data['content'] = nl2br($data['content']);
        $this->db->insert(db_prefix() . 'boq_comments', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $boq = $this->get($data['boqid']);

            // No notifications client when boq is with draft status
            if ($boq->status == '6' && $client == false) {
                return true;
            }

            if ($client == true) {
                // Get creator and assigned
                $this->db->select('staffid,email,phonenumber');
                $this->db->where('staffid', $boq->addedfrom);
                $this->db->or_where('staffid', $boq->assigned);
                $staff_boq = $this->db->get(db_prefix() . 'staff')->result_array();
                $notifiedUsers  = [];
                foreach ($staff_boq as $member) {
                    $notified = add_notification([
                        'description'     => 'not_boq_comment_from_client',
                        'touserid'        => $member['staffid'],
                        'fromcompany'     => 1,
                        'fromuserid'      => 0,
                        'link'            => 'boqs/list_boqs/' . $data['boqid'],
                        'additional_data' => serialize([
                            $boq->subject,
                        ]),
                    ]);

                    if ($notified) {
                        array_push($notifiedUsers, $member['staffid']);
                    }

                    $template     = mail_template('boq_comment_to_staff', $boq->id, $member['email']);
                    $merge_fields = $template->get_merge_fields();
                    $template->send();
                    // Send email/sms to admin that client commented
                    $this->app_sms->trigger(SMS_TRIGGER_boq_NEW_COMMENT_TO_STAFF, $member['phonenumber'], $merge_fields);
                }
                pusher_trigger_notification($notifiedUsers);
            } else {
                // Send email/sms to client that admin commented
                $template     = mail_template('boq_comment_to_customer', $boq);
                $merge_fields = $template->get_merge_fields();
                $template->send();
                $this->app_sms->trigger(SMS_TRIGGER_boq_NEW_COMMENT_TO_CUSTOMER, $boq->phone, $merge_fields);
            }

            return true;
        }

        return false;
    }

    public function edit_comment($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'boq_comments', [
            'content' => nl2br($data['content']),
        ]);
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get boq comments
     * @param  mixed $id boq id
     * @return array
     */
    public function get_comments($id)
    {
        $this->db->where('boqid', $id);
        $this->db->order_by('dateadded', 'ASC');

        return $this->db->get(db_prefix() . 'boq_comments')->result_array();
    }

    /**
     * Get boq single comment
     * @param  mixed $id  comment id
     * @return object
     */
    public function get_comment($id)
    {
        $this->db->where('id', $id);

        return $this->db->get(db_prefix() . 'boq_comments')->row();
    }

    /**
     * Remove boq comment
     * @param  mixed $id comment id
     * @return boolean
     */
    public function remove_comment($id)
    {
        $comment = $this->get_comment($id);
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'boq_comments');
        if ($this->db->affected_rows() > 0) {
            log_activity('boq Comment Removed [boqID:' . $comment->boqid . ', Comment Content: ' . $comment->content . ']');

            return true;
        }

        return false;
    }

    /**
     * Copy boq
     * @param  mixed $id boq id
     * @return mixed
     */
    public function copy($id)
    {
        $this->copy      = true;
        $boq        = $this->get($id, [], true);
        $not_copy_fields = [
            'addedfrom',
            'id',
            'datecreated',
            'hash',
            'status',
            'invoice_id',
            'estimate_id',
            'is_expiry_notified',
            'date_converted',
            'signature',
            'acceptance_firstname',
            'acceptance_lastname',
            'acceptance_email',
            'acceptance_date',
            'acceptance_ip',
        ];
        $fields      = $this->db->list_fields(db_prefix() . 'boqs');
        $insert_data = [];
        foreach ($fields as $field) {
            if (!in_array($field, $not_copy_fields)) {
                $insert_data[$field] = $boq->$field;
            }
        }

        $insert_data['addedfrom']   = get_staff_user_id();
        $insert_data['datecreated'] = date('Y-m-d H:i:s');
        $insert_data['date']        = _d(date('Y-m-d'));
        $insert_data['status']      = 6;
        $insert_data['hash']        = app_generate_hash();

        // in case open till is expired set new 7 days starting from current date
        if ($insert_data['open_till'] && get_option('boq_due_after') != 0) {
            $insert_data['open_till'] = _d(date('Y-m-d', strtotime('+' . get_option('boq_due_after') . ' DAY', strtotime(date('Y-m-d')))));
        }

        $insert_data['newitems'] = [];
        $custom_fields_items     = get_custom_fields('items');
        $key                     = 1;
        foreach ($boq->items as $item) {
            $insert_data['newitems'][$key]['description']      = $item['description'];
            $insert_data['newitems'][$key]['long_description'] = clear_textarea_breaks($item['long_description']);
            $insert_data['newitems'][$key]['qty']              = $item['qty'];
            $insert_data['newitems'][$key]['unit']             = $item['unit'];
            $insert_data['newitems'][$key]['taxname']          = [];
            $taxes                                             = get_boq_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                // tax name is in format TAX1|10.00
                array_push($insert_data['newitems'][$key]['taxname'], $tax['taxname']);
            }
            $insert_data['newitems'][$key]['rate']  = $item['rate'];
            $insert_data['newitems'][$key]['order'] = $item['item_order'];
            foreach ($custom_fields_items as $cf) {
                $insert_data['newitems'][$key]['custom_fields']['items'][$cf['id']] = get_custom_field_value($item['id'], $cf['id'], 'items', false);

                if (!defined('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST')) {
                    define('COPY_CUSTOM_FIELDS_LIKE_HANDLE_POST', true);
                }
            }
            $key++;
        }

        $id = $this->add($insert_data);

        if ($id) {
            $custom_fields = get_custom_fields('boq');
            foreach ($custom_fields as $field) {
                $value = get_custom_field_value($boq->id, $field['id'], 'boq', false);
                if ($value == '') {
                    continue;
                }
                $this->db->insert(db_prefix() . 'customfieldsvalues', [
                    'relid'   => $id,
                    'fieldid' => $field['id'],
                    'fieldto' => 'boq',
                    'value'   => $value,
                ]);
            }

            $tags = get_tags_in($boq->id, 'boq');
            handle_tags_save($tags, $id, 'boq');

            log_activity('Copied boq ' . format_boq_number($boq->id));

            return $id;
        }

        return false;
    }

    /**
     * Take boq action (change status) manually
     * @param  mixed $status status id
     * @param  mixed  $id     boq id
     * @param  boolean $client is request coming from client side or not
     * @return boolean
     */
    public function mark_action_status($status, $id, $client = false)
    {
        $original_boq = $this->get($id);
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'boqs', [
            'status' => $status,
        ]);

        if ($this->db->affected_rows() > 0) {
            // Client take action
            if ($client == true) {
                $revert = false;
                // Declined
                if ($status == 2) {
                    $message = 'not_boq_boq_declined';
                } elseif ($status == 3) {
                    $message = 'not_boq_boq_accepted';
                // Accepted
                } else {
                    $revert = true;
                }
                // This is protection that only 3 and 4 statuses can be taken as action from the client side
                if ($revert == true) {
                    $this->db->where('id', $id);
                    $this->db->update(db_prefix() . 'boqs', [
                        'status' => $original_boq->status,
                    ]);

                    return false;
                }

                // Get creator and assigned;
                $this->db->where('staffid', $original_boq->addedfrom);
                $this->db->or_where('staffid', $original_boq->assigned);
                $staff_boq = $this->db->get(db_prefix() . 'staff')->result_array();
                $notifiedUsers  = [];
                foreach ($staff_boq as $member) {
                    $notified = add_notification([
                            'fromcompany'     => true,
                            'touserid'        => $member['staffid'],
                            'description'     => $message,
                            'link'            => 'boqs/list_boqs/' . $id,
                            'additional_data' => serialize([
                                format_boq_number($id),
                            ]),
                        ]);
                    if ($notified) {
                        array_push($notifiedUsers, $member['staffid']);
                    }
                }

                pusher_trigger_notification($notifiedUsers);

                // Send thank you to the customer email template
                if ($status == 3) {
                    foreach ($staff_boq as $member) {
                        send_mail_template('boq_accepted_to_staff', $original_boq, $member['email']);
                    }

                    send_mail_template('boq_accepted_to_customer', $original_boq);

                    hooks()->do_action('boq_accepted', $id);
                } else {

                    // Client declined send template to admin
                    foreach ($staff_boq as $member) {
                        send_mail_template('boq_declined_to_staff', $original_boq, $member['email']);
                    }

                    hooks()->do_action('boq_declined', $id);
                }
            } else {
                // in case admin mark as open the the open till date is smaller then current date set open till date 7 days more
                if ((date('Y-m-d', strtotime($original_boq->open_till)) < date('Y-m-d')) && $status == 1) {
                    $open_till = date('Y-m-d', strtotime('+7 DAY', strtotime(date('Y-m-d'))));
                    $this->db->where('id', $id);
                    $this->db->update(db_prefix() . 'boqs', [
                        'open_till' => $open_till,
                    ]);
                }
            }

            log_activity('boq Status Changes [boqID:' . $id . ', Status:' . format_boq_status($status, '', false) . ',Client Action: ' . (int) $client . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete boq
     * @param  mixed $id boq id
     * @return boolean
     */
    public function delete($id)
    {
        $this->clear_signature($id);
        $boq = $this->get($id);

        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'boqs');
        if ($this->db->affected_rows() > 0) {
            if (!is_null($boq->short_link)) {
                app_archive_short_link($boq->short_link);
            }

            delete_tracked_emails($id, 'boq');

            $this->db->where('boqid', $id);
            $this->db->delete(db_prefix() . 'boq_comments');
            // Get related tasks
            $this->db->where('rel_type', 'boq');
            $this->db->where('rel_id', $id);

            $tasks = $this->db->get(db_prefix() . 'tasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }

            $attachments = $this->get_attachments($id);
            foreach ($attachments as $attachment) {
                $this->delete_attachment($attachment['id']);
            }

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'boq');
            $this->db->delete(db_prefix() . 'notes');

            $this->db->where('relid IN (SELECT id from ' . db_prefix() . 'itemable WHERE rel_type="boq" AND rel_id="' . $this->db->escape_str($id) . '")');
            $this->db->where('fieldto', 'items');
            $this->db->delete(db_prefix() . 'customfieldsvalues');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'boq');
            $this->db->delete(db_prefix() . 'itemable');


            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'boq');
            $this->db->delete(db_prefix() . 'item_tax');

            $this->db->where('rel_id', $id);
            $this->db->where('rel_type', 'boq');
            $this->db->delete(db_prefix() . 'taggables');

            // Delete the custom field values
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'boq');
            $this->db->delete(db_prefix() . 'customfieldsvalues');

            $this->db->where('rel_type', 'boq');
            $this->db->where('rel_id', $id);
            $this->db->delete(db_prefix() . 'reminders');

            $this->db->where('rel_type', 'boq');
            $this->db->where('rel_id', $id);
            $this->db->delete(db_prefix() . 'views_tracking');

            log_activity('boq Deleted [boqID:' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Get relation boq data. Ex lead or customer will return the necesary db fields
     * @param  mixed $rel_id
     * @param  string $rel_type customer/lead
     * @return object
     */
    public function get_relation_data_values($rel_id, $rel_type)
    {
        $data = new StdClass();
        if ($rel_type == 'customer') {
            $this->db->where('userid', $rel_id);
            $_data = $this->db->get(db_prefix() . 'clients')->row();

            $primary_contact_id = get_primary_contact_user_id($rel_id);

            if ($primary_contact_id) {
                $contact     = $this->clients_model->get_contact($primary_contact_id);
                $data->email = $contact->email;
            }

            $data->phone            = $_data->phonenumber;
            $data->is_using_company = false;
            if (isset($contact)) {
                $data->to = $contact->firstname . ' ' . $contact->lastname;
            } else {
                if (!empty($_data->company)) {
                    $data->to               = $_data->company;
                    $data->is_using_company = true;
                }
            }
            $data->company = $_data->company;
            $data->address = clear_textarea_breaks($_data->address);
            $data->zip     = $_data->zip;
            $data->country = $_data->country;
            $data->state   = $_data->state;
            $data->city    = $_data->city;

            $default_currency = $this->clients_model->get_customer_default_currency($rel_id);
            if ($default_currency != 0) {
                $data->currency = $default_currency;
            }
        } elseif ($rel_type = 'lead') {
            $this->db->where('id', $rel_id);
            $_data       = $this->db->get(db_prefix() . 'leads')->row();
            $data->phone = $_data->phonenumber;

            $data->is_using_company = false;

            if (empty($_data->company)) {
                $data->to = $_data->name;
            } else {
                $data->to               = $_data->company;
                $data->is_using_company = true;
            }

            $data->company = $_data->company;
            $data->address = $_data->address;
            $data->email   = $_data->email;
            $data->zip     = $_data->zip;
            $data->country = $_data->country;
            $data->state   = $_data->state;
            $data->city    = $_data->city;
        }

        return $data;
    }

    /**
     * Sent boq to email
     * @param  mixed  $id        boqid
     * @param  string  $template  email template to sent
     * @param  boolean $attachpdf attach boq pdf or not
     * @return boolean
     */
    public function send_expiry_reminder($id)
    {
        $boq = $this->get($id);

        // For all cases update this to prevent sending multiple reminders eq on fail
        $this->db->where('id', $boq->id);
        $this->db->update(db_prefix() . 'boqs', [
            'is_expiry_notified' => 1,
        ]);

        $template     = mail_template('boq_expiration_reminder', $boq);
        $merge_fields = $template->get_merge_fields();

        $template->send();

        if (can_send_sms_based_on_creation_date($boq->datecreated)) {
            $sms_sent = $this->app_sms->trigger(SMS_TRIGGER_boq_EXP_REMINDER, $boq->phone, $merge_fields);
        }

        return true;
    }

    public function send_boq_to_email($id, $attachpdf = true, $cc = '')
    {
        // boq status is draft update to sent
        if (total_rows(db_prefix() . 'boqs', ['id' => $id, 'status' => 6]) > 0) {
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'boqs', ['status' => 4]);
        }

        $boq = $this->get($id);

        $sent = send_mail_template('boq_send_to_customer', $boq, $attachpdf, $cc);

        if ($sent) {

            // Set to status sent
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'boqs', [
                'status' => 4,
            ]);

            hooks()->do_action('boq_sent', $id);

            return true;
        }

        return false;
    }
}
