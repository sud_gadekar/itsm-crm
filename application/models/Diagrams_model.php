<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Diagrams_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get diagram by id
     * @param  string $id   diagram ID
     * @param  string $slug if search by slug
     * @return mixed       if ID or slug passed return object else array
     */
    public function get($id = '', $slug = '')
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'diagrams')
                ->join(db_prefix().'clients','tbldiagrams.clientid = tblclients.userid');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        }
        if ($slug != '') {
            $this->db->where('slug', $slug);
        }
        if (is_numeric($id) || $slug != '') {
            return $this->db->get()->row();
        }

        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'diagrams')
                ->join(db_prefix().'clients','tbldiagrams.clientid = tblclients.userid');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('tbldiagrams.clientid', $id);
        }
        
        return $this->db->get()->result_array();
    }

    /**
     * Get related artices based on diagram id
     * @param  mixed $current_id current diagram id
     * @return array
     */
    public function get_related_diagrams($current_id, $customers = true)
    {
        $total_related_diagrams = hooks()->apply_filters('total_related_diagrams', 5);

        $this->db->select('diagramgroup');
        $this->db->where('id', $current_id);
        $diagram = $this->db->get(db_prefix() . 'diagrams')->row();

        $this->db->where('diagramgroup', $diagram->diagramgroup);
        $this->db->where('id !=', $current_id);
        $this->db->where('active', 1);
        if ($customers == true) {
            $this->db->where('staff_diagram', 0);
        } else {
            $this->db->where('staff_diagram', 1);
        }
        $this->db->limit($total_related_diagrams);

        return $this->db->get(db_prefix() . 'diagrams')->result_array();
    }

    /**
     * Add new diagram
     * @param array $data diagram data
     */
    public function add_diagram($data)
    {
        $data['created_by'] = date('Y-m-d H:i:s');
        //$data = hooks()->apply_filters('before_add_kb_diagram', $data);

        $this->db->insert(db_prefix() . 'diagrams', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Diagram Added [id: ' . $insert_id . ']');
        }

        return $insert_id;
    }

    /**
     * Update diagram
     * @param  array $data diagram data
     * @param  mixed $id   id
     * @return boolean
     */
    public function update_diagram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'diagrams', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Diagram Updated [id: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Change diagram status
     * @param  mixed $id     diagram id
     * @param  boolean $status is active or not
     */
    public function change_diagram_status($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'diagrams', [
            'active' => $status,
        ]);
        log_activity('diagram Status Changed [id: ' . $id . ' Status: ' . $status . ']');
    }

    public function update_groups_order()
    {
        $data = $this->input->post();
        foreach ($data['order'] as $group) {
            $this->db->where('groupid', $group[0]);
            $this->db->update(db_prefix() . 'diagrams_groups', [
                'group_order' => $group[1],
            ]);
        }
    }

    /**
     * Delete diagram from database and all diagram connections
     * @param  mixed $id diagram ID
     * @return boolean
     */
    public function delete_diagram($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'diagrams');
        if ($this->db->affected_rows() > 0) {
            log_activity('Diagram Deleted [id: ' . $id . ']');
            return true;
        }
        return false;
    }
}
