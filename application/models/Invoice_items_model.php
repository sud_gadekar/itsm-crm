<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Invoice_items_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get invoice item by ID
     * @param  mixed $id
     * @return mixed - array if not passed id, object if id passed
     */
    public function get($id = '')
    {
        $columns             = $this->db->list_fields(db_prefix() . 'items');
        $rateCurrencyColumns = '';
        foreach ($columns as $column) {
            if (strpos($column, 'rate_currency_') !== false) {
                $rateCurrencyColumns .= $column . ',';
            }
        }
        $this->db->select($rateCurrencyColumns . '' . db_prefix() . 'items.id as itemid,items.sku,items.default_currency,rate,converted_rate,region,manufacturer,
            t1.taxrate as taxrate,t1.id as taxid,t1.name as taxname,
            t2.taxrate as taxrate_2,t2.id as taxid_2,t2.name as taxname_2,
            description,long_description,group_id,' . db_prefix() . 'items_groups.name as group_name,unit');
        $this->db->from(db_prefix() . 'items');
        $this->db->join('' . db_prefix() . 'taxes t1', 't1.id = ' . db_prefix() . 'items.tax', 'left');
        $this->db->join('' . db_prefix() . 'taxes t2', 't2.id = ' . db_prefix() . 'items.tax2', 'left');
        $this->db->join(db_prefix() . 'items_groups', '' . db_prefix() . 'items_groups.id = ' . db_prefix() . 'items.group_id', 'left');
        $this->db->order_by('description', 'asc');
        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'items.id', $id);

            return $this->db->get()->row();
        }

        return $this->db->get()->result_array();
    }

    public function get_grouped()
    {
        $items = [];
        $this->db->order_by('name', 'asc');
        $groups = $this->db->get(db_prefix() . 'items_groups')->result_array();

        array_unshift($groups, [
            'id'   => 0,
            'name' => '',
        ]);

        foreach ($groups as $group) {
            $this->db->select('*,' . db_prefix() . 'items_groups.name as group_name,' . db_prefix() . 'items.id as id');
            $this->db->where('group_id', $group['id']);
            $this->db->join(db_prefix() . 'items_groups', '' . db_prefix() . 'items_groups.id = ' . db_prefix() . 'items.group_id', 'left');
            $this->db->join(db_prefix() . 'items_regions', '' . db_prefix() . 'items_regions.id = ' . db_prefix() . 'items.region', 'left');
            $this->db->join(db_prefix() . 'items_manufacturers', '' . db_prefix() . 'items_manufacturers.id = ' . db_prefix() . 'items.manufacturer', 'left');

            $this->db->order_by('description', 'asc');
            $_items = $this->db->get(db_prefix() . 'items')->result_array();
            if (count($_items) > 0) {
                $items[$group['id']] = [];
                foreach ($_items as $i) {
                    array_push($items[$group['id']], $i);
                }
            }
        }

        return $items;
    }

    /**
     * Add new invoice item
     * @param array $data Invoice item data
     * @return boolean
     */
    public function add($data)
    {
        unset($data['itemid']);
        if ($data['tax'] == '') {
            unset($data['tax']);
        }

        if (isset($data['tax2']) && $data['tax2'] == '') {
            unset($data['tax2']);
        }

        if (isset($data['group_id']) && $data['group_id'] == '') {
            $data['group_id'] = 0;
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        $columns = $this->db->list_fields(db_prefix() . 'items');
        $this->load->dbforge();
        foreach ($data as $column => $itemData) {
            if (!in_array($column, $columns) && strpos($column, 'rate_currency_') !== false) {
                $field = [
                    $column => [
                        'type' => 'decimal(15,' . get_decimal_places() . ')',
                        'null' => true,
                    ],
                ];
                $this->dbforge->add_column('items', $field);
            }
        }

        //$currencyData = $this->currencies_model->get($data['default_currency']);
        //$data['converted_rate'] = $data['rate'] * $currencyData->conversion_rate;

        $this->db->insert(db_prefix() . 'items', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields, true);
            }

            hooks()->do_action('item_created', $insert_id);

            log_activity('New Invoice Item Added [ID:' . $insert_id . ', ' . $data['description'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Update invoiec item
     * @param  array $data Invoice data to update
     * @return boolean
     */
    public function edit($data)
    {
        $itemid = $data['itemid'];
        unset($data['itemid']);

        if (isset($data['group_id']) && $data['group_id'] == '') {
            $data['group_id'] = 0;
        }

        if (isset($data['tax']) && $data['tax'] == '') {
            $data['tax'] = null;
        }

        if (isset($data['tax2']) && $data['tax2'] == '') {
            $data['tax2'] = null;
        }

        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        $columns = $this->db->list_fields(db_prefix() . 'items');
        $this->load->dbforge();

        foreach ($data as $column => $itemData) {
            if (!in_array($column, $columns) && strpos($column, 'rate_currency_') !== false) {
                $field = [
                        $column => [
                            'type' => 'decimal(15,' . get_decimal_places() . ')',
                            'null' => true,
                        ],
                ];
                $this->dbforge->add_column('items', $field);
            }
        }

        $affectedRows = 0;

        $data = hooks()->apply_filters('before_update_item', $data, $itemid);

        $this->db->where('id', $itemid);
        $this->db->update(db_prefix() . 'items', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Invoice Item Updated [ID: ' . $itemid . ', ' . $data['description'] . ']');
            $affectedRows++;
        }

        if (isset($custom_fields)) {
            if (handle_custom_fields_post($itemid, $custom_fields, true)) {
                $affectedRows++;
            }
        }

        if ($affectedRows > 0) {
            hooks()->do_action('item_updated', $itemid);
        }

        return $affectedRows > 0 ? true : false;
    }

    public function search($q)
    {
        $this->db->select('rate, id, description as name, long_description as subtext');
        $this->db->like('description', $q);
        $this->db->or_like('long_description', $q);

        $items = $this->db->get(db_prefix() . 'items')->result_array();

        foreach ($items as $key => $item) {
            $items[$key]['subtext'] = strip_tags(mb_substr($item['subtext'], 0, 200)) . '...';
            $items[$key]['name']    = '(' . app_format_number($item['rate']) . ') ' . $item['name'];
        }

        return $items;
    }

    /**
     * Delete invoice item
     * @param  mixed $id
     * @return boolean
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'items');
        if ($this->db->affected_rows() > 0) {
            $this->db->where('relid', $id);
            $this->db->where('fieldto', 'items_pr');
            $this->db->delete(db_prefix() . 'customfieldsvalues');

            log_activity('Invoice Item Deleted [ID: ' . $id . ']');

            hooks()->do_action('item_deleted', $id);

            return true;
        }

        return false;
    }

    public function get_groups()
    {
        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix() . 'items_groups')->result_array();
    }

    public function add_group($data)
    {
        $this->db->insert(db_prefix() . 'items_groups', $data);
        log_activity('Items Group Created [Name: ' . $data['name'] . ']');

        return $this->db->insert_id();
    }

    public function edit_group($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'items_groups', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Items Group Updated [Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    public function delete_group($id)
    {
        $this->db->where('id', $id);
        $group = $this->db->get(db_prefix() . 'items_groups')->row();

        if ($group) {
            $this->db->where('group_id', $id);
            $this->db->update(db_prefix() . 'items', [
                'group_id' => 0,
            ]);

            $this->db->where('id', $id);
            $this->db->delete(db_prefix() . 'items_groups');

            log_activity('Item Group Deleted [Name: ' . $group->name . ']');

            return true;
        }

        return false;
    }

    

    //regions
    /**
     * Get items region by id
     * @param  mixed $id region id
     * @return mixed     if id passed return object else array
     */
    public function get_region($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'items_regions')->row();
        }

        return $this->db->get(db_prefix() . 'items_regions')->result_array();
    }

    /**
     * Add new items region
     * @param array $data items region data
     */
    public function add_region($data)
    {
        $this->db->insert(db_prefix() . 'items_regions', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New items region added [ID: ' . $insert_id . ', Name: ' . $data['region_title'] . ']');
        }

        return $insert_id;
    }

    /**
     * Update items region
     * @param  array $data items region $_POST data
     * @param  mixed $id   items region id
     * @return boolean
     */
    public function update_region($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'items_regions', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('items region updated [ID: ' . $id . ' Name: ' . $data['region_title'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete items region
     * @param  mixed $id items region id
     * @return mixed
     */
    public function delete_region($id)
    {
        // $current = $this->get($id);
        //// Check if the region id is used in itemss table
        // if (is_reference_in_table('region', db_prefix() . 'itemss', $id)) {
        //     return [
        //         'referenced' => true,
        //     ];
        // }
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'items_regions');
        if ($this->db->affected_rows() > 0) {
            log_activity('items region deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    //manufacturers
    /**
     * Get item manufacturer by id
     * @param  mixed $id manufacturer id
     * @return mixed     if id passed return object else array
     */
    public function get_manufacturer($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'items_manufacturers')->row();
        }

        return $this->db->get(db_prefix() . 'items_manufacturers')->result_array();
    }

    /**
     * Add new item manufacturer
     * @param array $data item manufacturer data
     */
    public function add_manufacturer($data)
    {
        $this->db->insert(db_prefix() . 'items_manufacturers', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New item manufacturer added [ID: ' . $insert_id . ', Name: ' . $data['manufacturer_title'] . ']');
        }

        return $insert_id;
    }

    /**
     * Update item manufacturer
     * @param  array $data item manufacturer $_POST data
     * @param  mixed $id   item manufacturer id
     * @return boolean
     */
    public function update_manufacturer($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'items_manufacturers', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('item manufacturer updated [ID: ' . $id . ' Name: ' . $data['manufacturer_title'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete item manufacturer
     * @param  mixed $id item manufacturer id
     * @return mixed
     */
    public function delete_manufacturer($id)
    {
        // $current = $this->get($id);
        //// Check if the manufacturer id is used in items table
        // if (is_reference_in_table('manufacturer', db_prefix() . 'items', $id)) {
        //     return [
        //         'referenced' => true,
        //     ];
        // }
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'items_manufacturers');
        if ($this->db->affected_rows() > 0) {
            log_activity('item manufacturer deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }
}
