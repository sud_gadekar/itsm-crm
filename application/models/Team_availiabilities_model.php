<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Team_availiabilities_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get diagram by id
     * @param  string $id   diagram ID
     * @param  string $slug if search by slug
     * @return mixed       if ID or slug passed return object else array
     */
    public function get($staff_id = '', $id = '', $contact_id = '', $client_id = '')
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'team_availiable');

        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        }
        if ($staff_id != '' && $staff_id != null) {
            $this->db->where('staff_id', $staff_id);
        }
        if (is_numeric($id) || $staff_id != '') {
            return $this->db->get()->result_array();
        }
        // if($client_id){
        //     $this->db->where(db_prefix() . 'team_availiable.customer_id', $client_id);
        // }

        return $this->db->get()->result_array();
    }

    
    public function get_app($staff_id = '', $id = '', $contact_id = '', $client_id = '')
    {
        $this->db->select(db_prefix() . 'team_availiable.*, tblstaff.email, tblstaff.firstname, tblstaff.lastname, roles.name as role');
        $this->db->from(db_prefix() . 'team_availiable');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'team_availiable.staff_id', 'left');
        $this->db->join(db_prefix() . 'roles', db_prefix() . 'roles.roleid = ' . db_prefix() . 'staff.role', 'left');

        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('id', $id);
        }
        if ($staff_id != '' && $staff_id != null) {
            $this->db->where('staff_id', $staff_id);
        }
        if (is_numeric($id) || $staff_id != '') {
            return $this->db->get()->result_array();
        }
        // if($client_id){
        //     $this->db->where(db_prefix() . 'team_availiable.customer_id', $client_id);
        // }

        $data = $this->db->get()->result_array();

        foreach($data as $key=>$row){
            if($row['status_id'] == 1){
                $type_name = 'On Leave';
            } else if($row['status_id'] == 2){
                $type_name = 'On Project';
            } else if($row['status_id'] == 3){
                $type_name = 'On Support Request';
            } else if($row['status_id'] == 4){
                $type_name = 'On Service Request';
            } else if($row['status_id'] == 5) {
                $type_name = 'Availiable';
            } else {
                $type_name = '';
            }
            $data[$key]['status_name'] = _l($type_name);
        }

        return $data;
    }

    public function get_by_customer($id)
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'diagrams')
                ->join(db_prefix().'clients','tbldiagrams.clientid = tblclients.userid');
        $this->db->order_by('created_at', 'desc');
        if (is_numeric($id)) {
            $this->db->where('tbldiagrams.clientid', $id);
        }
        
        return $this->db->get()->result_array();
    }

    /**
     * Add team availiability
     */
   public function add($data)
    {
        $this->db->insert(db_prefix() . 'team_availiable', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New Team availiability added [' . $insert_id . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Add new diagram
     * @param array $data diagram data
     */
    public function add_diagram($data)
    {
        $data['created_by'] = date('Y-m-d H:i:s');
        //$data = hooks()->apply_filters('before_add_kb_diagram', $data);

        $this->db->insert(db_prefix() . 'diagrams', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Diagram Added [id: ' . $insert_id . ']');
        }

        return $insert_id;
    }

    /**
     * Update diagram
     * @param  array $data diagram data
     * @param  mixed $id   id
     * @return boolean
     */
    public function update_diagram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'diagrams', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Diagram Updated [id: ' . $id . ']');

            return true;
        }

        return false;
    }

    /**
     * Change diagram status
     * @param  mixed $id     diagram id
     * @param  boolean $status is active or not
     */
    public function change_diagram_status($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'diagrams', [
            'active' => $status,
        ]);
        log_activity('diagram Status Changed [id: ' . $id . ' Status: ' . $status . ']');
    }

    public function update_groups_order()
    {
        $data = $this->input->post();
        foreach ($data['order'] as $group) {
            $this->db->where('groupid', $group[0]);
            $this->db->update(db_prefix() . 'diagrams_groups', [
                'group_order' => $group[1],
            ]);
        }
    }

    /**
     * Delete diagram from database and all diagram connections
     * @param  mixed $id diagram ID
     * @return boolean
     */
    public function delete_diagram($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'diagrams');
        if ($this->db->affected_rows() > 0) {
            log_activity('Diagram Deleted [id: ' . $id . ']');
            return true;
        }
        return false;
    }
}
