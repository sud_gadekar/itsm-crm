<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Network_server_model extends App_Model
{

	public function __construct()
    {
        parent::__construct();
    }

    public function get($where = '')
    {
        if($where)
        {
            $this->db->where('type',$where)->select('tblnetworks.id,tblclients.company, tblinventory_types.type_name, tblnetworks.title')
                    ->from(db_prefix() . 'networks')
                    ->join(db_prefix().'clients','tblnetworks.clientid = tblclients.userid')
                    ->join(db_prefix().'inventory_types','tblnetworks.type = tblinventory_types.id');

            return $this->db->get()->result_array();
        }
        else
        {
            $this->db->select('tblnetworks.id,tblclients.company, tblinventory_types.type_name, tblnetworks.title')
                     ->from(db_prefix() . 'networks')
                     ->join(db_prefix().'clients','tblnetworks.clientid = tblclients.userid')
                     ->join(db_prefix().'inventory_types','tblnetworks.type = tblinventory_types.id');

            return $this->db->get()->result_array();
        }  
        
    }

    public function get_by_customer($id)
    {
        
            $this->db->select('tblnetworks.id,tblclients.company, tblinventory_types.type_name, tblnetworks.title')
                     ->from(db_prefix() . 'networks')
                     ->join(db_prefix().'clients','tblnetworks.clientid = tblclients.userid')
                     ->join(db_prefix().'inventory_types','tblnetworks.type = tblinventory_types.id')
                     ->where('tblnetworks.clientid',$id);
                    
            return $this->db->get()->result_array();
    }

    public function add($data) 
    {
    	if($data)
    	{
    		$this->db->insert(db_prefix() . 'networks', $data);
       		$insert_id = $this->db->insert_id();
       		if($insert_id)
       		{
       			log_activity('New network / server added [' . $insert_id . ']');
            	return $insert_id;
       		}
    	}
    }

    public function get_data_by_id($id)
    {
    	if($id)
    	{
    		return $this->db->where('id',$id)->get(db_prefix().'networks')->row();
    	}
    }

    public function get_network_server_description($id)
    {
        return $this->db->where('service_overviewid',$id)->select('*')->get(db_prefix().'service_overview_attachments')
                        ->row();
    }

    public function get_it_overviews($id = '', $contact_id = '', $client_id = '')
    {
        $this->db->select('tblnetworks.id,tblclients.company, tblinventory_types.type_name, tblnetworks.title')
                     ->from(db_prefix() . 'networks')
                     ->join(db_prefix().'clients','tblnetworks.clientid = tblclients.userid')
                     ->join(db_prefix().'inventory_types','tblnetworks.type = tblinventory_types.id')
                     ->where('tblnetworks.clientid', $client_id);
        if ($id) {
            $data = array();
            $data['it_overview_detail'] = $this->db->where('tblnetworks.id', $id)->get()->result_array();
            $data['service_overviews'] = $this->get_network_server_service_overview($id);
            $data['dependencies'] = $this->get_network_server_asset($id);
            $data['digrams'] = $this->get_network_server_diagram($id);
            $data['network_monitoring'] = $this->get_network_server_url($id);
            $data['gaps'] = $this->get_network_server_gap($id);
            $data['licenses'] = $this->get_network_server_software_license($id);

            return $data;
        }
        return $this->db->get()->result_array();  
        
    }

    public function get_network_server_service_overview($id)
    {
        $base_url = BASE_URL();
        return $this->db->where('service_overviewid',$id)->select('*, concat("/uploads/network_servers/",service_overviewid,"/",file_name) as file_path')->get(db_prefix().'service_overview_attachments')->result_array();
    }

    public function get_network_server_asset($id){

        return $this->db->select('tblinventory_details.customer_prefix, tblnetwork_assets.id, tblinventory_products.product_name, tblinventory_products.manufacture_id,tblinventory_manufacture.manufacture_name,tblinventory_details.hostname,tblclient_locations.location,type.type_name as type, subtype.type_name as subtype,tblinventory_statuses.status_name, tblinventory_details.processor, tblinventory_details.ram, tblinventory_details.storage, tblinventory_details.ip_address, tblinventory_details.serial_no, tblinventory_details.operating_system, tblinventory_details.sw_valid_from, tblinventory_details.sw_valid_to, tblinventory_details.sw_amount, tblinventory_details.sw_renewal, tblinventory_details.backup_required,tblcontracts.subject, tblcontacts.firstname, tblcontacts.lastname,tblclients.company')
                    ->from('tblnetwork_assets')
                    ->join('tblinventory_details','tblnetwork_assets.inventory_id = tblinventory_details.id','left')
                    ->join('tblinventory_products','tblinventory_details.product_name = tblinventory_products.id','left')
                    ->join('tblinventory_manufacture','tblinventory_details.manufacture = tblinventory_manufacture.id','left')
                    ->join('tblclient_locations','tblinventory_details.location = tblclient_locations.id','left')
                    ->join('tblinventory_types as type','tblinventory_details.type = type.id','left')
                    ->join('tblinventory_types as subtype','tblinventory_details.sub_type = subtype.id','left')
                    ->join('tblinventory_statuses','tblinventory_details.status = tblinventory_statuses.id','left')
                    ->join('tblcontracts','tblinventory_details.support_contract = tblcontracts.id','left')
                    ->join('tblcontacts','tblinventory_details.contact = tblcontacts.id','left')
                    ->join('tblclients','tblinventory_details.clientid = tblclients.userid','left')
                    ->where('network_id',$id)->get()->result_array();
                //print_r($this->db->last_query());die();
    }

    public function get_network_server_diagram($id)
    {
        return $this->db->select('tblnetwork_diagrams.id, tbldiagrams.title, tbldiagrams.diagram_file, tbldiagrams.id as diagramsid, concat("/uploads/diagrams/",tblnetwork_diagrams.diagram_id,"/thumb_",diagram_file) as file_path')
                        ->from('tblnetwork_diagrams')
                        ->join('tbldiagrams','tblnetwork_diagrams.diagram_id = tbldiagrams.id')

                        ->where('network_id',$id)->get()->result_array();
    }

    public function get_network_server_url($id)
    {
        return $this->db->where('network_id',$id)->select('*')->get(db_prefix().'network_urls')->result_array();
    }

    public function get_network_server_gap($id)
    {
        return $this->db->select('tblnetwork_gaps.id,tblclients.company, tblgaps.name, tblgaps.description, tblgap_category.category_title,
        tblgap_impact.impact_title, tblgap_status.status_title,tblgaps.created_at')
                        ->from('tblnetwork_gaps')
                        ->join('tblgaps','tblnetwork_gaps.gap_id = tblgaps.id')
                        ->join('tblgap_category','tblgaps.category_id = tblgap_category.id')
                        ->join('tblgap_impact','tblgaps.impact_id = tblgap_impact.id')
                        ->join('tblgap_status','tblgaps.status_id = tblgap_status.id')
                        ->join('tblclients','tblgaps.clientid = tblclients.userid')
                        ->where('network_id',$id)->get()->result_array();
    }

    public function get_network_server_software_license($id)
    {
        
        return $this->db->select('tblnetwork_software_licenses.id, tblrenewals.name, tblclients.company, tblrenewals.vendor, tblrenewal_types.type_name, tblrenewals.start_date, tblrenewal_vendors.vendor_name, tblrenewals.start_date, tblrenewals.end_date')
                ->from(db_prefix() . 'network_software_licenses')
                ->join(db_prefix() . 'renewals','tblnetwork_software_licenses.renewal_id = tblrenewals.id','left')
                ->join('tblclients','tblrenewals.clientid = tblclients.userid','left')
                ->join('tblrenewal_types','tblrenewals.renewal_type = tblrenewal_types.id','left')
                ->join('tblrenewal_vendors','tblrenewals.vendor = tblrenewal_vendors.id','left')
                ->where('network_id',$id)->get()->result_array();

        // return $this->db->get()->result_array();
    }

    
    public function get_all_diagram($id)
    {
        if($id)
        {
            return $this->db->where('clientid',$id)->get(db_prefix().'diagrams')->result_array();
        }
        return $this->db->get(db_prefix().'diagrams')->result_array();
    }

    public function get_all_gap($id)
    {
        if($id)
        {
            return $this->db->where('clientid',$id)->get(db_prefix().'gaps')->result_array();
        }
        return $this->db->get(db_prefix().'gaps')->result_array();
    }

    public function get_all_software_license($id)
    {
        if($id)
        {
            return $this->db->where('clientid',$id)->get(db_prefix().'renewals')->result_array();
        }
        return $this->db->get(db_prefix().'renewals')->result_array();
    }
}