<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Checklists_model extends App_Model
{
    private $piping = false;

    public function __construct()
    {
        parent::__construct(); 
    }

    
    public function get($id = '', $where = [], $contact_id = '', $client_id = '')
    {
        $this->db->select('tblchecklist.*, tblclients.company')
                ->from(db_prefix() . 'checklist')
                ->join(db_prefix() . 'clients','tblchecklist.client_id = tblclients.userid', 'left')
                ->where($where);
        if ($id) {
            $this->db->where('tblchecklist.id', $id);
        }
        if($client_id){
            $this->db->where(db_prefix() . 'checklist.client_id', $client_id);
        }

        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('tblchecklist.*, tblclients.company')
                ->from(db_prefix() . 'checklist')
                ->join(db_prefix() . 'clients','tblchecklist.client_id = tblclients.userid')
                // ->join(db_prefix() . 'inventory_details','tblchecklist.inventory = tblinventory_details.id')
                // ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('tblchecklist.client_id',$id);

        return $this->db->get()->result_array();
    }

    /**
     * Add new gap
     * @param mixed $data All $_POST data
     * @return  mixed
     */
    public function add($data)
    {
        $this->db->insert(db_prefix() . 'checklist', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New Health Checklist Added [' . $insert_id . ']');
            return $insert_id;
        }

        return false;
    }

    //categories
    /**
     * Get gap category by id
     * @param  mixed $id category id
     * @return mixed     if id passed return object else array
     */
    public function get_log($id = '')
    {
        //echo $id; die;
        if (!empty($id)) {
            return $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblchecklist.checklist_title, tblinventory_products.product_name, tblchecklist_status.name as status')
                ->from(db_prefix() . 'checklist_logs')
                ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
                ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
                ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
            ->where('checklist_id', $id)->group_by('tblchecklist_logs.log_date')->get()->result_array();
        }

        return $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblchecklist.checklist_title, tblinventory_products.product_name, tblchecklist_status.name as status')
                ->from(db_prefix() . 'checklist_logs')
                ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
                ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
                ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->group_by('tblchecklist_logs.checklist_id')->group_by('tblchecklist_logs.log_date')->get()->result_array();
    }

    public function get_log_details($date = '', $id = '')
    {
        if (!empty($id) && !empty($date)) {
            return $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblchecklist.checklist_title, tblinventory_products.product_name, tblchecklist_status.name as status')
                ->from(db_prefix() . 'checklist_logs')
                ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
                ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
                ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('checklist_id', $id)->where('log_date', $date)->get()->result_array();
        }

        return $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblchecklist.checklist_title, tblinventory_products.product_name, tblchecklist_status.name as status')
                ->from(db_prefix() . 'checklist_logs')
                ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
                ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
                ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->get()->result_array();
    }
    
    public function checklist_logs($id = '')
    {
        //echo $id; die;
        if ($id) {
            return $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblchecklist.checklist_title, tblinventory_products.product_name, tblchecklist_status.name as status')
                ->from(db_prefix() . 'checklist_logs')
                ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
                ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
                ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
            ->where('tblchecklist_logs.checklist_id', $id)->get()->result_array();
        }
    }

    // Checklist statuses

    /**
     * Get checklist status by id
     * @param  mixed $id status id
     * @return mixed     if id passed return object else array
     */
    public function get_checklist_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('statusid', $id);

            return $this->db->get(db_prefix() . 'checklist_status')->row();
        }

        return $this->db->get(db_prefix() . 'checklist_status')->result_array();
    }

    /**
     * Add new checklist status
     * @param array checklist status $_POST data
     * @return mixed
     */
    public function add_checklist_status($data)
    {
        $this->db->insert(db_prefix() . 'checklist_status', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New Checklist Status Added [ID: ' . $insert_id . ', ' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Update checklist status
     * @param  array $data checklist status $_POST data
     * @param  mixed $id   checklist status id
     * @return boolean
     */
    public function update_checklist_status($data, $id)
    {
        $this->db->where('statusid', $id);
        $this->db->update(db_prefix() . 'checklist_status', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('Checklist Status Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete checklist status
     * @param  mixed $id checklist status id
     * @return mixed
     */
    public function delete_checklist_status($id)
    {
        $current = $this->get_checklist_status($id);
        // Default statuses cant be deleted
        if ($current->isdefault == 1) {
            return [
                'default' => true,
            ];
        // Not default check if if used in table
        } elseif (is_reference_in_table('status', db_prefix() . 'checklist', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $this->db->where('statusid', $id);
        $this->db->delete(db_prefix() . 'checklist_status');
        if ($this->db->affected_rows() > 0) {
            log_activity('Checklist Status Deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }
}