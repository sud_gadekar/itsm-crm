<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     * Used in home dashboard page
     * Return all upcoming events this week
     */
    public function get_upcoming_events()
    {
        $monday_this_week = date('Y-m-d', strtotime('monday this week'));
        $sunday_this_week = date('Y-m-d', strtotime('sunday this week'));

        $this->db->where("(start BETWEEN '$monday_this_week' and '$sunday_this_week')");
        $this->db->where('(userid = ' . get_staff_user_id() . ' OR public = 1)');
        $this->db->order_by('start', 'desc');
        $this->db->limit(6);

        return $this->db->get(db_prefix() . 'events')->result_array();
    }

    /**
     * @param  integer (optional) Limit upcoming events
     * @return integer
     * Used in home dashboard page
     * Return total upcoming events next week
     */
    public function get_upcoming_events_next_week()
    {
        $monday_this_week = date('Y-m-d', strtotime('monday next week'));
        $sunday_this_week = date('Y-m-d', strtotime('sunday next week'));
        $this->db->where("(start BETWEEN '$monday_this_week' and '$sunday_this_week')");
        $this->db->where('(userid = ' . get_staff_user_id() . ' OR public = 1)');

        return $this->db->count_all_results(db_prefix() . 'events');
    }

    /**
     * @param  mixed
     * @return array
     * Used in home dashboard page, currency passed from javascript (undefined or integer)
     * Displays weekly payment statistics (chart)
     */
    public function get_weekly_payments_statistics($currency)
    {
        $all_payments                 = [];
        $has_permission_payments_view = has_permission('payments', '', 'view');
        $this->db->select(db_prefix() . 'invoicepaymentrecords.id, amount,' . db_prefix() . 'invoicepaymentrecords.date');
        $this->db->from(db_prefix() . 'invoicepaymentrecords');
        $this->db->join(db_prefix() . 'invoices', '' . db_prefix() . 'invoices.id = ' . db_prefix() . 'invoicepaymentrecords.invoiceid');
        $this->db->where('YEARWEEK(' . db_prefix() . 'invoicepaymentrecords.date) = YEARWEEK(CURRENT_DATE)');
        $this->db->where('' . db_prefix() . 'invoices.status !=', 5);
        if ($currency != 'undefined') {
            $this->db->where('currency', $currency);
        }

        if (!$has_permission_payments_view) {
            $this->db->where('invoiceid IN (SELECT id FROM ' . db_prefix() . 'invoices WHERE addedfrom=' . get_staff_user_id() . ')');
        }

        // Current week
        $all_payments[] = $this->db->get()->result_array();
        $this->db->select(db_prefix() . 'invoicepaymentrecords.id, amount,' . db_prefix() . 'invoicepaymentrecords.date');
        $this->db->from(db_prefix() . 'invoicepaymentrecords');
        $this->db->join(db_prefix() . 'invoices', '' . db_prefix() . 'invoices.id = ' . db_prefix() . 'invoicepaymentrecords.invoiceid');
        $this->db->where('YEARWEEK(' . db_prefix() . 'invoicepaymentrecords.date) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY) ');

        $this->db->where('' . db_prefix() . 'invoices.status !=', 5);
        if ($currency != 'undefined') {
            $this->db->where('currency', $currency);
        }
        // Last Week
        $all_payments[] = $this->db->get()->result_array();

        $chart = [
            'labels'   => get_weekdays(),
            'datasets' => [
                [
                    'label'           => _l('this_week_payments'),
                    'backgroundColor' => 'rgba(37,155,35,0.2)',
                    'borderColor'     => '#84c529',
                    'borderWidth'     => 1,
                    'tension'         => false,
                    'data'            => [
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    ],
                ],
                [
                    'label'           => _l('last_week_payments'),
                    'backgroundColor' => 'rgba(197, 61, 169, 0.5)',
                    'borderColor'     => '#c53da9',
                    'borderWidth'     => 1,
                    'tension'         => false,
                    'data'            => [
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    ],
                ],
            ],
        ];


        for ($i = 0; $i < count($all_payments); $i++) {
            foreach ($all_payments[$i] as $payment) {
                $payment_day = date('l', strtotime($payment['date']));
                $x           = 0;
                foreach (get_weekdays_original() as $day) {
                    if ($payment_day == $day) {
                        $chart['datasets'][$i]['data'][$x] += $payment['amount'];
                    }
                    $x++;
                }
            }
        }

        return $chart;
    }

    public function projects_status_stats()
    {
        $this->load->model('projects_model');
        $statuses = $this->projects_model->get_project_statuses();
        $colors   = get_system_favourite_colors();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];


        $has_permission = has_permission('projects', '', 'view');
        $sql            = '';
        foreach ($statuses as $status) {
            $sql .= ' SELECT COUNT(*) as total';
            $sql .= ' FROM ' . db_prefix() . 'projects';
            $sql .= ' WHERE status=' . $status['id'];
            if (!$has_permission) {
                $sql .= ' AND id IN (SELECT project_id FROM ' . db_prefix() . 'project_members WHERE staff_id=' . get_staff_user_id() . ')';
            }
            $sql .= ' UNION ALL ';
            $sql = trim($sql);
        }

        $result = [];
        if ($sql != '') {
            // Remove the last UNION ALL
            $sql    = substr($sql, 0, -10);
            $result = $this->db->query($sql)->result();
        }

        foreach ($statuses as $key => $status) {
            array_push($_data['statusLink'], admin_url('projects?status=' . $status['id']));
            array_push($chart['labels'], $status['name']);
            array_push($_data['backgroundColor'], $status['color']);
            array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
            array_push($_data['data'], $result[$key]->total);
        }

        $chart['datasets'][]           = $_data;
        $chart['datasets'][0]['label'] = _l('home_stats_by_project_status');

        return $chart;
    }

    public function leads_status_stats()
    {
        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $result = get_leads_summary();

        foreach ($result as $status) {
            if ($status['color'] == '') {
                $status['color'] = '#737373';
            }
            array_push($chart['labels'], $status['name']);
            array_push($_data['backgroundColor'], $status['color']);
            if (!isset($status['junk']) && !isset($status['lost'])) {
                array_push($_data['statusLink'], admin_url('leads?status=' . $status['id']));
            }
            array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
            array_push($_data['data'], $status['total']);
        }

        $chart['datasets'][] = $_data;

        return $chart;
    }

    /**
     * Display total tickets awaiting reply by department (chart)
     * @return array
     */
    public function tickets_awaiting_reply_by_department()
    {
        $this->load->model('departments_model');
        $departments = $this->departments_model->get();
        $colors      = get_system_favourite_colors();
        $chart       = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];

        $i = 0;
        foreach ($departments as $department) {
            if (!is_admin()) {
                if (get_option('staff_access_only_assigned_departments') == 1) {
                    $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                    $departments_ids      = [];
                    if (count($staff_deparments_ids) == 0) {
                        $departments = $this->departments_model->get();
                        foreach ($departments as $department) {
                            array_push($departments_ids, $department['departmentid']);
                        }
                    } else {
                        $departments_ids = $staff_deparments_ids;
                    }
                    if (count($departments_ids) > 0) {
                        $this->db->where('department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                    }
                }
            }
            $this->db->where_in('status', [
                1,
                2,
                4,
            ]);

            $this->db->where('department', $department['departmentid']);
            $total = $this->db->count_all_results(db_prefix() . 'tickets');

            if ($total > 0) {
                $color = '#333';
                if (isset($colors[$i])) {
                    $color = $colors[$i];
                }
                array_push($chart['labels'], $department['name']);
                array_push($_data['backgroundColor'], $color);
                array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                array_push($_data['data'], $total);
            }
            $i++;
        }

        $chart['datasets'][] = $_data;

        return $chart;
    }

    /**
     * Display total tickets awaiting reply by status (chart)
     * @return array
     */
    public function tickets_awaiting_reply_by_status()
    {
        $this->load->model('tickets_model');
        $statuses             = $this->tickets_model->get_ticket_status();
        $_statuses_with_reply = [
            1,
            2,
            4,
        ];

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) {
            if (in_array($status['ticketstatusid'], $_statuses_with_reply)) {
                if (!is_admin()) {
                    if (get_option('staff_access_only_assigned_departments') == 1) {
                        $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                        $departments_ids      = [];
                        if (count($staff_deparments_ids) == 0) {
                            $departments = $this->departments_model->get();
                            foreach ($departments as $department) {
                                array_push($departments_ids, $department['departmentid']);
                            }
                        } else {
                            $departments_ids = $staff_deparments_ids;
                        }
                        if (count($departments_ids) > 0) {
                            $this->db->where('department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                        }
                    }
                }

                $this->db->where('status', $status['ticketstatusid']);
                $total = $this->db->count_all_results(db_prefix() . 'tickets');
                if ($total > 0) {
                    array_push($chart['labels'], ticket_status_translate($status['ticketstatusid']));
                    array_push($_data['statusLink'], admin_url('tickets/index/' . $status['ticketstatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;

        return $chart;
    }

    public function tickets_by_status()
    {
        $this->load->model('tickets_model');
        $this->load->model('departments_model');
        
        $statuses             = $this->tickets_model->get_ticket_status();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];


        foreach ($statuses as $status) 
        {

            if ($status['ticketstatusid']) 
            {
                // if (!is_admin()) {
                //     if (get_option('staff_access_only_assigned_departments') == 1) {
                //         $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                //         $departments_ids      = [];
                //         if (count($staff_deparments_ids) == 0) {
                //             $departments = $this->departments_model->get();
                //             foreach ($departments as $department) {
                //                 array_push($departments_ids, $department['departmentid']);
                //             }
                //         } else {
                //             $departments_ids = $staff_deparments_ids;
                //         }
                //         if (count($departments_ids) > 0) {
                //             $this->db->where('department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                //         }
                //     }
                // }

                $this->db->where('status', $status['ticketstatusid']);
                if(!is_admin())
                {
                    $this->db->where('assigned', $this->session->userdata('staff_user_id'));
                }
                $total = $this->db->count_all_results(db_prefix() . 'tickets');
                if ($total > 0) {
                    array_push($chart['labels'], ticket_status_translate($status['ticketstatusid']));
                    array_push($_data['statusLink'], admin_url('tickets/index/' . $status['ticketstatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }
        $chart['datasets'][] = $_data;


        return $chart;
    }



    public function service_requests_by_status()
    {
        $this->load->model('service_requests_model');
        $statuses             = $this->service_requests_model->get_service_request_status();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
            if ($status['service_requeststatusid']) 
            {   

                // if (!is_admin()) {
                //     if (get_option('staff_access_only_assigned_departments') == 1) {
                //         $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                //         $departments_ids      = [];
                //         if (count($staff_deparments_ids) == 0) {
                //             $departments = $this->departments_model->get();
                //             foreach ($departments as $department) {
                //                 array_push($departments_ids, $department['departmentid']);
                //             }
                //         } else {
                //             $departments_ids = $staff_deparments_ids;
                //         }
                //         if (count($departments_ids) > 0) {
                //             $this->db->where('department IN (SELECT departmentid FROM ' . db_prefix() . 'staff_departments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                //         }
                //     }
                // }
                $this->db->where('status', $status['service_requeststatusid']);
                if(!is_admin())
                {
                    $this->db->where('assigned', $this->session->userdata('staff_user_id'));
                }
                $total = $this->db->count_all_results(db_prefix() . 'service_requests');
                if ($total > 0) {
                    array_push($chart['labels'], service_request_status_translate($status['service_requeststatusid']));
                    array_push($_data['statusLink'], admin_url('service_requests/index/' . $status['service_requeststatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }
        $chart['datasets'][] = $_data;


        return $chart;
    }


    public function projects_by_status()
    {
        $statuses = hooks()->apply_filters('before_get_project_statuses', [
            [
                'id'             => 1,
                'color'          => '#989898',
                'name'           => _l('project_status_1'),
                'order'          => 1,
                'filter_default' => true,
            ],
            [
                'id'             => 2,
                'color'          => '#03a9f4',
                'name'           => _l('project_status_2'),
                'order'          => 2,
                'filter_default' => true,
            ],
            [
                'id'             => 3,
                'color'          => '#ff6f00',
                'name'           => _l('project_status_3'),
                'order'          => 3,
                'filter_default' => true,
            ],
            [
                'id'             => 4,
                'color'          => '#84c529',
                'name'           => _l('project_status_4'),
                'order'          => 100,
                'filter_default' => false,
            ],
            [
                'id'             => 5,
                'color'          => '#989898',
                'name'           => _l('project_status_5'),
                'order'          => 4,
                'filter_default' => false,
            ],
        ]);

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
            if ($status['id']) 
            {
                $this->db->where('status', $status['id']);
                $total = $this->db->count_all_results(db_prefix() . 'projects');
                if ($total > 0) {
                    array_push($chart['labels'], $status['name']);
                    array_push($_data['statusLink'], admin_url('projects/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $status['color']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;

        return $chart;

    }

    public function tasks_by_status()
    {
        $statuses = hooks()->apply_filters('before_get_task_statuses', [
            [
                'id'             => 1,
                'color'          => '#989898',
                'name'           => _l('task_status_1'),
                'order'          => 1,
                'filter_default' => true,
            ],
            [
                'id'             => 2,
                'color'          => '#adca65',
                'name'           => _l('task_status_2'),
                'order'          => 2,
                'filter_default' => true,
            ],
            [
                'id'             => 3,
                'color'          => '#2d2d2d',
                'name'           => _l('On Hold'),
                'order'          => 3,
                'filter_default' => true,
            ],
            [
                'id'             => 4,
                'color'          => '#03A9F4',
                'name'           => _l('task_status_4'),
                'order'          => 4,
                'filter_default' => true,
            ],
            [
                'id'             => 5,
                'color'          => '#84c529',
                'name'           => _l('task_status_5'),
                'order'          => 100,
                'filter_default' => false,
            ],
        ]);

         $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
            if ($status['id']) 
            {
                $this->db->where('status', $status['id']);
                if(!is_admin())
                {
                    $this->db->join('tbltask_assigned','tbltask_assigned.taskid = tbltasks.id')
                             ->where('tbltask_assigned.staffid', $this->session->userdata('staff_user_id'));
                }
                $total = $this->db->count_all_results(db_prefix() . 'tasks');
                if ($total > 0) {
                    array_push($chart['labels'], $status['name']);
                    array_push($_data['statusLink'], admin_url('projects/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $status['color']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;

        return $chart;
    }

    public function gaps_by_status()
    {
        $statuses = $this->db->select('*')->get('tblgap_status')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
           
            $color = '#085ec0';
            //$color = sprintf('#%06X', mt_rand(0xFF9999, 0xFFFF00));
            if ($status['id']) 
            {
                $this->db->where('status_id', $status['id']);
                if(!is_admin())
                {
                    $this->db->where('staffid', $this->session->userdata('staff_user_id'));
                }
                $total = $this->db->count_all_results(db_prefix() . 'gaps');
                if ($total > 0) {
                    array_push($chart['labels'], $status['status_title']);
                    array_push($_data['statusLink'], admin_url('gaps/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;

    }
    public function inventorys_by_status()
    {
        $statuses = $this->db->select('*')->get('tblinventory_types')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
            //$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
            $color = '#73ef8d';
            if ($status['id']) 
            {
                $this->db->where('type', $status['id']);
                $total = $this->db->count_all_results(db_prefix() . 'inventory_details');
                if ($total > 0) {
                    array_push($chart['labels'], $status['type_name']);
                    array_push($_data['statusLink'], admin_url('inventory_details/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;
    }

    public function inventorysubtypes_by_status()
    {
        $statuses = $this->db->select('*')->get('tblinventory_types')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        foreach ($statuses as $status) 
        {
            //$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
            $color = '#ca4867';
            if ($status['id']) 
            {
                $this->db->where('sub_type', $status['id']);
                $total = $this->db->count_all_results(db_prefix() . 'inventory_details');
                if ($total > 0) {
                    array_push($chart['labels'], $status['type_name']);
                    array_push($_data['statusLink'], admin_url('inventory_details/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;
    }



    //Client View PieChart

     public function tickets_by_status_clientid()
    {
        $this->load->model('tickets_model');
        $statuses             = $this->tickets_model->get_ticket_status();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');


        foreach ($statuses as $status) 
        {
            if ($status['ticketstatusid']) 
            {
                $this->db->where('status', $status['ticketstatusid'])->where('userid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'tickets');
                if ($total > 0) {
                    array_push($chart['labels'], ticket_status_translate($status['ticketstatusid']));
                    array_push($_data['statusLink'], admin_url('tickets/index/' . $status['ticketstatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }
        $chart['datasets'][] = $_data;


        return $chart;
    }

    public function service_requests_by_status_clientid()
    {
        $this->load->model('service_requests_model');
        $statuses             = $this->service_requests_model->get_service_request_status();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            if ($status['service_requeststatusid']) 
            {
                $this->db->where('status', $status['service_requeststatusid'])->where('userid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'service_requests');
                if ($total > 0) {
                    array_push($chart['labels'], service_request_status_translate($status['service_requeststatusid']));
                    array_push($_data['statusLink'], admin_url('service_requests/index/' . $status['service_requeststatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }
        $chart['datasets'][] = $_data;


        return $chart;
    }

     public function projects_by_status_clientid()
    {
        $statuses = hooks()->apply_filters('before_get_project_statuses', [
            [
                'id'             => 1,
                'color'          => '#989898',
                'name'           => _l('project_status_1'),
                'order'          => 1,
                'filter_default' => true,
            ],
            [
                'id'             => 2,
                'color'          => '#03a9f4',
                'name'           => _l('project_status_2'),
                'order'          => 2,
                'filter_default' => true,
            ],
            [
                'id'             => 3,
                'color'          => '#ff6f00',
                'name'           => _l('project_status_3'),
                'order'          => 3,
                'filter_default' => true,
            ],
            [
                'id'             => 4,
                'color'          => '#84c529',
                'name'           => _l('project_status_4'),
                'order'          => 100,
                'filter_default' => false,
            ],
            [
                'id'             => 5,
                'color'          => '#989898',
                'name'           => _l('project_status_5'),
                'order'          => 4,
                'filter_default' => false,
            ],
        ]);

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            if ($status['id']) 
            {
                $this->db->where('status', $status['id'])->where('clientid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'projects');
                if ($total > 0) {
                    array_push($chart['labels'], $status['name']);
                    array_push($_data['statusLink'], admin_url('projects/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $status['color']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;

        return $chart;

    }


    public function tasks_by_status_clientid()
    {
        $statuses = hooks()->apply_filters('before_get_task_statuses', [
            [
                'id'             => 1,
                'color'          => '#989898',
                'name'           => _l('task_status_1'),
                'order'          => 1,
                'filter_default' => true,
            ],
            [
                'id'             => 2,
                'color'          => '#adca65',
                'name'           => _l('task_status_2'),
                'order'          => 2,
                'filter_default' => true,
            ],
            [
                'id'             => 3,
                'color'          => '#2d2d2d',
                'name'           => _l('On Hold'),
                'order'          => 3,
                'filter_default' => true,
            ],
            [
                'id'             => 4,
                'color'          => '#03A9F4',
                'name'           => _l('task_status_4'),
                'order'          => 4,
                'filter_default' => true,
            ],
            [
                'id'             => 5,
                'color'          => '#84c529',
                'name'           => _l('task_status_5'),
                'order'          => 100,
                'filter_default' => false,
            ],
        ]);

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            if ($status['id']) 
            {
                $this->db->where('status', $status['id']);
                $total = $this->db->count_all_results(db_prefix() . 'tasks');
                if ($total > 0) {
                    array_push($chart['labels'], $status['name']);
                    array_push($_data['statusLink'], admin_url('taska/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $status['color']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;

        return $chart;

    }


    public function gaps_by_status_clientid()
    {
        $statuses = $this->db->select('*')->get('tblgap_status')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            $color = '#085ec0';
            if ($status['id']) 
            {
                $this->db->where('status_id', $status['id'])->where('clientid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'gaps');
                if ($total > 0) {
                    array_push($chart['labels'], $status['status_title']);
                    array_push($_data['statusLink'], admin_url('gaps/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;

    }
    public function inventorys_by_status_clientid()
    {
        $statuses = $this->db->select('*')->get('tblinventory_types')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            $color = '#73ef8d';
            if ($status['id']) 
            {
                $this->db->where('type', $status['id'])->where('clientid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'inventory_details');
                if ($total > 0) {
                    array_push($chart['labels'], $status['type_name']);
                    array_push($_data['statusLink'], admin_url('inventory_details/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;
    }

    public function inventorysubtypes_by_status_clientid()
    {
        $statuses = $this->db->select('*')->get('tblinventory_types')->result_array();

        $chart = [
            'labels'   => [],
            'datasets' => [],
        ];

        $_data                         = [];
        $_data['data']                 = [];
        $_data['backgroundColor']      = [];
        $_data['hoverBackgroundColor'] = [];
        $_data['statusLink']           = [];

        $data['session'] = $this->session->userdata('client_user_id');

        foreach ($statuses as $status) 
        {
            $color = '#ca4867';
            if ($status['id']) 
            {
                $this->db->where('sub_type', $status['id'])->where('clientid',$data['session']);
                $total = $this->db->count_all_results(db_prefix() . 'inventory_details');
                if ($total > 0) {
                    array_push($chart['labels'], $status['type_name']);
                    array_push($_data['statusLink'], admin_url('inventory_details/index/' . $status['id']));
                    array_push($_data['backgroundColor'], $color);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        
        return $chart;
    }

public function all_data_count($client_id)
    {
        
          $res= $this->db->select('count(*) as total')->from('tbltickets')->where('contactid',$client_id)->get()->row();
          return ($res) ? $res->total : 0;
    }

    public function all_data_count_sr($client_id)
    {
        
          $res= $this->db->select('count(*) as total')->from('tblservice_requests')->where('contactid',$client_id)->get()->row();
          return ($res) ? $res->total : 0;
            

    }

      public function all_data_count_project($client_id)
    {
        
          $res= $this->db->select('count(*) as total')->from('tblprojects')->where('clientid',$client_id)->get()->row();
          return ($res) ? $res->total : 0;


    }

     public function all_data_count_gap($client_id)
    {
        
          $res= $this->db->select('count(*) as total')->from('tblgaps')->where('clientid',$client_id)->get()->row();
          return ($res) ? $res->total : 0;


    } 
     public function all_data_count_order($client_id)
    {
        
          $res= $this->db->select('count(*) as total')->from('tblorder_tracker')->where('client_id',$client_id)->get()->row();
        //  print_r($res);exit;
          return ($res) ? $res->total : 0;


    }

      public function all_data_count_knowledge($client_id)
    {
         // $res= $this->db->select('clients')->from('tblknowledge_base')->get();
         // print_r($res);exit;
           
          $res= $this->db->select('count(*) as total')->from('tblknowledge_base')->get()->row();
          
          return ($res) ? $res->total : 0;


    }


}
