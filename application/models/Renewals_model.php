<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Renewals_model extends App_Model
{
    private $piping = false;

    public function __construct()
    {
        parent::__construct(); 
    }

    
    public function get($id = '', $where = [], $contact_id = '', $client_id = '')
    {

        //print_r($where);die();
        $this->db->select('tblrenewals.id, tblrenewals.name, tblrenewals.vendor,tblrenewals.start_date,
        ,tblrenewals.end_date, tblclients.company, tblrenewal_types.type_name, tblrenewals.notify_to, tblrenewal_vendors.vendor_name')
                ->from(db_prefix() . 'renewals')
                ->join(db_prefix() . 'clients','tblrenewals.clientid = tblclients.userid')
                ->join(db_prefix() . 'renewal_types','tblrenewals.renewal_type = tblrenewal_types.id')
                ->join(db_prefix() . 'renewal_vendors','tblrenewals.vendor = tblrenewal_vendors.id','left')
                ->where($where);
        if ($id) {
            $this->db->where('tblrenewals.id', $id);
        }
        if($client_id){
            $this->db->where(db_prefix() . 'renewals.clientid', $client_id);
        }
        return $this->db->get()->result_array();
    }

    public function get_by_customer($id)
    {
        $this->db->select('tblrenewals.id, tblrenewals.name, tblrenewals.vendor,tblrenewals.start_date,
        ,tblrenewals.end_date, tblclients.company, tblrenewal_types.type_name, tblrenewals.notify_to')
                ->from(db_prefix() . 'renewals')
                ->join(db_prefix() . 'clients','tblrenewals.clientid = tblclients.userid')
                ->join(db_prefix() . 'renewal_types','tblrenewals.renewal_type = tblrenewal_types.id')
                ->where('tblrenewals.clientid',$id);

        return $this->db->get()->result_array();
    }





    // Get Active Renewal count

    public function get_active_renewal()
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE start_date<= CURDATE() AND CURDATE()<= end_date");
        if($count)
        {
            return $count->num_rows();
        }   
    }

    // Get Expired Renewal

    public function get_expired_renewal()
    {
       $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date<= CURDATE()");
        if($count)
        {
            return $count->num_rows();
        }  
    }


    //Get expired_in_one_month_renewal

    public function get_expired_in_one_month_renewal()
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 30 DAY) AND end_date >= CURDATE()");
        if($count)
        {
            return $count->num_rows();
        } 
    }

    public function get_expired_in_two_month_renewal()
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 60 DAY) AND end_date >= CURDATE() ");
        if($count)
        {
            return $count->num_rows();
        } 
    }




    //Get Renewal By client Id


     // Get Active Renewal count

    public function get_active_renewal_by_client($id)
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE start_date<= CURDATE() AND CURDATE()<= end_date AND clientid = ".$id);
        if($count)
        {
            return $count->num_rows();
        }   
    }

    // Get Expired Renewal

    public function get_expired_renewal_by_client($id)
    {
       $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date<= CURDATE()AND clientid = ".$id);
        if($count)
        {
            return $count->num_rows();
        }  
    }


    //Get expired_in_one_month_renewal

    public function get_expired_in_one_month_renewal_by_client($id)
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 30 DAY) AND end_date >= CURDATE() AND clientid = ".$id);
        if($count)
        {
            return $count->num_rows();
        } 
    }

    public function get_expired_in_two_month_renewal_by_client($id)
    {
        $count = $this->db->query("SELECT * FROM `tblrenewals` WHERE end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 60 DAY) AND end_date >= CURDATE() AND clientid = ".$id);
        if($count)
        {
            return $count->num_rows();
        } 
    }
    /**
     * Add new gap
     * @param mixed $data All $_POST data
     * @return  mixed
     */
    public function add($data)
    {
        $this->db->insert(db_prefix() . 'renewals', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            if (isset($custom_fields)) {
                handle_custom_fields_post($insert_id, $custom_fields);
            }
            log_activity('New Renewal Added [' . $insert_id . ']');
            return $insert_id;
        }

        return false;
    }

    //categories
    /**
     * Get gap category by id
     * @param  mixed $id category id
     * @return mixed     if id passed return object else array
     */
    public function get_type($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'renewal_types')->row();
        }

        return $this->db->get(db_prefix() . 'renewal_types')->result_array();
    }

     public function get_vendor($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->get(db_prefix() . 'renewal_vendors')->row();
        }

        return $this->db->get(db_prefix() . 'renewal_vendors')->result_array();
    }

    /**
     * Add new gap category
     * @param array $data gap category data
     */
    public function add_category($data)
    {
        $this->db->insert(db_prefix() . 'gap_category', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New gap category added [ID: ' . $insert_id . ', Name: ' . $data['category_title'] . ']');
        }

        return $insert_id;
    }

    /**
     * Update gap category
     * @param  array $data gap category $_POST data
     * @param  mixed $id   gap category id
     * @return boolean
     */
    public function update_category($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'gap_category', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('gap category updated [ID: ' . $id . ' Name: ' . $data['category_title'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete gap category
     * @param  mixed $id gap category id
     * @return mixed
     */
    public function delete_category($id)
    {
        // $current = $this->get($id);
        //// Check if the category id is used in gaps table
        // if (is_reference_in_table('category', db_prefix() . 'gaps', $id)) {
        //     return [
        //         'referenced' => true,
        //     ];
        // }
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'gap_category');
        if ($this->db->affected_rows() > 0) {
            log_activity('gap category deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }


    //impacts
    /**
     * Get gap impact by id
     * @param  mixed $id impact id
     * @return mixed     if id passed return object else array
     */
    public function get_impact($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'gap_impact')->row();
        }

        return $this->db->get(db_prefix() . 'gap_impact')->result_array();
    }

    /**
     * Add new gap impact
     * @param array $data gap impact data
     */
    public function add_impact($data)
    {
        $this->db->insert(db_prefix() . 'gap_impact', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New gap impact added [ID: ' . $insert_id . ', Name: ' . $data['impact_title'] . ']');
        }

        return $insert_id;
    }

    /**
     * Update gap impact
     * @param  array $data gap impact $_POST data
     * @param  mixed $id   gap impact id
     * @return boolean
     */
    public function update_impact($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'gap_impact', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('gap impact updated [ID: ' . $id . ' Name: ' . $data['impact_title'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete gap impact
     * @param  mixed $id gap impact id
     * @return mixed
     */
    public function delete_impact($id)
    {
        // $current = $this->get($id);
        //// Check if the impact id is used in gaps table
        // if (is_reference_in_table('impact', db_prefix() . 'gaps', $id)) {
        //     return [
        //         'referenced' => true,
        //     ];
        // }
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'gap_impact');
        if ($this->db->affected_rows() > 0) {
            log_activity('gap impact deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }

    // gap statuses
    /**
     * Get gap status by id
     * @param  mixed $id status id
     * @return mixed     if id passed return object else array
     */
    public function get_gap_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'gap_status')->row();
        }
        $this->db->order_by('id', 'asc');

        return $this->db->get(db_prefix() . 'gap_status')->result_array();
    }

    /**
     * Add new gap status
     * @param array gap status $_POST data
     * @return mixed
     */
    public function add_gap_status($data)
    {
        $this->db->insert(db_prefix() . 'gap_status', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            log_activity('New gap status added [ID: ' . $insert_id . ', ' . $data['status_title'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
     * Update gap status
     * @param  array $data gap status $_POST data
     * @param  mixed $id   gap status id
     * @return boolean
     */
    public function update_gap_status($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'gap_status', $data);
        if ($this->db->affected_rows() > 0) {
            log_activity('gap status updated [ID: ' . $id . ' Name: ' . $data['status_title'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete gap status
     * @param  mixed $id gap status id
     * @return mixed
     */
    public function delete_gap_status($id)
    {
        // $current = $this->get_gap_status($id);
        // // Default statuses cant be deleted
        // if ($current->isdefault == 1) {
        //     return [
        //         'default' => true,
        //     ];
        // // Not default check if if used in table
        // } elseif (is_reference_in_table('status', db_prefix() . 'gaps', $id)) {
        //     return [
        //         'referenced' => true,
        //     ];
        // }
        $this->db->where('id', $id);
        $this->db->delete(db_prefix() . 'gap_status');
        if ($this->db->affected_rows() > 0) {
            log_activity('gap status deleted [ID: ' . $id . ']');

            return true;
        }

        return false;
    }
}