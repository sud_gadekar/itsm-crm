<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq extends ClientsController
{
    public function index($id, $hash)
    {
        echo "BOQ"; die;
        check_boq_restrictions($id, $hash);
        $boq = $this->boqs_model->get($id);

        if ($boq->rel_type == 'customer' && !is_client_logged_in()) {
            load_client_language($boq->rel_id);
        } else if($boq->rel_type == 'lead') {
            load_lead_language($boq->rel_id);
        }

        $identity_confirmation_enabled = get_option('boq_accept_identity_confirmation');
        if ($this->input->post()) {
            $action = $this->input->post('action');
            switch ($action) {
                case 'boq_pdf':

                    $boq_number = format_boq_number($id);
                    $companyname     = get_option('invoice_company_name');
                    if ($companyname != '') {
                        $boq_number .= '-' . mb_strtoupper(slug_it($companyname), 'UTF-8');
                    }

                    try {
                        $pdf = boq_pdf($boq);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                        die;
                    }

                    $pdf->Output($boq_number . '.pdf', 'D');

                    break;
                case 'boq_comment':
                    // comment is blank
                    if (!$this->input->post('content')) {
                        redirect($this->uri->uri_string());
                    }
                    $data               = $this->input->post();
                    $data['boqid'] = $id;
                    $this->boqs_model->add_comment($data, true);
                    redirect($this->uri->uri_string() . '?tab=discussion');

                    break;
                case 'accept_boq':
                    $success = $this->boqs_model->mark_action_status(3, $id, true);
                    if ($success) {
                        process_digital_signature_image($this->input->post('signature', false), boq_ATTACHMENTS_FOLDER . $id);

                        $this->db->where('id', $id);
                        $this->db->update(db_prefix().'boqs', get_acceptance_info_array());
                        redirect($this->uri->uri_string(), 'refresh');
                    }

                    break;
                case 'decline_boq':
                    $success = $this->boqs_model->mark_action_status(2, $id, true);
                    if ($success) {
                        redirect($this->uri->uri_string(), 'refresh');
                    }

                    break;
            }
        }

        $number_word_lang_rel_id = 'unknown';
        if ($boq->rel_type == 'customer') {
            $number_word_lang_rel_id = $boq->rel_id;
        }
        $this->load->library('app_number_to_word', [
            'clientid' => $number_word_lang_rel_id,
        ],'numberword');

        $this->disableNavigation();
        $this->disableSubMenu();

        $data['title']     = $boq->subject;
        $data['boq']  = hooks()->apply_filters('boq_html_pdf_data', $boq);
        $data['bodyclass'] = 'boq boq-view';

        $data['identity_confirmation_enabled'] = $identity_confirmation_enabled;
        if ($identity_confirmation_enabled == '1') {
            $data['bodyclass'] .= ' identity-confirmation';
        }

        $this->app_scripts->theme('sticky-js','assets/plugins/sticky/sticky.js');

        $data['comments'] = $this->boqs_model->get_comments($id);
        add_views_tracking('boq', $id);
        hooks()->do_action('boq_html_viewed', $id);
        $this->app_css->remove('reset-css','customers-area-default');
        $data                      = hooks()->apply_filters('boq_customers_area_view_data', $data);
        no_index_customers_area();
        $this->data($data);
        $this->view('viewboq');
        $this->layout();
    }
}
