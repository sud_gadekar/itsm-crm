<?php

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\Validatesteam;

class Teams extends ClientsController
{
    /**
     * @since  2.3.3
     */
    use Validatesteam;

    public $teamId;

    protected $team;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('clients_model');

        $this->teamId = get_team_user_id();
        $this->team   = $this->clients_model->get_team($this->teamId);

        if (!can_loggged_in_user_manage_teams()) {
            show_404();
        }

        hooks()->do_action('after_clients_area_init', $this);
    }

    /**
     * View all customer teams
     *
     * @return mixed
     */
    public function index()
    {
        if (is_gdpr() && get_option('gdpr_enable_consent_for_teams') == '1') {
            $this->load->model('gdpr_model');
            $data['consent_purposes'] = $this->gdpr_model->get_consent_purposes();
        }
        $data['teams']  = $this->clients_model->get_teams(get_client_user_id(), ['active' => 1, 'id !=' => $this->teamId]);
        $data['title']     = _l('customer_teams');
        $data['bodyclass'] = 'teams';
        $this->data($data);
        $this->view('teams');
        $this->layout();
    }

    /**
     * Manage team
     *
     * @param  int $id
     *
     * @return mixed
   

    /**
     * Delete team
     *
     * @param  int $customer_id
     * @param  int $id
     *
     * @return mixed
     */
    public function delete($customer_id, $id)
    {
        $team = $this->clients_model->get_team($id);

        if ($customer_id != get_client_user_id() || $team->is_primary == 1) {
            show_404();
            die();
        }

        $this->clients_model->delete_team($id);

        redirect(site_url('teams'));
    }

    /**
     * Delete the given team profile image
     *
     * @param  int $id
     *
     * @return void
     */
    public function delete_profile_image($id)
    {
        $team = $this->clients_model->get_team($id);

        if ($team->userid != get_client_user_id()) {
            ajax_access_denied();
        }

        $this->clients_model->delete_team_profile_image($id);
    }

    /**
     * Validate email
     *
     * @param  string $email
     * @param  id $id
     *
     * @return boolean
     */
    public function team_email_profile_unique($email, $id)
    {
        return total_rows(
            db_prefix() . 'teams',
            'id !=' . $id . ' AND email="' . get_instance()->db->escape_str($email) . '"'
        ) > 0 ? false : true;
    }
}
