<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('tickets_model');
        $this->load->model('gaps_model');
    }

    /* This is admin dashboard view */
    public function index()
    {
        close_setup_menu();
        $this->load->model('departments_model');
        $this->load->model('todo_model');
        $data['departments'] = $this->departments_model->get();

        $data['todos'] = $this->todo_model->get_todo_items(0);
        // Only show last 5 finished todo items
        $this->todo_model->setTodosLimit(5);
        $data['todos_finished']            = $this->todo_model->get_todo_items(1);
        $data['upcoming_events_next_week'] = $this->dashboard_model->get_upcoming_events_next_week();
        $data['upcoming_events']           = $this->dashboard_model->get_upcoming_events();
        $data['title']                     = _l('dashboard_string');

        $this->load->model('contracts_model');
        $data['expiringContracts'] = $this->contracts_model->get_contracts_about_to_expire(get_staff_user_id());

        $this->load->model('currencies_model');
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['activity_log']  = $this->misc_model->get_activity_log();
        // Tickets charts
        $tickets_awaiting_reply_by_status     = $this->dashboard_model->tickets_awaiting_reply_by_status();
        $tickets_awaiting_reply_by_department = $this->dashboard_model->tickets_awaiting_reply_by_department();

        $data['tickets_reply_by_status']              = json_encode($tickets_awaiting_reply_by_status);
        $data['tickets_awaiting_reply_by_department'] = json_encode($tickets_awaiting_reply_by_department);

        $data['tickets_reply_by_status_no_json']              = $tickets_awaiting_reply_by_status;
        $data['tickets_awaiting_reply_by_department_no_json'] = $tickets_awaiting_reply_by_department;

        $data['projects_status_stats'] = json_encode($this->dashboard_model->projects_status_stats());
        $data['leads_status_stats']    = json_encode($this->dashboard_model->leads_status_stats());
        $data['google_ids_calendars']  = $this->misc_model->get_google_calendar_ids();
        $data['bodyclass']             = 'dashboard invoices-total-manual';
        $this->load->model('announcements_model');
        $data['staff_announcements']             = $this->announcements_model->get();
        $data['total_undismissed_announcements'] = $this->announcements_model->get_total_undismissed_announcements();

        $this->load->model('projects_model');
        $data['projects_activity'] = $this->projects_model->get_activity('', hooks()->apply_filters('projects_activity_dashboard_limit', 20));
        add_calendar_assets();
        $this->load->model('utilities_model');
        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();

        $this->load->model('proposals_model');
        $data['proposal_statuses'] = $this->proposals_model->get_statuses();

        $wps_currency = 'undefined';
        if (is_using_multiple_currencies()) {
            $wps_currency = $data['base_currency']->id;
        }


        $tickets_by_status = $this->dashboard_model->tickets_by_status();

        $data['tickets_reply_by_status']              = json_encode($tickets_by_status);

        $data['tickets_status_no_json']              = $tickets_by_status;
        

        //print_r($data);die();

        // pie chart SR data

        $service_requests_by_status = $this->dashboard_model->service_requests_by_status();

        $data['service_requests_reply_by_status']              = json_encode($service_requests_by_status);

        $data['service_requests_status_no_json']              = $service_requests_by_status;


        $projects_by_status = $this->dashboard_model->projects_by_status();

        $data['projects_reply_by_status']              = json_encode($projects_by_status);

        $data['projects_status_no_json']              = $projects_by_status;


        $tasks_by_status = $this->dashboard_model->tasks_by_status();

        $data['tasks_reply_by_status']              = json_encode($tasks_by_status);

        $data['tasks_status_no_json']              = $tasks_by_status;


        $gaps_by_status = $this->dashboard_model->gaps_by_status();
        
        $data['gaps_reply_by_status']              = json_encode($gaps_by_status);

        $data['gaps_status_no_json']              = $gaps_by_status;


        $inventorys_by_status = $this->dashboard_model->inventorys_by_status();
        
        $data['inventorys_reply_by_status']              = json_encode($inventorys_by_status);

        $data['inventorys_status_no_json']              = $inventorys_by_status;



        $inventorysubtypes_by_status = $this->dashboard_model->inventorysubtypes_by_status();
        
        $data['inventorysubtypes_reply_by_status']              = json_encode($inventorysubtypes_by_status);

        $data['inventorysubtypes_status_no_json']              = $inventorysubtypes_by_status;





        //print_r($data['tickets_status_no_json']);die();

        $data['weekly_payment_stats'] = json_encode($this->dashboard_model->get_weekly_payments_statistics($wps_currency));

        $data['dashboard'] = true;

        $data['user_dashboard_visibility'] = get_staff_meta(get_staff_user_id(), 'dashboard_widgets_visibility');

        if (!$data['user_dashboard_visibility']) {
            $data['user_dashboard_visibility'] = [];
        } else {
            $data['user_dashboard_visibility'] = unserialize($data['user_dashboard_visibility']);
        }
        $data['user_dashboard_visibility'] = json_encode($data['user_dashboard_visibility']);

        $data = hooks()->apply_filters('before_dashboard_render', $data);

        // Ticket Status
        $data['statuses']             = $this->tickets_model->get_ticket_status();

        $data['gap_statuses'] = $this->gaps_model->get_gap_status();

        $data['project_statuses'] = $this->projects_model->get_project_statuses();
        $data['inventory_types'] = $this->db->where('parent_id',0)->select('*')
                                            ->get(db_prefix().'inventory_types')->result_array();

        foreach ($data['inventory_types'] as $key => $value) {
            $data['inventory_types_counts'][$key] = $this->db->where('type',$value['id'])->select('*')
                                                      ->get(db_prefix().'inventory_details')->num_rows();           
        }   

        $data['network_overviews_types'] = $this->db->where('parent_id',1)->select('*')
                                            ->get(db_prefix().'inventory_types')->result_array();

        foreach ($data['network_overviews_types'] as $key => $value) {
            $data['network_overviews_types_counts'][$key] = $this->db->where('sub_type',$value['id'])->select('*')
                                                      ->get(db_prefix().'inventory_details')->num_rows(); 
        }

        $data['server_overviews_types'] = $this->db->where('parent_id',2)->select('*')
                                            ->get(db_prefix().'inventory_types')->result_array();

        foreach ($data['server_overviews_types'] as $key => $value) {
            $data['server_overviews_types_counts'][$key] = $this->db->where('sub_type',$value['id'])->select('*')
                                                      ->get(db_prefix().'inventory_details')->num_rows(); 
        }

        //print_r($data);die();
        $this->load->view('admin/dashboard/dashboard', $data);
    }

    /* Chart weekly payments statistics on home page / ajax */
    public function weekly_payments_statistics($currency)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->dashboard_model->get_weekly_payments_statistics($currency));
            die();
        }
    }
}
