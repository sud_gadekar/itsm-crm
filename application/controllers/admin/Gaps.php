<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Gaps extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_gaps_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        $this->load->model('gaps_model');
    }

    public function index($status = '', $userid = '')
    {
        if (!has_permission('gaps', '', 'view')) {
            access_denied('gaps');
        }
        close_setup_menu();
        if (!is_numeric($status))
        {
            $status = '';
        }
        // print_r($this->input);die;
        // if ($this->input->is_ajax_request())
        // {
        //     if (!$this->input->post('filters_gap_id'))
        //     {
        //         $tableParams = ['status' => $status, 'userid' => $userid, ];
        //     }
        //     else
        //     {
        //         // request for othes gaps when single gap is opened
        //         $tableParams = ['userid' => $this->input->post('filters_userid'), 'where_not_gap_id' => $this->input->post('filters_gap_id'), ];
        //         if ($tableParams['userid'] == 0)
        //         {
        //             unset($tableParams['userid']);
        //             $tableParams['by_email'] = $this->input->post('filters_email');
        //         }
        //     }

        //     $this->app->get_table_data('gaps', $tableParams);
        // }
        if($this->input->post('id'))
        {
            $where = 'status_id='.$this->input->post('id');
            $data['gaps_lists'] = $this->gaps_model->get($this->input->post('id'), $where);
            $this->load->view('admin/gaps/filter_table', $data);
        }
        else
        {
            $data['chosen_gap_status'] = $status;
            //$data['weekly_gaps_opening_statistics'] = json_encode($this->gaps_model->get_weekly_gaps_opening_statistics());
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();

            $data['title'] = _l('Gaps');
            $data['statuses'] = $this->gaps_model->get_gap_status();
            
            $data['categories'] = $this->gaps_model->get_category();
            $data['impacts'] = $this->gaps_model->get_impact();
            $data['bodyclass'] = 'gaps-page';
            //add_admin_gaps_js_assets();
            $data['default_gaps_list_statuses'] = hooks()->apply_filters('default_gaps_list_statuses', [1, 2, 4]);
            $data['gaps_lists'] = $this->gaps_model->get();
            //print_r($data);die();
            $this->load->view('admin/gaps/list', $data);
        }
    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['gaps_lists'] = '';
            $customerData = $this->input->post();
            $data['gaps_lists'] = $this->gaps_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $this->load->view('admin/gaps/filter_table', $data);
        }
    }

    public function add($id = false)
    {
        if (!has_permission('gaps', '', 'view')) {
            access_denied('gaps');
        }
        if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('gaps', '', 'create')) {
                    access_denied('gaps');
                }

                $data = $this->input->post();

                $id = $this->gaps_model->add($data);
                if ($id) {
                    log_activity('New Gap  Added [' . $id . ']');
                    set_alert('success', _l('added_successfully', _l('Gap')));
                    return redirect(admin_url('gaps'));
                }
            }else {
                if (!has_permission('gaps', '', 'edit')) {
                    access_denied('gaps');
                }
                //print_r($this->input->post());die();    
                $data = $this->input->post();
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'gaps', $data);

                if ($success) {
                    log_activity('Gap  update [' . $data['id'] . ']');
                    set_alert('success', _l('updated_successfully', _l('Gap')));
                }
                return redirect(admin_url('gaps'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('gap'));
        } else {
            $data['gaps'] = $this->db->where('id',$id)->get(db_prefix().'gaps')->row();
            $data['notes'] = $this->db->where('gap_id',$id)->select('*')->get('tblgap_notes')->result_array();
            $title = _l('edit', _l('gap'));
        }
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['staffs'] = $this->db->select('*')->get('tblstaff')->result_array();
        $data['categories'] = $this->db->select('*')->get('tblgap_category')->result_array();
        $data['impacts'] = $this->db->select('*')->get('tblgap_impact')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblgap_status')->result_array();
        $data['bodyclass']  = 'gap';
        $data['title']      = $title;
        //print_r($data);die();
        $this->load->view('admin/gaps/add', $data);
    }

    public function gap_note($id = false)
    {
        if (!has_permission('gaps', '', 'view')) {
            access_denied('gaps');
        }
        $data = $this->input->post();
        if ($this->input->post()) {
            if (isset($data['id'])) {
                if (!has_permission('gaps', '', 'edit')) {
                    access_denied('gaps');
                }
                
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'gap_notes', $data);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Gap Note')));
                }
            }else {
                if (!has_permission('gaps', '', 'create')) {
                    access_denied('gaps');
                }

                $this->db->insert(db_prefix() . 'gap_notes', $data);
                $insert_id = $this->db->insert_id();
                if ($insert_id) {
                    if (isset($custom_fields)) {
                        handle_custom_fields_post($insert_id, $custom_fields);
                    }
                    log_activity('New Gap Note Added [' . $insert_id . ']');
                    set_alert('success', _l('added_successfully', _l('Gap Note')));
                    //return redirect(admin_url('gaps'));
                }
            }
        }

        
        $data['gaps'] = $this->db->where('id',$data['gap_id'])->get(db_prefix().'gaps')->row();
        $data['notes'] = $this->db->where('gap_id',$data['gap_id'])->select('*')->get('tblgap_notes')->result_array();
        $title = _l('edit', _l('gap'));

        $data['categories'] = $this->db->select('*')->get('tblgap_category')->result_array();
        $data['impacts'] = $this->db->select('*')->get('tblgap_impact')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblgap_status')->result_array();
        $data['bodyclass']  = 'gap';
        $data['title']      = $title;

        return redirect(admin_url('gaps/add/'.$data['gap_id']));
        //$this->load->view('admin/gaps/add/'.$data['gap_id'], $data);
    }

    public function delete($gapid)
    {
        if (!has_permission('gaps', '', 'delete')) {
            access_denied('gaps');
        }

        if($gapid)
        {
            $res = $this->db->where('id',$gapid)->delete('tblgaps');
            if($res)
            {
                set_alert('success', _l('deleted', _l('Gap')));
            }
            else
            {
                set_alert('warning', _l('problem_deleting', _l('Gap')));
            }
            redirect(admin_url('gaps'));
        }
    }

    public function gap($id)
    {
        if (!has_permission('gaps', '', 'view')) {
            access_denied('gaps');
        }
        if (!$id)
        {
            redirect(admin_url('gaps/add'));
        }
    }

    
    public function change_status_ajax($id, $status)
    {
        if ($this->input->is_ajax_request())
        {
            echo json_encode($this->gaps_model->change_gap_status($id, $status));
        }
    }
    
    // Categories
    /* Get all gap categories */
    public function categories()
    {
        if (!is_admin())
        {
            access_denied('Gap Categories');
        }
        $data['categories'] = $this->gaps_model->get_category();
        $data['title'] = _l('Categories');
        $this->load->view('admin/gaps/categories/manage', $data);
    }
    /* Add new category od update existing*/
    public function category()
    {
        if (!is_admin())
        {
            access_denied('Gap Categories');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->gaps_model->add_category($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('Gap Category')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->gaps_model->update_category($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('Gap Category')));
                }
            }

        }
    }
    /* Delete gap category */
    public function delete_category($id)
    {
        if (!is_admin())
        {
            access_denied('Gap Categories');
        }
        if (!$id)
        {
            redirect(admin_url('gaps/categories'));
        }
        $response = $this->gaps_model->delete_category($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('Gap Category')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('Gap Category')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Gap Category')));
        }
        redirect(admin_url('gaps/categories'));
    }
    
    // Impacts
    /* Get all gap impacts */
    public function impacts()
    {
        if (!is_admin())
        {
            access_denied('Gap Impacts');
        }
        $data['impacts'] = $this->gaps_model->get_impact();
        $data['title'] = _l('Impacts');
        $this->load->view('admin/gaps/impacts/manage', $data);
    }
    /* Add new impact od update existing*/
    public function impact()
    {
        if (!is_admin())
        {
            access_denied('Gap Impacts');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->gaps_model->add_impact($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('Gap Impact')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->gaps_model->update_impact($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('Gap Impact')));
                }
            }

        }
    }
    /* Delete gap impact */
    public function delete_impact($id)
    {
        if (!is_admin())
        {
            access_denied('Gap Impacts');
        }
        if (!$id)
        {
            redirect(admin_url('gaps/impacts'));
        }
        $response = $this->gaps_model->delete_impact($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('Gap Impact')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('Gap Impact')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Gap Impact')));
        }
        redirect(admin_url('gaps/impacts'));
    }

    // gap statuses
    /* Get all gap statuses */
    public function statuses()
    {
        if (!is_admin())
        {
            access_denied('Gap Statuses');
        }
        $data['statuses'] = $this->gaps_model->get_gap_status();
        $data['title'] = 'Gap Statuses';
        $this->load->view('admin/gaps/statuses/manage', $data);
    }
    /* Add new or edit existing status */
    public function status()
    {
        if (!is_admin())
        {
            access_denied('Gap Statuses');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->gaps_model->add_gap_status($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('Gap Status')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->gaps_model->update_gap_status($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('Gap Status')));
                }
            }

        }
    }
    /* Delete gap status from database */
    public function delete_status($id)
    {
        if (!is_admin())
        {
            access_denied('Gap Statuses');
        }
        if (!$id)
        {
            redirect(admin_url('gaps/statuses'));
        }
        $response = $this->gaps_model->delete_gap_status($id);
        if (is_array($response) && isset($response['default']))
        {
            set_alert('warning', _l('cant_delete_default', _l('Gap Status')));
        }
        elseif (is_array($response) && isset($response['referenced']))
        {
            set_alert('danger', _l('is_referenced', _l('Gap Status')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('Gap Status')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Gap Status')));
        }
        redirect(admin_url('gaps/statuses'));
    }
}

