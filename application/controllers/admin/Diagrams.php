<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Diagrams extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('diagrams_model');
    }

    /* List all knowledgebase diagram */
    public function index()
    {
        if (!has_permission('diagrams', '', 'view')) {
            access_denied('diagrams');
        }
        $data['diagrams_lists'] = $this->diagrams_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        // print_r($data);die();
        $this->load->view('admin/diagrams/diagrams', $data);
    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['diagrams_lists'] = '';
            $customerData = $this->input->post();
            $data['diagrams_lists'] = $this->diagrams_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            //print_r($data);die();
            $this->load->view('admin/diagrams/filter_table', $data);
        }
    }

    /* Add new diagram or edit existing*/
    public function diagram($id = '')
    {
        if (!has_permission('diagrams', '', 'view')) {
            access_denied('diagrams');
        }
        if ($this->input->post()) {
            $data                = $this->input->post();
            $data['description'] = html_purify($this->input->post('description', false));

            if ($id == '') {
                if (!has_permission('diagrams', '', 'create')) {
                    access_denied('diagrams');
                }
                $id = $this->diagrams_model->add_diagram($data);
                if ($id) {
                    handle_diagram_file_upload($id);
                    set_alert('success', _l('Diagram Added Successfully'));
                    redirect(admin_url('diagrams/diagram/' . $id));
                }
            } else {
                if (!has_permission('diagrams', '', 'edit')) {
                    access_denied('diagrams');
                }
                handle_diagram_file_upload($id);
                $success = $this->diagrams_model->update_diagram($data, $id);
                if ($success) {
                    set_alert('success', _l('Diagram Updated Successfully'));
                }
                redirect(admin_url('diagrams/diagram/' . $id));
            }
        }
        if ($id == '') {
            $title = _l('Add New Diagram');
        } else {
            $diagram         = $this->diagrams_model->get($id);
            $data['diagram'] = $diagram;
            $title           = _l('edit', _l('Diagram')) . ' ' . $diagram->title;
        }

        $this->app_scripts->add('tinymce-stickytoolbar',site_url('assets/plugins/tinymce-stickytoolbar/stickytoolbar.js'));
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['bodyclass'] = 'kb-diagram';
        $data['title']     = $title;
        $this->load->view('admin/diagrams/diagram', $data);
    }

    /* Change diagram active or inactive */
    public function change_diagram_status($id, $status)
    {
        if (has_permission('diagrams', '', 'edit')) {
            if ($this->input->is_ajax_request()) {
                $this->diagrams_model->change_diagram_status($id, $status);
            }
        }
    }

    /* Remove diagram image / ajax */
    public function remove_diagram_file($id = '')
    {
        if (!has_permission('diagrams', '', 'delete')) {
            access_denied('diagrams');
        }
        if (!$id) {
            redirect(admin_url('diagrams'));
        }
        hooks()->do_action('before_remove_diagram_file');
        $diagrams = $this->diagrams_model->get($id);
        if (file_exists(get_upload_path_by_type('diagram_file') . $id)) {
            delete_dir(get_upload_path_by_type('diagram_file') . $id);
        }
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'diagrams', [
            'diagram_file' => null,
        ]);

        if (!is_numeric($id)) {
            // print_r('No Id');die();
            redirect(admin_url('diagrams/diagram/' . $id));
        } else {
            set_alert('success', _l('Diagram Deleted'));
            redirect(admin_url('diagrams/diagram/' . $id));
        }
    }

    /* Delete diagram from database */
    public function delete($id)
    {
        if (!has_permission('diagrams', '', 'delete')) {
            access_denied('diagrams');
        }
        if (!$id) {
            redirect(admin_url('diagrams'));
        }
        $response = $this->diagrams_model->delete_diagram($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('diagram')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('diagram')));
        }
        redirect(admin_url('diagrams'));
    }

    public function get_diagram_by_id_ajax($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->diagrams_model->get($id));
        }
    }
}
