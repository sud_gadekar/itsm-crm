<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Renewals extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_renewals_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        $this->load->model('renewals_model');
    }

    public function index()
    {
        if (!has_permission('renewals', '', 'view')) {
            access_denied('renewals');
        }
        $data['active_renewal'] = $this->renewals_model->get_active_renewal();
        $data['expired_renewal'] = $this->renewals_model->get_expired_renewal();
        $data['expired_in_one_month_renewal'] = $this->renewals_model->get_expired_in_one_month_renewal();
        $data['expired_in_two_month_renewal'] = $this->renewals_model->get_expired_in_two_month_renewal();
    	$data['renewals_lists'] = $this->renewals_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['title']      = 'Renewal';

        if($this->input->post())
        {   
            if($this->input->post('id') == 1)
            {
                $where="start_date<= CURDATE() AND CURDATE()<= end_date";
                $data['renewals_lists'] = $this->renewals_model->get(null,$where);
            }
            elseif ($this->input->post('id') == 2) 
            {
               $where="end_date<= CURDATE()";
               $data['renewals_lists'] = $this->renewals_model->get(null,$where);
            }
            elseif ($this->input->post('id') == 3) 
            {
               $where="end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 30 DAY) AND end_date >= CURDATE()";
               $data['renewals_lists'] = $this->renewals_model->get(null,$where);
            }
            elseif ($this->input->post('id') == 4) 
            {
               $where="end_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 60 DAY) AND end_date >= CURDATE()";
               $data['renewals_lists'] = $this->renewals_model->get(null,$where);
            }
            $this->load->view('admin/renewals/filter_table', $data);
        }
        else
        {
            $this->load->view('admin/renewals/manage', $data);
        }

       

    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['renewals_lists'] = '';
            $customerData = $this->input->post();
            $data['renewals_lists'] = $this->renewals_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $this->load->view('admin/renewals/filter_table', $data);
        }
    }

    public function renewal($id = '')
    {
        if (!has_permission('renewals', '', 'view')) {
            access_denied('renewals');
        }
        // print_r($this->input->post());die();
    	if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('renewals', '', 'create')) {
                    access_denied('renewals');
                }

                $data = $this->input->post();

                if(isset($data['notify_to'])){
                    $data['notify_to'] = implode(',', $data['notify_to']);
                }

                if(isset($data['start_date']) && $data['start_date'] != '') 
                {
                	$start_time = to_sql_date($data['start_date'], true);
		            $end_time   = to_sql_date($data['end_date'], true);
		            $start_time = strtotime($start_time);
		            $end_time   = strtotime($end_time);
		            //print_r('Helllo');die();
		            if ($end_time < $start_time) {
			             set_alert('warning', _l('End Date is smaller then start Date'));
			        }
			        else
			        {
			        	$id = $this->renewals_model->add($data);
		                if ($id) {
		                    set_alert('success', _l('added_successfully', _l('Renewals')));
		                    return redirect(admin_url('renewals'));
		                }
			        }
                }
            }
            else 
            { 
                if (!has_permission('renewals', '', 'edit')) {
                    access_denied('renewals');
                } 
                $data = $this->input->post();
                  if(isset($data['notify_to'])){
                    $data['notify_to'] = implode(',', $data['notify_to']);
                  }
                //print_r($data);die();
                if(isset($data['start_date']) && $data['start_date'] != '') 
                {
                	$start_time = to_sql_date($data['start_date'], true);
		            $end_time   = to_sql_date($data['end_date'], true);
		            $start_time = strtotime($start_time);
		            $end_time   = strtotime($end_time);
		            //print_r('Helllo');die();
		            if ($end_time < $start_time) {
			             set_alert('warning', _l('End Date is smaller then start Date'));
			        }
			        else
			        {
			          $success = $this->db->where('id',$data['id'])->update(db_prefix().'renewals', $data);
		                if ($success) {
		                    set_alert('success', _l('updated_successfully', _l('Renewals')));
		                }
		                return redirect(admin_url('renewals'));
			        }
                }
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('Renewal'));
        } else {
            $data['renewals_details'] = $this->db->where('id',$id)->get(db_prefix().'renewals')->row();
            $data['notes']  = $this->misc_model->get_notes($id, 'renewal');
            $title = _l('edit', _l('Renewal'));
        }

        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['renewal_types'] = $this->db->select('*')->get(db_prefix().'renewal_types')->result_array();
        $data['renewal_vendors'] = $this->db->select('*')->get('tblrenewal_vendors')->result_array();
        $data['bodyclass']  = 'renewals';
        $data['title']      = $title;
      // print_r($data);die();
        $this->load->view('admin/renewals/renewal', $data);
    }

    

    /* Add new renewal note */
    public function add_note($rel_id)
    {
        if (!is_staff_member() || !has_permission('renewals', '', 'create')) {
            //access_denied('renewals');
            ajax_access_denied();
        }

        if ($this->input->post()) {
            $data = $this->input->post();

            // Causing issues with duplicate ID or if my prefixed file for renewals.php is used
            $data['description'] = isset($data['renewal_note_description']) ? $data['renewal_note_description'] : $data['description'];

            if (isset($data['renewal_note_description'])) {
                unset($data['renewal_note_description']);
            }

            $note_id = $this->misc_model->add_note($data, 'renewal', $rel_id);

            if ($note_id) {
                set_alert('success', _l('Renewal Note Added Successfully', _l('Renewals')));
		        return redirect(admin_url('renewals/renewal/'.$rel_id));
            }
        }
        set_alert('danger', _l('Something went wrong', _l('Renewals')));
	    return redirect(admin_url('renewals/renewal/'.$rel_id));
    }

    public function delete_note($id, $renewal_id)
    {
        if (!is_staff_member() || !has_permission('renewals', '', 'create')) {
            //access_denied('renewals');
            ajax_access_denied();
        }
        echo json_encode([
            'success' => $this->misc_model->delete_note($id),
        ]);
    }


    public function get_notify_me()
    {
        if($this->input->post())
        {
            $data = $this->input->post();
            $result = $this->db->where('userid',$data['id'])->get('tblcontacts')->result_array();
            echo json_encode($result);
        }
    }


    public function renewals_types()
    {
    	if (!is_admin())
        {
            access_denied('Renewals Type');
        }

        $data['renewals_types'] = $this->renewals_model->get_type();
        $data['title'] = _l('Type');
        //print_r($data);die();
        $this->load->view('admin/renewals/types/manage', $data);
    }

    public function renewals_vendors()
    {
        if (!is_admin())
        {
            access_denied('Renewals Vendor');
        }

        $data['renewals_vendors'] = $this->renewals_model->get_vendor();
        $data['title'] = _l('Vendor');
        //print_r($data);die();
        $this->load->view('admin/renewals/vendors/manage', $data);
    }



    //Add Provisional Type Of Renewals
    
    public function type()
    {
    	if($this->input->post()){
            $data = $this->input->post();
            if(!$data['id']){
                $id = $this->db->insert('tblrenewal_types',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Renewal type')));    //return redirect(
                    
                }
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'renewal_types',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('Renewal type')));    //return redirect(
                    
                }
            }       
        } 
    }

    public function vendor()
    {
        if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            if(!isset($data['id'])){
                $id = $this->db->insert('tblrenewal_vendors',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Renewal vendor')));    //return redirect(
                    $this->renewals_vendors();
                }
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'renewal_vendors',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('Renewal vendor')));    //return redirect(
                    $this->renewals_vendors();
                }
            }       
        } 
    }




    public function delete($id)
    {
        if (!has_permission('renewals', '', 'delete')) {
            access_denied('renewals');
        }
        if (!$id) {
            redirect(admin_url('renewals'));
        }
        $response = $this->db->where('id',$id)->delete(db_prefix().'renewals');
        if ($response === true) 
        {
            set_alert('success', _l('deleted', _l('Renewals')));
            redirect(admin_url('renewals'));
        } else 
        {  
            set_alert('warning', _l('problem_deleting', _l('Renewals')));
            redirect(admin_url('renewals'));
        }
    }
}

