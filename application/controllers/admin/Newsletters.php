<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Newsletters extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsletters_model');
    }

    /* List all knowledgebase newsletter */
    public function index()
    {
        // if (!has_permission('newsletters', '', 'view')) {
        //     access_denied('newsletters');
        // }
        $data['newsletters_lists'] = $this->newsletters_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        // print_r($data);die();
        $this->load->view('admin/newsletters/newsletters', $data);
    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['newsletters_lists'] = '';
            $customerData = $this->input->post();
            $data['newsletters_lists'] = $this->newsletters_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            //print_r($data);die();
            $this->load->view('admin/newsletters/filter_table', $data);
        }
    }

    /* Add new newsletter or edit existing*/
    public function newsletter($id = '')
    {
        // if (!has_permission('newsletters', '', 'view')) {
        //     access_denied('newsletters');
        // }
        if ($this->input->post()) {
            $data                = $this->input->post();

            if ($id == '') {
                // if (!has_permission('newsletters', '', 'create')) {
                //     access_denied('newsletters');
                // }
                $id = $this->newsletters_model->add_newsletter($data);
                if ($id) {
                    handle_nl_file_upload($id);
                    set_alert('success', _l('Newsletter / Educational Content Added Successfully'));
                    redirect(admin_url('newsletters/newsletter/' . $id));
                }
            } else {
                // if (!has_permission('newsletters', '', 'edit')) {
                //     access_denied('newsletters');
                // }
                handle_nl_file_upload($id);
                $success = $this->newsletters_model->update_newsletter($data, $id);
                if ($success) {
                    set_alert('success', _l('Newsletter / Educational Content Updated Successfully'));
                }
                redirect(admin_url('newsletters/newsletter/' . $id));
            }
        }
        if ($id == '') {
            $title = _l('Add New Newsletter / Educational Content');
        } else {
            $newsletter         = $this->newsletters_model->get($id);
            $data['newsletter'] = $newsletter;
            $title           = _l('edit', _l('Newsletter / Educational Content'));
        }

        $this->app_scripts->add('tinymce-stickytoolbar',site_url('assets/plugins/tinymce-stickytoolbar/stickytoolbar.js'));
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['bodyclass'] = 'kb-newsletter';
        $data['title']     = $title;
        $this->load->view('admin/newsletters/newsletter', $data);
    }

    /* Change newsletter active or inactive */
    public function change_newsletter_status($id, $status)
    {
        if (has_permission('newsletters', '', 'edit')) {
            if ($this->input->is_ajax_request()) {
                $this->newsletters_model->change_newsletter_status($id, $status);
            }
        }
    }

    /* Remove newsletter image / ajax */
    public function remove_nl_file($id = '')
    {
        // if (!has_permission('newsletters', '', 'delete')) {
        //     access_denied('newsletters');
        // }
        if (!$id) {
            redirect(admin_url('newsletters'));
        }
        hooks()->do_action('before_remove_nl_file');
        $newsletters = $this->newsletters_model->get($id);
        if (file_exists(get_upload_path_by_type('nl_file') . $id)) {
            delete_dir(get_upload_path_by_type('nl_file') . $id);
        }
        $this->db->where('id', $id);
        $this->db->update(db_prefix() . 'newsletters', [
            'nl_file' => null,
        ]);

        if (!is_numeric($id)) {
            // print_r('No Id');die();
            redirect(admin_url('newsletters/newsletter/' . $id));
        } else {
            set_alert('success', _l('Newsletter Deleted'));
            redirect(admin_url('newsletters/newsletter/' . $id));
        }
    }

    /* Delete newsletter from database */
    public function delete($id)
    {
        // if (!has_permission('newsletters', '', 'delete')) {
        //     access_denied('newsletters');
        // }
        if (!$id) {
            redirect(admin_url('newsletters'));
        }
        $response = $this->newsletters_model->delete_newsletter($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('newsletter')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('newsletter')));
        }
        redirect(admin_url('newsletters'));
    }

    public function get_newsletter_by_id_ajax($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->newsletters_model->get($id));
        }
    }
}
