<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Inventory_details extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('inventory_details_model');
    }

    public function index($id = '')
    {
        if (!has_permission('inventories', '', 'view')) {
            access_denied('inventories');
        }
        if($this->input->post())
        {
            $data = $this->input->post();
            $where = 'sub_type='.$data['id'];
            $data['inventory_details_lists'] = $this->inventory_details_model->get($data['id'],$where);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['subtypes'] = $this->db->where('parent_id !=',0)->get('tblinventory_types')->result_array();

            $this->load->view('admin/inventory_details/filter_table', $data);
        }
        else
        {
            // print_r('edsd');die();
            $data['inventory_details_lists'] = $this->inventory_details_model->get();
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['subtypes'] = $this->db->where('parent_id !=',0)->get('tblinventory_types')->result_array();
        
            $this->load->view('admin/inventory_details/manage', $data);
        }
    }

    public function filter_by_customer()
    {
        if (!has_permission('inventories', '', 'view')) {
            access_denied('inventories');
        }
       if($this->input->post())
        {   $data['inventory_details_lists'] = '';
            $customerData = $this->input->post();
            $data['inventory_details_lists'] = $this->inventory_details_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            //print_r($data);die();
            $this->load->view('admin/inventory_details/filter_table', $data);
        }
    }

    public function list_inventory_details($id = '')
    {
        close_setup_menu();
        if (!has_permission('inventories', '', 'view')) {
            access_denied('inventories');
        }

        $data['inventory_detailid']  = $id;
        $data['years']      = $this->inventory_details_model->get_inventory_details_years();
        $data['title']      = _l('inventory_details');
     
        $this->load->view('admin/inventory_details/manage', $data);
    }

    public function table($clientid = '')
    {
        
        if (!has_permission('inventories', '', 'view')) {
            access_denied('inventories');
        }
        $this->app->get_table_data('inventory_details', [
            'clientid' => $clientid,
        ]);
    }

    public function inventory_detail($id = '')
    {
        if (!has_permission('inventories', '', 'view')) {
            access_denied('inventories');
        }
       
        if ($this->input->post()) {
            if ($id == '') 
            {
                if (!has_permission('inventories', '', 'create')) {
                    access_denied('inventories');
                }
                $data = $this->input->post();

                if(isset($data['customer_prefix']))
                {
                    $res = $this->db->like('customer_prefix',$data['customer_prefix'])
                                    ->get('tblinventory_details')->num_rows();

                    if($res < 9)
                    {
                        $res = "00".++$res;
                    }
                    elseif ($res >= 9 && $res < 99) 
                    {
                        $res = "0".++$res;
                    }
                    else
                    {
                        $res = ++$res;
                    }
                    $data['customer_prefix'] = $data['customer_prefix'].$res;
                }
                $id = $this->inventory_details_model->add($data);
                if ($id) {
                    
                    set_alert('success', _l('added_successfully', _l('Inventory')));
                    return redirect(admin_url('inventory_details'));
                }
            }
            else 
            {  
                if (!has_permission('inventories', '', 'edit')) {
                    access_denied('inventories');
                } 
                $data = $this->input->post();
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'inventory_details', $data);

                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Inventory')));
                }
                return redirect(admin_url('inventory_details'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('inventory_detail_lowercase'));
        } else {
            $data['inventory_details'] = $this->db->where('id',$id)->get(db_prefix().'inventory_details')->row();
            $title = _l('edit', _l('inventory_detail_lowercase'));
        }

        

        $data['parent_types'] = $this->db->where('parent_id',0)->select('*')->get('tblinventory_types')->result_array();
        $data['parent_types'] = $this->db->where('parent_id',0)->select('*')->get('tblinventory_types')->result_array();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['inventory_statuses'] = $this->db->select('*')->get(db_prefix().'inventory_statuses')->result_array();
        $data['inventory_manufactures'] = $this->db->select('*')->get('tblinventory_manufacture')->result_array();
        $data['inventory_products'] = $this->db->select('*')->get('tblinventory_products')->result_array();
        $data['bodyclass']  = 'inventory_detail';
        $data['title']      = $title;
       // print_r($data);die();
        $this->load->view('admin/inventory_details/inventory_detail', $data);
    }


    public function get_sub_type(){
        if($this->input->post()){
            $data = $this->input->post();  
            $result = $this->db->where('parent_id', $data['id'])->select('*')->get('tblinventory_types')->result_array();
            echo json_encode($result);
        }
    }
    public function get_product()
    {
         if($this->input->post()){
            $data = $this->input->post();  
            //print_r($data);die();
            $result = $this->db->where('manufacture_id', $data['id'])->select('*')->get('tblinventory_products')->result_array();
            echo json_encode($result);
        }
    }

    public function get_client_location(){
        if($this->input->post()){
            $data = $this->input->post();
            $location_result = $this->db->where('customer_id', $data['id'])
                                        ->select('*')->get('tblclient_locations')
                                        ->result_array();
            echo json_encode($location_result);
        }
    }

    public function get_client_contract() {
        if($this->input->post()){
            $data = $this->input->post();
            $contract_result = $this->db->where('client', $data['id'])
                                    ->select('*')->get(db_prefix().'contracts')
                                    ->result_array();
            echo json_encode($contract_result);
        }
    }

    public function get_client_contact() {
        if($this->input->post()){
            $data = $this->input->post();
            $contract_result = $this->db->where('userid', $data['id'])
                                    ->select('*')->get(db_prefix().'contacts')
                                    ->result_array();
            echo json_encode($contract_result);
        }
    }
    public function get_client_prefix()
    {
         if($this->input->post()){
            $data = $this->input->post();
            $prefix_result = $this->db->where('userid', $data['id'])
                                    ->select('*')->get(db_prefix().'clients')
                                    ->result_array();
            echo json_encode($prefix_result);
        }
    }
    
    // Provision to Load view  inventory types

    public function inventory_types()
    {
        if (!is_admin()) {
            access_denied('Inventory Type');
        }
        $data['inventory_types'] = $this->inventory_details_model->get_inventory_type(0);
        $data['inventory_subtypes'] = $this->inventory_details_model->get_inventory_subtype(0);
        $data['title']      = _l('Inventory Type');
        //print_r($data);die();
        $this->load->view('admin/inventory_details/inventory_types/manage', $data);
    }

    // Provision to Load view  manufacturers

    public function inventory_manufacturers()
    {
        if (!is_admin()) {
            access_denied('Inventory manufacture');
        }
        $data['inventory_manufactures'] = $this->inventory_details_model->get_inventory_manufacture();
       
        $data['title']      = _l('Inventory manufacture');
        //print_r($data);die();
        $this->load->view('admin/inventory_details/inventory_manufactures/manage', $data);
    }


     // Provision to Load view  inventory product

    public function inventory_products()
    {
        if (!is_admin()) {
            access_denied('Inventory product');
        }
        $data['inventory_products'] = $this->inventory_details_model->get_inventory_product();
        $data['inventory_manufactures'] = $this->inventory_details_model->get_inventory_manufacture();
        $data['title']      = _l('Inventory product');
        //print_r($data);die();
        $this->load->view('admin/inventory_details/inventory_products/manage', $data);
    }


    public function inventory_statuses()
    {
        if (!is_admin()) {
            access_denied('Inventory statuses');
        }
        $data['inventory_statuses'] = $this->inventory_details_model->get_inventory_status();
        //$data['inventory_manufactures'] = $this->inventory_details_model->get_inventory_manufacture();
        $data['title']      = _l('Inventory status');
        //print_r($data);die();
        $this->load->view('admin/inventory_details/inventory_statuses/manage', $data);
    }


    //After sub-mitting type function call
    public function inventory_type(){


        if($this->input->post()){
            $data = $this->input->post();
            if(!$data['id']){
                $data['parent_id'] = 0;
                $id = $this->db->insert('tblinventory_types',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('inventory_type')));    //return redirect(
                }
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'inventory_types',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('inventory_type')));    //return redirect(
                }
            }
            
        } 
    }

       //After sub-mitting subtype function call
    public function inventory_subtype(){
        print_r($this->input->post());
        if($this->input->post()){
            $data = $this->input->post();

            if(!$data['id'])
            {
                $id = $this->db->insert('tblinventory_types',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('inventory subtype')));    //return redirect(
                }
            }
            else
            {
               $id = $this->db->where('id', $data['id'])->update(db_prefix().'inventory_types',$data);
               if ($id) {
                    set_alert('success', _l('updated_successfully', _l('inventory subtype')));    //return redirect(
                }
            }
        }
    }

    public function inventory_product()
    {
           if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            if(!isset($data['id'])){
                $id = $this->db->insert('tblinventory_products',$data);
                if ($id) 
                {
                    set_alert('success', _l('added_successfully', _l('inventory product')));    //return redirect(
                    redirect(admin_url('inventory_details/inventory_products'));
                }
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'inventory_products',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('inventory product')));    //return redirect(
                }
                redirect(admin_url('inventory_details/inventory_products'));
            }
        } 
    }

    public function inventory_product_from_add()
    {
          if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            if(!$data['id']){
                $id = $this->db->insert('tblinventory_products',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('inventory product')));    //return redirect(
                }
                redirect(admin_url('inventory_details/inventory_detail'));
            }
        } 
    }

    public function inventory_statuse()
    {
        if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            if(!$data['id']){
                $id = $this->db->insert('tblinventory_statuses',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('inventory status')));    //return redirect(
                }
                $this->inventory_statuses();
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'inventory_statuses',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('inventory status')));    //return redirect(
                }
                $this->inventory_statuses();
            }
            
        } 
    }
   

    //Delete inventory TYpes

    public function delete_inventory_type($id){ 
        //print_r($id);die();
        if($id){
            $result = $this->db->where('id',$id)->where('parent_id',$id)->delete(db_prefix().'inventory_types');
            //print_r($this->db->last_query());die();
            if($result){
                set_alert('success', _l('deleted_successfully', _l('Inventory type')));    //return redirect(   
            }
        }
    }


    //Delete inventory sub types

    public function delete_inventory_subtype($id){ 
        //print_r($id);die();
        if($id){
            $result = $this->db->where('id',$id)->delete(db_prefix().'inventory_types');
            //print_r($this->db->last_query());die();
            if($result){
                set_alert('success', _l('deleted_successfully', _l('Inventory type')));    //return redirect(   
            }
               redirect(admin_url('inventory_details/inventory_types'));
        }
    }

    public function delete_inventory_statuse($id)
    {
        if($id){
            $result = $this->db->where('id',$id)->delete(db_prefix().'inventory_statuses');
            //print_r($this->db->last_query());die();
            if($result){
                set_alert('success', _l('deleted_successfully', _l('Inventory status')));    //return redirect(   
            }
               redirect(admin_url('inventory_details/inventory_statuses'));
        }
    }










 //After sub-mitting type function call
    public function inventory_manufacture(){

        if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            if(!$data['id']){
                $id = $this->db->insert('tblinventory_manufacture',$data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('inventory manufacture')));    //return redirect(
                }
                $this->inventory_manufacturers();
            }
            else{
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'inventory_manufacture',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('inventory manufacture')));    //return redirect(
                }
                $this->inventory_manufacturers();
            }
            
        } 
    }


    public function get_inventory_details_total()
    {
        if ($this->input->post()) {
            $data['totals'] = $this->inventory_details_model->get_inventory_details_total($this->input->post());

            if ($data['totals']['currency_switcher'] == true) {
                $this->load->model('currencies_model');
                $data['currencies'] = $this->currencies_model->get();
            }

            $data['inventory_details_years'] = $this->inventory_details_model->get_inventory_details_years();

            if (count($data['inventory_details_years']) >= 1 && $data['inventory_details_years'][0]['year'] != date('Y')) {
                array_unshift($data['inventory_details_years'], ['year' => date('Y')]);
            }

            $data['_currency'] = $data['totals']['currencyid'];
            $this->load->view('admin/inventory_details/inventory_details_total_template', $data);
        }
    }

    public function delete($id)
    {
         
        if (!has_permission('inventories', '', 'delete')) {
            access_denied('inventories');
        }
        if (!$id) {
            redirect(admin_url('inventory_details/list_inventory_details'));
        }
        $response = $this->db->where('id',$id)->delete('tblinventory_details');
        if ($response === true) {
            set_alert('success', _l('deleted', _l('Inventory')));
        } else {
            if (is_array($response) && $response['invoiced'] == true) {
                set_alert('warning', _l('inventory_detail_invoice_delete_not_allowed'));
            } else {
                set_alert('warning', _l('problem_deleting', _l('Inventory')));
            }
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'inventory_details/') !== false) {
            redirect(admin_url('inventory_details/list_inventory_details'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function copy($id)
    {
        if (!has_permission('inventories', '', 'create')) {
            access_denied('inventories');
        }
        $new_inventory_detail_id = $this->inventory_details_model->copy($id);
        if ($new_inventory_detail_id) {
            set_alert('success', _l('inventory_detail_copy_success'));
            redirect(admin_url('inventory_details/inventory_detail/' . $new_inventory_detail_id));
        } else {
            set_alert('warning', _l('inventory_detail_copy_fail'));
        }
        redirect(admin_url('inventory_details/list_inventory_details/' . $id));
    }

    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('Convert inventory_detail to Invoice');
        }
        if (!$id) {
            redirect(admin_url('inventory_details/list_inventory_details'));
        }
        $draft_invoice = false;
        if ($this->input->get('save_as_draft')) {
            $draft_invoice = true;
        }

        $params = [];
        if ($this->input->get('include_note') == 'true') {
            $params['include_note'] = true;
        }

        if ($this->input->get('include_name') == 'true') {
            $params['include_name'] = true;
        }

        $invoiceid = $this->inventory_details_model->convert_to_invoice($id, $draft_invoice, $params);
        if ($invoiceid) {
            set_alert('success', _l('inventory_detail_converted_to_invoice'));
            redirect(admin_url('invoices/invoice/' . $invoiceid));
        } else {
            set_alert('warning', _l('inventory_detail_converted_to_invoice_fail'));
        }
        redirect(admin_url('inventory_details/list_inventory_details/' . $id));
    }

    public function get_inventory_detail_data_ajax($id)
    {
        if (!has_permission('inventories', '', 'view')) {
            echo _l('access_denied');
            die;
        }
        $inventory_detail = $this->inventory_details_model->get($id);

        if (!$inventory_detail || (!has_permission('inventories', '', 'view') && $inventory_detail->addedfrom != get_staff_user_id())) {
            echo _l('inventory_detail_not_found');
            die;
        }

        $data['inventory_detail'] = $inventory_detail;
        if ($inventory_detail->billable == 1) {
            if ($inventory_detail->invoiceid !== null) {
                $this->load->model('invoices_model');
                $data['invoice'] = $this->invoices_model->get($inventory_detail->invoiceid);
            }
        }

        $data['child_inventory_details'] = $this->inventory_details_model->get_child_inventory_details($id);
        $data['members']        = $this->staff_model->get('', ['active' => 1]);
        $this->load->view('admin/inventory_details/inventory_detail_preview_template', $data);
    }

    public function get_customer_change_data($customer_id = '')
    {
        echo json_encode([
            'customer_has_projects' => customer_has_projects($customer_id),
            'client_currency'       => $this->clients_model->get_customer_default_currency($customer_id),
        ]);
    }

    public function categories()
    {
        if (!is_admin()) {
            access_denied('inventory_details');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('inventory_details_categories');
        }
        $data['title'] = _l('inventory_detail_categories');
        $this->load->view('admin/inventory_details/manage_categories', $data);
    }

    public function category()
    {
        if (!is_admin() && get_option('staff_members_create_inline_inventory_detail_categories') == '0') {
            access_denied('inventory_details');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->inventory_details_model->add_category($this->input->post());
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $id ? _l('added_successfully', _l('inventory_detail_category')) : '',
                    'id'      => $id,
                    'name'    => $this->input->post('name'),
                ]);
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->inventory_details_model->update_category($data, $id);
                $message = _l('updated_successfully', _l('inventory_detail_category'));
                echo json_encode(['success' => $success, 'message' => $message]);
            }
        }
    }

    public function delete_category($id)
    {
        if (!is_admin()) {
            access_denied('inventory_details');
        }
        if (!$id) {
            redirect(admin_url('inventory_details/categories'));
        }
        $response = $this->inventory_details_model->delete_category($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('inventory_detail_category_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('inventory_detail_category')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('inventory_detail_category_lowercase')));
        }
        redirect(admin_url('inventory_details/categories'));
    }

    public function add_inventory_detail_attachment($id)
    {
        handle_inventory_detail_attachments($id);
        echo json_encode([
            'url' => admin_url('inventory_details/list_inventory_details/' . $id),
        ]);
    }

    public function delete_inventory_detail_attachment($id, $preview = '')
    {
        $this->db->where('rel_id', $id);
        $this->db->where('rel_type', 'inventory_detail');
        $file = $this->db->get(db_prefix().'files')->row();

        if ($file->staffid == get_staff_user_id() || is_admin()) {
            $success = $this->inventory_details_model->delete_inventory_detail_attachment($id);
            if ($success) {
                set_alert('success', _l('deleted', _l('inventory_detail_receipt')));
            } else {
                set_alert('warning', _l('problem_deleting', _l('inventory_detail_receipt_lowercase')));
            }
            if ($preview == '') {
                redirect(admin_url('inventory_details/inventory_detail/' . $id));
            } else {
                redirect(admin_url('inventory_details/list_inventory_details/' . $id));
            }
        } else {
            access_denied('inventory_details');
        }
    }
}
