<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boqs extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('boqs_model');
        $this->load->model('currencies_model');
    }

    public function index($boq_id = '')
    {
        $this->list_boqs($boq_id);
    }

    public function list_boqs($boq_id = '')
    {
        close_setup_menu();

        if (!has_permission('boqs', '', 'view') && !has_permission('boqs', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('boqs');
        }

        $isPipeline = $this->session->userdata('boqs_pipeline') == 'true';

        if ($isPipeline && !$this->input->get('status')) {
            $data['title']           = _l('boqs_pipeline');
            $data['bodyclass']       = 'boqs-pipeline';
            $data['switch_pipeline'] = false;
            // Direct access
            if (is_numeric($boq_id)) {
                $data['boqid'] = $boq_id;
            } else {
                $data['boqid'] = $this->session->flashdata('boqid');
            }

            $this->load->view('admin/boqs/pipeline/manage', $data);
        } else {

            // Pipeline was initiated but user click from home page and need to show table only to filter
            if ($this->input->get('status') && $isPipeline) {
                $this->pipeline(0, true);
            }

            $data['boq_id']           = $boq_id;
            $data['switch_pipeline']       = true;
            $data['title']                 = _l('boqs');
            $data['statuses']              = $this->boqs_model->get_statuses();
            $data['boqs_sale_agents'] = $this->boqs_model->get_sale_agents();
            $data['years']                 = $this->boqs_model->get_boqs_years();
            $this->load->view('admin/boqs/manage', $data);
        }
    }

    public function table()
    {
        if (
            !has_permission('boqs', '', 'view')
            && !has_permission('boqs', '', 'view_own')
            && get_option('allow_staff_view_boqs_assigned') == 0
        ) {
            ajax_access_denied();
        }

        $this->app->get_table_data('boqs');
    }

    public function boq_relations($rel_id, $rel_type)
    {
        $this->app->get_table_data('boqs_relations', [
            'rel_id'   => $rel_id,
            'rel_type' => $rel_type,
        ]);
    }

    public function delete_attachment($id)
    {
        $file = $this->misc_model->get_file($id);
        if ($file->staffid == get_staff_user_id() || is_admin()) {
            echo $this->boqs_model->delete_attachment($id);
        } else {
            ajax_access_denied();
        }
    }

    public function clear_signature($id)
    {
        if (has_permission('boqs', '', 'delete')) {
            $this->boqs_model->clear_signature($id);
        }

        redirect(admin_url('boqs/list_boqs/' . $id));
    }

    public function sync_data()
    {
        if (has_permission('boqs', '', 'create') || has_permission('boqs', '', 'edit')) {
            $has_permission_view = has_permission('boqs', '', 'view');

            $this->db->where('rel_id', $this->input->post('rel_id'));
            $this->db->where('rel_type', $this->input->post('rel_type'));

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }

            $address = trim($this->input->post('address'));
            $address = nl2br($address);
            $this->db->update(db_prefix() . 'boqs', [
                'phone'   => $this->input->post('phone'),
                'zip'     => $this->input->post('zip'),
                'country' => $this->input->post('country'),
                'state'   => $this->input->post('state'),
                'address' => $address,
                'city'    => $this->input->post('city'),
            ]);

            if ($this->db->affected_rows() > 0) {
                echo json_encode([
                    'message' => _l('all_data_synced_successfully'),
                ]);
            } else {
                echo json_encode([
                    'message' => _l('sync_boqs_up_to_date'),
                ]);
            }
        }
    }

    public function boq($id = '')
    {
        if ($this->input->post()) {
            $boq_data = $this->input->post();
            if ($id == '') {
                if (!has_permission('boqs', '', 'create')) {
                    access_denied('boqs');
                }
                $id = $this->boqs_model->add($boq_data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('boq')));
                    if ($this->set_boq_pipeline_autoload($id)) {
                        redirect(admin_url('boqs'));
                    } else {
                        redirect(admin_url('boqs/list_boqs/' . $id));
                    }
                }
            } else {
                if (!has_permission('boqs', '', 'edit')) {
                    access_denied('boqs');
                }
                $success = $this->boqs_model->update($boq_data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('boq')));
                }
                if ($this->set_boq_pipeline_autoload($id)) {
                    redirect(admin_url('boqs'));
                } else {
                    redirect(admin_url('boqs/list_boqs/' . $id));
                }
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('boq_lowercase'));
        } else {
            $data['boq'] = $this->boqs_model->get($id);

            if (!$data['boq'] || !user_can_view_boq($id)) {
                blank_page(_l('boq_not_found'));
            }

            $data['estimate']    = $data['boq'];
            $data['is_boq'] = true;
            $title               = _l('edit', _l('boq_lowercase'));
        }

        $this->load->model('taxes_model');
        $data['taxes'] = $this->taxes_model->get();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows(db_prefix() . 'items') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['statuses']      = $this->boqs_model->get_statuses();
        $data['staff']         = $this->staff_model->get('', ['active' => 1]);
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();

        $data['title'] = $title;
        $this->load->view('admin/boqs/boq', $data);
    }

    public function get_template()
    {
        $name = $this->input->get('name');
        echo $this->load->view('admin/boqs/templates/' . $name, [], true);
    }

    public function send_expiry_reminder($id)
    {
        $canView = user_can_view_boq($id);
        if (!$canView) {
            access_denied('boqs');
        } else {
            if (!has_permission('boqs', '', 'view') && !has_permission('boqs', '', 'view_own') && $canView == false) {
                access_denied('boqs');
            }
        }

        $success = $this->boqs_model->send_expiry_reminder($id);
        if ($success) {
            set_alert('success', _l('sent_expiry_reminder_success'));
        } else {
            set_alert('danger', _l('sent_expiry_reminder_fail'));
        }
        if ($this->set_boq_pipeline_autoload($id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(admin_url('boqs/list_boqs/' . $id));
        }
    }

    public function clear_acceptance_info($id)
    {
        if (is_admin()) {
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'boqs', get_acceptance_info_array(true));
        }

        redirect(admin_url('boqs/list_boqs/' . $id));
    }

    public function pdf($id)
    {
        if (!$id) {
            redirect(admin_url('boqs'));
        }

        $canView = user_can_view_boq($id);
        if (!$canView) {
            access_denied('boqs');
        } else {
            if (!has_permission('boqs', '', 'view') && !has_permission('boqs', '', 'view_own') && $canView == false) {
                access_denied('boqs');
            }
        }

        $boq = $this->boqs_model->get($id);

        try {
            $pdf = boq_pdf($boq);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';

        if ($this->input->get('output_type')) {
            $type = $this->input->get('output_type');
        }

        if ($this->input->get('print')) {
            $type = 'I';
        }

        $boq_number = format_boq_number($id);
        $pdf->Output($boq_number . '.pdf', $type);
    }

    public function get_boq_data_ajax($id, $to_return = false)
    {
        if (!has_permission('boqs', '', 'view') && !has_permission('boqs', '', 'view_own') && get_option('allow_staff_view_boqs_assigned') == 0) {
            echo _l('access_denied');
            die;
        }

        $boq = $this->boqs_model->get($id, [], true);

        if (!$boq || !user_can_view_boq($id)) {
            echo _l('boq_not_found');
            die;
        }

        $this->app_mail_template->set_rel_id($boq->id);
        $data = prepare_mail_preview_data('boq_send_to_customer', $boq->email);

        $merge_fields = [];

        $merge_fields[] = [
            [
                'name' => 'Items Table',
                'key'  => '{boq_items}',
            ],
        ];

        $merge_fields = array_merge($merge_fields, $this->app_merge_fields->get_flat('boqs', 'other', '{email_signature}'));

        $data['boq_statuses']     = $this->boqs_model->get_statuses();
        $data['members']               = $this->staff_model->get('', ['active' => 1]);
        $data['boq_merge_fields'] = $merge_fields;
        $data['boq']              = $boq;
        $data['totalNotes']            = total_rows(db_prefix() . 'notes', ['rel_id' => $id, 'rel_type' => 'boq']);
        if ($to_return == false) {
            $this->load->view('admin/boqs/boqs_preview_template', $data);
        } else {
            return $this->load->view('admin/boqs/boqs_preview_template', $data, true);
        }
    }

    public function add_note($rel_id)
    {
        if ($this->input->post() && user_can_view_boq($rel_id)) {
            $this->misc_model->add_note($this->input->post(), 'boq', $rel_id);
            echo $rel_id;
        }
    }

    public function get_notes($id)
    {
        if (user_can_view_boq($id)) {
            $data['notes'] = $this->misc_model->get_notes($id, 'boq');
            $this->load->view('admin/includes/sales_notes_template', $data);
        }
    }

    public function convert_to_estimate($id)
    {
        if (!has_permission('estimates', '', 'create')) {
            access_denied('estimates');
        }
        if ($this->input->post()) {
            $this->load->model('estimates_model');
            $estimate_id = $this->estimates_model->add($this->input->post());
            if ($estimate_id) {
                set_alert('success', _l('boq_converted_to_estimate_success'));
                $this->db->where('id', $id);
                $this->db->update(db_prefix() . 'boqs', [
                    'estimate_id' => $estimate_id,
                    'status'      => 3,
                ]);
                log_activity('boq Converted to Estimate [EstimateID: ' . $estimate_id . ', boqID: ' . $id . ']');

                hooks()->do_action('boq_converted_to_estimate', ['boq_id' => $id, 'estimate_id' => $estimate_id]);

                redirect(admin_url('estimates/estimate/' . $estimate_id));
            } else {
                set_alert('danger', _l('boq_converted_to_estimate_fail'));
            }
            if ($this->set_boq_pipeline_autoload($id)) {
                redirect(admin_url('boqs'));
            } else {
                redirect(admin_url('boqs/list_boqs/' . $id));
            }
        }
    }

    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('invoices');
        }
        if ($this->input->post()) {
            $this->load->model('invoices_model');
            $invoice_id = $this->invoices_model->add($this->input->post());
            if ($invoice_id) {
                set_alert('success', _l('boq_converted_to_invoice_success'));
                $this->db->where('id', $id);
                $this->db->update(db_prefix() . 'boqs', [
                    'invoice_id' => $invoice_id,
                    'status'     => 3,
                ]);
                log_activity('boq Converted to Invoice [InvoiceID: ' . $invoice_id . ', boqID: ' . $id . ']');
                hooks()->do_action('boq_converted_to_invoice', ['boq_id' => $id, 'invoice_id' => $invoice_id]);
                redirect(admin_url('invoices/invoice/' . $invoice_id));
            } else {
                set_alert('danger', _l('boq_converted_to_invoice_fail'));
            }
            if ($this->set_boq_pipeline_autoload($id)) {
                redirect(admin_url('boqs'));
            } else {
                redirect(admin_url('boqs/list_boqs/' . $id));
            }
        }
    }

    public function get_invoice_convert_data($id)
    {
        $this->load->model('payment_modes_model');
        $data['payment_modes'] = $this->payment_modes_model->get('', [
            'expenses_only !=' => 1,
        ]);
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows(db_prefix() . 'items') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']          = $this->staff_model->get('', ['active' => 1]);
        $data['boq']       = $this->boqs_model->get($id);
        $data['billable_tasks'] = [];
        $data['add_items']      = $this->_parse_items($data['boq']);

        if ($data['boq']->rel_type == 'lead') {
            $this->db->where('leadid', $data['boq']->rel_id);
            $data['customer_id'] = $this->db->get(db_prefix() . 'clients')->row()->userid;
        } else {
            $data['customer_id'] = $data['boq']->rel_id;
        }
        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'boq',
            'rel_id'     => $id,
        ];
        $this->load->view('admin/boqs/invoice_convert_template', $data);
    }

    public function get_estimate_convert_data($id)
    {
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows(db_prefix() . 'items') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['staff']     = $this->staff_model->get('', ['active' => 1]);
        $data['boq']  = $this->boqs_model->get($id);
        $data['add_items'] = $this->_parse_items($data['boq']);

        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();
        if ($data['boq']->rel_type == 'lead') {
            $this->db->where('leadid', $data['boq']->rel_id);
            $data['customer_id'] = $this->db->get(db_prefix() . 'clients')->row()->userid;
        } else {
            $data['customer_id'] = $data['boq']->rel_id;
        }

        $data['custom_fields_rel_transfer'] = [
            'belongs_to' => 'boq',
            'rel_id'     => $id,
        ];

        $this->load->view('admin/boqs/estimate_convert_template', $data);
    }

    private function _parse_items($boq)
    {
        $items = [];
        foreach ($boq->items as $item) {
            $taxnames = [];
            $taxes    = get_boq_item_taxes($item['id']);
            foreach ($taxes as $tax) {
                array_push($taxnames, $tax['taxname']);
            }
            $item['taxname']        = $taxnames;
            $item['parent_item_id'] = $item['id'];
            $item['id']             = 0;
            $items[]                = $item;
        }

        return $items;
    }

    /* Send boq to email */
    public function send_to_email($id)
    {
        $canView = user_can_view_boq($id);
        if (!$canView) {
            access_denied('boqs');
        } else {
            if (!has_permission('boqs', '', 'view') && !has_permission('boqs', '', 'view_own') && $canView == false) {
                access_denied('boqs');
            }
        }

        if ($this->input->post()) {
            try {
                $success = $this->boqs_model->send_boq_to_email(
                    $id,
                    $this->input->post('attach_pdf'),
                    $this->input->post('cc')
                );
            } catch (Exception $e) {
                $message = $e->getMessage();
                echo $message;
                if (strpos($message, 'Unable to get the size of the image') !== false) {
                    show_pdf_unable_to_get_image_size_error();
                }
                die;
            }

            if ($success) {
                set_alert('success', _l('boq_sent_to_email_success'));
            } else {
                set_alert('danger', _l('boq_sent_to_email_fail'));
            }

            if ($this->set_boq_pipeline_autoload($id)) {
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(admin_url('boqs/list_boqs/' . $id));
            }
        }
    }

    public function copy($id)
    {
        if (!has_permission('boqs', '', 'create')) {
            access_denied('boqs');
        }
        $new_id = $this->boqs_model->copy($id);
        if ($new_id) {
            set_alert('success', _l('boq_copy_success'));
            $this->set_boq_pipeline_autoload($new_id);
            redirect(admin_url('boqs/boq/' . $new_id));
        } else {
            set_alert('success', _l('boq_copy_fail'));
        }
        if ($this->set_boq_pipeline_autoload($id)) {
            redirect(admin_url('boqs'));
        } else {
            redirect(admin_url('boqs/list_boqs/' . $id));
        }
    }

    public function mark_action_status($status, $id)
    {
        if (!has_permission('boqs', '', 'edit')) {
            access_denied('boqs');
        }
        $success = $this->boqs_model->mark_action_status($status, $id);
        if ($success) {
            set_alert('success', _l('boq_status_changed_success'));
        } else {
            set_alert('danger', _l('boq_status_changed_fail'));
        }
        if ($this->set_boq_pipeline_autoload($id)) {
            redirect(admin_url('boqs'));
        } else {
            redirect(admin_url('boqs/list_boqs/' . $id));
        }
    }

    public function delete($id)
    {
        if (!has_permission('boqs', '', 'delete')) {
            access_denied('boqs');
        }
        $response = $this->boqs_model->delete($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('boq')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('boq_lowercase')));
        }
        redirect(admin_url('boqs'));
    }

    public function get_relation_data_values($rel_id, $rel_type)
    {
        echo json_encode($this->boqs_model->get_relation_data_values($rel_id, $rel_type));
    }

    public function add_boq_comment()
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->boqs_model->add_comment($this->input->post()),
            ]);
        }
    }

    public function edit_comment($id)
    {
        if ($this->input->post()) {
            echo json_encode([
                'success' => $this->boqs_model->edit_comment($this->input->post(), $id),
                'message' => _l('comment_updated_successfully'),
            ]);
        }
    }

    public function get_boq_comments($id)
    {
        $data['comments'] = $this->boqs_model->get_comments($id);
        $this->load->view('admin/boqs/comments_template', $data);
    }

    public function remove_comment($id)
    {
        $this->db->where('id', $id);
        $comment = $this->db->get(db_prefix() . 'boq_comments')->row();
        if ($comment) {
            if ($comment->staffid != get_staff_user_id() && !is_admin()) {
                echo json_encode([
                    'success' => false,
                ]);
                die;
            }
            echo json_encode([
                'success' => $this->boqs_model->remove_comment($id),
            ]);
        } else {
            echo json_encode([
                'success' => false,
            ]);
        }
    }

    public function save_boq_data()
    {
        if (!has_permission('boqs', '', 'edit') && !has_permission('boqs', '', 'create')) {
            header('HTTP/1.0 400 Bad error');
            echo json_encode([
                'success' => false,
                'message' => _l('access_denied'),
            ]);
            die;
        }
        $success = false;
        $message = '';

        $this->db->where('id', $this->input->post('boq_id'));
        $this->db->update(db_prefix() . 'boqs', [
            'content' => html_purify($this->input->post('content', false)),
        ]);

        $success = $this->db->affected_rows() > 0;
        $message = _l('updated_successfully', _l('boq'));

        echo json_encode([
            'success' => $success,
            'message' => $message,
        ]);
    }

    // Pipeline
    public function pipeline($set = 0, $manual = false)
    {
        if ($set == 1) {
            $set = 'true';
        } else {
            $set = 'false';
        }
        $this->session->set_userdata([
            'boqs_pipeline' => $set,
        ]);
        if ($manual == false) {
            redirect(admin_url('boqs'));
        }
    }

    public function pipeline_open($id)
    {
        if (has_permission('boqs', '', 'view') || has_permission('boqs', '', 'view_own') || get_option('allow_staff_view_boqs_assigned') == 1) {
            $data['boq']      = $this->get_boq_data_ajax($id, true);
            $data['boq_data'] = $this->boqs_model->get($id);
            $this->load->view('admin/boqs/pipeline/boq', $data);
        }
    }

    public function update_pipeline()
    {
        if (has_permission('boqs', '', 'edit')) {
            $this->boqs_model->update_pipeline($this->input->post());
        }
    }

    public function get_pipeline()
    {
        if (has_permission('boqs', '', 'view') || has_permission('boqs', '', 'view_own') || get_option('allow_staff_view_boqs_assigned') == 1) {
            $data['statuses'] = $this->boqs_model->get_statuses();
            $this->load->view('admin/boqs/pipeline/pipeline', $data);
        }
    }

    public function pipeline_load_more()
    {
        $status = $this->input->get('status');
        $page   = $this->input->get('page');

        $boqs = $this->boqs_model->do_kanban_query($status, $this->input->get('search'), $page, [
            'sort_by' => $this->input->get('sort_by'),
            'sort'    => $this->input->get('sort'),
        ]);

        foreach ($boqs as $boq) {
            $this->load->view('admin/boqs/pipeline/_kanban_card', [
                'boq' => $boq,
                'status'   => $status,
            ]);
        }
    }

    public function set_boq_pipeline_autoload($id)
    {
        if ($id == '') {
            return false;
        }

        if ($this->session->has_userdata('boqs_pipeline') && $this->session->userdata('boqs_pipeline') == 'true') {
            $this->session->set_flashdata('boqid', $id);

            return true;
        }

        return false;
    }

    public function get_due_date()
    {
        if ($this->input->post()) {
            $date    = $this->input->post('date');
            $duedate = '';
            if (get_option('boq_due_after') != 0) {
                $date    = to_sql_date($date);
                $d       = date('Y-m-d', strtotime('+' . get_option('boq_due_after') . ' DAY', strtotime($date)));
                $duedate = _d($d);
                echo $duedate;
            }
        }
    }
}
