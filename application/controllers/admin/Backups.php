<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Backups extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_backups_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        $this->load->model('backups_model');
    }

    public function index()
    {
        if (!has_permission('backups', '', 'view')) {
            access_denied('backups');
        }
    	$data['backups_lists'] = $this->backups_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->get()->result_array();
        //print_r($data);die();
        $data['title']      = 'backup';
    	$this->load->view('admin/backups/manage', $data);

    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['backups_lists'] = '';
            $customerData = $this->input->post();
            $data['backups_lists'] = $this->backups_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id','left')
                ->get()->result_array();
                // print_r($data);die();
            $this->load->view('admin/backups/filter_table', $data);
        }
    }

    public function backup($id = '')
    {
        if (!has_permission('backups', '', 'view')) {
            access_denied('backups');
        }
    	if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('backups', '', 'create')) {
                    access_denied('backups');
                }
                $data = $this->input->post();
                unset($data['server_ids']);
                unset($data['first_backup']);
                unset($data['second_backup']);
                unset($data['third_backup']);
                $data['server'] = implode(',',$data['server']);
                $id = $this->backups_model->add($data);
		        if ($id) {
		            set_alert('success', _l('added_successfully', _l('backup policy')));
		            return redirect(admin_url('backups'));
		        }
            }
            else 
            {  
                if (!has_permission('backups', '', 'edit')) {
                    access_denied('backups');
                }
                $data = $this->input->post();
                unset($data['server_ids']);
                unset($data['first_backup']);
                unset($data['second_backup']);
                unset($data['third_backup']);
                $data['server'] = implode(',',$data['server']);
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'backups', $data);
		        if ($success) {
		            set_alert('success', _l('updated_successfully', _l('backup policy')));
		        }
		        return redirect(admin_url('backups'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('backup'));
        } else {
            $data['backups_details'] = $this->db->where('id',$id)->get(db_prefix().'backups')->row();
            $title = _l('edit', _l('backup'));
        }

        $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('backup_required',1)->get()->result_array();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['bodyclass']  = 'backups';
        $data['title']      = $title;
        // print_r($data);die();
        $this->load->view('admin/backups/backup', $data);
    }

    public function backup_log_data($id)
    {
        if($id)
        {   
            $data['logs'] = $this->db->select('tblbackup_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
            ->from(db_prefix() . 'backup_logs')
            ->join(db_prefix() . 'backups','tblbackup_logs.backup_id = tblbackups.id')
            ->join(db_prefix() . 'inventory_details','tblbackup_logs.server = tblinventory_details.id')
            ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
            ->where('backup_id', $id)->get()->result_array();

            $log_table = "<table class='table table-striped table-bordered'>";
            $log_table .= "<thead><th>Sr.No.</th><th>Date</th><th>Inventory Product</th><th>Status</th><th>Remark</th></thead>";
            $log_table .= "<tbody>";
            foreach($data['logs'] as $key => $log){
                $log_table .= "<tr><td>";
                $log_table .= $key+1;
                $log_table .= "</td><td>".date('d-m-Y',strtotime($log['backup_date']));
                $log_table .= "</td><td>".$log['product_name']." (".$log['hostname']." | ".$log['purpose'].")";
                $log_table .= "</td><td>".$log['status'];
                $log_table .= "</td><td>".$log['remark'];
                $log_table .= "</td></tr>";
            }
            $log_table .= "</tbody></table>";

            echo $log_table;
            //print_r($data['logs']);
        }
    }

    public function get_backup_inventories($id)
    {
        if($id)
        {   
            $data['server'] = $this->db->select('server')->from('tblbackups')->where('id', $id)->get()->row();
            $servers = explode(',',$data['server']->server);
            
            $log_table = "<table class='table table-striped table-bordered'>";
            $log_table .= "<thead><th>Sr.No.</th><th>Inventory Product</th><th>Status</th><th>Remark</th></thead>";
            $log_table .= "<tbody>";

            foreach($servers as $key=>$server){
                $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('tblinventory_details.id', $server)->get()->row();

                $log_table .= "<tr><td>";
                $log_table .= $key+1;
                $log_table .= "</td><td>".$data['inventory']->product_name." (".$data['inventory']->hostname."|".$data['inventory']->purpose.") <input type='hidden' name='server".$key."' id='server".$key."' value='".$data['inventory']->id."'>";
                $log_table .= "</td><td><div class='form-group'>
                <select class='' name='status".$key."' id='status".$key."' data-width='100%'>
                    <option value='' selected=''> Select </option>
                    <option value='Success'>Success</option>
                    <option value='Failure'>Failure</option>
                    <option value='Warning'>Warning</option>
                </select></div>";
                $log_table .= "</td><td><input class='form-group' type='text' name='remark".$key."' id='remark".$key."' value=''>";
                $log_table .= "</td></tr>";
            }
            $log_table .= "</tbody></table><input type='hidden' name='server_cnt' id='server_cnt' value='".count($servers)."'>";

            echo $log_table;
        }
    }

    public function get_inventories($id)
    {
        if($id)
        {   
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose,tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('clientid', $id)->get()->result_array();

            echo json_encode($data['inventory']);
        }
    }

    public function get_client_inventories($id)
    {
        if($id)
        {   
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('backup_required',1)->where('clientid', $id)->get()->result_array();

            echo json_encode($data['inventory']);
        }
    }

    public function client_backup_policy($id)
    {
        if($id)
        {   
            $data['policies'] = $this->db->select('id, policy_title')
            ->from(db_prefix() . 'backups')
            ->where('client_id', $id)->get()->result_array();

            echo json_encode($data['policies']);
        }
    }

    public function backup_logs()
    {
        if (!has_permission('backups', '', 'view')) {
            access_denied('backups');
        }
        $data['backup_logs'] = $this->backups_model->get_log();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['backups'] = $this->db->select('tblbackups.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                           ->from(db_prefix() . 'backups')
                           ->join(db_prefix() . 'inventory_details','tblbackups.server = tblinventory_details.id')
                           ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                           ->get()->result_array();
        $data['title'] = _l('Backup Logs');
        //print_r($data);die();
        $this->load->view('admin/backups/backup_logs/manage', $data);
    }



    //Add Provisional log Of backups
    
    public function log()
    {
        if (!has_permission('backups', '', 'view')) {
            access_denied('backups');
        }
    	if($this->input->post()){
            $data = $this->input->post();
            $new_data = array();
            unset($data['client_id']);
            if(empty($data['id'])){
                if (!has_permission('backups', '', 'create')) {
                    access_denied('backups');
                }

                for($i=0; $i<$data['server_cnt']; $i++){
                    $new_data['backup_id'] = $data['backup_id'];
                    $new_data['server'] = $data['server'.$i];
                    $new_data['backup_date'] = $data['backup_date'];
                    $new_data['status'] = $data['status'.$i];
                    $new_data['remark'] = $data['remark'.$i];
                    $id = $this->db->insert('tblbackup_logs',$new_data);
                }
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('backup log')));    //return redirect(
                }
            }
            else{
                if (!has_permission('backups', '', 'edit')) {
                    access_denied('backups');
                }
                unset($data['policy']);
                unset($data['inventory_product']);
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'backup_logs',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('backup log')));    //return redirect(
                }
            } 
            redirect(admin_url('backups/backup_logs'));      
        } 
    }


    public function delete($id)
    {
        if (!has_permission('backups', '', 'delete')) {
            access_denied('backups');
        }
        if (!$id) {
            redirect(admin_url('backups'));
        }
        $response = $this->db->where('id',$id)->delete(db_prefix().'backups');
        if ($response === true) 
        {
            set_alert('success', _l('deleted', _l('backups')));
            redirect(admin_url('backups'));
        } else 
        {  
            set_alert('warning', _l('problem_deleting', _l('backups')));
            redirect(admin_url('backups'));
        }
    }
}

