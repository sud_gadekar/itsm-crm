<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Reports extends AdminController
{
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

   
   

    public function __construct()
    {
       
        
        parent::__construct();
        if (!has_permission('reports', '', 'view')) {
            access_denied('reports');
        }
        $this->ci = &get_instance();
        $this->load->model('reports_model');
    }

    /* No access on this url */
    public function index()
    {
        redirect(admin_url());
    }

    /* See knowledge base article reports*/
    public function knowledge_base_articles()
    {
        $this->load->model('knowledge_base_model');
        $data['groups'] = $this->knowledge_base_model->get_kbg();
        $data['title']  = _l('kb_reports');
        $this->load->view('admin/reports/knowledge_base_articles', $data);
    }

    /*
        public function tax_summary(){
           $this->load->model('taxes_model');
           $this->load->model('payments_model');
           $this->load->model('invoices_model');
           $data['taxes'] = $this->db->query("SELECT DISTINCT taxname,taxrate FROM ".db_prefix()."item_tax WHERE rel_type='invoice'")->result_array();
            $this->load->view('admin/reports/tax_summary',$data);
        }*/
    /* Repoert leads conversions */
    public function leads()
    {
        $type = 'leads';
        if ($this->input->get('type')) {
            $type                       = $type . '_' . $this->input->get('type');
            $data['leads_staff_report'] = json_encode($this->reports_model->leads_staff_report());
            $data['sales_staff_report'] = json_encode($this->reports_model->sales_staff_report());
        }
        $this->load->model('leads_model');
        $data['statuses']               = $this->leads_model->get_status();
        $data['leads_this_week_report'] = json_encode($this->reports_model->leads_this_week_report());
        $data['leads_sources_report']   = json_encode($this->reports_model->leads_sources_report());
        $this->load->view('admin/reports/' . $type, $data);
    }

    /* Sales reportts */
    public function sales()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode']      = $this->db->query('SELECT @@sql_mode as mode')->row();

        if (is_using_multiple_currencies() || is_using_multiple_currencies(db_prefix() . 'creditnotes') || is_using_multiple_currencies(db_prefix() . 'estimates') || is_using_multiple_currencies(db_prefix() . 'proposals')) {
            $this->load->model('currencies_model');
            $data['currencies'] = $this->currencies_model->get();
        }
        $this->load->model('invoices_model');
        $this->load->model('gaps_model');
        $this->load->model('tickets_model');
        $this->load->model('tasks_model');
        $this->load->model('service_requests_model');
        $this->load->model('estimates_model');
        $this->load->model('proposals_model');
        $this->load->model('credit_notes_model');
       

        $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
        $data['invoice_statuses']      = $this->invoices_model->get_statuses();

        $data['incident_statuses']        = $this->tickets_model->get_ticket_status();
        $data['service_request_statuses'] = $this->service_requests_model->get_service_request_status();
        $data['task_statuses']            = $this->tasks_model->get_statuses();

        $data['inventory_types']          = $this->db->where('parent_id =',0)->get('tblinventory_types')->result_array();

        $data['gap_statuses']             = $this->gaps_model->get_gap_status();

        $data['clients']                  = $this->db->get('tblclients')->result_array();   

        $data['staffs']                   = $this->db>where('active =',1)->get('tblstaff')->result_array();   

        $data['estimate_statuses']     = $this->estimates_model->get_statuses();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();
        $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();

        $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();

        $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
        $data['proposals_statuses']    = $this->proposals_model->get_statuses();

        $data['invoice_taxes']     = $this->distinct_taxes('invoice');
        $data['estimate_taxes']    = $this->distinct_taxes('estimate');
        $data['proposal_taxes']    = $this->distinct_taxes('proposal');
        $data['credit_note_taxes'] = $this->distinct_taxes('credit_note');



        //$tickets_awaiting_reply_by_status     = $this->dashboard_model->tickets_awaiting_reply_by_status();
        //$tickets_awaiting_reply_by_department = $this->dashboard_model->tickets_awaiting_reply_by_department();

        // $data['tickets_reply_by_status']              = json_encode($tickets_awaiting_reply_by_status);
        // $data['tickets_awaiting_reply_by_department'] = json_encode($tickets_awaiting_reply_by_department);

        // $data['tickets_reply_by_status_no_json']              = $tickets_awaiting_reply_by_status;
        // $data['tickets_awaiting_reply_by_department_no_json'] = $tickets_awaiting_reply_by_department;

        // $tickets_by_status = $this->dashboard_model->tickets_by_status();

        // $data['tickets_reply_by_status']              = json_encode($tickets_by_status);

        // $data['tickets_status_no_json']              = $tickets_by_status;

        //print_r($data);die();
        $data['title'] = _l('sales_reports');
        $this->load->view('admin/reports/sales', $data);
    }

    public function statuses()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode']      = $this->db->query('SELECT @@sql_mode as mode')->row();

        if (is_using_multiple_currencies() || is_using_multiple_currencies(db_prefix() . 'creditnotes') || is_using_multiple_currencies(db_prefix() . 'estimates') || is_using_multiple_currencies(db_prefix() . 'proposals')) {
            $this->load->model('currencies_model');
            $data['currencies'] = $this->currencies_model->get();
        }
        $this->load->model('invoices_model');
        $this->load->model('gaps_model');
        $this->load->model('tickets_model');
        $this->load->model('tasks_model');
        $this->load->model('service_requests_model');
        $this->load->model('estimates_model');
        $this->load->model('proposals_model');
        $this->load->model('credit_notes_model');
         $this->load->model('dashboard_model');

        $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
        $data['invoice_statuses']      = $this->invoices_model->get_statuses();

        $data['incident_statuses']        = $this->tickets_model->get_ticket_status();
        $data['service_request_statuses'] = $this->service_requests_model->get_service_request_status();
        $data['task_statuses']            = $this->tasks_model->get_statuses();

        $data['inventory_types']          = $this->db->where('parent_id =',0)
                                                      ->get('tblinventory_types')->result_array();

        $data['gap_statuses']             = $this->gaps_model->get_gap_status();

        $data['clients']                  = $this->db->get('tblclients')->result_array();     

        $data['staffs']                   = $this->db->get('tblstaff')->result_array();      

        $data['request_for']                   = $this->db->get('tbltickets_request_for')->result_array();      

        $data['services']                   = $this->db->get('tblservices')->result_array();  

        $data['estimate_statuses']     = $this->estimates_model->get_statuses();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();
        $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();

        $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();

        $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
        $data['proposals_statuses']    = $this->proposals_model->get_statuses();

        $data['invoice_taxes']     = $this->distinct_taxes('invoice');
        $data['estimate_taxes']    = $this->distinct_taxes('estimate');
        $data['proposal_taxes']    = $this->distinct_taxes('proposal');
        $data['credit_note_taxes'] = $this->distinct_taxes('credit_note');


    

        $tickets_by_status = $this->dashboard_model->tickets_by_status();

        $data['tickets_reply_by_status']              = json_encode($tickets_by_status);

        $data['tickets_status_no_json']              = $tickets_by_status;




        //print_r($data);die();
        $data['title'] = _l('sales_reports');
        $this->load->view('admin/reports/statuses', $data);
    }

    public function types()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode']      = $this->db->query('SELECT @@sql_mode as mode')->row();

        if (is_using_multiple_currencies() || is_using_multiple_currencies(db_prefix() . 'creditnotes') || is_using_multiple_currencies(db_prefix() . 'estimates') || is_using_multiple_currencies(db_prefix() . 'proposals')) {
            $this->load->model('currencies_model');
            $data['currencies'] = $this->currencies_model->get();
        }
        $this->load->model('invoices_model');
        $this->load->model('gaps_model');
        $this->load->model('tickets_model');
        $this->load->model('tasks_model');
        $this->load->model('service_requests_model');
        $this->load->model('estimates_model');
        $this->load->model('proposals_model');
        $this->load->model('credit_notes_model');
         $this->load->model('dashboard_model');

        $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
        $data['invoice_statuses']      = $this->invoices_model->get_statuses();

        $data['incident_statuses']        = $this->tickets_model->get_ticket_status();
        $data['service_request_statuses'] = $this->service_requests_model->get_service_request_status();
        $data['task_statuses']            = $this->tasks_model->get_statuses();

        $data['inventory_types']          = $this->db->where('parent_id =',0)
                                                      ->get('tblinventory_types')->result_array();

        $data['gap_statuses']             = $this->gaps_model->get_gap_status();

        $data['clients']                  = $this->db->get('tblclients')->result_array();     

        $data['staffs']                   = $this->db->get('tblstaff')->result_array();  

        $data['estimate_statuses']     = $this->estimates_model->get_statuses();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();
        $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();

        $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();

        $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
        $data['proposals_statuses']    = $this->proposals_model->get_statuses();

        $data['invoice_taxes']     = $this->distinct_taxes('invoice');
        $data['estimate_taxes']    = $this->distinct_taxes('estimate');
        $data['proposal_taxes']    = $this->distinct_taxes('proposal');
        $data['credit_note_taxes'] = $this->distinct_taxes('credit_note');

        $tickets_by_status = $this->dashboard_model->tickets_by_status();

        $data['tickets_reply_by_status']              = json_encode($tickets_by_status);

        $data['tickets_status_no_json']              = $tickets_by_status;

        //print_r($data);die();
        $data['title'] = _l('sales_reports');
        $this->load->view('admin/reports/types', $data);
    }

    /* Customer report */
    public function customers_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $select = [
                get_sql_select_client_company(),
                '(SELECT COUNT(clientid) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
                '(SELECT SUM(subtotal) - SUM(discount_total) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
                '(SELECT SUM(total) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
            ];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' ' . $custom_date_select . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
            }
            $by_currency = $this->input->post('report_currency');
            $currency    = $this->currencies_model->get_base_currency();
            if ($by_currency) {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' AND currency =' . $this->db->escape_str($by_currency) . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
                $currency = $this->currencies_model->get($by_currency);
            }
            $aColumns     = $select;
            $sIndexColumn = 'userid';
            $sTable       = db_prefix() . 'clients';
            $where        = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, [
                'userid',
            ]);
            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($i == 0) {
                        $_data = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } elseif ($aColumns[$i] == $select[2] || $aColumns[$i] == $select[3]) {
                        if ($_data == null) {
                            $_data = 0;
                        }
                        $_data = app_format_money($_data, $currency->name);
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
                $x++;
            }
            echo json_encode($output);
            die();
        }
    }

    public function get_primary_contact_mails()
    {
        if($this->input->post())
        {
            $primary_contact_mails = $this->db->where('userid', $this->input->post('clientid'))
                                              ->where('is_primary', 1)
                                              ->select('email')
                                              ->get('tblcontacts')->result_array();

            print_r(json_encode($primary_contact_mails));
        }
    }

    public function payments_received()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('payment_modes_model');
            $payment_gateways = $this->payment_modes_model->get_payment_gateways(true);
            $select           = [
                db_prefix() . 'invoicepaymentrecords.id',
                db_prefix() . 'invoicepaymentrecords.date',
                'invoiceid',
                get_sql_select_client_company(),
                'paymentmode',
                'transactionid',
                'note',
                'amount',
            ];
            $where = [
                'AND status != 5',
            ];

            $custom_date_select = $this->get_where_report_period(db_prefix() . 'invoicepaymentrecords.date');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'invoicepaymentrecords';
            $join         = [
                'JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'invoicepaymentrecords.invoiceid',
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'invoices.clientid',
                'LEFT JOIN ' . db_prefix() . 'payment_modes ON ' . db_prefix() . 'payment_modes.id = ' . db_prefix() . 'invoicepaymentrecords.paymentmode',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'number',
                'clientid',
                db_prefix() . 'payment_modes.name',
                db_prefix() . 'payment_modes.id as paymentmodeid',
                'paymentmethod',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data['total_amount'] = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($aColumns[$i] == 'paymentmode') {
                        $_data = $aRow['name'];
                        if (is_null($aRow['paymentmodeid'])) {
                            foreach ($payment_gateways as $gateway) {
                                if ($aRow['paymentmode'] == $gateway['id']) {
                                    $_data = $gateway['name'];
                                }
                            }
                        }
                        if (!empty($aRow['paymentmethod'])) {
                            $_data .= ' - ' . $aRow['paymentmethod'];
                        }
                    } elseif ($aColumns[$i] == db_prefix() . 'invoicepaymentrecords.id') {
                        $_data = '<a href="' . admin_url('payments/payment/' . $_data) . '" target="_blank">' . $_data . '</a>';
                    } elseif ($aColumns[$i] == db_prefix() . 'invoicepaymentrecords.date') {
                        $_data = _d($_data);
                    } elseif ($aColumns[$i] == 'invoiceid') {
                        $_data = '<a href="' . admin_url('invoices/list_invoices/' . $aRow[$aColumns[$i]]) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                    } elseif ($i == 3) {
                        if (empty($aRow['deleted_customer_name'])) {
                            $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                        } else {
                            $row[] = $aRow['deleted_customer_name'];
                        }
                    } elseif ($aColumns[$i] == 'amount') {
                        $footer_data['total_amount'] += $_data;
                        $_data = app_format_money($_data, $currency->name);
                    }

                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = app_format_money($footer_data['total_amount'], $currency->name);
            $output['sums']              = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function proposals_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('proposals_model');

            $proposalsTaxes    = $this->distinct_taxes('proposal');
            $totalTaxesColumns = count($proposalsTaxes);

            $select = [
                'id',
                'subject',
                'proposal_to',
                'date',
                'open_till',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'status',
            ];

            $proposalsTaxesSelect = array_reverse($proposalsTaxes);

            foreach ($proposalsTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="proposal" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'proposals.id) as total_tax_single_' . $key);
            }

            $where              = [];
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('proposal_status')) {
                $statuses  = $this->input->post('proposal_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('proposals_sale_agents')) {
                $agents  = $this->input->post('proposals_sale_agents');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND assigned IN (' . implode(', ', $_agents) . ')');
                }
            }


            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'proposals';
            $join         = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'rel_id',
                'rel_type',
                'discount_percent',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($proposalsTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . format_proposal_number($aRow['id']) . '</a>';

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . $aRow['subject'] . '</a>';

                if ($aRow['rel_type'] == 'lead') {
                    $row[] = '<a href="#" onclick="init_lead(' . $aRow['rel_id'] . ');return false;" target="_blank" data-toggle="tooltip" data-title="' . _l('lead') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('lead') . '</span>';
                } elseif ($aRow['rel_type'] == 'customer') {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('client') . '</span>';
                } else {
                    $row[] = '';
                }

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['open_till']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($proposalsTaxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[]              = format_proposal_status($aRow['status']);
                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function estimates_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('estimates_model');

            $estimateTaxes     = $this->distinct_taxes('estimate');
            $totalTaxesColumns = count($estimateTaxes);

            $select = [
                'number',
                get_sql_select_client_company(),
                'invoiceid',
                'YEAR(date) as year',
                'date',
                'expirydate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'reference_no',
                'status',
            ];

            $estimatesTaxesSelect = array_reverse($estimateTaxes);

            foreach ($estimatesTaxesSelect as $key => $tax) {
                array_splice($select, 9, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="estimate" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'estimates.id) as total_tax_single_' . $key);
            }

            $where              = [];
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('estimate_status')) {
                $statuses  = $this->input->post('estimate_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('sale_agent_estimates')) {
                $agents  = $this->input->post('sale_agent_estimates');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'estimates';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'estimates.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'estimates.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($estimateTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('estimates/list_estimates/' . $aRow['id']) . '" target="_blank">' . format_estimate_number($aRow['id']) . '</a>';

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                if ($aRow['invoiceid'] === null) {
                    $row[] = '';
                } else {
                    $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoiceid']) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['expirydate']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($estimateTaxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];


                $row[] = $aRow['reference_no'];

                $row[] = format_estimate_status($aRow['status']);

                $output['aaData'][] = $row;
            }
            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    private function get_where_report_period($field = 'date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
                if ($months_report == '1') {
                    $beginMonth = date('Y-m-01', strtotime('first day of last month'));
                    $endMonth   = date('Y-m-t', strtotime('last day of last month'));
                } else {
                    $months_report = (int) $months_report;
                    $months_report--;
                    $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                    $endMonth   = date('Y-m-t');
                }

                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif ($months_report == 'this_month') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif ($months_report == 'this_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date('Y-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date('Y-12-31'))) . '")';
            } elseif ($months_report == 'last_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $this->db->escape_str($from_date) . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $this->db->escape_str($from_date) . '" AND "' . $this->db->escape_str($to_date) . '")';
                }
            }
        }

        return $custom_date_select;
    }

    public function items()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $v = $this->db->query('SELECT VERSION() as version')->row();
            // 5.6 mysql version don't have the ANY_VALUE function implemented.

            if ($v && strpos($v->version, '5.7') !== false) {
                $aColumns = [
                        'ANY_VALUE(description) as description',
                        'ANY_VALUE((SUM(' . db_prefix() . 'itemable.qty))) as quantity_sold',
                        'ANY_VALUE(SUM(rate*qty)) as rate',
                        'ANY_VALUE(AVG(rate*qty)) as avg_price',
                    ];
            } else {
                $aColumns = [
                        'description as description',
                        '(SUM(' . db_prefix() . 'itemable.qty)) as quantity_sold',
                        'SUM(rate*qty) as rate',
                        'AVG(rate*qty) as avg_price',
                    ];
            }

            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'itemable';
            $join         = ['JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'itemable.rel_id'];

            $where = ['AND rel_type="invoice"', 'AND status != 5', 'AND status=2'];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }
            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('sale_agent_items')) {
                $agents  = $this->input->post('sale_agent_items');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [], 'GROUP by description');

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total_amount' => 0,
                'total_qty'    => 0,
            ];

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = $aRow['description'];
                $row[] = $aRow['quantity_sold'];
                $row[] = app_format_money($aRow['rate'], $currency->name);
                $row[] = app_format_money($aRow['avg_price'], $currency->name);
                $footer_data['total_amount'] += $aRow['rate'];
                $footer_data['total_qty'] += $aRow['quantity_sold'];
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = app_format_money($footer_data['total_amount'], $currency->name);

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function credit_notes()
    {
        if ($this->input->is_ajax_request()) {
            $credit_note_taxes = $this->distinct_taxes('credit_note');
            $totalTaxesColumns = count($credit_note_taxes);

            $this->load->model('currencies_model');

            $select = [
                'number',
                'date',
                get_sql_select_client_company(),
                'reference_no',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT ' . db_prefix() . 'creditnotes.total - (
                  (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.credit_id=' . db_prefix() . 'creditnotes.id)
                  +
                  (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'creditnote_refunds WHERE ' . db_prefix() . 'creditnote_refunds.credit_note_id=' . db_prefix() . 'creditnotes.id)
                  )
                ) as remaining_amount',
                'status',
            ];

            $where = [];

            $credit_note_taxes_select = array_reverse($credit_note_taxes);

            foreach ($credit_note_taxes_select as $key => $tax) {
                array_splice($select, 5, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="credit_note" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'creditnotes.id) as total_tax_single_' . $key);
            }

            $custom_date_select = $this->get_where_report_period();

            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');

            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('credit_note_status')) {
                $statuses  = $this->input->post('credit_note_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'creditnotes';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'creditnotes.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'creditnotes.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'            => 0,
                'subtotal'         => 0,
                'total_tax'        => 0,
                'discount_total'   => 0,
                'adjustment'       => 0,
                'remaining_amount' => 0,
            ];

            foreach ($credit_note_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }
            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('credit_notes/list_credit_notes/' . $aRow['id']) . '" target="_blank">' . format_credit_note_number($aRow['id']) . '</a>';

                $row[] = _d($aRow['date']);

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['reference_no'];

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($credit_note_taxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = app_format_money($aRow['remaining_amount'], $currency->name);
                $footer_data['remaining_amount'] += $aRow['remaining_amount'];

                $row[] = format_credit_note_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function invoices_report()
    {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes     = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = [
                'number',
                get_sql_select_client_company(),
                'YEAR(date) as year',
                'date',
                'duedate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.invoice_id=' . db_prefix() . 'invoices.id) as credits_applied',
                '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'invoicepaymentrecords WHERE invoiceid = ' . db_prefix() . 'invoices.id) - (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.invoice_id=' . db_prefix() . 'invoices.id))',
                'status',
            ];

            $where = [
                'AND status != 5',
            ];

            $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach ($invoiceTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'invoices.id) as total_tax_single_' . $key);
            }

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('sale_agent_invoices')) {
                $agents  = $this->input->post('sale_agent_invoices');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency              = $this->input->post('report_currency');
            $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency                          = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }

            if ($this->input->post('invoice_status')) {
                $statuses  = $this->input->post('invoice_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'invoices';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'invoices.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'invoices.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'           => 0,
                'subtotal'        => 0,
                'total_tax'       => 0,
                'discount_total'  => 0,
                'adjustment'      => 0,
                'applied_credits' => 0,
                'amount_open'     => 0,
            ];

            foreach ($invoice_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['duedate']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($invoice_taxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = app_format_money($aRow['credits_applied'], $currency->name);
                $footer_data['applied_credits'] += $aRow['credits_applied'];

                $amountOpen = $aRow['amount_open'];
                $row[]      = app_format_money($amountOpen, $currency->name);
                $footer_data['amount_open'] += $amountOpen;

                $row[] = format_invoice_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function incidents_report()
    {

        if ($this->input->is_ajax_request()) 
        {
           
            $this->load->model('dashboard_model');
            $this->load->model('tickets_model');

            $statuses             = $this->tickets_model->get_ticket_status();

            $select = [
                'ticketid',
                'subject',
                'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name', 
                
                db_prefix() . 'services.name as service_name',
                'status',
                //'priority',
                'date'
                
            ];

            $where  = []; 
            $join = [
                    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'tickets.contactid',
                    'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'tickets.service',
                    'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'tickets.department',
                    'LEFT JOIN ' . db_prefix() . 'tickets_status ON ' . db_prefix() . 'tickets_status.ticketstatusid = ' . db_prefix() . 'tickets.status',
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'tickets.userid',
                    'LEFT JOIN ' . db_prefix() . 'tickets_priorities ON ' . db_prefix() . 'tickets_priorities.priorityid = ' . db_prefix() . 'tickets.priority',
                    ];

            $additionalSelect = ['adminread', 'department', 'ticketkey', db_prefix() . 'tickets.userid', 'statuscolor', db_prefix() . 'tickets.name as ticket_opened_by_name', db_prefix() . 'tickets.email', db_prefix() . 'tickets.userid', 'assigned', db_prefix() . 'clients.company', 'is_approved','approval_assigned_to_staffid','incident_mail_flag'];



            $custom_date_select = $this->get_where_report_period();

            //print_r($custom_date_select);die();

            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

    
            if ($this->input->post('incident_status')) {
                $statuses  = $this->input->post('incident_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }               
            } 

            if ($this->input->post('service_category')) {
                $services  = $this->input->post('service_category');
                $_services = [];
                if (is_array($services)) {
                    foreach ($services as $service) {
                        if ($service != '') {
                            array_push($_services, $this->db->escape_str($service));
                        }
                    }
                }
                if (count($_services) > 0) {
                    array_push($where, 'AND service IN (' . implode(', ', $_services) . ')');
                }               
            }

            if ($this->input->post('staff_ticket_staffid')) {
                $staffs  = $this->input->post('staff_ticket_staffid');
                $_staffs = [];
                if (is_array($staffs)) {
                    foreach ($staffs as $staff) {
                        if ($staff != '') {
                            array_push($_staffs, $this->db->escape_str($staff));
                        }
                    }
                }
                if (count($_staffs) > 0) {
                    array_push($where, 'AND tbltickets.assigned IN (' . implode(', ', $_staffs) . ')');
                }
            }

            if ($this->input->post('staff_ticket_lastrply')) {
                $hours = $this->input->post('staff_ticket_lastrply');
                $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                //echo $lastDate; die;
                array_push($where, 'AND (tbltickets.lastreply < "'.$lastDate.'" OR tbltickets.lastreply is null)');
            }
            
            // if($this->input->post('incident_clientid'))
            // {
            //     array_push($where, 'AND tbltickets.userid = '.$this->input->post('incident_clientid'));
            // }
            
            if($this->input->post('report_clientid'))
            {
                array_push($where, 'AND tbltickets.userid = '.$this->input->post('report_clientid'));
            }


            $aColumns     = $select;
            $sIndexColumn = 'ticketid';
            $sTable       = db_prefix() . 'tickets';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            //print_r($rResult);die();

            foreach ($rResult as $aRow) {

                $row = [];

                for ($i= 0;$i < count($aColumns);$i++)
                {
                    if ($aColumns[$i] == 'ticketid')
                    {
                        $row[] = '<a href="' . admin_url('tickets/ticket/' . $aRow['ticketid']) . '" target="_blank">' .'IN'.$aRow['ticketid'] . '</a>';
                    }
                }

                $row[] = $aRow['subject'];

                $row[] = $aRow['contact_full_name'].'('.$aRow['company'].')';

                // if($aRow['assigned'] != 0)
                // {
                //     $assigned = $this->ci->db->where('staffid',$aRow['assigned'])->get('tblstaff')->row();
                //     $row[] = $assigned->firstname.' '.$assigned->lastname;
                // }
                // else
                // {
                //      $row[] = '';
                // }

                // if($aRow['email'] != null)
                // {
                //     $row[] = $aRow['email'];
                // }
                // else
                // {
                //     $row[] = '';
                // }

                // if($aRow['department'] != null)
                // {
                //     $row[] = $this->db->where('departmentid',$aRow['department'])->get('tbldepartments')->row()->name;
                // }
                // else
                // {
                //     $row[] = '';
                // }

                // if($aRow['priority'] != 0)
                // {
                //     $row[] = $this->ci->db->where('priorityid',$aRow['priority'])->get('tbltickets_priorities')->row()->name;
                // }
                // else
                // {
                //     $row[] = '';
                // }

                if ($aRow['status'] != 0)
                {
                    $row[] = '<span class="label inline-block ticket-status-' . $aRow['status'] . '" style="border:1px solid ' . $aRow['statuscolor'] . '; color:' . $aRow['statuscolor'] . '">' . ticket_status_translate($aRow['status']) . '</span>';
                }
                else
                {
                    $row[] = '';
                }
                
                $row[] = $aRow['service_name'];

                if($aRow['date'] != null)
                {
                    $row[] = $aRow['date'];
                }
                else
                {
                    $row[] = '';
                }

                $resolved_date = $this->ci->db->where('ticketid', $aRow['ticketid'])->where('status', 3)
                                  ->get('tblticket_status_logs')->row();

                //print_r($resolved_date);die();

                if($resolved_date != null)
                {
                    $row[] = $resolved_date->datetime;
                }
                else
                {
                    $row[] = '';
                }


                if($resolved_date != null)
                {
                    $ticket_id = $resolved_date->ticketid;
                    if($ticket_id)
                    {
                        $assigned_id = $this->ci->db->where('ticketid',$ticket_id)
                                            ->get('tbltickets')->row()->assigned;
                       if($assigned_id) 
                       {
                        $assigned = $this->ci->db->where('staffid',$assigned_id)->get('tblstaff')->row();
                        $row[] = $assigned->firstname.' '.$assigned->lastname;
                       } 
                       else{
                        $row[] = '' ;
                       }                     
                    }
                }
                else
                {
                      $row[] = '';
                }


                $output['aaData'][] = $row;

               
            }

            $result_report = data_tables_init_report($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output_report  = $result_report['output'];
            $rResult_report = $result_report['rResult'];

            $service_data = [];
            $service_name = $this->db->get('tblservices')->result_array();

            foreach ($service_name as $key => $service) 
            {
                $count = 1;
                foreach ($result_report['rResult'] as $key2 => $value) 
                {   
                    if($service['name'] == $value['service_name'])
                    {   
                        $service_data[$service['name']] =  $count++;
                    }
                }
            }
            
            $staff_data = [];
            $staff_name = $this->db->get('tblstaff')->result_array();
            foreach ($staff_name as $key => $staff) 
            {
                $count = 1;
                foreach ($result_report['rResult'] as $key2 => $value) 
                {   
                    if($staff['staffid'] == $value['assigned'])
                    {   
                        $staff_data[$staff['firstname']] =  $count++;
                    }
                }
            }

            $this->session->set_tempdata('incident_service_count', json_encode($service_data,300));
            $this->session->set_tempdata('incident_staff_count', json_encode($staff_data,300));

            echo json_encode($output); 
            die();
        }
    }

    public function get_incident_service_count()
    {
        $result = '';

        $result = $this->session->tempdata('incident_service_count');
      
        echo $result;

        unset($_SESSION['incident_service_count']);
    }

    public function get_incident_staff_count()
    {
        $result = '';
        $result = $this->session->tempdata('incident_staff_count');
        echo $result;
        unset($_SESSION['incident_staff_count']);
    }

    public function get_service_request_staff_count()
    {
        $result = '';
        $result = $this->session->tempdata('service_request_staff_count');
        echo $result;
        unset($_SESSION['service_request_staff_count']);
    }

    public function get_all_tickets_staff_count()
    {
        $result = '';
        $result = $this->session->tempdata('all_tickets_staff_count');
        echo $result;
        unset($_SESSION['all_tickets_staff_count']);
    }

    public function store_incident_chart_image()
    {
        if($this->input->post())
        {
            $imageData = str_replace('[removed]','',$this->input->post());
            $data = base64_decode($imageData['data']);

            file_put_contents(TEMP_FOLDER.'Incident_chart.png', $data);
            echo "true";
        }
    }


    public function store_service_request_chart_image()
    {
        if($this->input->post())
        {
            $imageData = str_replace('[removed]','',$this->input->post());
            $data = base64_decode($imageData['data']);

            file_put_contents(TEMP_FOLDER.'Service_request_chart.png', $data);
        }
    }

    public function store_service_request_requestfor_chart_image()
    {
        if($this->input->post())
        {
            $imageData = str_replace('[removed]','',$this->input->post());
            $data = base64_decode($imageData['data']);

            file_put_contents(TEMP_FOLDER.'SR_requestfor_chart.png', $data);
        }
    }
    public function send_pdf_to_mail()
    {
        if($this->input->post())
        {
            $table = $this->input->post('tableData');
            if($table)
            {
                $table = preg_replace("/<img[^>]+\>/i", "", $table); 
                //echo $content;
            }

            $email = $this->input->post('email');

            $pdf_name = $this->input->post('pdfname');

            //print_r($this->input->post());die();
            $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
             
            $tcpdf->SetCreator(PDF_CREATOR);
            $tcpdf->SetAuthor('');
            
            $tcpdf->SetTitle("Report");
            
            $tcpdf->SetKeywords('TCPDF, PDF, example, test, guide');
             
            $tcpdf->setFooterData(array(0,65,0), array(0,65,127));
             
            $tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

            $tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
             
            $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
             
            // set default margins
            $tcpdf->SetMargins(0, PDF_MARGIN_TOP, 0);
            // Set Header Margin
            //$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            // Set Footer Margin
            //$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
             
            // set auto for page breaks
            $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
             
            // set image for scale factor
            $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
             
            // it is optional :: set some language-dependent strings
            if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
            {
            // optional
            require_once(dirname(__FILE__).'/lang/eng.php');
            // optional
            $tcpdf->setLanguageArray($l);
            }
             
            $tcpdf->setFontSubsetting(true);
             
            $tcpdf->SetFont('calibri', '', 10, '', true);
             
            $tcpdf->AddPage();
             
            $tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,197,198), 'opacity'=>1, 'blend_mode'=>'Normal'));

            $html = '';

            $html .= '<style>'.file_get_contents(base_url().'assets/css/tcpdf_report.css').'</style>';

             
            $html .= <<<EOD
                    <h3 style="text-align:center;">$pdf_name</h3>
                        $table;
                    EOD;
           
            $tcpdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            
           

            //Backup Chart Code In TCPDF
            if($this->input->post('success_count') || $this->input->post('failure_count') || $this->input->post('warning_count'))
            {
                $tcpdf->SetFont('calibri', 'B', 16);
                $tcpdf->AddPage();

                $tcpdf->SetTextColor(62, 149, 205);
                $tcpdf->Write(0, 'Success :- '.$this->input->post('success_count').''); 
                
                $tcpdf->Ln();$tcpdf->Ln();

                $tcpdf->SetTextColor(142, 94, 162);
                $tcpdf->Write(0, 'Failure:- '.$this->input->post('failure_count').'');
                
                $tcpdf->Ln();$tcpdf->Ln();

                $tcpdf->SetTextColor(60, 186, 159);
                $tcpdf->Write(0, 'Warning:- '.$this->input->post('warning_count').'');

                $tcpdf->Ln();$tcpdf->Ln();

                $xc = 105;
                $yc = 100;
                $r = 40;

                $total = $this->input->post('success_count') + $this->input->post('failure_count') + $this->input->post('warning_count');


                $success_percentage = ($this->input->post('success_count') / $total) * 360;
                $failure_percentage = ($this->input->post('failure_count') / $total) * 360;
                $warning_percentage = ($this->input->post('warning_count') / $total) * 360;

                $tcpdf->SetFillColor(62, 149, 205);
                $tcpdf->PieSector($xc, $yc, $r, 0, $success_percentage, 'FD', false, 0, 2);

                $tcpdf->SetFillColor(142, 94, 162);
                $tcpdf->PieSector($xc, $yc, $r, $success_percentage, $success_percentage+$failure_percentage, 'FD', false, 0, 2);

                $tcpdf->SetFillColor(60, 186, 159);
                $tcpdf->PieSector($xc, $yc, $r, $success_percentage+$failure_percentage, 0, 'FD', false, 0, 2);

                $tcpdf->SetTextColor(255,255,255);
            }
            

            $tcpdf->Output(TEMP_FOLDER.'Report.pdf', 'F');
          
            $_attachments = TEMP_FOLDER.'Report.pdf';

            if($email)
            {
                $mail_sucess =  send_mail_template('report', $email, $_attachments, $pdf_name);

                if($mail_sucess)
                {
                   set_alert('success', _l('report send successfully'));
                   redirect(admin_url('reports/statuses'));
                }
                else
                {
                   set_alert('warning', _l('report not send'));
                   redirect(admin_url('reports/statuses'));
                }
            }
            
        }
    }

    public function service_requests_report()
    {
        if ($this->input->is_ajax_request()) {
           
            $select = [
                'service_requestid',
                'subject',
                'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name', 
                //'assigned',
                db_prefix() . 'services.name as service_name',
                'status',
                //'priority',
                'date',
                
            ];

            $where  = []; 


            $join = [
                    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid',
                    'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service',
                    'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department',
                    'LEFT JOIN ' . db_prefix() . 'service_requests_status ON ' . db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status',
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid',
                    'LEFT JOIN ' . db_prefix() . 'service_requests_priorities ON ' . db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority',
                    ];

            $additionalSelect = ['adminread', 'department', 'request_for','service_requestkey', db_prefix() . 'service_requests.userid', 'statuscolor', db_prefix() . 'service_requests.name as service_request_opened_by_name', db_prefix() . 'service_requests.email', db_prefix() . 'service_requests.userid', 'assigned', db_prefix() . 'clients.company', 'is_approved','approval_assigned_to_staffid'];



            // $where = [
            //     'AND status != 5',
            // ];


            //$invoiceTaxesSelect = array_reverse($invoice_taxes);

            // foreach ($invoiceTaxesSelect as $key => $tax) {
            //     array_splice($select, 8, 0, '(
            //         SELECT CASE
            //         WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
            //         WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
            //         ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
            //         END
            //         FROM ' . db_prefix() . 'itemable
            //         INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
            //         WHERE ' . db_prefix() . 'itemable.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'invoices.id) as total_tax_single_' . $key);
            // }

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            // if ($this->input->post('sale_agent_invoices')) {
            //     $agents  = $this->input->post('sale_agent_invoices');
            //     $_agents = [];
            //     if (is_array($agents)) {
            //         foreach ($agents as $agent) {
            //             if ($agent != '') {
            //                 array_push($_agents, $this->db->escape_str($agent));
            //             }
            //         }
            //     }
            //     if (count($_agents) > 0) {
            //         array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
            //     }
            // }

            //$by_currency              = $this->input->post('report_currency');
            //$totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            // if ($by_currency) {
            //     $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
            //     $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
            //     $select[$totalPaymentsColumnIndex] = $_temp;

            //     $currency = $this->currencies_model->get($by_currency);
            //     array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            // } else {
            //     $currency                          = $this->currencies_model->get_base_currency();
            //     $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            // }
            //print_r($this->input->post('incident_clientid'));die();

            if ($this->input->post('incident_status') || $this->input->post('report_clientid') || $this->input->post('request_for')) {
                $statuses  = $this->input->post('incident_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }

                if($this->input->post('report_clientid'))
                {
                    array_push($where, 'AND tblservice_requests.userid = '.$this->input->post('report_clientid'));
                }

                if($this->input->post('request_for'))
                {
                    array_push($where, 'AND tblservice_requests.request_for = '.$this->input->post('request_for'));
                }
            }

            if ($this->input->post('staff_ticket_lastrply')) {
                $hours = $this->input->post('staff_ticket_lastrply');
                $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                //echo $lastDate; die;
                array_push($where, 'AND (tblservice_requests.lastreply < "'.$lastDate.'" OR tblservice_requests.lastreply is null)');
            } 

            if ($this->input->post('staff_ticket_staffid')) {
                $staffs  = $this->input->post('staff_ticket_staffid');
                $_staffs = [];
                if (is_array($staffs)) {
                    foreach ($staffs as $staff) {
                        if ($staff != '') {
                            array_push($_staffs, $this->db->escape_str($staff));
                        }
                    }
                }
                if (count($_staffs) > 0) {
                    array_push($where, 'AND tblservice_requests.assigned IN (' . implode(', ', $_staffs) . ')');
                }
            }

            if ($this->input->post('service_category')) {
                $services  = $this->input->post('service_category');
                $_services = [];
                if (is_array($services)) {
                    foreach ($services as $service) {
                        if ($service != '') {
                            array_push($_services, $this->db->escape_str($service));
                        }
                    }
                }
                if (count($_services) > 0) {
                    array_push($where, 'AND service IN (' . implode(', ', $_services) . ')');
                }               
            }

            $aColumns     = $select;
            $sIndexColumn = 'service_requestid';
            $sTable       = db_prefix() . 'service_requests';
            
            // $join         = [
            //     'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'invoices.clientid',
            // ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            //print_r($result);die();

            $output  = $result['output'];
            $rResult = $result['rResult'];

        
            // foreach ($invoice_taxes as $key => $tax) {
            //     $footer_data['total_tax_single_' . $key] = 0;
            // }
            //print_r($rResult );die();

            foreach ($rResult as $aRow) {
                $row = [];

                for ($i= 0;$i < count($aColumns);$i++)
                {
                    if ($aColumns[$i] == 'service_requestid')
                    {
                        $row[] = '<a href="' . admin_url('service_requests/service_request/' . $aRow['service_requestid']) . '" target="_blank">' .'SR'.$aRow['service_requestid'] . '</a>';
                    }
                }

                $row[] = $aRow['subject'];

                $row[] = $aRow['contact_full_name'].'('.$aRow['company'].')';

                // if($aRow['assigned'] != 0)
                // {
                //     $assigned = $this->ci->db->where('staffid',$aRow['assigned'])->get('tblstaff')->row();
                //     $row[] = $assigned->firstname.' '.$assigned->lastname;
                // }
                // else
                // {
                //      $row[] = '';
                // }

                // if($aRow['email'] != null)
                // {
                //     $row[] = $aRow['email'];
                // }
                // else
                // {
                //     $row[] = '';
                // }

                // if($aRow['department'] != 0)
                // {
                //     $row[] = $this->db->where('departmentid',$aRow['department'])->get('tbldepartments')->row()->name;
                // }
                // else
                // {
                //     $row[] = '';
                // }

                if($aRow['request_for'] != null)
                {
                    $row[] = $this->db->where('requestid', $aRow['request_for'])->get('tbltickets_request_for')->row()->name;
                }
                else
                {
                    $row[] = '';
                }

                // if($aRow['priority'] != 0)
                // {
                //     $row[] = $this->ci->db->where('priorityid',$aRow['priority'])->get('tblservice_requests_priorities')->row()->name;
                // }
                // else
                // {
                //     $row[] = '';
                // }
                if ($aRow['status'] != 0)
                {
                    $row[] = '<span class="label inline-block service_request-status-' . $aRow['status'] . '" style="border:1px solid ' . $aRow['statuscolor'] . '; color:' . $aRow['statuscolor'] . '">' . service_request_status_translate($aRow['status']) . '</span>';
                }
                else
                {
                    $row[] = '';
                }
                //$row[] = $aRow['assigned'];

                $row[] = $aRow['service_name'];

                //$row[] = $aRow['status'];

                //$row[] = $aRow['priority'];

                if($aRow['date'] != null)
                {
                    $row[] = $aRow['date'];
                }
                else
                {
                    $row[] = '';
                }

                $resolved_date = $this->ci->db->where('service_requestid', $aRow['service_requestid'])
                                              ->where('status', 3)
                                              ->get('tblservice_request_status_logs')->row();

                //print_r($resolved_date);die();

                if($resolved_date != null)
                {
                    $row[] = $resolved_date->datetime;
                }
                else
                {
                    $row[] = '';
                }

                if($resolved_date != null)
                {
                    $service_request_id = $resolved_date->service_requestid;
                    if($service_request_id)
                    {
                        $assigned_id = $this->ci->db->where('service_requestid',$service_request_id)
                                            ->get('tblservice_requests')->row()->assigned;
                       if($assigned_id != 0) 
                       {
                        $assigned = $this->ci->db->where('staffid',$assigned_id)->get('tblstaff')->row();
                        $row[] = $assigned->firstname.' '.$assigned->lastname;
                       } 
                       else{
                        $row[] = '' ;
                       }                  
                    }
                }
                else
                {
                      $row[] = '';
                }

             
                
                $output['aaData'][] = $row;
            }

           $result_report = data_tables_init_report($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output_report  = $result_report['output'];
            $rResult_report = $result_report['rResult'];

            $service_data = [];
            $requestfor_data = [];

            $service_name = $this->db->get('tblservices')->result_array();

            $requestfor = $this->db->get('tbltickets_request_for')->result_array();

            foreach ($service_name as $key => $service) 
            {
                $count = 1;
                foreach ($result_report['rResult'] as $key2 => $value) 
                {   
                    if($service['name'] == $value['service_name'])
                    {   
                        $service_data[$service['name']] =  $count++;
                    }
                }
            }

            // print_r($service_name);
            //print_r($result_report['rResult']);die();


            foreach ($requestfor as $key => $request) 
            {   
                $count = 1;
                foreach ($result_report['rResult'] as $key2 => $value) 
                {   
                    if($request['requestid'] == $value['request_for'])
                    {   
                        $requestfor_data[$request['name']] =  $count++;
                    }
                }
            }
            
            $staff_data = [];
            $staff_name = $this->db->get('tblstaff')->result_array();
            foreach ($staff_name as $key => $staff) 
            {
                $count = 1;
                foreach ($result_report['rResult'] as $key2 => $value) 
                {   
                    if($staff['staffid'] == $value['assigned'])
                    {   
                        $staff_data[$staff['firstname']] =  $count++;
                    }
                }
            }

            $this->session->set_tempdata('service_request_staff_count', json_encode($staff_data,300));
            $this->session->set_tempdata('service_request_service_count', json_encode($service_data,300));
            $this->session->set_tempdata('service_request_requestfor_count', json_encode($requestfor_data,300));
           
            echo json_encode($output);
            die();
        }
    }

    public function all_tickets_report()
    {
        if ($this->input->is_ajax_request()) {

            $this->load->model('dashboard_model');
            $this->load->model('tickets_model');

            $select = [
                'service_requestid',
                'subject',
                'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name', 
                //'assigned',
                db_prefix() . 'services.name as service_name',
                'status',
                //'priority',
                'date',
            ];

            $where  = []; 

            $join = [
                    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'service_requests.contactid',
                    'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'service_requests.service',
                    'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'service_requests.department',
                    'LEFT JOIN ' . db_prefix() . 'service_requests_status ON ' . db_prefix() . 'service_requests_status.service_requeststatusid = ' . db_prefix() . 'service_requests.status',
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'service_requests.userid',
                    'LEFT JOIN ' . db_prefix() . 'service_requests_priorities ON ' . db_prefix() . 'service_requests_priorities.priorityid = ' . db_prefix() . 'service_requests.priority',
                    ];

            $additionalSelect = ['adminread', 'department', 'service_requestkey', db_prefix() . 'service_requests.userid', 'statuscolor', db_prefix() . 'service_requests.name as service_request_opened_by_name', db_prefix() . 'service_requests.email', db_prefix() . 'service_requests.userid', 'assigned', db_prefix() . 'clients.company', 'is_approved','approval_assigned_to_staffid'];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('incident_status') || $this->input->post('report_clientid')) {
                $statuses  = $this->input->post('incident_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }

                if($this->input->post('report_clientid'))
                {
                    array_push($where, 'AND tblservice_requests.userid = '.$this->input->post('report_clientid'));
                }
            }

            if ($this->input->post('staff_ticket_lastrply')) {
                $hours = $this->input->post('staff_ticket_lastrply');
                $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                //echo $lastDate; die;
                array_push($where, 'AND (tblservice_requests.lastreply < "'.$lastDate.'" OR tblservice_requests.lastreply is null)');
            } 

            if ($this->input->post('staff_ticket_staffid')) {
                $staffs  = $this->input->post('staff_ticket_staffid');
                $_staffs = [];
                if (is_array($staffs)) {
                    foreach ($staffs as $staff) {
                        if ($staff != '') {
                            array_push($_staffs, $this->db->escape_str($staff));
                        }
                    }
                }
                if (count($_staffs) > 0) {
                    array_push($where, 'AND tblservice_requests.assigned IN (' . implode(', ', $_staffs) . ')');
                }
            }

            if ($this->input->post('service_category')) {
                $services  = $this->input->post('service_category');
                $_services = [];
                if (is_array($services)) {
                    foreach ($services as $service) {
                        if ($service != '') {
                            array_push($_services, $this->db->escape_str($service));
                        }
                    }
                }
                if (count($_services) > 0) {
                    array_push($where, 'AND service IN (' . implode(', ', $_services) . ')');
                }               
            }

            $aColumns     = $select;
            $sIndexColumn = 'service_requestid';
            $sTable       = db_prefix() . 'service_requests';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output  = $result['output'];
            $rResult = $result['rResult'];
            $serviceResult = $result['rResult'];
            $total_sr_records = $result['output']['iTotalRecords'];

            //print_r($output['iTotalRecords']);die();

            foreach ($rResult as $aRow) {
                $row = [];

                for ($i= 0;$i < count($aColumns);$i++)
                {
                    if ($aColumns[$i] == 'service_requestid')
                    {
                        $row[] = '<a href="' . admin_url('service_requests/service_request/' . $aRow['service_requestid']) . '" target="_blank">' .'SR'.$aRow['service_requestid'] . '</a>';
                    }
                }

                $row[] = $aRow['subject'];

                $row[] = $aRow['contact_full_name'].'('.$aRow['company'].')';

                // if($aRow['request_for'] != null)
                // {
                //     $row[] = $this->db->where('requestid', $aRow['request_for'])->get('tbltickets_request_for')->row()->name;
                // }
                // else
                // {
                //     $row[] = '';
                // }

                if ($aRow['status'] != 0)
                {
                    $row[] = '<span class="label inline-block service_request-status-' . $aRow['status'] . '" style="border:1px solid ' . $aRow['statuscolor'] . '; color:' . $aRow['statuscolor'] . '">' . service_request_status_translate($aRow['status']) . '</span>';
                }
                else
                {
                    $row[] = '';
                }
                //$row[] = $aRow['assigned'];

                $row[] = $aRow['service_name'];

                //$row[] = $aRow['status'];

                //$row[] = $aRow['priority'];

                if($aRow['date'] != null)
                {
                    $row[] = $aRow['date'];
                }
                else
                {
                    $row[] = '';
                }

                $resolved_date = $this->ci->db->where('service_requestid', $aRow['service_requestid'])
                                              ->where('status', 3)
                                              ->get('tblservice_request_status_logs')->row();

                //print_r($resolved_date);die();

                if($resolved_date != null)
                {
                    $row[] = $resolved_date->datetime;
                }
                else
                {
                    $row[] = '';
                }

                if($resolved_date != null)
                {
                    $service_request_id = $resolved_date->service_requestid;
                    if($service_request_id)
                    {
                        $assigned_id = $this->ci->db->where('service_requestid',$service_request_id)
                                            ->get('tblservice_requests')->row()->assigned;
                       if($assigned_id != 0) 
                       {
                        $assigned = $this->ci->db->where('staffid',$assigned_id)->get('tblstaff')->row();
                        $row[] = $assigned->firstname.' '.$assigned->lastname;
                       } 
                       else{
                        $row[] = '' ;
                       }                  
                    }
                }
                else
                {
                      $row[] = '';
                }
                
                $output['aaData'][] = $row;
            }

            $statuses = $this->tickets_model->get_ticket_status();
            $select = [
                'ticketid',
                'subject',
                'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name', 
                db_prefix() . 'services.name as service_name',
                'status',
                //'priority',
                'date'
            ];

            $where  = []; 
            $join = [
                    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'tickets.contactid',
                    'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'tickets.service',
                    'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'tickets.department',
                    'LEFT JOIN ' . db_prefix() . 'tickets_status ON ' . db_prefix() . 'tickets_status.ticketstatusid = ' . db_prefix() . 'tickets.status',
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'tickets.userid',
                    'LEFT JOIN ' . db_prefix() . 'tickets_priorities ON ' . db_prefix() . 'tickets_priorities.priorityid = ' . db_prefix() . 'tickets.priority',
                    ];

            $additionalSelect = ['adminread', 'department', 'ticketkey', db_prefix() . 'tickets.userid', 'statuscolor', db_prefix() . 'tickets.name as ticket_opened_by_name', db_prefix() . 'tickets.email', db_prefix() . 'tickets.userid', 'assigned', db_prefix() . 'clients.company', 'is_approved','approval_assigned_to_staffid','incident_mail_flag'];

            $custom_date_select = $this->get_where_report_period();
            //print_r($custom_date_select);die();

            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }
    
            if ($this->input->post('incident_status')) {
                $statuses  = $this->input->post('incident_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }               
            }

            if ($this->input->post('staff_ticket_lastrply')) {
                $hours = $this->input->post('staff_ticket_lastrply');
                $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                //echo $lastDate; die;
                array_push($where, 'AND (tbltickets.lastreply < "'.$lastDate.'" OR tbltickets.lastreply is null)');
            } 

            if ($this->input->post('staff_ticket_staffid')) {
                $staffs  = $this->input->post('staff_ticket_staffid');
                $_staffs = [];
                if (is_array($staffs)) {
                    foreach ($staffs as $staff) {
                        if ($staff != '') {
                            array_push($_staffs, $this->db->escape_str($staff));
                        }
                    }
                }
                if (count($_staffs) > 0) {
                    array_push($where, 'AND tbltickets.assigned IN (' . implode(', ', $_staffs) . ')');
                }
            }

            if ($this->input->post('service_category')) {
                $services  = $this->input->post('service_category');
                $_services = [];
                if (is_array($services)) {
                    foreach ($services as $service) {
                        if ($service != '') {
                            array_push($_services, $this->db->escape_str($service));
                        }
                    }
                }
                if (count($_services) > 0) {
                    array_push($where, 'AND service IN (' . implode(', ', $_services) . ')');
                }               
            }
            
            if($this->input->post('report_clientid'))
            {
                array_push($where, 'AND tbltickets.userid = '.$this->input->post('report_clientid'));
            }

            $aColumns     = $select;
            $sIndexColumn = 'ticketid';
            $sTable       = db_prefix() . 'tickets';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            //$output  = $result['output'];
            $output['iTotalRecords'] += $result['output']['iTotalRecords'];
            $output['iTotalDisplayRecords'] += $result['output']['iTotalDisplayRecords'];
            $rResult = $result['rResult'];
            $supportResult = $result['rResult'];

            foreach ($rResult as $aRow) {

                $row = [];
                for ($i= 0;$i < count($aColumns);$i++)
                {
                    if ($aColumns[$i] == 'ticketid')
                    {
                        $row[] = '<a href="' . admin_url('tickets/ticket/' . $aRow['ticketid']) . '" target="_blank">' .'IN'.$aRow['ticketid'] . '</a>';
                    }
                }

                $row[] = $aRow['subject'];
                $row[] = $aRow['contact_full_name'].'('.$aRow['company'].')';

                if ($aRow['status'] != 0)
                {
                    $row[] = '<span class="label inline-block ticket-status-' . $aRow['status'] . '" style="border:1px solid ' . $aRow['statuscolor'] . '; color:' . $aRow['statuscolor'] . '">' . ticket_status_translate($aRow['status']) . '</span>';
                }
                else
                {
                    $row[] = '';
                }
                
                $row[] = $aRow['service_name'];

                if($aRow['date'] != null)
                {
                    $row[] = $aRow['date'];
                }
                else
                {
                    $row[] = '';
                }

                $resolved_date = $this->ci->db->where('ticketid', $aRow['ticketid'])->where('status', 3)
                                  ->get('tblticket_status_logs')->row();

                if($resolved_date != null)
                {
                    $row[] = $resolved_date->datetime;
                }
                else
                {
                    $row[] = '';
                }

                if($resolved_date != null)
                {
                    $ticket_id = $resolved_date->ticketid;
                    if($ticket_id)
                    {
                        $assigned_id = $this->ci->db->where('ticketid',$ticket_id)
                                            ->get('tbltickets')->row()->assigned;
                       if($assigned_id) 
                       {
                        $assigned = $this->ci->db->where('staffid',$assigned_id)->get('tblstaff')->row();
                        $row[] = $assigned->firstname.' '.$assigned->lastname;
                       } 
                       else{
                        $row[] = '' ;
                       }                     
                    }
                }
                else
                {
                      $row[] = '';
                }
                $output['aaData'][] = $row;
            }

            // $result_report = data_tables_init_report($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            // $output_report  = $result_report['output'];
            // $rResult_report = $result_report['rResult'];

            $ticket_data = [];
            $ticket_data[''] = 0;
            $ticket_data['Support'] =  $result['output']['iTotalRecords'];
            $ticket_data['Service'] =  $total_sr_records;
            $this->session->set_tempdata('tickets_type_count', json_encode($ticket_data,300));
            
            $staff_data = [];
            $staff_name = $this->db->get('tblstaff')->result_array();
            foreach ($staff_name as $key => $staff) 
            {
                $count = 1;
                foreach ($supportResult as $key2 => $value) 
                {   
                    if($staff['staffid'] == $value['assigned'])
                    {   
                        $staff_data[$staff['firstname']] =  $count++;
                    }
                }
                foreach ($serviceResult as $key2 => $value) 
                {   
                    if($staff['staffid'] == $value['assigned'])
                    {   
                        $staff_data[$staff['firstname']] =  $count++;
                    }
                }
            }

            $this->session->set_tempdata('all_tickets_staff_count', json_encode($staff_data,300));
           
            echo json_encode($output);
            die();
        }
    }

    public function get_tickets_type_count()
    {
        $result = '';
        $result = $this->session->tempdata('tickets_type_count');
        echo $result;
        unset($_SESSION['tickets_type_count']);
    }
    
    public function staff_tickets_report()
    {
        if ($this->input->is_ajax_request()) {

            $this->load->model('dashboard_model');
            $this->load->model('tickets_model');
            
            $statuses = $this->tickets_model->get_ticket_status();

            $select = [
                'staffid',
                'email',
                'CONCAT(firstname, \' \', lastname) as full_name'
            ];

            $where  = []; 

            if ($this->input->post('staff_ticket_staffid')) {
                $staffs  = $this->input->post('staff_ticket_staffid');
                $_staffs = [];
                if (is_array($staffs)) {
                    foreach ($staffs as $staff) {
                        if ($staff != '') {
                            array_push($_staffs, $this->db->escape_str($staff));
                        }
                    }
                }
                if (count($_staffs) > 0) {
                    array_push($where, 'AND staffid IN (' . implode(', ', $_staffs) . ')');
                }
            }

            $join = [];

            $additionalSelect = [];

            // $custom_date_select = $this->get_where_report_period();
            // if ($custom_date_select != '') {
            //     array_push($where, $custom_date_select);
            // }

            $aColumns     = $select;
            $sIndexColumn = 'staffid';
            $sTable       = db_prefix() . 'staff';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            //print_r($output['iTotalRecords']);die();
            
            foreach ($rResult as $aRow) {
                $row = [];

                $staffId = $aRow['staffid'];
                $row[] = $aRow['full_name'];
                foreach($statuses as $status){
                    $totalTickets = 0;
                    $statusId = $status['ticketstatusid'];
                    $statusName = $status['name'];

                    //support request
                    if ($this->input->post('staff_ticket_reqtype') == 'support' || $this->input->post('staff_ticket_reqtype') == 'all') {
                        $this->db->select('COUNT(ticketid) as total')->from(db_prefix() . 'tickets');
                        $this->db->where('status', $statusId)->where('assigned', $staffId);
                        if ($this->input->post('staff_ticket_lastrply')) {
                            $hours = $this->input->post('staff_ticket_lastrply');
                            $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                            //echo $lastDate; die;
                            $this->db->where("(lastreply < '".$lastDate."' OR lastreply is null)", NULL, FALSE);
                        }
                        $ticket = $this->db->get()->row();
                        $totalTickets += $ticket->total;
                    }

                    //service request
                    if ($this->input->post('staff_ticket_reqtype') == 'service' || $this->input->post('staff_ticket_reqtype') == 'all') {
                        $this->db->select('COUNT(service_requestid) as total')->from(db_prefix() . 'service_requests');
                        $this->db->where('status', $statusId)->where('assigned', $staffId);
                        if ($this->input->post('staff_ticket_lastrply')) {
                            $hours = $this->input->post('staff_ticket_lastrply');
                            $lastDate = date('Y-m-d H:i:s', strtotime('-'.$hours.' hours'));
                            //echo $lastDate; die;
                            $this->db->where("(lastreply < '".$lastDate."' OR lastreply is null)", NULL, FALSE);
                        }
                        $service = $this->db->get()->row();
                        $totalTickets += $service->total;
                    }

                    $row[] = $totalTickets;
                }
                
                $output['aaData'][] = $row;
            }

            // print_r($output['aaData']); die();
           
            echo json_encode($output);
            die();
        }
    }

    public function get_service_request_service_count()
    {
        $result = '';

        $result = $this->session->tempdata('service_request_service_count');
      
        echo $result;

        unset($_SESSION['service_request_service_count']);
    }

    public function get_service_request_requestfor_count()
    {
        $result = '';

        $result = $this->session->tempdata('service_request_requestfor_count');
      
        echo $result;

        unset($_SESSION['service_request_requestfor_count']);
    }

    public function get_all_ticket_service_count()
    {
        $result = '';

        $result = $this->session->tempdata('all_ticket_service_count');
      
        echo $result;

        unset($_SESSION['all_ticket_service_count']);
    }

    public function get_all_ticket_requestfor_count()
    {
        $result = '';

        $result = $this->session->tempdata('all_ticket_requestfor_count');
      
        echo $result;

        unset($_SESSION['all_ticket_requestfor_count']);
    }

    public function tasks_report()
    {
        if ($this->input->is_ajax_request()) {
            
            $hasPermissionEdit   = has_permission('tasks', '', 'edit');
            $hasPermissionDelete = has_permission('tasks', '', 'delete');

            $tasksPriorities     = get_tasks_priorities();
            
            $select = [
               
                db_prefix() . 'tasks.id as id',
                db_prefix() . 'tasks.name as task_name',
                'status',
                'startdate',
                'duedate',
                 get_sql_select_task_asignees_full_names() . ' as assignees',
                '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM ' . db_prefix() . 'taggables JOIN ' . db_prefix() . 'tags ON ' . db_prefix() . 'taggables.tag_id = ' . db_prefix() . 'tags.id WHERE rel_id = ' . db_prefix() . 'tasks.id and rel_type="task" ORDER by tag_order ASC) as tags',
                'priority',
            ];

            $where  = []; 

            include_once(APPPATH . 'views/admin/tables/includes/tasks_filter.php');

            array_push($where, 'AND CASE WHEN rel_type="project" AND rel_id IN (SELECT project_id FROM ' . db_prefix() . 'project_settings WHERE project_id=rel_id AND name="hide_tasks_on_main_tasks_table" AND value=1) THEN rel_type != "project" ELSE 1=1 END');


            $join  = [];


            $custom_date_select = $this->get_where_report_period('dateadded');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

    
            if ($this->input->post('task_status') || $this->input->post('task_assigned') ) {
                $statuses  = $this->input->post('task_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }

                if ($this->input->post('task_assigned')) {
                
                    $data = $this->db->where('staffid', $this->input->post('task_assigned'))
                                     ->get('tbltask_assigned')->result_array();

                    $_staff = [];

                    if(!empty($data))
                    {
                        foreach ($data as $value) {
                            if ($value != '') {
                                array_push($_staff, $this->db->escape_str($value['taskid']));
                            }
                        }
                        if (count($_staff) > 0) {
                            array_push($where, 'AND id IN (' . implode(', ', $_staff) . ')');
                        }
                    }
                    
                    //print_r($where);die();
                }
            }

            



            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'tasks';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, 
                [
                    'rel_type',
                    'rel_id',
                    'recurring',
                    tasks_rel_name_select_query() . ' as rel_name',
                    'billed',
                    '(SELECT staffid FROM ' . db_prefix() . 'task_assigned WHERE taskid=' . db_prefix() . 'tasks.id AND staffid=' . get_staff_user_id() . ') as is_assigned',
                    get_sql_select_task_assignees_ids() . ' as assignees_ids',
                    '(SELECT MAX(id) FROM ' . db_prefix() . 'taskstimers WHERE task_id=' . db_prefix() . 'tasks.id and staff_id=' . get_staff_user_id() . ' and end_time IS NULL) as not_finished_timer_by_current_staff',
                    '(SELECT staffid FROM ' . db_prefix() . 'task_assigned WHERE taskid=' . db_prefix() . 'tasks.id AND staffid=' . get_staff_user_id() . ') as current_user_is_assigned',
                    '(SELECT CASE WHEN addedfrom=' . get_staff_user_id() . ' AND is_added_from_contact=0 THEN 1 ELSE 0 END) as current_user_is_creator',
                ]
            );

            //print_r($result);die();

            $output  = $result['output'];
            $rResult = $result['rResult'];


            //print_r($rResult);die();

            foreach ($rResult as $aRow) {
            $row = [];

           

            $row[] = '<a href="' . admin_url('tasks/view/' . $aRow['id']) . '" onclick="init_task_modal(' . $aRow['id'] . '); return false;">' . $aRow['id'] . '</a>';

            $outputName = '';

            if ($aRow['not_finished_timer_by_current_staff']) {
                $outputName .= '<span class="pull-left text-danger"><i class="fa fa-clock-o fa-fw"></i></span>';
            }

            $outputName .= '<a href="' . admin_url('tasks/view/' . $aRow['id']) . '" class="display-block main-tasks-table-href-name' . (!empty($aRow['rel_id']) ? ' mbot5' : '') . '" onclick="init_task_modal(' . $aRow['id'] . '); return false;">' . $aRow['task_name'] . '</a>';

            if ($aRow['rel_name']) {
                $relName = task_rel_name($aRow['rel_name'], $aRow['rel_id'], $aRow['rel_type']);

                $link = task_rel_link($aRow['rel_id'], $aRow['rel_type']);

                $outputName .= '<span class="hide"> - </span><a class="text-muted task-table-related" data-toggle="tooltip" title="' . _l('task_related_to') . '" href="' . $link . '">' . $relName . '</a>';
            }

            if ($aRow['recurring'] == 1) {
                $outputName .= '<br /><span class="label label-primary inline-block mtop4"> ' . _l('recurring_task') . '</span>';
            }

            $outputName .= '<div class="row-options">';

            $class = 'text-success bold';
            $style = '';

            $tooltip = '';
            if ($aRow['billed'] == 1 || !$aRow['is_assigned'] || $aRow['status'] == Tasks_model::STATUS_COMPLETE) {
                $class = 'text-dark disabled';
                $style = 'style="opacity:0.6;cursor: not-allowed;"';
                if ($aRow['status'] == Tasks_model::STATUS_COMPLETE) {
                    $tooltip = ' data-toggle="tooltip" data-title="' . format_task_status($aRow['status'], false, true) . '"';
                } elseif ($aRow['billed'] == 1) {
                    $tooltip = ' data-toggle="tooltip" data-title="' . _l('task_billed_cant_start_timer') . '"';
                } elseif (!$aRow['is_assigned']) {
                    $tooltip = ' data-toggle="tooltip" data-title="' . _l('task_start_timer_only_assignee') . '"';
                }
            }

            // if ($aRow['not_finished_timer_by_current_staff']) {
            //     $outputName .= '<a href="#" class="text-danger tasks-table-stop-timer" onclick="timer_action(this,' . $aRow['id'] . ',' . $aRow['not_finished_timer_by_current_staff'] . '); return false;">' . _l('task_stop_timer') . '</a>';
            // } else {
            //     $outputName .= '<span' . $tooltip . ' ' . $style . '>
            //     <a href="#" class="' . $class . ' tasks-table-start-timer" onclick="timer_action(this,' . $aRow['id'] . '); return false;">' . _l('task_start_timer') . '</a>
            //     </span>';
            // }

            // if ($hasPermissionEdit) {
            //     $outputName .= '<span class="text-dark"> | </span><a href="#" onclick="edit_task(' . $aRow['id'] . '); return false">' . _l('edit') . '</a>';
            // }

            // if ($hasPermissionDelete) {
            //     $outputName .= '<span class="text-dark"> | </span><a href="' . admin_url('tasks/delete_task/' . $aRow['id']) . '" class="text-danger _delete task-delete">' . _l('delete') . '</a>';
            // }
            $outputName .= '</div>';

            $row[] = $outputName;

            $canChangeStatus = ($aRow['current_user_is_creator'] != '0' || $aRow['current_user_is_assigned'] || has_permission('tasks', '', 'edit'));
            $status          = get_task_status_by_id($aRow['status']);
            $outputStatus    = '';

            $outputStatus .= '<span class="inline-block label" style="color:' . $status['color'] . ';border:1px solid ' . $status['color'] . '" task-status-table="' . $aRow['status'] . '">';

            $outputStatus .= $status['name'];

            if ($canChangeStatus) {
                $outputStatus .= '<div class="dropdown inline-block mleft5 table-export-exclude">';
                $outputStatus .= '<a href="#" style="font-size:14px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableTaskStatus-' . $aRow['id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                // $outputStatus .= '<span data-toggle="tooltip" title="' . _l('ticket_single_change_status') . '"><i class="fa fa-caret-down" aria-hidden="true"></i></span>';
                $outputStatus .= '</a>';

                $outputStatus .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableTaskStatus-' . $aRow['id'] . '">';
                // foreach ($task_statuses as $taskChangeStatus) {
                //     if ($aRow['status'] != $taskChangeStatus['id']) {
                //         $outputStatus .= '<li>
                //           <a href="#" onclick="task_mark_as(' . $taskChangeStatus['id'] . ',' . $aRow['id'] . '); return false;">
                //              ' . _l('task_mark_as', $taskChangeStatus['name']) . '
                //           </a>
                //        </li>';
                //     }
                // }
                $outputStatus .= '</ul>';
                $outputStatus .= '</div>';
            }

            $outputStatus .= '</span>';

            $row[] = $outputStatus;

            $row[] = _d($aRow['startdate']);

            $row[] = _d($aRow['duedate']);

            $row[] = format_members_by_ids_and_names($aRow['assignees_ids'], $aRow['assignees']);

            $row[] = render_tags($aRow['tags']);

            $outputPriority = '<span style="color:' . task_priority_color($aRow['priority']) . ';" class="inline-block">' . task_priority($aRow['priority']);

            if ( $aRow['status'] != Tasks_model::STATUS_COMPLETE) {
                $outputPriority .= '<div class="dropdown inline-block mleft5 table-export-exclude">';
                $outputPriority .= '<a href="#" style="font-size:14px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableTaskPriority-' . $aRow['id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                // $outputPriority .= '<span data-toggle="tooltip" title="' . _l('task_single_priority') . '"><i class="fa fa-caret-down" aria-hidden="true"></i></span>';
                $outputPriority .= '</a>';

                $outputPriority .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableTaskPriority-' . $aRow['id'] . '">';
                // foreach ($tasksPriorities as $priority) {
                //     if ($aRow['priority'] != $priority['id']) {
                //         $outputPriority .= '<li>
                //           <a href="#" onclick="task_change_priority(' . $priority['id'] . ',' . $aRow['id'] . '); return false;">
                //              ' . $priority['name'] . '
                //           </a>
                //        </li>';
                //     }
                // }
                $outputPriority .= '</ul>';
                $outputPriority .= '</div>';
            }

            $outputPriority .= '</span>';
            $row[] = $outputPriority;

           

            $row['DT_RowClass'] = 'has-row-options';

            if ((!empty($aRow['duedate']) && $aRow['duedate'] < date('Y-m-d')) && $aRow['status'] != Tasks_model::STATUS_COMPLETE) {
                $row['DT_RowClass'] .= ' text-danger';
            }

            $row = hooks()->apply_filters('tasks_table_row_data', $row, $aRow);

            $output['aaData'][] = $row;
        }


           
           
            echo json_encode($output);
            die();
        }
    }

    public function inventorys_report()
    {
        if ($this->input->is_ajax_request()) {
           
            $select = [
                'customer_prefix',
                'clientid',
                'tblinventory_details.type as type', 
                'tblinventory_details.sub_type as sub_type',
                'tblinventory_details.product_name as product_name',
                'hostname',
                'serial_no'
            ];

            $where  = []; 


            $join = [
                    'LEFT JOIN ' . db_prefix() . 'inventory_types as type ON ' . db_prefix() . 'inventory_details.type = type.id',
                    'LEFT JOIN ' . db_prefix() . 'inventory_products ON ' . db_prefix() . 'inventory_details.product_name = ' . db_prefix() . 'inventory_products.id',
                    'LEFT JOIN ' . db_prefix() . 'client_locations ON ' . db_prefix() . 'inventory_details.location = ' . db_prefix() . 'client_locations.id',
                    'LEFT JOIN ' . db_prefix() . 'contracts ON ' . db_prefix() . 'inventory_details.support_contract = ' . db_prefix() . 'contracts.id',    
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'inventory_details.clientid = ' . db_prefix() . 'clients.userid',        
                    'LEFT JOIN ' . db_prefix() . 'inventory_manufacture ON ' . db_prefix() . 'inventory_details.manufacture = ' . db_prefix() . 'inventory_manufacture.id',
                    'LEFT JOIN ' . db_prefix() . 'inventory_statuses ON ' . db_prefix() . 'inventory_details.status = ' . db_prefix() . 'inventory_statuses.id',        
                    'LEFT JOIN ' . db_prefix() . 'inventory_types as subtype ON ' . db_prefix() . 'inventory_details.sub_type = subtype.id',

                    ];

            $additionalSelect = ['tblinventory_details.*, tblinventory_manufacture.manufacture_name, tblinventory_statuses.status_name, type.type_name as type, subtype.type_name as subtype_name'];



            $custom_date_select = $this->get_where_report_period('tblinventory_details.created_at');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

    
            if ($this->input->post('inventory_status') || $this->input->post('report_clientid')) {
                $statuses  = $this->input->post('inventory_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND type IN (' . implode(', ', $_statuses) . ')');
                }

                if($this->input->post('report_clientid'))
                {
                    array_push($where, 'AND tblinventory_details.clientid = '.$this->input->post('report_clientid'));
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'inventory_details';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            //print_r($result);die();

            $output  = $result['output'];
            $rResult = $result['rResult'];

            //print_r($rResult);die();

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = $aRow['customer_prefix'];

                if($aRow['clientid'] != 0 )
                {
                    $row[] = $this->db->where('userid', $aRow['clientid'])->get('tblclients')->row()->company;
                }   
                else
                {
                    $row[] = '';
                }
                

                $row[] = $aRow['type'];

                if($aRow['subtype_name'] != null)
                {
                    $row[] = $aRow['subtype_name'];
                }
                else
                {
                     $row[] = '';
                }


                if($aRow['product_name'] != 0)
                {
                    $row[] = $this->db->where('id', $aRow['product_name'])->get('tblinventory_products')->row()->product_name;
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['hostname'] != null)
                {
                    $row[] = $aRow['hostname'];
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['serial_no'] != null)
                {
                    $row[] = $aRow['serial_no'];
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['purpose'] != null)
                {
                    $row[] = $aRow['purpose'];
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['processor'] != null)
                {
                    $row[] = $aRow['processor'];
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['ram'] != null)
                {
                    $row[] = $aRow['ram'];
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['storage'] != null)
                {
                    $row[] = $aRow['storage'];
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['status'] != 0)
                {
                    $row[] = $this->db->where('id',$aRow['status'])
                                  ->get('tblinventory_statuses')->row()->status_name;
                }
                else
                {
                    $row[] = '';
                }
                if($aRow['location'] != 0)
                {
                    $row[] = $this->db->where('id',$aRow['location'])
                                  ->get('tblclient_locations')->row()->location;
                                  
                }
                else
                {
                    $row[] = '';
                }

                
                
                $output['aaData'][] = $row;
            }

           
           
            echo json_encode($output);
            die();
        }
    }

    public function gaps_report()
    {
        if ($this->input->is_ajax_request()) {
          
            $select = [
                'tblgaps.id',
                'tblgaps.clientid',
                'tblgaps.name', 
                'tblgaps.staffid',
                'tblgaps.category_id',
                'tblgaps.impact_id',
                'tblgaps.status_id',
                'tblgaps.created_at'
            ];

            $where  = []; 


            $join = [
                    'LEFT JOIN ' . db_prefix() . 'gap_category ON ' . db_prefix() . 'gaps.category_id = ' . db_prefix() . 'gap_category.id',
                    'LEFT JOIN ' . db_prefix() . 'gap_impact ON ' . db_prefix() . 'gaps.impact_id = ' . db_prefix() . 'gap_impact.id',
                    'LEFT JOIN ' . db_prefix() . 'gap_status ON ' . db_prefix() . 'gaps.status_id = ' . db_prefix() . 'gap_status.id', 
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'gaps.clientid = ' . db_prefix() . 'clients.userid',
                    'LEFT JOIN ' . db_prefix() . 'staff ON ' . db_prefix() . 'gaps.staffid = ' . db_prefix() . 'staff.staffid',
                    ];

            $additionalSelect = ['tblgaps.created_at, tblgap_category.category_title, tblgap_impact.impact_title,tblgap_status.status_title, tblclients.company'];



            $custom_date_select = $this->get_where_report_period('tblgaps.created_at');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

    
            if ($this->input->post('gap_status') || $this->input->post('report_clientid')) {
                $statuses  = $this->input->post('gap_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status_id IN (' . implode(', ', $_statuses) . ')');
                }

                if($this->input->post('report_clientid'))
                {
                    array_push($where, 'AND tblgaps.clientid = '.$this->input->post('report_clientid'));
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'gaps';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            //print_r($result);die();

            $output  = $result['output'];
            $rResult = $result['rResult'];

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = $aRow['tblgaps.id'];

                $row[] = $aRow['company'];

                if($aRow['tblgaps.name'] != null)
                {
                    $row[] = $aRow['tblgaps.name'];
                }
                else
                {
                     $row[] = '';
                }

                if($aRow['tblgaps.staffid'])
                {
                    $staff_data = $this->db->where('staffid',$aRow['tblgaps.staffid'])->get('tblstaff')->row();
                    $row[] = $staff_data->firstname.' '.$staff_data->lastname;
                }
                else
                {
                    $row[] = '';
                }
                

                $row[] = $aRow['category_title'];

                $row[] = $aRow['impact_title'];

                $row[] = $aRow['status_title'];

                 $row[] = $aRow['created_at'];

                


                
                
                $output['aaData'][] = $row;
            }

           
           
            echo json_encode($output);
            die();
        }
    }

    public function backups_report()
    {
        $success_count = 0;
        $failure_count = 0;
        $warning_count = 0;

        if ($this->input->is_ajax_request()) {
          
            $select = [
                'tblbackups.id',
                'tblbackups.policy_title',

                'tblinventory_products.product_name',
                'tblbackup_logs.status',
                'tblbackup_logs.remark',

                'tblbackups.created_at',
                'tblbackups.restore_point',
                'tblbackups.archive',
                'tblbackups.archive_retention_period',
                'tblbackups.full_backup',
                'tblbackups.incremental_backup',
                'tblbackups.recovery_time_obj',
                'tblbackups.recovery_point_obj'
            ];

            $where  = []; 


            $join = [
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'backups.client_id = ' . db_prefix() . 'clients.userid',
                    'LEFT JOIN ' . db_prefix() . 'inventory_details a ON ' . db_prefix() . 'backups.first_backup_destination = a.id',
                    'LEFT JOIN ' . db_prefix() . 'inventory_products aa ON a.product_name = aa.id', 
                    'LEFT JOIN ' . db_prefix() . 'inventory_details b ON ' . db_prefix() . 'backups.second_backup_destination = b.id',  
                    'LEFT JOIN ' . db_prefix() . 'inventory_products bb ON b.product_name = bb.id',
                    'LEFT JOIN ' . db_prefix() . 'inventory_details c ON ' . db_prefix() . 'backups.third_backup_destination = c.id',  
                    'LEFT JOIN ' . db_prefix() . 'inventory_products cc ON c.product_name = cc.id',

                    'LEFT JOIN ' . db_prefix() . 'backup_logs ON ' . db_prefix() . 'backups.id = ' . db_prefix() . 'backup_logs.backup_id',

                    'LEFT JOIN ' . db_prefix() . 'inventory_details ON ' . db_prefix() . 'inventory_details.id = ' . db_prefix() . 'backup_logs.server',
                    'LEFT JOIN ' . db_prefix() . 'inventory_products ON ' . db_prefix() . 'inventory_details.product_name = ' . db_prefix() . 'inventory_products.id',


                    
                    ];

            $additionalSelect = ['tblclients.company, aa.product_name as first_backup_destination, bb.product_name as second_backup_destination, cc.product_name as third_backup_destination'];



            $custom_date_select = $this->get_where_report_period('tblbackups.created_at');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if($this->input->post('report_clientid'))
            {
                array_push($where, 'AND tblbackups.client_id = '.$this->input->post('report_clientid'));
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'backups';

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            foreach ($rResult as $key => $aRow) {
                $key++;    
                $row = [];

                $row[] = $key;

                $row[] = $aRow['company'];

                if($aRow['tblbackups.policy_title'] != null)
                {
                    $row[] = $aRow['tblbackups.policy_title'];
                }
                else
                {
                     $row[] = '';
                }

                if($aRow['tblinventory_products.product_name'] != null)
                {
                    
                    $row[] = $aRow['tblinventory_products.product_name'];
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['tblbackup_logs.status'] != null)
                {
                  if($aRow['tblbackup_logs.status'] == 'Success')
                  {
                    $success_count++;
                  }
                  if($aRow['tblbackup_logs.status'] == 'Warning')
                  {
                    $warning_count++;
                  }
                 if($aRow['tblbackup_logs.status'] == 'Failure')
                  {
                    $failure_count++;
                  } 

                $row[] = $aRow['tblbackup_logs.status'];

                }
                else
                {
                    $row[] = '';
                }

                if($aRow['tblbackup_logs.remark'] != null)
                {
                    
                    $row[] = $aRow['tblbackup_logs.remark'];
                }
                else
                {
                    $row[] = '';
                }
                
                if($aRow['tblbackups.restore_point'] != 0)
                {
                    
                    $row[] = $aRow['tblbackups.restore_point'];
                }
                else
                {
                    $row[] = '';
                }
                

                $row[] = $aRow['tblbackups.archive'];

                
                if($aRow['tblbackups.archive_retention_period'] != 0)
                {
                    
                    $row[] = $aRow['tblbackups.archive_retention_period'].' Months';
                }
                else
                {
                    $row[] = '';
                }

                $row[] = $aRow['tblbackups.full_backup'];

                $row[] = $aRow['tblbackups.incremental_backup'];

                if($aRow['first_backup_destination'] != null)
                {
                    
                    $row[] = $aRow['first_backup_destination'];
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['second_backup_destination'] != null)
                {
                    
                    $row[] = $aRow['second_backup_destination'];
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['third_backup_destination'] != null)
                {
                    
                    $row[] = $aRow['third_backup_destination'];
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['tblbackups.recovery_time_obj'] != 0)
                {
                    
                    $row[] = $aRow['tblbackups.recovery_time_obj'].' Hours';
                }
                else
                {
                    $row[] = '';
                }

                if($aRow['tblbackups.recovery_point_obj'] != 0)
                {
                    
                    $row[] = $aRow['tblbackups.recovery_point_obj'].' Days';
                }
                else
                {
                    $row[] = '';
                }

                $output['aaData'][] = $row;
            }
   
            $this->session->set_flashdata('success_count', $success_count);
            $this->session->set_flashdata('failure_count', $failure_count);
            $this->session->set_flashdata('warning_count', $warning_count);
           


            echo json_encode($output);
            die();
           //}
            
        }

    }

    public function get_backup_count()
    {
        $result = $this->session->flashdata('success_count').','.$this->session->flashdata('failure_count').','.$this->session->flashdata('warning_count');
      
        echo $result;

        unset( $_SESSION['success_count'],$_SESSION['failure_count'],$_SESSION['warning_count']);
        

    }

    public function expenses($type = 'simple_report')
    {
        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();

        $data['title'] = _l('expenses_report');
        if ($type != 'simple_report') {
            $this->load->model('expenses_model');
            $data['categories'] = $this->expenses_model->get_category();
            $data['years']      = $this->expenses_model->get_expenses_years();

            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    db_prefix().'expenses.category',
                    'amount',
                    'expense_name',
                    'tax',
                    'tax2',
                    '(SELECT taxrate FROM ' . db_prefix() . 'taxes WHERE id=' . db_prefix() . 'expenses.tax)',
                    'amount as amount_with_tax',
                    'billable',
                    'date',
                    get_sql_select_client_company(),
                    'invoiceid',
                    'reference_no',
                    'paymentmode',
                ];
                $join = [
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'expenses.clientid',
                    'LEFT JOIN ' . db_prefix() . 'expenses_categories ON ' . db_prefix() . 'expenses_categories.id = ' . db_prefix() . 'expenses.category',
                ];
                $where  = [];
                $filter = [];
                include_once(APPPATH . 'views/admin/tables/includes/expenses_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $by_currency = $this->input->post('currency');
                if ($by_currency) {
                    $currency = $this->currencies_model->get($by_currency);
                    array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
                } else {
                    $currency = $this->currencies_model->get_base_currency();
                }

                $sIndexColumn = 'id';
                $sTable       = db_prefix() . 'expenses';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                    db_prefix() . 'expenses_categories.name as category_name',
                    db_prefix() . 'expenses.id',
                    db_prefix() . 'expenses.clientid',
                    'currency',
                ]);
                $output  = $result['output'];
                $rResult = $result['rResult'];
                $this->load->model('currencies_model');
                $this->load->model('payment_modes_model');

                $footer_data = [
                    'tax_1'           => 0,
                    'tax_2'           => 0,
                    'amount'          => 0,
                    'total_tax'       => 0,
                    'amount_with_tax' => 0,
                ];

                foreach ($rResult as $aRow) {
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                            $_data = $aRow[strafter($aColumns[$i], 'as ')];
                        } else {
                            $_data = $aRow[$aColumns[$i]];
                        }
                        if ($aRow['tax'] != 0) {
                            $_tax = get_tax_by_id($aRow['tax']);
                        }
                        if ($aRow['tax2'] != 0) {
                            $_tax2 = get_tax_by_id($aRow['tax2']);
                        }
                        if ($aColumns[$i] == 'category') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['category_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'expense_name') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['expense_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'amount' || $i == 6) {
                            $total = $_data;
                            if ($i != 6) {
                                $footer_data['amount'] += $total;
                            } else {
                                if ($aRow['tax'] != 0 && $i == 6) {
                                    $total += ($total / 100 * $_tax->taxrate);
                                }
                                if ($aRow['tax2'] != 0 && $i == 6) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                }
                                $footer_data['amount_with_tax'] += $total;
                            }

                            $_data = app_format_money($total, $currency->name);
                        } elseif ($i == 9) {
                            $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                        } elseif ($aColumns[$i] == 'paymentmode') {
                            $_data = '';
                            if ($aRow['paymentmode'] != '0' && !empty($aRow['paymentmode'])) {
                                $payment_mode = $this->payment_modes_model->get($aRow['paymentmode'], [], false, true);
                                if ($payment_mode) {
                                    $_data = $payment_mode->name;
                                }
                            }
                        } elseif ($aColumns[$i] == 'date') {
                            $_data = _d($_data);
                        } elseif ($aColumns[$i] == 'tax') {
                            if ($aRow['tax'] != 0) {
                                $_data = $_tax->name . ' - ' . app_format_number($_tax->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($aColumns[$i] == 'tax2') {
                            if ($aRow['tax2'] != 0) {
                                $_data = $_tax2->name . ' - ' . app_format_number($_tax2->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($i == 5) {
                            if ($aRow['tax'] != 0 || $aRow['tax2'] != 0) {
                                if ($aRow['tax'] != 0) {
                                    $total = ($total / 100 * $_tax->taxrate);
                                    $footer_data['tax_1'] += $total;
                                }
                                if ($aRow['tax2'] != 0) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                    $footer_data['tax_2'] += $total;
                                }
                                $_data = app_format_money($total, $currency->name);
                                $footer_data['total_tax'] += $total;
                            } else {
                                $_data = app_format_number(0);
                            }
                        } elseif ($aColumns[$i] == 'billable') {
                            if ($aRow['billable'] == 1) {
                                $_data = _l('expenses_list_billable');
                            } else {
                                $_data = _l('expense_not_billable');
                            }
                        } elseif ($aColumns[$i] == 'invoiceid') {
                            if ($_data) {
                                $_data = '<a href="' . admin_url('invoices/list_invoices/' . $_data) . '">' . format_invoice_number($_data) . '</a>';
                            } else {
                                $_data = '';
                            }
                        }
                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                foreach ($footer_data as $key => $total) {
                    $footer_data[$key] = app_format_money($total, $currency->name);
                }

                $output['sums'] = $footer_data;
                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/expenses_detailed', $data);
        } else {
            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), [
                'billable' => 0,
            ], [
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor'     => '#fc2d42',
            ], $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), [
                'billable' => 1,
            ], [
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor'     => '#84c529',
            ], $data['current_year']));

            $data['expense_years'] = $this->expenses_model->get_expenses_years();

            if (count($data['expense_years']) > 0) {
                // Perhaps no expenses in new year?
                if (!in_array_multidimensional($data['expense_years'], 'year', date('Y'))) {
                    array_unshift($data['expense_years'], ['year' => date('Y')]);
                }
            }

            $data['categories'] = $this->expenses_model->get_category();

            $this->load->view('admin/reports/expenses', $data);
        }
    }

    public function expenses_vs_income($year = '')
    {
        $_expenses_years = [];
        $_years          = [];
        $this->load->model('expenses_model');
        $expenses_years = $this->expenses_model->get_expenses_years();
        $payments_years = $this->reports_model->get_distinct_payments_years();

        foreach ($expenses_years as $y) {
            array_push($_years, $y['year']);
        }
        foreach ($payments_years as $y) {
            array_push($_years, $y['year']);
        }

        $_years = array_map('unserialize', array_unique(array_map('serialize', $_years)));

        if (!in_array(date('Y'), $_years)) {
            $_years[] = date('Y');
        }

        rsort($_years, SORT_NUMERIC);
        $data['report_year'] = $year == '' ? date('Y') : $year;

        $data['years']                           = $_years;
        $data['chart_expenses_vs_income_values'] = json_encode($this->reports_model->get_expenses_vs_income_report($year));
        $data['base_currency']                   = get_base_currency();
        $data['title']                           = _l('als_expenses_vs_income');
        $this->load->view('admin/reports/expenses_vs_income', $data);
    }

    /* Total income report / ajax chart*/
    public function total_income_report()
    {
        echo json_encode($this->reports_model->total_income_report());
    }

    public function report_by_payment_modes()
    {
        echo json_encode($this->reports_model->report_by_payment_modes());
    }

    public function report_by_customer_groups()
    {
        echo json_encode($this->reports_model->report_by_customer_groups());
    }

    /* Leads conversion monthly report / ajax chart*/
    public function leads_monthly_report($month)
    {
        echo json_encode($this->reports_model->leads_monthly_report($month));
    }

    private function distinct_taxes($rel_type)
    {
        return $this->db->query('SELECT DISTINCT taxname,taxrate FROM ' . db_prefix() . "item_tax WHERE rel_type='" . $rel_type . "' ORDER BY taxname ASC")->result_array();
    }
}
