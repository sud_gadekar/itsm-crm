<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Invoice_items extends AdminController
{
    private $not_importable_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_items_model');
        $this->load->model('currencies_model');
    }

    /* List all available items */
    public function index()
    {
        if (!has_permission('items', '', 'view')) {
            access_denied('Invoice Items');
        }

        $this->load->model('taxes_model');
        $data['taxes']        = $this->taxes_model->get();
        $data['items_groups'] = $this->invoice_items_model->get_groups();

        $data['currencies'] = $this->currencies_model->get();
        $data['regions'] = $this->db->select('*')->get('tblitems_regions')->result_array();
        $data['manufacturers'] = $this->db->select('*')->get('tblitems_manufacturers')->result_array();

        $data['base_currency'] = $this->currencies_model->get_base_currency();

        $data['title'] = _l('invoice_items');
        $total_items = $this->db->select('*')->get('tblitems')->num_rows() + 1;
        $data['sku'] = 'DIT'.sprintf("%04d", $total_items);
        $this->load->view('admin/invoice_items/manage', $data);
    }

    public function table()
    {
        if (!has_permission('items', '', 'view')) {
            ajax_access_denied();
        }
        $this->app->get_table_data('invoice_items');
    }

    /* Edit or update items / ajax request /*/
    public function manage()
    {
        if (has_permission('items', '', 'view')) {
            if ($this->input->post()) {
                $data = $this->input->post();
                if ($data['itemid'] == '') {
                    if (!has_permission('items', '', 'create')) {
                        header('HTTP/1.0 400 Bad error');
                        echo _l('access_denied');
                        die;
                    }
                    $id      = $this->invoice_items_model->add($data);
                    $success = false;
                    $message = '';
                    if ($id) {
                        $success = true;
                        $message = _l('added_successfully', _l('sales_item'));
                    }
                    echo json_encode([
                        'success' => $success,
                        'message' => $message,
                        'item'    => $this->invoice_items_model->get($id),
                        'currencies' => $this->currencies_model->get(),
                    ]);
                } else {
                    if (!has_permission('items', '', 'edit')) {
                        header('HTTP/1.0 400 Bad error');
                        echo _l('access_denied');
                        die;
                    }
                    $success = $this->invoice_items_model->edit($data);
                    $message = '';
                    if ($success) {
                        $message = _l('updated_successfully', _l('sales_item'));
                    }
                    echo json_encode([
                        'success' => $success,
                        'message' => $message,
                        'currencies' => $this->currencies_model->get(),
                    ]);
                }
            }
        }
    }

    public function import()
    {
        if (!has_permission('items', '', 'create')) {
            access_denied('Items Import');
        }

        $this->load->library('import/import_items', [], 'import');

        $this->import->setDatabaseFields($this->db->list_fields(db_prefix().'items'))
                     ->setCustomFields(get_custom_fields('items'));

        if ($this->input->post('download_sample') === 'true') {
            $this->import->downloadSample();
        }

        if ($this->input->post()
            && isset($_FILES['file_csv']['name']) && $_FILES['file_csv']['name'] != '') {
            $this->import->setSimulation($this->input->post('simulate'))
                          ->setTemporaryFileLocation($_FILES['file_csv']['tmp_name'])
                          ->setFilename($_FILES['file_csv']['name'])
                          ->perform();

            $data['total_rows_post'] = $this->import->totalRows();

            if (!$this->import->isSimulation()) {
                set_alert('success', _l('import_total_imported', $this->import->totalImported()));
            }
        }

        $data['title'] = _l('import');
        $this->load->view('admin/invoice_items/import', $data);
    }

    public function add_group()
    {
        if ($this->input->post() && has_permission('items', '', 'create')) {
            $this->invoice_items_model->add_group($this->input->post());
            set_alert('success', _l('added_successfully', _l('item_group')));
        }
    }

    public function update_group($id)
    {
        if ($this->input->post() && has_permission('items', '', 'edit')) {
            $this->invoice_items_model->edit_group($this->input->post(), $id);
            set_alert('success', _l('updated_successfully', _l('item_group')));
        }
    }

    public function delete_group($id)
    {
        if (has_permission('items', '', 'delete')) {
            if ($this->invoice_items_model->delete_group($id)) {
                set_alert('success', _l('deleted', _l('item_group')));
            }
        }
        redirect(admin_url('invoice_items?groups_modal=true'));
    }

    /* Delete item*/
    public function delete($id)
    {
        if (!has_permission('items', '', 'delete')) {
            access_denied('Invoice Items');
        }

        if (!$id) {
            redirect(admin_url('invoice_items'));
        }

        $response = $this->invoice_items_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('invoice_item_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('invoice_item')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('invoice_item_lowercase')));
        }
        redirect(admin_url('invoice_items'));
    }

    public function bulk_action()
    {
        hooks()->do_action('before_do_bulk_action_for_items');
        $total_deleted = 0;
        if ($this->input->post()) {
            $ids                   = $this->input->post('ids');
            $has_permission_delete = has_permission('items', '', 'delete');
            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($this->input->post('mass_delete')) {
                        if ($has_permission_delete) {
                            if ($this->invoice_items_model->delete($id)) {
                                $total_deleted++;
                            }
                        }
                    }
                }
            }
        }

        if ($this->input->post('mass_delete')) {
            set_alert('success', _l('total_items_deleted', $total_deleted));
        }
    }

    public function search()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            echo json_encode($this->invoice_items_model->search($this->input->post('q')));
        }
    }

    /* Get item by id / ajax */
    public function get_item_by_id($id)
    {
        if ($this->input->is_ajax_request()) {
            $item                     = $this->invoice_items_model->get($id);
            $item->long_description   = nl2br($item->long_description);
            $item->custom_fields_html = render_custom_fields('items', $id, [], ['items_pr' => true]);
            $item->custom_fields      = [];

            $cf = get_custom_fields('items');

            foreach ($cf as $custom_field) {
                $val = get_custom_field_value($id, $custom_field['id'], 'items_pr');
                if ($custom_field['type'] == 'textarea') {
                    $val = clear_textarea_breaks($val);
                }
                $custom_field['value'] = $val;
                $item->custom_fields[] = $custom_field;
            }

            echo json_encode($item);
        }
    }

    public function get_currency_rate($id)
    {
        if ($this->input->is_ajax_request()) {
            $currencyData = $this->currencies_model->get($id);
            $currencyData->conversion_rate = $currencyData->conversion_rate;
            echo json_encode($currencyData);
        }
    }

    //Item Regions
    public function regions()
    {
        if (!is_admin())
        {
            access_denied('Items Regions');
        }
        $data['regions'] = $this->invoice_items_model->get_region();
        $data['title'] = _l('Regions');
        $this->load->view('admin/invoice_items/regions/manage', $data);
    }
    /* Add new region od update existing*/
    public function region()
    {
        if (!is_admin())
        {
            access_denied('Items Regions');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->invoice_items_model->add_region($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('Items region')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->invoice_items_model->update_region($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('Items region')));
                }
            }

        }
    }
    /* Delete items region */
    public function delete_region($id)
    {
        if (!is_admin())
        {
            access_denied('Items Regions');
        }
        if (!$id)
        {
            redirect(admin_url('invoice_items/regions'));
        }
        $response = $this->invoice_items_model->delete_region($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('Items Region')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('Items Region')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Items Region')));
        }
        redirect(admin_url('invoice_items/regions'));
    }

    // manufacturers
    /* Get all items manufacturers */
    public function manufacturers()
    {
        if (!is_admin())
        {
            access_denied('Items manufacturers');
        }
        $data['manufacturers'] = $this->invoice_items_model->get_manufacturer();
        $data['title'] = _l('Manufacturers');
        $this->load->view('admin/invoice_items/manufacturers/manage', $data);
    }
    /* Add new manufacturer od update existing*/
    public function manufacturer()
    {
        if (!is_admin())
        {
            access_denied('Items manufacturers');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->invoice_items_model->add_manufacturer($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('Items manufacturer')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->invoice_items_model->update_manufacturer($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('Items manufacturer')));
                }
            }

        }
    }
    /* Delete items manufacturer */
    public function delete_manufacturer($id)
    {
        if (!is_admin())
        {
            access_denied('Items manufacturers');
        }
        if (!$id)
        {
            redirect(admin_url('invoice_items/manufacturers'));
        }
        $response = $this->invoice_items_model->delete_manufacturer($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('Items manufacturer')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('Items manufacturer')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('Items manufacturer')));
        }
        redirect(admin_url('invoice_items/manufacturers'));
    }
}
