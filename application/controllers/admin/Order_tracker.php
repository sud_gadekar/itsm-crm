<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Order_tracker extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_sales_order_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        $this->load->model('order_tracker_model');
    }

    public function index()
    {
        if (!has_permission('sales_order', '', 'view')) {
            access_denied('sales_order');
        }
    	$data['order_tracker_lists'] = $this->order_tracker_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblorder_status')->result_array();
        //print_r($data);die();
        $data['title']      = 'Order Tracker';
    	$this->load->view('admin/order_tracker/manage', $data);

    }

    public function filter_by_customer()
    {
        if($this->input->post())
        {   $data['order_tracker_lists'] = '';
            $customerData = $this->input->post();
            if($customerData['val']=='cust'){
                $data['order_tracker_lists'] = $this->order_tracker_model->get_by_customer($customerData['id']);
            }
            if($customerData['val']=='order'){
                $data['order_tracker_lists'] = $this->order_tracker_model->get_by_order($customerData['id']);
            }
            if($customerData['val']=='status'){
                $data['order_tracker_lists'] = $this->order_tracker_model->get_by_status($customerData['id']);
            }
            
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['statuses'] = $this->db->select('*')->get('tblorder_status')->result_array();
            $data['title'] = 'Order Tracker';
            $this->load->view('admin/order_tracker/filter_table', $data);
        }
    }

    public function order($id = '')
    {
        if (!has_permission('sales_order', '', 'view')) {
            access_denied('sales_order');
        }
    	if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('sales_order', '', 'create')) {
                    access_denied('sales_order');
                }
                $data = $this->input->post();
                unset($data['contact_ids']);
                $data['contact_id'] = implode(',',$data['contact_id']);
                $id = $this->order_tracker_model->add($data);
		        if ($id) {
                    for($i=0; $i<=$data['itemcount']; $i++){
                        if($data['item_id'.$i]){
                            $itemdata['order_id'] = $id;
                            $itemdata['item_id'] =$data['item_id'.$i];
                            $itemdata['quantity'] =$data['quantity'.$i];
                            $itemdata['vendor_name'] =$data['vendor_name'.$i];
                            $itemdata['expected_delivery_date'] =$data['expected_delivery_date'.$i];
                            $itemdata['delivery_date'] =$data['delivery_date'.$i];
                            $itemdata['item_received'] =$data['item_received'.$i];
                            $itemdata['item_delivered'] =$data['item_delivered'.$i];
                            $this->db->insert(db_prefix() . 'order_tracker_items', $itemdata);
                        }
                    }

                    $status_data['order_id'] = $id;
                    $status_data['status_id'] = $data['status_id'];
                    $tracking_status = $this->order_tracker_model->add_tracking_status($status_data);
		            set_alert('success', _l('added_successfully', _l('Order')));
		            return redirect(admin_url('order_tracker'));
		        }
            }
            else 
            {  
                if (!has_permission('sales_order', '', 'edit')) {
                    access_denied('sales_order');
                }
                $data = $this->input->post();
                unset($data['contact_ids']);
                $data['contact_id'] = implode(',',$data['contact_id']);
                $this->db->where('order_id',$id)->delete(db_prefix().'order_tracker_items');
                for($i=0; $i<=$data['itemcount']; $i++){
                    if($data['item_id'.$i]){
                        $itemdata['order_id'] = $id;
                        $itemdata['item_id'] =$data['item_id'.$i];
                        $itemdata['quantity'] =$data['quantity'.$i];
                        $itemdata['vendor_name'] =$data['vendor_name'.$i];
                        $itemdata['expected_delivery_date'] =$data['expected_delivery_date'.$i];
                        $itemdata['delivery_date'] =$data['delivery_date'.$i];
                        $itemdata['item_received'] =$data['item_received'.$i];
                        $itemdata['item_delivered'] =$data['item_delivered'.$i];
                        $this->db->insert(db_prefix() . 'order_tracker_items', $itemdata);
                        unset($data['item_id'.$i], $data['quantity'.$i], $data['vendor_name'.$i], $data['expected_delivery_date'.$i], $data['delivery_date'.$i], $data['item_received'.$i], $data['item_delivered'.$i]);
                    }
                }
                if($data['itemcount'] <= 0){
                    unset($data['quantity0']);
                    unset($data['vendor_name0']);
                    unset($data['expected_delivery_date0']);
                    unset($data['delivery_date0']);
                }
                unset($data['itemcount']);

                $success = $this->db->where('id',$id)->update(db_prefix().'order_tracker', $data);
		        if ($success) {
                    $status_data['order_id'] = $id;
                    $status_data['status_id'] = $data['status_id'];
                    $tracking_status = $this->order_tracker_model->add_tracking_status($status_data);
		            set_alert('success', _l('updated_successfully', _l('Order')));
		        }
		        return redirect(admin_url('order_tracker'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('Order'));
            $data['order_item_lists'] = array();
        } else {
            $data['order_details'] = $this->db->where('id',$id)->get(db_prefix().'order_tracker')->row();
            //$data['notes'] = $this->db->where('order_id',$id)->select('*')->get('tblorder_tracker_notes')->result_array();

            $data['notes'] = $this->db->select('tblorder_tracker_notes.*, a.firstname, a.lastname, b.firstname as fname, b.lastname as lname')
                    ->from(db_prefix() . 'order_tracker_notes')
                    ->join(db_prefix() . 'staff a','tblorder_tracker_notes.added_by = a.staffid','left')
                    ->join(db_prefix() . 'staff b','tblorder_tracker_notes.updated_by = b.staffid','left')
                    ->where('order_id', $id)->get()->result_array();


            $data['order_item_lists'] = $this->db->select('tblorder_tracker_items.*, tblitems.description')
                    ->from(db_prefix() . 'order_tracker_items')
                    ->join(db_prefix() . 'items','tblorder_tracker_items.item_id = tblitems.id','left')
                    ->where('order_id', $id)->get()->result_array();
            $title = _l('edit', _l('Order'));
        }

        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['sr_requests'] = $this->db->select('*')->get('tblservice_requests')->result_array();
        $data['projects'] = $this->db->select('*')->get('tblprojects')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblorder_status')->result_array();
        $data['items'] = $this->db->select('*')->get('tblitems')->result_array();
        $data['sales_staffs']  = $this->db->select('tblstaff.*, CONCAT(tblstaff.firstname,\' \',tblstaff.lastname) as full_name')
                                    ->from(db_prefix() . 'staff')
                                    ->join(db_prefix() . 'staff_departments','tblstaff.staffid = tblstaff_departments.staffid')
                                    ->where('tblstaff.is_not_staff',0)->where('tblstaff.active',1)->where('tblstaff_departments.departmentid',6)->order_by('tblstaff.firstname', 'desc')->get()->result_array();

        $data['bodyclass']  = 'order_tracker';
        $data['title']      = $title;
        // print_r($data);die();
        $this->load->view('admin/order_tracker/order', $data);
    }

    public function order_note($id = false)
    {
        if (!has_permission('sales_order', '', 'view')) {
            access_denied('sales_order');
        }
        $data = $this->input->post();
        if ($this->input->post()) {
            if (isset($data['id'])) {
                if (!has_permission('sales_order', '', 'edit')) {
                    access_denied('sales_order');
                }

                if($data['share_to_customer']=='on'){
                    $data['share_to_customer']=1;
                }else{
                    $data['share_to_customer']=0;
                }
                $data['updated_by']=get_staff_user_id();

                $success = $this->db->where('id',$data['id'])->update(db_prefix().'order_tracker_notes', $data);
                if ($success) {

                    if($data['share_to_customer']==1){
                        $order_details = $this->db->where('id',$data['order_id'])->get(db_prefix().'order_tracker')->row();
                        $contact_ids = explode(',', $order_details->contact_id);

                        foreach($contact_ids as $contact_id){
                            $contact_details = $this->db->where('id',$contact_id)->get(db_prefix().'contacts')->row();
                            $email = $contact_details->email;

                            $success1 = $this->send_email_template($data['id'], $data['order_id'], $email);
                        }
                    }

                    set_alert('success', _l('updated_successfully', _l('order Note')));
                }
            }else {
                if (!has_permission('sales_order', '', 'create')) {
                    access_denied('sales_order');
                }

                if($data['share_to_customer']=='on'){
                    $data['share_to_customer']=1;
                }else{
                    $data['share_to_customer']=0;
                }
                $data['added_by']=get_staff_user_id();

                $this->db->insert(db_prefix() . 'order_tracker_notes', $data);
                $insert_id = $this->db->insert_id();
                if ($insert_id) {

                    if($data['share_to_customer']==1){
                        $order_details = $this->db->where('id',$data['order_id'])->get(db_prefix().'order_tracker')->row();
                        $contact_ids = explode(',', $order_details->contact_id);

                        foreach($contact_ids as $contact_id){
                            $contact_details = $this->db->where('id',$contact_id)->get(db_prefix().'contacts')->row();
                            $email = $contact_details->email;

                            $success1 = $this->send_email_template($insert_id, $data['order_id'], $email);
                        }
                    }

                    if (isset($custom_fields)) {
                        handle_custom_fields_post($insert_id, $custom_fields);
                    }
                    log_activity('New Order Note Added [' . $insert_id . ']');
                    set_alert('success', _l('added_successfully', _l('order Note')));
                    //return redirect(admin_url('orders'));
                }
            }
        }

        return redirect(admin_url('order_tracker/order/'.$data['order_id']));
        //$this->load->view('admin/orders/add/'.$data['order_id'], $data);
    }

    public function get_client_contacts($id)
    {
        if($id)
        {   
            $data['contacts'] = $this->db->select('id, firstname, lastname, email')
                ->from(db_prefix() . 'contacts')
                ->where('userid', $id)->get()->result_array();

            echo json_encode($data['contacts']);
        }
    }


    public function send_email_template($note_id, $order_id, $email, $cc = '', $template = 'order_note_send_to_contact')
    {   
        if (!$email) {
            return false;
        }
        //$notes = $this->db->where('id',$note_id)->get(db_prefix().'order_tracker_notes')->row();
        $notes = $this->db->select('tblorder_tracker_notes.*, tblorder_tracker.so_number, tblorder_tracker.description, tblorder_tracker.client_id, tblorder_tracker.status_id')
                                    ->from(db_prefix() . 'order_tracker_notes')
                                    ->join(db_prefix() . 'order_tracker','tblorder_tracker_notes.order_id = tblorder_tracker.id')
                                    ->where('tblorder_tracker_notes.id',$note_id)->get()->row();

        $sent = send_mail_template($template, $notes, $email, $cc);
        return $sent ? true : false;
    }

    public function delete($id)
    {
        if (!has_permission('sales_order', '', 'delete')) {
            access_denied('sales_order');
        }
        if (!$id) {
            redirect(admin_url('order_tracker'));
        }
        $response = $this->db->where('id',$id)->delete(db_prefix().'order_tracker');
        if ($response === true) 
        {
            set_alert('success', _l('deleted', _l('sales_order')));
            redirect(admin_url('order_tracker'));
        } else 
        {  
            set_alert('warning', _l('problem_deleting', _l('sales_order')));
            redirect(admin_url('order_tracker'));
        }
    }


    // Order statuses
    /* Get all order statuses */
    public function statuses()
    {
        if (!is_admin()) {
            access_denied('Order Statuses');
        }
        $data['statuses'] = $this->order_tracker_model->get_order_status();
        $data['title']    = 'Order statuses';
        // print_r($data);die();
        $this->load->view('admin/order_tracker/order_statuses/manage', $data);
    }

    /* Add new or edit existing status */
    public function status()
    {
        if (!is_admin()) {
            access_denied('Order Statuses');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->order_tracker_model->add_order_status($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Order Status')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->order_tracker_model->update_order_status($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Order Status')));
                }
            }
            die;
        }
    }

    /* Delete order status from database */
    public function delete_order_status($id)
    {
        if (!is_admin()) {
            access_denied('Order Statuses');
        }
        if (!$id) {
            redirect(admin_url('order_tracker/statuses'));
        }
        $response = $this->order_tracker_model->delete_order_status($id);
        if (is_array($response) && isset($response['default'])) {
            set_alert('warning', _l('cant_delete_default', _l('order_status_lowercase')));
        } elseif (is_array($response) && isset($response['referenced'])) {
            set_alert('danger', _l('is_referenced', _l('order_status_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('order_status')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('order_status_lowercase')));
        }
        redirect(admin_url('order_tracker/statuses'));
    }
}

