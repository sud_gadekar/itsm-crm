<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Tickets extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_tickets_to_none_staff_members') == 0 && !is_staff_member()) {
            redirect(admin_url());
        }
        $this->load->model('tickets_model');

    }

    public function index($status = '', $userid = '')
    {
        if (!has_permission('incidents', '', 'view') && !has_permission('incidents', '', 'view_own')) {
            access_denied('incidents');
        }
        close_setup_menu();

        if (!is_numeric($status)) {
            $status = '';
        }

        if ($this->input->is_ajax_request()) {
            if (!$this->input->post('filters_ticket_id')) {
                $tableParams = [
                    'status' => $status,
                    'userid' => $userid,
                ];
            } else {
                // request for othes tickets when single ticket is opened
                $tableParams = [
                    'userid'              => $this->input->post('filters_userid'),
                    'where_not_ticket_id' => $this->input->post('filters_ticket_id'),
                ];
                if ($tableParams['userid'] == 0) {
                    unset($tableParams['userid']);
                    $tableParams['by_email'] = $this->input->post('filters_email');
                }
            }

            $this->app->get_table_data('tickets', $tableParams);
        }

        $data['chosen_ticket_status']              = $status;
        $data['weekly_tickets_opening_statistics'] = json_encode($this->tickets_model->get_weekly_tickets_opening_statistics());
        $data['title']                             = _l('support_tickets');
        $this->load->model('departments_model');
        $data['statuses']             = $this->tickets_model->get_ticket_status();
        $data['staff_deparments_ids'] = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
        $data['departments']          = $this->departments_model->get();
        $data['priorities']           = $this->tickets_model->get_priority();
        $data['services']             = $this->tickets_model->get_service();
        $data['ticket_assignees']     = $this->tickets_model->get_tickets_assignes_disctinct();
        $data['clients']              = $this->db->select('tblclients.userid, tblclients.company')
                                                 ->get(db_prefix().'clients')->result_array();
        $data['bodyclass']            = 'tickets-page';

        
        add_admin_tickets_js_assets();
        $data['default_tickets_list_statuses'] = hooks()->apply_filters('default_tickets_list_statuses', [1, 2, 4]);
         
        //print_r($data);die();
        $this->load->view('admin/tickets/list', $data);
    }

    public function guest_enquiries()
    {
        $data['title']      = _l('Guest Enquiries');
        $data['enquiries']  = $this->db->select('*')->where('is_delete',0)->get(db_prefix().'guest_contacts')->result_array();
         
        //print_r($data);die();
        $this->load->view('admin/tickets/guest_enquiries', $data);
    }
    
    public function guest_delete($id)
    {
        if($id){
            $data['is_delete'] = 1;
            $this->db->where('id', $id);
            $this->db->update(db_prefix() . 'guest_contacts', $data);
            //$this->db->delete(db_prefix() . 'guest_contacts');
            if ($this->db->affected_rows() > 0) {
                set_alert('success', _l('deleted', _l('Sign Up Request')));
            } else {
                set_alert('warning', _l('problem_deleting', _l('sign up request')));
            }
        }

        $data['title']      = _l('Guest Enquiries');
        $data['enquiries']  = $this->db->select('*')->get(db_prefix().'guest_contacts')->result_array();
        redirect(admin_url('tickets/guest_enquiries'));
        //$this->load->view('admin/tickets/guest_enquiries', $data);
    }

    public function add($userid = false)
    {
        if (!has_permission('incidents', '', 'view')) {
            access_denied('incidents');
        }
        if ($this->input->post()) {
            if (!has_permission('incidents', '', 'create')) {
                access_denied('incidents');
            }
            
            $data            = $this->input->post(); 
            //print_r($data);die();
            unset($data['cntid']);
            if(isset($data['additional_charge'])){
                $data['additional_charges'] = implode(',', $data['additional_charge']);
                unset($data['additional_charge']);
            }
            //print_r($data);die();
            $data['message'] = html_purify($this->input->post('message', false));
            if($data['mailid']){
                 $this->db->where('id', $data['mailid']);
                 $this->db->update(db_prefix() . 'mail_inbox', array('converted_incident' => 1));
            }
            unset($data['mailid']);
            $id              = $this->tickets_model->add($data, get_staff_user_id());
            if ($id) {
                set_alert('success', _l('new_ticket_added_successfully', $id));
                redirect(admin_url('tickets/ticket/' . $id));
            }
        }
        $data['cntid']='';
        $data['userid']='';
        $data['subject'] = '';
        $data['sender_name'] = '';
        $data['from_email'] = '';
        $data['body'] = '';
        $data['ticketContact'] = false;
        $data['contactid'] = '';
        $data['domain_name'] = '';
        $data['flag'] = '';
        $data['mailid'] = '';
        $data['company'] = '';

        if (isset($_GET['id']))
        {
            $data['mailid'] = $_GET['id'];
            $output = $this->db->where('id', $_GET['id']);
            $output = $this->db->get(db_prefix() . 'mail_inbox')->row();
            $data['subject'] = $output->subject;
            $data['sender_name'] = $output->sender_name;

            $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
            preg_match_all($pattern, $output->from_email, $matches);
            
            $data['from_email'] = $matches[0][0];

            $data['domain_name'] = substr(strrchr($data['from_email'], "@"), 1);
            //print_r($data['from_email']);die;
            if ($data['from_email'])
            {
                //$domain_response = $this->db->like('website', $data['domain_name'], 'before');
                //$domain_response = $this->db->get(db_prefix() . 'clients')->row();.
                $res = $this->db->where('email',$data['from_email']);
                $res = $this->db->get(db_prefix() . 'contacts')->row();
                if($res){
                    $data['contactid'] = $res->id;
                    $data['from_email'] = $res->email;
                    $data['sender_name'] = $res->firstname.' '.$res->lastname ; 
                    
                    //print_r($res);die();
                    
                    if($res->userid !=0 ){
                    $data['company'] = $this->db->where('userid',$res->userid)->select('company')->get('tblclients')->row()->company;   
                    }
                }
                if (!empty($res->userid))
                {
                    $data['flag'] = 1;
                    $data['userid'] = $res->userid;
                }
                else {
                    $data['flag'] = 2;
                }
            }
            $data['body'] = $output->body;
        }
        
        if ($userid !== false) {
            $data['userid'] = $userid;
            $data['client'] = $this->clients_model->get($userid);
            //print_r($data['company']);die();
        }
        // Load necessary models
        $this->load->model('knowledge_base_model');
        $this->load->model('departments_model');

        $data['departments']        = $this->departments_model->get();

        $data['predefined_replies'] = $this->tickets_model->get_predefined_reply();
        $data['priorities']         = $this->tickets_model->get_priority();
        $data['services']           = $this->tickets_model->get_service();
        //print_r($data['ticket']->userid);die;
        //$data['charges']           = $this->db->select('*')
                                        // ->from('tbladditional_service_charges')
                                        // ->join('tbladditional_services','tbladditional_service_charges.id = tbladditional_services.chargestype','inner')
                                        // ->where('tbladditional_services.customer_id',$data['ticket']->userid)
                                        // ->get()->result_array();


        $whereStaff                 = [];
        if (get_option('access_tickets_to_none_staff_members') == 0) {
            $whereStaff['is_not_staff'] = 0;
        }
        $data['staff']     = $this->staff_model->get('', $whereStaff);
        $data['articles']  = $this->knowledge_base_model->get();
        $data['bodyclass'] = 'ticket';
        $data['title']     = _l('new_ticket');

        if ($this->input->get('project_id') && $this->input->get('project_id') > 0) {
            // request from project area to create new ticket
            $data['project_id'] = $this->input->get('project_id');
            $data['userid']     = get_client_id_by_project_id($data['project_id']);
            if (total_rows(db_prefix().'contacts', ['active' => 1, 'userid' => $data['userid']]) == 1) {
                $contact = $this->clients_model->get_contacts($data['userid']);
                if (isset($contact[0])) {
                    $data['contact'] = $contact[0];
                }
            }
        } elseif ($this->input->get('contact_id') && $this->input->get('contact_id') > 0 && $this->input->get('userid')) {
            $contact_id = $this->input->get('contact_id');
            if (total_rows(db_prefix().'contacts', ['active' => 1, 'id' => $contact_id]) == 1) {
                $contact = $this->clients_model->get_contact($contact_id);
                if ($contact) {
                    $data['contact'] = (array) $contact;
                }
            }
        }
        add_admin_tickets_js_assets();
        //print_r($data);die();
        $this->load->view('admin/tickets/add', $data);
    }

    public function get_contract($userid){

            $res =  $this->db->where('client',$userid)->get(db_prefix() . 'contracts')->result_array();
            echo json_encode($res);
    }

    public function delete($ticketid)
    {
        if (!has_permission('incidents', '', 'delete')) {
            access_denied('incidents');
        }
        if (!$ticketid) {
            redirect(admin_url('tickets'));
        }

        $response = $this->tickets_model->delete($ticketid);

        if ($response == true) {
            set_alert('success', _l('deleted', _l('ticket')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('ticket_lowercase')));
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'tickets/ticket') !== false) {
            redirect(admin_url('tickets'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function approve($ticketid)
    {
        //print_r($ticketid);die;
        if (!$ticketid) {
            redirect(admin_url('tickets'));
        }
       
        
        if($this->session->userdata('staff_user_id')){
            $data = array('is_approved' => 1,
                      'approved_by' => $this->session->userdata('staff_user_id'));

            $response = $this->db->set($data)
                  ->where('ticketid',$ticketid)
                  ->update(db_prefix() . 'tickets');
        }
        if ($response == true) {
            set_alert('success', 'Approved', _l('ticket'));
        } else {
            set_alert('warning', 'Problem approving', _l('ticket_lowercase'));
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'tickets/ticket') !== false) {
            redirect(admin_url('tickets'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function reject($ticketid)
    {
        //print_r($ticketid);die;
        if (!$ticketid) {
            redirect(admin_url('tickets'));
        }

        $response = $this->db->set('is_approved',0)
                  ->where('ticketid',$ticketid)
                  ->update(db_prefix() . 'tickets');

        if ($response == true) {
            set_alert('success', 'Reject', _l('ticket'));
        } else {
            set_alert('warning', 'Problem approving', _l('ticket_lowercase'));
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'tickets/ticket') !== false) {
            redirect(admin_url('tickets'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function convert_to_pdf($ticketid){

            //$data = $this->db->where('ticketid',$ticketid)->select('*')->get('tbltickets')->row();

            $data = $this->db->select('tbltickets.ticketpattern, tbltickets.ticketid, tbltickets.email, tbltickets.name, tbltickets.subject as ticket_subject,tbltickets_status.name as statusname, tbltickets.billing_type,tblclients.company,tblservices.name as servicename,tblcontacts.firstname, tblcontacts.lastname, tblcontacts.email,tbltickets.date,tblcontracts.subject,tblstaff.firstname as staff_firstname, tblstaff.lastname as staff_lastname')
                             ->from('tbltickets')
                             ->join('tblclients','tbltickets.userid = tblclients.userid','left')
                             ->join('tblservices','tbltickets.service = tblservices.serviceid','left')
                             ->join('tblcontacts','tbltickets.contactid = tblcontacts.id','left')
                             ->join('tblcontracts','tbltickets.contract = tblcontracts.id','left')
                             ->join('tblstaff','tbltickets.assigned = tblstaff.staffid','left')
                             ->join('tbltickets_status','tbltickets.status = tbltickets_status.ticketstatusid','left')
                             ->where('ticketid',$ticketid)->get()->row();
            //print_r($data);die();

            $resolvedTime = $this->db->where(array('ticketid' => $ticketid, 'status' => 3))
                                     ->select('datetime')->get('tblticket_status_logs')->row();

            $resolve_time = '';
            if(isset($resolvedTime))  {
                $resolve_time = $resolvedTime->datetime;
            } 

            $worklogs = $this->db->where('rel_id',$ticketid)->where('rel_type','ticket')->get('tbltasks')->result_array();

       
            $timeSheets= [];

            foreach ($worklogs as $worklog) {

                $response = $this->tasks_model->get($worklog['id']);

                foreach ($response->timesheets as $timesheet) {
                    $timesheet['full_name'] =  $timesheet['full_name'];
                    $timesheet['name'] = $worklog['name'];
                    $timesheet['dateadded'] = $worklog['dateadded'];
                    $timesheet['datefinished'] = $worklog['datefinished'];
                    $timesheet['hourly_rate'] = $worklog['hourly_rate'];
                    $timesheet['billed'] = $worklog['billed'];
                    $timeSheets[] = $timesheet;
                }
            }

            //print_r($response);die();
 
            $billingType = '';
            $name = '';
            $resolved ='';
            $requested = '';

            $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
             
            // set document information
            $tcpdf->SetCreator(PDF_CREATOR);
            $tcpdf->SetAuthor('');
            
            $tcpdf->SetTitle('Support Request Report');
            //$tcpdf->SetSubject('TCPDF Tutorial');
            $tcpdf->SetKeywords('TCPDF, PDF, example, test, guide');
             
            //set default header information
             
            $image = admin_url('uploads/company/c1eecc062fa266ee3fc8f8c7436312cf.png');

            // print_r($image);die()
             $tcpdf->SetHeaderData($image, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
            $tcpdf->setFooterData(array(0,65,0), array(0,65,127));
             
            //set header  textual styles
            $tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //set footer textual styles
            $tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
             
            //set default monospaced textual style
            $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
             
            // set default margins
            $tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            // Set Header Margin
            $tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            // Set Footer Margin
            $tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
             
            // set auto for page breaks
            $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
             
            // set image for scale factor
            $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
             
            // it is optional :: set some language-dependent strings
            if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
            {
            // optional
            require_once(dirname(__FILE__).'/lang/eng.php');
            // optional
            $tcpdf->setLanguageArray($l);
            }
             
            // set default font for subsetting mode
            $tcpdf->setFontSubsetting(true);
             
            // Set textual style
            // dejavusans is an UTF-8 Unicode textual style, on the off chance that you just need to
            // print standard ASCII roasts, you can utilize center text styles like
            // helvetica or times to lessen record estimate.
            $tcpdf->SetFont('calibri', '', 12, '', true);
             
            // Add a new page
            // This technique has a few choices, check the source code documentation for more data.
            $tcpdf->AddPage();
             
            // set text shadow for effect
            $tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,197,198), 'opacity'=>1, 'blend_mode'=>'Normal'));
             
            //Set some substance to print

            $set_html = '';
           

            //$set_html .= '<style>'.file_get_contents(base_url('assets/css/stylesheet.css')).'</style>';
            ////$imagesDecoding = base_url('assets/images/DecodingImage1.png');
            ////$imagesPowerMyIt = base_url('assets/images/powerMyItLogo.png');
             
            $set_html .= <<<EOD
             
            <table style="width:100%">
              <tr>
               <td><img style="height:50px;" src="assets/images/DecodingImage1.png"></td>
                <td></td>
                <td><img style="height:50px;" src="assets/images/powerMyItLogo.png"></td>
              </tr>
            </table>
            <br><br>


            <table style="width:100%;border:1px solid #ead1bf;padding-top:5px;padding-bottom:5px;">
              <tr>
                <td style="text-align:center;font-size:24px;font-weight:bold"><h3>Support Request Report</h3></td>
              </tr>
            </table>
            <br><br>

            <table style="width:100%;">
              <tr>
                <td style="font:19px;font-family:Calibri">Subject : $data->ticket_subject</td>
              </tr>
            </table>
            <br><br>

            <table style="width:100%;background-color:#262761;padding:10px;color:white;text-align:center;">
              <tr>
                <td style="font:16px 'Calibri'">INCIDENT $data->ticketpattern$data->ticketid</td>
              </tr>
            </table>
            <br><br>

            <table style="width:100%;font-size:10px;padding:10px;">
              <tr style="padding:2px;">
                <th style="color: #567087">REQUESTOR</th>
                <th style="color: #567087">TICKET DETAILS</th>
                <th style="color: #567087">BILLING</th>
              </tr>
              <hr>
              <tr style="padding:2px;">
                  <td style="font-size:13px">Name: $data->name</td>
                  <td style="font-size:13px">Service: $data->servicename</td>
                  <td style="font-size:13px">Billing: $data->billing_type</td>
              </tr>
              <tr>
                  <td style="font-size:13px">Company: $data->company</td>
                  <td style="font-size:13px">Created Time: $data->date</td>
                  <td style="font-size:13px">Contract: $data->subject</td>
              </tr>
              <tr>
                  <td style="font-size:13px">Email Address: $data->email</td>
                  <td style="font-size:13px">Resolved Time: $resolve_time</td>
                  <td style="font-size:13px"></td>
              </tr>
              <tr>
                  <td style="font-size:13px"></td>
                  <td style="font-size:13px">Resolved By: $data->staff_firstname&nbsp;$data->staff_lastname</td>
                  <td style="font-size:13px"> </td>
              </tr>
            </table>
            <br><br>


              <br><br>  <br><br>  <br><br>

            
            EOD;
           
            //Print content utilizing writeHTMLCell()
            $tcpdf->writeHTMLCell(0, 0, '', '', $set_html, 0, 1, 0, true, '', true);

        //if(true){
         // if(count($timeSheets) > 0){
             $html = '

             <table>';

             $html .='
                    <table style="width:100%;background-color:#262761;padding:10px;color:white;">
                      <tr>
                        <td style="font-size:13px;">Task No</td>
                        <td style="font-size:13px;">Notes</td>
                        <td style="font-size:13px;">Done By</td>
                        <td style="font-size:13px;">From Time - To Time</td>
                        <td style="font-size:13px;">Total Time</td>
                      </tr>
                    </table>';

            $totaTime = 0;
            $totalCost = 0;
            foreach($timeSheets as $key => $worklog) {
                $totaTime += $worklog['time_spent'];
                $totalCost += $worklog['hourly_rate'];
                $html .= '
               <table style="width:100%;font-size:12px;padding:10px;">
                    <tr style="text-align:left;font-size:10px;font-family:Calibri">
                        <td>' . $worklog['task_id'] . '</td>
                        <td>' . $worklog['note'] . '</td>
                         <td>' . $worklog['full_name'] . '</td>
                        <td>' . strftime('%Y-%m-%d %H:%M:%S', $worklog['start_time']) .' - '.strftime('%Y-%m-%d %H:%M:%S', $worklog['end_time']). '</td>
                        <td>' . _l('time_h').': '.seconds_to_time_format($worklog['time_spent']) . '</td>
                    </tr>
                </table><hr>';
            }

              $html .= '
               
                <p style="text-align:right">'.' <b>Total Time Taken </b>'._l('(h)').': '.seconds_to_time_format($totaTime).'</p>

                <p>Ticket Status : '.$data->statusname.'<br>Thank you<br>'.$data->staff_firstname.' '.$data->staff_lastname.'<br>Decoding IT Solutions</p>
                ';


            $html .= '</table>';



            $tcpdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        //}
              
            // Close and yield PDF record
            // This technique has a few choices, check the source code documentation for more data.
            $tcpdf->Output('SupportRequest.pdf', 'I');
            // successfully created CodeIgniter TCPDF Integration
            
    }



    public function delete_attachment($id)
    {
        if (is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_ticket_attachments') == '1')) {
            if (get_option('staff_access_only_assigned_departments') == 1 && !is_admin()) {
                $attachment = $this->tickets_model->get_ticket_attachment($id);
                $ticket     = $this->tickets_model->get_ticket_by_id($attachment->ticketid);

                $this->load->model('departments_model');
                $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                if (!in_array($ticket->department, $staff_departments)) {
                    set_alert('danger', _l('ticket_access_by_department_denied'));
                    redirect(admin_url('access_denied'));
                }
            }

            $this->tickets_model->delete_ticket_attachment($id);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function ticket($id)
    {
        if (!has_permission('incidents', '', 'edit')) {
            access_denied('incidents');
        }
        if (!$id) {
            redirect(admin_url('tickets/add'));
        }

        $data['ticket'] = $this->tickets_model->get_ticket_by_id($id);

        
        if (!$data['ticket']) {
            blank_page(_l('ticket_not_found'));
        }

        if (get_option('staff_access_only_assigned_departments') == 1) {
            if (!is_admin()) {
                $this->load->model('departments_model');
                $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                if (!in_array($data['ticket']->department, $staff_departments)) {
                    set_alert('danger', _l('ticket_access_by_department_denied'));
                    redirect(admin_url('access_denied'));
                }
            }
        }
        // print_r();die();
        if ($this->input->post()) {
            $returnToTicketList = false;
            $data               = $this->input->post();
            //print_r($data);die;
           

            if (isset($data['ticket_add_response_and_back_to_list'])) {
                $returnToTicketList = true;
                unset($data['ticket_add_response_and_back_to_list']);
            }

            $data['message'] = html_purify($this->input->post('message', false));
            $replyid         = $this->tickets_model->add_reply($data, $id, get_staff_user_id());

            if ($replyid) {
                set_alert('success', _l('replied_to_ticket_successfully', $id));
            }
            if (!$returnToTicketList) {
                redirect(admin_url('tickets/ticket/' . $id));
            } else {
                set_ticket_open(0, $id);
                redirect(admin_url('tickets'));
            }
        }
        // Load necessary models
        $this->load->model('knowledge_base_model');
        $this->load->model('departments_model');
        $this->load->model('contracts_model');

        $data['statuses']                       = $this->tickets_model->get_ticket_status();
        $data['statuses']['callback_translate'] = 'ticket_status_translate';
        $data['contracts']          = $this->db->where('client',$data['ticket']->userid)
                                                ->get('tblcontracts')->result_array();
        $data['company_contacts']   = $this->clients_model->get_contacts($data['ticket']->userid);
        $data['company_staffs']     = $this->staff_model->get();
        $data['departments']        = $this->departments_model->get();
        $data['predefined_replies'] = $this->tickets_model->get_predefined_reply();
        $data['priorities']         = $this->tickets_model->get_priority();
        $data['services']           = $this->tickets_model->get_service();
        $whereStaff                 = [];
        if (get_option('access_tickets_to_none_staff_members') == 0) {
            $whereStaff['is_not_staff'] = 0;
        }
        $data['staff']                = $this->staff_model->get('', $whereStaff);
        $data['articles']             = $this->knowledge_base_model->get();
        $data['ticket_replies']       = $this->tickets_model->get_ticket_replies($id);
        //print_r($data);die();
        $data['bodyclass']            = 'top-tabs ticket single-ticket';
        $data['title']                = $data['ticket']->subject;
        $data['ticket']->ticket_notes = $this->misc_model->get_notes($id, 'ticket');
        
        $data['ticket_replies_mail']      = $this->db->like('subject',$id)
                                                 ->like('subject','Ticket')
                                                 ->get(db_prefix().'mail_inbox')
                                                 ->result_array() ;

        //print_r($data['ticket']->userid);die();
        $data['teams'] = $this->db->select('staff.staffid,teams.customer_id,teams.staffid,teams.role,staff.firstname,staff.lastname,staff.email,staff.phonenumber,teams.id')
                               ->from('tblteams')
                               ->join('tblstaff','tblstaff.staffid = tblteams.staffid','inner')
                               ->where('tblteams.customer_id',$data['ticket']->userid)
                               ->get()->result_array(); 


        $data['notes'] = $this->db->where('rel_id', $id)->where('rel_type','ticket')->get('tblnotes')->result_array(); 

        $res =  explode (",", $data['ticket']->additional_charges);
        if(isset($data['ticket']->additional_charges)){
            $data['addtional_charges_select'] = $this->db->query('SELECT * FROM tbladditional_service_charges where ID in '.'('.$data['ticket']->additional_charges.')')->result_array();
        }
        
        add_admin_tickets_js_assets();
        //print_r($data);die; 
        $this->load->view('admin/tickets/single', $data);
    }

    public function edit_message()
    {
        if ($this->input->post()) {
            $data         = $this->input->post();
            $data['data'] = html_purify($this->input->post('data', false));

            if ($data['type'] == 'reply') {
                $this->db->where('id', $data['id']);
                $this->db->update(db_prefix().'ticket_replies', [
                    'message' => $data['data'],
                ]);
            } elseif ($data['type'] == 'ticket') {
                $this->db->where('ticketid', $data['id']);
                $this->db->update(db_prefix().'tickets', [
                    'message' => $data['data'],
                ]);
            }
            if ($this->db->affected_rows() > 0) {
                set_alert('success', _l('ticket_message_updated_successfully'));
            }
            redirect(admin_url('tickets/ticket/' . $data['main_ticket']));
        }
    }

    public function before_closing_request(){
       
        $data = $this->input->post();
        $user_acceptance = (isset($data['user_acceptance']))?1:0 ;
        $worklog = (isset($data['worklog']))?1:0 ;

        $ticketId = $data['ticketid'];
        
        if($user_acceptance == 1 && $worklog == 1) {
            if($ticketId){
                $data = array(
                    'user_acceptance' => $user_acceptance,
                    'worklog' => $worklog
                     );
                $this->db->set('status',3)->where('ticketid', $ticketId)
                         ->update(db_prefix().'tickets', $data);   

                $this->db->where('ticketid', $ticketId);
                $this->db->insert(db_prefix() . 'ticket_status_logs', 
                    ['ticketid' => $ticketId,'status' => 3,'datetime' => date('Y-m-d H:i:s'),'updatetime' => date('Y-m-d H:i:s')]); 

            }
        }
         redirect(admin_url('tickets/ticket/').$ticketId.'?tab=settings');
    }

    public function before_close_status()
    {
        $data = $this->input->post();
        $attach_service_report = (isset($data['attach_service_report']))?1:0 ;
        $ticketId = $data['ticketid'];

        if($attach_service_report == 1) 
        {
            $data = array('attach_service_report' => $attach_service_report);
        }
        else
        {
            $data = array('attach_service_report' => 0);
        }
        
        $this->db->set('status',5)->where('ticketid', $ticketId)
                 ->update(db_prefix().'tickets', $data);   

        $this->db->where('ticketid', $ticketId);
        $this->db->insert(db_prefix() . 'ticket_status_logs', 
            ['ticketid' => $ticketId,'status' => 5,'datetime' => date('Y-m-d H:i:s'),'updatetime' => date('Y-m-d H:i:s')]);  

        redirect(admin_url('tickets/ticket/').$ticketId);
    }

    public function delete_ticket_reply($ticket_id, $reply_id)
    {
        if (!$reply_id) {
            redirect(admin_url('tickets'));
        }
        $response = $this->tickets_model->delete_ticket_reply($ticket_id, $reply_id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('ticket_reply')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('ticket_reply')));
        }
        redirect(admin_url('tickets/ticket/' . $ticket_id));
    }

    public function change_status_ajax($id, $status)
    {
        if ($this->input->is_ajax_request()) {
            //print_r($this->input->is_ajax_request());die();
            echo json_encode($this->tickets_model->change_ticket_status($id, $status));
        }

        //redirect($this->uri->uri_string());
    }

    public function update_single_ticket_settings()
    {
        if ($this->input->post()) {
            
            $data = $this->input->post();
            unset($data['DataTables_Table_0_length']);
            unset($data['DataTables_Table_1_length']);
            // print_r($data);die();
            if(isset($data['additional_charge'])){
                $data['additional_charges'] = implode(',', $data['additional_charge']);
                unset($data['additional_charge']);
            }
            
            $this->session->mark_as_flash('active_tab');
            $this->session->mark_as_flash('active_tab_settings');
            $success = $this->tickets_model->update_single_ticket_settings($data);
            if ($success) {
                $this->session->set_flashdata('active_tab', true);
                $this->session->set_flashdata('active_tab_settings', true);
                if (get_option('staff_access_only_assigned_departments') == 1) {
                    $ticket = $this->tickets_model->get_ticket_by_id($this->input->post('ticketid'));
                    $this->load->model('departments_model');
                    $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                    if (!in_array($ticket->department, $staff_departments) && !is_admin()) {
                        set_alert('success', _l('ticket_settings_updated_successfully_and_reassigned', $ticket->department_name));
                        echo json_encode([
                            'success'               => $success,
                            'department_reassigned' => true,
                        ]);
                        die();
                    }
                }
                set_alert('success', _l('ticket_settings_updated_successfully'));
            }
            echo json_encode([
                'success' => $success,
            ]);
            die();
        }
    }

    // Priorities
    /* Get all ticket priorities */
    public function priorities()
    {
        if (!is_admin()) {
            access_denied('Ticket Priorities');
        }
        $data['priorities'] = $this->tickets_model->get_priority();
        $data['title']      = _l('ticket_priorities');
        $this->load->view('admin/tickets/priorities/manage', $data);
    }

    /* Add new priority od update existing*/
    public function priority()
    {
        if (!is_admin()) {
            access_denied('Ticket Priorities');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->tickets_model->add_priority($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('ticket_priority')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->tickets_model->update_priority($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('ticket_priority')));
                }
            }
            die;
        }
    }

    /* Delete ticket priority */
    public function delete_priority($id)
    {
        if (!is_admin()) {
            access_denied('Ticket Priorities');
        }
        if (!$id) {
            redirect(admin_url('tickets/priorities'));
        }
        $response = $this->tickets_model->delete_priority($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('ticket_priority_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('ticket_priority')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('ticket_priority_lowercase')));
        }
        redirect(admin_url('tickets/priorities'));
    }

    // Deliveries
    /* Get all ticket deliveries */
    public function deliveries()
    {
        if (!is_admin()) {
            access_denied('Ticket Deliveries');
        }
        $data['deliveries'] = $this->tickets_model->get_delivery();
        $data['title']      = _l('Ticket Deliveries');
        $this->load->view('admin/tickets/deliveries/manage', $data);
    }

    /* Add new delivery od update existing*/
    public function delivery()
    {
        if (!is_admin()) {
            access_denied('Ticket Deliveries');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->tickets_model->add_delivery($this->input->post());
                if ($id) {
                    set_alert('success', _l('Added Successfully', _l('ticket_delivery')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->tickets_model->update_delivery($data, $id);
                if ($success) {
                    set_alert('success', _l('Updated uccessfully', _l('ticket_delivery')));
                }
            }
            die;
        }
    }

    /* Delete ticket delivery */
    public function delete_delivery($id)
    {
        if (!is_admin()) {
            access_denied('Ticket Deliveries');
        }
        if (!$id) {
            redirect(admin_url('tickets/deliveries'));
        }
        $response = $this->tickets_model->delete_delivery($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('ticket_delivery_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('Deleted', _l('ticket_delivery')));
        } else {
            set_alert('warning', _l('Problem deleting', _l('ticket_delivery_lowercase')));
        }
        redirect(admin_url('tickets/deliveries'));
    }

    // Requests
    /* Get all ticket requests */
    public function requests()
    {
        if (!is_admin()) {
            access_denied('Ticket Requests');
        }
        $data['requests'] = $this->tickets_model->get_request_for();
        $data['title']      = _l('Ticket Requests');
        $this->load->view('admin/tickets/requests/manage', $data);
    }

    /* Add new request_for od update existing*/
    public function request_for()
    {
        if (!is_admin()) {
            access_denied('Ticket Requests');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->tickets_model->add_request_for($this->input->post());
                if ($id) {
                    set_alert('success', _l('Added Successfully', _l('ticket_request_for')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->tickets_model->update_request_for($data, $id);
                if ($success) {
                    set_alert('success', _l('Updated uccessfully', _l('ticket_request_for')));
                }
            }
            die;
        }
    }

    /* Delete ticket request_for */
    public function delete_request_for($id)
    {
        if (!is_admin()) {
            access_denied('Ticket Requests');
        }
        if (!$id) {
            redirect(admin_url('tickets/requests'));
        }
        $response = $this->tickets_model->delete_request_for($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('ticket_request_for_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('Deleted', _l('ticket_request_for')));
        } else {
            set_alert('warning', _l('Problem deleting', _l('ticket_request_for_lowercase')));
        }
        redirect(admin_url('tickets/requests'));
    }


    /* List all ticket predefined replies */
    public function predefined_replies()
    {
        if (!is_admin()) {
            access_denied('Predefined Replies');
        }
        if ($this->input->is_ajax_request()) {
            $aColumns = [
                'name',
            ];
            $sIndexColumn = 'id';
            $sTable       = db_prefix().'tickets_predefined_replies';
            $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], [
                'id',
            ]);
            $output  = $result['output'];
            $rResult = $result['rResult'];
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    $_data = $aRow[$aColumns[$i]];
                    if ($aColumns[$i] == 'name') {
                        $_data = '<a href="' . admin_url('tickets/predefined_reply/' . $aRow['id']) . '">' . $_data . '</a>';
                    }
                    $row[] = $_data;
                }
                $options            = icon_btn('tickets/predefined_reply/' . $aRow['id'], 'pencil-square-o');
                $row[]              = $options .= icon_btn('tickets/delete_predefined_reply/' . $aRow['id'], 'remove', 'btn-danger _delete');
                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
        $data['title'] = _l('predefined_replies');
        //print_r($data);die();
        $this->load->view('admin/tickets/predefined_replies/manage', $data);
    }

    public function get_predefined_reply_ajax($id)
    {
        echo json_encode($this->tickets_model->get_predefined_reply($id));
    }

    public function ticket_change_data()
    {
        if ($this->input->is_ajax_request()) {
            $contact_id = $this->input->post('contact_id');
            //print_r($contact_id);die();
            $res = $this->db->where('id',$contact_id)->get(db_prefix() . 'contacts')->row();
            
            echo json_encode([
                'contact_data'          => $this->clients_model->get_contact($contact_id),
                'contract_data'         => $this->db->where('client',$res->userid)->get(db_prefix() . 'contracts')->result_array(),
                'company_data'          => $this->clients_model->get($res->userid),
                'company_contacts'      => $this->clients_model->get_contacts($res->userid),
                'company_staffs'        => $this->staff_model->get(),
                'addtional_charges_data'=> $this->db->select('tbladditional_service_charges.id,tbladditional_service_charges.chargetype,tbladditional_service_charges.rate,tbladditional_service_charges.currency,')
                                                    ->from('tbladditional_service_charges')
                                                    ->where('tbladditional_service_charges.customer_id',$res->userid)
                                                    ->get()->result_array(),
                'customer_has_projects' => customer_has_projects(get_user_id_by_contact_id($contact_id)),
                'customer_notes'        =>$this->db->where('rel_id',$res->userid)->get(db_prefix().'notes')->result_array()
            ]);
        }
    }

    /* Add new reply or edit existing */
    public function predefined_reply($id = '')
    {
        if (!is_admin() && get_option('staff_members_save_tickets_predefined_replies') == '0') {
            access_denied('Predefined Reply');
        }
        if ($this->input->post()) {
            $data              = $this->input->post();
            $data['message']   = html_purify($this->input->post('message', false));
            $ticketAreaRequest = isset($data['ticket_area']);

            if (isset($data['ticket_area'])) {
                unset($data['ticket_area']);
            }

            if ($id == '') {
                $id = $this->tickets_model->add_predefined_reply($data);
                if (!$ticketAreaRequest) {
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('predefined_reply')));
                        redirect(admin_url('tickets/predefined_reply/' . $id));
                    }
                } else {
                    echo json_encode(['success' => $id ? true : false, 'id' => $id]);
                    die;
                }
            } else {
                $success = $this->tickets_model->update_predefined_reply($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('predefined_reply')));
                }
                redirect(admin_url('tickets/predefined_reply/' . $id));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('predefined_reply_lowercase'));
        } else {
            $predefined_reply         = $this->tickets_model->get_predefined_reply($id);
            $data['predefined_reply'] = $predefined_reply;
            $title                    = _l('edit', _l('predefined_reply_lowercase')) . ' ' . $predefined_reply->name;
        }
        $data['title'] = $title;
        $this->load->view('admin/tickets/predefined_replies/reply', $data);
    }

    /* Delete ticket reply from database */
    public function delete_predefined_reply($id)
    {
        if (!is_admin()) {
            access_denied('Delete Predefined Reply');
        }
        if (!$id) {
            redirect(admin_url('tickets/predefined_replies'));
        }
        $response = $this->tickets_model->delete_predefined_reply($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('predefined_reply')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('predefined_reply_lowercase')));
        }
        redirect(admin_url('tickets/predefined_replies'));
    }

    // Ticket statuses
    /* Get all ticket statuses */
    public function statuses()
    {
        if (!is_admin()) {
            access_denied('Ticket Statuses');
        }
        $data['statuses'] = $this->tickets_model->get_ticket_status();
        $data['title']    = 'Ticket statuses';
        // print_r($data);die();
        $this->load->view('admin/tickets/tickets_statuses/manage', $data);
    }

    /* Add new or edit existing status */
    public function status()
    {
        if (!is_admin()) {
            access_denied('Ticket Statuses');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->tickets_model->add_ticket_status($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('ticket_status')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->tickets_model->update_ticket_status($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('ticket_status')));
                }
            }
            die;
        }
    }

    /* Delete ticket status from database */
    public function delete_ticket_status($id)
    {
        if (!is_admin()) {
            access_denied('Ticket Statuses');
        }
        if (!$id) {
            redirect(admin_url('tickets/statuses'));
        }
        $response = $this->tickets_model->delete_ticket_status($id);
        if (is_array($response) && isset($response['default'])) {
            set_alert('warning', _l('cant_delete_default', _l('ticket_status_lowercase')));
        } elseif (is_array($response) && isset($response['referenced'])) {
            set_alert('danger', _l('is_referenced', _l('ticket_status_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('ticket_status')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('ticket_status_lowercase')));
        }
        redirect(admin_url('tickets/statuses'));
    }

    /* List all ticket services */
    public function services()
    {
        if (!is_admin()) {
            access_denied('Ticket Services');
        }
        if ($this->input->is_ajax_request()) {
            $aColumns = [
                'name',
            ];
            $sIndexColumn = 'serviceid';
            $sTable       = db_prefix().'services';
            $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], [
                'serviceid',
            ]);
            $output  = $result['output'];
            $rResult = $result['rResult'];
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    $_data = $aRow[$aColumns[$i]];
                    if ($aColumns[$i] == 'name') {
                        $_data = '<a href="#" onclick="edit_service(this,' . $aRow['serviceid'] . ');return false" data-name="' . $aRow['name'] . '">' . $_data . '</a>';
                    }
                    $row[] = $_data;
                }
                $options = icon_btn('#', 'pencil-square-o', 'btn-default', [
                    'data-name' => $aRow['name'],
                    'onclick'   => 'edit_service(this,' . $aRow['serviceid'] . '); return false;',
                ]);
                $row[]              = $options .= icon_btn('tickets/delete_service/' . $aRow['serviceid'], 'remove', 'btn-danger _delete');
                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
        $data['title'] = _l('services');
        $this->load->view('admin/tickets/services/manage', $data);
    }

    /* Add new service od delete existing one */
    public function service($id = '')
    {
        if (!is_admin() && get_option('staff_members_save_tickets_predefined_replies') == '0') {
            access_denied('Ticket Services');
        }

        if ($this->input->post()) {
            $post_data = $this->input->post();
            if (!$this->input->post('id')) {
                $requestFromTicketArea = isset($post_data['ticket_area']);
                if (isset($post_data['ticket_area'])) {
                    unset($post_data['ticket_area']);
                }
                $id = $this->tickets_model->add_service($post_data);
                if (!$requestFromTicketArea) {
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('service')));
                    }
                } else {
                    echo json_encode(['success' => $id ? true : false, 'id' => $id, 'name' => $post_data['name']]);
                }
            } else {
                $id = $post_data['id'];
                unset($post_data['id']);
                $success = $this->tickets_model->update_service($post_data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('service')));
                }
            }
            die;
        }
    }

    /* Delete ticket service from database */
    public function delete_service($id)
    {
        if (!is_admin()) {
            access_denied('Ticket Services');
        }
        if (!$id) {
            redirect(admin_url('tickets/services'));
        }
        $response = $this->tickets_model->delete_service($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('service_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('service')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('service_lowercase')));
        }
        redirect(admin_url('tickets/services'));
    }

    public function block_sender()
    {
        if ($this->input->post()) {
            $this->load->model('spam_filters_model');
            $sender  = $this->input->post('sender');
            $success = $this->spam_filters_model->add(['type' => 'sender', 'value' => $sender], 'tickets');
            if ($success) {
                set_alert('success', _l('sender_blocked_successfully'));
            }
        }
    }

    public function bulk_action()
    {
        hooks()->do_action('before_do_bulk_action_for_tickets');
        if ($this->input->post()) {
            $total_deleted = 0;
            $ids           = $this->input->post('ids');
            $status        = $this->input->post('status');
            $department    = $this->input->post('department');
            $service       = $this->input->post('service');
            $priority      = $this->input->post('priority');
            $tags          = $this->input->post('tags');
            $is_admin      = is_admin();
            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($this->input->post('mass_delete')) {
                        if ($is_admin) {
                            if ($this->tickets_model->delete($id)) {
                                $total_deleted++;
                            }
                        }
                    } else {
                        if ($status) {
                            $this->db->where('ticketid', $id);
                            $this->db->update(db_prefix().'tickets', [
                                'status' => $status,
                            ]);
                        }
                        if ($department) {
                            $this->db->where('ticketid', $id);
                            $this->db->update(db_prefix().'tickets', [
                                'department' => $department,
                            ]);
                        }
                        if ($priority) {
                            $this->db->where('ticketid', $id);
                            $this->db->update(db_prefix().'tickets', [
                                'priority' => $priority,
                            ]);
                        }

                        if ($service) {
                            $this->db->where('ticketid', $id);
                            $this->db->update(db_prefix().'tickets', [
                                'service' => $service,
                            ]);
                        }
                        if ($tags) {
                            handle_tags_save($tags, $id, 'ticket');
                        }
                    }
                }
            }

            if ($this->input->post('mass_delete')) {
                set_alert('success', _l('total_tickets_deleted', $total_deleted));
            }
        }
    }
}
