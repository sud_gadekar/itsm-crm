<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Service_requests extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_service_requests_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        // $this->load->model('service_requests_model');
        $this->load->model('service_requests_model');
        $this->load->model('clients_model');
    }
    public function index($status = '', $userid = '')
    {
        if (!has_permission('service_requests', '', 'view') && !has_permission('service_requests', '', 'view_own')) {
            access_denied('service_requests');
        }
        close_setup_menu();
        if (!is_numeric($status))
        {
            $status = '';
        }
        // print_r($this->input);die;
        if ($this->input->is_ajax_request())
        {
            if (!$this->input->post('filters_service_request_id'))
            {
                $tableParams = ['status' => $status, 'userid' => $userid, ];
            }
            else
            {
                // request for othes service_requests when single service_request is opened
                $tableParams = ['userid' => $this->input->post('filters_userid'), 'where_not_service_request_id' => $this->input->post('filters_service_request_id'), ];
                if ($tableParams['userid'] == 0)
                {
                    unset($tableParams['userid']);
                    $tableParams['by_email'] = $this->input->post('filters_email');
                }
            }
            //echo $tableParams;die;
            $this->app->get_table_data('service_requests', $tableParams);
        }

        $this->load->model('tickets_model');
        $data['chosen_service_request_status'] = $status;
        $data['weekly_service_requests_opening_statistics'] = json_encode($this->service_requests_model->get_weekly_service_requests_opening_statistics());
        $data['title'] = _l('support_service_requests');
        $this->load->model('departments_model');
        $data['statuses'] = $this->service_requests_model->get_service_request_status();
        $data['staff_deparments_ids'] = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
        $data['departments'] = $this->departments_model->get();
        $data['priorities'] = $this->service_requests_model->get_priority();
        $data['deliveries'] = $this->tickets_model->get_delivery();
        $data['requests'] = $this->tickets_model->get_request_for();
        $data['services'] = $this->service_requests_model->get_service();
        $data['service_request_assignees'] = $this->service_requests_model->get_service_requests_assignes_disctinct();

        $data['clients']              = $this->db->select('tblclients.userid, tblclients.company')
                                                 ->get(db_prefix().'clients')->result_array();

        $data['bodyclass'] = 'service_requests-page';
        add_admin_service_requests_js_assets();
        $data['default_service_requests_list_statuses'] = hooks()->apply_filters('default_service_requests_list_statuses', [1, 2, 4]);
        $this->load->view('admin/service_requests/list', $data);
    }

    public function add($userid = false)
    {
        if (!has_permission('service_requests', '', 'view')) {
            access_denied('service_requests');
        }
        if ($this->input->post())
        {
            if (!has_permission('service_requests', '', 'create')) {
                access_denied('service_requests');
            }
            $data = $this->input->post();
            unset($data['cntid']);

            if(isset($data['additional_charge'])){
                $data['additional_charges'] = implode(',', $data['additional_charge']);
                unset($data['additional_charge']);
            }
            //print_r($data);die;
            $data['message'] = html_purify($this->input->post('message', false));
            if($data['mailid']){

                 $this->db->where('id', $data['mailid']);
                 $this->db->update(db_prefix() . 'mail_inbox', array('converted_sr' => 1));
            }
            unset($data['mailid']);
            $id = $this->service_requests_model->add($data, get_staff_user_id());
            if ($id)
            {
                set_alert('success', _l('new_service_request_added_successfully', $id));
                redirect(admin_url('service_requests/service_request/' . $id));
            }
        }
        $data['cntid']='';
        $data['userid']='';
        $data['subject'] = '';
        $data['sender_name'] = '';
        $data['from_email'] = '';
        $data['body'] = '';
        $data['service_requestContact'] = false;
        $data['contactid'] = '';
        $data['domain_name'] = '';
        $data['flag'] = '';
        $data['mailid'] = '';
        $data['company'] = '';
        if (isset($_GET['id']))
        {
            $data['mailid'] = $_GET['id'];
            $output = $this->db->where('id', $_GET['id']);
            $output = $this->db->get(db_prefix() . 'mail_inbox')->row();
            $data['subject'] = $output->subject;
            $data['sender_name'] = $output->sender_name;

            $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
            preg_match_all($pattern, $output->from_email, $matches);
            
            $data['from_email'] = $matches[0][0];

            $data['domain_name'] = substr(strrchr($data['from_email'], "@"), 1);
            if ($data['from_email'])
            {
                //$domain_response = $this->db->like('website', $data['domain_name'], 'before');
                //$domain_response = $this->db->get(db_prefix() . 'clients')->row();.
                $res = $this->db->where('email',$data['from_email']);
                $res = $this->db->get(db_prefix() . 'contacts')->row();
                if($res){
                    //print_r($res);die();
                    $data['contactid'] = $res->id;
                    //$data['cntid'] = $res->id;
                    $data['from_email'] = $res->email;
                    $data['sender_name'] = $res->firstname.' '.$res->lastname ;
                    if($res->userid !=0 ){
                        $data['company'] = $this->db->where('userid',$res->userid)->select('company')->get('tblclients')->row()->company;    
                    }
                    
                }

                if (!empty($res->userid))
                {
                    $data['flag'] = 1;
                    $data['userid'] = $res->userid;
                }
                else {
                    $data['flag'] = 2;
                }
            }
            $data['body'] = $output->body;
        }
        if ($userid !== false)
        {
            $data['userid'] = $userid;
            $data['client'] = $this->clients_model->get($userid);
        }
        // Load necessary models
        $this->load->model('knowledge_base_model');
        $this->load->model('departments_model');
        $this->load->model('tickets_model');
        $data['departments'] = $this->departments_model->get();
        $data['predefined_replies'] = $this->service_requests_model->get_predefined_reply();
        $data['priorities'] = $this->service_requests_model->get_priority();
        $data['deliveries'] = $this->tickets_model->get_delivery();
        $data['requests'] = $this->tickets_model->get_request_for();
        $data['services'] = $this->service_requests_model->get_service();

        
        $whereStaff = [];
        if (get_option('access_service_requests_to_none_staff_members') == 0)
        {
            $whereStaff['is_not_staff'] = 0;
        }
        $data['staff'] = $this->staff_model->get('', $whereStaff);
        $data['articles'] = $this->knowledge_base_model->get();
        $data['bodyclass'] = 'service_request';
        $data['title'] = _l('new_service_request');
        if ($this->input->get('project_id') && $this->input->get('project_id') > 0)
        {
            // request from project area to create new service_request
            $data['project_id'] = $this->input->get('project_id');
            $data['userid'] = get_client_id_by_project_id($data['project_id']);
            if (total_rows(db_prefix() . 'contacts', ['active' => 1, 'userid' => $data['userid']]) == 1)
            {
                $contact = $this->clients_model->get_contacts($data['userid']);
                if (isset($contact[0]))
                {
                    $data['contact'] = $contact[0];
                }
            }
        }
        elseif ($this->input->get('contact_id') && $this->input->get('contact_id') > 0 && $this->input->get('userid'))
        {
            $contact_id = $this->input->get('contact_id');
            if (total_rows(db_prefix() . 'contacts', ['active' => 1, 'id' => $contact_id]) == 1)
            {
                $contact = $this->clients_model->get_contact($contact_id);
                if ($contact)
                {
                    $data['contact'] = (array)$contact;
                }
            }
        }
        add_admin_service_requests_js_assets();
        //print_r($data);die();
        $this->load->view('admin/service_requests/add', $data);
    }

        public function approve($service_requestid)
    {
        //print_r($service_requestid);die;
        if (!$service_requestid) {
            redirect(admin_url('service_requests'));
        }
       
        
        if($this->session->userdata('staff_user_id')){
            $data = array('is_approved' => 1,
                      'approved_by' => $this->session->userdata('staff_user_id'));

            $response = $this->db->set($data)
                  ->where('service_requestid',$service_requestid)
                  ->update(db_prefix() . 'service_requests');
        }
        if ($response == true) {
            set_alert('success', 'Approved', _l('service_request'));
        } else {
            set_alert('warning', 'Problem approving', _l('service_request_lowercase'));
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'service_requests/service_request') !== false) {
            redirect(admin_url('service_requests'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function reject($service_requestid)
    {
        //print_r($service_requestid);die;
        if (!$service_requestid) {
            redirect(admin_url('service_requests'));
        }

        $response = $this->db->set('is_approved',0)
                  ->where('service_requestid',$service_requestid)
                  ->update(db_prefix() . 'service_requests');

        if ($response == true) {
            set_alert('success', 'Reject', _l('service_request'));
        } else {
            set_alert('warning', 'Problem approving', _l('service_request_lowercase'));
        }

        if (strpos($_SERVER['HTTP_REFERER'], 'service_requests/service_request') !== false) {
            redirect(admin_url('service_requests'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function delete($service_requestid)
    {
        if (!has_permission('service_requests', '', 'delete')) {
            access_denied('service_requests');
        }
        // echo 'delete';
        // die;
        if (!$service_requestid)
        {
            redirect(admin_url('service_requests'));
        }
        $response = $this->service_requests_model->delete($service_requestid);
        if ($response == true)
        {
            set_alert('success', _l('deleted', _l('service_request')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('service_request_lowercase')));
        }
        if (strpos($_SERVER['HTTP_REFERER'], 'service_requests/service_request') !== false)
        {
            redirect(admin_url('service_requests'));
        }
        else
        {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }


     public function convert_to_pdf($service_requestid){

         //print_r($service_requestid);die();

            $data = $this->db->select('tblservice_requests.servicerequestpattern, tblservice_requests.service_requestid, tblservice_requests.email, tblservice_requests.name, tblservice_requests.subject as service_request_subject, 
                tblservice_requests_status.name as statusname, tblservice_requests.billing_type,tblclients.company,tblservices.name as servicename,tblcontacts.firstname, tblcontacts.lastname, tblcontacts.email,tblservice_requests.date,tblcontracts.subject,tblstaff.firstname as staff_firstname, tblstaff.lastname as staff_lastname')
                             ->from('tblservice_requests')
                             ->join('tblclients','tblservice_requests.userid = tblclients.userid','left')
                             ->join('tblservices','tblservice_requests.service = tblservices.serviceid','left')
                             ->join('tblcontacts','tblservice_requests.contactid = tblcontacts.id','left')
                             ->join('tblcontracts','tblservice_requests.contract = tblcontracts.id','left')
                             ->join('tblstaff','tblservice_requests.assigned = tblstaff.staffid','left')
                             ->join('tblservice_requests_status','tblservice_requests.status = tblservice_requests_status.service_requeststatusid','left')
                             ->where('service_requestid',$service_requestid)->get()->row();



            $resolvedTime = $this->db->where(array('service_requestid' => $service_requestid, 'status' => 3))
                                     ->select('datetime')->get('tblservice_request_status_logs')->row();
            $resolve_time = '';
            if(isset($resolvedTime))  {
                $resolve_time = $resolvedTime->datetime;
            }                       
            //print_r($resolvedTime);die();
            //print_r($this->db->last_query());die();

            //$staffName = $this->db->where('staffid',$data->assigned)
                                 // ->select('firstname,lastname')->get('tblstaff')
                                  //->row() ;          

            //$requestedBy = $this->db->where('id',$data->contactid)->select('firstname,lastname')
                                    //->get('tblcontacts')->row();   
                                    
            $worklogs = $this->db->where('rel_id',$service_requestid)->where('rel_type','service_request')->get('tbltasks')->result_array();


            $timeSheets= [];

            foreach ($worklogs as $worklog) {

                $response = $this->tasks_model->get($worklog['id']);

                foreach ($response->timesheets as $timesheet) {
                    $timesheet['full_name'] =  $timesheet['full_name'];
                    $timesheet['name'] = $worklog['name'];
                    $timesheet['dateadded'] = $worklog['dateadded'];
                    $timesheet['datefinished'] = $worklog['datefinished'];
                    $timesheet['hourly_rate'] = $worklog['hourly_rate'];
                    $timesheet['billed'] = $worklog['billed'];
                    $timeSheets[] = $timesheet;
                }
            }
                        

            $billingType = '';
            $name = '';
            $resolved ='';
            $requested = '';

            $tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
             
            // set document information
            $tcpdf->SetCreator(PDF_CREATOR);
            $tcpdf->SetAuthor('Muhammad Saqlain Arif');
            $tcpdf->SetTitle('SR Report');
            //$tcpdf->SetSubject('TCPDF Tutorial');
            $tcpdf->SetKeywords('TCPDF, PDF, example, test, guide');
             
            //set default header information


             
            //$tcpdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING);
            $tcpdf->setFooterData(array(0,65,0), array(0,65,127));
             
            //set header  textual styles
            $tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //set footer textual styles
            $tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
             
            //set default monospaced textual style
            $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
             
            // set default margins
            $tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            // Set Header Margin
            $tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            // Set Footer Margin
            $tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
             
            // set auto for page breaks
            $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
             
            // set image for scale factor
            $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
             
            // it is optional :: set some language-dependent strings
            if (@file_exists(dirname(__FILE__).'/lang/eng.php'))
            {
            // optional
            require_once(dirname(__FILE__).'/lang/eng.php');
            // optional
            $tcpdf->setLanguageArray($l);
            }
             
            // set default font for subsetting mode
            $tcpdf->setFontSubsetting(true);
             
            // Set textual style
            // dejavusans is an UTF-8 Unicode textual style, on the off chance that you just need to
            // print standard ASCII roasts, you can utilize center text styles like
            // helvetica or times to lessen record estimate.
            $tcpdf->SetFont('calibri', '', 12, '', true);
             
            // Add a new page
            // This technique has a few choices, check the source code documentation for more data.
            $tcpdf->AddPage();
             
            // set text shadow for effect
            $tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,197,198), 'opacity'=>1, 'blend_mode'=>'Normal'));
             
            //Set some substance to print
            ////$imagesDecoding = base_url('assets/images/DecodingImage1.png');
            ////$imagesPowerMyIt = base_url('assets/images/powerMyItLogo.png');

            $set_html = <<<EOD
             
                   <table style="width:100%">
                      <tr>
                        <td><img style="height:50px;" src="assets/images/DecodingImage1.png"></td>
                        <td></td>
                        <td><img style="height:50px;" src="assets/images/powerMyItLogo.png"></td>
                      </tr>
                    </table>
                    <br><br>


                    <table style="width:100%;border:1px solid #ead1bf;padding-top:5px;padding-bottom:5px;">
                      <tr>
                        <td style="text-align:center;font-size:24px;font-weight:bold;"><h3>Service Request Report</h3></td>
                      </tr>
                    </table>
                    <br><br>

                    <table style="width:100%;">
                      <tr>
                        <td style="font:19px;font-family:Calibri">Subject : $data->service_request_subject</td>
                      </tr>
                    </table>
                    <br><br>

                    <table style="width:100%;background-color:#262761;padding:10px;color:white;text-align:center;">
                      <tr>
                        <td>Service Request $data->servicerequestpattern$data->service_requestid</td>
                      </tr>
                    </table>
                    <br><br>

                    

                    <table style="width:100%;font-size:10px;padding:10px;">
                      <tr style="padding:2px;">
                        <th style="color: #567087">REQUESTOR</th>
                        <th style="color: #567087">TICKET DETAILS</th>
                        <th style="color: #567087">BILLING</th>
                      </tr>
                      <hr>
                      <tr style="padding:2px;">
                          <td style="font-size:13px;">Name: $data->name</td>
                          <td style="font-size:13px;">Service: $data->servicename</td>
                          <td style="font-size:13px;">Billing: $data->billing_type</td>
                      </tr>
                      <tr>
                          <td style="font-size:13px;">Company: $data->company</td>
                          <td style="font-size:13px;">Created Time: $data->date</td>
                          <td style="font-size:13px;">Contract: $data->subject</td>
                      </tr>
                      <tr>
                          <td style="font-size:13px;">Email Address: $data->email</td>
                          <td style="font-size:13px;">Resolved Time: $resolve_time</td>
                          <td style="font-size:13px;"></td>
                      </tr>
                      <tr>
                          <td style="font-size:13px;"></td>
                          <td style="font-size:13px;">Resolved By: $data->staff_firstname&nbsp;$data->staff_lastname</td>
                          <td style="font-size:13px;"> </td>
                      </tr>
                    </table>
                    <br><br>


                      <br><br>  <br><br>  <br><br>
            EOD;

             //Print content utilizing writeHTMLCell()
        $tcpdf->writeHTMLCell(0, 0, '', '', $set_html, 0, 1, 0, true, '', true);

    


            $html = '

             <table>';

             $html .='
                    <table style="width:100%;background-color:#262761;padding:10px;color:white;">
                      <tr>
                        <td style="font-size:13px;">Task No</td>
                        <td style="font-size:13px;">Notes</td>
                        <td style="font-size:13px;">Done By</td>
                        <td style="font-size:13px;">From Time - To Time</td>
                        <td style="font-size:13px;">Time spent</td>
                      </tr>
                    </table>';

            $totaTime = 0;
            $totalCost = 0;
            foreach($timeSheets as $worklog) {
                $totaTime += $worklog['time_spent'];
                $totalCost += $worklog['hourly_rate'];
                $html .= '
               <table style="width:100%;font-size:12px;padding:10px;">
                    <tr style="text-align:left">
                        <td>' . $worklog['task_id'] . '</td>
                        <td>' . $worklog['note'] . '</td>
                        <td>' . $worklog['full_name'] . '</td>
                        <td>' . strftime('%Y-%m-%d %H:%M:%S', $worklog['start_time']) .' - '.strftime('%Y-%m-%d %H:%M:%S', $worklog['end_time']). '</td>
                        <td>' . _l('time_h').': '.seconds_to_time_format($worklog['time_spent']) . '</td>
                    </tr>
                </table><hr>';
            }

            $html .= '
               
                <p style="text-align:right">'.' <b>Total Time Taken </b>'._l('(h)').': '.seconds_to_time_format($totaTime).'</p>

                <p>Ticket Status : '.$data->statusname.'<br>Thank you<br>'.$data->staff_firstname.' '.$data->staff_lastname.'<br>Decoding IT Solutions</p>
                ';

            $html .= '</table>';

            $tcpdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        
            $tcpdf->Output('Service Request.pdf', 'I');
            // successfully created CodeIgniter TCPDF Integration

    }
    public function delete_attachment($id)
    {
        if (is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_service_request_attachments') == '1'))
        {
            if (get_option('staff_access_only_assigned_departments') == 1 && !is_admin())
            {
                $attachment = $this->service_requests_model->get_service_request_attachment($id);
                $service_request = $this->service_requests_model->get_service_request_by_id($attachment->service_requestid);
                $this->load->model('departments_model');
                $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                if (!in_array($service_request->department, $staff_departments))
                {
                    set_alert('danger', _l('service_request_access_by_department_denied'));
                    redirect(admin_url('access_denied'));
                }
            }
            $this->service_requests_model->delete_service_request_attachment($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function service_request($id)
    {
        if (!has_permission('service_requests', '', 'edit')) {
            access_denied('service_requests');
        }
        //print_r($id);die();
        if (!$id)
        {
            redirect(admin_url('service_request/add'));
        }
        $data['service_request'] = $this->service_requests_model->get_service_request_by_id($id);
        //print_r($data['service_request']->userid);die;
        if (!$data['service_request'])
        {

            blank_page(_l('service_request_not_found'));
        }
        if (get_option('staff_access_only_assigned_departments') == 1)
        {
            if (!is_admin())
            {
                $this->load->model('departments_model');
                $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                if (!in_array($data['service_request']->department, $staff_departments))
                {
                    // echo 'hidb';die;
                    set_alert('danger', _l('service_request_access_by_department_denied'));
                    redirect(admin_url('access_denied'));
                }
            }
        }
        if ($this->input->post())
        {
            $returnToservice_requestList = false;


            $data = $this->input->post();
            if (isset($data['service_request_add_response_and_back_to_list']))
            {
                $returnToservice_requestList = true;
                unset($data['service_request_add_response_and_back_to_list']);
            }
            $data['message'] = html_purify($this->input->post('message', false));
            $replyid = $this->service_requests_model->add_reply($data, $id, get_staff_user_id());
            if ($replyid)
            {
                set_alert('success', _l('replied_to_service_request_successfully', $id));
            }
            if (!$returnToservice_requestList)
            {
                redirect(admin_url('service_requests/service_request/' . $id));
            }
            else
            {
                set_service_request_open(0, $id);
                redirect(admin_url('service_requests'));
            }
        }
        // Load necessary models
        $this->load->model('knowledge_base_model');
        $this->load->model('departments_model');

        $data['contacts'] = $this->db->where('userid',$data['service_request']->userid)
                                     ->get(db_prefix().'contacts')->result_array();
       

        $this->load->model('tickets_model');
        $data['statuses'] = $this->service_requests_model->get_service_request_status();
        $data['statuses']['callback_translate'] = 'service_request_status_translate';

        $data['contracts'] = $this->db->where('client',$data['service_request']->userid)
                                                ->get('tblcontracts')->result_array();
        $data['company_contacts']   = $this->clients_model->get_contacts($data['service_request']->userid);
        $data['company_staffs']   = $this->staff_model->get();
        $data['departments'] = $this->departments_model->get();
        $data['predefined_replies'] = $this->service_requests_model->get_predefined_reply();
        $data['priorities'] = $this->service_requests_model->get_priority();
        $data['deliveries'] = $this->tickets_model->get_delivery();
        $data['requests'] = $this->tickets_model->get_request_for();
        $data['services'] = $this->service_requests_model->get_service();
        $whereStaff = [];
        if (get_option('access_service_requests_to_none_staff_members') == 0)
        {
            $whereStaff['is_not_staff'] = 0;
        }
        $data['staff'] = $this->staff_model->get('', $whereStaff);
        $data['articles'] = $this->knowledge_base_model->get();
        $data['service_request_replies'] = $this->service_requests_model->get_service_request_replies($id);
        $data['bodyclass'] = 'top-tabs service_request single-service_request';
        $data['title'] = $data['service_request']->subject;
        $data['service_request']->service_request_notes = $this->misc_model->get_notes($id, 'service_request');
        $data['service_request_replies_mail']      = $this->db->like('subject',$id)
                                                    ->like('subject','Service request')
                                                    ->get(db_prefix().'mail_inbox')
                                                    ->result_array() ;

        $data['teams'] = $this->db->select('staff.staffid,teams.customer_id,teams.staffid,teams.role,staff.firstname,staff.lastname,staff.email,staff.phonenumber,teams.id')
                            ->from('tblteams')
                            ->join('tblstaff','tblstaff.staffid = tblteams.staffid','inner')
                            ->where('tblteams.customer_id',$data['service_request']->userid)
                            ->get()->result_array(); 


        $data['notes'] = $this->db->where('rel_id', $id)->where('rel_type','service_request')->get('tblnotes')->result_array();
        //print_r($data);die;
        $res =  explode (",", $data['service_request']->additional_charges);
        if(isset($data['service_request']->additional_charges)){
            $data['addtional_charges_select'] = $this->db->query('SELECT * FROM tbladditional_service_charges where ID in '.'('.$data['service_request']->additional_charges.')')->result_array();
        }
        add_admin_service_requests_js_assets();
        //print_r($data);die();
        $this->load->view('admin/service_requests/single', $data);
    }
    public function edit_message()
    {
        if ($this->input->post())
        {
            $data = $this->input->post();
            $data['data'] = html_purify($this->input->post('data', false));
            if ($data['type'] == 'reply')
            {
                $this->db->where('id', $data['id']);
                $this->db->update(db_prefix() . 'service_request_replies', ['message' => $data['data'], ]);
            }
            elseif ($data['type'] == 'service_request')
            {
                $this->db->where('service_requestid', $data['id']);
                $this->db->update(db_prefix() . 'service_requests', ['message' => $data['data'], ]);
            }
            if ($this->db->affected_rows() > 0)
            {
                set_alert('success', _l('service_request_message_updated_successfully'));
            }
            redirect(admin_url('service_requests/service_request/' . $data['main_service_request']));
        }
    }
    public function delete_service_request_reply($service_request_id, $reply_id)
    {
        if (!$reply_id)
        {
            redirect(admin_url('service_requests'));
        }
        $response = $this->service_requests_model->delete_service_request_reply($service_request_id, $reply_id);
        if ($response == true)
        {
            set_alert('success', _l('deleted', _l('service_request_reply')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('service_request_reply')));
        }
        redirect(admin_url('service_requests/service_request/' . $service_request_id));
    }
    public function change_status_ajax($id, $status)
    {
        if ($this->input->is_ajax_request())
        {
            echo json_encode($this->service_requests_model->change_service_request_status($id, $status));
        }
    }

    public function before_closing_request(){
       
        $data = $this->input->post();

        $user_acceptance = (isset($data['user_acceptance']))?1:0 ;
        $worklog = (isset($data['worklog']))?1:0 ;

        $service_requestId = $data['service_requestid'];
        
        if($user_acceptance == 1 && $worklog == 1) {
            if($service_requestId){
                $data = array(
                    'user_acceptance' => $user_acceptance,
                    'worklog' => $worklog
                     );
                $this->db->set('status',3)->where('service_requestid', $service_requestId)
                                          ->update(db_prefix().'service_requests', $data); 

                $this->db->where('service_requestid', $service_requestId);
                $this->db->insert(db_prefix() . 'service_request_status_logs', 
                                                ['service_requestid' => $service_requestId,'status' => 3]);    
            }
        }
        redirect(admin_url('service_requests/service_request/').$service_requestId.'?tab=settings');
    }

    public function before_close_status()
    {
        $data = $this->input->post();
        $attach_service_report = (isset($data['attach_service_report']))?1:0 ;
        $service_requestId = $data['service_requestid'];
        
        if($attach_service_report == 1) 
        {
            $data = array('attach_service_report' => $attach_service_report);
        }
        else
        {
            $data = array('attach_service_report' => 0);
        }
        $this->db->set('status',5)->where('service_requestid', $service_requestId)
                                  ->update(db_prefix().'service_requests', $data);   

        $this->db->where('service_requestid', $service_requestId);
        $this->db->insert(db_prefix() . 'service_request_status_logs', 
                                        ['service_requestid' => $service_requestId,'status' => 5]);  

       redirect(admin_url('service_requests/service_request/').$service_requestId);
    }


    public function update_single_service_request_settings()
    {
        if ($this->input->post())
        {
            $data = $this->input->post();
            //print_r($this->input->post());die;
            unset($data['DataTables_Table_1_length']);
            unset($data['DataTables_Table_0_length']);
            if(isset($data['additional_charge'])){
                $data['additional_charges'] = implode(',', $data['additional_charge']);
                unset($data['additional_charge']);
            }
            $this->session->mark_as_flash('active_tab');
            $this->session->mark_as_flash('active_tab_settings');
            $success = $this->service_requests_model->update_single_service_request_settings( $data);
            if ($success)
            {
                $this->session->set_flashdata('active_tab', true);
                $this->session->set_flashdata('active_tab_settings', true);
                if (get_option('staff_access_only_assigned_departments') == 1)
                {
                    $service_request = $this->service_requests_model->get_service_request_by_id($this->input->post('service_requestid'));
                    $this->load->model('departments_model');
                    $staff_departments = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                    if (!in_array($service_request->department, $staff_departments) && !is_admin())
                    {
                        set_alert('success', _l('service_request_settings_updated_successfully_and_reassigned', $service_request->department_name));
                        echo json_encode(['success' => $success, 'department_reassigned' => true, ]);

                    }
                }
                set_alert('success', _l('service_request_settings_updated_successfully'));
            }
            echo json_encode(['success' => $success, ]);

        }
    }
    // Priorities
    /* Get all service_request priorities */
    public function priorities()
    {
        if (!is_admin())
        {
            access_denied('service_request Priorities');
        }
        $data['priorities'] = $this->service_requests_model->get_priority();
        $data['title'] = _l('service_request_priorities');
        $this->load->view('admin/service_requests/priorities/manage', $data);
    }
    /* Add new priority od update existing*/
    public function priority()
    {
        if (!is_admin())
        {
            access_denied('service_request Priorities');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->service_requests_model->add_priority($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('service_request_priority')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->service_requests_model->update_priority($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('service_request_priority')));
                }
            }

        }
    }
    /* Delete service_request priority */
    public function delete_priority($id)
    {
        if (!is_admin())
        {
            access_denied('service_request Priorities');
        }
        if (!$id)
        {
            redirect(admin_url('service_requests/priorities'));
        }
        $response = $this->service_requests_model->delete_priority($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('service_request_priority_lowercase')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('service_request_priority')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('service_request_priority_lowercase')));
        }
        redirect(admin_url('service_requests/priorities'));
    }
    /* List all service_request predefined replies */
    public function predefined_replies()
    {
        if (!is_admin())
        {
            access_denied('Predefined Replies');
        }
        if ($this->input->is_ajax_request())
        {
            $aColumns = ['name', ];
            $sIndexColumn = 'id';
            $sTable = db_prefix() . 'service_requests_predefined_replies';
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id', ]);
            $output = $result['output'];
            $rResult = $result['rResult'];
            // print_r($rResult);die();
            foreach ($rResult as $aRow)
            {
                $row = [];
                for ($i = 0;$i < count($aColumns);$i++)
                {
                    $_data = $aRow[$aColumns[$i]];
                    if ($aColumns[$i] == 'name')
                    {
                        $_data = '<a href="' . admin_url('service_requests/predefined_reply/' . $aRow['id']) . '">' . $_data . '</a>';
                    }
                    $row[] = $_data;
                }
                $options = icon_btn('service_requests/predefined_reply/' . $aRow['id'], 'pencil-square-o');
                $row[] = $options .= icon_btn('service_requests/delete_predefined_reply/' . $aRow['id'], 'remove', 'btn-danger _delete');
                $output['aaData'][] = $row;
            }
            echo json_encode($output);
            die();
        }
        $data['title'] = _l('predefined_replies');
        //print_r($data);die();
        $this->load->view('admin/service_requests/predefined_replies/manage', $data);
    }
    public function get_predefined_reply_ajax($id)
    {
        echo json_encode($this->service_requests_model->get_predefined_reply($id));
    }
    public function service_request_change_data()
    {
        if ($this->input->is_ajax_request())
        {
            $contact_id = $this->input->post('contact_id');
            $res = $this->db->where('id',$contact_id)->get(db_prefix() . 'contacts')->row();

            echo json_encode([
                'contact_data' => $this->clients_model->get_contact($contact_id), 
                'contract_data'         => $this->db->where('client',$res->userid)->get(db_prefix() . 'contracts')->result_array(),
                'company_data'          => $this->clients_model->get($res->userid),
                'company_contacts'      => $this->clients_model->get_contacts($res->userid),
                'company_staffs'        => $this->staff_model->get(),
                'addtional_charges_data'=> $this->db->select('tbladditional_service_charges.id,tbladditional_service_charges.chargetype,tbladditional_service_charges.rate,tbladditional_service_charges.currency,')
                                                    ->from('tbladditional_service_charges')
                                                    ->where('tbladditional_service_charges.customer_id',$res->userid)
                                                    ->get()->result_array(),
                'customer_has_projects' => customer_has_projects(get_user_id_by_contact_id($contact_id)), ]);
        }
    }
    /* Add new reply or edit existing */
    public function predefined_reply($id = '')
    {
        if (!is_admin() && get_option('staff_members_save_service_requests_predefined_replies') == '0')
        {
            access_denied('Predefined Reply');
        }
        if ($this->input->post())
        {
            $data = $this->input->post();
            $data['message'] = html_purify($this->input->post('message', false));
            $service_requestAreaRequest = isset($data['service_request_area']);
            if (isset($data['service_request_area']))
            {
                unset($data['service_request_area']);
            }
            if ($id == '')
            {
                $id = $this->service_requests_model->add_predefined_reply($data);
                if (!$service_requestAreaRequest)
                {
                    if ($id)
                    {
                        set_alert('success', _l('added_successfully', _l('predefined_reply')));
                        redirect(admin_url('service_requests/predefined_reply/' . $id));
                    }
                }
                else
                {
                    echo json_encode(['success' => $id ? true : false, 'id' => $id]);

                }
            }
            else
            {
                $success = $this->service_requests_model->update_predefined_reply($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('predefined_reply')));
                }
                redirect(admin_url('service_requests/predefined_reply/' . $id));
            }
        }
        if ($id == '')
        {
            $title = _l('add_new', _l('predefined_reply_lowercase'));
        }
        else
        {
            $predefined_reply = $this->service_requests_model->get_predefined_reply($id);
            $data['predefined_reply'] = $predefined_reply;
            $title = _l('edit', _l('predefined_reply_lowercase')) . ' ' . $predefined_reply->name;
        }
        $data['title'] = $title;
        //print_r($data);die();
        $this->load->view('admin/service_requests/predefined_replies/reply', $data);
    }
    /* Delete service_request reply from database */
    public function delete_predefined_reply($id)
    {
        if (!is_admin())
        {
            access_denied('Delete Predefined Reply');
        }
        if (!$id)
        {
            redirect(admin_url('service_requests/predefined_replies'));
        }
        $response = $this->service_requests_model->delete_predefined_reply($id);
        if ($response == true)
        {
            set_alert('success', _l('deleted', _l('predefined_reply')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('predefined_reply_lowercase')));
        }
        redirect(admin_url('service_requests/predefined_replies'));
    }
    // service_request statuses
    /* Get all service_request statuses */
    public function statuses()
    {
        if (!is_admin())
        {
            access_denied('service_request Statuses');
        }
        $data['statuses'] = $this->service_requests_model->get_service_request_status();
        $data['title'] = 'service_request statuses';
        $this->load->view('admin/service_requests/service_requests_statuses/manage', $data);
    }
    /* Add new or edit existing status */
    public function status()
    {
        if (!is_admin())
        {
            access_denied('service_request Statuses');
        }
        if ($this->input->post())
        {
            if (!$this->input->post('id'))
            {
                $id = $this->service_requests_model->add_service_request_status($this->input->post());
                if ($id)
                {
                    set_alert('success', _l('added_successfully', _l('service_request_status')));
                }
            }
            else
            {
                $data = $this->input->post();
                $id = $data['id'];
                unset($data['id']);
                $success = $this->service_requests_model->update_service_request_status($data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('service_request_status')));
                }
            }

        }
    }
    /* Delete service_request status from database */
    public function delete_service_request_status($id)
    {
        if (!is_admin())
        {
            access_denied('service_request Statuses');
        }
        if (!$id)
        {
            redirect(admin_url('service_requests/statuses'));
        }
        $response = $this->service_requests_model->delete_service_request_status($id);
        if (is_array($response) && isset($response['default']))
        {
            set_alert('warning', _l('cant_delete_default', _l('service_request_status_lowercase')));
        }
        elseif (is_array($response) && isset($response['referenced']))
        {
            set_alert('danger', _l('is_referenced', _l('service_request_status_lowercase')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('service_request_status')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('service_request_status_lowercase')));
        }
        redirect(admin_url('service_requests/statuses'));
    }
    /* List all service_request services */
    public function services()
    {
        if (!is_admin())
        {
            access_denied('service_request Services');
        }
        if ($this->input->is_ajax_request())
        {
            $aColumns = ['name', ];
            $sIndexColumn = 'serviceid';
            $sTable = db_prefix() . 'services';
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['serviceid', ]);
            $output = $result['output'];
            $rResult = $result['rResult'];
            foreach ($rResult as $aRow)
            {
                $row = [];
                for ($i = 0;$i < count($aColumns);$i++)
                {
                    $_data = $aRow[$aColumns[$i]];
                    if ($aColumns[$i] == 'name')
                    {
                        $_data = '<a href="#" onclick="edit_service(this,' . $aRow['serviceid'] . ');return false" data-name="' . $aRow['name'] . '">' . $_data . '</a>';
                    }
                    $row[] = $_data;
                }
                $options = icon_btn('#', 'pencil-square-o', 'btn-default', ['data-name' => $aRow['name'], 'onclick' => 'edit_service(this,' . $aRow['serviceid'] . '); return false;', ]);
                $row[] = $options .= icon_btn('service_requests/delete_service/' . $aRow['serviceid'], 'remove', 'btn-danger _delete');
                $output['aaData'][] = $row;
            }
            echo json_encode($output);

        }
        $data['title'] = _l('services');
        $this->load->view('admin/service_requests/services/manage', $data);
    }
    /* Add new service od delete existing one */
    public function service($id = '')
    {
        if (!is_admin() && get_option('staff_members_save_service_requests_predefined_replies') == '0')
        {
            access_denied('service_request Services');
        }
        if ($this->input->post())
        {
            $post_data = $this->input->post();
            if (!$this->input->post('id'))
            {
                $requestFromservice_requestArea = isset($post_data['service_request_area']);
                if (isset($post_data['service_request_area']))
                {
                    unset($post_data['service_request_area']);
                }
                $id = $this->service_requests_model->add_service($post_data);
                if (!$requestFromservice_requestArea)
                {
                    if ($id)
                    {
                        set_alert('success', _l('added_successfully', _l('service')));
                    }
                }
                else
                {
                    echo json_encode(['success' => $id ? true : false, 'id' => $id, 'name' => $post_data['name']]);
                }
            }
            else
            {
                $id = $post_data['id'];
                unset($post_data['id']);
                $success = $this->service_requests_model->update_service($post_data, $id);
                if ($success)
                {
                    set_alert('success', _l('updated_successfully', _l('service')));
                }
            }

        }
    }
    /* Delete service_request service from database */
    public function delete_service($id)
    {
        if (!is_admin())
        {
            access_denied('service_request Services');
        }
        if (!$id)
        {
            redirect(admin_url('service_requests/services'));
        }
        $response = $this->service_requests_model->delete_service($id);
        if (is_array($response) && isset($response['referenced']))
        {
            set_alert('warning', _l('is_referenced', _l('service_lowercase')));
        }
        elseif ($response == true)
        {
            set_alert('success', _l('deleted', _l('service')));
        }
        else
        {
            set_alert('warning', _l('problem_deleting', _l('service_lowercase')));
        }
        redirect(admin_url('service_requests/services'));
    }
    public function block_sender()
    {
        if ($this->input->post())
        {
            $this->load->model('spam_filters_model');
            $sender = $this->input->post('sender');
            $success = $this->spam_filters_model->add(['type' => 'sender', 'value' => $sender], 'service_requests');
            if ($success)
            {
                set_alert('success', _l('sender_blocked_successfully'));
            }
        }
    }
    public function bulk_action()
    {
        hooks()->do_action('before_do_bulk_action_for_service_requests');
        if ($this->input->post())
        {
            $total_deleted = 0;
            $ids = $this->input->post('ids');
            $status = $this->input->post('status');
            $department = $this->input->post('department');
            $service = $this->input->post('service');
            $priority = $this->input->post('priority');
            $tags = $this->input->post('tags');
            $is_admin = is_admin();
            if (is_array($ids))
            {
                foreach ($ids as $id)
                {
                    if ($this->input->post('mass_delete'))
                    {
                        if ($is_admin)
                        {
                            if ($this->service_requests_model->delete($id))
                            {
                                $total_deleted++;
                            }
                        }
                    }
                    else
                    {
                        if ($status)
                        {
                            $this->db->where('service_requestid', $id);
                            $this->db->update(db_prefix() . 'service_requests', ['status' => $status, ]);
                        }
                        if ($department)
                        {
                            $this->db->where('service_requestid', $id);
                            $this->db->update(db_prefix() . 'service_requests', ['department' => $department, ]);
                        }
                        if ($priority)
                        {
                            $this->db->where('service_requestid', $id);
                            $this->db->update(db_prefix() . 'service_requests', ['priority' => $priority, ]);
                        }
                        if ($service)
                        {
                            $this->db->where('service_requestid', $id);
                            $this->db->update(db_prefix() . 'service_requests', ['service' => $service, ]);
                        }
                        if ($tags)
                        {
                            handle_tags_save($tags, $id, 'service_request');
                        }
                    }
                }
            }
            if ($this->input->post('mass_delete'))
            {
                set_alert('success', _l('total_service_requests_deleted', $total_deleted));
            }
        }
    }
}

