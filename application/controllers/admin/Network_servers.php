<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Network_servers extends AdminController
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('network_server_model');
        $this->load->helper('url'); 
    }

    public function network()
    { 
        if (!has_permission('it_overview', '', 'view')) {
            access_denied('it_overview');
        }
        if($this->input->post())
        {
            $data['network_servers'] = $this->network_server_model->get($this->input->post('id'));
            $this->load->view('admin/network_servers/filter_network_table', $data);
        }
        else
        {
            $data['network_servers'] = $this->network_server_model->get();
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['inventory_types'] = $this->db->where('parent_id',0)->get('tblinventory_types')->result_array(); 
       
             $this->load->view('admin/network_servers/network', $data);
        }
    	
    }

    // public function server()
    // { 
    //     if (!has_permission('it_overview', '', 'view')) {
    //         access_denied('it_overview');
    //     }
    //     $where = 'server';
    //     $data['network_servers'] = $this->network_server_model->get($where);
    //     $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
    //     print_r($data);die();
    //      $this->load->view('admin/network_servers/server', $data);
    // }

    public function filter_customer_by_network()
    { 
        if (!has_permission('it_overview', '', 'view')) {
            access_denied('it_overview');
        }
        if($this->input->post())
        {   
            
            $data['network_servers'] = '';
            $customerData = $this->input->post();
            $data['network_servers'] = $this->network_server_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();

            //print_r($data);die();
            $this->load->view('admin/network_servers/filter_network_table', $data);
        }
    }

    public function filter_customer_by_server()
    { 
        if (!has_permission('it_overview', '', 'view')) {
            access_denied('it_overview');
        }
        if($this->input->post())
        {   
            $type = 'server';
            $data['network_servers'] = '';
            $customerData = $this->input->post();
            $data['network_servers'] = $this->network_server_model->get_by_customer($customerData['id'],$type);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $this->load->view('admin/network_servers/filter_server_table', $data);
        }
    }

    public function network_server($id = '')
    { 
        if (!has_permission('it_overview', '', 'view')) {
            access_denied('it_overview');
        }   
    	if ($this->input->post()) {
            if (is_numeric($id) == '') { 
                if (!has_permission('it_overview', '', 'create')) {
                    access_denied('it_overview');
                }
                $data = $this->input->post();
                $id = $this->network_server_model->add($data);
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('network / server')));
                    return redirect(admin_url('network_servers/network_server/'.$id.'#details'));
                }
            }else { 

                if (!has_permission('it_overview', '', 'edit')) {
                    access_denied('it_overview');
                }
                $data = $this->input->post();
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'networks', $data);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('network / server')));
                }
                return redirect(admin_url('network_servers/network_server/'.$id.'#details'));
            }
        }

        if(is_numeric($id)){
            if (!has_permission('it_overview', '', 'edit')) {
                    access_denied('it_overview');
                }
            // print_r($id);
            $data['network_server_edit_data']          = $this->network_server_model->get_data_by_id($id);
            $data['network_server_assets']             = $this->network_server_model->get_network_server_asset($id);
            $data['network_server_diagrams']           = $this->network_server_model->get_network_server_diagram($id);
            $data['network_server_urls']               = $this->network_server_model->get_network_server_url($id);
            $data['network_server_gaps']               = $this->network_server_model->get_network_server_gap($id);
            $data['network_servers_software_licenses'] = $this->network_server_model->get_network_server_software_license($id);



            $data['service_overviews'] = $this->db->where('service_overviewid',$id)
                                                  ->get('tblservice_overview_attachments')->result_array();

            $data['inventory_types'] = $this->db->where('parent_id',0)->get('tblinventory_types')->result_array();    

            $data['id'] = $id;

            // $type = 0;
            // if($data['network_server_edit_data']->type == "network")
            // {
            //     $type = 1;
            // }
            // else if($data['network_server_edit_data']->type == "server")
            // {
            //     $type = 2;   
            // }
            //print_r($data);die();

            if(isset($data['network_server_edit_data']->clientid))
            {
                $data['assets'] = $this->db->select('tblinventory_details.id,tblinventory_products.product_name,tblinventory_details.hostname, tblinventory_details.purpose')
                                    ->from('tblinventory_details')
                                    ->join('tblinventory_products','tblinventory_details.product_name=tblinventory_products.id','left')
                                    ->where('tblinventory_details.clientid',$data['network_server_edit_data']->clientid)
                                    ->get()->result_array();



                $data['diagrams']         = $this->network_server_model->get_all_diagram($data['network_server_edit_data']->clientid);
                $data['gaps']             = $this->network_server_model->get_all_gap($data['network_server_edit_data']->clientid);
                $data['software_licenses']= $this->network_server_model->get_all_software_license($data['network_server_edit_data']->clientid);
            }

            
            //print_r($data);die();
        }
        else{
            //We Get $id as as type i.e network or server 
            $data['title'] = "Network/Server"; //$id;
        }

        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['inventory_types'] = $this->db->where('parent_id',0)->get('tblinventory_types')->result_array(); 
        //print_r($data);die();
        $this->load->view('admin/network_servers/network_server', $data);
    }

    public function assets($network_server_id){ 
        if($this->input->post())
        {   
            $inventory_data = $this->input->post("inventory_id");
            $network_id = $this->input->post("network_id");

            $data = [];
            foreach ($inventory_data as $key => $value) {
                $data[] = [
                    'inventory_id' => $value,
                    'network_id' => $network_id
                ];
            }

            foreach ($data as $key => $inventory) 
            {
                $count = $this->db->where('inventory_id',$inventory['inventory_id'])
                              ->where('network_id',$network_server_id)
                              ->select('*')->from(db_prefix().'network_assets')
                              ->get()->num_rows();

                if($count <= 0 )
                {
                    $this->db->insert_batch(db_prefix().'network_assets',$data);
                    if($this->db->affected_rows() > 0)
                    {   
                        set_alert('success', _l('added_successfully', _l('network assets')));
                        redirect(admin_url('network_servers/network_server/' . $network_server_id.'#assets'));
                    }
                }
                else
                {
                    set_alert('warning', _l('network assets already added', _l('network assets')));
                    redirect(admin_url('network_servers/network_server/' . $network_server_id.'#assets'));
                } 
            }               
        }
    }

    public function diagrams($network_server_id){ 
        if($this->input->post()){
            $data = $this->input->post();
            $count = $this->db->where('diagram_id',$data['diagram_id'])->where('network_id',$network_server_id)
                              ->select('*')->from(db_prefix().'network_diagrams')
                              ->get()->num_rows();

            if($count <= 0)
            {
                $id = $this->db->insert(db_prefix().'network_diagrams',$data);
                if($id)
                {   
                    set_alert('success', _l('added_successfully', _l('network diagrams')));
                    redirect(admin_url('network_servers/network_server/' . $network_server_id.'#diagrams'));
                }
            }
            else
            {
                set_alert('warning', _l('network diagram already added', _l('network diagram')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#diagrams'));
            } 
        }
    }

    public function urls($network_server_id){ 
        if($this->input->post()){
            $data = $this->input->post();
                $id = $this->db->insert(db_prefix().'network_urls',$data);
                if($id)
                {
                    set_alert('success', _l('added_successfully', _l('network urls')));
                    redirect(admin_url('network_servers/network_server/' . $network_server_id.'#urls'));
                }
        }
    }

    public function gaps($network_server_id){ 
        if($this->input->post()){
            $data = $this->input->post();
            //print_r($data);die();
            $count = $this->db->where('gap_id',$data['gap_id'])->where('network_id',$network_server_id)
                              ->select('*')->from(db_prefix().'network_gaps')
                              ->get()->num_rows();
            if($count <=0)
            {
                $id = $this->db->insert(db_prefix().'network_gaps',$data);
                if($id)
                {
                    set_alert('success', _l('added_successfully', _l('network gaps')));
                    redirect(admin_url('network_servers/network_server/' . $network_server_id.'#gaps'));
                }
            }
            else
            {
                set_alert('warning', _l('network gap already added', _l('network gap')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#gaps'));
            }     
        }
    }


    public function software_licenses($network_server_id)
    {
        if($this->input->post())
        {
            $data = $this->input->post();
            //print_r($data);die();

            $count = $this->db->where('renewal_id',$data['renewal_id'])->where('network_id',$network_server_id)
                              ->select('*')->from(db_prefix().'network_software_licenses')
                              ->get()->num_rows();

            if($count <=0)
            {
                $id = $this->db->insert(db_prefix().'network_software_licenses',$data);
                if($id)
                {
                    set_alert('success', _l('added_successfully', _l('Software and license')));
                    redirect(admin_url('network_servers/network_server/' . $network_server_id.'#software_licenses'));
                }
            }
            else
            {
                set_alert('warning', _l('Software and license already added', _l('Software and license')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#software_licenses'));
            } 
        }
    }


    public function service_overview()
    {
        $data = [];
        $count = count($_FILES['files']['name']);
        if($this->input->post())
        {
            
            $postData = $this->input->post();
            $network_server_id = $postData['overview_id'];
            $summary = $postData['summary'];
        }

        

        //print_r($count);die();
        for($i=0;$i<$count;$i++){

            if(!empty($_FILES['files']['name'][$i])){
     
                  $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                  $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                  $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                  $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                  $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                if (!file_exists('./uploads/network_servers/'.$network_server_id)) {
                    mkdir('./uploads/network_servers/'.$network_server_id, 777, true);
                    chmod('./uploads/network_servers/'.$network_server_id, 0755);
                 }

                  $config['upload_path'] = "./uploads/network_servers/".$network_server_id; 
                  $config['allowed_types'] = '*';
                  $config['max_size'] = '20000'; // max_size in kb
                  //$config['file_name'] = $_FILES['files']['name'][$i];
          
                  $this->load->library('upload',$config); 
         
                    if($this->upload->do_upload('file')){

                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
                        $data['totalFiles'][] = $filename;
                        continue;
                    }
            }
            else
            {

                $ql = $this->db->select('service_overviewid')->from('tblservice_overview_attachments')
                           ->where('service_overviewid',$network_server_id)->get();

                if( $ql->num_rows() > 0 ) 
                {
                    $this->db->where('service_overviewid',$network_server_id)
                         ->update('tblservice_overview_attachments',array('summary' => $summary ));
                } 
                else
                { 
                    $this->db->insert('tblservice_overview_attachments', array('summary' => $summary, 'service_overviewid' =>  $network_server_id));
                   
                }
            }
        }
        foreach ($data['totalFiles'] as $key => $value) 
        {
                $res = $this->db->insert('tblservice_overview_attachments', 
                    array('service_overviewid' => $network_server_id, 
                          'summary' => $summary,
                          'file_name' => $value,
                         ));
                

        }
       
       if($res)
       {
        set_alert('success', _l('Files added successfully', _l('')));
       }
       redirect(admin_url('network_servers/network_server/' . $network_server_id.'#service_overview'));
    }

    public function network_server_asset_delete($id, $network_server_id)
    {
        if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetwork_assets');
            if($result)
            {
                set_alert('success', _l('deleted successfully', _l('network assets')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#assets'));
            }
        }
    }

    public function network_server_diagram_delete($id, $network_server_id)
    {
         if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetwork_diagrams');
            if($result)
            {
                set_alert('success', _l('deleted successfully', _l('network diagrams')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#diagrams'));
            }
        }
    }

    public function network_server_url_delete($id, $network_server_id)
    {
         if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetwork_urls');
            if($result)
            {
                set_alert('success', _l('deleted successfully', _l('network url')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#urls'));
            }
        }
    }

    public function network_server_gap_delete($id, $network_server_id)
    {
        if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetwork_gaps');
            if($result)
            {
                set_alert('success', _l('deleted successfully', _l('network gap')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#gaps'));
            }
        }
    }
    public function network_server_software_license_delete($id, $network_server_id)
    {
        if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetwork_software_licenses');
            if($result)
            {
                set_alert('success', _l('deleted successfully', _l('software and licenses')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#software_licenses'));
            }
        }
    }

    public function delete_service_overview_file($id, $network_server_id)
    {
        if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblservice_overview_attachments');
            if($result)
            {
                set_alert('success', _l('Service overview file deleted successfully', _l('')));
                redirect(admin_url('network_servers/network_server/' . $network_server_id.'#service_overview'));
            }
        }
    }

    public function delete_it_overview($id)
    { 
        if (!has_permission('it_overview', '', 'delete')) {
            access_denied('it_overview');
        }
        if($id)
        {
            $result = $this->db->where('id',$id)->delete('tblnetworks');
            if($result)
            {
                set_alert('success', _l('IT overview deleted successfully', _l('')));
                redirect(admin_url('network_servers/network'));
            }
            else
            {
                set_alert('success', _l('Problem deleting IT overview', _l('')));
                redirect(admin_url('network_servers/network'));
            }
        }
    }

}
