<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Checklists extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        if (get_option('access_checklist_to_none_staff_members') == 0 && !is_staff_member())
        {
            redirect(admin_url());
        }
        $this->load->model('checklists_model');
    }

    public function index()
    {
        if (!has_permission('checklist', '', 'view')) {
            access_denied('checklist');
        }
    	$data['checklists_lists'] = $this->checklists_model->get();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->get()->result_array();
        //print_r($data);die();
        $data['title']      = 'checklist';
    	$this->load->view('admin/checklists/manage', $data);

    }

    public function filter_by_customer()
    {
       if($this->input->post())
        {   $data['checklists_lists'] = '';
            $customerData = $this->input->post();
            $data['checklists_lists'] = $this->checklists_model->get_by_customer($customerData['id']);
            $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id','left')
                ->get()->result_array();
                // print_r($data);die();
            $this->load->view('admin/checklists/filter_table', $data);
        }
    }

    public function health_checklist($id = '')
    {
        if (!has_permission('checklist', '', 'view')) {
            access_denied('checklist');
        }
    	if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('checklist', '', 'create')) {
                    access_denied('checklist');
                }
                $data = $this->input->post();
                unset($data['inventory_ids']);
                $data['inventory'] = implode(',',$data['inventory']);
                $id = $this->checklists_model->add($data);
		        if ($id) {
		            set_alert('success', _l('added_successfully', _l('Health checklist')));
		            return redirect(admin_url('checklists'));
		        }
            }
            else 
            {  
                if (!has_permission('checklist', '', 'edit')) {
                    access_denied('checklist');
                }
                $data = $this->input->post();
                unset($data['inventory_ids']);
                $data['inventory'] = implode(',',$data['inventory']);
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'checklist', $data);
		        if ($success) {
		            set_alert('success', _l('updated_successfully', _l('Health checklist')));
		        }
		        return redirect(admin_url('checklists'));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('checklist'));
        } else {
            $data['checklists_details'] = $this->db->where('id',$id)->get(db_prefix().'checklist')->row();
            $title = _l('edit', _l('checklist'));
        }

        $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->get()->result_array();
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['bodyclass']  = 'checklists';
        $data['title']      = $title;
        // print_r($data);die();
        $this->load->view('admin/checklists/health_checklist', $data);
    }

    //Add Provisional log Of checklists

    public function checklist_logs($id = '')
    {
        if (!has_permission('checklist', '', 'view')) {
            access_denied('checklist');
        }
        $data['checklist_logs'] = $this->checklists_model->get_log($id);
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblchecklist_status')->result_array();
        $data['checklists'] = $this->db->select('tblchecklist.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                           ->from(db_prefix() . 'checklist')
                           ->join(db_prefix() . 'inventory_details','tblchecklist.inventory = tblinventory_details.id')
                           ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                           ->get()->result_array();
        $data['title'] = _l('Health Checklist Logs');
        //print_r($data);die();
        $this->load->view('admin/checklists/checklist_logs/checklist_log', $data);
    }

    public function manage_logs($date='', $id='')
    {
        if (!has_permission('checklist', '', 'view')) {
            access_denied('checklist');
        }
        $data['checklist_logs'] = $this->checklists_model->get_log_details($date, $id);
        $data['clients'] = $this->db->select('*')->get('tblclients')->result_array();
        $data['statuses'] = $this->db->select('*')->get('tblchecklist_status')->result_array();
        $data['checklists'] = $this->db->select('tblchecklist.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                           ->from(db_prefix() . 'checklist')
                           ->join(db_prefix() . 'inventory_details','tblchecklist.inventory = tblinventory_details.id')
                           ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                           ->get()->result_array();
        $data['title'] = _l('Health Checklist Logs');
        //print_r($data);die();
        $this->load->view('admin/checklists/checklist_logs/manage', $data);
    }
    
    public function log()
    {
        if (!has_permission('checklist', '', 'view')) {
            access_denied('checklist');
        }
    	if($this->input->post()){
            $data = $this->input->post();
            $new_data = array();
            unset($data['client_id']);
            if(empty($data['id'])){
                if (!has_permission('checklist', '', 'create')) {
                    access_denied('checklist');
                }

                for($i=0; $i<$data['inventory_cnt']; $i++){
                    $new_data['checklist_id'] = $data['checklist_id'];
                    $new_data['inventory'] = $data['inventory'.$i];
                    $new_data['log_date'] = $data['log_date'];
                    $new_data['checklist_status'] = $data['checklist_status'.$i];
                    $new_data['remark'] = $data['remark'.$i];
                    $id = $this->db->insert('tblchecklist_logs',$new_data);
                }
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('checklist log')));    //return redirect(
                }
            }
            else{
                if (!has_permission('checklist', '', 'edit')) {
                    access_denied('checklist');
                }
                unset($data['policy']);
                unset($data['inventory_product']);
                $id = $this->db->where('id',$data['id'])->update(db_prefix().'checklist_logs',$data);
                if ($id) {
                    set_alert('success', _l('updated_successfully', _l('checklist log')));    //return redirect(
                }
            } 
            redirect(admin_url('checklists/checklist_logs'));      
        } 
    }


    public function checklist_log_data($id)
    {
        if($id)
        {   
            $data['logs'] = $this->db->select('tblchecklist_logs.*, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name, tblchecklist_status.name, tblchecklist_status.statuscolor')
            ->from(db_prefix() . 'checklist_logs')
            ->join(db_prefix() . 'checklist','tblchecklist_logs.checklist_id = tblchecklist.id')
            ->join(db_prefix() . 'checklist_status','tblchecklist_logs.checklist_status = tblchecklist_status.statusid')
            ->join(db_prefix() . 'inventory_details','tblchecklist_logs.inventory = tblinventory_details.id')
            ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
            ->where('checklist_id', $id)->get()->result_array();

            $log_table = "<table class='table table-striped table-bordered'>";
            $log_table .= "<thead><th>Sr.No.</th><th>Date</th><th>Inventory Product</th><th>Status</th><th>Remark</th></thead>";
            $log_table .= "<tbody>";
            foreach($data['logs'] as $key => $log){
                $log_table .= "<tr><td>";
                $log_table .= $key+1;
                $log_table .= "</td><td>".date('d-m-Y',strtotime($log['log_date']));
                $log_table .= "</td><td>".$log['product_name']." (".$log['hostname']." | ".$log['purpose'].")";
                $log_table .= "</td><td style='background: ".$log['statuscolor'].";'>".$log['name'];
                $log_table .= "</td><td>".$log['remark'];
                $log_table .= "</td></tr>";
            }
            $log_table .= "</tbody></table>";

            echo $log_table;
            //print_r($data['logs']);
        }
    }

    public function get_checklist_inventories($id)
    {
        if($id)
        {   
            $data['inventory'] = $this->db->select('inventory')->from('tblchecklist')->where('id', $id)->get()->row();
            $inventorys = explode(',',$data['inventory']->inventory);
            $statuses = $this->db->select('*')->get('tblchecklist_status')->result_array();
            $checklist_status='';
            foreach($statuses as $status){
                $checklist_status .= '<option value="'.$status['statusid'].'" style="color: '.$status['statuscolor'].'">'.$status['name'].'</option>';
            }
            
            $log_table = "<table class='table table-striped table-bordered'>";
            $log_table .= "<thead><th>Sr.No.</th><th>Inventory Product</th><th>Status</th><th>Remark</th></thead>";
            $log_table .= "<tbody>";

            foreach($inventorys as $key=>$inventory){
                $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('tblinventory_details.id', $inventory)->get()->row();

                $log_table .= "<tr><td>";
                $log_table .= $key+1;
                $log_table .= "</td><td>".$data['inventory']->product_name." (".$data['inventory']->hostname."|".$data['inventory']->purpose.") <input type='hidden' name='inventory".$key."' id='inventory".$key."' value='".$data['inventory']->id."'>";
                $log_table .= "</td><td><div class='form-group'>
                <select class='' name='checklist_status".$key."' id='checklist_status".$key."' data-width='100%'>
                    <option value='' selected=''> Select </option>".$checklist_status."
                </select></div>";
                $log_table .= "</td><td><input class='form-group' type='text' name='remark".$key."' id='remark".$key."' value='' style='width: 350px;'>";
                $log_table .= "</td></tr>";
            }
            $log_table .= "</tbody></table><input type='hidden' name='inventory_cnt' id='inventory_cnt' value='".count($inventorys)."'>";

            echo $log_table;
        }
    }

    public function get_inventories($id)
    {
        if($id)
        {   
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose,tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('clientid', $id)->get()->result_array();

            echo json_encode($data['inventory']);
        }
    }

    public function get_client_inventories($id)
    {
        if($id)
        {   
            $data['inventory'] = $this->db->select('tblinventory_details.id, tblinventory_details.hostname, tblinventory_details.purpose, tblinventory_products.product_name')
                ->from(db_prefix() . 'inventory_details')
                ->join(db_prefix() . 'inventory_products','tblinventory_details.product_name = tblinventory_products.id')
                ->where('backup_required',1)->where('clientid', $id)->get()->result_array();

            echo json_encode($data['inventory']);
        }
    }

    public function client_checklists($id)
    {
        if($id)
        {   
            $data['policies'] = $this->db->select('id, checklist_title')
            ->from(db_prefix() . 'checklist')
            ->where('client_id', $id)->get()->result_array();

            echo json_encode($data['policies']);
        }
    }


    public function delete($id)
    {
        if (!has_permission('checklist', '', 'delete')) {
            access_denied('checklist');
        }
        if (!$id) {
            redirect(admin_url('checklist'));
        }
        $response = $this->db->where('id',$id)->delete(db_prefix().'checklist');
        if ($response === true) 
        {
            set_alert('success', _l('deleted', _l('checklist')));
            redirect(admin_url('checklist'));
        } else 
        {  
            set_alert('warning', _l('problem_deleting', _l('checklist')));
            redirect(admin_url('checklist'));
        }
    }


    // Checklist statuses
    /* Get all checklist statuses */
    public function statuses()
    {
        if (!is_admin()) {
            access_denied('Checklist Statuses');
        }
        $data['statuses'] = $this->checklists_model->get_checklist_status();
        $data['title']    = 'Checklist statuses';
        // print_r($data);die();
        $this->load->view('admin/checklists/checklist_statuses/manage', $data);
    }

    /* Add new or edit existing status */
    public function status()
    {
        if (!is_admin()) {
            access_denied('Checklist Statuses');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->checklists_model->add_checklist_status($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('Checklist Status')));
                }
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->checklists_model->update_checklist_status($data, $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('Checklist Status')));
                }
            }
            die;
        }
    }

    /* Delete checklist status from database */
    public function delete_checklist_status($id)
    {
        if (!is_admin()) {
            access_denied('Checklist Statuses');
        }
        if (!$id) {
            redirect(admin_url('checklists/statuses'));
        }
        $response = $this->checklists_model->delete_checklist_status($id);
        if (is_array($response) && isset($response['default'])) {
            set_alert('warning', _l('cant_delete_default', _l('checklist_status_lowercase')));
        } elseif (is_array($response) && isset($response['referenced'])) {
            set_alert('danger', _l('is_referenced', _l('checklist_status_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('checklist_status')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('checklist_status_lowercase')));
        }
        redirect(admin_url('checklists/statuses'));
    }
}

