<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Team_availiabilities extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('team_availiabilities_model');
        $this->load->model('staff_model');
        $this->load->model('projects_model');
    }

    public function index()
    {
        // if (!has_permission('diagrams', '', 'view')) {
        //     access_denied('diagrams');
        // }
        if (is_staff_logged_in() && !is_admin()) {
            $data['team_availiables'] = $this->team_availiabilities_model->get(get_staff_user_id());
             //print_r($data);die();
        } 
        else{
            $data['team_availiables'] = $this->team_availiabilities_model->get();
            //print_r($data);die();
        }

        $this->load->view('admin/team_availiable/manage', $data);
        
    }

    //Add And Edit Team Avaliablity
    public function  team_availiable($id = false) {

        if ($this->input->post()) {
            if ($id == '') {
                // if (!has_permission('', '', '')) {
                //     access_denied('');
                // }

                $data = $this->input->post();

                $count = $this->db->where('staff_id',$data['staff_id'])
                                ->select('*')->from(db_prefix().'team_availiable')
                                ->get()->num_rows();
                if($count <= 0) {
                    $id = $this->team_availiabilities_model->add($data);
                    if ($id) {
                        log_activity('New Staff Availiablity  Added [' . $id . ']');
                        set_alert('success', _l('added_successfully', _l('Staff availablity')));
                        return redirect(admin_url('team_availiabilities'));
                    }
                } else {
                    set_alert('warning', _l('Staff availablity already added', _l('')));
                    return redirect(admin_url('team_availiabilities'));
                }
                
            }else {
                // if (!has_permission('', '', '')) {
                //     access_denied('');
                // }
                //print_r($this->input->post());die();    
                $data = $this->input->post();
                $success = $this->db->where('id',$data['id'])->update(db_prefix().'team_availiable', $data);

                if ($success) {
                    log_activity('Staff Availiablity update [' . $data['id'] . ']');
                    set_alert('success', _l('updated_successfully', _l('')));
                }
                return redirect(admin_url('team_availiabilities'));
            }
        }
        if ($id == '') {
            $title = "Add Team Availiablity";
        } else {
            $data['team_availiables'] = $this->db->where('id',$id)->get(db_prefix().'team_availiable')->row();
            $title = "Edit Team Availiablity";

            //print_r($data);die();
        }
        $data['staffs']           = $this->staff_model->get();
        $data['projects']         = $this->projects_model->get();
        $data['bodyclass']        = 'team_availiabilities';
        $data['title']            = $title;
        
        $this->load->view('admin/team_availiable/add', $data);

    }

    public function get_ticket_subject() {
        if($this->input->post('table_name') == "ticket") {
            
            $ticket_subject = $this->db->select('ticketpattern as pattern, ticketid as id, subject')
                                       ->get('tbltickets')->result_array();


            echo json_encode($ticket_subject);
            
        } 
        if ($this->input->post('table_name') == "service_request") {
            $ticket_subject = $this->db->select('servicerequestpattern as pattern, service_requestid as id, subject')
                                       ->get('tblservice_requests')->result_array();

            echo json_encode($ticket_subject);
            
        }

    }

    public function delete($team_avaliablity_id)
    {
        if (!has_permission('', '', '')) {
            access_denied('');
        }

        if($team_avaliablity_id)
        {
            $res = $this->db->where('id',$team_avaliablity_id)->delete('tblteam_availiable');
            if($res)
            {
                set_alert('success', _l('deleted', _l('Staff Availiablity')));
            }
            else
            {
                set_alert('warning', _l('problem_deleting', _l('Staff Availiablity')));
            }
            redirect(admin_url('team_availiabilities'));
        }
    }

   
}

