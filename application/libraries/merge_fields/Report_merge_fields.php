<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Reports Type',
                    'key'       => '{report_type}',
                    'available' => [
                        'report',
                    ],
                ],
                
            ];
    }

    /**
 * Merge fields for renewals
 * @param  string $template  template name, used to identify url
 * @param  mixed $renewal_id renewal id
 * @param  mixed $reply_id  reply id
 * @return array
 */
    public function format($template, $pdf_name, $reply_id = '')
    {
        
        $fields = [];
        
        $fields['{report_type}'] = $pdf_name;
        
        return hooks()->apply_filters('report_merge_fields', $fields);
    }
}
