<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_tracker_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Order Number',
                    'key'       => '{so_number}',
                    'available' => [
                        'order_tracker',
                    ],
                ],
                [
                    'name'      => 'Description',
                    'key'       => '{description}',
                    'available' => [
                        'order_tracker',
                    ],
                ],
                [
                    'name'      => 'Order Status',
                    'key'       => '{order_status}',
                    'available' => [
                        'order_tracker',
                    ],
                ],
                [
                    'name'      => 'Order Note',
                    'key'       => '{order_note}',
                    'available' => [
                        'order_tracker',
                    ],
                ],
            ];
    }

    /**
 * Merge fields for orders
 * @param  string $template  template name, used to identify url
 * @param  mixed $note_id order id
 * @param  mixed $reply_id  reply id
 * @return array
 */
    public function format($template, $note_id, $reply_id = '')
    {
        $fields = [];

        $this->ci->db->where('id', $note_id);
        $note = $this->ci->db->get(db_prefix() . 'order_tracker_notes')->row();

        $this->ci->db->where('id', $note->order_id);
        $order = $this->ci->db->get(db_prefix() . 'order_tracker')->row();
 

        if (!$order) {
            return $fields;
        }

        
        $custom_fields = get_custom_fields('order_trackers');
        foreach ($custom_fields as $field) {
            $fields['{' . $field['slug'] . '}'] = get_custom_field_value($renewal_id, $field['id'], 'order_trackers');
        }

        $fields['{order_id}'] = $order->id;
        $fields['{so_number}'] = $order->so_number;
        $fields['{description}'] = $order->description;
        $fields['{order_note}'] = $note->order_note;

        $fields['{order_status}'] = $this->ci->db->where('statusid',$order->status_id)
        										 ->get('tblorder_status')->row()->name;
        //$fields['{renewal_type}'] = $renewal->type;
        // print_r($renewal->end_date);die();

        return hooks()->apply_filters('order_tracker_merge_fields', $fields);
    }
}
