<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boqs_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Boq ID',
                    'key'       => '{boq_id}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq Number',
                    'key'       => '{boq_number}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Subject',
                    'key'       => '{boq_subject}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq Total',
                    'key'       => '{boq_total}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq Subtotal',
                    'key'       => '{boq_subtotal}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Open Till',
                    'key'       => '{boq_open_till}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq Assigned',
                    'key'       => '{boq_assigned}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq To',
                    'key'       => '{boq_boq_to}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Address',
                    'key'       => '{boq_address}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'City',
                    'key'       => '{boq_city}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'State',
                    'key'       => '{boq_state}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Zip Code',
                    'key'       => '{boq_zip}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Country',
                    'key'       => '{boq_country}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Email',
                    'key'       => '{boq_email}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Phone',
                    'key'       => '{boq_phone}',
                    'available' => [
                        'boqs',
                    ],
                ],
                [
                    'name'      => 'Boq Link',
                    'key'       => '{boq_link}',
                    'available' => [
                        'boqs',
                    ],
                ],
            ];
    }

    /**
 * Merge fields for boqs
 * @param  mixed $boq_id boq id
 * @return array
 */
    public function format($boq_id)
    {
        $fields = [];
        $this->ci->db->where('id', $boq_id);
        $this->ci->db->join(db_prefix() . 'countries', db_prefix() . 'countries.country_id=' . db_prefix() . 'boqs.country', 'left');
        $boq = $this->ci->db->get(db_prefix() . 'boqs')->row();


        if (!$boq) {
            return $fields;
        }

        if ($boq->currency != 0) {
            $currency = get_currency($boq->currency);
        } else {
            $currency = get_base_currency();
        }

        $fields['{boq_id}']          = $boq_id;
        $fields['{boq_number}']      = format_boq_number($boq_id);
        $fields['{boq_link}']        = site_url('boq/' . $boq_id . '/' . $boq->hash);
        $fields['{boq_subject}']     = $boq->subject;
        $fields['{boq_total}']       = app_format_money($boq->total, $currency);
        $fields['{boq_subtotal}']    = app_format_money($boq->subtotal, $currency);
        $fields['{boq_open_till}']   = _d($boq->open_till);
        $fields['{boq_boq_to}'] = $boq->boq_to;
        $fields['{boq_address}']     = $boq->address;
        $fields['{boq_email}']       = $boq->email;
        $fields['{boq_phone}']       = $boq->phone;

        $fields['{boq_city}']        = $boq->city;
        $fields['{boq_state}']       = $boq->state;
        $fields['{boq_zip}']         = $boq->zip;
        $fields['{boq_country}']     = $boq->short_name;
        $fields['{boq_assigned}']    = get_staff_full_name($boq->assigned);
        $fields['{boq_short_url}']   = get_boq_shortlink($boq);

        $custom_fields = get_custom_fields('boq');
        foreach ($custom_fields as $field) {
            $fields['{' . $field['slug'] . '}'] = get_custom_field_value($boq_id, $field['id'], 'boq');
        }

        return hooks()->apply_filters('boq_merge_fields', $fields, [
        'id'       => $boq_id,
        'boq' => $boq,
     ]);
    }
}
