<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Renewal_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Renewal ID',
                    'key'       => '{renewal_id}',
                    'available' => [
                        'renewal',
                    ],
                ],
                [
                    'name'      => 'Renewal Name',
                    'key'       => '{renewal_name}',
                    'available' => [
                        'renewal',
                    ],
                ],
                [
                    'name'      => 'Renewal Customer',
                    'key'       => '{renewal_customer}',
                    'available' => [
                        'renewal',
                    ],
                ],
                [
                    'name'      => 'Renewal End Date',
                    'key'       => '{renewal_end_date}',
                    'available' => [
                        'renewal',
                    ],
                ],
                [
                    'name'      => 'Renewal Vendor',
                    'key'       => '{renewal_vendor}',
                    'available' => [
                        'renewal',
                    ],
                ],
                
            ];
    }

    /**
 * Merge fields for renewals
 * @param  string $template  template name, used to identify url
 * @param  mixed $renewal_id renewal id
 * @param  mixed $reply_id  reply id
 * @return array
 */
    public function format($template, $renewal_id, $reply_id = '')
    {
        $fields = [];

        $this->ci->db->where('id', $renewal_id);
        $renewal = $this->ci->db->get(db_prefix() . 'renewals')->row();
 

        if (!$renewal) {
            return $fields;
        }

        
        $custom_fields = get_custom_fields('renewals');
        foreach ($custom_fields as $field) {
            $fields['{' . $field['slug'] . '}'] = get_custom_field_value($renewal_id, $field['id'], 'renewals');
        }

        $fields['{renewal_id}'] = $renewal->id;
        $fields['{renewal_name}'] = $renewal->name;

        $fields['{renewal_customer}'] = $this->ci->db->where('userid',$renewal->clientid)
        										 ->get('tblclients')->row()->company;
        //$fields['{renewal_type}'] = $renewal->type;
        // print_r($renewal->end_date);die();
        $fields['{renewal_end_date}'] = $renewal->end_date;
        $fields['{renewal_vendor}'] = $renewal->vendor;

        
        return hooks()->apply_filters('renewal_merge_fields', $fields);
    }
}
