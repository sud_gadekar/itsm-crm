<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Service_request_merge_fields extends App_merge_fields
{
    public function build()
    {
        return [
                [
                    'name'      => 'Service Request ID',
                    'key'       => '{service_request_id}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request URL',
                    'key'       => '{service_request_url}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Public URL',
                    'key'       => '{service_request_public_url}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Department',
                    'key'       => '{service_request_department}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Department Email',
                    'key'       => '{service_request_department_email}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Date Opened',
                    'key'       => '{service_request_date}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Subject',
                    'key'       => '{service_request_subject}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Service Request Billing Type',
                    'key'       => '{service_request_billing_type}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Service Request Contract',
                    'key'       => '{service_request_contract}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Message',
                    'key'       => '{service_request_message}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Status',
                    'key'       => '{service_request_status}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Priority',
                    'key'       => '{service_request_priority}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'service_request Service',
                    'key'       => '{service_request_service}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Project name',
                    'key'       => '{project_name}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'SR Report',
                    'key'       => '{service_request_sr_report}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'SR Type',
                    'key'       => '{service_name}',
                    'available' => [
                        'service_request',
                    ],
                ],
                [
                    'name'      => 'Request For',
                    'key'       => '{request_for_name}',
                    'available' => [
                        'service_request',
                    ],
                ],
            ];
    }

    /**
 * Merge fields for service_requests
 * @param  string $template  template name, used to identify url
 * @param  mixed $service_request_id service_request id
 * @param  mixed $reply_id  reply id
 * @return array
 */
    public function format($template, $service_request_id, $reply_id = '')
    {
        $fields = [];

        $this->ci->db->where('service_requestid', $service_request_id);
        $service_request = $this->ci->db->get(db_prefix() . 'service_requests')->row();

        if (!$service_request) {
            return $fields;
        }

        // Replace contact firstname with the service_request name in case the service_request is not linked to any contact.
        // eq email or form imported.
        if (!empty($service_request->name)) {
            $fields['{contact_firstname}'] = $service_request->name;
        }

        if (!empty($service_request->email)) {
            $fields['{contact_email}'] = $service_request->email;
        }

        $fields['{service_request_priority}'] = '';
        $fields['{service_request_service}']  = '';


        $this->ci->db->where('departmentid', $service_request->department);
        $department = $this->ci->db->get(db_prefix() . 'departments')->row();

        if ($department) {
            $fields['{service_request_department}']       = $department->name;
            $fields['{service_request_department_email}'] = $department->email;
        }

        $languageChanged = false;
        if (!is_client_logged_in()
        && !empty($service_request->userid)
        && isset($GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS'])
        && !$GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS']->get_staff_id() // email to client
    ) {
            load_client_language($service_request->userid);
            $languageChanged = true;
        } else {
            if (isset($GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS'])) {
                $sending_to_staff_id = $GLOBALS['SENDING_EMAIL_TEMPLATE_CLASS']->get_staff_id();
                if ($sending_to_staff_id) {
                    load_admin_language($sending_to_staff_id);
                    $languageChanged = true;
                }
            }
        }

        $fields['{service_request_status}']   = service_request_status_translate($service_request->status);
        $fields['{service_request_priority}'] = service_request_priority_translate($service_request->priority);

        $custom_fields = get_custom_fields('service_requests');
        foreach ($custom_fields as $field) {
            $fields['{' . $field['slug'] . '}'] = get_custom_field_value($service_request_id, $field['id'], 'service_requests');
        }

        if (!is_client_logged_in() && $languageChanged) {
            load_admin_language();
        } elseif (is_client_logged_in() && $languageChanged) {
            load_client_language();
        }

        $this->ci->db->where('serviceid', $service_request->service);
        $service = $this->ci->db->get(db_prefix() . 'services')->row();

        if ($service) {
            $fields['{service_request_service}'] = $service->name;
        }

        $fields['{service_request_id}'] = $service_request_id;

        $customerTemplates = [
        'new-service_request-opened-admin',
        'service_request-reply',
        'service_request-autoresponse',
        'auto-close-service_request',
    ];

        if (in_array($template, $customerTemplates)) {
            $fields['{service_request_url}'] = site_url('clients/service_request/' . $service_request_id);
        } else {
            $fields['{service_request_url}'] = admin_url('service_requests/service_request/' . $service_request_id);
        }

        $reply = false;
        if ($template == 'service_request-reply-to-admin' || $template == 'service_request-reply') {
            $this->ci->db->where('service_requestid', $service_request_id);
            $this->ci->db->limit(1);
            $this->ci->db->order_by('date', 'desc');
            $reply                      = $this->ci->db->get(db_prefix() . 'service_request_replies')->row();
            $fields['{service_request_message}'] = $reply->message;
        } else {
            $fields['{service_request_message}'] = $service_request->message;
        }

        $fields['{service_request_date}']       = _dt($service_request->date);
        $fields['{service_request_subject}']    = $service_request->subject;

        if(isset($service_request->billing_type)){
            $fields['{service_request_billing_type}']    = $service_request->billing_type;    
        }
        else{
            $fields['{service_request_billing_type}']    = '';
        }

        $fields['{service_request_public_url}'] = get_service_request_public_url($service_request);
        $fields['{project_name}']      = get_project_name_by_id($service_request->project_id);

        if(isset($service_request->billing_type)){
            $fields['{service_request_billing_type}']    = $service_request->billing_type;    
        }
        else{
            $fields['{service_request_billing_type}'] = '';
        }
        
        if(isset($service_request->billing_type)){
            if($service_request->billing_type == 'contract')
            {
                $fields['{service_request_contract}']    = $this->ci->db->where('id',$service_request->contract)->get('tblcontracts')->row()->subject;;
            }
            else{
                $fields['{service_request_contract}'] = '';
            }
        }

        if(isset($service_request->attach_service_report)){
            if($service_request->attach_service_report == 1)
            {
                $fields['{service_request_sr_report}'] = admin_url('service_requests/convert_to_pdf/').$service_request_id;
            }
            else
            {
                $fields['{service_request_sr_report}'] = '';
            }
        }

        if(isset($service_request->service)){
            $fields['{service_name}']    = $this->ci->db->where('serviceid',$service_request->service)->get('tblservices')->row()->name;
        }
        else{
            $fields['{service_name}'] = '';
        }

        if(isset($service_request->request_for)){
            $fields['{request_for_name}']    = $this->ci->db->where('requestid',$service_request->request_for)->get('tbltickets_request_for')->row()->name;
        }
        else{
            $fields['{request_for_name}'] = '';
        }
        



        return hooks()->apply_filters('service_request_merge_fields', $fields, [
        'id'       => $service_request_id,
        'reply_id' => $reply_id,
        'template' => $template,
        'service_request'   => $service_request,
        'reply'    => $reply,
     ]);
    }
}
