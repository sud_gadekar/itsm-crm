<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Gdpr_boqs
{
    private $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function export($rel_id, $rel_type)
    {
        // $readboqsDir = '';
        // $tmpDir           = get_temp_dir();


        if (!class_exists('boqs_model')) {
            $this->ci->load->model('boqs_model');
        }

        $this->ci->db->where('rel_id', $rel_id);
        $this->ci->db->where('rel_type', $rel_type);

        $boqs = $this->ci->db->get(db_prefix().'boqs')->result_array();

        $this->ci->db->where('show_on_client_portal', 1);
        $this->ci->db->where('fieldto', 'boq');
        $this->ci->db->order_by('field_order', 'asc');
        $custom_fields = $this->ci->db->get(db_prefix().'customfields')->result_array();
        /*
            if (count($boqs) > 0) {
                $uniqueIdentifier = $tmpDir . $rel_id . time() . '-boqs';
                $readboqsDir = $uniqueIdentifier;
            }*/
        $this->ci->load->model('currencies_model');
        foreach ($boqs as $proposaArrayKey => $boq) {

        // $boq['attachments'] = _prepare_attachments_array_for_export($this->ci->boqs_model->get_attachments($boq['id']));

            // $boqs[$proposaArrayKey] = parse_boq_content_merge_fields($boq);

            $boqs[$proposaArrayKey]['country'] = get_country($boq['country']);

            $boqs[$proposaArrayKey]['currency'] = $this->ci->currencies_model->get($boq['currency']);

            $boqs[$proposaArrayKey]['items'] = _prepare_items_array_for_export(get_items_by_type('boq', $boq['id']), 'boq');

            $boqs[$proposaArrayKey]['comments'] = $this->ci->boqs_model->get_comments($boq['id']);

            $boqs[$proposaArrayKey]['views'] = get_views_tracking('boq', $boq['id']);

            $boqs[$proposaArrayKey]['tracked_emails'] = get_tracked_emails($boq['id'], 'boq');

            $boqs[$proposaArrayKey]['additional_fields'] = [];
            foreach ($custom_fields as $cf) {
                $boqs[$proposaArrayKey]['additional_fields'][] = [
                    'name'  => $cf['name'],
                    'value' => get_custom_field_value($boq['id'], $cf['id'], 'boq'),
                ];
            }

            /*  $tmpboqsDirName = $uniqueIdentifier;
              if (!is_dir($tmpboqsDirName)) {
                  mkdir($tmpboqsDirName, 0755);
              }

              $tmpboqsDirName = $tmpboqsDirName . '/' . $boq['id'];

              mkdir($tmpboqsDirName, 0755);*/

/*        if (count($boq['attachments']) > 0 || !empty($boq['signature'])) {
            $attachmentsDir = $tmpboqsDirName . '/attachments';
            mkdir($attachmentsDir, 0755);

            foreach ($boq['attachments'] as $att) {
                xcopy(get_upload_path_by_type('boq') . $boq['id'] . '/' . $att['file_name'], $attachmentsDir . '/' . $att['file_name']);
            }

            if (!empty($boq['signature'])) {
                xcopy(get_upload_path_by_type('boq') . $boq['id'] . '/' . $boq['signature'], $attachmentsDir . '/' . $boq['signature']);
            }
        }*/

        // unset($boq['id']);

        // $fp = fopen($tmpboqsDirName . '/boq.json', 'w');
        // fwrite($fp, json_encode($boq, JSON_PRETTY_PRINT));
        // fclose($fp);
        }

        return $boqs;
    }
}
