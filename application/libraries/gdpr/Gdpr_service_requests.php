<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Gdpr_service_requests
{
    private $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function export($contact_id)
    {
        $this->ci->load->model('service_requests_model');

        $this->ci->db->where('contactid', $contact_id);
        $service_requests = $this->ci->db->get(db_prefix().'service_requests')->result_array();

        $this->ci->db->where('show_on_client_portal', 1);
        $this->ci->db->where('fieldto', 'service_requests');
        $this->ci->db->order_by('field_order', 'asc');
        $custom_fields = $this->ci->db->get(db_prefix().'customfields')->result_array();

        foreach ($service_requests as $service_requestKey => $service_request) {
            $this->ci->db->where('service_requestid', $service_request['service_requestid']);
            $service_requests[$service_requestKey]['replies'] = $this->ci->db->get(db_prefix().'service_request_replies')->result_array();

            $this->ci->db->where('departmentid', $service_request['department']);
            $dept = $this->ci->db->get(db_prefix().'departments')->row();

            if ($dept) {
                $service_requests[$service_requestKey]['department_name'] = $dept->name;
            }

            $this->ci->db->where('priorityid', $service_request['priority']);
            $priority = $this->ci->db->get(db_prefix().'service_requests_priorities')->row();
            if ($priority) {
                $service_requests[$service_requestKey]['priority_name'] = $priority->name;
            }

            $this->ci->db->where('service_requeststatusid', $service_request['status']);
            $status = $this->ci->db->get(db_prefix().'service_requests_status')->row();
            if ($status) {
                $service_requests[$service_requestKey]['status_name'] = $status->name;
            }

            $this->ci->db->where('serviceid', $service_request['service']);
            $service = $this->ci->db->get(db_prefix().'services')->row();
            if ($service) {
                $service_requests[$service_requestKey]['service_name'] = $service->name;
            }

            $service_requests[$service_requestKey]['additional_fields'] = [];
            foreach ($custom_fields as $cf) {
                $service_requests[$service_requestKey]['additional_fields'][] = [
                    'name'  => $cf['name'],
                    'value' => get_custom_field_value($service_request['service_requestid'], $cf['id'], 'service_requests'),
                ];
            }
        }

        return $service_requests;
    }
}
