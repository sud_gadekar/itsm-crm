<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/Service_requestTemplate.php');

class Service_request_assigned_to_staff extends App_mail_template
{
    use Service_requestTemplate;

    protected $for = 'staff';

    protected $staff_email;

    protected $staffid;

    protected $service_requestid;

    protected $client_id;

    protected $contact_id;

    public $slug = 'service_request-assigned-to-admin';

    public $rel_type = 'service_request';

    public function __construct($staff_email, $staffid, $service_requestid, $client_id, $contact_id)
    {
        parent::__construct();

        $this->staff_email = $staff_email;
        $this->staffid     = $staffid;
        $this->service_requestid    = $service_requestid;
        $this->client_id   = $client_id;
        $this->contact_id  = $contact_id;
    }

    public function build()
    {

        $this->to($this->staff_email)
        ->set_rel_id($this->service_requestid)
        ->set_staff_id($this->staffid)
        ->set_merge_fields('client_merge_fields', $this->client_id, $this->contact_id)
        ->set_merge_fields('service_request_merge_fields', $this->slug, $this->service_requestid);
    }
}
