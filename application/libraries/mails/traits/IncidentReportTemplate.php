<?php

defined('BASEPATH') or exit('No direct script access allowed');

trait IncidentReportTemplate
{
   
    protected function _subject()
    {
        /**
         * IMPORTANT
         * Do not change/remove this line, this is used for email piping so the software can recognize the service_request id.
         */
        // if (substr($this->template->subject, 0, 10) != '[service_request ID') {
        //     return $this->template->subject . ' [service_request ID: ' . $this->service_requestid . ']';
        // }

        return parent::_subject();
    }

    protected function _reply_to()
    {
        $default = parent::_reply_to();

        // Should be loaded?
        if (!class_exists('service_requests_model')) {
            $this->ci->load->model('service_requests_model');
        }

        // $service_request = $this->get_service_request_for_mail();

        // if (!empty($service_request->department_email) && valid_email($service_request->department_email)) {
        //     return $service_request->department_email;
        // }

        return $default;
    }

    protected function _from()
    {
        $default = parent::_from();

        $service_request = $this->get_service_request_for_mail();

        // if (!empty($service_request->department_email)
        //     && $service_request->dept_email_from_header == 1
        //     && valid_email($service_request->department_email)) {
        //     return [
        //         'fromname'  => $default['fromname'],
        //         'fromemail' => $service_request->department_email,
        //     ];
        // }

        return $default;
    }

    private function get_service_request_for_mail()
    {
        // $this->ci->db->select(db_prefix() . 'departments.email as department_email, email_from_header as dept_email_from_header')
        //     ->where('service_requestid', $this->service_requestid)
        //     ->join(db_prefix() . 'departments', db_prefix() . 'departments.departmentid=' . db_prefix() . 'service_requests.department', 'left');

        // return $this->ci->db->get(db_prefix() . 'service_requests')->row();
    }

   
    private function add_report_attachments($pdf_name)
    {
            $this->add_attachment([
                    'attachment' => TEMP_FOLDER.'Report.pdf',
                    'filename'   => 'Report.pdf',
                    'type'       => 'pdf',
                    'read'       => true,
                ]);

            if($pdf_name == 'Support Request Report'){
                $this->add_attachment([
                    'attachment' => TEMP_FOLDER.'Incident_chart.png',
                    'filename'   => 'Incident_chart.png',
                    'type'       => 'png',
                    'read'       => true,
                ]);
            }
            elseif ($pdf_name == 'Service Report') {
                $this->add_attachment([
                    'attachment' => TEMP_FOLDER.'Service_request_chart.png',
                    'filename'   => 'Service_request_chart.png',
                    'type'       => 'png',
                    'read'       => true,
                ]);
                $this->add_attachment([
                    'attachment' => TEMP_FOLDER.'SR_requestfor_chart.png',
                    'filename'   => 'SR_requestfor_chart.png',
                    'type'       => 'png',
                    'read'       => true,
                ]);
            }
            
        //}
    }


}


