<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/Service_requestTemplate.php');

class Service_request_auto_close_to_customer extends App_mail_template
{
    use Service_requestTemplate;

    protected $for = 'customer';

    protected $service_request;

    protected $email;

    protected $service_requestid;

    public $slug = 'auto-close-service_request';

    public $rel_type = 'service_request';

    public function __construct($service_request, $email)
    {
        parent::__construct();

        $this->service_request   = $service_request;
        $this->email    = $email;
        $this->service_requestid = $service_request->service_requestid;
    }

    public function build()
    {
        $this->to($this->email)
        ->set_rel_id($this->service_request->service_requestid)
        ->set_merge_fields('client_merge_fields', $this->service_request->userid, $this->service_request->contactid)
        ->set_merge_fields('service_request_merge_fields', $this->slug, $this->service_request->service_requestid);
    }
}
