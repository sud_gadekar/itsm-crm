<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_note_send_to_contact extends App_mail_template
{
    protected $for = 'customer';

    protected $notes;

    protected $email;

    public $slug = 'send-contact-order-note';

    public $rel_type = 'order_tracker';

    public function __construct($notes, $email, $cc = '')
    {
        parent::__construct();

        $this->email            = $email;
        $this->notes            = $notes;
        $this->cc               = $cc;

        //print_r($this->notes); die;
    }

    public function build()
    {   
        $this->to($this->email)
        ->set_rel_id($this->notes->order_id)
        ->set_merge_fields('client_merge_fields', $this->notes->client_id)
        ->set_merge_fields('order_tracker_merge_fields', $this->slug, $this->notes->id);
        //->set_merge_fields('client_merge_fields', $this->order_details->client_id, $this->order_details->client_id);
    }
}
