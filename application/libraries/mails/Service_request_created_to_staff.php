<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/Service_requestTemplate.php');

class Service_request_created_to_staff extends App_mail_template
{
    use service_requestTemplate;

    protected $for = 'staff';

    protected $service_requestid;

    protected $client_id;

    protected $contact_id;

    protected $staff;

    protected $service_request_attachments;

    public $slug = 'new-service_request-created-staff';

    public $rel_type = 'service_request';

    public function __construct($service_requestid, $client_id, $contact_id, $staff, $service_request_attachments)
    {
        parent::__construct();

        $this->service_requestid   = $service_requestid;
        $this->client_id  = $client_id;
        $this->contact_id = $contact_id;
        $this->staff      = $staff;

        $this->service_request_attachments = $service_request_attachments;
    }

    public function build()
    {

        $this->add_service_request_attachments();

        $this->to($this->staff['email'])
        ->set_rel_id($this->service_requestid)
        ->set_staff_id($this->staff['staffid'])
        ->set_merge_fields('client_merge_fields', $this->client_id, $this->contact_id)
        ->set_merge_fields('service_request_merge_fields', $this->slug, $this->service_requestid);
    }
}
