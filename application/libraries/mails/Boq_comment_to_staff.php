<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq_comment_to_staff extends App_mail_template
{
    protected $for = 'staff';

    protected $boq_id;

    protected $staff_email;

    public $slug = 'boq-comment-to-admin';

    public $rel_type = 'boq';

    public function __construct($boq_id, $staff_email)
    {
        parent::__construct();

        $this->boq_id = $boq_id;
        $this->staff_email = $staff_email;

        // For SMS
        $this->set_merge_fields('boqs_merge_fields', $this->boq_id);
    }

    public function build()
    {
        $this->to($this->staff_email)
        ->set_rel_id($this->boq_id);
    }
}
