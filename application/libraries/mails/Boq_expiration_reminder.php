<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq_expiration_reminder extends App_mail_template
{
    protected $for = 'customer';

    protected $boq;

    public $slug = 'boq-expiry-reminder';

    public $rel_type = 'boq';

    public function __construct($boq)
    {
        parent::__construct();

        $this->boq = $boq;

        // For SMS
        $this->set_merge_fields('boqs_merge_fields', $this->boq->id);
    }

    public function build()
    {
        set_mailing_constant();
        $pdf    = boq_pdf($this->boq);
        $attach = $pdf->Output(slug_it($this->boq->subject) . '.pdf', 'S');

        $this->add_attachment([
            'attachment' => $attach,
            'filename'   => slug_it($this->boq->subject) . '.pdf',
            'type'       => 'application/pdf',
        ]);

        $this->to($this->boq->email)
        ->set_rel_id($this->boq->id);
    }
}
