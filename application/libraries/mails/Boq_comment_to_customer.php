<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq_comment_to_customer extends App_mail_template
{
    protected $for = 'customer';

    protected $boq;

    public $slug = 'boq-comment-to-client';

    public $rel_type = 'boq';

    public function __construct($boq)
    {
        parent::__construct();

        $this->boq = $boq;

        // For SMS
        $this->set_merge_fields('boqs_merge_fields', $this->boq->id);
    }

    public function build()
    {
        $this->to($this->boq->email)
        ->set_rel_id($this->boq->id);
    }
}
