<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/IncidentReportTemplate.php');

class Report extends App_mail_template
{
    use IncidentReportTemplate;

    //protected $for = 'customer';

    protected $customer_email;

    protected $pdf_name;

    public $slug = 'reports';

    //public $rel_type = 'ticket';

    public function __construct($email, $ticket_attachments, $pdf_name)
    {
        parent::__construct();

        $this->customer_email = $email;
        $this->pdf_name = $pdf_name;

// print_r($this->pdf_name);die();
    }

    public function build()
    {
        $this->add_report_attachments($this->pdf_name);
        $this->to($this->customer_email)
        ->set_merge_fields('report_merge_fields', $this->slug, $this->pdf_name);
    }
}
