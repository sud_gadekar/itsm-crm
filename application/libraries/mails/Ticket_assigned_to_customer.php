<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/TicketTemplate.php');

class Ticket_assigned_to_customer extends App_mail_template
{
    use TicketTemplate;

    protected $for = 'customer';

    protected $customer_email;

    protected $customerid;

    protected $ticketid;

    protected $client_id;

    protected $contact_id;

    public $slug = 'ticket-assigned-to-customer';

    public $rel_type = 'ticket';

    public function __construct($customer_email, $customerid, $ticketid, $client_id, $contact_id)
    {
        parent::__construct();

        $this->customer_email = $customer_email;
        $this->customerid     = $customerid;
        $this->ticketid    = $ticketid;
        $this->client_id   = $client_id;
        $this->contact_id  = $contact_id;
    }

    public function build()
    {

        $this->to($this->customer_email)
        ->set_rel_id($this->ticketid)
        ->set_staff_id($this->customerid)
        ->set_merge_fields('client_merge_fields', $this->client_id, $this->contact_id)
        ->set_merge_fields('ticket_merge_fields', $this->slug, $this->ticketid);
    }
}
