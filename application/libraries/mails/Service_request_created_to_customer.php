<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/Service_requestTemplate.php');

class Service_request_created_to_customer extends App_mail_template
{
    use service_requestTemplate;

    protected $for = 'customer';

    protected $service_request;

    protected $email;

    protected $service_requestid;

    protected $service_request_attachments;

    public $slug = 'new-service_request-opened-admin';

    public $rel_type = 'service_request';

    public function __construct($service_request, $email, $service_request_attachments, $cc)
    {
       //print_r($cc);die;
        parent::__construct();

        $this->service_request             = $service_request;
        $this->email                       = $email;
        $this->service_requestid           = $service_request->service_requestid;
        $this->service_request_attachments = $service_request_attachments;
        $this->cc                          = $cc;
    }

    public function build()
    {
        //print_r($this->service_request);die;

        $this->add_service_request_attachments();

        $this->to($this->email)
        ->set_rel_id($this->service_request->service_requestid)
        
        ->set_merge_fields('client_merge_fields', $this->service_request->userid, $this->service_request->contactid)
        ->set_merge_fields('service_request_merge_fields', $this->slug, $this->service_request->service_requestid);

    }
}
