<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq_send_to_customer extends App_mail_template
{
    protected $for = 'customer';

    protected $boq;

    protected $attach_pdf;

    public $slug = 'boq-send-to-customer';

    public $rel_type = 'boq';

    public function __construct($boq, $attach_pdf, $cc = '')
    {
        parent::__construct();

        $this->boq   = $boq;
        $this->attach_pdf = $attach_pdf;
        $this->cc         = $cc;
    }

    public function build()
    {
        if ($this->attach_pdf) {
            set_mailing_constant();
            $pdf    = boq_pdf($this->boq);
            $attach = $pdf->Output(slug_it($this->boq->subject) . '.pdf', 'S');
            $this->add_attachment([
                'attachment' => $attach,
                'filename'   => slug_it($this->boq->subject) . '.pdf',
                'type'       => 'application/pdf',
            ]);
        }

        if ($this->ci->input->post('email_attachments')) {
            $_other_attachments = $this->ci->input->post('email_attachments');
            foreach ($_other_attachments as $attachment) {
                $_attachment = $this->ci->boqs_model->get_attachments($this->boq->id, $attachment);
                $this->add_attachment([
                    'attachment' => get_upload_path_by_type('boq') . $this->boq->id . '/' . $_attachment->file_name,
                    'filename'   => $_attachment->file_name,
                    'type'       => $_attachment->filetype,
                    'read'       => true,
                ]);
            }
        }

        $this->to($this->boq->email)
        ->set_rel_id($this->boq->id)
        ->set_merge_fields('boqs_merge_fields', $this->boq->id);
    }
}
