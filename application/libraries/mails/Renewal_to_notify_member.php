<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Renewal_to_notify_member extends App_mail_template
{
    
    //protected $for = 'customer';

    protected $renewal = '';

    protected $email='';

    protected $renewalid = '';

    public $slug = 'renewal_notify_member';

    public $rel_type = 'renewal';

    public function __construct($renewal, $email)
    {
        parent::__construct();

        $this->renewal = $renewal;
        $this->email    = $email;
        $this->renewalid = $renewal['id'];
        //print_r($this->renewal);die();
    }

    public function build()
    {
        //print_r($renewal);die();
        $this->to($this->email)
        ->set_rel_id($this->renewalid)
        ->set_merge_fields('client_merge_fields', $this->renewal['clientid'])
        ->set_merge_fields('renewal_merge_fields', $this->slug, $this->renewalid);
    }
}
