<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(APPPATH . 'libraries/mails/traits/Service_requestTemplate.php');

class Service_request_new_reply_to_staff extends App_mail_template
{
    use Service_requestTemplate;

    protected $for = 'staff';

    protected $service_request;

    protected $staff;

    protected $service_requestid;

    protected $service_request_attachments;

    public $slug = 'service_request-reply-to-admin';

    public $rel_type = 'service_request';

    public function __construct($service_request, $staff, $service_request_attachments)
    {
        parent::__construct();

        $this->service_request             = $service_request;
        $this->staff              = $staff;
        $this->service_requestid           = $service_request->service_requestid;
        $this->service_request_attachments = $service_request_attachments;
    }

    public function build()
    {

        $this->add_service_request_attachments();

        $this->to($this->staff['email'])
        ->set_rel_id($this->service_request->service_requestid)
        ->set_staff_id($this->staff['staffid'])
        ->set_merge_fields('client_merge_fields', $this->service_request->userid, $this->service_request->contactid)
        ->set_merge_fields('service_request_merge_fields', $this->slug, $this->service_request->service_requestid);
    }
}
