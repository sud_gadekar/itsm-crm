<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Boq_declined_to_staff extends App_mail_template
{
    protected $for = 'staff';

    protected $boq;

    protected $staff_email;

    public $slug = 'boq-client-declined';

    public $rel_type = 'boq';

    public function __construct($boq, $staff_email)
    {
        parent::__construct();

        $this->boq    = $boq;
        $this->staff_email = $staff_email;
    }

    public function build()
    {
        $this->to($this->staff_email)
        ->set_rel_id($this->boq->id)
        ->set_merge_fields('boqs_merge_fields', $this->boq->id);
    }
}
