<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(__DIR__ . '/App_pdf.php');

class Boq_pdf extends App_pdf
{
    protected $boq;

    private $boq_number;

    public function __construct($boq, $tag = '')
    {
        $boq                = hooks()->apply_filters('boq_html_pdf_data', $boq);
        $GLOBALS['boq_pdf'] = $boq;

        parent::__construct();

        $this->tag      = $tag;
        $this->boq = $boq;

        $this->boq_number = format_boq_number($this->boq->id);

        if ($boq->rel_id != null && $boq->rel_type == 'customer') {
            $this->load_language($boq->rel_id);
        }

        $this->SetTitle($this->boq_number);
        $this->SetDisplayMode('default', 'OneColumn');

        # Don't remove these lines - important for the PDF layout
        $this->boq->content = $this->fix_editor_html($this->boq->content);
    }

    public function prepare()
    {
        $number_word_lang_rel_id = 'unknown';

        if ($this->boq->rel_type == 'customer') {
            $number_word_lang_rel_id = $this->boq->rel_id;
        }

        $this->with_number_to_word($number_word_lang_rel_id);

        $total = '';
        if ($this->boq->total != 0) {
            $total = app_format_money($this->boq->total, get_currency($this->boq->currency));
            $total = _l('boq_total') . ': ' . $total;
        }

        $this->set_view_vars([
            'number'       => $this->boq_number,
            'boq'     => $this->boq,
            'total'        => $total,
            'boq_url' => site_url('boq/' . $this->boq->id . '/' . $this->boq->hash),
        ]);

        return $this->build();
    }

    protected function type()
    {
        return 'boq';
    }

    protected function file_path()
    {
        $customPath = APPPATH . 'views/themes/' . active_clients_theme() . '/views/my_boqpdf.php';
        $actualPath = APPPATH . 'views/themes/' . active_clients_theme() . '/views/boqpdf.php';

        if (file_exists($customPath)) {
            $actualPath = $customPath;
        }

        return $actualPath;
    }
}
