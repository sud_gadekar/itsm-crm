<?php

defined('BASEPATH') or exit('No direct script access allowed');

function app_init_admin_sidebar_menu_items()
{
    $CI = &get_instance();

    $CI->app_menu->add_sidebar_menu_item('dashboard', [
        'name'     => _l('als_dashboard'),
        'href'     => admin_url(),
        'position' => 1,
        'icon'     => 'fa fa-home',
    ]);

    if (has_permission('customers', '', 'view')
        || (have_assigned_customers()
        || (!have_assigned_customers() && has_permission('customers', '', 'create')))) {
        $CI->app_menu->add_sidebar_menu_item('customers', [
            'name'     => _l('als_clients'),
            'href'     => admin_url('clients'),
            'position' => 5,
            'icon'     => 'fa fa-user-o',
        ]);
    }
        if ((!is_staff_member() && get_option('access_tickets_to_none_staff_members') == 1) || is_staff_member()) {
        $CI->app_menu->add_sidebar_menu_item('incident', [
                'name'     => "Support Request",
                'href'     => admin_url('tickets'),
                'icon'     => 'fa fa-ticket',
                'position' => 7,
        ]);
        }

        $CI->app_menu->add_sidebar_menu_item('service_request', [        
                'name'     => 'Service Requests',
                'href'     => admin_url('service_requests'),
                'position' => 8,
                'icon'     => 'fa fa-wrench',
            ]);

        $CI->app_menu->add_sidebar_menu_item('assets', [        
                'name'     => 'Inventory details',
                'href'     => admin_url('inventory_details'),
                'position' => 9,
                'icon'     => 'fa fa-book',
        ]);  
        
        $CI->app_menu->add_sidebar_menu_item('diagrams', [        
                'name'     => 'Diagrams',
                'href'     => admin_url('diagrams'),
                'position' => 30,
                'icon'     => 'fa fa-cubes',
        ]);   

          
        
        $CI->app_menu->add_sidebar_menu_item('gaps', [        
                'name'     => 'Gaps',
                'href'     => admin_url('gaps'),
                'position' => 31,
                'icon'     => 'fa fa-exchange',
        ]); 

        $CI->app_menu->add_sidebar_menu_item('networks', [        
                'name'     => 'IT Overview',
                'href'     => admin_url('network_servers/network'),
                'position' => 32,
                'icon'     => 'fa fa-globe',
        ]); 

        $CI->app_menu->add_sidebar_menu_item('team_availability', [        
                'name'     => 'Team Availability',
                'href'     => admin_url('team_availiabilities'),
                'position' => 33,
                'icon'     => 'fa fa-user-plus',
        ]); 
    
        $CI->app_menu->add_sidebar_menu_item('backup', [        
                'name'     => 'Backup Policy',
                'href'     => admin_url('backups'),
                'position' => 34,
                'icon'     => 'fa fa-hdd-o',
        ]);

        $CI->app_menu->add_sidebar_menu_item('checklist', [        
                'name'     => 'Checklist',
                'href'     => admin_url('checklists'),
                'position' => 36,
                'icon'     => 'fa fa-heartbeat',
        ]);

        $CI->app_menu->add_sidebar_menu_item('order_tracker', [        
                'name'     => 'Purchase order',
                'href'     => admin_url('order_tracker'),
                'position' => 37,
                'icon'     => 'fa fa-first-order',
        ]);
    
        $CI->app_menu->add_sidebar_menu_item('renewal', [        
                'name'     => 'Software & License',
                'href'     => admin_url('renewals'),
                'position' => 35,
                'icon'     => 'fa fa-retweet',
        ]); 
    

    $CI->app_menu->add_sidebar_menu_item('sales', [
            'collapse' => true,
            'name'     => _l('als_sales'),
            'position' => 40,
            'icon'     => 'fa fa-balance-scale',
        ]);

    if ((has_permission('boqs', '', 'view') || has_permission('boqs', '', 'view_own'))
        || (staff_has_assigned_boqs() && get_option('allow_staff_view_boqs_assigned') == 1)) {
        // $CI->app_menu->add_sidebar_children_item('sales', [
        //         'slug'     => 'proposals',
        //         'name'     => _l('Propsals'),
        //         'href'     => admin_url('proposals'),
        //         'position' => 5,
        // ]);

        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'boqs',
                'name'     => _l('BOQs'),
                'href'     => admin_url('boqs'),
                'position' => 5,
        ]);
    }

    if ((has_permission('estimates', '', 'view') || has_permission('estimates', '', 'view_own'))
        || (staff_has_assigned_estimates() && get_option('allow_staff_view_estimates_assigned') == 1)) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'estimates',
                'name'     => _l('estimates'),
                'href'     => admin_url('estimates'),
                'position' => 10,
        ]);
    }

//     if ((has_permission('invoices', '', 'view') || has_permission('invoices', '', 'view_own'))
//          || (staff_has_assigned_invoices() && get_option('allow_staff_view_invoices_assigned') == 1)) {
//         $CI->app_menu->add_sidebar_children_item('sales', [
//                 'slug'     => 'invoices',
//                 'name'     => _l('invoices'),
//                 'href'     => admin_url('invoices'),
//                 'position' => 15,
//         ]);
//     }

//     if (has_permission('payments', '', 'view') || has_permission('invoices', '', 'view_own')
//            || (get_option('allow_staff_view_invoices_assigned') == 1 && staff_has_assigned_invoices())) {
//         $CI->app_menu->add_sidebar_children_item('sales', [
//                 'slug'     => 'payments',
//                 'name'     => _l('payments'),
//                 'href'     => admin_url('payments'),
//                 'position' => 20,
//         ]);
//     }

//     if (has_permission('credit_notes', '', 'view') || has_permission('credit_notes', '', 'view_own')) {
//         $CI->app_menu->add_sidebar_children_item('sales', [
//                 'slug'     => 'credit_notes',
//                 'name'     => _l('credit_notes'),
//                 'href'     => admin_url('credit_notes'),
//                 'position' => 25,
//         ]);
//     }

    if (has_permission('items', '', 'view')) {
        $CI->app_menu->add_sidebar_children_item('sales', [
                'slug'     => 'items',
                'name'     => _l('items'),
                'href'     => admin_url('invoice_items'),
                'position' => 30,
        ]);
    }

        $CI->app_menu->add_sidebar_menu_item('projects', [
                'name'     => _l('projects'),
                'href'     => admin_url('projects'),
                'icon'     => 'fa fa-bars',
                'position' => 7,
        ]);


    if (has_permission('subscriptions', '', 'view') || has_permission('subscriptions', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('subscriptions', [
                'name'     => _l('subscriptions'),
                'href'     => admin_url('subscriptions'),
                'icon'     => 'fa fa-repeat',
                'position' => 15,
        ]);
    }

    if (has_permission('expenses', '', 'view') || has_permission('expenses', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('expenses', [
                'name'     => _l('expenses'),
                'href'     => admin_url('expenses'),
                'icon'     => 'fa fa-file-text-o',
                'position' => 20,
        ]);
    }

    if (has_permission('contracts', '', 'view') || has_permission('contracts', '', 'view_own')) {
        $CI->app_menu->add_sidebar_menu_item('contracts', [
                'name'     => _l('contracts'),
                'href'     => admin_url('contracts'),
                'icon'     => 'fa fa-file',
                'position' => 25,
        ]);
    }

  
    $CI->app_menu->add_sidebar_menu_item('tasks', [
                'name'     => _l('als_tasks'),
                'href'     => admin_url('tasks'),
                'icon'     => 'fa fa-tasks',
                'position' => 35,
        ]);

   

    if (is_staff_member()) {
        $CI->app_menu->add_sidebar_menu_item('leads', [
                'name'     => _l('als_leads'),
                'href'     => admin_url('leads'),
                'icon'     => 'fa fa-tty',
                'position' => 45,
        ]);
    }

    if (has_permission('knowledge_base', '', 'view')) {
        $CI->app_menu->add_sidebar_menu_item('knowledge-base', [
                'name'     => _l('als_kb'),
                'href'     => admin_url('knowledge_base'),
                'icon'     => 'fa fa-folder-open-o',
                'position' => 50,
        ]);

        $CI->app_menu->add_sidebar_menu_item('newsletters', [
                'name'     => _l('Newsletters & Educational Contents'),
                'href'     => admin_url('newsletters'),
                'icon'     => 'fa fa-folder-open-o',
                'position' => 55,
        ]);
    }

    
        $CI->app_menu->add_sidebar_menu_item('guest_enquiries', [
                'name'     => _l('Sign Up'),
                'href'     => admin_url('tickets/guest_enquiries'),
                'icon'     => 'fa fa-phone',
                'position' => 57,
        ]);



    $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'media',
                'name'     => _l('als_media'),
                'href'     => admin_url('utilities/media'),
                'position' => 5,
        ]);

    if (has_permission('bulk_pdf_exporter', '', 'view')) {
        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'bulk-pdf-exporter',
                'name'     => _l('bulk_pdf_exporter'),
                'href'     => admin_url('utilities/bulk_pdf_exporter'),
                'position' => 10,
        ]);
    }

    $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'calendar',
                'name'     => _l('als_calendar_submenu'),
                'href'     => admin_url('utilities/calendar'),
                'position' => 15,
        ]);


    if (is_admin()) {
        $CI->app_menu->add_sidebar_menu_item('announcements', [
                'name'     => _l('als_announcements_submenu'),
                'href'     => admin_url('announcements'),
                'icon'     => 'fa fa-bullhorn',
                'position' => 60,
        ]);

        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'activity-log',
                'name'     => _l('als_activity_log_submenu'),
                'href'     => admin_url('utilities/activity_log'),
                'icon'     => 'fa fa-user-o',
                'position' => 25,

        ]);

        $CI->app_menu->add_sidebar_children_item('utilities', [
                'slug'     => 'ticket-pipe-log',
                'name'     => _l('ticket_pipe_log'),
                'href'     => admin_url('utilities/pipe_log'),
                'position' => 30,
        ]);
    }

    if (has_permission('reports', '', 'view')) {
        $CI->app_menu->add_sidebar_menu_item('reports', [
                'collapse' => true,
                'name'     => _l('als_reports'),
                'href'     => admin_url('reports'),
                'icon'     => 'fa fa-area-chart',
                'position' => 60,
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'sales-reports',
                'name'     => _l('als_reports_sales_submenu'),
                'href'     => admin_url('reports/sales'),
                'position' => 5,
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'expenses-reports',
                'name'     => _l('als_reports_expenses'),
                'href'     => admin_url('reports/expenses'),
                'position' => 10,
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'expenses-vs-income-reports',
                'name'     => _l('als_expenses_vs_income'),
                'href'     => admin_url('reports/expenses_vs_income'),
                'position' => 15,
        ]);
        $CI->app_menu->add_sidebar_children_item('reports', [
                'slug'     => 'leads-reports',
                'name'     => _l('als_reports_leads_submenu'),
                'href'     => admin_url('reports/leads'),
                'position' => 20,
        ]);

        if (is_admin()) {
            $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'timesheets-reports',
                    'name'     => _l('timesheets_overview'),
                    'href'     => admin_url('staff/timesheets?view=all'),
                    'position' => 25,
            ]);
        }

        $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'knowledge-base-reports',
                    'name'     => _l('als_kb_articles_submenu'),
                    'href'     => admin_url('reports/knowledge_base_articles'),
                    'position' => 30,
            ]);

        $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'status-reports',
                    'name'     => _l('Helpdesk Reports'),
                    'href'     => admin_url('reports/statuses'),
                    'position' => 35,
            ]);

        $CI->app_menu->add_sidebar_children_item('reports', [
                    'slug'     => 'type-reports',
                    'name'     => _l('Type Based Reports'),
                    'href'     => admin_url('reports/types'),
                    'position' => 36,
            ]);
    }

     $CI->app_menu->add_sidebar_menu_item('service', [
            'collapse' => true,
            'name'     => _l('Services'),
            'position' => 90,
            'icon'     => 'fa fa-balance-scale',
        ]);
    


     if (is_admin()) {
        $CI->app_menu->add_sidebar_menu_item('activity', [
                    'slug'     => 'activity-log',
                    'name'     => _l('als_activity_log_submenu'),
                    'href'     => admin_url('utilities/activity_log'),
                    'icon'     => 'fa fa-history',
                    'position' => 65,
        ]);
    }
    // Setup menu
    if (has_permission('staff', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('staff', [
                    'name'     => _l('als_staff'),
                    'href'     => admin_url('staff'),
                    'position' => 5,
            ]);
    }

    if (is_admin()) {
        $CI->app_menu->add_setup_menu_item('customers', [
                    'collapse' => true,
                    'name'     => _l('clients'),
                    'position' => 10,
            ]);

        $CI->app_menu->add_setup_children_item('customers', [
                    'slug'     => 'customer-groups',
                    'name'     => _l('customer_groups'),
                    'href'     => admin_url('clients/groups'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_menu_item('support', [
                    'collapse' => true,
                    'name'     => _l('support'),
                    'position' => 15,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'departments',
                    'name'     => _l('acs_departments'),
                    'href'     => admin_url('departments'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-predefined-replies',
                    'name'     => _l('acs_ticket_predefined_replies_submenu'),
                    'href'     => admin_url('tickets/predefined_replies'),
                    'position' => 10,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'service_requests-predefined-replies',
                    'name'     => _l('SR predefined replies'),
                    'href'     => admin_url('service_requests/predefined_replies'),
                    'position' => 11,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-priorities',
                    'name'     => _l('acs_ticket_priority_submenu'),
                    'href'     => admin_url('tickets/priorities'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-statuses',
                    'name'     => _l('acs_ticket_statuses_submenu'),
                    'href'     => admin_url('tickets/statuses'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'service_requests-priorities',
                    'name'     => _l('acs_service_request_priority_submenu'),
                    'href'     => admin_url('service_requests/priorities'),
                    'position' => 25,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'service_requests-statuses',
                    'name'     => _l('acs_service_request_statuses_submenu'),
                    'href'     => admin_url('service_requests/statuses'),
                    'position' => 30,
            ]);


        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-services',
                    'name'     => _l('acs_ticket_services_submenu'),
                    'href'     => admin_url('tickets/services'),
                    'position' => 35,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                    'slug'     => 'tickets-spam-filters',
                    'name'     => _l('spam_filters'),
                    'href'     => admin_url('spam_filters/view/tickets'),
                    'position' => 40,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                'slug'     => 'tickets-request-for',
                'name'     => _l('Request For'),
                'href'     => admin_url('tickets/requests'),
                'position' => 45,
            ]);
        $CI->app_menu->add_setup_children_item('support', [
                'slug'     => 'tickets-deliveries',
                'name'     => _l('Delivery'),
                'href'     => admin_url('tickets/deliveries'),
                'position' => 50,
           ]);

         $CI->app_menu->add_setup_children_item('support', [
                'slug'     => 'role',
                'name'     => _l('Team Role'),
                'href'     => admin_url('clients/roles'),
                'position' => 55,
           ]);

        // $CI->app_menu->add_setup_children_item('support', [
        //         'slug'     => 'additional_service_charge',
        //         'name'     => _l('Addtional Service charges'),
        //         'href'     => admin_url('clients/additional_service_charges'),
        //         'position' => 60,
        //    ]);

        $CI->app_menu->add_setup_menu_item('leads', [
                    'collapse' => true,
                    'name'     => _l('acs_leads'),
                    'position' => 20,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-sources',
                    'name'     => _l('acs_leads_sources_submenu'),
                    'href'     => admin_url('leads/sources'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-statuses',
                    'name'     => _l('acs_leads_statuses_submenu'),
                    'href'     => admin_url('leads/statuses'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'leads-email-integration',
                    'name'     => _l('leads_email_integration'),
                    'href'     => admin_url('leads/email_integration'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('leads', [
                    'slug'     => 'web-to-lead',
                    'name'     => _l('web_to_lead'),
                    'href'     => admin_url('leads/forms'),
                    'position' => 20,
            ]);

        $CI->app_menu->add_setup_menu_item('finance', [
                    'collapse' => true,
                    'name'     => _l('acs_finance'),
                    'position' => 25,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'taxes',
                    'name'     => _l('acs_sales_taxes_submenu'),
                    'href'     => admin_url('taxes'),
                    'position' => 5,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'currencies',
                    'name'     => _l('acs_sales_currencies_submenu'),
                    'href'     => admin_url('currencies'),
                    'position' => 10,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'payment-modes',
                    'name'     => _l('acs_sales_payment_modes_submenu'),
                    'href'     => admin_url('paymentmodes'),
                    'position' => 15,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'expenses-categories',
                    'name'     => _l('acs_expense_categories'),
                    'href'     => admin_url('expenses/categories'),
                    'position' => 20,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'items-regions',
                    'name'     => 'Items Regions',
                    'href'     => admin_url('invoice_items/regions'),
                    'position' => 21,
            ]);
        $CI->app_menu->add_setup_children_item('finance', [
                    'slug'     => 'items-manufacturers',
                    'name'     => 'Items Manufacturers',
                    'href'     => admin_url('invoice_items/manufacturers'),
                    'position' => 22,
            ]);

        $CI->app_menu->add_setup_menu_item('contracts', [
                    'collapse' => true,
                    'name'     => _l('acs_contracts'),
                    'position' => 30,
            ]);
        $CI->app_menu->add_setup_children_item('contracts', [
                    'slug'     => 'contracts-types',
                    'name'     => _l('acs_contract_types'),
                    'href'     => admin_url('contracts/types'),
                    'position' => 5,
            ]);


        $CI->app_menu->add_setup_menu_item('inventory', [
                    'collapse' => true,
                    'name'     => _l('inventory'),
                    'position' => 32,
        ]);

        $CI->app_menu->add_setup_children_item('inventory', [
                    'slug'     => 'inventory_types',
                    'name'     => _l('Inventory types'),
                    'href'     => admin_url('inventory_details/inventory_types'),
                    'position' => 5,
            ]);

        $CI->app_menu->add_setup_children_item('inventory', [
                'slug'     => 'inventory_manufacturer',
                'name'     => _l('Inventory Manufacturer'),
                'href'     => admin_url('inventory_details/inventory_manufacturers'),
                'position' => 10,
        ]);

        $CI->app_menu->add_setup_children_item('inventory', [
                    'slug'     => 'inventory_product',
                    'name'     => _l('Inventory product'),
                    'href'     => admin_url('inventory_details/inventory_products'),
                    'position' => 15,
        ]);
        
        $CI->app_menu->add_setup_children_item('inventory', [
                    'slug'     => 'inventory_status',
                    'name'     => _l('Inventory status'),
                    'href'     => admin_url('inventory_details/inventory_statuses'),
                    'position' => 20,
        ]);
       

        

        $CI->app_menu->add_setup_menu_item('gaps', [
                'collapse' => true,
                'name'     => _l('GAPs'),
                'position' => 35,
        ]);
        $CI->app_menu->add_setup_children_item('gaps', [
                'slug'     => 'gaps-categories',
                'name'     => _l('Categories'),
                'href'     => admin_url('gaps/categories'),
                'position' => 5,
        ]);
        $CI->app_menu->add_setup_children_item('gaps', [
                'slug'     => 'gaps-impacts',
                'name'     => _l('Impacts'),
                'href'     => admin_url('gaps/impacts'),
                'position' => 5,
        ]);
        $CI->app_menu->add_setup_children_item('gaps', [
                'slug'     => 'gaps-statuses',
                'name'     => _l('Statuses'),
                'href'     => admin_url('gaps/statuses'),
                'position' => 5,
        ]);

        $CI->app_menu->add_setup_menu_item('renewals', [
                    'collapse' => true,
                    'name'     => _l('renewals'),
                    'position' => 36,
        ]);

        $CI->app_menu->add_setup_children_item('renewals', [
                    'slug'     => 'renewals_type',
                    'name'     => _l('Renewals type'),
                    'href'     => admin_url('renewals/renewals_types'),
                    'position' => 5,
        ]);

        $CI->app_menu->add_setup_children_item('renewals', [
                    'slug'     => 'renewals_vendor',
                    'name'     => _l('Renewals vendor'),
                    'href'     => admin_url('renewals/renewals_vendors'),
                    'position' => 10,
        ]);

        $modules_name = _l('modules');

        if ($modulesNeedsUpgrade = $CI->app_modules->number_of_modules_that_require_database_upgrade()) {
            $modules_name .= '<span class="badge menu-badge bg-warning">' . $modulesNeedsUpgrade . '</span>';
        }

        $CI->app_menu->add_setup_menu_item('modules', [
                    'href'     => admin_url('modules'),
                    'name'     => $modules_name,
                    'position' => 37,
            ]);

        $CI->app_menu->add_setup_menu_item('order_status', [
                    'href'     => admin_url('order_tracker/statuses'),
                    'name'     => 'Order Statuses',
                    'position' => 38,
            ]);

        $CI->app_menu->add_setup_menu_item('custom-fields', [
                    'href'     => admin_url('custom_fields'),
                    'name'     => _l('asc_custom_fields'),
                    'position' => 40,
            ]);

        $CI->app_menu->add_setup_menu_item('gdpr', [
                    'href'     => admin_url('gdpr'),
                    'name'     => _l('gdpr_short'),
                    'position' => 50,
            ]);

        $CI->app_menu->add_setup_menu_item('roles', [
                    'href'     => admin_url('roles'),
                    'name'     => _l('acs_roles'),
                    'position' => 55,
            ]);

/*             $CI->app_menu->add_setup_menu_item('api', [
                          'href'     => admin_url('api'),
                          'name'     => 'API',
                          'position' => 65,
                  ]);*/

    }

    if (has_permission('settings', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('settings', [
                    'href'     => admin_url('settings'),
                    'name'     => _l('acs_settings'),
                    'position' => 200,
            ]);
    }

    if (has_permission('email_templates', '', 'view')) {
        $CI->app_menu->add_setup_menu_item('email-templates', [
                    'href'     => admin_url('emails'),
                    'name'     => _l('acs_email_templates'),
                    'position' => 40,
            ]);
    }
}