<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Render admin service_requests table
 * @param string  $name        table name
 * @param boolean $bulk_action include checkboxes on the left side for bulk actions
 */
function AdminServiceRequestsTableStructure($name = '', $bulk_action = false)
{
    //$table = '<table class="table customizable-table service_requests-table dt-table-loading  table-service_requests" id="table-service_requests" data-last-order-identifier="service_requests" data-default-order="' . get_table_last_order('service_requests') . '">';
    
    $table = '<table class="table customizable-table dt-table-loading ' . ($name == '' ? 'service_requests-table' : $name) . ' table-service_requests" id="table-service_requests" data-last-order-identifier="service_requests" data-default-order="' . get_table_last_order('service_requests') . '">';
    $table .= '<thead>';
    $table .= '<tr>';
    $table .= '<th class="' . ($bulk_action == true ? '' : 'not_visible') . '">';
    $table .= '<span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="service_requests"><label></label></div>';
    $table .= '</th>';

    $table .= '<th class="toggleable" id="th-number">' . _l('the_number_sign') . '</th>';
    $table .= '<th class="toggleable" id="th-subject">' . _l('service_request_dt_subject') . '</th>';
    $table .= '<th class="toggleable" id="th-submitter">' . _l('service_request_dt_submitter') . '</th>';
    $table .= '<th class="toggleable" id="th">' . _l('Assigned To') . '</th>';
    // $table .= '<th class="toggleable" id="th-tags">' . _l('tags') . '</th>';
    // $table .= '<th class="toggleable" id="th-department">' . _l('service_request_dt_department') . '</th>';
    $services_th_attrs = '';
    if (get_option('services') == 0) {
        $services_th_attrs = ' class="not_visible"';
    }
    $table .= '<th' . $services_th_attrs . '>' . _l('service_request_dt_service') . '</th>';
    $table .= '<th class="toggleable" id="th">' . _l('Request For') . '</th>';
    $table .= '<th class="toggleable" id="th-status">' . _l('service_request_dt_status') . '</th>';
    $table .= '<th class="toggleable" id="th-priority">' . _l('service_request_dt_priority') . '</th>';
    $table .= '<th class="toggleable" id="th-last-reply">' . _l('service_request_dt_last_reply') . '</th>';

    $table .= '<th class="toggleable" id="th-time">' . _l('Approval Status') . '</th>';
    $table .= '<th class="toggleable service_request_created_column" id="th-created">' . _l('service_request_date_created') . '</th>';

    

    $custom_fields = get_table_custom_fields('service_requests');
    
    foreach ($custom_fields as $field) {
        //print_r($field);
        $table .= '<th>' . $field['name'] . '</th>';
    }
    //print_r($custom_fields);

    $table .= '</tr>';
    $table .= '</thead>';
    $table .= '<tbody style="color:#008ece;"></tbody>';
    $table .= '</table>';

    $table .= '<script id="hidden-columns-table-service_requests" type="text/json">';
    $table .= get_staff_meta(get_staff_user_id(), 'hidden-columns-table-service_requests');
    $table .= '</script>';
    
    return $table;
}

/**
 * Function to translate service_request status
 * The app offers ability to translate service_request status no matter if they are stored in database
 * @param  mixed $id
 * @return string
 */
function service_request_status_translate($id)
{	
    if ($id == '' || is_null($id)) {
        return '';
    }
    //$line = _l('service_request_status_db_' . $id, '', false);
    //if ($line == 'db_translate_not_found') {
        $CI = & get_instance();
        $CI->db->where('service_requeststatusid', $id);
        $status = $CI->db->get(db_prefix() . 'service_requests_status')->row();
        return !$status ? '' : $status->name;
    //}
    return $line;
}

/**
 * Function to translate service_request priority
 * The apps offers ability to translate service_request priority no matter if they are stored in database
 * @param  mixed $id
 * @return string
 */
function service_request_priority_translate($id)
{
    if ($id == '' || is_null($id)) {
        return '';
    }

    $line = _l('service_request_priority_db_' . $id, '', false);

    if ($line == 'db_translate_not_found') {
        $CI = & get_instance();
        $CI->db->where('priorityid', $id);
        $priority = $CI->db->get(db_prefix() . 'service_requests_priorities')->row();

        return !$priority ? '' : $priority->name;
    }
    // echo $line;die;
    return $line;
}

/**
 * When service_request will be opened automatically set to open
 * @param integer  $current Current status
 * @param integer  $id      service_requestid
 * @param boolean $admin   Admin opened or client opened
 */
function set_service_request_open($current, $id, $admin = true)
{
    if ($current == 1) {
        return;
    }

    $field = ($admin == false ? 'clientread' : 'adminread');

    $CI = & get_instance();
    $CI->db->where('service_requestid', $id);

    $CI->db->update(db_prefix() . 'service_requests', [
        $field => 1,
    ]);
}

/**
 * Check whether to show service_request submitter on clients area table based on applied settings and contact
 * @since  2.3.2
 * @return boolean
 */
function show_service_request_submitter_on_clients_area_table()
{
    $show_submitter_on_table = true;
    if (!can_logged_in_contact_view_all_service_requests()) {
        $show_submitter_on_table = false;
    }

    return hooks()->apply_filters('show_service_request_submitter_on_clients_area_table', $show_submitter_on_table);
}

/**
 * Check whether the logged in contact can view all service_requests in customers area
 * @since  2.3.2
 * @return boolean
 */
function can_logged_in_contact_view_all_service_requests()
{
    return !(!is_primary_contact() && get_option('only_show_contact_service_requests') == 1);
}

/**
 * Get clients area service_request summary statuses data
 * @since  2.3.2
 * @param  array $statuses  current statuses
 * @return array
 */
function get_clients_area_service_requests_summary($statuses)
{
    //print_r($statuses);die();
    foreach ($statuses as $key => $status) {
        $where = ['userid' => get_client_user_id(), 'status' => $status['service_requeststatusid']];
        if (!can_logged_in_contact_view_all_service_requests()) {
            $where[db_prefix() . 'service_requests.contactid'] = get_contact_user_id();
        }
        $statuses[$key]['total_service_requests']   = total_rows(db_prefix() . 'service_requests', $where);
        $statuses[$key]['translated_name'] = service_request_status_translate($status['service_requeststatusid']);
        $statuses[$key]['url']             = site_url('clients/service_requests/' . $status['service_requeststatusid']);
    }
    return hooks()->apply_filters('clients_area_service_requests_summary', $statuses);
}

/**
 * Check whether contact can change the service_request status (single service_request) in clients area
 * @since  2.3.2
 * @param  mixed $status  the status id, if not passed, will first check from settings
 * @return boolean
 */
function can_change_service_request_status_in_clients_area($status = null)
{
    $option = get_option('allow_customer_to_change_service_request_status');

    if (is_null($status)) {
        return $option == 1;
    }

    /*
    *   For all cases check the option too again, because if the option is set to No, no status changes on any status is allowed
     */
    if ($option == 0) {
        return false;
    }

    $forbidden = hooks()->apply_filters('forbidden_service_request_statuses_to_change_in_clients_area', [3, 4]);

    if (in_array($status, $forbidden)) {
        return false;
    }

    return true;
}

/**
 * For html5 form accepted attributes
 * This function is used for the service_requests form attachments
 * @return string
 */
function get_service_request_form_accepted_mimes()
{
    $service_request_allowed_extensions = get_option('service_request_attachments_file_extensions');

    $_service_request_allowed_extensions = array_map(function ($ext) {
        return trim($ext);
    }, explode(',', $service_request_allowed_extensions));

    $all_form_ext = str_replace([' '], '', $service_request_allowed_extensions);

    if (is_array($_service_request_allowed_extensions)) {
        foreach ($_service_request_allowed_extensions as $ext) {
            $all_form_ext .= ',' . get_mime_by_extension($ext);
        }
    }

    return $all_form_ext;
}

function service_request_message_save_as_predefined_reply_javascript()
{
    if (!is_admin() && get_option('staff_members_save_service_requests_predefined_replies') == '0') {
        return false;
    } ?>
<div class="modal fade" id="savePredefinedReplyFromMessageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo _l('predefined_replies_dt_name'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo render_input('name', 'predefined_reply_add_edit_name'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="button" class="btn btn-info"
                    id="saveServiceRequestMessagePredefinedReply"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function() {
    var editorMessage = tinymce.get('message');
    if (typeof(editorMessage) != 'undefined') {
        editorMessage.on('change', function() {
            if (editorMessage.getContent().trim() != '') {
                if ($('#savePredefinedReplyFromMessage').length == 0) {
                    $('[app-field-wrapper="message"] [role="menubar"] .mce-container-body:first')
                        .append(
                            "<a id=\"savePredefinedReplyFromMessage\" data-toggle=\"modal\" data-target=\"#savePredefinedReplyFromMessageModal\" class=\"save_predefined_reply_from_message pointer\" href=\"#\"><?php echo _l('save_message_as_predefined_reply'); ?></a>"
                            );
                }
                // For open is handled on contact select
                if ($('#single-service_request-form').length > 0) {
                    var contactIDSelect = $('#contactid');
                    if (contactIDSelect.data('no-contact') == undefined && contactIDSelect.data(
                            'service_request-emails') == '0') {
                        show_service_request_no_contact_email_warning($('input[name="userid"]').val(),
                            contactIDSelect.val());
                    } else {
                        clear_service_request_no_contact_email_warning();
                    }
                }
            } else {
                $('#savePredefinedReplyFromMessage').remove();
                clear_service_request_no_contact_email_warning();
            }
        });
    }
    $('body').on('click', '#saveServiceRequestMessagePredefinedReply', function(e) {
        e.preventDefault();
        var data = {}
        data.message = editorMessage.getContent();
        data.name = $('#savePredefinedReplyFromMessageModal #name').val();
        data.service_request_area = true;
        $.post(admin_url + 'service_requests/predefined_reply', data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                var predefined_reply_select = $('#insert_predefined_reply');
                predefined_reply_select.find('option:first').after('<option value="' + response
                    .id + '">' + data.name + '</option>');
                predefined_reply_select.selectpicker('refresh');
            }
            $('#savePredefinedReplyFromMessageModal').modal('hide');
        });
    });
});
</script>
<?php
}

function get_service_request_public_url($service_request)
{
    if (is_array($service_request)) {
        $service_request = array_to_object($service_request);
    }

    $CI = &get_instance();

    if (!$service_request->service_requestkey) {
        $CI->db->where('service_requestid', $service_request->service_requestid);
        $CI->db->update('service_requests', ['service_requestkey' => $key = app_generate_hash()]);
    } else {
        $key = $service_request->service_requestkey;
    }

    return site_url('forms/service_requests/' . $key);
}

function service_request_public_form_customers_footer() {
    // Create new listeners for the public_form
    // removes the one from clients.js (if loaded) and using new ones
    ?>
<style>
.single-service_request-project-area {
    display: none !important;
}
</style>
<script>
$(function() {
    setTimeout(function() {
        $('#service_request-reply').appFormValidator();

        $('.toggle-change-service_request-status').off('click');
        $('.toggle-change-service_request-status').on('click', function() {
            $('.service_request-status,.service_request-status-inline').toggleClass('hide');
        });

        $('#service_request_status_single').off('change');
        $('#service_request_status_single').on('change', function() {
            data = {};
            data.status_id = $(this).val();
            data.service_request_id = $('input[name="service_request_id"]').val();
            $.post(site_url + 'clients/change_service_request_status/', data).done(function() {
                window.location.reload();
            });
        });
    }, 2000)
})
</script>
<?php
}