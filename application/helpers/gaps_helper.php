<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Render admin gaps table
 * @param string  $name        table name
 * @param boolean $bulk_action include checkboxes on the left side for bulk actions
 */
function AdminGapsTableStructure($name = '', $bulk_action = false)
{
    //$table = '<table class="table customizable-table gaps-table dt-table-loading  table-gaps" id="table-gaps" data-last-order-identifier="gaps" data-default-order="' . get_table_last_order('gaps') . '">';
    $table = '<table class="table customizable-table dt-table-loading ' . ($name == '' ? 'gaps-table' : $name) . ' table-gaps" id="table-gaps" data-last-order-identifier="gaps" data-default-order="' . get_table_last_order('gaps') . '">';
    $table .= '<thead>';
    $table .= '<tr>';
    $table .= '<th class="' . ($bulk_action == true ? '' : 'not_visible') . '">';
    $table .= '<span class="hide"> - </span><div class="checkbox mass_select_all_wrap"><input type="checkbox" id="mass_select_all" data-to-table="gaps"><label></label></div>';
    $table .= '</th>';

    $table .= '<th class="toggleable" id="th-number">' . _l('the_number_sign') . '</th>';
    $table .= '<th class="toggleable" id="th-subject">' . _l('gap_dt_subject') . '</th>';
    $table .= '<th class="toggleable" id="th-tags">' . _l('tags') . '</th>';
    $table .= '<th class="toggleable" id="th-department">' . _l('gap_dt_department') . '</th>';
    $services_th_attrs = '';
    if (get_option('services') == 0) {
        $services_th_attrs = ' class="not_visible"';
    }
    $table .= '<th' . $services_th_attrs . '>' . _l('gap_dt_service') . '</th>';
    $table .= '<th class="toggleable" id="th-submitter">' . _l('gap_dt_submitter') . '</th>';
    $table .= '<th class="toggleable" id="th-status">' . _l('gap_dt_status') . '</th>';
    $table .= '<th class="toggleable" id="th-priority">' . _l('gap_dt_priority') . '</th>';
    $table .= '<th class="toggleable" id="th-last-reply">' . _l('gap_dt_last_reply') . '</th>';
    $table .= '<th class="toggleable gap_created_column" id="th-created">' . _l('gap_date_created') . '</th>';

    $table .= '<th class="toggleable" id="th-time">' . _l('Approval Status') . '</th>';

    $custom_fields = get_table_custom_fields('gaps');
    
    foreach ($custom_fields as $field) {
        //print_r($field);
        $table .= '<th>' . $field['name'] . '</th>';
    }
    //print_r($custom_fields);

    $table .= '</tr>';
    $table .= '</thead>';
    $table .= '<tbody></tbody>';
    $table .= '</table>';

    $table .= '<script id="hidden-columns-table-gaps" type="text/json">';
    $table .= get_staff_meta(get_staff_user_id(), 'hidden-columns-table-gaps');
    $table .= '</script>';
    
    return $table;
}

/**
 * Function to translate gap status
 * The app offers ability to translate gap status no matter if they are stored in database
 * @param  mixed $id
 * @return string
 */
function gap_status_translate($id)
{	
    if ($id == '' || is_null($id)) {
        return '';
    }
    //$line = _l('gap_status_db_' . $id, '', false);
    //if ($line == 'db_translate_not_found') {
        $CI = & get_instance();
        $CI->db->where('id', $id);
        $status = $CI->db->get(db_prefix() . 'gap_status')->row();
        return !$status ? '' : $status->name;
    //}
    return $line;
}

/**
 * Function to translate gap priority
 * The apps offers ability to translate gap priority no matter if they are stored in database
 * @param  mixed $id
 * @return string
 */
function gap_priority_translate($id)
{
    if ($id == '' || is_null($id)) {
        return '';
    }

    $line = _l('gap_priority_db_' . $id, '', false);

    if ($line == 'db_translate_not_found') {
        $CI = & get_instance();
        $CI->db->where('priorityid', $id);
        $priority = $CI->db->get(db_prefix() . 'gaps_priorities')->row();

        return !$priority ? '' : $priority->name;
    }
    // echo $line;die;
    return $line;
}

/**
 * When gap will be opened automatically set to open
 * @param integer  $current Current status
 * @param integer  $id      gapid
 * @param boolean $admin   Admin opened or client opened
 */
function set_gap_open($current, $id, $admin = true)
{
    if ($current == 1) {
        return;
    }

    $field = ($admin == false ? 'clientread' : 'adminread');

    $CI = & get_instance();
    $CI->db->where('gapid', $id);

    $CI->db->update(db_prefix() . 'gaps', [
        $field => 1,
    ]);
}

/**
 * Check whether to show gap submitter on clients area table based on applied settings and contact
 * @since  2.3.2
 * @return boolean
 */
function show_gap_submitter_on_clients_area_table()
{
    $show_submitter_on_table = true;
    if (!can_logged_in_contact_view_all_gaps()) {
        $show_submitter_on_table = false;
    }

    return hooks()->apply_filters('show_gap_submitter_on_clients_area_table', $show_submitter_on_table);
}

/**
 * Check whether the logged in contact can view all gaps in customers area
 * @since  2.3.2
 * @return boolean
 */
function can_logged_in_contact_view_all_gaps()
{
    return !(!is_primary_contact() && get_option('only_show_contact_gaps') == 1);
}

/**
 * Get clients area gap summary statuses data
 * @since  2.3.2
 * @param  array $statuses  current statuses
 * @return array
 */
function get_clients_area_gaps_summary($statuses)
{
    //print_r($statuses);die();
    foreach ($statuses as $key => $status) {
        $where = ['userid' => get_client_user_id(), 'status' => $status['gapstatusid']];
        if (!can_logged_in_contact_view_all_gaps()) {
            $where[db_prefix() . 'gaps.contactid'] = get_contact_user_id();
        }
        $statuses[$key]['total_gaps']   = total_rows(db_prefix() . 'gaps', $where);
        $statuses[$key]['translated_name'] = gap_status_translate($status['gapstatusid']);
        $statuses[$key]['url']             = site_url('clients/gaps/' . $status['gapstatusid']);
    }
    return hooks()->apply_filters('clients_area_gaps_summary', $statuses);
}

/**
 * Check whether contact can change the gap status (single gap) in clients area
 * @since  2.3.2
 * @param  mixed $status  the status id, if not passed, will first check from settings
 * @return boolean
 */
function can_change_gap_status_in_clients_area($status = null)
{
    $option = get_option('allow_customer_to_change_gap_status');

    if (is_null($status)) {
        return $option == 1;
    }

    /*
    *   For all cases check the option too again, because if the option is set to No, no status changes on any status is allowed
     */
    if ($option == 0) {
        return false;
    }

    $forbidden = hooks()->apply_filters('forbidden_gap_statuses_to_change_in_clients_area', [3, 4]);

    if (in_array($status, $forbidden)) {
        return false;
    }

    return true;
}

/**
 * For html5 form accepted attributes
 * This function is used for the gaps form attachments
 * @return string
 */
function get_gap_form_accepted_mimes()
{
    $gap_allowed_extensions = get_option('gap_attachments_file_extensions');

    $_gap_allowed_extensions = array_map(function ($ext) {
        return trim($ext);
    }, explode(',', $gap_allowed_extensions));

    $all_form_ext = str_replace([' '], '', $gap_allowed_extensions);

    if (is_array($_gap_allowed_extensions)) {
        foreach ($_gap_allowed_extensions as $ext) {
            $all_form_ext .= ',' . get_mime_by_extension($ext);
        }
    }

    return $all_form_ext;
}

function gap_message_save_as_predefined_reply_javascript()
{
    if (!is_admin() && get_option('staff_members_save_gaps_predefined_replies') == '0') {
        return false;
    } ?>
<div class="modal fade" id="savePredefinedReplyFromMessageModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo _l('predefined_replies_dt_name'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo render_input('name', 'predefined_reply_add_edit_name'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="button" class="btn btn-info"
                    id="saveServiceRequestMessagePredefinedReply"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function() {
    var editorMessage = tinymce.get('message');
    if (typeof(editorMessage) != 'undefined') {
        editorMessage.on('change', function() {
            if (editorMessage.getContent().trim() != '') {
                if ($('#savePredefinedReplyFromMessage').length == 0) {
                    $('[app-field-wrapper="message"] [role="menubar"] .mce-container-body:first')
                        .append(
                            "<a id=\"savePredefinedReplyFromMessage\" data-toggle=\"modal\" data-target=\"#savePredefinedReplyFromMessageModal\" class=\"save_predefined_reply_from_message pointer\" href=\"#\"><?php echo _l('save_message_as_predefined_reply'); ?></a>"
                            );
                }
                // For open is handled on contact select
                if ($('#single-gap-form').length > 0) {
                    var contactIDSelect = $('#contactid');
                    if (contactIDSelect.data('no-contact') == undefined && contactIDSelect.data(
                            'gap-emails') == '0') {
                        show_gap_no_contact_email_warning($('input[name="userid"]').val(),
                            contactIDSelect.val());
                    } else {
                        clear_gap_no_contact_email_warning();
                    }
                }
            } else {
                $('#savePredefinedReplyFromMessage').remove();
                clear_gap_no_contact_email_warning();
            }
        });
    }
    $('body').on('click', '#saveServiceRequestMessagePredefinedReply', function(e) {
        e.preventDefault();
        var data = {}
        data.message = editorMessage.getContent();
        data.name = $('#savePredefinedReplyFromMessageModal #name').val();
        data.gap_area = true;
        $.post(admin_url + 'gaps/predefined_reply', data).done(function(response) {
            response = JSON.parse(response);
            if (response.success == true) {
                var predefined_reply_select = $('#insert_predefined_reply');
                predefined_reply_select.find('option:first').after('<option value="' + response
                    .id + '">' + data.name + '</option>');
                predefined_reply_select.selectpicker('refresh');
            }
            $('#savePredefinedReplyFromMessageModal').modal('hide');
        });
    });
});
</script>
<?php
}

function get_gap_public_url($gap)
{
    if (is_array($gap)) {
        $gap = array_to_object($gap);
    }

    $CI = &get_instance();

    if (!$gap->gapkey) {
        $CI->db->where('gapid', $gap->gapid);
        $CI->db->update('gaps', ['gapkey' => $key = app_generate_hash()]);
    } else {
        $key = $gap->gapkey;
    }

    return site_url('forms/gaps/' . $key);
}

function gap_public_form_customers_footer() {
    // Create new listeners for the public_form
    // removes the one from clients.js (if loaded) and using new ones
    ?>
<style>
.single-gap-project-area {
    display: none !important;
}
</style>
<script>
$(function() {
    setTimeout(function() {
        $('#gap-reply').appFormValidator();

        $('.toggle-change-gap-status').off('click');
        $('.toggle-change-gap-status').on('click', function() {
            $('.gap-status,.gap-status-inline').toggleClass('hide');
        });

        $('#gap_status_single').off('change');
        $('#gap_status_single').on('change', function() {
            data = {};
            data.status_id = $(this).val();
            data.gap_id = $('input[name="gap_id"]').val();
            $.post(site_url + 'clients/change_gap_status/', data).done(function() {
                window.location.reload();
            });
        });
    }, 2000)
})
</script>
<?php
}