<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Get boq short_url
 * @since  Version 2.7.3
 * @param  object $boq
 * @return string Url
 */
function get_boq_shortlink($boq)
{
    $long_url = site_url("invoice/{$boq->id}/{$boq->hash}");
    if (!get_option('bitly_access_token')) {
        return $long_url;
    }

    // Check if boq has short link, if yes return short link
    if (!empty($boq->short_link)) {
        return $boq->short_link;
    }

    // Create short link and return the newly created short link
    $short_link = app_generate_short_link([
        'long_url'  => $long_url,
        'title'     => format_boq_number($boq->id)
    ]);

    if ($short_link) {
        $CI = &get_instance();
        $CI->db->where('id', $boq->id);
        $CI->db->update(db_prefix() . 'boqs', [
            'short_link' => $short_link
        ]);
        return $short_link;
    }
    return $long_url;
}

/**
 * Check if boq hash is equal
 * @param  mixed $id   boq id
 * @param  string $hash boq hash
 * @return void
 */
function check_boq_restrictions($id, $hash)
{
    $CI = &get_instance();
    $CI->load->model('boqs_model');
    if (!$hash || !$id) {
        show_404();
    }
    $boq = $CI->boqs_model->get($id);
    if (!$boq || ($boq->hash != $hash)) {
        show_404();
    }
}

/**
 * Check if boq email template for expiry reminders is enabled
 * @return boolean
 */
function is_boqs_email_expiry_reminder_enabled()
{
    return total_rows(db_prefix() . 'emailtemplates', ['slug' => 'boq-expiry-reminder', 'active' => 1]) > 0;
}

/**
 * Check if there are sources for sending boq expiry reminders
 * Will be either email or SMS
 * @return boolean
 */
function is_boqs_expiry_reminders_enabled()
{
    return is_boqs_email_expiry_reminder_enabled() || is_sms_trigger_active(SMS_TRIGGER_boq_EXP_REMINDER);
}

/**
 * Return boq status color class based on twitter bootstrap
 * @param  mixed  $id
 * @param  boolean $replace_default_by_muted
 * @return string
 */
function boq_status_color_class($id, $replace_default_by_muted = false)
{
    if ($id == 1) {
        $class = 'default';
    } elseif ($id == 2) {
        $class = 'danger';
    } elseif ($id == 3) {
        $class = 'success';
    } elseif ($id == 4 || $id == 5) {
        // status sent and revised
        $class = 'info';
    } elseif ($id == 6) {
        $class = 'default';
    }
    if ($class == 'default') {
        if ($replace_default_by_muted == true) {
            $class = 'muted';
        }
    }

    return $class;
}
/**
 * Format boq status with label or not
 * @param  mixed  $status  boq status id
 * @param  string  $classes additional label classes
 * @param  boolean $label   to include the label or return just translated text
 * @return string
 */
function format_boq_status($status, $classes = '', $label = true)
{
    $id = $status;
    if ($status == 1) {
        $status      = _l('boq_status_open');
        $label_class = 'default';
    } elseif ($status == 2) {
        $status      = _l('boq_status_declined');
        $label_class = 'danger';
    } elseif ($status == 3) {
        $status      = _l('boq_status_accepted');
        $label_class = 'success';
    } elseif ($status == 4) {
        $status      = _l('boq_status_sent');
        $label_class = 'info';
    } elseif ($status == 5) {
        $status      = _l('boq_status_revised');
        $label_class = 'info';
    } elseif ($status == 6) {
        $status      = _l('boq_status_draft');
        $label_class = 'default';
    }

    if ($label == true) {
        return '<span class="label label-' . $label_class . ' ' . $classes . ' s-status boq-status-' . $id . '">' . $status . '</span>';
    }

    return $status;
}

/**
 * Function that format boq number based on the prefix option and the boq id
 * @param  mixed $id boq id
 * @return string
 */
function format_boq_number($id)
{
    return get_option('boq_number_prefix') . str_pad($id, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT);
}


/**
 * Function that return boq item taxes based on passed item id
 * @param  mixed $itemid
 * @return array
 */
function get_boq_item_taxes($itemid)
{
    $CI = &get_instance();
    $CI->db->where('itemid', $itemid);
    $CI->db->where('rel_type', 'boq');
    $taxes = $CI->db->get(db_prefix() . 'item_tax')->result_array();
    $i     = 0;
    foreach ($taxes as $tax) {
        $taxes[$i]['taxname'] = $tax['taxname'] . '|' . $tax['taxrate'];
        $i++;
    }

    return $taxes;
}


/**
 * Calculate boq percent by status
 * @param  mixed $status          boq status
 * @param  mixed $total_estimates in case the total is calculated in other place
 * @return array
 */
function get_boqs_percent_by_status($status, $total_boqs = '')
{
    $has_permission_view                 = has_permission('boqs', '', 'view');
    $has_permission_view_own             = has_permission('boqs', '', 'view_own');
    $allow_staff_view_boqs_assigned = get_option('allow_staff_view_boqs_assigned');
    $staffId                             = get_staff_user_id();

    $whereUser = '';
    if (!$has_permission_view) {
        if ($has_permission_view_own) {
            $whereUser = '(addedfrom=' . $staffId;
            if ($allow_staff_view_boqs_assigned == 1) {
                $whereUser .= ' OR assigned=' . $staffId;
            }
            $whereUser .= ')';
        } else {
            $whereUser .= 'assigned=' . $staffId;
        }
    }

    if (!is_numeric($total_boqs)) {
        $total_boqs = total_rows(db_prefix() . 'boqs', $whereUser);
    }

    $data            = [];
    $total_by_status = 0;
    $where           = 'status=' . get_instance()->db->escape_str($status);
    if (!$has_permission_view) {
        $where .= ' AND (' . $whereUser . ')';
    }

    $total_by_status = total_rows(db_prefix() . 'boqs', $where);
    $percent         = ($total_boqs > 0 ? number_format(($total_by_status * 100) / $total_boqs, 2) : 0);

    $data['total_by_status'] = $total_by_status;
    $data['percent']         = $percent;
    $data['total']           = $total_boqs;

    return $data;
}

/**
 * Function that will search possible boq templates in applicaion/views/admin/boq/templates
 * Will return any found files and user will be able to add new template
 * @return array
 */
function get_boq_templates()
{
    $boq_templates = [];
    if (is_dir(VIEWPATH . 'admin/boqs/templates')) {
        foreach (list_files(VIEWPATH . 'admin/boqs/templates') as $template) {
            $boq_templates[] = $template;
        }
    }

    return $boq_templates;
}
/**
 * Check if staff member can view boq
 * @param  mixed $id boq id
 * @param  mixed $staff_id
 * @return boolean
 */
function user_can_view_boq($id, $staff_id = false)
{
    $CI = &get_instance();

    $staff_id = $staff_id ? $staff_id : get_staff_user_id();

    if (has_permission('boqs', $staff_id, 'view')) {
        return true;
    }

    $CI->db->select('id, addedfrom, assigned');
    $CI->db->from(db_prefix() . 'boqs');
    $CI->db->where('id', $id);
    $boq = $CI->db->get()->row();

    if ((has_permission('boqs', $staff_id, 'view_own') && $boq->addedfrom == $staff_id)
        || ($boq->assigned == $staff_id && get_option('allow_staff_view_boqs_assigned') == 1)
    ) {
        return true;
    }

    return false;
}
function parse_boq_content_merge_fields($boq)
{
    $id = is_array($boq) ? $boq['id'] : $boq->id;
    $CI = &get_instance();

    $CI->load->library('merge_fields/boqs_merge_fields');
    $CI->load->library('merge_fields/other_merge_fields');

    $merge_fields = [];
    $merge_fields = array_merge($merge_fields, $CI->boqs_merge_fields->format($id));
    $merge_fields = array_merge($merge_fields, $CI->other_merge_fields->format());
    foreach ($merge_fields as $key => $val) {
        $content = is_array($boq) ? $boq['content'] : $boq->content;

        if (stripos($content, $key) !== false) {
            if (is_array($boq)) {
                $boq['content'] = str_ireplace($key, $val, $content);
            } else {
                $boq->content = str_ireplace($key, $val, $content);
            }
        } else {
            if (is_array($boq)) {
                $boq['content'] = str_ireplace($key, '', $content);
            } else {
                $boq->content = str_ireplace($key, '', $content);
            }
        }
    }

    return $boq;
}

/**
 * Check if staff member have assigned boqs / added as sale agent
 * @param  mixed $staff_id staff id to check
 * @return boolean
 */
function staff_has_assigned_boqs($staff_id = '')
{
    $CI       = &get_instance();
    $staff_id = is_numeric($staff_id) ? $staff_id : get_staff_user_id();
    $cache    = $CI->app_object_cache->get('staff-total-assigned-boqs-' . $staff_id);
    if (is_numeric($cache)) {
        $result = $cache;
    } else {
        $result = total_rows(db_prefix() . 'boqs', ['assigned' => $staff_id]);
        $CI->app_object_cache->add('staff-total-assigned-boqs-' . $staff_id, $result);
    }

    return $result > 0 ? true : false;
}

function get_boqs_sql_where_staff($staff_id)
{
    $has_permission_view_own            = has_permission('boqs', '', 'view_own');
    $allow_staff_view_invoices_assigned = get_option('allow_staff_view_boqs_assigned');
    $CI                                 = &get_instance();

    $whereUser = '';
    if ($has_permission_view_own) {
        $whereUser = '((' . db_prefix() . 'boqs.addedfrom=' . $CI->db->escape_str($staff_id) . ' AND ' . db_prefix() . 'boqs.addedfrom IN (SELECT staff_id FROM ' . db_prefix() . 'staff_permissions WHERE feature = "boqs" AND capability="view_own"))';
        if ($allow_staff_view_invoices_assigned == 1) {
            $whereUser .= ' OR assigned=' . $CI->db->escape_str($staff_id);
        }
        $whereUser .= ')';
    } else {
        $whereUser .= 'assigned=' . $CI->db->escape_str($staff_id);
    }

    return $whereUser;
}
