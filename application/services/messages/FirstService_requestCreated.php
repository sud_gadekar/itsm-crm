<?php

namespace app\services\messages;

defined('BASEPATH') or exit('No direct script access allowed');

use app\services\messages\AbstractPopupMessage;

class FirstService_requestCreated extends AbstractPopupMessage
{
    public function isVisible(...$params)
    {
        $service_request_id = $params[0];

        return $service_request_id == 1;
    }

    public function getMessage(...$params)
    {
        return 'First SR Created! <br /> <span style="font-size:26px;">Did you know that you can embed SR Form (Setup->Settings->Support->SR Form) directly in your websites?</span>';
    }
}
