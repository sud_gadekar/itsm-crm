-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for crm
CREATE DATABASE IF NOT EXISTS `crm` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `crm`;

-- Dumping structure for table crm.tblactivity_log
CREATE TABLE IF NOT EXISTS `tblactivity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext NOT NULL,
  `date` datetime NOT NULL,
  `staffid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staffid` (`staffid`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblactivity_log: ~256 rows (approximately)
/*!40000 ALTER TABLE `tblactivity_log` DISABLE KEYS */;
INSERT INTO `tblactivity_log` (`id`, `description`, `date`, `staffid`) VALUES
	(1, 'Staff Member Updated [ID: 5, User User]', '2021-05-12 14:47:51', 'Admin Admin'),
	(2, 'Staff Member Updated [ID: 5, User User]', '2021-05-12 14:49:14', 'Admin Admin'),
	(3, 'Staff Member Updated [ID: 5, User User]', '2021-05-13 16:51:31', 'Admin Admin'),
	(4, 'Email Template Updated [New Lead Assigned to Staff Member]', '2021-05-17 18:54:44', 'Admin Admin'),
	(5, 'New Department Added [Salse, ID: 3]', '2021-05-18 12:43:01', 'Admin Admin'),
	(6, 'Staff Member Updated [ID: 4, Deepak G]', '2021-05-18 12:43:33', 'Admin Admin'),
	(7, 'New Role Added [ID: 3.Sales Manager]', '2021-05-18 15:58:38', 'Admin Admin'),
	(8, 'Lead Updated [ID: 4]', '2021-05-18 16:04:51', 'Admin Admin'),
	(9, 'New Lead Added [ID: 5]', '2021-05-18 16:25:10', 'Admin Admin'),
	(10, 'Failed to send email template - SMTP connect() failed. https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting<br /><pre>\n\n</pre>', '2021-05-18 16:25:31', 'Admin Admin'),
	(11, 'New Lead Added [ID: 6]', '2021-05-19 18:43:03', 'Admin Admin'),
	(12, 'Email Sent To [Email: deepak@gmail.com, Template: New Lead Assigned to Staff Member]', '2021-05-19 18:43:11', 'Admin Admin'),
	(13, 'New Lead Added [ID: 7]', '2021-05-19 18:47:08', 'Admin Admin'),
	(14, 'Email Sent To [Email: deepak@gmail.com, Template: New Lead Assigned to Staff Member]', '2021-05-19 18:47:11', 'Admin Admin'),
	(15, 'Leads Status Updated [StatusID: 2, Name: New Lead]', '2021-05-24 12:48:53', 'Admin Admin'),
	(16, 'New Leads Status Added [StatusID: 3, Name: Open Won]', '2021-05-24 12:50:58', 'Admin Admin'),
	(17, 'New Leads Status Added [StatusID: 4, Name: Closed Won]', '2021-05-24 12:51:55', 'Admin Admin'),
	(18, 'New Leads Status Added [StatusID: 5, Name: Closed Won]', '2021-05-24 12:51:57', 'Admin Admin'),
	(19, 'New Leads Status Added [StatusID: 6, Name: Closed Lost]', '2021-05-24 12:52:20', 'Admin Admin'),
	(20, 'Leads Status Deleted [StatusID: 5]', '2021-05-24 12:53:06', 'Admin Admin'),
	(21, 'Staff Member Updated [ID: 1, Admin Admin]', '2021-05-25 12:34:53', 'Admin Admin'),
	(22, 'New Lead Added [ID: 8]', '2021-05-26 12:45:26', 'Admin Admin'),
	(23, 'Email Sent To [Email: deepak@gmail.com, Template: New Lead Assigned to Staff Member]', '2021-05-26 12:45:37', 'Admin Admin'),
	(24, 'Cron Invoked Manually', '2021-05-26 18:06:17', 'Admin Admin'),
	(25, 'Email Sent To [Email: deepak@gmail.com, Template: New Task Assigned (Sent to Staff)]', '2021-05-26 18:06:26', 'Admin Admin'),
	(26, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: Yes, IP: ::1]', '2021-06-15 10:41:16', NULL),
	(27, 'New Ticket Status Added [ID: 6, Good]', '2021-06-16 17:18:35', 'Admin Admin'),
	(28, 'New Checklist Status Added [ID: 1, Good]', '2021-06-16 17:22:15', 'Admin Admin'),
	(29, 'New Checklist Status Added [ID: 2, Critical]', '2021-06-16 17:42:12', 'Admin Admin'),
	(30, 'Checklist Status Updated [ID: 2 Name: Critical Edit]', '2021-06-16 17:42:33', 'Admin Admin'),
	(31, 'Checklist Status Updated [ID: 2 Name: Critical]', '2021-06-16 17:43:37', 'Admin Admin'),
	(32, 'New Checklist Status Added [ID: 3, Warning]', '2021-06-16 17:44:17', 'Admin Admin'),
	(33, 'New Health Checklist Added [2]', '2021-06-18 12:20:13', 'Admin Admin'),
	(34, 'New Article Group Added [GroupID: 1]', '2021-06-23 10:05:05', 'Admin Admin'),
	(35, 'New Article Added [ArticleID: 2 GroupID: 1]', '2021-06-23 14:32:34', 'Admin Admin'),
	(36, 'Article Updated [ArticleID: 2]', '2021-06-23 14:37:30', 'Admin Admin'),
	(37, 'New Order Status Added [ID: 1, Placed]', '2021-06-28 16:13:34', 'Admin Admin'),
	(38, 'Order Status Updated [ID: 1 Name: Placed Edit]', '2021-06-28 16:14:48', 'Admin Admin'),
	(39, 'Order Status Updated [ID: 1 Name: Placed]', '2021-06-28 16:14:57', 'Admin Admin'),
	(40, 'New Order Status Added [ID: 2, Shipped]', '2021-06-28 16:15:16', 'Admin Admin'),
	(41, 'New Order Added [2]', '2021-06-29 18:52:26', 'Admin Admin'),
	(42, 'New Order Added [3]', '2021-06-29 19:31:05', 'Admin Admin'),
	(43, 'New Order Status Added [1]', '2021-06-29 19:31:05', 'Admin Admin'),
	(44, 'New Order Status Added [2]', '2021-06-29 19:32:01', 'Admin Admin'),
	(45, 'Contact Updated [ID: 31]', '2021-07-01 10:46:57', 'Admin Admin'),
	(46, 'Contact Updated [ID: 20]', '2021-07-06 11:12:23', 'Admin Admin'),
	(47, 'Contact Updated [ID: 3]', '2021-07-06 11:12:39', 'Admin Admin'),
	(48, 'Failed to send email template - SMTP connect() failed. https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting<br /><pre>\n\n</pre>', '2021-07-06 12:04:14', 'Admin Admin'),
	(49, 'Contact Created [ID: 39]', '2021-07-06 12:04:14', 'Admin Admin'),
	(50, 'Contact Updated [ID: 37]', '2021-07-08 17:29:54', 'Admin Admin'),
	(51, 'Failed Login Attempt [Email: akshayd@gmail.com, Is Staff Member: No, IP: ::1]', '2021-07-09 15:51:35', NULL),
	(52, 'Contact Updated [ID: 39]', '2021-07-09 15:53:12', 'Admin Admin'),
	(53, 'New Health Checklist Added [1]', '2021-07-15 15:04:10', 'Admin Admin'),
	(54, 'New Health Checklist Added [2]', '2021-07-15 19:39:20', 'Admin Admin'),
	(55, 'New Task Added [ID:13, Name: Test Task]', '2021-07-16 17:13:38', 'Admin Admin'),
	(56, 'New Task Added [ID:14, Name: New Checklist Task]', '2021-07-16 17:17:55', 'Admin Admin'),
	(57, 'New Task Added [ID:15, Name: Akshay B Dange]', '2021-07-16 17:58:21', 'Admin Admin'),
	(58, 'New Order Status Added [3]', '2021-07-20 10:52:07', 'Admin Admin'),
	(59, 'New Order Status Added [4]', '2021-07-20 11:03:08', 'Admin Admin'),
	(60, 'New Order Status Added [5]', '2021-07-20 11:11:03', 'Admin Admin'),
	(61, 'New Order Status Added [6]', '2021-07-20 11:12:10', 'Admin Admin'),
	(62, 'New Order Added [4]', '2021-07-20 11:21:01', 'Admin Admin'),
	(63, 'New Order Status Added [7]', '2021-07-20 11:21:01', 'Admin Admin'),
	(64, 'New Order Added [5]', '2021-07-20 12:43:22', 'Admin Admin'),
	(65, 'New Order Status Added [8]', '2021-07-20 12:43:22', 'Admin Admin'),
	(66, 'New Order Status Added [9]', '2021-07-20 12:47:04', 'Admin Admin'),
	(67, 'New Order Status Added [10]', '2021-07-20 12:47:40', 'Admin Admin'),
	(68, 'New Order Note Added [1]', '2021-07-20 13:21:49', 'Admin Admin'),
	(69, 'New Order Note Added [2]', '2021-07-20 13:22:29', 'Admin Admin'),
	(70, 'New Order Note Added [3]', '2021-07-20 13:23:12', 'Admin Admin'),
	(71, 'New Order Status Added [11]', '2021-07-20 16:16:55', 'Admin Admin'),
	(72, 'New Order Status Added [12]', '2021-07-20 16:19:13', 'Admin Admin'),
	(73, 'New Order Added [6]', '2021-07-20 16:21:51', 'Admin Admin'),
	(74, 'New Order Status Added [13]', '2021-07-20 16:21:51', 'Admin Admin'),
	(75, 'New Order Note Added [4]', '2021-07-20 16:22:15', 'Admin Admin'),
	(76, 'New Order Added [7]', '2021-07-26 10:34:13', 'Admin Admin'),
	(77, 'New Order Status Added [14]', '2021-07-26 10:34:14', 'Admin Admin'),
	(78, 'Email Template Updated [Send Order Note to Client]', '2021-07-26 15:58:15', 'Admin Admin'),
	(79, 'New Order Status Added [15]', '2021-07-26 16:11:59', 'Admin Admin'),
	(80, 'New Order Note Added [12]', '2021-07-26 16:12:24', 'Admin Admin'),
	(81, 'New Order Note Added [13]', '2021-07-26 16:15:21', 'Admin Admin'),
	(82, 'New Order Note Added [14]', '2021-07-26 16:16:27', 'Admin Admin'),
	(83, 'New Order Note Added [15]', '2021-07-26 16:17:10', 'Admin Admin'),
	(84, 'New Proposal Created [ID: 1]', '2021-08-02 11:33:05', 'Admin Admin'),
	(85, 'New Reminder Added [ProposalID: 1 Description: Testing]', '2021-08-02 11:35:30', 'Admin Admin'),
	(86, 'New boq Created [ID: 2]', '2021-08-04 11:44:13', 'Admin Admin'),
	(87, 'boq Updated [ID:2]', '2021-08-04 11:55:43', 'Admin Admin'),
	(88, 'boq Updated [ID:2]', '2021-08-04 11:56:16', 'Admin Admin'),
	(89, 'boq Converted to Estimate [EstimateID: 1, boqID: 2]', '2021-08-04 11:58:30', 'Admin Admin'),
	(90, 'New Order Status Added [16]', '2021-08-09 12:02:24', 'Admin Admin'),
	(91, 'New Order Note Added [16]', '2021-08-09 12:03:54', 'Admin Admin'),
	(92, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Send Order Note to Client]', '2021-08-09 12:22:24', 'Admin Admin'),
	(93, 'New Order Note Added [20]', '2021-08-09 12:22:24', 'Admin Admin'),
	(94, 'New Order Note Added [21]', '2021-08-09 12:27:02', 'Admin Admin'),
	(95, 'New Order Note Added [22]', '2021-08-09 12:28:00', 'Admin Admin'),
	(96, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Send Order Note to Client]', '2021-08-09 12:30:45', 'Admin Admin'),
	(97, 'New Order Note Added [25]', '2021-08-09 12:30:46', 'Admin Admin'),
	(98, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Send Order Note to Client]', '2021-08-09 16:40:52', 'Admin Admin'),
	(99, 'New Order Note Added [26]', '2021-08-09 16:40:52', 'Admin Admin'),
	(100, 'New Order Note Added [27]', '2021-08-09 16:45:28', 'Admin Admin'),
	(101, 'New Order Note Added [28]', '2021-08-09 16:46:27', 'Admin Admin'),
	(102, 'New Order Note Added [29]', '2021-08-09 16:47:45', 'Admin Admin'),
	(103, 'New Order Note Added [30]', '2021-08-09 16:48:11', 'Admin Admin'),
	(104, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Send Order Note to Client]', '2021-08-09 16:49:25', 'Admin Admin'),
	(105, 'New Order Note Added [31]', '2021-08-09 16:49:25', 'Admin Admin'),
	(106, 'New Order Note Added [32]', '2021-08-12 15:41:45', 'Admin Admin'),
	(107, 'New Currency Added [ID: INR]', '2021-08-16 18:57:34', 'Admin Admin'),
	(108, 'Currency Updated [EUR]', '2021-08-16 18:58:15', 'Admin Admin'),
	(109, 'New Invoice Item Added [ID:3, Test Item New]', '2021-08-16 19:17:12', 'Admin Admin'),
	(110, 'Invoice Item Updated [ID: 3, Test Item New]', '2021-08-17 11:14:29', 'Admin Admin'),
	(111, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: Yes, IP: ::1]', '2021-08-19 10:33:21', NULL),
	(112, 'Currency Updated [USD]', '2021-08-20 10:09:29', 'Admin Admin'),
	(113, 'Currency Updated [INR]', '2021-08-20 10:09:40', 'Admin Admin'),
	(114, 'New boq Created [ID: 3]', '2021-08-26 10:13:25', 'Admin Admin'),
	(115, 'Staff Member Updated [ID: 5, User User]', '2021-08-26 15:17:55', 'Admin Admin'),
	(116, 'Staff Member Updated [ID: 5, User User]', '2021-08-26 15:18:12', 'Admin Admin'),
	(117, 'Tried to access page where don\'t have permission [sales_order]', '2021-08-26 15:19:11', 'User User'),
	(118, 'New Order Added [8]', '2021-08-26 15:20:05', 'User User'),
	(119, 'New Order Status Added [17]', '2021-08-26 15:20:05', 'User User'),
	(120, 'Contact Updated [ID: 3]', '2021-08-26 18:12:50', 'Admin Admin'),
	(121, 'boq Updated [ID:3]', '2021-08-26 19:22:52', 'Admin Admin'),
	(122, 'boq Updated [ID:3]', '2021-08-26 19:31:04', 'Admin Admin'),
	(123, 'boq Updated [ID:3]', '2021-08-26 19:32:13', 'Admin Admin'),
	(124, 'boq Updated [ID:3]', '2021-08-26 19:32:41', 'Admin Admin'),
	(125, 'boq Updated [ID:3]', '2021-08-26 19:34:01', 'Admin Admin'),
	(126, 'New items region added [ID: 1, Name: Nashik]', '2021-08-27 14:32:12', 'Admin Admin'),
	(127, 'items region updated [ID: 1 Name: Nashik Road]', '2021-08-27 14:32:24', 'Admin Admin'),
	(128, 'New items region added [ID: 2, Name: Pune]', '2021-08-27 14:34:26', 'Admin Admin'),
	(129, 'New item manufacturer added [ID: 1, Name: EKC]', '2021-08-27 14:39:22', 'Admin Admin'),
	(130, 'item manufacturer updated [ID: 1 Name: EKC D]', '2021-08-27 14:39:33', 'Admin Admin'),
	(131, 'Invoice Item Updated [ID: 1, Test 1]', '2021-08-27 15:06:48', 'Admin Admin'),
	(132, 'New Invoice Item Added [ID:4, Pen Drive]', '2021-08-27 15:12:18', 'Admin Admin'),
	(133, 'New Invoice Item Added [ID:5, CFL]', '2021-09-01 16:28:39', 'Admin Admin'),
	(134, 'New Invoice Item Added [ID:6, Maruti WagonR Tyer]', '2021-09-07 11:32:49', 'Admin Admin'),
	(135, 'Invoice Item Updated [ID: 6, Maruti WagonR Tyer]', '2021-09-07 11:33:15', 'Admin Admin'),
	(136, 'New Order Status Added [18]', '2021-09-10 10:44:05', 'Admin Admin'),
	(137, 'Staff Member Updated [ID: 5, User User]', '2021-09-10 10:55:10', 'Admin Admin'),
	(138, 'Staff Member Updated [ID: 5, User User]', '2021-09-10 10:55:37', 'Admin Admin'),
	(139, 'New Order Status Added [19]', '2021-09-10 10:56:34', 'User User'),
	(140, 'New Order Status Added [20]', '2021-09-10 13:10:26', 'User User'),
	(141, 'New Order Status Added [21]', '2021-09-10 16:01:10', 'User User'),
	(142, 'Email Sent To [Email: aarti@gmail.com, Template: Send Order Note to Client]', '2021-09-14 16:23:18', 'Admin Admin'),
	(143, 'Email Sent To [Email: akshayd@gmail.com, Template: Send Order Note to Client]', '2021-09-14 16:23:21', 'Admin Admin'),
	(144, 'New Order Note Added [33]', '2021-09-14 16:23:21', 'Admin Admin'),
	(145, 'Email Template Updated [Send Order Note to Client]', '2021-09-14 16:40:56', 'Admin Admin'),
	(146, 'New Order Status Added [22]', '2021-09-14 16:41:26', 'Admin Admin'),
	(147, 'Email Sent To [Email: akshaydange@leometric.com, Template: Send Order Note to Client]', '2021-09-14 16:42:00', 'Admin Admin'),
	(148, 'New Order Note Added [36]', '2021-09-14 16:42:00', 'Admin Admin'),
	(149, 'New Order Added [7]', '2021-09-14 16:54:25', 'Admin Admin'),
	(150, 'New Order Status Added [23]', '2021-09-14 16:54:25', 'Admin Admin'),
	(151, 'New Order Status Added [24]', '2021-09-14 16:55:53', 'Admin Admin'),
	(152, 'New Order Status Added [25]', '2021-09-15 10:23:58', 'Admin Admin'),
	(153, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Send Order Note to Client]', '2021-09-15 10:24:56', 'Admin Admin'),
	(154, 'New Order Note Added [37]', '2021-09-15 10:24:56', 'Admin Admin'),
	(155, 'Staff Member Updated [ID: 5, User User]', '2021-09-15 16:33:46', 'Admin Admin'),
	(156, 'Staff Member Updated [ID: 5, User User]', '2021-09-15 16:34:01', 'Admin Admin'),
	(157, 'Staff Member Updated [ID: 5, User User]', '2021-09-15 16:34:02', 'Admin Admin'),
	(158, 'New Health Checklist Added [3]', '2021-09-17 15:14:41', 'Admin Admin'),
	(159, 'Contact Updated [ID: 3]', '2021-09-21 13:05:56', 'Admin Admin'),
	(160, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: No, IP: ::1]', '2021-09-21 13:10:11', NULL),
	(161, 'Contact Updated [ID: 3]', '2021-09-21 13:11:51', 'Admin Admin'),
	(162, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: No, IP: ::1]', '2021-10-14 10:31:18', NULL),
	(163, 'New User Added [ID: 1, Name: Leometric | CRM ]', '2021-10-14 13:26:01', 'Admin Admin'),
	(164, 'Contact Updated [ID: 37]', '2021-10-19 16:02:47', 'Admin Admin'),
	(165, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: No, IP: ::1]', '2021-10-20 10:20:20', NULL),
	(166, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Ticket Opened - Autoresponse]', '2021-10-20 17:17:15', 'Akshay B Dange'),
	(167, 'New Ticket Created [ID: 3]', '2021-10-20 17:17:15', 'Akshay B Dange'),
	(168, 'Ticket Updated [ID: 3]', '2021-10-20 17:19:21', 'Admin Admin'),
	(169, 'New network / server added [4]', '2021-10-21 13:30:40', 'Admin Admin'),
	(170, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:05:14', NULL),
	(171, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:06:10', NULL),
	(172, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:06:23', NULL),
	(173, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:10:04', NULL),
	(174, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:17:05', NULL),
	(175, 'Contact Updated [ID: 37]', '2021-10-25 12:17:37', 'Admin Admin'),
	(176, 'Non Existing User Tried to Login [Email: akshay.dange74@gmail.co, Is Staff Member: No, IP: ::1]', '2021-10-25 12:18:02', 'Akshay B Dange'),
	(177, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2021-10-25 12:18:15', 'Akshay B Dange'),
	(178, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Ticket Opened - Autoresponse]', '2021-10-28 10:56:49', 'Akshay B Dange'),
	(179, 'New Ticket Created [ID: 1]', '2021-10-28 10:56:49', 'Akshay B Dange'),
	(180, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Ticket Opened - Autoresponse]', '2021-10-28 11:01:02', 'Akshay B Dange'),
	(181, 'New Ticket Created [ID: 2]', '2021-10-28 11:01:03', 'Akshay B Dange'),
	(182, 'Non Existing User Tried to Login [Email: akshay.dange74@gmail.com, Is Staff Member: Yes, IP: ::1]', '2021-10-28 12:15:28', 'Akshay B Dange'),
	(183, 'New Ticket Created [ID: 3]', '2021-10-28 15:48:31', NULL),
	(184, 'New Ticket Created [ID: 4]', '2021-10-28 15:49:47', NULL),
	(185, 'Failed to send email template - SMTP connect() failed. https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting<br /><pre>\n\n</pre>', '2021-10-28 15:53:39', NULL),
	(186, 'New Ticket Created [ID: 5]', '2021-10-28 15:53:40', NULL),
	(187, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New service_request Opened - Autoresponse]', '2021-10-28 16:00:30', NULL),
	(188, 'New service_request Created [ID: 3]', '2021-10-28 16:00:30', NULL),
	(189, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New service_request Opened - Autoresponse]', '2021-10-28 16:03:18', NULL),
	(190, 'New service_request Created [ID: 4]', '2021-10-28 16:03:18', NULL),
	(191, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New service_request Opened - Autoresponse]', '2021-10-28 16:03:50', NULL),
	(192, 'New service_request Created [ID: 5]', '2021-10-28 16:03:50', NULL),
	(193, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New service_request Opened - Autoresponse]', '2021-10-28 16:06:42', NULL),
	(194, 'New service_request Created [ID: 6]', '2021-10-28 16:06:42', NULL),
	(195, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New service_request Opened - Autoresponse]', '2021-10-28 16:10:31', 'Akshay B Dange'),
	(196, 'New service_request Created [ID: 7]', '2021-10-28 16:10:31', 'Akshay B Dange'),
	(197, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Ticket Opened - Autoresponse]', '2021-10-28 16:12:50', NULL),
	(198, 'New Ticket Created [ID: 6]', '2021-10-28 16:12:50', NULL),
	(199, 'Email Sent To [Email: kshaydange@leometric.com, Template: New service_request Opened (Opened by Staff, Sent to Approval authority)]', '2021-11-03 10:48:06', 'Admin Admin'),
	(200, 'service_request Updated [ID: 1]', '2021-11-03 10:48:06', 'Admin Admin'),
	(201, 'Email Sent To [Email: kshaydange@leometric.com, Template: New service_request Opened (Opened by Staff, Sent to Approval authority)]', '2021-11-03 10:48:10', 'Admin Admin'),
	(202, 'Email Sent To [Email: akshaydange@leometric.com, Template: New service_request Opened (Opened by Staff, Sent to Approval authority)]', '2021-11-03 15:50:04', 'Admin Admin'),
	(203, 'service_request Updated [ID: 2]', '2021-11-03 15:50:04', 'Admin Admin'),
	(204, 'New Diagram Added [id: 2]', '2021-11-08 11:45:49', 'Admin Admin'),
	(205, 'Contract Updated [Test Contract]', '2021-11-08 19:25:16', 'Admin Admin'),
	(206, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Comment  (Sent to Customer Contacts)]', '2021-11-09 12:53:09', 'Akshay B Dange'),
	(207, 'Email Sent To [Email: akshaydange@leometric.com, Template: New Comment (Sent to Staff) ]', '2021-11-09 12:54:51', 'Akshay B Dange'),
	(208, 'Email Sent To [Email: akshaydange@leometric.com, Template: New Comment (Sent to Staff) ]', '2021-11-09 12:55:17', 'Akshay B Dange'),
	(209, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Comment  (Sent to Customer Contacts)]', '2021-11-09 16:59:58', NULL),
	(210, 'New Ticket Reply [ReplyID: 11]', '2021-11-09 19:28:54', 'Akshay B Dange'),
	(211, 'New Ticket Reply [ReplyID: 12]', '2021-11-09 19:36:51', NULL),
	(212, 'New Ticket Reply [ReplyID: 13]', '2021-11-09 19:40:42', NULL),
	(213, 'New service_request Reply [ReplyID: 2]', '2021-11-09 19:46:04', NULL),
	(214, 'New service_request Reply [ReplyID: 3]', '2021-11-11 12:53:55', 'Akshay B Dange'),
	(215, 'New Ticket Reply [ReplyID: 14]', '2021-11-11 19:25:21', NULL),
	(216, 'Non Existing User Tried to Login [Email: akshay.dange74@leometric.com, Is Staff Member: No, IP: ::1]', '2021-11-12 14:59:24', NULL),
	(217, 'New Task Added [ID:16, Name: API Testing Task]', '2021-11-13 13:06:52', NULL),
	(218, 'Email Sent To [Email: user@user.com, Template: New Task Assigned (Sent to Staff)]', '2021-11-13 13:27:17', 'Admin Admin'),
	(219, 'Email Sent To [Email: deepak@gmail.com, Template: New Task Assigned (Sent to Staff)]', '2021-11-13 13:27:22', 'Admin Admin'),
	(220, 'Contact Updated [ID: 37]', '2021-11-16 17:55:48', NULL),
	(221, 'Contact Updated [ID: 37]', '2021-11-16 17:58:30', NULL),
	(222, 'Contact Updated [ID: 37]', '2021-11-16 18:01:06', NULL),
	(223, 'Contact Updated [ID: 37]', '2021-11-16 18:01:21', NULL),
	(224, 'Contact Updated [ID: 37]', '2021-11-16 18:54:00', NULL),
	(225, 'Email Sent To [Email: akshaydange@leometric.com, Template: New Customer Profile File(s) Uploaded (Sent to Staff)]', '2021-11-17 12:52:15', 'Akki Dange'),
	(226, 'Email Sent To [Email: akshaydange@leometric.com, Template: New Customer Profile File(s) Uploaded (Sent to Staff)]', '2021-11-17 17:08:56', 'Akki Dange'),
	(227, 'New Ticket Reply [ReplyID: 15]', '2021-11-25 14:51:30', 'Admin Admin'),
	(228, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: Ticket Reply (Sent to Customer)]', '2021-11-25 14:51:50', 'Admin Admin'),
	(229, 'Email Sent To [Email: akshaydange@leometric.com, Template: New Customer Profile File(s) Uploaded (Sent to Staff)]', '2021-12-06 14:31:31', 'Akki Dange'),
	(230, 'New Task Added [ID:17, Name: Test timesheet]', '2021-12-06 15:01:50', 'Admin Admin'),
	(231, 'New Newsletter Added [id: 2]', '2021-12-08 12:06:24', 'Admin Admin'),
	(232, 'New Newsletter Added [id: 3]', '2021-12-08 12:11:04', 'Admin Admin'),
	(233, 'New Newsletter Added [id: 4]', '2021-12-08 12:17:05', 'Admin Admin'),
	(234, 'New Newsletter Added [id: 1]', '2021-12-08 12:30:11', 'Admin Admin'),
	(235, 'New Newsletter Added [id: 2]', '2021-12-08 12:31:09', 'Admin Admin'),
	(236, 'New Newsletter Added [id: 1]', '2021-12-08 12:37:30', 'Admin Admin'),
	(237, 'New Newsletter Added [id: 2]', '2021-12-08 12:38:45', 'Admin Admin'),
	(238, 'Newsletter Updated [id: 2]', '2021-12-08 12:42:33', 'Admin Admin'),
	(239, 'New Ticket Reply [ReplyID: 16]', '2021-12-09 12:35:03', NULL),
	(240, 'New Ticket Reply [ReplyID: 17]', '2021-12-09 12:39:52', NULL),
	(241, 'New Ticket Reply [ReplyID: 18]', '2021-12-09 12:41:20', NULL),
	(242, 'Contact Updated [ID: 37]', '2021-12-09 15:36:11', NULL),
	(243, 'Contact Updated [ID: 37]', '2021-12-09 15:39:21', NULL),
	(244, 'Email sent to: akshayd@leometric.com Subject: Guest Contact - Need Urgent Support', '2021-12-09 16:48:25', 'Akki Dange'),
	(245, 'Email sent to: akshayd@leometric.com Subject: Guest Contact - Need Urgent Support', '2021-12-09 17:04:21', 'Akki Dange'),
	(246, 'Email sent to: akshayd@leometric.com Subject: Guest Contact - Ask For a Quote', '2021-12-10 12:45:44', NULL),
	(247, 'Email sent to: sudarshang@leometric.com Subject: Guest Contact - Create My Account', '2021-12-10 12:50:14', NULL),
	(248, 'Contact Updated [ID: 37]', '2021-12-11 12:40:39', 'Admin Admin'),
	(249, 'New Ticket Reply [ReplyID: 19]', '2021-12-11 19:05:42', NULL),
	(250, 'New Ticket Reply [ReplyID: 20]', '2021-12-13 11:31:43', NULL),
	(251, 'New Ticket Reply [ReplyID: 21]', '2021-12-13 11:32:08', NULL),
	(252, 'New Ticket Reply [ReplyID: 22]', '2021-12-13 11:35:53', NULL),
	(253, 'New Ticket Reply [ReplyID: 23]', '2021-12-13 11:36:36', NULL),
	(254, 'New Ticket Reply [ReplyID: 25]', '2021-12-13 12:08:01', 'Akki Dange'),
	(255, 'New Ticket Reply [ReplyID: 28]', '2021-12-13 12:15:53', NULL),
	(256, 'New Service Request Reply [ReplyID: 4]', '2021-12-13 14:50:17', NULL),
	(257, 'New Service Request Reply [ReplyID: 5]', '2021-12-13 14:51:10', NULL),
	(258, 'New Service Request Reply [ReplyID: 6]', '2021-12-13 15:06:05', NULL),
	(259, 'New Service Request Reply [ReplyID: 7]', '2021-12-13 15:06:54', NULL),
	(260, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: No, IP: ::1]', '2022-01-06 17:04:50', NULL),
	(261, 'Non Existing User Tried to Login [Email: ithapemonika555@gmail.com, Is Staff Member: No, IP: ::1]', '2022-01-08 10:53:52', NULL),
	(262, 'Email Sent To [Email: akshay.dange74@gmail.com, Template: New Ticket Opened - Autoresponse]', '2022-01-08 11:14:22', 'Akki Dange'),
	(263, 'New Ticket Created [ID: 7]', '2022-01-08 11:14:22', 'Akki Dange'),
	(264, 'Non Existing User Tried to Login [Email: akshay.dange74@gmail.com, Is Staff Member: Yes, IP: ::1]', '2022-01-08 14:14:04', 'Akki Dange'),
	(265, 'Staff Member Updated [ID: 5, User User]', '2022-01-08 18:08:17', 'Admin Admin'),
	(266, 'Staff Member Updated [ID: 5, User User]', '2022-01-08 18:08:29', 'Admin Admin'),
	(267, 'Staff Member Updated [ID: 5, User User]', '2022-01-25 16:28:15', 'Admin Admin'),
	(268, 'Staff Member Updated [ID: 5, User User]', '2022-01-25 16:28:39', 'Admin Admin'),
	(269, 'Tried to access page where don\'t have permission [incidents]', '2022-01-25 16:29:15', 'User User'),
	(270, 'Failed Login Attempt [Email: akshaydange@leometric.com, Is Staff Member: No, IP: ::1]', '2022-01-25 17:37:22', 'Admin Admin'),
	(271, 'Non Existing User Tried to Login [Email: shriram@mailinator.com, Is Staff Member: Yes, IP: ::1]', '2022-01-27 12:30:23', NULL),
	(272, 'Non Existing User Tried to Login [Email: shriram@mailinator.com, Is Staff Member: Yes, IP: ::1]', '2022-01-27 12:33:53', NULL),
	(273, 'Failed Login Attempt [Email: akshay.dange74@gmail.com, Is Staff Member: No, IP: ::1]', '2022-02-16 13:12:54', NULL),
	(274, 'Email Sent To [Email: vidhi123@gmail.com, Template: New Contact Added/Registered (Welcome Email)]', '2022-02-26 17:11:26', 'Admin Admin'),
	(275, 'Contact Created [ID: 40]', '2022-02-26 17:11:26', 'Admin Admin'),
	(276, 'Contact Updated [ID: 40]', '2022-02-26 17:11:50', 'Admin Admin'),
	(277, 'Contact Updated [ID: 37]', '2022-02-26 17:17:48', 'Admin Admin'),
	(278, 'Failed to send email template - SMTP Error: The following recipients failed: vidhi123@gmail.com: Domain leometric.com has exceeded the max emails per hour (500/500 (100%))\r\nallowed.  Message discarded.\r\n<br /><pre>\n\n</pre>', '2022-02-26 17:19:59', 'Admin Admin'),
	(279, 'New Ticket Created [ID: 8]', '2022-02-26 17:19:59', 'Admin Admin'),
	(280, 'Contact Updated [ID: 2]', '2022-02-28 12:00:00', 'Admin Admin');
/*!40000 ALTER TABLE `tblactivity_log` ENABLE KEYS */;

-- Dumping structure for table crm.tbladditional_services
CREATE TABLE IF NOT EXISTS `tbladditional_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `chargestype` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tbladditional_services: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbladditional_services` DISABLE KEYS */;
INSERT INTO `tbladditional_services` (`id`, `customer_id`, `chargestype`, `rate`) VALUES
	(1, 2, 1, 0),
	(2, 1, 2, 0),
	(3, 2, 2, 0),
	(4, 2, 1, 0),
	(5, 1, 1, 0);
/*!40000 ALTER TABLE `tbladditional_services` ENABLE KEYS */;

-- Dumping structure for table crm.tbladditional_service_charges
CREATE TABLE IF NOT EXISTS `tbladditional_service_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `chargetype` varchar(50) NOT NULL,
  `rate` int(11) NOT NULL,
  `currency` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tbladditional_service_charges: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbladditional_service_charges` DISABLE KEYS */;
INSERT INTO `tbladditional_service_charges` (`id`, `customer_id`, `chargetype`, `rate`, `currency`) VALUES
	(1, 2, 'Per Hour', 500, 'Rs'),
	(2, 1, 'Per Ticket', 4500, 'Rs');
/*!40000 ALTER TABLE `tbladditional_service_charges` ENABLE KEYS */;

-- Dumping structure for table crm.tblannouncements
CREATE TABLE IF NOT EXISTS `tblannouncements` (
  `announcementid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `message` text,
  `showtousers` int(11) NOT NULL,
  `showtostaff` int(11) NOT NULL,
  `showname` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  `userid` varchar(100) NOT NULL,
  PRIMARY KEY (`announcementid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblannouncements: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblannouncements` DISABLE KEYS */;
INSERT INTO `tblannouncements` (`announcementid`, `name`, `message`, `showtousers`, `showtostaff`, `showname`, `dateadded`, `userid`) VALUES
	(1, 'Test Announcements', 'Test Announcements message', 1, 0, 4, '2021-11-16 11:47:59', '1');
/*!40000 ALTER TABLE `tblannouncements` ENABLE KEYS */;

-- Dumping structure for table crm.tblbackups
CREATE TABLE IF NOT EXISTS `tblbackups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `policy_title` varchar(100) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `restore_point` int(11) DEFAULT NULL,
  `archive` enum('Yes','No') DEFAULT NULL,
  `archive_retention_period` int(11) DEFAULT NULL,
  `full_backup` enum('Weekly','Daily') DEFAULT NULL,
  `incremental_backup` enum('Weekly','Daily') DEFAULT NULL,
  `first_backup_destination` int(11) DEFAULT NULL,
  `second_backup_destination` int(11) DEFAULT NULL,
  `third_backup_destination` int(11) DEFAULT NULL,
  `recovery_time_obj` int(11) DEFAULT NULL,
  `recovery_point_obj` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblbackups: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblbackups` DISABLE KEYS */;
INSERT INTO `tblbackups` (`id`, `client_id`, `policy_title`, `server`, `restore_point`, `archive`, `archive_retention_period`, `full_backup`, `incremental_backup`, `first_backup_destination`, `second_backup_destination`, `third_backup_destination`, `recovery_time_obj`, `recovery_point_obj`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 5, 'LEOPOLICY001', '3,1', 2, 'Yes', 2, 'Weekly', 'Daily', 3, 0, 0, 3, 7, '2021-04-26 16:22:22', NULL, '2021-11-24 12:00:01', NULL),
	(2, 6, 'LEOPOLICY007', '1', 2, 'Yes', 2, 'Weekly', 'Daily', 3, 0, 0, 3, 1, '2021-04-28 12:37:06', NULL, '2021-11-24 12:00:05', NULL);
/*!40000 ALTER TABLE `tblbackups` ENABLE KEYS */;

-- Dumping structure for table crm.tblbackup_logs
CREATE TABLE IF NOT EXISTS `tblbackup_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backup_id` int(11) DEFAULT NULL,
  `server` int(11) DEFAULT NULL,
  `backup_date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblbackup_logs: ~6 rows (approximately)
/*!40000 ALTER TABLE `tblbackup_logs` DISABLE KEYS */;
INSERT INTO `tblbackup_logs` (`id`, `backup_id`, `server`, `backup_date`, `status`, `remark`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 1, 3, '2021-04-28', 'Success', NULL, '2021-04-28 12:20:11', NULL, '2021-04-28 12:20:11', NULL),
	(2, 1, 1, '2021-04-28', 'Warning', NULL, '2021-04-28 12:20:11', NULL, '2021-04-28 12:20:11', NULL),
	(3, 1, 3, '2021-04-29', 'Success', 'NA', '2021-04-29 14:59:23', NULL, '2021-04-29 14:59:23', NULL),
	(4, 1, 1, '2021-04-29', 'Failure', 'Failed due to net connection', '2021-04-29 14:59:24', NULL, '2021-04-29 14:59:24', NULL),
	(5, 1, 3, '2022-02-16', 'Success', '', '2022-02-16 15:34:55', NULL, '2022-02-16 15:34:55', NULL),
	(6, 1, 1, '2022-02-16', 'Success', '', '2022-02-16 15:34:55', NULL, '2022-02-16 15:34:55', NULL);
/*!40000 ALTER TABLE `tblbackup_logs` ENABLE KEYS */;

-- Dumping structure for table crm.tblboqs
CREATE TABLE IF NOT EXISTS `tblboqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) DEFAULT NULL,
  `content` longtext,
  `addedfrom` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `total` decimal(15,2) DEFAULT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `total_tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `adjustment` decimal(15,2) DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL,
  `discount_total` decimal(15,2) NOT NULL,
  `discount_type` varchar(30) DEFAULT NULL,
  `tags` text,
  `show_quantity_as` int(11) NOT NULL DEFAULT '1',
  `currency` int(11) NOT NULL,
  `open_till` date DEFAULT NULL,
  `date` date NOT NULL,
  `rel_id` int(11) DEFAULT NULL,
  `rel_type` varchar(40) DEFAULT NULL,
  `assigned` int(11) DEFAULT NULL,
  `hash` varchar(32) NOT NULL,
  `boq_to` varchar(191) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '0',
  `zip` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `allow_comments` tinyint(1) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  `estimate_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `date_converted` datetime DEFAULT NULL,
  `pipeline_order` int(11) NOT NULL DEFAULT '0',
  `is_expiry_notified` int(11) NOT NULL DEFAULT '0',
  `acceptance_firstname` varchar(50) DEFAULT NULL,
  `acceptance_lastname` varchar(50) DEFAULT NULL,
  `acceptance_email` varchar(100) DEFAULT NULL,
  `acceptance_date` datetime DEFAULT NULL,
  `acceptance_ip` varchar(40) DEFAULT NULL,
  `signature` varchar(40) DEFAULT NULL,
  `short_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblboqs: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblboqs` DISABLE KEYS */;
INSERT INTO `tblboqs` (`id`, `subject`, `content`, `addedfrom`, `datecreated`, `total`, `subtotal`, `total_tax`, `adjustment`, `discount_percent`, `discount_total`, `discount_type`, `tags`, `show_quantity_as`, `currency`, `open_till`, `date`, `rel_id`, `rel_type`, `assigned`, `hash`, `boq_to`, `country`, `zip`, `state`, `city`, `address`, `email`, `phone`, `allow_comments`, `status`, `estimate_id`, `invoice_id`, `date_converted`, `pipeline_order`, `is_expiry_notified`, `acceptance_firstname`, `acceptance_lastname`, `acceptance_email`, `acceptance_date`, `acceptance_ip`, `signature`, `short_link`) VALUES
	(1, 'Work Report : 05/06/2020', '{proposal_items}', 1, '2021-08-02 11:33:03', 300.00, 300.00, 0.00, 0.00, 0.00, 0.00, '', NULL, 1, 3, '2021-08-09', '2021-08-02', 5, 'customer', 1, '3fa88037bd59fe9eab892ef959e8eb3e', 'Akshay B Dange', 102, '422102', 'Maharashtra', 'Nashik', 'Nashik', 'akshay.dange74@gmail.com', '09405310077', 1, 6, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Test Article', '{boq_items}', 1, '2021-08-04 11:44:12', NULL, 0.00, 0.00, NULL, 0.00, 0.00, '', '', 1, 3, '2021-08-11', '2021-08-04', 1, 'customer', 1, '524f0349b70b798925dfdc1f278383c4', 'Akshay Dange', 102, '422214', 'Maharashtra', 'Pune', 'Pune', 'akshay.dange74@gmail.com', '9405310077', 1, 3, 1, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Testing', '{boq_items}', 1, '2021-08-26 10:13:25', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, '', NULL, 1, 1, NULL, '2021-08-26', 3, 'customer', 0, '50cb9f3b153059acce4c3293555892bf', 'Aarti Dange', 102, '422102', 'Maharashtra', 'Nashik', 'Nashik', 'aarti@gmail.com', '9405310077', 1, 6, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblboqs` ENABLE KEYS */;

-- Dumping structure for table crm.tblboq_comments
CREATE TABLE IF NOT EXISTS `tblboq_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` mediumtext,
  `boqid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblboq_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblboq_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblboq_comments` ENABLE KEYS */;

-- Dumping structure for table crm.tblchecklist
CREATE TABLE IF NOT EXISTS `tblchecklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `checklist_title` varchar(100) DEFAULT NULL,
  `inventory` varchar(255) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblchecklist: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblchecklist` DISABLE KEYS */;
INSERT INTO `tblchecklist` (`id`, `client_id`, `checklist_title`, `inventory`, `frequency`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 1, 'Test Checklist', '3,1', 1, '2021-07-15 15:04:10', NULL, '2021-07-15 15:04:10', NULL),
	(2, 5, 'Demo Edit', '3,1', 2, '2021-07-15 19:39:20', NULL, '2021-10-28 13:50:23', NULL),
	(3, 1, 'Test Checklist', '1', 2, '2021-09-17 15:14:41', NULL, '2021-09-17 15:15:04', NULL);
/*!40000 ALTER TABLE `tblchecklist` ENABLE KEYS */;

-- Dumping structure for table crm.tblchecklist_logs
CREATE TABLE IF NOT EXISTS `tblchecklist_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checklist_id` int(11) DEFAULT NULL,
  `inventory` int(11) DEFAULT NULL,
  `log_date` date DEFAULT NULL,
  `checklist_status` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblchecklist_logs: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblchecklist_logs` DISABLE KEYS */;
INSERT INTO `tblchecklist_logs` (`id`, `checklist_id`, `inventory`, `log_date`, `checklist_status`, `remark`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 1, 1, '2021-07-15', 1, 'SUCCESS', '2021-07-15 15:05:09', NULL, '2021-10-22 18:13:52', NULL),
	(2, 1, 3, '2021-07-15', 2, 'NA', '2021-07-15 15:05:09', NULL, '2021-10-22 18:14:58', NULL),
	(3, 2, 3, '2021-07-16', 2, 'NA', '2021-07-15 18:47:48', NULL, '2021-07-15 19:41:20', NULL),
	(4, 2, 1, '2021-07-15', 1, 'SUCCESS', '2021-07-15 18:47:48', NULL, '2021-07-15 19:39:57', NULL),
	(5, 3, 1, '2021-09-17', 1, 'SUCCESS', '2021-09-17 15:15:51', NULL, '2021-09-17 15:15:51', NULL);
/*!40000 ALTER TABLE `tblchecklist_logs` ENABLE KEYS */;

-- Dumping structure for table crm.tblchecklist_status
CREATE TABLE IF NOT EXISTS `tblchecklist_status` (
  `statusid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `statuscolor` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`statusid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblchecklist_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblchecklist_status` DISABLE KEYS */;
INSERT INTO `tblchecklist_status` (`statusid`, `name`, `statuscolor`) VALUES
	(1, 'Good', '#278d10'),
	(2, 'Critical', '#ff002a'),
	(3, 'Warning', '#ff9d00');
/*!40000 ALTER TABLE `tblchecklist_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblclients
CREATE TABLE IF NOT EXISTS `tblclients` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(191) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(30) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '0',
  `city` varchar(100) DEFAULT NULL,
  `zip` varchar(15) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `company_prefix` varchar(100) NOT NULL,
  `network_map_url` text,
  `datecreated` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `leadid` int(11) DEFAULT NULL,
  `billing_street` varchar(200) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_state` varchar(100) DEFAULT NULL,
  `billing_zip` varchar(100) DEFAULT NULL,
  `billing_country` int(11) DEFAULT '0',
  `shipping_street` varchar(200) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_zip` varchar(100) DEFAULT NULL,
  `shipping_country` int(11) DEFAULT '0',
  `longitude` varchar(191) DEFAULT NULL,
  `latitude` varchar(191) DEFAULT NULL,
  `default_language` varchar(40) DEFAULT NULL,
  `default_currency` int(11) NOT NULL DEFAULT '0',
  `show_primary_contact` int(11) NOT NULL DEFAULT '0',
  `stripe_id` varchar(40) DEFAULT NULL,
  `registration_confirmed` int(11) NOT NULL DEFAULT '1',
  `addedfrom` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  KEY `country` (`country`),
  KEY `leadid` (`leadid`),
  KEY `company` (`company`),
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblclients: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblclients` DISABLE KEYS */;
INSERT INTO `tblclients` (`userid`, `company`, `vat`, `phonenumber`, `country`, `city`, `zip`, `state`, `address`, `website`, `company_prefix`, `network_map_url`, `datecreated`, `active`, `leadid`, `billing_street`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `shipping_street`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `longitude`, `latitude`, `default_language`, `default_currency`, `show_primary_contact`, `stripe_id`, `registration_confirmed`, `addedfrom`) VALUES
	(1, 'Leometric', '', '', 0, '', '', '', '', 'leometric.com', 'LEO', 'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg', '2021-03-15 10:27:48', 1, NULL, '', '', '', '', 0, '', '', '', '', 0, NULL, NULL, '', 3, 0, NULL, 1, 1),
	(2, 'Decoding IT', '', '', 0, '', '', '', '', '', '', 'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg', '2021-03-15 12:28:35', 1, NULL, '', '', '', '', 0, '', '', '', '', 0, NULL, NULL, '', 0, 0, NULL, 1, 1),
	(3, 'aDcoders Technologies', NULL, '9405310077', 102, 'Nashik', '422102', 'Maharashtra', 'Nashik', 'www.aartidange.com', '', 'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg', '2021-05-06 15:27:34', 1, NULL, 'Nashik', 'Nashik', 'Maharashtra', '422102', 102, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'english', 0, 0, NULL, 1, 1),
	(5, 'Akshay Technologies', NULL, '09405310077', 102, 'Nashik', '422102', 'Maharashtra', 'Nashik', 'www.akshay.com', '', 'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg', '2021-05-07 17:22:16', 1, 2, 'Nashik', 'Nashik', 'Maharashtra', '422102', 102, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'english', 3, 0, NULL, 1, 1),
	(6, 'Vidhi Group of Companies', NULL, '9405310077', 0, 'PALSE', '422102', 'Maharashtra', 'B-10, DEVAMANI SOCIETY, SHIVAJI UDYAN', 'www.cutiepievidhi.com', '', 'https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/86/71/867186_v2.jpeg', '2021-05-11 19:04:54', 1, 3, 'B-10, DEVAMANI SOCIETY, SHIVAJI UDYAN', 'PALSE', 'Maharashtra', '422102', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', 0, 0, NULL, 1, 1);
/*!40000 ALTER TABLE `tblclients` ENABLE KEYS */;

-- Dumping structure for table crm.tblclient_locations
CREATE TABLE IF NOT EXISTS `tblclient_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblclient_locations: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblclient_locations` DISABLE KEYS */;
INSERT INTO `tblclient_locations` (`id`, `customer_id`, `location`) VALUES
	(2, 2, 'SangamnerTaluka'),
	(4, 1, 'OmanTime'),
	(5, 1, 'Nashik');
/*!40000 ALTER TABLE `tblclient_locations` ENABLE KEYS */;

-- Dumping structure for table crm.tblconsents
CREATE TABLE IF NOT EXISTS `tblconsents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(10) NOT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(40) NOT NULL,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `lead_id` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `opt_in_purpose_description` text,
  `purpose_id` int(11) NOT NULL,
  `staff_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purpose_id` (`purpose_id`),
  KEY `contact_id` (`contact_id`),
  KEY `lead_id` (`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblconsents: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblconsents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblconsents` ENABLE KEYS */;

-- Dumping structure for table crm.tblconsent_purposes
CREATE TABLE IF NOT EXISTS `tblconsent_purposes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `date_created` datetime NOT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblconsent_purposes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblconsent_purposes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblconsent_purposes` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontacts
CREATE TABLE IF NOT EXISTS `tblcontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `is_primary` int(11) NOT NULL DEFAULT '1',
  `firstname` varchar(191) NOT NULL,
  `lastname` varchar(191) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phonenumber` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `new_pass_key` varchar(32) DEFAULT NULL,
  `new_pass_key_requested` datetime DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `email_verification_key` varchar(32) DEFAULT NULL,
  `email_verification_sent_at` datetime DEFAULT NULL,
  `last_ip` varchar(40) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_password_change` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `profile_image` varchar(191) DEFAULT NULL,
  `direction` varchar(3) DEFAULT NULL,
  `invoice_emails` tinyint(1) NOT NULL DEFAULT '1',
  `estimate_emails` tinyint(1) NOT NULL DEFAULT '1',
  `credit_note_emails` tinyint(1) NOT NULL DEFAULT '1',
  `contract_emails` tinyint(1) NOT NULL DEFAULT '1',
  `task_emails` tinyint(1) NOT NULL DEFAULT '1',
  `project_emails` tinyint(1) NOT NULL DEFAULT '1',
  `ticket_emails` tinyint(1) NOT NULL DEFAULT '1',
  `service_request_emails` tinyint(4) NOT NULL DEFAULT '1',
  `token` text NOT NULL,
  `fcm_token` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `firstname` (`firstname`),
  KEY `lastname` (`lastname`),
  KEY `email` (`email`),
  KEY `is_primary` (`is_primary`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontacts: ~24 rows (approximately)
/*!40000 ALTER TABLE `tblcontacts` DISABLE KEYS */;
INSERT INTO `tblcontacts` (`id`, `userid`, `is_primary`, `firstname`, `lastname`, `email`, `phonenumber`, `title`, `datecreated`, `password`, `new_pass_key`, `new_pass_key_requested`, `email_verified_at`, `email_verification_key`, `email_verification_sent_at`, `last_ip`, `last_login`, `last_password_change`, `active`, `profile_image`, `direction`, `invoice_emails`, `estimate_emails`, `credit_note_emails`, `contract_emails`, `task_emails`, `project_emails`, `ticket_emails`, `service_request_emails`, `token`, `fcm_token`) VALUES
	(3, 1, 1, 'Akshay', 'Dange', 'akshaydange@leometric.com', '9405310077', 'Lead', '2021-03-15 10:30:13', '$2a$08$/e.Mkhdbl0OHtAjAbKsHLO0dbSxp53qh0pNzdc.LOWlE/iabA.X0a', NULL, NULL, '2021-03-15 10:30:12', NULL, NULL, '::1', '2021-09-21 13:12:08', '2021-09-21 13:11:50', 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9', ''),
	(4, 1, 1, 'Dhanesh', 'Gadekar', 'customer@gmail.com', '', 'CEO', '2021-03-15 10:28:54', '$2a$08$lJBk/ggiCCBtjbh7DNbKieq6bFjXfJh85P25x2BrjNtydNh4OfQ5y', NULL, NULL, '2021-03-15 10:28:54', NULL, NULL, '::1', '2021-03-17 23:01:29', NULL, 1, NULL, '', 0, 0, 0, 1, 1, 1, 1, 1, '', ''),
	(7, 2, 0, 'Rohit', 'Saluja', 'rohit@gmail.com', '', '', '2021-03-15 12:29:03', '$2a$08$tlkEm7fCLWRswu544Yea4eIiizp4Zj71Vx0HVXLS5oNL9OLXH3b1S', NULL, NULL, '2021-03-15 12:29:03', NULL, NULL, '::1', '2021-03-16 17:20:21', NULL, 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, '', ''),
	(12, 1, 0, 'Kiran', 'Gadekar', 'kiran@leometric.com', '', '', '2021-03-15 02:18:04', '$2a$08$0rwLPjvLJ.pPv2qvqcQzS.bL2.hZ/V0T3MtZZ/uuNEY7KlpSbVOdW', NULL, NULL, '2021-03-15 15:22:19', NULL, NULL, '::1', '2021-03-16 17:19:49', '2021-03-15 15:22:05', 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, '', ''),
	(20, 1, 0, 'akshaydange@leometric.com', 'dange', 'kshaydange@leometric.com', '', '', '2021-03-15 10:18:10', '$2a$08$HoJsQEZDIPRUCNb0LHYZ4eTsTwDeMHiBA/qZhjYhyDInqCMzC3mOG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, '', ''),
	(21, 1, 0, 'Leometric Technology | CRM', '', 'admin@leometric.com', '', NULL, '2021-03-16 05:37:06', '$2a$08$7SqPjwIVkz4haSJfsDrYOOGIVlm1.DPiFR1kDCdz14rA.6FLXLxce', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(22, 1, 0, 'Leometric | CRM', '', 'sudarshan@leometric.com', '', NULL, '2021-03-16 11:16:35', '$2a$08$Kxegwm4OSVl3zFj3qpQ9zOzux1/IS6hAwGLuUCG38nDl8/2MPOc9K', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(23, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy9.mail.unifiedlayer.com', '', NULL, '2021-03-16 11:16:35', '$2a$08$n0kBH93MiwHTfHhhex7aWu/EdyTxFtby4XuQN9YdOLQujr2L8j5v.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(24, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy13.mail.unifiedlayer.com', '', NULL, '2021-03-16 11:38:10', '$2a$08$kM7w/uCNq64lt5QXYOdER.OUxR7UtZJ/Md37I9Fj3.79uL0aTQP4S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(25, 0, 0, 'techlead@gmail.com', '', 'techlead@gmail.com', '', NULL, '2021-03-17 01:08:33', '$2a$08$WN1jmrztF7kBfrTpnsXjT.bKXsaWKfEWH05i6h/FumQc1EclIj23W', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(26, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy7.mail.unifiedlayer.com', '', NULL, '2021-03-17 04:36:02', '$2a$08$2GzQXbAfXNGZuNXgCg8nA.adTsH0BZcYSBEb/8P0QTcgDDqq/r6gu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(27, 0, 0, 'Rohit Saluja', '', 'rohit@decodingit.com', '', NULL, '2021-03-17 05:14:32', '$2a$08$P2yWf1ctZ/CgSnNQaUDSxOabSCYYSxhYmoNEUCA8.Vb/HSQqJ1q1y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(28, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy6.mail.unifiedlayer.com', '', NULL, '2021-03-18 11:53:01', '$2a$08$guW87VQHFi.pE5Zb8FviZOPnOStcEHfWnaOn11OpI4KL1WfXqbX/y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(29, 1, 0, 'sudarshan@leometric.com', '', 'udarshan@leometric.com', '', NULL, '2021-03-18 11:58:32', '$2a$08$pP0gULeUO5g..u5.5XPNiu533V1sQmK/NWp.N3p6G2zByx./7Qgay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(30, 0, 0, 'Sudarshan Gadekar', '', 'sudarshangadekar35@gmail.com', '', NULL, '2021-03-18 02:23:46', '$2a$08$aq0KmWyyCgk/u/C7eeTK1O13IbGErSZEjJUIQxTBM3VWg2C/tjsL6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(31, 1, 0, 'leocrmtest@gmail.com', 'Test', 'leocrmtest@gmail.com', '', '', '2021-03-18 05:03:32', '$2a$08$EidS0g.AuOv2qmjXT2xnr.NqiC6StHd2tAv8pCGsP8lgWjsiHCuX2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-01 10:46:57', 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, '', ''),
	(32, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy4.mail.unifiedlayer.com', '', NULL, '2021-03-18 05:03:32', '$2a$08$qj.Wk2VPcGwVBwuvuI/oM.pGo5oJ4FDg.Rm9xmWDqCSXdoLlSETe2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(33, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy14.mail.unifiedlayer.com', '', NULL, '2021-03-18 05:09:02', '$2a$08$5twmNdfNhFuvHrwjvxAqAOHJj7IASoPNqgvlp6iehmzffBIxTuPfu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(34, 0, 0, 'Mail Delivery System', '', 'MAILER-DAEMON@soproxy3.mail.unifiedlayer.com', '', NULL, '2021-03-18 06:14:32', '$2a$08$PyV.yAc1QVIyIli4GPDcxuqQ8uYIYQ4QFjgH01GmbCLiBmBFKJKaK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, '', ''),
	(35, 3, 1, 'Aarti', 'Dange', 'aarti@gmail.com', '9405310077', 'HR Manager', '2021-05-06 15:27:34', '$2a$08$AbayjUQ7/HCoyGDKh3jzHO2KTucTLoCTEorJlJsSvot6ghxbg1SqC', NULL, NULL, '2021-05-06 15:27:34', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, 0, 1, 1, 1, 1, 0, '', ''),
	(37, 5, 1, 'Akki', 'Dange', 'akshay.dange74@gmail.com', '8208884255', 'Senior Developer', '2021-05-07 17:22:16', '$2a$08$tkakdXkHkkpAhzSZnkMAFeylgt9EPtxlPgtwBMyAkJNiszSQRZm/W', NULL, NULL, '2021-05-07 17:22:16', NULL, NULL, '::1', '2022-03-02 14:33:42', '2021-10-25 12:17:37', 1, 'CTA-01 (1).png', 'ltr', 0, 0, 0, 0, 0, 0, 1, 0, '2b947967655967ed15fb1e32b36ee3ec', ''),
	(38, 6, 1, 'Vidhi', 'Landge', 'vidhi@gmail.com', '9405310077', 'Cutie Pie', '2021-05-11 19:04:55', '$2a$08$ACgeRYF5zECUHulzFBbziuj.V2iAizFhA4J6YRNxEdLor0n.vJb8e', NULL, NULL, '2021-05-11 19:04:54', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, 0, 1, 1, 1, 1, 0, '', ''),
	(39, 3, 0, 'Akshay', 'Dange', 'akshayd@gmail.com', '', '', '2021-07-06 12:04:09', '$2a$08$TnTf04FCBmz23W5y5jc.QugADqWedVxtzgKG2HQ/3j7AK5abc08uW', NULL, NULL, '2021-07-06 12:04:09', NULL, NULL, '::1', '2021-07-09 15:53:29', '2021-07-09 15:53:12', 1, NULL, '', 0, 0, 0, 1, 1, 1, 1, 1, '', ''),
	(40, 5, 0, 'Vidhi', 'Landge', 'vidhi123@gmail.com', '9405310077', '', '2022-02-26 17:11:15', '$2a$08$Mjwkw7FOhvJmupbIU.voX.4GW5PLcWE2.JUmcJvTpo0NGptd6RTTu', NULL, NULL, '2022-02-26 17:11:15', NULL, NULL, '::1', '2022-02-26 17:20:26', NULL, 1, NULL, '', 0, 0, 1, 1, 1, 1, 1, 1, '', '');
/*!40000 ALTER TABLE `tblcontacts` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontact_permissions
CREATE TABLE IF NOT EXISTS `tblcontact_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontact_permissions: ~61 rows (approximately)
/*!40000 ALTER TABLE `tblcontact_permissions` DISABLE KEYS */;
INSERT INTO `tblcontact_permissions` (`id`, `permission_id`, `userid`) VALUES
	(3, 3, 1),
	(5, 5, 1),
	(6, 6, 1),
	(21, 3, 4),
	(23, 5, 4),
	(24, 6, 4),
	(34, 3, 9),
	(35, 5, 9),
	(36, 6, 9),
	(37, 3, 2),
	(38, 5, 2),
	(39, 6, 2),
	(40, 3, 3),
	(41, 5, 3),
	(42, 6, 3),
	(43, 3, 7),
	(44, 5, 7),
	(45, 6, 7),
	(46, 3, 35),
	(47, 5, 35),
	(48, 6, 35),
	(49, 3, 36),
	(50, 5, 36),
	(51, 6, 36),
	(52, 3, 37),
	(53, 5, 37),
	(54, 6, 37),
	(55, 3, 38),
	(56, 5, 38),
	(57, 6, 38),
	(58, 3, 39),
	(59, 5, 39),
	(60, 6, 39),
	(61, 8, 37),
	(62, 13, 37),
	(63, 12, 3),
	(64, 4, 3),
	(65, 7, 3),
	(66, 8, 3),
	(67, 9, 3),
	(68, 10, 3),
	(69, 11, 3),
	(70, 13, 3),
	(71, 14, 3),
	(72, 15, 3),
	(73, 16, 3),
	(75, 7, 37),
	(76, 9, 37),
	(77, 10, 37),
	(78, 11, 37),
	(79, 12, 37),
	(80, 14, 37),
	(81, 15, 37),
	(82, 16, 37),
	(83, 3, 40),
	(84, 4, 40),
	(85, 5, 40),
	(86, 6, 40),
	(87, 7, 40),
	(88, 8, 40),
	(89, 9, 40),
	(90, 10, 40),
	(91, 11, 40),
	(92, 12, 40),
	(93, 13, 40),
	(94, 14, 40),
	(95, 15, 40),
	(96, 16, 40);
/*!40000 ALTER TABLE `tblcontact_permissions` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontracts
CREATE TABLE IF NOT EXISTS `tblcontracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext,
  `description` text,
  `subject` varchar(191) DEFAULT NULL,
  `client` int(11) NOT NULL,
  `datestart` date DEFAULT NULL,
  `dateend` date DEFAULT NULL,
  `contract_type` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  `isexpirynotified` int(11) NOT NULL DEFAULT '0',
  `contract_value` decimal(15,2) DEFAULT NULL,
  `trash` tinyint(1) DEFAULT '0',
  `not_visible_to_client` tinyint(1) NOT NULL DEFAULT '0',
  `hash` varchar(32) DEFAULT NULL,
  `signed` tinyint(1) NOT NULL DEFAULT '0',
  `signature` varchar(40) DEFAULT NULL,
  `marked_as_signed` tinyint(1) NOT NULL DEFAULT '0',
  `acceptance_firstname` varchar(50) DEFAULT NULL,
  `acceptance_lastname` varchar(50) DEFAULT NULL,
  `acceptance_email` varchar(100) DEFAULT NULL,
  `acceptance_date` datetime DEFAULT NULL,
  `acceptance_ip` varchar(40) DEFAULT NULL,
  `short_link` varchar(100) DEFAULT NULL,
  `incident` varchar(50) DEFAULT NULL,
  `service_request` varchar(50) DEFAULT NULL,
  `network_monitor` int(11) DEFAULT NULL,
  `security_monitor` int(11) DEFAULT NULL,
  `backup_monitor` int(11) DEFAULT NULL,
  `gap_analysis` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client` (`client`),
  KEY `contract_type` (`contract_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontracts: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblcontracts` DISABLE KEYS */;
INSERT INTO `tblcontracts` (`id`, `content`, `description`, `subject`, `client`, `datestart`, `dateend`, `contract_type`, `project_id`, `addedfrom`, `dateadded`, `isexpirynotified`, `contract_value`, `trash`, `not_visible_to_client`, `hash`, `signed`, `signature`, `marked_as_signed`, `acceptance_firstname`, `acceptance_lastname`, `acceptance_email`, `acceptance_date`, `acceptance_ip`, `short_link`, `incident`, `service_request`, `network_monitor`, `security_monitor`, `backup_monitor`, `gap_analysis`) VALUES
	(1, NULL, 'Test', 'Test Contract', 1, '2021-03-16', '2021-03-18', 1, 0, 1, '2021-03-16 23:11:57', 1, 25000.00, 0, 0, '8e1b841a8598fd9c6809f4fd42ae1b3e', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0),
	(2, NULL, 'Testing', 'Apprisal', 5, '2021-03-16', '2021-03-25', 2, 0, 1, '2021-03-16 23:12:48', 0, 45000.00, 0, 0, 'cba652e00357c813b3d34d7392ad28a7', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, NULL, 'Test', 'Test Contact', 5, '2021-03-17', '2021-03-19', 1, 0, 1, '2021-03-17 16:30:03', 1, 23456.00, 0, 0, '79442478146da930959e31cd9db3c5a4', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '23', '234', 1, 1, 1, 1);
/*!40000 ALTER TABLE `tblcontracts` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontracts_types
CREATE TABLE IF NOT EXISTS `tblcontracts_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontracts_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblcontracts_types` DISABLE KEYS */;
INSERT INTO `tblcontracts_types` (`id`, `name`) VALUES
	(1, 'Monthly'),
	(2, 'Quarterly'),
	(3, 'Advance');
/*!40000 ALTER TABLE `tblcontracts_types` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontract_comments
CREATE TABLE IF NOT EXISTS `tblcontract_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` mediumtext,
  `contract_id` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontract_comments: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblcontract_comments` DISABLE KEYS */;
INSERT INTO `tblcontract_comments` (`id`, `content`, `contract_id`, `staffid`, `dateadded`) VALUES
	(1, 'Testing Disc', 3, 1, '2021-11-09 12:52:59'),
	(2, 'Congratulations on yours appraisal ', 2, 0, '2021-11-09 12:54:43'),
	(3, 'Wish you best luck', 2, 0, '2021-11-09 12:55:09'),
	(4, 'sdkjjsdkjsk ihsidhy sdihsui', 2, 0, '2021-11-09 16:59:52'),
	(5, 'sdkjjsdkjsk ihsidhy sdihsui', 2, 0, '2021-11-09 17:04:24');
/*!40000 ALTER TABLE `tblcontract_comments` ENABLE KEYS */;

-- Dumping structure for table crm.tblcontract_renewals
CREATE TABLE IF NOT EXISTS `tblcontract_renewals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractid` int(11) NOT NULL,
  `old_start_date` date NOT NULL,
  `new_start_date` date NOT NULL,
  `old_end_date` date DEFAULT NULL,
  `new_end_date` date DEFAULT NULL,
  `old_value` decimal(15,2) DEFAULT NULL,
  `new_value` decimal(15,2) DEFAULT NULL,
  `date_renewed` datetime NOT NULL,
  `renewed_by` varchar(100) NOT NULL,
  `renewed_by_staff_id` int(11) NOT NULL DEFAULT '0',
  `is_on_old_expiry_notified` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcontract_renewals: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcontract_renewals` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcontract_renewals` ENABLE KEYS */;

-- Dumping structure for table crm.tblcountries
CREATE TABLE IF NOT EXISTS `tblcountries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `iso2` char(2) DEFAULT NULL,
  `short_name` varchar(80) NOT NULL DEFAULT '',
  `long_name` varchar(80) NOT NULL DEFAULT '',
  `iso3` char(3) DEFAULT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `un_member` varchar(12) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `cctld` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcountries: ~250 rows (approximately)
/*!40000 ALTER TABLE `tblcountries` DISABLE KEYS */;
INSERT INTO `tblcountries` (`country_id`, `iso2`, `short_name`, `long_name`, `iso3`, `numcode`, `un_member`, `calling_code`, `cctld`) VALUES
	(1, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', 'yes', '93', '.af'),
	(2, 'AX', 'Aland Islands', '&Aring;land Islands', 'ALA', '248', 'no', '358', '.ax'),
	(3, 'AL', 'Albania', 'Republic of Albania', 'ALB', '008', 'yes', '355', '.al'),
	(4, 'DZ', 'Algeria', 'People\'s Democratic Republic of Algeria', 'DZA', '012', 'yes', '213', '.dz'),
	(5, 'AS', 'American Samoa', 'American Samoa', 'ASM', '016', 'no', '1+684', '.as'),
	(6, 'AD', 'Andorra', 'Principality of Andorra', 'AND', '020', 'yes', '376', '.ad'),
	(7, 'AO', 'Angola', 'Republic of Angola', 'AGO', '024', 'yes', '244', '.ao'),
	(8, 'AI', 'Anguilla', 'Anguilla', 'AIA', '660', 'no', '1+264', '.ai'),
	(9, 'AQ', 'Antarctica', 'Antarctica', 'ATA', '010', 'no', '672', '.aq'),
	(10, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', 'yes', '1+268', '.ag'),
	(11, 'AR', 'Argentina', 'Argentine Republic', 'ARG', '032', 'yes', '54', '.ar'),
	(12, 'AM', 'Armenia', 'Republic of Armenia', 'ARM', '051', 'yes', '374', '.am'),
	(13, 'AW', 'Aruba', 'Aruba', 'ABW', '533', 'no', '297', '.aw'),
	(14, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', 'yes', '61', '.au'),
	(15, 'AT', 'Austria', 'Republic of Austria', 'AUT', '040', 'yes', '43', '.at'),
	(16, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', '031', 'yes', '994', '.az'),
	(17, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', 'BHS', '044', 'yes', '1+242', '.bs'),
	(18, 'BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', '048', 'yes', '973', '.bh'),
	(19, 'BD', 'Bangladesh', 'People\'s Republic of Bangladesh', 'BGD', '050', 'yes', '880', '.bd'),
	(20, 'BB', 'Barbados', 'Barbados', 'BRB', '052', 'yes', '1+246', '.bb'),
	(21, 'BY', 'Belarus', 'Republic of Belarus', 'BLR', '112', 'yes', '375', '.by'),
	(22, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', 'yes', '32', '.be'),
	(23, 'BZ', 'Belize', 'Belize', 'BLZ', '084', 'yes', '501', '.bz'),
	(24, 'BJ', 'Benin', 'Republic of Benin', 'BEN', '204', 'yes', '229', '.bj'),
	(25, 'BM', 'Bermuda', 'Bermuda Islands', 'BMU', '060', 'no', '1+441', '.bm'),
	(26, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', 'yes', '975', '.bt'),
	(27, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', 'yes', '591', '.bo'),
	(28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', 'no', '599', '.bq'),
	(29, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', '070', 'yes', '387', '.ba'),
	(30, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', 'yes', '267', '.bw'),
	(31, 'BV', 'Bouvet Island', 'Bouvet Island', 'BVT', '074', 'no', 'NONE', '.bv'),
	(32, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', 'yes', '55', '.br'),
	(33, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IOT', '086', 'no', '246', '.io'),
	(34, 'BN', 'Brunei', 'Brunei Darussalam', 'BRN', '096', 'yes', '673', '.bn'),
	(35, 'BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', '100', 'yes', '359', '.bg'),
	(36, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', 'yes', '226', '.bf'),
	(37, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', 'yes', '257', '.bi'),
	(38, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', 'yes', '855', '.kh'),
	(39, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', 'yes', '237', '.cm'),
	(40, 'CA', 'Canada', 'Canada', 'CAN', '124', 'yes', '1', '.ca'),
	(41, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', '132', 'yes', '238', '.cv'),
	(42, 'KY', 'Cayman Islands', 'The Cayman Islands', 'CYM', '136', 'no', '1+345', '.ky'),
	(43, 'CF', 'Central African Republic', 'Central African Republic', 'CAF', '140', 'yes', '236', '.cf'),
	(44, 'TD', 'Chad', 'Republic of Chad', 'TCD', '148', 'yes', '235', '.td'),
	(45, 'CL', 'Chile', 'Republic of Chile', 'CHL', '152', 'yes', '56', '.cl'),
	(46, 'CN', 'China', 'People\'s Republic of China', 'CHN', '156', 'yes', '86', '.cn'),
	(47, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', 'no', '61', '.cx'),
	(48, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', 'no', '61', '.cc'),
	(49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', '170', 'yes', '57', '.co'),
	(50, 'KM', 'Comoros', 'Union of the Comoros', 'COM', '174', 'yes', '269', '.km'),
	(51, 'CG', 'Congo', 'Republic of the Congo', 'COG', '178', 'yes', '242', '.cg'),
	(52, 'CK', 'Cook Islands', 'Cook Islands', 'COK', '184', 'some', '682', '.ck'),
	(53, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', 'yes', '506', '.cr'),
	(54, 'CI', 'Cote d\'ivoire (Ivory Coast)', 'Republic of C&ocirc;te D\'Ivoire (Ivory Coast)', 'CIV', '384', 'yes', '225', '.ci'),
	(55, 'HR', 'Croatia', 'Republic of Croatia', 'HRV', '191', 'yes', '385', '.hr'),
	(56, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', 'yes', '53', '.cu'),
	(57, 'CW', 'Curacao', 'Cura&ccedil;ao', 'CUW', '531', 'no', '599', '.cw'),
	(58, 'CY', 'Cyprus', 'Republic of Cyprus', 'CYP', '196', 'yes', '357', '.cy'),
	(59, 'CZ', 'Czech Republic', 'Czech Republic', 'CZE', '203', 'yes', '420', '.cz'),
	(60, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'COD', '180', 'yes', '243', '.cd'),
	(61, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', 'yes', '45', '.dk'),
	(62, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', 'yes', '253', '.dj'),
	(63, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', 'yes', '1+767', '.dm'),
	(64, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', 'yes', '1+809, 8', '.do'),
	(65, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', 'yes', '593', '.ec'),
	(66, 'EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', '818', 'yes', '20', '.eg'),
	(67, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', 'yes', '503', '.sv'),
	(68, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', 'yes', '240', '.gq'),
	(69, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', 'yes', '291', '.er'),
	(70, 'EE', 'Estonia', 'Republic of Estonia', 'EST', '233', 'yes', '372', '.ee'),
	(71, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', 'yes', '251', '.et'),
	(72, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FLK', '238', 'no', '500', '.fk'),
	(73, 'FO', 'Faroe Islands', 'The Faroe Islands', 'FRO', '234', 'no', '298', '.fo'),
	(74, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', 'yes', '679', '.fj'),
	(75, 'FI', 'Finland', 'Republic of Finland', 'FIN', '246', 'yes', '358', '.fi'),
	(76, 'FR', 'France', 'French Republic', 'FRA', '250', 'yes', '33', '.fr'),
	(77, 'GF', 'French Guiana', 'French Guiana', 'GUF', '254', 'no', '594', '.gf'),
	(78, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', 'no', '689', '.pf'),
	(79, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', 'no', NULL, '.tf'),
	(80, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', 'yes', '241', '.ga'),
	(81, 'GM', 'Gambia', 'Republic of The Gambia', 'GMB', '270', 'yes', '220', '.gm'),
	(82, 'GE', 'Georgia', 'Georgia', 'GEO', '268', 'yes', '995', '.ge'),
	(83, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', 'yes', '49', '.de'),
	(84, 'GH', 'Ghana', 'Republic of Ghana', 'GHA', '288', 'yes', '233', '.gh'),
	(85, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', 'no', '350', '.gi'),
	(86, 'GR', 'Greece', 'Hellenic Republic', 'GRC', '300', 'yes', '30', '.gr'),
	(87, 'GL', 'Greenland', 'Greenland', 'GRL', '304', 'no', '299', '.gl'),
	(88, 'GD', 'Grenada', 'Grenada', 'GRD', '308', 'yes', '1+473', '.gd'),
	(89, 'GP', 'Guadaloupe', 'Guadeloupe', 'GLP', '312', 'no', '590', '.gp'),
	(90, 'GU', 'Guam', 'Guam', 'GUM', '316', 'no', '1+671', '.gu'),
	(91, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', 'yes', '502', '.gt'),
	(92, 'GG', 'Guernsey', 'Guernsey', 'GGY', '831', 'no', '44', '.gg'),
	(93, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', 'yes', '224', '.gn'),
	(94, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', 'yes', '245', '.gw'),
	(95, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', 'yes', '592', '.gy'),
	(96, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', 'yes', '509', '.ht'),
	(97, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'no', 'NONE', '.hm'),
	(98, 'HN', 'Honduras', 'Republic of Honduras', 'HND', '340', 'yes', '504', '.hn'),
	(99, 'HK', 'Hong Kong', 'Hong Kong', 'HKG', '344', 'no', '852', '.hk'),
	(100, 'HU', 'Hungary', 'Hungary', 'HUN', '348', 'yes', '36', '.hu'),
	(101, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', 'yes', '354', '.is'),
	(102, 'IN', 'India', 'Republic of India', 'IND', '356', 'yes', '91', '.in'),
	(103, 'ID', 'Indonesia', 'Republic of Indonesia', 'IDN', '360', 'yes', '62', '.id'),
	(104, 'IR', 'Iran', 'Islamic Republic of Iran', 'IRN', '364', 'yes', '98', '.ir'),
	(105, 'IQ', 'Iraq', 'Republic of Iraq', 'IRQ', '368', 'yes', '964', '.iq'),
	(106, 'IE', 'Ireland', 'Ireland', 'IRL', '372', 'yes', '353', '.ie'),
	(107, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', 'no', '44', '.im'),
	(108, 'IL', 'Israel', 'State of Israel', 'ISR', '376', 'yes', '972', '.il'),
	(109, 'IT', 'Italy', 'Italian Republic', 'ITA', '380', 'yes', '39', '.jm'),
	(110, 'JM', 'Jamaica', 'Jamaica', 'JAM', '388', 'yes', '1+876', '.jm'),
	(111, 'JP', 'Japan', 'Japan', 'JPN', '392', 'yes', '81', '.jp'),
	(112, 'JE', 'Jersey', 'The Bailiwick of Jersey', 'JEY', '832', 'no', '44', '.je'),
	(113, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', '400', 'yes', '962', '.jo'),
	(114, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', 'yes', '7', '.kz'),
	(115, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', 'yes', '254', '.ke'),
	(116, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', 'yes', '686', '.ki'),
	(117, 'XK', 'Kosovo', 'Republic of Kosovo', '---', '---', 'some', '381', ''),
	(118, 'KW', 'Kuwait', 'State of Kuwait', 'KWT', '414', 'yes', '965', '.kw'),
	(119, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', '417', 'yes', '996', '.kg'),
	(120, 'LA', 'Laos', 'Lao People\'s Democratic Republic', 'LAO', '418', 'yes', '856', '.la'),
	(121, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', 'yes', '371', '.lv'),
	(122, 'LB', 'Lebanon', 'Republic of Lebanon', 'LBN', '422', 'yes', '961', '.lb'),
	(123, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', 'yes', '266', '.ls'),
	(124, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', 'yes', '231', '.lr'),
	(125, 'LY', 'Libya', 'Libya', 'LBY', '434', 'yes', '218', '.ly'),
	(126, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', 'yes', '423', '.li'),
	(127, 'LT', 'Lithuania', 'Republic of Lithuania', 'LTU', '440', 'yes', '370', '.lt'),
	(128, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', 'yes', '352', '.lu'),
	(129, 'MO', 'Macao', 'The Macao Special Administrative Region', 'MAC', '446', 'no', '853', '.mo'),
	(130, 'MK', 'North Macedonia', 'Republic of North Macedonia', 'MKD', '807', 'yes', '389', '.mk'),
	(131, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', 'yes', '261', '.mg'),
	(132, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', 'yes', '265', '.mw'),
	(133, 'MY', 'Malaysia', 'Malaysia', 'MYS', '458', 'yes', '60', '.my'),
	(134, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', 'yes', '960', '.mv'),
	(135, 'ML', 'Mali', 'Republic of Mali', 'MLI', '466', 'yes', '223', '.ml'),
	(136, 'MT', 'Malta', 'Republic of Malta', 'MLT', '470', 'yes', '356', '.mt'),
	(137, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', 'yes', '692', '.mh'),
	(138, 'MQ', 'Martinique', 'Martinique', 'MTQ', '474', 'no', '596', '.mq'),
	(139, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', 'yes', '222', '.mr'),
	(140, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', 'yes', '230', '.mu'),
	(141, 'YT', 'Mayotte', 'Mayotte', 'MYT', '175', 'no', '262', '.yt'),
	(142, 'MX', 'Mexico', 'United Mexican States', 'MEX', '484', 'yes', '52', '.mx'),
	(143, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', 'yes', '691', '.fm'),
	(144, 'MD', 'Moldava', 'Republic of Moldova', 'MDA', '498', 'yes', '373', '.md'),
	(145, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', 'yes', '377', '.mc'),
	(146, 'MN', 'Mongolia', 'Mongolia', 'MNG', '496', 'yes', '976', '.mn'),
	(147, 'ME', 'Montenegro', 'Montenegro', 'MNE', '499', 'yes', '382', '.me'),
	(148, 'MS', 'Montserrat', 'Montserrat', 'MSR', '500', 'no', '1+664', '.ms'),
	(149, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', 'yes', '212', '.ma'),
	(150, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', 'yes', '258', '.mz'),
	(151, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MMR', '104', 'yes', '95', '.mm'),
	(152, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', 'yes', '264', '.na'),
	(153, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', 'yes', '674', '.nr'),
	(154, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', 'yes', '977', '.np'),
	(155, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', 'yes', '31', '.nl'),
	(156, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', 'no', '687', '.nc'),
	(157, 'NZ', 'New Zealand', 'New Zealand', 'NZL', '554', 'yes', '64', '.nz'),
	(158, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', 'yes', '505', '.ni'),
	(159, 'NE', 'Niger', 'Republic of Niger', 'NER', '562', 'yes', '227', '.ne'),
	(160, 'NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', '566', 'yes', '234', '.ng'),
	(161, 'NU', 'Niue', 'Niue', 'NIU', '570', 'some', '683', '.nu'),
	(162, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', 'no', '672', '.nf'),
	(163, 'KP', 'North Korea', 'Democratic People\'s Republic of Korea', 'PRK', '408', 'yes', '850', '.kp'),
	(164, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', 'MNP', '580', 'no', '1+670', '.mp'),
	(165, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', 'yes', '47', '.no'),
	(166, 'OM', 'Oman', 'Sultanate of Oman', 'OMN', '512', 'yes', '968', '.om'),
	(167, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', '586', 'yes', '92', '.pk'),
	(168, 'PW', 'Palau', 'Republic of Palau', 'PLW', '585', 'yes', '680', '.pw'),
	(169, 'PS', 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', 'PSE', '275', 'some', '970', '.ps'),
	(170, 'PA', 'Panama', 'Republic of Panama', 'PAN', '591', 'yes', '507', '.pa'),
	(171, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', 'yes', '675', '.pg'),
	(172, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', 'yes', '595', '.py'),
	(173, 'PE', 'Peru', 'Republic of Peru', 'PER', '604', 'yes', '51', '.pe'),
	(174, 'PH', 'Phillipines', 'Republic of the Philippines', 'PHL', '608', 'yes', '63', '.ph'),
	(175, 'PN', 'Pitcairn', 'Pitcairn', 'PCN', '612', 'no', 'NONE', '.pn'),
	(176, 'PL', 'Poland', 'Republic of Poland', 'POL', '616', 'yes', '48', '.pl'),
	(177, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', 'yes', '351', '.pt'),
	(178, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', 'no', '1+939', '.pr'),
	(179, 'QA', 'Qatar', 'State of Qatar', 'QAT', '634', 'yes', '974', '.qa'),
	(180, 'RE', 'Reunion', 'R&eacute;union', 'REU', '638', 'no', '262', '.re'),
	(181, 'RO', 'Romania', 'Romania', 'ROU', '642', 'yes', '40', '.ro'),
	(182, 'RU', 'Russia', 'Russian Federation', 'RUS', '643', 'yes', '7', '.ru'),
	(183, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', 'yes', '250', '.rw'),
	(184, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BLM', '652', 'no', '590', '.bl'),
	(185, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', 'no', '290', '.sh'),
	(186, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KNA', '659', 'yes', '1+869', '.kn'),
	(187, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', 'yes', '1+758', '.lc'),
	(188, 'MF', 'Saint Martin', 'Saint Martin', 'MAF', '663', 'no', '590', '.mf'),
	(189, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', 'no', '508', '.pm'),
	(190, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', 'yes', '1+784', '.vc'),
	(191, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', 'yes', '685', '.ws'),
	(192, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', 'yes', '378', '.sm'),
	(193, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'STP', '678', 'yes', '239', '.st'),
	(194, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', '682', 'yes', '966', '.sa'),
	(195, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', 'yes', '221', '.sn'),
	(196, 'RS', 'Serbia', 'Republic of Serbia', 'SRB', '688', 'yes', '381', '.rs'),
	(197, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', 'yes', '248', '.sc'),
	(198, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', 'yes', '232', '.sl'),
	(199, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', 'yes', '65', '.sg'),
	(200, 'SX', 'Sint Maarten', 'Sint Maarten', 'SXM', '534', 'no', '1+721', '.sx'),
	(201, 'SK', 'Slovakia', 'Slovak Republic', 'SVK', '703', 'yes', '421', '.sk'),
	(202, 'SI', 'Slovenia', 'Republic of Slovenia', 'SVN', '705', 'yes', '386', '.si'),
	(203, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', 'yes', '677', '.sb'),
	(204, 'SO', 'Somalia', 'Somali Republic', 'SOM', '706', 'yes', '252', '.so'),
	(205, 'ZA', 'South Africa', 'Republic of South Africa', 'ZAF', '710', 'yes', '27', '.za'),
	(206, 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', '239', 'no', '500', '.gs'),
	(207, 'KR', 'South Korea', 'Republic of Korea', 'KOR', '410', 'yes', '82', '.kr'),
	(208, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', 'yes', '211', '.ss'),
	(209, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', 'yes', '34', '.es'),
	(210, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', 'yes', '94', '.lk'),
	(211, 'SD', 'Sudan', 'Republic of the Sudan', 'SDN', '729', 'yes', '249', '.sd'),
	(212, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', 'yes', '597', '.sr'),
	(213, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJM', '744', 'no', '47', '.sj'),
	(214, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', '748', 'yes', '268', '.sz'),
	(215, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', 'yes', '46', '.se'),
	(216, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', 'yes', '41', '.ch'),
	(217, 'SY', 'Syria', 'Syrian Arab Republic', 'SYR', '760', 'yes', '963', '.sy'),
	(218, 'TW', 'Taiwan', 'Republic of China (Taiwan)', 'TWN', '158', 'former', '886', '.tw'),
	(219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', 'yes', '992', '.tj'),
	(220, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', 'yes', '255', '.tz'),
	(221, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', 'yes', '66', '.th'),
	(222, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TLS', '626', 'yes', '670', '.tl'),
	(223, 'TG', 'Togo', 'Togolese Republic', 'TGO', '768', 'yes', '228', '.tg'),
	(224, 'TK', 'Tokelau', 'Tokelau', 'TKL', '772', 'no', '690', '.tk'),
	(225, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', 'yes', '676', '.to'),
	(226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', 'yes', '1+868', '.tt'),
	(227, 'TN', 'Tunisia', 'Republic of Tunisia', 'TUN', '788', 'yes', '216', '.tn'),
	(228, 'TR', 'Turkey', 'Republic of Turkey', 'TUR', '792', 'yes', '90', '.tr'),
	(229, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', 'yes', '993', '.tm'),
	(230, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', 'no', '1+649', '.tc'),
	(231, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', 'yes', '688', '.tv'),
	(232, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', 'yes', '256', '.ug'),
	(233, 'UA', 'Ukraine', 'Ukraine', 'UKR', '804', 'yes', '380', '.ua'),
	(234, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', 'yes', '971', '.ae'),
	(235, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GBR', '826', 'yes', '44', '.uk'),
	(236, 'US', 'United States', 'United States of America', 'USA', '840', 'yes', '1', '.us'),
	(237, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'no', 'NONE', 'NONE'),
	(238, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', 'yes', '598', '.uy'),
	(239, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', 'yes', '998', '.uz'),
	(240, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', 'yes', '678', '.vu'),
	(241, 'VA', 'Vatican City', 'State of the Vatican City', 'VAT', '336', 'no', '39', '.va'),
	(242, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', 'yes', '58', '.ve'),
	(243, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', 'yes', '84', '.vn'),
	(244, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', '092', 'no', '1+284', '.vg'),
	(245, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', 'VIR', '850', 'no', '1+340', '.vi'),
	(246, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', 'no', '681', '.wf'),
	(247, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', 'no', '212', '.eh'),
	(248, 'YE', 'Yemen', 'Republic of Yemen', 'YEM', '887', 'yes', '967', '.ye'),
	(249, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', 'yes', '260', '.zm'),
	(250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', 'yes', '263', '.zw');
/*!40000 ALTER TABLE `tblcountries` ENABLE KEYS */;

-- Dumping structure for table crm.tblcreditnotes
CREATE TABLE IF NOT EXISTS `tblcreditnotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `deleted_customer_name` varchar(100) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '1',
  `datecreated` datetime NOT NULL,
  `date` date NOT NULL,
  `adminnote` text,
  `terms` text,
  `clientnote` text,
  `currency` int(11) NOT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `total_tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL,
  `adjustment` decimal(15,2) DEFAULT NULL,
  `addedfrom` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `discount_percent` decimal(15,2) DEFAULT '0.00',
  `discount_total` decimal(15,2) DEFAULT '0.00',
  `discount_type` varchar(30) NOT NULL,
  `billing_street` varchar(200) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_state` varchar(100) DEFAULT NULL,
  `billing_zip` varchar(100) DEFAULT NULL,
  `billing_country` int(11) DEFAULT NULL,
  `shipping_street` varchar(200) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_zip` varchar(100) DEFAULT NULL,
  `shipping_country` int(11) DEFAULT NULL,
  `include_shipping` tinyint(1) NOT NULL,
  `show_shipping_on_credit_note` tinyint(1) NOT NULL DEFAULT '1',
  `show_quantity_as` int(11) NOT NULL DEFAULT '1',
  `reference_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currency` (`currency`),
  KEY `clientid` (`clientid`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcreditnotes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcreditnotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcreditnotes` ENABLE KEYS */;

-- Dumping structure for table crm.tblcreditnote_refunds
CREATE TABLE IF NOT EXISTS `tblcreditnote_refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_note_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `refunded_on` date NOT NULL,
  `payment_mode` varchar(40) NOT NULL,
  `note` text,
  `amount` decimal(15,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcreditnote_refunds: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcreditnote_refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcreditnote_refunds` ENABLE KEYS */;

-- Dumping structure for table crm.tblcredits
CREATE TABLE IF NOT EXISTS `tblcredits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `date_applied` datetime NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcredits: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcredits` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcredits` ENABLE KEYS */;

-- Dumping structure for table crm.tblcurrencies
CREATE TABLE IF NOT EXISTS `tblcurrencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `conversion_rate` float DEFAULT NULL,
  `decimal_separator` varchar(5) DEFAULT NULL,
  `thousand_separator` varchar(5) DEFAULT NULL,
  `placement` varchar(10) DEFAULT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcurrencies: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblcurrencies` DISABLE KEYS */;
INSERT INTO `tblcurrencies` (`id`, `symbol`, `name`, `conversion_rate`, `decimal_separator`, `thousand_separator`, `placement`, `isdefault`) VALUES
	(1, '$', 'USD', 1, '.', ',', 'before', 1),
	(2, '€', 'EUR', 22.4, ',', '.', 'before', 0),
	(3, 'Rs.', 'INR', 73.17, '.', ',', 'before', 0);
/*!40000 ALTER TABLE `tblcurrencies` ENABLE KEYS */;

-- Dumping structure for table crm.tblcustomers_groups
CREATE TABLE IF NOT EXISTS `tblcustomers_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcustomers_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcustomers_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcustomers_groups` ENABLE KEYS */;

-- Dumping structure for table crm.tblcustomer_admins
CREATE TABLE IF NOT EXISTS `tblcustomer_admins` (
  `staff_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_assigned` text NOT NULL,
  KEY `customer_id` (`customer_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcustomer_admins: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcustomer_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcustomer_admins` ENABLE KEYS */;

-- Dumping structure for table crm.tblcustomer_groups
CREATE TABLE IF NOT EXISTS `tblcustomer_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `groupid` (`groupid`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcustomer_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcustomer_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcustomer_groups` ENABLE KEYS */;

-- Dumping structure for table crm.tblcustomfields
CREATE TABLE IF NOT EXISTS `tblcustomfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldto` varchar(15) NOT NULL,
  `name` varchar(150) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL,
  `options` mediumtext,
  `display_inline` tinyint(1) NOT NULL DEFAULT '0',
  `field_order` int(11) DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `show_on_pdf` int(11) NOT NULL DEFAULT '0',
  `show_on_ticket_form` tinyint(1) NOT NULL DEFAULT '0',
  `only_admin` tinyint(1) NOT NULL DEFAULT '0',
  `show_on_table` tinyint(1) NOT NULL DEFAULT '0',
  `show_on_client_portal` int(11) NOT NULL DEFAULT '0',
  `disalow_client_to_edit` int(11) NOT NULL DEFAULT '0',
  `bs_column` int(11) NOT NULL DEFAULT '12',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcustomfields: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcustomfields` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcustomfields` ENABLE KEYS */;

-- Dumping structure for table crm.tblcustomfieldsvalues
CREATE TABLE IF NOT EXISTS `tblcustomfieldsvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relid` int(11) NOT NULL,
  `fieldid` int(11) NOT NULL,
  `fieldto` varchar(15) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relid` (`relid`),
  KEY `fieldto` (`fieldto`),
  KEY `fieldid` (`fieldid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblcustomfieldsvalues: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblcustomfieldsvalues` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcustomfieldsvalues` ENABLE KEYS */;

-- Dumping structure for table crm.tbldepartments
CREATE TABLE IF NOT EXISTS `tbldepartments` (
  `departmentid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `imap_username` varchar(191) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_from_header` tinyint(1) NOT NULL DEFAULT '0',
  `host` varchar(150) DEFAULT NULL,
  `password` mediumtext,
  `encryption` varchar(3) DEFAULT NULL,
  `folder` varchar(191) NOT NULL DEFAULT 'INBOX',
  `delete_after_import` int(11) NOT NULL DEFAULT '0',
  `calendar_id` mediumtext,
  `hidefromclient` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`departmentid`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbldepartments: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbldepartments` DISABLE KEYS */;
INSERT INTO `tbldepartments` (`departmentid`, `name`, `imap_username`, `email`, `email_from_header`, `host`, `password`, `encryption`, `folder`, `delete_after_import`, `calendar_id`, `hidefromclient`) VALUES
	(1, 'Support and Maintence', 'itsm@powermyit.com', 'leocrmtest@gmail.com', 0, 'outlook.office365.com', '7e05ec45dbc9efbe6977161eeb130ab9af6c3ff257860942d54285584561221624d816a27a731f192ec04f420f1144765963ae694365c0aca8d6c3206dc356695/VxHPNnD00prb2qLw/XnO7Pu8hS44Y4F4epJgI5aSw=', 'tls', 'INBOX', 0, NULL, 1),
	(2, 'Technical Lead', '', 'techlead@gmail.com', 0, '', '', '', 'INBOX', 0, NULL, 0),
	(6, 'Salse', '', 'salse@mail.com', 0, '', '', '', 'INBOX', 0, NULL, 0);
/*!40000 ALTER TABLE `tbldepartments` ENABLE KEYS */;

-- Dumping structure for table crm.tbldiagrams
CREATE TABLE IF NOT EXISTS `tbldiagrams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `diagram_file` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tbldiagrams: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbldiagrams` DISABLE KEYS */;
INSERT INTO `tbldiagrams` (`id`, `title`, `clientid`, `diagram_file`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 'LeoDiagram', 1, 'bg5.jpg', 'New LeoDiagram Test', '2021-04-05 09:37:53', 2021, '2021-04-05 11:10:21', NULL),
	(2, 'New Diagram', 5, 'NCDEX-homepage_banner__20211028.jpg', '', '2021-11-08 11:45:49', 2021, '2021-11-08 11:45:49', NULL);
/*!40000 ALTER TABLE `tbldiagrams` ENABLE KEYS */;

-- Dumping structure for table crm.tbldismissed_announcements
CREATE TABLE IF NOT EXISTS `tbldismissed_announcements` (
  `dismissedannouncementid` int(11) NOT NULL AUTO_INCREMENT,
  `announcementid` int(11) NOT NULL,
  `staff` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`dismissedannouncementid`),
  KEY `announcementid` (`announcementid`),
  KEY `staff` (`staff`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbldismissed_announcements: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbldismissed_announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbldismissed_announcements` ENABLE KEYS */;

-- Dumping structure for table crm.tblemailtemplates
CREATE TABLE IF NOT EXISTS `tblemailtemplates` (
  `emailtemplateid` int(11) NOT NULL AUTO_INCREMENT,
  `type` mediumtext NOT NULL,
  `slug` varchar(100) NOT NULL,
  `language` varchar(40) DEFAULT NULL,
  `name` mediumtext NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` text NOT NULL,
  `fromname` mediumtext NOT NULL,
  `fromemail` varchar(100) DEFAULT NULL,
  `plaintext` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL,
  PRIMARY KEY (`emailtemplateid`)
) ENGINE=InnoDB AUTO_INCREMENT=2377 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblemailtemplates: ~2,124 rows (approximately)
/*!40000 ALTER TABLE `tblemailtemplates` DISABLE KEYS */;
INSERT INTO `tblemailtemplates` (`emailtemplateid`, `type`, `slug`, `language`, `name`, `subject`, `message`, `fromname`, `fromemail`, `plaintext`, `active`, `order`) VALUES
	(1, 'client', 'new-client-created', 'english', 'New Contact Added/Registered (Welcome Email)', 'Welcome aboard', 'Dear {contact_firstname} {contact_lastname}<br /><br />Thank you for registering on the <strong>{companyname}</strong> CRM System.<br /><br />We just wanted to say welcome.<br /><br />Please contact us if you need any help.<br /><br />Click here to view your profile: <a href="{crm_url}">{crm_url}</a><br /><br />Kind Regards, <br />{email_signature}<br /><br />(This is an automated email, so please don\'t reply to this email address)', '{companyname} | CRM', '', 0, 1, 0),
	(2, 'invoice', 'invoice-send-to-client', 'english', 'Send Invoice to Customer', 'Invoice with number {invoice_number} created', '<span style="font-size: 12pt;">Dear {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">We have prepared the following invoice for you: <strong># {invoice_number}</strong></span><br /><br /><span style="font-size: 12pt;"><strong>Invoice status</strong>: {invoice_status}</span><br /><br /><span style="font-size: 12pt;">You can view the invoice on the following link: <a href="{invoice_link}">{invoice_number}</a></span><br /><br /><span style="font-size: 12pt;">Please contact us for more information.</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(3, 'ticket', 'new-ticket-opened-admin', 'english', 'New Ticket Opened (Opened by Staff, Sent to Customer)', 'New Support Ticket Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support ticket has been opened.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}<br /></span><strong><strong>Billing Type : </strong></strong><span style="font-size: 12pt;">{ticket_billing_type}<br /><strong>Contract : </strong>{ticket_contract}</span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_public_url}">#{ticket_id}</a><br /><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(4, 'ticket', 'ticket-reply', 'english', 'Ticket Reply (Sent to Customer)', 'New Ticket Reply', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">You have a new ticket reply to ticket <a href="{ticket_public_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;"><strong>Ticket Subject:</strong> {ticket_subject}<br /></span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_public_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(5, 'ticket', 'ticket-autoresponse', 'english', 'New Ticket Opened - Autoresponse', 'New Support Ticket Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_public_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(6, 'invoice', 'invoice-payment-recorded', 'english', 'Invoice Payment Recorded (Sent to Customer)', 'Invoice Payment Recorded', '<span style="font-size: 12pt;">Hello {contact_firstname}&nbsp;{contact_lastname}<br /><br /></span>Thank you for the payment. Find the payment details below:<br /><br />-------------------------------------------------<br /><br />Amount:&nbsp;<strong>{payment_total}<br /></strong>Date:&nbsp;<strong>{payment_date}</strong><br />Invoice number:&nbsp;<span style="font-size: 12pt;"><strong># {invoice_number}<br /><br /></strong></span>-------------------------------------------------<br /><br />You can always view the invoice for this payment at the following link:&nbsp;<a href="{invoice_link}"><span style="font-size: 12pt;">{invoice_number}</span></a><br /><br />We are looking forward working with you.<br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(7, 'invoice', 'invoice-overdue-notice', 'english', 'Invoice Overdue Notice', 'Invoice Overdue Notice - {invoice_number}', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">This is an overdue notice for invoice <strong># {invoice_number}</strong></span><br /><br /><span style="font-size: 12pt;">This invoice was due: {invoice_duedate}</span><br /><br /><span style="font-size: 12pt;">You can view the invoice on the following link: <a href="{invoice_link}">{invoice_number}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(8, 'invoice', 'invoice-already-send', 'english', 'Invoice Already Sent to Customer', 'Invoice # {invoice_number} ', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">At your request, here is the invoice with number <strong># {invoice_number}</strong></span><br /><br /><span style="font-size: 12pt;">You can view the invoice on the following link: <a href="{invoice_link}">{invoice_number}</a></span><br /><br /><span style="font-size: 12pt;">Please contact us for more information.</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(9, 'ticket', 'new-ticket-created-staff', 'english', 'New Ticket Created (Opened by Customer, Sent to Staff Members)', 'New Ticket Created', '<span style="font-size: 12pt;">A new support ticket has been opened.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {ticket_priority}<br /></span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_url}">#{ticket_id}</a></span><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(10, 'estimate', 'estimate-send-to-client', 'english', 'Send Estimate to Customer', 'Estimate # {estimate_number} created', '<span style="font-size: 12pt;">Dear {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">Please find the attached estimate <strong># {estimate_number}</strong></span><br /><br /><span style="font-size: 12pt;"><strong>Estimate status:</strong> {estimate_status}</span><br /><br /><span style="font-size: 12pt;">You can view the estimate on the following link: <a href="{estimate_link}">{estimate_number}</a></span><br /><br /><span style="font-size: 12pt;">We look forward to your communication.</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}<br /></span>', '{companyname} | CRM', '', 0, 1, 0),
	(11, 'ticket', 'ticket-reply-to-admin', 'english', 'Ticket Reply (Sent to Staff)', 'New Support Ticket Reply', '<span style="font-size: 12pt;">A new support ticket reply from {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {ticket_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(12, 'estimate', 'estimate-already-send', 'english', 'Estimate Already Sent to Customer', 'Estimate # {estimate_number} ', '<span style="font-size: 12pt;">Dear {contact_firstname} {contact_lastname}</span><br /> <br /><span style="font-size: 12pt;">Thank you for your estimate request.</span><br /> <br /><span style="font-size: 12pt;">You can view the estimate on the following link: <a href="{estimate_link}">{estimate_number}</a></span><br /> <br /><span style="font-size: 12pt;">Please contact us for more information.</span><br /> <br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(13, 'contract', 'contract-expiration', 'english', 'Contract Expiration Reminder (Sent to Customer Contacts)', 'Contract Expiration Reminder', '<span style="font-size: 12pt;">Dear {client_company}</span><br /><br /><span style="font-size: 12pt;">This is a reminder that the following contract will expire soon:</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {contract_subject}</span><br /><span style="font-size: 12pt;"><strong>Description:</strong> {contract_description}</span><br /><span style="font-size: 12pt;"><strong>Date Start:</strong> {contract_datestart}</span><br /><span style="font-size: 12pt;"><strong>Date End:</strong> {contract_dateend}</span><br /><br /><span style="font-size: 12pt;">Please contact us for more information.</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(14, 'tasks', 'task-assigned', 'english', 'New Task Assigned (Sent to Staff)', 'New Task Assigned to You - {task_name}', '<span style="font-size: 12pt;">Dear {staff_firstname}</span><br /><br /><span style="font-size: 12pt;">You have been assigned to a new task:</span><br /><br /><span style="font-size: 12pt;"><strong>Name:</strong> {task_name}<br /></span><strong>Start Date:</strong> {task_startdate}<br /><span style="font-size: 12pt;"><strong>Due date:</strong> {task_duedate}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {task_priority}<br /><br /></span><span style="font-size: 12pt;"><span>You can view the task on the following link</span>: <a href="{task_link}">{task_name}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(15, 'tasks', 'task-added-as-follower', 'english', 'Staff Member Added as Follower on Task (Sent to Staff)', 'You are added as follower on task - {task_name}', '<span style="font-size: 12pt;">Hi {staff_firstname}<br /></span><br /><span style="font-size: 12pt;">You have been added as follower on the following task:</span><br /><br /><span style="font-size: 12pt;"><strong>Name:</strong> {task_name}</span><br /><span style="font-size: 12pt;"><strong>Start date:</strong> {task_startdate}</span><br /><br /><span>You can view the task on the following link</span><span>: </span><a href="{task_link}">{task_name}</a><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(16, 'tasks', 'task-commented', 'english', 'New Comment on Task (Sent to Staff)', 'New Comment on Task - {task_name}', 'Dear {staff_firstname}<br /><br />A comment has been made on the following task:<br /><br /><strong>Name:</strong> {task_name}<br /><strong>Comment:</strong> {task_comment}<br /><br />You can view the task on the following link: <a href="{task_link}">{task_name}</a><br /><br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(17, 'tasks', 'task-added-attachment', 'english', 'New Attachment(s) on Task (Sent to Staff)', 'New Attachment on Task - {task_name}', 'Hi {staff_firstname}<br /><br /><strong>{task_user_take_action}</strong> added an attachment on the following task:<br /><br /><strong>Name:</strong> {task_name}<br /><br />You can view the task on the following link: <a href="{task_link}">{task_name}</a><br /><br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(18, 'estimate', 'estimate-declined-to-staff', 'english', 'Estimate Declined (Sent to Staff)', 'Customer Declined Estimate', '<span style="font-size: 12pt;">Hi</span><br /> <br /><span style="font-size: 12pt;">Customer ({client_company}) declined estimate with number <strong># {estimate_number}</strong></span><br /> <br /><span style="font-size: 12pt;">You can view the estimate on the following link: <a href="{estimate_link}">{estimate_number}</a></span><br /> <br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(19, 'estimate', 'estimate-accepted-to-staff', 'english', 'Estimate Accepted (Sent to Staff)', 'Customer Accepted Estimate', '<span style="font-size: 12pt;">Hi</span><br /> <br /><span style="font-size: 12pt;">Customer ({client_company}) accepted estimate with number <strong># {estimate_number}</strong></span><br /> <br /><span style="font-size: 12pt;">You can view the estimate on the following link: <a href="{estimate_link}">{estimate_number}</a></span><br /> <br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(20, 'proposals', 'proposal-client-accepted', 'english', 'Customer Action - Accepted (Sent to Staff)', 'Customer Accepted Proposal', '<div>Hi<br /> <br />Client <strong>{proposal_proposal_to}</strong> accepted the following proposal:<br /> <br /><strong>Number:</strong> {proposal_number}<br /><strong>Subject</strong>: {proposal_subject}<br /><strong>Total</strong>: {proposal_total}<br /> <br />View the proposal on the following link: <a href="{proposal_link}">{proposal_number}</a><br /> <br />Kind Regards,<br />{email_signature}</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>', '{companyname} | CRM', '', 0, 1, 0),
	(21, 'proposals', 'proposal-send-to-customer', 'english', 'Send Proposal to Customer', 'Proposal With Number {proposal_number} Created', 'Dear {proposal_proposal_to}<br /><br />Please find our attached proposal.<br /><br />This proposal is valid until: {proposal_open_till}<br />You can view the proposal on the following link: <a href="{proposal_link}">{proposal_number}</a><br /><br />Please don\'t hesitate to comment online if you have any questions.<br /><br />We look forward to your communication.<br /><br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(22, 'proposals', 'proposal-client-declined', 'english', 'Customer Action - Declined (Sent to Staff)', 'Client Declined Proposal', 'Hi<br /> <br />Customer <strong>{proposal_proposal_to}</strong> declined the proposal <strong>{proposal_subject}</strong><br /> <br />View the proposal on the following link <a href="{proposal_link}">{proposal_number}</a>&nbsp;or from the admin area.<br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(23, 'proposals', 'proposal-client-thank-you', 'english', 'Thank You Email (Sent to Customer After Accept)', 'Thank for you accepting proposal', 'Dear {proposal_proposal_to}<br /> <br />Thank for for accepting the proposal.<br /> <br />We look forward to doing business with you.<br /> <br />We will contact you as soon as possible<br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(24, 'proposals', 'proposal-comment-to-client', 'english', 'New Comment  (Sent to Customer/Lead)', 'New Proposal Comment', 'Dear {proposal_proposal_to}<br /> <br />A new comment has been made on the following proposal: <strong>{proposal_number}</strong><br /> <br />You can view and reply to the comment on the following link: <a href="{proposal_link}">{proposal_number}</a><br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(25, 'proposals', 'proposal-comment-to-admin', 'english', 'New Comment (Sent to Staff) ', 'New Proposal Comment', 'Hi<br /> <br />A new comment has been made to the proposal <strong>{proposal_subject}</strong><br /> <br />You can view and reply to the comment on the following link: <a href="{proposal_link}">{proposal_number}</a>&nbsp;or from the admin area.<br /> <br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(26, 'estimate', 'estimate-thank-you-to-customer', 'english', 'Thank You Email (Sent to Customer After Accept)', 'Thank for you accepting estimate', '<span style="font-size: 12pt;">Dear {contact_firstname} {contact_lastname}</span><br /> <br /><span style="font-size: 12pt;">Thank for for accepting the estimate.</span><br /> <br /><span style="font-size: 12pt;">We look forward to doing business with you.</span><br /> <br /><span style="font-size: 12pt;">We will contact you as soon as possible.</span><br /> <br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(27, 'tasks', 'task-deadline-notification', 'english', 'Task Deadline Reminder - Sent to Assigned Members', 'Task Deadline Reminder', 'Hi {staff_firstname}&nbsp;{staff_lastname}<br /><br />This is an automated email from {companyname}.<br /><br />The task <strong>{task_name}</strong> deadline is on <strong>{task_duedate}</strong>. <br />This task is still not finished.<br /><br />You can view the task on the following link: <a href="{task_link}">{task_name}</a><br /><br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(28, 'contract', 'send-contract', 'english', 'Send Contract to Customer', 'Contract - {contract_subject}', '<p><span style="font-size: 12pt;">Hi&nbsp;{contact_firstname}&nbsp;{contact_lastname}</span><br /><br /><span style="font-size: 12pt;">Please find the <a href="{contract_link}">{contract_subject}</a> attached.<br /><br />Description: {contract_description}<br /><br /></span><span style="font-size: 12pt;">Looking forward to hear from you.</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(29, 'invoice', 'invoice-payment-recorded-to-staff', 'english', 'Invoice Payment Recorded (Sent to Staff)', 'New Invoice Payment', '<span style="font-size: 12pt;">Hi</span><br /><br /><span style="font-size: 12pt;">Customer recorded payment for invoice <strong># {invoice_number}</strong></span><br /> <br /><span style="font-size: 12pt;">You can view the invoice on the following link: <a href="{invoice_link}">{invoice_number}</a></span><br /> <br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(30, 'ticket', 'auto-close-ticket', 'english', 'Auto Close Ticket', 'Ticket Auto Closed', '<p><span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">Ticket {ticket_subject} has been auto close due to inactivity.</span><br /><br /><span style="font-size: 12pt;"><strong>Ticket #</strong>: <a href="{ticket_public_url}">{ticket_id}</a></span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(31, 'project', 'new-project-discussion-created-to-staff', 'english', 'New Project Discussion (Sent to Project Members)', 'New Project Discussion Created - {project_name}', '<p>Hi {staff_firstname}<br /><br />New project discussion created from <strong>{discussion_creator}</strong><br /><br /><strong>Subject:</strong> {discussion_subject}<br /><strong>Description:</strong> {discussion_description}<br /><br />You can view the discussion on the following link: <a href="{discussion_link}">{discussion_subject}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(32, 'project', 'new-project-discussion-created-to-customer', 'english', 'New Project Discussion (Sent to Customer Contacts)', 'New Project Discussion Created - {project_name}', '<p>Hello {contact_firstname} {contact_lastname}<br /><br />New project discussion created from <strong>{discussion_creator}</strong><br /><br /><strong>Subject:</strong> {discussion_subject}<br /><strong>Description:</strong> {discussion_description}<br /><br />You can view the discussion on the following link: <a href="{discussion_link}">{discussion_subject}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(33, 'project', 'new-project-file-uploaded-to-customer', 'english', 'New Project File(s) Uploaded (Sent to Customer Contacts)', 'New Project File(s) Uploaded - {project_name}', '<p>Hello {contact_firstname} {contact_lastname}<br /><br />New project file is uploaded on <strong>{project_name}</strong> from <strong>{file_creator}</strong><br /><br />You can view the project on the following link: <a href="{project_link}">{project_name}</a><br /><br />To view the file in our CRM you can click on the following link: <a href="{discussion_link}">{discussion_subject}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(34, 'project', 'new-project-file-uploaded-to-staff', 'english', 'New Project File(s) Uploaded (Sent to Project Members)', 'New Project File(s) Uploaded - {project_name}', '<p>Hello&nbsp;{staff_firstname}</p>\r\n<p>New project&nbsp;file is uploaded on&nbsp;<strong>{project_name}</strong> from&nbsp;<strong>{file_creator}</strong></p>\r\n<p>You can view the project on the following link: <a href="{project_link}">{project_name}<br /></a><br />To view&nbsp;the file you can click on the following link: <a href="{discussion_link}">{discussion_subject}</a></p>\r\n<p>Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(35, 'project', 'new-project-discussion-comment-to-customer', 'english', 'New Discussion Comment  (Sent to Customer Contacts)', 'New Discussion Comment', '<p><span style="font-size: 12pt;">Hello {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New discussion comment has been made on <strong>{discussion_subject}</strong> from <strong>{comment_creator}</strong></span><br /><br /><span style="font-size: 12pt;"><strong>Discussion subject:</strong> {discussion_subject}</span><br /><span style="font-size: 12pt;"><strong>Comment</strong>: {discussion_comment}</span><br /><br /><span style="font-size: 12pt;">You can view the discussion on the following link: <a href="{discussion_link}">{discussion_subject}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(36, 'project', 'new-project-discussion-comment-to-staff', 'english', 'New Discussion Comment (Sent to Project Members)', 'New Discussion Comment', '<p>Hi {staff_firstname}<br /><br />New discussion comment has been made on <strong>{discussion_subject}</strong> from <strong>{comment_creator}</strong><br /><br /><strong>Discussion subject:</strong> {discussion_subject}<br /><strong>Comment:</strong> {discussion_comment}<br /><br />You can view the discussion on the following link: <a href="{discussion_link}">{discussion_subject}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(37, 'project', 'staff-added-as-project-member', 'english', 'Staff Added as Project Member', 'New project assigned to you', '<p>Hi {staff_firstname}<br /><br />New project has been assigned to you.<br /><br />You can view the project on the following link <a href="{project_link}">{project_name}</a><br /><br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(38, 'estimate', 'estimate-expiry-reminder', 'english', 'Estimate Expiration Reminder', 'Estimate Expiration Reminder', '<p><span style="font-size: 12pt;">Hello {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">The estimate with <strong># {estimate_number}</strong> will expire on <strong>{estimate_expirydate}</strong></span><br /><br /><span style="font-size: 12pt;">You can view the estimate on the following link: <a href="{estimate_link}">{estimate_number}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(39, 'proposals', 'proposal-expiry-reminder', 'english', 'Proposal Expiration Reminder', 'Proposal Expiration Reminder', '<p>Hello {proposal_proposal_to}<br /><br />The proposal {proposal_number}&nbsp;will expire on <strong>{proposal_open_till}</strong><br /><br />You can view the proposal on the following link: <a href="{proposal_link}">{proposal_number}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(40, 'staff', 'new-staff-created', 'english', 'New Staff Created (Welcome Email)', 'You are added as staff member', 'Hi {staff_firstname}<br /><br />You are added as member on our CRM.<br /><br />Please use the following logic credentials:<br /><br /><strong>Email:</strong> {staff_email}<br /><strong>Password:</strong> {password}<br /><br />Click <a href="{admin_url}">here </a>to login in the dashboard.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(41, 'client', 'contact-forgot-password', 'english', 'Forgot Password', 'Create New Password', '<h2>Create a new password</h2>\r\nForgot your password?<br /> To create a new password, just follow this link:<br /> <br /><a href="{reset_password_url}">Reset Password</a><br /> <br /> You received this email, because it was requested by a {companyname}&nbsp;user. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same. <br /><br /> {email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(42, 'client', 'contact-password-reseted', 'english', 'Password Reset - Confirmation', 'Your password has been changed', '<strong><span style="font-size: 14pt;">You have changed your password.</span><br /></strong><br /> Please, keep it in your records so you don\'t forget it.<br /> <br /> Your email address for login is: {contact_email}<br /><br />If this wasnt you, please contact us.<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(43, 'client', 'contact-set-password', 'english', 'Set New Password', 'Set new password on {companyname} ', '<h2><span style="font-size: 14pt;">Setup your new password on {companyname}</span></h2>\r\nPlease use the following link to set up your new password:<br /><br /><a href="{set_password_url}">Set new password</a><br /><br />Keep it in your records so you don\'t forget it.<br /><br />Please set your new password in <strong>48 hours</strong>. After that, you won\'t be able to set your password because this link will expire.<br /><br />You can login at: <a href="{crm_url}">{crm_url}</a><br />Your email address for login: {contact_email}<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(44, 'staff', 'staff-forgot-password', 'english', 'Forgot Password', 'Create New Password', '<h2><span style="font-size: 14pt;">Create a new password</span></h2>\r\nForgot your password?<br /> To create a new password, just follow this link:<br /> <br /><a href="{reset_password_url}">Reset Password</a><br /> <br /> You received this email, because it was requested by a <strong>{companyname}</strong>&nbsp;user. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same. <br /><br /> {email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(45, 'staff', 'staff-password-reseted', 'english', 'Password Reset - Confirmation', 'Your password has been changed', '<span style="font-size: 14pt;"><strong>You have changed your password.<br /></strong></span><br /> Please, keep it in your records so you don\'t forget it.<br /> <br /> Your email address for login is: {staff_email}<br /><br /> If this wasnt you, please contact us.<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(46, 'project', 'assigned-to-project', 'english', 'New Project Created (Sent to Customer Contacts)', 'New Project Created', '<p>Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}</p>\r\n<p>New project is assigned to your company.<br /><br /><strong>Project Name:</strong>&nbsp;{project_name}<br /><strong>Project Start Date:</strong>&nbsp;{project_start_date}</p>\r\n<p>You can view the project on the following link:&nbsp;<a href="{project_link}">{project_name}</a></p>\r\n<p>We are looking forward hearing from you.<br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(47, 'tasks', 'task-added-attachment-to-contacts', 'english', 'New Attachment(s) on Task (Sent to Customer Contacts)', 'New Attachment on Task - {task_name}', '<span>Hi {contact_firstname} {contact_lastname}</span><br /><br /><strong>{task_user_take_action}</strong><span> added an attachment on the following task:</span><br /><br /><strong>Name:</strong><span> {task_name}</span><br /><br /><span>You can view the task on the following link: </span><a href="{task_link}">{task_name}</a><br /><br /><span>Kind Regards,</span><br /><span>{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(48, 'tasks', 'task-commented-to-contacts', 'english', 'New Comment on Task (Sent to Customer Contacts)', 'New Comment on Task - {task_name}', '<span>Dear {contact_firstname} {contact_lastname}</span><br /><br /><span>A comment has been made on the following task:</span><br /><br /><strong>Name:</strong><span> {task_name}</span><br /><strong>Comment:</strong><span> {task_comment}</span><br /><br /><span>You can view the task on the following link: </span><a href="{task_link}">{task_name}</a><br /><br /><span>Kind Regards,</span><br /><span>{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(49, 'leads', 'new-lead-assigned', 'english', 'New Lead Assigned to Staff Member', 'New lead assigned to you', '<p>Hello {lead_assigned}<br /><br />New lead is assigned to you.<br /><br /><strong>Deal Name:</strong> {deal_name}<br /><strong>Contact Name:</strong>&nbsp;{lead_name}<br /><strong>Contact Email:</strong>&nbsp;{lead_email}<br /><br />You can view the lead on the following link: <a href="{lead_link}">{lead_name}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(50, 'client', 'client-statement', 'english', 'Statement - Account Summary', 'Account Statement from {statement_from} to {statement_to}', 'Dear {contact_firstname} {contact_lastname}, <br /><br />Its been a great experience working with you.<br /><br />Attached with this email is a list of all transactions for the period between {statement_from} to {statement_to}<br /><br />For your information your account balance due is total:&nbsp;{statement_balance_due}<br /><br />Please contact us if you need more information.<br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(51, 'ticket', 'ticket-assigned-to-admin', 'english', 'New Ticket Assigned (Sent to Staff)', 'New support ticket has been assigned to you', '<p><span style="font-size: 12pt;">Hi</span></p>\r\n<p><span style="font-size: 12pt;">A new support ticket&nbsp;has been assigned to you.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {ticket_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(52, 'client', 'new-client-registered-to-admin', 'english', 'New Customer Registration (Sent to admins)', 'New Customer Registration', 'Hello.<br /><br />New customer registration on your customer portal:<br /><br /><strong>Firstname:</strong>&nbsp;{contact_firstname}<br /><strong>Lastname:</strong>&nbsp;{contact_lastname}<br /><strong>Company:</strong>&nbsp;{client_company}<br /><strong>Email:</strong>&nbsp;{contact_email}<br /><br />Best Regards', '{companyname} | CRM', '', 0, 1, 0),
	(53, 'leads', 'new-web-to-lead-form-submitted', 'english', 'Web to lead form submitted - Sent to lead', '{lead_name} - We Received Your Request', 'Hello {lead_name}.<br /><br /><strong>Your request has been received.</strong><br /><br />This email is to let you know that we received your request and we will get back to you as soon as possible with more information.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 0, 0),
	(54, 'staff', 'two-factor-authentication', 'english', 'Two Factor Authentication', 'Confirm Your Login', '<p>Hi {staff_firstname}</p>\r\n<p style="text-align: left;">You received this email because you have enabled two factor authentication in your account.<br />Use the following code to confirm your login:</p>\r\n<p style="text-align: left;"><span style="font-size: 18pt;"><strong>{two_factor_auth_code}<br /><br /></strong><span style="font-size: 12pt;">{email_signature}</span><strong><br /><br /><br /><br /></strong></span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(55, 'project', 'project-finished-to-customer', 'english', 'Project Marked as Finished (Sent to Customer Contacts)', 'Project Marked as Finished', '<p>Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}</p>\r\n<p>You are receiving this email because project&nbsp;<strong>{project_name}</strong> has been marked as finished. This project is assigned under your company and we just wanted to keep you up to date.<br /><br />You can view the project on the following link:&nbsp;<a href="{project_link}">{project_name}</a></p>\r\n<p>If you have any questions don\'t hesitate to contact us.<br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(56, 'credit_note', 'credit-note-send-to-client', 'english', 'Send Credit Note To Email', 'Credit Note With Number #{credit_note_number} Created', 'Dear&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br />We have attached the credit note with number <strong>#{credit_note_number} </strong>for your reference.<br /><br /><strong>Date:</strong>&nbsp;{credit_note_date}<br /><strong>Total Amount:</strong>&nbsp;{credit_note_total}<br /><br /><span style="font-size: 12pt;">Please contact us for more information.</span><br /> <br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(57, 'tasks', 'task-status-change-to-staff', 'english', 'Task Status Changed (Sent to Staff)', 'Task Status Changed', '<span style="font-size: 12pt;">Hi {staff_firstname}</span><br /><br /><span style="font-size: 12pt;"><strong>{task_user_take_action}</strong> marked task as <strong>{task_status}</strong></span><br /><br /><span style="font-size: 12pt;"><strong>Name:</strong> {task_name}</span><br /><span style="font-size: 12pt;"><strong>Due date:</strong> {task_duedate}</span><br /><br /><span style="font-size: 12pt;">You can view the task on the following link: <a href="{task_link}">{task_name}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(58, 'tasks', 'task-status-change-to-contacts', 'english', 'Task Status Changed (Sent to Customer Contacts)', 'Task Status Changed', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;"><strong>{task_user_take_action}</strong> marked task as <strong>{task_status}</strong></span><br /><br /><span style="font-size: 12pt;"><strong>Name:</strong> {task_name}</span><br /><span style="font-size: 12pt;"><strong>Due date:</strong> {task_duedate}</span><br /><br /><span style="font-size: 12pt;">You can view the task on the following link: <a href="{task_link}">{task_name}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(59, 'staff', 'reminder-email-staff', 'english', 'Staff Reminder Email', 'You Have a New Reminder!', '<p>Hello&nbsp;{staff_firstname}<br /><br /><strong>You have a new reminder&nbsp;linked to&nbsp;{staff_reminder_relation_name}!<br /><br />Reminder description:</strong><br />{staff_reminder_description}<br /><br />Click <a href="{staff_reminder_relation_link}">here</a> to view&nbsp;<a href="{staff_reminder_relation_link}">{staff_reminder_relation_name}</a><br /><br />Best Regards<br /><br /></p>', '{companyname} | CRM', '', 0, 1, 0),
	(60, 'contract', 'contract-comment-to-client', 'english', 'New Comment  (Sent to Customer Contacts)', 'New Contract Comment', 'Dear {contact_firstname} {contact_lastname}<br /> <br />A new comment has been made on the following contract: <strong>{contract_subject}</strong><br /> <br />You can view and reply to the comment on the following link: <a href="{contract_link}">{contract_subject}</a><br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(61, 'contract', 'contract-comment-to-admin', 'english', 'New Comment (Sent to Staff) ', 'New Contract Comment', 'Hi {staff_firstname}<br /><br />A new comment has been made to the contract&nbsp;<strong>{contract_subject}</strong><br /><br />You can view and reply to the comment on the following link: <a href="{contract_link}">{contract_subject}</a>&nbsp;or from the admin area.<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(62, 'subscriptions', 'send-subscription', 'english', 'Send Subscription to Customer', 'Subscription Created', 'Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br />We have prepared the subscription&nbsp;<strong>{subscription_name}</strong> for your company.<br /><br />Click <a href="{subscription_link}">here</a> to review the subscription and subscribe.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(63, 'subscriptions', 'subscription-payment-failed', 'english', 'Subscription Payment Failed', 'Your most recent invoice payment failed', 'Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br br="" />Unfortunately, your most recent invoice payment for&nbsp;<strong>{subscription_name}</strong> was declined.<br /><br />This could be due to a change in your card number, your card expiring,<br />cancellation of your credit card, or the card issuer not recognizing the<br />payment and therefore taking action to prevent it.<br /><br />Please update your payment information as soon as possible by logging in here:<br /><a href="{crm_url}/login">{crm_url}/login</a><br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(64, 'subscriptions', 'subscription-canceled', 'english', 'Subscription Canceled (Sent to customer primary contact)', 'Your subscription has been canceled', 'Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br />Your subscription&nbsp;<strong>{subscription_name} </strong>has been canceled, if you have any questions don\'t hesitate to contact us.<br /><br />It was a pleasure doing business with you.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(65, 'subscriptions', 'subscription-payment-succeeded', 'english', 'Subscription Payment Succeeded (Sent to customer primary contact)', 'Subscription  Payment Receipt - {subscription_name}', 'Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br />This email is to let you know that we received your payment for subscription&nbsp;<strong>{subscription_name}&nbsp;</strong>of&nbsp;<strong><span>{payment_total}<br /><br /></span></strong>The invoice associated with it is now with status&nbsp;<strong>{invoice_status}<br /></strong><br />Thank you for your confidence.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(66, 'contract', 'contract-expiration-to-staff', 'english', 'Contract Expiration Reminder (Sent to Staff)', 'Contract Expiration Reminder', 'Hi {staff_firstname}<br /><br /><span style="font-size: 12pt;">This is a reminder that the following contract will expire soon:</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {contract_subject}</span><br /><span style="font-size: 12pt;"><strong>Description:</strong> {contract_description}</span><br /><span style="font-size: 12pt;"><strong>Date Start:</strong> {contract_datestart}</span><br /><span style="font-size: 12pt;"><strong>Date End:</strong> {contract_dateend}</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(67, 'gdpr', 'gdpr-removal-request', 'english', 'Removal Request From Contact (Sent to administrators)', 'Data Removal Request Received', 'Hello&nbsp;{staff_firstname}&nbsp;{staff_lastname}<br /><br />Data removal has been requested by&nbsp;{contact_firstname} {contact_lastname}<br /><br />You can review this request and take proper actions directly from the admin area.', '{companyname} | CRM', '', 0, 1, 0),
	(68, 'gdpr', 'gdpr-removal-request-lead', 'english', 'Removal Request From Lead (Sent to administrators)', 'Data Removal Request Received', 'Hello&nbsp;{staff_firstname}&nbsp;{staff_lastname}<br /><br />Data removal has been requested by {lead_name}<br /><br />You can review this request and take proper actions directly from the admin area.<br /><br />To view the lead inside the admin area click here:&nbsp;<a href="{lead_link}">{lead_link}</a>', '{companyname} | CRM', '', 0, 1, 0),
	(69, 'client', 'client-registration-confirmed', 'english', 'Customer Registration Confirmed', 'Your registration is confirmed', '<p>Dear {contact_firstname} {contact_lastname}<br /><br />We just wanted to let you know that your registration at&nbsp;{companyname} is successfully confirmed and your account is now active.<br /><br />You can login at&nbsp;<a href="{crm_url}">{crm_url}</a> with the email and password you provided during registration.<br /><br />Please contact us if you need any help.<br /><br />Kind Regards, <br />{email_signature}</p>\r\n<p><br />(This is an automated email, so please don\'t reply to this email address)</p>', '{companyname} | CRM', '', 0, 1, 0),
	(70, 'contract', 'contract-signed-to-staff', 'english', 'Contract Signed (Sent to Staff)', 'Customer Signed a Contract', 'Hi {staff_firstname}<br /><br />A contract with subject&nbsp;<strong>{contract_subject} </strong>has been successfully signed by the customer.<br /><br />You can view the contract at the following link: <a href="{contract_link}">{contract_subject}</a>&nbsp;or from the admin area.<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(71, 'subscriptions', 'customer-subscribed-to-staff', 'english', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator)', 'Customer Subscribed to a Subscription', 'The customer <strong>{client_company}</strong> subscribed to a subscription with name&nbsp;<strong>{subscription_name}</strong><br /><br /><strong>ID</strong>:&nbsp;{subscription_id}<br /><strong>Subscription name</strong>:&nbsp;{subscription_name}<br /><strong>Subscription description</strong>:&nbsp;{subscription_description}<br /><br />You can view the subscription by clicking <a href="{subscription_link}">here</a><br />\r\n<div style="text-align: center;"><span style="font-size: 10pt;">&nbsp;</span></div>\r\nBest Regards,<br />{email_signature}<br /><br /><span style="font-size: 10pt;"><span style="color: #999999;">You are receiving this email because you are either administrator or you are creator of the subscription.</span></span>', '{companyname} | CRM', '', 0, 1, 0),
	(72, 'client', 'contact-verification-email', 'english', 'Email Verification (Sent to Contact After Registration)', 'Verify Email Address', '<p>Hello&nbsp;{contact_firstname}<br /><br />Please click the button below to verify your email address.<br /><br /><a href="{email_verification_url}">Verify Email Address</a><br /><br />If you did not create an account, no further action is required</p>\r\n<p><br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(73, 'client', 'new-customer-profile-file-uploaded-to-staff', 'english', 'New Customer Profile File(s) Uploaded (Sent to Staff)', 'Customer Uploaded New File(s) in Profile', 'Hi!<br /><br />New file(s) is uploaded into the customer ({client_company}) profile by&nbsp;{contact_firstname}<br /><br />You can check the uploaded files into the admin area by clicking <a href="{customer_profile_files_admin_link}">here</a> or at the following link:&nbsp;{customer_profile_files_admin_link}<br /><br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(74, 'staff', 'event-notification-to-staff', 'english', 'Event Notification (Calendar)', 'Upcoming Event - {event_title}', 'Hi {staff_firstname}! <br /><br />This is a reminder for event <a href=\\"{event_link}\\">{event_title}</a> scheduled at {event_start_date}. <br /><br />Regards.', '', '', 0, 1, 0),
	(75, 'subscriptions', 'subscription-payment-requires-action', 'english', 'Credit Card Authorization Required - SCA', 'Important: Confirm your subscription {subscription_name} payment', '<p>Hello {contact_firstname}</p>\r\n<p><strong>Your bank sometimes requires an additional step to make sure an online transaction was authorized.</strong><br /><br />Because of European regulation to protect consumers, many online payments now require two-factor authentication. Your bank ultimately decides when authentication is required to confirm a payment, but you may notice this step when you start paying for a service or when the cost changes.<br /><br />In order to pay the subscription <strong>{subscription_name}</strong>, you will need to&nbsp;confirm your payment by clicking on the follow link: <strong><a href="{subscription_authorize_payment_link}">{subscription_authorize_payment_link}</a></strong><br /><br />To view the subscription, please click at the following link: <a href="{subscription_link}"><span>{subscription_link}</span></a><br />or you can login in our dedicated area here: <a href="{crm_url}/login">{crm_url}/login</a> in case you want to update your credit card or view the subscriptions you are subscribed.<br /><br />Best Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(76, 'client', 'new-client-created', 'slovak', 'New Contact Added/Registered (Welcome Email) [slovak]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(77, 'invoice', 'invoice-send-to-client', 'slovak', 'Send Invoice to Customer [slovak]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(78, 'ticket', 'new-ticket-opened-admin', 'slovak', 'New Ticket Opened (Opened by Staff, Sent to Customer) [slovak]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(79, 'ticket', 'ticket-reply', 'slovak', 'Ticket Reply (Sent to Customer) [slovak]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(80, 'ticket', 'ticket-autoresponse', 'slovak', 'New Ticket Opened - Autoresponse [slovak]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(81, 'invoice', 'invoice-payment-recorded', 'slovak', 'Invoice Payment Recorded (Sent to Customer) [slovak]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(82, 'invoice', 'invoice-overdue-notice', 'slovak', 'Invoice Overdue Notice [slovak]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(83, 'invoice', 'invoice-already-send', 'slovak', 'Invoice Already Sent to Customer [slovak]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(84, 'ticket', 'new-ticket-created-staff', 'slovak', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [slovak]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(85, 'estimate', 'estimate-send-to-client', 'slovak', 'Send Estimate to Customer [slovak]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(86, 'ticket', 'ticket-reply-to-admin', 'slovak', 'Ticket Reply (Sent to Staff) [slovak]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(87, 'estimate', 'estimate-already-send', 'slovak', 'Estimate Already Sent to Customer [slovak]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(88, 'contract', 'contract-expiration', 'slovak', 'Contract Expiration Reminder (Sent to Customer Contacts) [slovak]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(89, 'tasks', 'task-assigned', 'slovak', 'New Task Assigned (Sent to Staff) [slovak]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(90, 'tasks', 'task-added-as-follower', 'slovak', 'Staff Member Added as Follower on Task (Sent to Staff) [slovak]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(91, 'tasks', 'task-commented', 'slovak', 'New Comment on Task (Sent to Staff) [slovak]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(92, 'tasks', 'task-added-attachment', 'slovak', 'New Attachment(s) on Task (Sent to Staff) [slovak]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(93, 'estimate', 'estimate-declined-to-staff', 'slovak', 'Estimate Declined (Sent to Staff) [slovak]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(94, 'estimate', 'estimate-accepted-to-staff', 'slovak', 'Estimate Accepted (Sent to Staff) [slovak]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(95, 'proposals', 'proposal-client-accepted', 'slovak', 'Customer Action - Accepted (Sent to Staff) [slovak]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(96, 'proposals', 'proposal-send-to-customer', 'slovak', 'Send Proposal to Customer [slovak]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(97, 'proposals', 'proposal-client-declined', 'slovak', 'Customer Action - Declined (Sent to Staff) [slovak]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(98, 'proposals', 'proposal-client-thank-you', 'slovak', 'Thank You Email (Sent to Customer After Accept) [slovak]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(99, 'proposals', 'proposal-comment-to-client', 'slovak', 'New Comment  (Sent to Customer/Lead) [slovak]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(100, 'proposals', 'proposal-comment-to-admin', 'slovak', 'New Comment (Sent to Staff)  [slovak]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(101, 'estimate', 'estimate-thank-you-to-customer', 'slovak', 'Thank You Email (Sent to Customer After Accept) [slovak]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(102, 'tasks', 'task-deadline-notification', 'slovak', 'Task Deadline Reminder - Sent to Assigned Members [slovak]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(103, 'contract', 'send-contract', 'slovak', 'Send Contract to Customer [slovak]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(104, 'invoice', 'invoice-payment-recorded-to-staff', 'slovak', 'Invoice Payment Recorded (Sent to Staff) [slovak]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(105, 'ticket', 'auto-close-ticket', 'slovak', 'Auto Close Ticket [slovak]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(106, 'project', 'new-project-discussion-created-to-staff', 'slovak', 'New Project Discussion (Sent to Project Members) [slovak]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(107, 'project', 'new-project-discussion-created-to-customer', 'slovak', 'New Project Discussion (Sent to Customer Contacts) [slovak]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(108, 'project', 'new-project-file-uploaded-to-customer', 'slovak', 'New Project File(s) Uploaded (Sent to Customer Contacts) [slovak]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(109, 'project', 'new-project-file-uploaded-to-staff', 'slovak', 'New Project File(s) Uploaded (Sent to Project Members) [slovak]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(110, 'project', 'new-project-discussion-comment-to-customer', 'slovak', 'New Discussion Comment  (Sent to Customer Contacts) [slovak]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(111, 'project', 'new-project-discussion-comment-to-staff', 'slovak', 'New Discussion Comment (Sent to Project Members) [slovak]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(112, 'project', 'staff-added-as-project-member', 'slovak', 'Staff Added as Project Member [slovak]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(113, 'estimate', 'estimate-expiry-reminder', 'slovak', 'Estimate Expiration Reminder [slovak]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(114, 'proposals', 'proposal-expiry-reminder', 'slovak', 'Proposal Expiration Reminder [slovak]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(115, 'staff', 'new-staff-created', 'slovak', 'New Staff Created (Welcome Email) [slovak]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(116, 'client', 'contact-forgot-password', 'slovak', 'Forgot Password [slovak]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(117, 'client', 'contact-password-reseted', 'slovak', 'Password Reset - Confirmation [slovak]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(118, 'client', 'contact-set-password', 'slovak', 'Set New Password [slovak]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(119, 'staff', 'staff-forgot-password', 'slovak', 'Forgot Password [slovak]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(120, 'staff', 'staff-password-reseted', 'slovak', 'Password Reset - Confirmation [slovak]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(121, 'project', 'assigned-to-project', 'slovak', 'New Project Created (Sent to Customer Contacts) [slovak]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(122, 'tasks', 'task-added-attachment-to-contacts', 'slovak', 'New Attachment(s) on Task (Sent to Customer Contacts) [slovak]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(123, 'tasks', 'task-commented-to-contacts', 'slovak', 'New Comment on Task (Sent to Customer Contacts) [slovak]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(124, 'leads', 'new-lead-assigned', 'slovak', 'New Lead Assigned to Staff Member [slovak]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(125, 'client', 'client-statement', 'slovak', 'Statement - Account Summary [slovak]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(126, 'ticket', 'ticket-assigned-to-admin', 'slovak', 'New Ticket Assigned (Sent to Staff) [slovak]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(127, 'client', 'new-client-registered-to-admin', 'slovak', 'New Customer Registration (Sent to admins) [slovak]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(128, 'leads', 'new-web-to-lead-form-submitted', 'slovak', 'Web to lead form submitted - Sent to lead [slovak]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(129, 'staff', 'two-factor-authentication', 'slovak', 'Two Factor Authentication [slovak]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(130, 'project', 'project-finished-to-customer', 'slovak', 'Project Marked as Finished (Sent to Customer Contacts) [slovak]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(131, 'credit_note', 'credit-note-send-to-client', 'slovak', 'Send Credit Note To Email [slovak]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(132, 'tasks', 'task-status-change-to-staff', 'slovak', 'Task Status Changed (Sent to Staff) [slovak]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(133, 'tasks', 'task-status-change-to-contacts', 'slovak', 'Task Status Changed (Sent to Customer Contacts) [slovak]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(134, 'staff', 'reminder-email-staff', 'slovak', 'Staff Reminder Email [slovak]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(135, 'contract', 'contract-comment-to-client', 'slovak', 'New Comment  (Sent to Customer Contacts) [slovak]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(136, 'contract', 'contract-comment-to-admin', 'slovak', 'New Comment (Sent to Staff)  [slovak]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(137, 'subscriptions', 'send-subscription', 'slovak', 'Send Subscription to Customer [slovak]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(138, 'subscriptions', 'subscription-payment-failed', 'slovak', 'Subscription Payment Failed [slovak]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(139, 'subscriptions', 'subscription-canceled', 'slovak', 'Subscription Canceled (Sent to customer primary contact) [slovak]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(140, 'subscriptions', 'subscription-payment-succeeded', 'slovak', 'Subscription Payment Succeeded (Sent to customer primary contact) [slovak]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(141, 'contract', 'contract-expiration-to-staff', 'slovak', 'Contract Expiration Reminder (Sent to Staff) [slovak]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(142, 'gdpr', 'gdpr-removal-request', 'slovak', 'Removal Request From Contact (Sent to administrators) [slovak]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(143, 'gdpr', 'gdpr-removal-request-lead', 'slovak', 'Removal Request From Lead (Sent to administrators) [slovak]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(144, 'client', 'client-registration-confirmed', 'slovak', 'Customer Registration Confirmed [slovak]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(145, 'contract', 'contract-signed-to-staff', 'slovak', 'Contract Signed (Sent to Staff) [slovak]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(146, 'subscriptions', 'customer-subscribed-to-staff', 'slovak', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [slovak]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(147, 'client', 'contact-verification-email', 'slovak', 'Email Verification (Sent to Contact After Registration) [slovak]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(148, 'client', 'new-customer-profile-file-uploaded-to-staff', 'slovak', 'New Customer Profile File(s) Uploaded (Sent to Staff) [slovak]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(149, 'staff', 'event-notification-to-staff', 'slovak', 'Event Notification (Calendar) [slovak]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(150, 'subscriptions', 'subscription-payment-requires-action', 'slovak', 'Credit Card Authorization Required - SCA [slovak]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(151, 'client', 'new-client-created', 'czech', 'New Contact Added/Registered (Welcome Email) [czech]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(152, 'invoice', 'invoice-send-to-client', 'czech', 'Send Invoice to Customer [czech]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(153, 'ticket', 'new-ticket-opened-admin', 'czech', 'New Ticket Opened (Opened by Staff, Sent to Customer) [czech]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(154, 'ticket', 'ticket-reply', 'czech', 'Ticket Reply (Sent to Customer) [czech]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(155, 'ticket', 'ticket-autoresponse', 'czech', 'New Ticket Opened - Autoresponse [czech]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(156, 'invoice', 'invoice-payment-recorded', 'czech', 'Invoice Payment Recorded (Sent to Customer) [czech]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(157, 'invoice', 'invoice-overdue-notice', 'czech', 'Invoice Overdue Notice [czech]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(158, 'invoice', 'invoice-already-send', 'czech', 'Invoice Already Sent to Customer [czech]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(159, 'ticket', 'new-ticket-created-staff', 'czech', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [czech]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(160, 'estimate', 'estimate-send-to-client', 'czech', 'Send Estimate to Customer [czech]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(161, 'ticket', 'ticket-reply-to-admin', 'czech', 'Ticket Reply (Sent to Staff) [czech]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(162, 'estimate', 'estimate-already-send', 'czech', 'Estimate Already Sent to Customer [czech]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(163, 'contract', 'contract-expiration', 'czech', 'Contract Expiration Reminder (Sent to Customer Contacts) [czech]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(164, 'tasks', 'task-assigned', 'czech', 'New Task Assigned (Sent to Staff) [czech]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(165, 'tasks', 'task-added-as-follower', 'czech', 'Staff Member Added as Follower on Task (Sent to Staff) [czech]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(166, 'tasks', 'task-commented', 'czech', 'New Comment on Task (Sent to Staff) [czech]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(167, 'tasks', 'task-added-attachment', 'czech', 'New Attachment(s) on Task (Sent to Staff) [czech]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(168, 'estimate', 'estimate-declined-to-staff', 'czech', 'Estimate Declined (Sent to Staff) [czech]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(169, 'estimate', 'estimate-accepted-to-staff', 'czech', 'Estimate Accepted (Sent to Staff) [czech]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(170, 'proposals', 'proposal-client-accepted', 'czech', 'Customer Action - Accepted (Sent to Staff) [czech]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(171, 'proposals', 'proposal-send-to-customer', 'czech', 'Send Proposal to Customer [czech]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(172, 'proposals', 'proposal-client-declined', 'czech', 'Customer Action - Declined (Sent to Staff) [czech]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(173, 'proposals', 'proposal-client-thank-you', 'czech', 'Thank You Email (Sent to Customer After Accept) [czech]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(174, 'proposals', 'proposal-comment-to-client', 'czech', 'New Comment  (Sent to Customer/Lead) [czech]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(175, 'proposals', 'proposal-comment-to-admin', 'czech', 'New Comment (Sent to Staff)  [czech]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(176, 'estimate', 'estimate-thank-you-to-customer', 'czech', 'Thank You Email (Sent to Customer After Accept) [czech]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(177, 'tasks', 'task-deadline-notification', 'czech', 'Task Deadline Reminder - Sent to Assigned Members [czech]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(178, 'contract', 'send-contract', 'czech', 'Send Contract to Customer [czech]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(179, 'invoice', 'invoice-payment-recorded-to-staff', 'czech', 'Invoice Payment Recorded (Sent to Staff) [czech]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(180, 'ticket', 'auto-close-ticket', 'czech', 'Auto Close Ticket [czech]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(181, 'project', 'new-project-discussion-created-to-staff', 'czech', 'New Project Discussion (Sent to Project Members) [czech]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(182, 'project', 'new-project-discussion-created-to-customer', 'czech', 'New Project Discussion (Sent to Customer Contacts) [czech]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(183, 'project', 'new-project-file-uploaded-to-customer', 'czech', 'New Project File(s) Uploaded (Sent to Customer Contacts) [czech]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(184, 'project', 'new-project-file-uploaded-to-staff', 'czech', 'New Project File(s) Uploaded (Sent to Project Members) [czech]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(185, 'project', 'new-project-discussion-comment-to-customer', 'czech', 'New Discussion Comment  (Sent to Customer Contacts) [czech]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(186, 'project', 'new-project-discussion-comment-to-staff', 'czech', 'New Discussion Comment (Sent to Project Members) [czech]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(187, 'project', 'staff-added-as-project-member', 'czech', 'Staff Added as Project Member [czech]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(188, 'estimate', 'estimate-expiry-reminder', 'czech', 'Estimate Expiration Reminder [czech]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(189, 'proposals', 'proposal-expiry-reminder', 'czech', 'Proposal Expiration Reminder [czech]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(190, 'staff', 'new-staff-created', 'czech', 'New Staff Created (Welcome Email) [czech]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(191, 'client', 'contact-forgot-password', 'czech', 'Forgot Password [czech]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(192, 'client', 'contact-password-reseted', 'czech', 'Password Reset - Confirmation [czech]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(193, 'client', 'contact-set-password', 'czech', 'Set New Password [czech]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(194, 'staff', 'staff-forgot-password', 'czech', 'Forgot Password [czech]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(195, 'staff', 'staff-password-reseted', 'czech', 'Password Reset - Confirmation [czech]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(196, 'project', 'assigned-to-project', 'czech', 'New Project Created (Sent to Customer Contacts) [czech]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(197, 'tasks', 'task-added-attachment-to-contacts', 'czech', 'New Attachment(s) on Task (Sent to Customer Contacts) [czech]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(198, 'tasks', 'task-commented-to-contacts', 'czech', 'New Comment on Task (Sent to Customer Contacts) [czech]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(199, 'leads', 'new-lead-assigned', 'czech', 'New Lead Assigned to Staff Member [czech]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(200, 'client', 'client-statement', 'czech', 'Statement - Account Summary [czech]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(201, 'ticket', 'ticket-assigned-to-admin', 'czech', 'New Ticket Assigned (Sent to Staff) [czech]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(202, 'client', 'new-client-registered-to-admin', 'czech', 'New Customer Registration (Sent to admins) [czech]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(203, 'leads', 'new-web-to-lead-form-submitted', 'czech', 'Web to lead form submitted - Sent to lead [czech]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(204, 'staff', 'two-factor-authentication', 'czech', 'Two Factor Authentication [czech]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(205, 'project', 'project-finished-to-customer', 'czech', 'Project Marked as Finished (Sent to Customer Contacts) [czech]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(206, 'credit_note', 'credit-note-send-to-client', 'czech', 'Send Credit Note To Email [czech]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(207, 'tasks', 'task-status-change-to-staff', 'czech', 'Task Status Changed (Sent to Staff) [czech]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(208, 'tasks', 'task-status-change-to-contacts', 'czech', 'Task Status Changed (Sent to Customer Contacts) [czech]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(209, 'staff', 'reminder-email-staff', 'czech', 'Staff Reminder Email [czech]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(210, 'contract', 'contract-comment-to-client', 'czech', 'New Comment  (Sent to Customer Contacts) [czech]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(211, 'contract', 'contract-comment-to-admin', 'czech', 'New Comment (Sent to Staff)  [czech]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(212, 'subscriptions', 'send-subscription', 'czech', 'Send Subscription to Customer [czech]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(213, 'subscriptions', 'subscription-payment-failed', 'czech', 'Subscription Payment Failed [czech]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(214, 'subscriptions', 'subscription-canceled', 'czech', 'Subscription Canceled (Sent to customer primary contact) [czech]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(215, 'subscriptions', 'subscription-payment-succeeded', 'czech', 'Subscription Payment Succeeded (Sent to customer primary contact) [czech]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(216, 'contract', 'contract-expiration-to-staff', 'czech', 'Contract Expiration Reminder (Sent to Staff) [czech]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(217, 'gdpr', 'gdpr-removal-request', 'czech', 'Removal Request From Contact (Sent to administrators) [czech]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(218, 'gdpr', 'gdpr-removal-request-lead', 'czech', 'Removal Request From Lead (Sent to administrators) [czech]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(219, 'client', 'client-registration-confirmed', 'czech', 'Customer Registration Confirmed [czech]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(220, 'contract', 'contract-signed-to-staff', 'czech', 'Contract Signed (Sent to Staff) [czech]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(221, 'subscriptions', 'customer-subscribed-to-staff', 'czech', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [czech]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(222, 'client', 'contact-verification-email', 'czech', 'Email Verification (Sent to Contact After Registration) [czech]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(223, 'client', 'new-customer-profile-file-uploaded-to-staff', 'czech', 'New Customer Profile File(s) Uploaded (Sent to Staff) [czech]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(224, 'staff', 'event-notification-to-staff', 'czech', 'Event Notification (Calendar) [czech]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(225, 'subscriptions', 'subscription-payment-requires-action', 'czech', 'Credit Card Authorization Required - SCA [czech]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(226, 'client', 'new-client-created', 'romanian', 'New Contact Added/Registered (Welcome Email) [romanian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(227, 'invoice', 'invoice-send-to-client', 'romanian', 'Send Invoice to Customer [romanian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(228, 'ticket', 'new-ticket-opened-admin', 'romanian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [romanian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(229, 'ticket', 'ticket-reply', 'romanian', 'Ticket Reply (Sent to Customer) [romanian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(230, 'ticket', 'ticket-autoresponse', 'romanian', 'New Ticket Opened - Autoresponse [romanian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(231, 'invoice', 'invoice-payment-recorded', 'romanian', 'Invoice Payment Recorded (Sent to Customer) [romanian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(232, 'invoice', 'invoice-overdue-notice', 'romanian', 'Invoice Overdue Notice [romanian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(233, 'invoice', 'invoice-already-send', 'romanian', 'Invoice Already Sent to Customer [romanian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(234, 'ticket', 'new-ticket-created-staff', 'romanian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [romanian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(235, 'estimate', 'estimate-send-to-client', 'romanian', 'Send Estimate to Customer [romanian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(236, 'ticket', 'ticket-reply-to-admin', 'romanian', 'Ticket Reply (Sent to Staff) [romanian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(237, 'estimate', 'estimate-already-send', 'romanian', 'Estimate Already Sent to Customer [romanian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(238, 'contract', 'contract-expiration', 'romanian', 'Contract Expiration Reminder (Sent to Customer Contacts) [romanian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(239, 'tasks', 'task-assigned', 'romanian', 'New Task Assigned (Sent to Staff) [romanian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(240, 'tasks', 'task-added-as-follower', 'romanian', 'Staff Member Added as Follower on Task (Sent to Staff) [romanian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(241, 'tasks', 'task-commented', 'romanian', 'New Comment on Task (Sent to Staff) [romanian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(242, 'tasks', 'task-added-attachment', 'romanian', 'New Attachment(s) on Task (Sent to Staff) [romanian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(243, 'estimate', 'estimate-declined-to-staff', 'romanian', 'Estimate Declined (Sent to Staff) [romanian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(244, 'estimate', 'estimate-accepted-to-staff', 'romanian', 'Estimate Accepted (Sent to Staff) [romanian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(245, 'proposals', 'proposal-client-accepted', 'romanian', 'Customer Action - Accepted (Sent to Staff) [romanian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(246, 'proposals', 'proposal-send-to-customer', 'romanian', 'Send Proposal to Customer [romanian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(247, 'proposals', 'proposal-client-declined', 'romanian', 'Customer Action - Declined (Sent to Staff) [romanian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(248, 'proposals', 'proposal-client-thank-you', 'romanian', 'Thank You Email (Sent to Customer After Accept) [romanian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(249, 'proposals', 'proposal-comment-to-client', 'romanian', 'New Comment  (Sent to Customer/Lead) [romanian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(250, 'proposals', 'proposal-comment-to-admin', 'romanian', 'New Comment (Sent to Staff)  [romanian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(251, 'estimate', 'estimate-thank-you-to-customer', 'romanian', 'Thank You Email (Sent to Customer After Accept) [romanian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(252, 'tasks', 'task-deadline-notification', 'romanian', 'Task Deadline Reminder - Sent to Assigned Members [romanian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(253, 'contract', 'send-contract', 'romanian', 'Send Contract to Customer [romanian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(254, 'invoice', 'invoice-payment-recorded-to-staff', 'romanian', 'Invoice Payment Recorded (Sent to Staff) [romanian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(255, 'ticket', 'auto-close-ticket', 'romanian', 'Auto Close Ticket [romanian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(256, 'project', 'new-project-discussion-created-to-staff', 'romanian', 'New Project Discussion (Sent to Project Members) [romanian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(257, 'project', 'new-project-discussion-created-to-customer', 'romanian', 'New Project Discussion (Sent to Customer Contacts) [romanian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(258, 'project', 'new-project-file-uploaded-to-customer', 'romanian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [romanian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(259, 'project', 'new-project-file-uploaded-to-staff', 'romanian', 'New Project File(s) Uploaded (Sent to Project Members) [romanian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(260, 'project', 'new-project-discussion-comment-to-customer', 'romanian', 'New Discussion Comment  (Sent to Customer Contacts) [romanian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(261, 'project', 'new-project-discussion-comment-to-staff', 'romanian', 'New Discussion Comment (Sent to Project Members) [romanian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(262, 'project', 'staff-added-as-project-member', 'romanian', 'Staff Added as Project Member [romanian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(263, 'estimate', 'estimate-expiry-reminder', 'romanian', 'Estimate Expiration Reminder [romanian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(264, 'proposals', 'proposal-expiry-reminder', 'romanian', 'Proposal Expiration Reminder [romanian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(265, 'staff', 'new-staff-created', 'romanian', 'New Staff Created (Welcome Email) [romanian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(266, 'client', 'contact-forgot-password', 'romanian', 'Forgot Password [romanian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(267, 'client', 'contact-password-reseted', 'romanian', 'Password Reset - Confirmation [romanian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(268, 'client', 'contact-set-password', 'romanian', 'Set New Password [romanian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(269, 'staff', 'staff-forgot-password', 'romanian', 'Forgot Password [romanian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(270, 'staff', 'staff-password-reseted', 'romanian', 'Password Reset - Confirmation [romanian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(271, 'project', 'assigned-to-project', 'romanian', 'New Project Created (Sent to Customer Contacts) [romanian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(272, 'tasks', 'task-added-attachment-to-contacts', 'romanian', 'New Attachment(s) on Task (Sent to Customer Contacts) [romanian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(273, 'tasks', 'task-commented-to-contacts', 'romanian', 'New Comment on Task (Sent to Customer Contacts) [romanian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(274, 'leads', 'new-lead-assigned', 'romanian', 'New Lead Assigned to Staff Member [romanian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(275, 'client', 'client-statement', 'romanian', 'Statement - Account Summary [romanian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(276, 'ticket', 'ticket-assigned-to-admin', 'romanian', 'New Ticket Assigned (Sent to Staff) [romanian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(277, 'client', 'new-client-registered-to-admin', 'romanian', 'New Customer Registration (Sent to admins) [romanian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(278, 'leads', 'new-web-to-lead-form-submitted', 'romanian', 'Web to lead form submitted - Sent to lead [romanian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(279, 'staff', 'two-factor-authentication', 'romanian', 'Two Factor Authentication [romanian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(280, 'project', 'project-finished-to-customer', 'romanian', 'Project Marked as Finished (Sent to Customer Contacts) [romanian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(281, 'credit_note', 'credit-note-send-to-client', 'romanian', 'Send Credit Note To Email [romanian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(282, 'tasks', 'task-status-change-to-staff', 'romanian', 'Task Status Changed (Sent to Staff) [romanian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(283, 'tasks', 'task-status-change-to-contacts', 'romanian', 'Task Status Changed (Sent to Customer Contacts) [romanian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(284, 'staff', 'reminder-email-staff', 'romanian', 'Staff Reminder Email [romanian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(285, 'contract', 'contract-comment-to-client', 'romanian', 'New Comment  (Sent to Customer Contacts) [romanian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(286, 'contract', 'contract-comment-to-admin', 'romanian', 'New Comment (Sent to Staff)  [romanian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(287, 'subscriptions', 'send-subscription', 'romanian', 'Send Subscription to Customer [romanian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(288, 'subscriptions', 'subscription-payment-failed', 'romanian', 'Subscription Payment Failed [romanian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(289, 'subscriptions', 'subscription-canceled', 'romanian', 'Subscription Canceled (Sent to customer primary contact) [romanian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(290, 'subscriptions', 'subscription-payment-succeeded', 'romanian', 'Subscription Payment Succeeded (Sent to customer primary contact) [romanian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(291, 'contract', 'contract-expiration-to-staff', 'romanian', 'Contract Expiration Reminder (Sent to Staff) [romanian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(292, 'gdpr', 'gdpr-removal-request', 'romanian', 'Removal Request From Contact (Sent to administrators) [romanian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(293, 'gdpr', 'gdpr-removal-request-lead', 'romanian', 'Removal Request From Lead (Sent to administrators) [romanian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(294, 'client', 'client-registration-confirmed', 'romanian', 'Customer Registration Confirmed [romanian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(295, 'contract', 'contract-signed-to-staff', 'romanian', 'Contract Signed (Sent to Staff) [romanian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(296, 'subscriptions', 'customer-subscribed-to-staff', 'romanian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [romanian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(297, 'client', 'contact-verification-email', 'romanian', 'Email Verification (Sent to Contact After Registration) [romanian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(298, 'client', 'new-customer-profile-file-uploaded-to-staff', 'romanian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [romanian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(299, 'staff', 'event-notification-to-staff', 'romanian', 'Event Notification (Calendar) [romanian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(300, 'subscriptions', 'subscription-payment-requires-action', 'romanian', 'Credit Card Authorization Required - SCA [romanian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(301, 'client', 'new-client-created', 'japanese', 'New Contact Added/Registered (Welcome Email) [japanese]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(302, 'invoice', 'invoice-send-to-client', 'japanese', 'Send Invoice to Customer [japanese]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(303, 'ticket', 'new-ticket-opened-admin', 'japanese', 'New Ticket Opened (Opened by Staff, Sent to Customer) [japanese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(304, 'ticket', 'ticket-reply', 'japanese', 'Ticket Reply (Sent to Customer) [japanese]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(305, 'ticket', 'ticket-autoresponse', 'japanese', 'New Ticket Opened - Autoresponse [japanese]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(306, 'invoice', 'invoice-payment-recorded', 'japanese', 'Invoice Payment Recorded (Sent to Customer) [japanese]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(307, 'invoice', 'invoice-overdue-notice', 'japanese', 'Invoice Overdue Notice [japanese]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(308, 'invoice', 'invoice-already-send', 'japanese', 'Invoice Already Sent to Customer [japanese]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(309, 'ticket', 'new-ticket-created-staff', 'japanese', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [japanese]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(310, 'estimate', 'estimate-send-to-client', 'japanese', 'Send Estimate to Customer [japanese]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(311, 'ticket', 'ticket-reply-to-admin', 'japanese', 'Ticket Reply (Sent to Staff) [japanese]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(312, 'estimate', 'estimate-already-send', 'japanese', 'Estimate Already Sent to Customer [japanese]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(313, 'contract', 'contract-expiration', 'japanese', 'Contract Expiration Reminder (Sent to Customer Contacts) [japanese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(314, 'tasks', 'task-assigned', 'japanese', 'New Task Assigned (Sent to Staff) [japanese]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(315, 'tasks', 'task-added-as-follower', 'japanese', 'Staff Member Added as Follower on Task (Sent to Staff) [japanese]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(316, 'tasks', 'task-commented', 'japanese', 'New Comment on Task (Sent to Staff) [japanese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(317, 'tasks', 'task-added-attachment', 'japanese', 'New Attachment(s) on Task (Sent to Staff) [japanese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(318, 'estimate', 'estimate-declined-to-staff', 'japanese', 'Estimate Declined (Sent to Staff) [japanese]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(319, 'estimate', 'estimate-accepted-to-staff', 'japanese', 'Estimate Accepted (Sent to Staff) [japanese]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(320, 'proposals', 'proposal-client-accepted', 'japanese', 'Customer Action - Accepted (Sent to Staff) [japanese]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(321, 'proposals', 'proposal-send-to-customer', 'japanese', 'Send Proposal to Customer [japanese]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(322, 'proposals', 'proposal-client-declined', 'japanese', 'Customer Action - Declined (Sent to Staff) [japanese]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(323, 'proposals', 'proposal-client-thank-you', 'japanese', 'Thank You Email (Sent to Customer After Accept) [japanese]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(324, 'proposals', 'proposal-comment-to-client', 'japanese', 'New Comment  (Sent to Customer/Lead) [japanese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(325, 'proposals', 'proposal-comment-to-admin', 'japanese', 'New Comment (Sent to Staff)  [japanese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(326, 'estimate', 'estimate-thank-you-to-customer', 'japanese', 'Thank You Email (Sent to Customer After Accept) [japanese]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(327, 'tasks', 'task-deadline-notification', 'japanese', 'Task Deadline Reminder - Sent to Assigned Members [japanese]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(328, 'contract', 'send-contract', 'japanese', 'Send Contract to Customer [japanese]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(329, 'invoice', 'invoice-payment-recorded-to-staff', 'japanese', 'Invoice Payment Recorded (Sent to Staff) [japanese]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(330, 'ticket', 'auto-close-ticket', 'japanese', 'Auto Close Ticket [japanese]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(331, 'project', 'new-project-discussion-created-to-staff', 'japanese', 'New Project Discussion (Sent to Project Members) [japanese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(332, 'project', 'new-project-discussion-created-to-customer', 'japanese', 'New Project Discussion (Sent to Customer Contacts) [japanese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(333, 'project', 'new-project-file-uploaded-to-customer', 'japanese', 'New Project File(s) Uploaded (Sent to Customer Contacts) [japanese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(334, 'project', 'new-project-file-uploaded-to-staff', 'japanese', 'New Project File(s) Uploaded (Sent to Project Members) [japanese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(335, 'project', 'new-project-discussion-comment-to-customer', 'japanese', 'New Discussion Comment  (Sent to Customer Contacts) [japanese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(336, 'project', 'new-project-discussion-comment-to-staff', 'japanese', 'New Discussion Comment (Sent to Project Members) [japanese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(337, 'project', 'staff-added-as-project-member', 'japanese', 'Staff Added as Project Member [japanese]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(338, 'estimate', 'estimate-expiry-reminder', 'japanese', 'Estimate Expiration Reminder [japanese]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(339, 'proposals', 'proposal-expiry-reminder', 'japanese', 'Proposal Expiration Reminder [japanese]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(340, 'staff', 'new-staff-created', 'japanese', 'New Staff Created (Welcome Email) [japanese]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(341, 'client', 'contact-forgot-password', 'japanese', 'Forgot Password [japanese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(342, 'client', 'contact-password-reseted', 'japanese', 'Password Reset - Confirmation [japanese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(343, 'client', 'contact-set-password', 'japanese', 'Set New Password [japanese]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(344, 'staff', 'staff-forgot-password', 'japanese', 'Forgot Password [japanese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(345, 'staff', 'staff-password-reseted', 'japanese', 'Password Reset - Confirmation [japanese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(346, 'project', 'assigned-to-project', 'japanese', 'New Project Created (Sent to Customer Contacts) [japanese]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(347, 'tasks', 'task-added-attachment-to-contacts', 'japanese', 'New Attachment(s) on Task (Sent to Customer Contacts) [japanese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(348, 'tasks', 'task-commented-to-contacts', 'japanese', 'New Comment on Task (Sent to Customer Contacts) [japanese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(349, 'leads', 'new-lead-assigned', 'japanese', 'New Lead Assigned to Staff Member [japanese]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(350, 'client', 'client-statement', 'japanese', 'Statement - Account Summary [japanese]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(351, 'ticket', 'ticket-assigned-to-admin', 'japanese', 'New Ticket Assigned (Sent to Staff) [japanese]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(352, 'client', 'new-client-registered-to-admin', 'japanese', 'New Customer Registration (Sent to admins) [japanese]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(353, 'leads', 'new-web-to-lead-form-submitted', 'japanese', 'Web to lead form submitted - Sent to lead [japanese]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(354, 'staff', 'two-factor-authentication', 'japanese', 'Two Factor Authentication [japanese]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(355, 'project', 'project-finished-to-customer', 'japanese', 'Project Marked as Finished (Sent to Customer Contacts) [japanese]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(356, 'credit_note', 'credit-note-send-to-client', 'japanese', 'Send Credit Note To Email [japanese]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(357, 'tasks', 'task-status-change-to-staff', 'japanese', 'Task Status Changed (Sent to Staff) [japanese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(358, 'tasks', 'task-status-change-to-contacts', 'japanese', 'Task Status Changed (Sent to Customer Contacts) [japanese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(359, 'staff', 'reminder-email-staff', 'japanese', 'Staff Reminder Email [japanese]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(360, 'contract', 'contract-comment-to-client', 'japanese', 'New Comment  (Sent to Customer Contacts) [japanese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(361, 'contract', 'contract-comment-to-admin', 'japanese', 'New Comment (Sent to Staff)  [japanese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(362, 'subscriptions', 'send-subscription', 'japanese', 'Send Subscription to Customer [japanese]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(363, 'subscriptions', 'subscription-payment-failed', 'japanese', 'Subscription Payment Failed [japanese]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(364, 'subscriptions', 'subscription-canceled', 'japanese', 'Subscription Canceled (Sent to customer primary contact) [japanese]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(365, 'subscriptions', 'subscription-payment-succeeded', 'japanese', 'Subscription Payment Succeeded (Sent to customer primary contact) [japanese]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(366, 'contract', 'contract-expiration-to-staff', 'japanese', 'Contract Expiration Reminder (Sent to Staff) [japanese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(367, 'gdpr', 'gdpr-removal-request', 'japanese', 'Removal Request From Contact (Sent to administrators) [japanese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(368, 'gdpr', 'gdpr-removal-request-lead', 'japanese', 'Removal Request From Lead (Sent to administrators) [japanese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(369, 'client', 'client-registration-confirmed', 'japanese', 'Customer Registration Confirmed [japanese]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(370, 'contract', 'contract-signed-to-staff', 'japanese', 'Contract Signed (Sent to Staff) [japanese]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(371, 'subscriptions', 'customer-subscribed-to-staff', 'japanese', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [japanese]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(372, 'client', 'contact-verification-email', 'japanese', 'Email Verification (Sent to Contact After Registration) [japanese]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(373, 'client', 'new-customer-profile-file-uploaded-to-staff', 'japanese', 'New Customer Profile File(s) Uploaded (Sent to Staff) [japanese]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(374, 'staff', 'event-notification-to-staff', 'japanese', 'Event Notification (Calendar) [japanese]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(375, 'subscriptions', 'subscription-payment-requires-action', 'japanese', 'Credit Card Authorization Required - SCA [japanese]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(376, 'client', 'new-client-created', 'indonesia', 'New Contact Added/Registered (Welcome Email) [indonesia]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(377, 'invoice', 'invoice-send-to-client', 'indonesia', 'Send Invoice to Customer [indonesia]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(378, 'ticket', 'new-ticket-opened-admin', 'indonesia', 'New Ticket Opened (Opened by Staff, Sent to Customer) [indonesia]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(379, 'ticket', 'ticket-reply', 'indonesia', 'Ticket Reply (Sent to Customer) [indonesia]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(380, 'ticket', 'ticket-autoresponse', 'indonesia', 'New Ticket Opened - Autoresponse [indonesia]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(381, 'invoice', 'invoice-payment-recorded', 'indonesia', 'Invoice Payment Recorded (Sent to Customer) [indonesia]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(382, 'invoice', 'invoice-overdue-notice', 'indonesia', 'Invoice Overdue Notice [indonesia]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(383, 'invoice', 'invoice-already-send', 'indonesia', 'Invoice Already Sent to Customer [indonesia]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(384, 'ticket', 'new-ticket-created-staff', 'indonesia', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [indonesia]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(385, 'estimate', 'estimate-send-to-client', 'indonesia', 'Send Estimate to Customer [indonesia]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(386, 'ticket', 'ticket-reply-to-admin', 'indonesia', 'Ticket Reply (Sent to Staff) [indonesia]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(387, 'estimate', 'estimate-already-send', 'indonesia', 'Estimate Already Sent to Customer [indonesia]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(388, 'contract', 'contract-expiration', 'indonesia', 'Contract Expiration Reminder (Sent to Customer Contacts) [indonesia]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(389, 'tasks', 'task-assigned', 'indonesia', 'New Task Assigned (Sent to Staff) [indonesia]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(390, 'tasks', 'task-added-as-follower', 'indonesia', 'Staff Member Added as Follower on Task (Sent to Staff) [indonesia]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(391, 'tasks', 'task-commented', 'indonesia', 'New Comment on Task (Sent to Staff) [indonesia]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(392, 'tasks', 'task-added-attachment', 'indonesia', 'New Attachment(s) on Task (Sent to Staff) [indonesia]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(393, 'estimate', 'estimate-declined-to-staff', 'indonesia', 'Estimate Declined (Sent to Staff) [indonesia]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(394, 'estimate', 'estimate-accepted-to-staff', 'indonesia', 'Estimate Accepted (Sent to Staff) [indonesia]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(395, 'proposals', 'proposal-client-accepted', 'indonesia', 'Customer Action - Accepted (Sent to Staff) [indonesia]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(396, 'proposals', 'proposal-send-to-customer', 'indonesia', 'Send Proposal to Customer [indonesia]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(397, 'proposals', 'proposal-client-declined', 'indonesia', 'Customer Action - Declined (Sent to Staff) [indonesia]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(398, 'proposals', 'proposal-client-thank-you', 'indonesia', 'Thank You Email (Sent to Customer After Accept) [indonesia]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(399, 'proposals', 'proposal-comment-to-client', 'indonesia', 'New Comment  (Sent to Customer/Lead) [indonesia]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(400, 'proposals', 'proposal-comment-to-admin', 'indonesia', 'New Comment (Sent to Staff)  [indonesia]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(401, 'estimate', 'estimate-thank-you-to-customer', 'indonesia', 'Thank You Email (Sent to Customer After Accept) [indonesia]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(402, 'tasks', 'task-deadline-notification', 'indonesia', 'Task Deadline Reminder - Sent to Assigned Members [indonesia]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(403, 'contract', 'send-contract', 'indonesia', 'Send Contract to Customer [indonesia]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(404, 'invoice', 'invoice-payment-recorded-to-staff', 'indonesia', 'Invoice Payment Recorded (Sent to Staff) [indonesia]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(405, 'ticket', 'auto-close-ticket', 'indonesia', 'Auto Close Ticket [indonesia]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(406, 'project', 'new-project-discussion-created-to-staff', 'indonesia', 'New Project Discussion (Sent to Project Members) [indonesia]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(407, 'project', 'new-project-discussion-created-to-customer', 'indonesia', 'New Project Discussion (Sent to Customer Contacts) [indonesia]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(408, 'project', 'new-project-file-uploaded-to-customer', 'indonesia', 'New Project File(s) Uploaded (Sent to Customer Contacts) [indonesia]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(409, 'project', 'new-project-file-uploaded-to-staff', 'indonesia', 'New Project File(s) Uploaded (Sent to Project Members) [indonesia]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(410, 'project', 'new-project-discussion-comment-to-customer', 'indonesia', 'New Discussion Comment  (Sent to Customer Contacts) [indonesia]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(411, 'project', 'new-project-discussion-comment-to-staff', 'indonesia', 'New Discussion Comment (Sent to Project Members) [indonesia]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(412, 'project', 'staff-added-as-project-member', 'indonesia', 'Staff Added as Project Member [indonesia]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(413, 'estimate', 'estimate-expiry-reminder', 'indonesia', 'Estimate Expiration Reminder [indonesia]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(414, 'proposals', 'proposal-expiry-reminder', 'indonesia', 'Proposal Expiration Reminder [indonesia]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(415, 'staff', 'new-staff-created', 'indonesia', 'New Staff Created (Welcome Email) [indonesia]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(416, 'client', 'contact-forgot-password', 'indonesia', 'Forgot Password [indonesia]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(417, 'client', 'contact-password-reseted', 'indonesia', 'Password Reset - Confirmation [indonesia]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(418, 'client', 'contact-set-password', 'indonesia', 'Set New Password [indonesia]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(419, 'staff', 'staff-forgot-password', 'indonesia', 'Forgot Password [indonesia]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(420, 'staff', 'staff-password-reseted', 'indonesia', 'Password Reset - Confirmation [indonesia]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(421, 'project', 'assigned-to-project', 'indonesia', 'New Project Created (Sent to Customer Contacts) [indonesia]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(422, 'tasks', 'task-added-attachment-to-contacts', 'indonesia', 'New Attachment(s) on Task (Sent to Customer Contacts) [indonesia]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(423, 'tasks', 'task-commented-to-contacts', 'indonesia', 'New Comment on Task (Sent to Customer Contacts) [indonesia]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(424, 'leads', 'new-lead-assigned', 'indonesia', 'New Lead Assigned to Staff Member [indonesia]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(425, 'client', 'client-statement', 'indonesia', 'Statement - Account Summary [indonesia]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(426, 'ticket', 'ticket-assigned-to-admin', 'indonesia', 'New Ticket Assigned (Sent to Staff) [indonesia]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(427, 'client', 'new-client-registered-to-admin', 'indonesia', 'New Customer Registration (Sent to admins) [indonesia]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(428, 'leads', 'new-web-to-lead-form-submitted', 'indonesia', 'Web to lead form submitted - Sent to lead [indonesia]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(429, 'staff', 'two-factor-authentication', 'indonesia', 'Two Factor Authentication [indonesia]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(430, 'project', 'project-finished-to-customer', 'indonesia', 'Project Marked as Finished (Sent to Customer Contacts) [indonesia]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(431, 'credit_note', 'credit-note-send-to-client', 'indonesia', 'Send Credit Note To Email [indonesia]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(432, 'tasks', 'task-status-change-to-staff', 'indonesia', 'Task Status Changed (Sent to Staff) [indonesia]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(433, 'tasks', 'task-status-change-to-contacts', 'indonesia', 'Task Status Changed (Sent to Customer Contacts) [indonesia]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(434, 'staff', 'reminder-email-staff', 'indonesia', 'Staff Reminder Email [indonesia]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(435, 'contract', 'contract-comment-to-client', 'indonesia', 'New Comment  (Sent to Customer Contacts) [indonesia]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(436, 'contract', 'contract-comment-to-admin', 'indonesia', 'New Comment (Sent to Staff)  [indonesia]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(437, 'subscriptions', 'send-subscription', 'indonesia', 'Send Subscription to Customer [indonesia]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(438, 'subscriptions', 'subscription-payment-failed', 'indonesia', 'Subscription Payment Failed [indonesia]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(439, 'subscriptions', 'subscription-canceled', 'indonesia', 'Subscription Canceled (Sent to customer primary contact) [indonesia]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(440, 'subscriptions', 'subscription-payment-succeeded', 'indonesia', 'Subscription Payment Succeeded (Sent to customer primary contact) [indonesia]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(441, 'contract', 'contract-expiration-to-staff', 'indonesia', 'Contract Expiration Reminder (Sent to Staff) [indonesia]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(442, 'gdpr', 'gdpr-removal-request', 'indonesia', 'Removal Request From Contact (Sent to administrators) [indonesia]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(443, 'gdpr', 'gdpr-removal-request-lead', 'indonesia', 'Removal Request From Lead (Sent to administrators) [indonesia]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(444, 'client', 'client-registration-confirmed', 'indonesia', 'Customer Registration Confirmed [indonesia]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(445, 'contract', 'contract-signed-to-staff', 'indonesia', 'Contract Signed (Sent to Staff) [indonesia]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(446, 'subscriptions', 'customer-subscribed-to-staff', 'indonesia', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [indonesia]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(447, 'client', 'contact-verification-email', 'indonesia', 'Email Verification (Sent to Contact After Registration) [indonesia]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(448, 'client', 'new-customer-profile-file-uploaded-to-staff', 'indonesia', 'New Customer Profile File(s) Uploaded (Sent to Staff) [indonesia]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(449, 'staff', 'event-notification-to-staff', 'indonesia', 'Event Notification (Calendar) [indonesia]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(450, 'subscriptions', 'subscription-payment-requires-action', 'indonesia', 'Credit Card Authorization Required - SCA [indonesia]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(451, 'client', 'new-client-created', 'bulgarian', 'New Contact Added/Registered (Welcome Email) [bulgarian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(452, 'invoice', 'invoice-send-to-client', 'bulgarian', 'Send Invoice to Customer [bulgarian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(453, 'ticket', 'new-ticket-opened-admin', 'bulgarian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [bulgarian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(454, 'ticket', 'ticket-reply', 'bulgarian', 'Ticket Reply (Sent to Customer) [bulgarian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(455, 'ticket', 'ticket-autoresponse', 'bulgarian', 'New Ticket Opened - Autoresponse [bulgarian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(456, 'invoice', 'invoice-payment-recorded', 'bulgarian', 'Invoice Payment Recorded (Sent to Customer) [bulgarian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(457, 'invoice', 'invoice-overdue-notice', 'bulgarian', 'Invoice Overdue Notice [bulgarian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(458, 'invoice', 'invoice-already-send', 'bulgarian', 'Invoice Already Sent to Customer [bulgarian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(459, 'ticket', 'new-ticket-created-staff', 'bulgarian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [bulgarian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(460, 'estimate', 'estimate-send-to-client', 'bulgarian', 'Send Estimate to Customer [bulgarian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(461, 'ticket', 'ticket-reply-to-admin', 'bulgarian', 'Ticket Reply (Sent to Staff) [bulgarian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(462, 'estimate', 'estimate-already-send', 'bulgarian', 'Estimate Already Sent to Customer [bulgarian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(463, 'contract', 'contract-expiration', 'bulgarian', 'Contract Expiration Reminder (Sent to Customer Contacts) [bulgarian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(464, 'tasks', 'task-assigned', 'bulgarian', 'New Task Assigned (Sent to Staff) [bulgarian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(465, 'tasks', 'task-added-as-follower', 'bulgarian', 'Staff Member Added as Follower on Task (Sent to Staff) [bulgarian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(466, 'tasks', 'task-commented', 'bulgarian', 'New Comment on Task (Sent to Staff) [bulgarian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(467, 'tasks', 'task-added-attachment', 'bulgarian', 'New Attachment(s) on Task (Sent to Staff) [bulgarian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(468, 'estimate', 'estimate-declined-to-staff', 'bulgarian', 'Estimate Declined (Sent to Staff) [bulgarian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(469, 'estimate', 'estimate-accepted-to-staff', 'bulgarian', 'Estimate Accepted (Sent to Staff) [bulgarian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(470, 'proposals', 'proposal-client-accepted', 'bulgarian', 'Customer Action - Accepted (Sent to Staff) [bulgarian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(471, 'proposals', 'proposal-send-to-customer', 'bulgarian', 'Send Proposal to Customer [bulgarian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(472, 'proposals', 'proposal-client-declined', 'bulgarian', 'Customer Action - Declined (Sent to Staff) [bulgarian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(473, 'proposals', 'proposal-client-thank-you', 'bulgarian', 'Thank You Email (Sent to Customer After Accept) [bulgarian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(474, 'proposals', 'proposal-comment-to-client', 'bulgarian', 'New Comment  (Sent to Customer/Lead) [bulgarian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(475, 'proposals', 'proposal-comment-to-admin', 'bulgarian', 'New Comment (Sent to Staff)  [bulgarian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(476, 'estimate', 'estimate-thank-you-to-customer', 'bulgarian', 'Thank You Email (Sent to Customer After Accept) [bulgarian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(477, 'tasks', 'task-deadline-notification', 'bulgarian', 'Task Deadline Reminder - Sent to Assigned Members [bulgarian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(478, 'contract', 'send-contract', 'bulgarian', 'Send Contract to Customer [bulgarian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(479, 'invoice', 'invoice-payment-recorded-to-staff', 'bulgarian', 'Invoice Payment Recorded (Sent to Staff) [bulgarian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(480, 'ticket', 'auto-close-ticket', 'bulgarian', 'Auto Close Ticket [bulgarian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(481, 'project', 'new-project-discussion-created-to-staff', 'bulgarian', 'New Project Discussion (Sent to Project Members) [bulgarian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(482, 'project', 'new-project-discussion-created-to-customer', 'bulgarian', 'New Project Discussion (Sent to Customer Contacts) [bulgarian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(483, 'project', 'new-project-file-uploaded-to-customer', 'bulgarian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [bulgarian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(484, 'project', 'new-project-file-uploaded-to-staff', 'bulgarian', 'New Project File(s) Uploaded (Sent to Project Members) [bulgarian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(485, 'project', 'new-project-discussion-comment-to-customer', 'bulgarian', 'New Discussion Comment  (Sent to Customer Contacts) [bulgarian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(486, 'project', 'new-project-discussion-comment-to-staff', 'bulgarian', 'New Discussion Comment (Sent to Project Members) [bulgarian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(487, 'project', 'staff-added-as-project-member', 'bulgarian', 'Staff Added as Project Member [bulgarian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(488, 'estimate', 'estimate-expiry-reminder', 'bulgarian', 'Estimate Expiration Reminder [bulgarian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(489, 'proposals', 'proposal-expiry-reminder', 'bulgarian', 'Proposal Expiration Reminder [bulgarian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(490, 'staff', 'new-staff-created', 'bulgarian', 'New Staff Created (Welcome Email) [bulgarian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(491, 'client', 'contact-forgot-password', 'bulgarian', 'Forgot Password [bulgarian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(492, 'client', 'contact-password-reseted', 'bulgarian', 'Password Reset - Confirmation [bulgarian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(493, 'client', 'contact-set-password', 'bulgarian', 'Set New Password [bulgarian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(494, 'staff', 'staff-forgot-password', 'bulgarian', 'Forgot Password [bulgarian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(495, 'staff', 'staff-password-reseted', 'bulgarian', 'Password Reset - Confirmation [bulgarian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(496, 'project', 'assigned-to-project', 'bulgarian', 'New Project Created (Sent to Customer Contacts) [bulgarian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(497, 'tasks', 'task-added-attachment-to-contacts', 'bulgarian', 'New Attachment(s) on Task (Sent to Customer Contacts) [bulgarian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(498, 'tasks', 'task-commented-to-contacts', 'bulgarian', 'New Comment on Task (Sent to Customer Contacts) [bulgarian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(499, 'leads', 'new-lead-assigned', 'bulgarian', 'New Lead Assigned to Staff Member [bulgarian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(500, 'client', 'client-statement', 'bulgarian', 'Statement - Account Summary [bulgarian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(501, 'ticket', 'ticket-assigned-to-admin', 'bulgarian', 'New Ticket Assigned (Sent to Staff) [bulgarian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(502, 'client', 'new-client-registered-to-admin', 'bulgarian', 'New Customer Registration (Sent to admins) [bulgarian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(503, 'leads', 'new-web-to-lead-form-submitted', 'bulgarian', 'Web to lead form submitted - Sent to lead [bulgarian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(504, 'staff', 'two-factor-authentication', 'bulgarian', 'Two Factor Authentication [bulgarian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(505, 'project', 'project-finished-to-customer', 'bulgarian', 'Project Marked as Finished (Sent to Customer Contacts) [bulgarian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(506, 'credit_note', 'credit-note-send-to-client', 'bulgarian', 'Send Credit Note To Email [bulgarian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(507, 'tasks', 'task-status-change-to-staff', 'bulgarian', 'Task Status Changed (Sent to Staff) [bulgarian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(508, 'tasks', 'task-status-change-to-contacts', 'bulgarian', 'Task Status Changed (Sent to Customer Contacts) [bulgarian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(509, 'staff', 'reminder-email-staff', 'bulgarian', 'Staff Reminder Email [bulgarian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(510, 'contract', 'contract-comment-to-client', 'bulgarian', 'New Comment  (Sent to Customer Contacts) [bulgarian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(511, 'contract', 'contract-comment-to-admin', 'bulgarian', 'New Comment (Sent to Staff)  [bulgarian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(512, 'subscriptions', 'send-subscription', 'bulgarian', 'Send Subscription to Customer [bulgarian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(513, 'subscriptions', 'subscription-payment-failed', 'bulgarian', 'Subscription Payment Failed [bulgarian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(514, 'subscriptions', 'subscription-canceled', 'bulgarian', 'Subscription Canceled (Sent to customer primary contact) [bulgarian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(515, 'subscriptions', 'subscription-payment-succeeded', 'bulgarian', 'Subscription Payment Succeeded (Sent to customer primary contact) [bulgarian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(516, 'contract', 'contract-expiration-to-staff', 'bulgarian', 'Contract Expiration Reminder (Sent to Staff) [bulgarian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(517, 'gdpr', 'gdpr-removal-request', 'bulgarian', 'Removal Request From Contact (Sent to administrators) [bulgarian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(518, 'gdpr', 'gdpr-removal-request-lead', 'bulgarian', 'Removal Request From Lead (Sent to administrators) [bulgarian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(519, 'client', 'client-registration-confirmed', 'bulgarian', 'Customer Registration Confirmed [bulgarian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(520, 'contract', 'contract-signed-to-staff', 'bulgarian', 'Contract Signed (Sent to Staff) [bulgarian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(521, 'subscriptions', 'customer-subscribed-to-staff', 'bulgarian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [bulgarian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(522, 'client', 'contact-verification-email', 'bulgarian', 'Email Verification (Sent to Contact After Registration) [bulgarian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(523, 'client', 'new-customer-profile-file-uploaded-to-staff', 'bulgarian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [bulgarian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(524, 'staff', 'event-notification-to-staff', 'bulgarian', 'Event Notification (Calendar) [bulgarian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(525, 'subscriptions', 'subscription-payment-requires-action', 'bulgarian', 'Credit Card Authorization Required - SCA [bulgarian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(526, 'client', 'new-client-created', 'vietnamese', 'New Contact Added/Registered (Welcome Email) [vietnamese]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(527, 'invoice', 'invoice-send-to-client', 'vietnamese', 'Send Invoice to Customer [vietnamese]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(528, 'ticket', 'new-ticket-opened-admin', 'vietnamese', 'New Ticket Opened (Opened by Staff, Sent to Customer) [vietnamese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(529, 'ticket', 'ticket-reply', 'vietnamese', 'Ticket Reply (Sent to Customer) [vietnamese]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(530, 'ticket', 'ticket-autoresponse', 'vietnamese', 'New Ticket Opened - Autoresponse [vietnamese]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(531, 'invoice', 'invoice-payment-recorded', 'vietnamese', 'Invoice Payment Recorded (Sent to Customer) [vietnamese]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(532, 'invoice', 'invoice-overdue-notice', 'vietnamese', 'Invoice Overdue Notice [vietnamese]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(533, 'invoice', 'invoice-already-send', 'vietnamese', 'Invoice Already Sent to Customer [vietnamese]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(534, 'ticket', 'new-ticket-created-staff', 'vietnamese', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [vietnamese]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(535, 'estimate', 'estimate-send-to-client', 'vietnamese', 'Send Estimate to Customer [vietnamese]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(536, 'ticket', 'ticket-reply-to-admin', 'vietnamese', 'Ticket Reply (Sent to Staff) [vietnamese]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(537, 'estimate', 'estimate-already-send', 'vietnamese', 'Estimate Already Sent to Customer [vietnamese]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(538, 'contract', 'contract-expiration', 'vietnamese', 'Contract Expiration Reminder (Sent to Customer Contacts) [vietnamese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(539, 'tasks', 'task-assigned', 'vietnamese', 'New Task Assigned (Sent to Staff) [vietnamese]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(540, 'tasks', 'task-added-as-follower', 'vietnamese', 'Staff Member Added as Follower on Task (Sent to Staff) [vietnamese]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(541, 'tasks', 'task-commented', 'vietnamese', 'New Comment on Task (Sent to Staff) [vietnamese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(542, 'tasks', 'task-added-attachment', 'vietnamese', 'New Attachment(s) on Task (Sent to Staff) [vietnamese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(543, 'estimate', 'estimate-declined-to-staff', 'vietnamese', 'Estimate Declined (Sent to Staff) [vietnamese]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(544, 'estimate', 'estimate-accepted-to-staff', 'vietnamese', 'Estimate Accepted (Sent to Staff) [vietnamese]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(545, 'proposals', 'proposal-client-accepted', 'vietnamese', 'Customer Action - Accepted (Sent to Staff) [vietnamese]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(546, 'proposals', 'proposal-send-to-customer', 'vietnamese', 'Send Proposal to Customer [vietnamese]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(547, 'proposals', 'proposal-client-declined', 'vietnamese', 'Customer Action - Declined (Sent to Staff) [vietnamese]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(548, 'proposals', 'proposal-client-thank-you', 'vietnamese', 'Thank You Email (Sent to Customer After Accept) [vietnamese]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(549, 'proposals', 'proposal-comment-to-client', 'vietnamese', 'New Comment  (Sent to Customer/Lead) [vietnamese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(550, 'proposals', 'proposal-comment-to-admin', 'vietnamese', 'New Comment (Sent to Staff)  [vietnamese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(551, 'estimate', 'estimate-thank-you-to-customer', 'vietnamese', 'Thank You Email (Sent to Customer After Accept) [vietnamese]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(552, 'tasks', 'task-deadline-notification', 'vietnamese', 'Task Deadline Reminder - Sent to Assigned Members [vietnamese]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(553, 'contract', 'send-contract', 'vietnamese', 'Send Contract to Customer [vietnamese]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(554, 'invoice', 'invoice-payment-recorded-to-staff', 'vietnamese', 'Invoice Payment Recorded (Sent to Staff) [vietnamese]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(555, 'ticket', 'auto-close-ticket', 'vietnamese', 'Auto Close Ticket [vietnamese]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(556, 'project', 'new-project-discussion-created-to-staff', 'vietnamese', 'New Project Discussion (Sent to Project Members) [vietnamese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(557, 'project', 'new-project-discussion-created-to-customer', 'vietnamese', 'New Project Discussion (Sent to Customer Contacts) [vietnamese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(558, 'project', 'new-project-file-uploaded-to-customer', 'vietnamese', 'New Project File(s) Uploaded (Sent to Customer Contacts) [vietnamese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(559, 'project', 'new-project-file-uploaded-to-staff', 'vietnamese', 'New Project File(s) Uploaded (Sent to Project Members) [vietnamese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(560, 'project', 'new-project-discussion-comment-to-customer', 'vietnamese', 'New Discussion Comment  (Sent to Customer Contacts) [vietnamese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(561, 'project', 'new-project-discussion-comment-to-staff', 'vietnamese', 'New Discussion Comment (Sent to Project Members) [vietnamese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(562, 'project', 'staff-added-as-project-member', 'vietnamese', 'Staff Added as Project Member [vietnamese]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(563, 'estimate', 'estimate-expiry-reminder', 'vietnamese', 'Estimate Expiration Reminder [vietnamese]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(564, 'proposals', 'proposal-expiry-reminder', 'vietnamese', 'Proposal Expiration Reminder [vietnamese]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(565, 'staff', 'new-staff-created', 'vietnamese', 'New Staff Created (Welcome Email) [vietnamese]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(566, 'client', 'contact-forgot-password', 'vietnamese', 'Forgot Password [vietnamese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(567, 'client', 'contact-password-reseted', 'vietnamese', 'Password Reset - Confirmation [vietnamese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(568, 'client', 'contact-set-password', 'vietnamese', 'Set New Password [vietnamese]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(569, 'staff', 'staff-forgot-password', 'vietnamese', 'Forgot Password [vietnamese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(570, 'staff', 'staff-password-reseted', 'vietnamese', 'Password Reset - Confirmation [vietnamese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(571, 'project', 'assigned-to-project', 'vietnamese', 'New Project Created (Sent to Customer Contacts) [vietnamese]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(572, 'tasks', 'task-added-attachment-to-contacts', 'vietnamese', 'New Attachment(s) on Task (Sent to Customer Contacts) [vietnamese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(573, 'tasks', 'task-commented-to-contacts', 'vietnamese', 'New Comment on Task (Sent to Customer Contacts) [vietnamese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(574, 'leads', 'new-lead-assigned', 'vietnamese', 'New Lead Assigned to Staff Member [vietnamese]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(575, 'client', 'client-statement', 'vietnamese', 'Statement - Account Summary [vietnamese]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(576, 'ticket', 'ticket-assigned-to-admin', 'vietnamese', 'New Ticket Assigned (Sent to Staff) [vietnamese]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(577, 'client', 'new-client-registered-to-admin', 'vietnamese', 'New Customer Registration (Sent to admins) [vietnamese]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(578, 'leads', 'new-web-to-lead-form-submitted', 'vietnamese', 'Web to lead form submitted - Sent to lead [vietnamese]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(579, 'staff', 'two-factor-authentication', 'vietnamese', 'Two Factor Authentication [vietnamese]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(580, 'project', 'project-finished-to-customer', 'vietnamese', 'Project Marked as Finished (Sent to Customer Contacts) [vietnamese]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(581, 'credit_note', 'credit-note-send-to-client', 'vietnamese', 'Send Credit Note To Email [vietnamese]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(582, 'tasks', 'task-status-change-to-staff', 'vietnamese', 'Task Status Changed (Sent to Staff) [vietnamese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(583, 'tasks', 'task-status-change-to-contacts', 'vietnamese', 'Task Status Changed (Sent to Customer Contacts) [vietnamese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(584, 'staff', 'reminder-email-staff', 'vietnamese', 'Staff Reminder Email [vietnamese]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(585, 'contract', 'contract-comment-to-client', 'vietnamese', 'New Comment  (Sent to Customer Contacts) [vietnamese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(586, 'contract', 'contract-comment-to-admin', 'vietnamese', 'New Comment (Sent to Staff)  [vietnamese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(587, 'subscriptions', 'send-subscription', 'vietnamese', 'Send Subscription to Customer [vietnamese]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(588, 'subscriptions', 'subscription-payment-failed', 'vietnamese', 'Subscription Payment Failed [vietnamese]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(589, 'subscriptions', 'subscription-canceled', 'vietnamese', 'Subscription Canceled (Sent to customer primary contact) [vietnamese]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(590, 'subscriptions', 'subscription-payment-succeeded', 'vietnamese', 'Subscription Payment Succeeded (Sent to customer primary contact) [vietnamese]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(591, 'contract', 'contract-expiration-to-staff', 'vietnamese', 'Contract Expiration Reminder (Sent to Staff) [vietnamese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(592, 'gdpr', 'gdpr-removal-request', 'vietnamese', 'Removal Request From Contact (Sent to administrators) [vietnamese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(593, 'gdpr', 'gdpr-removal-request-lead', 'vietnamese', 'Removal Request From Lead (Sent to administrators) [vietnamese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(594, 'client', 'client-registration-confirmed', 'vietnamese', 'Customer Registration Confirmed [vietnamese]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(595, 'contract', 'contract-signed-to-staff', 'vietnamese', 'Contract Signed (Sent to Staff) [vietnamese]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(596, 'subscriptions', 'customer-subscribed-to-staff', 'vietnamese', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [vietnamese]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(597, 'client', 'contact-verification-email', 'vietnamese', 'Email Verification (Sent to Contact After Registration) [vietnamese]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(598, 'client', 'new-customer-profile-file-uploaded-to-staff', 'vietnamese', 'New Customer Profile File(s) Uploaded (Sent to Staff) [vietnamese]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(599, 'staff', 'event-notification-to-staff', 'vietnamese', 'Event Notification (Calendar) [vietnamese]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(600, 'subscriptions', 'subscription-payment-requires-action', 'vietnamese', 'Credit Card Authorization Required - SCA [vietnamese]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(601, 'client', 'new-client-created', 'german', 'New Contact Added/Registered (Welcome Email) [german]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(602, 'invoice', 'invoice-send-to-client', 'german', 'Send Invoice to Customer [german]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(603, 'ticket', 'new-ticket-opened-admin', 'german', 'New Ticket Opened (Opened by Staff, Sent to Customer) [german]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(604, 'ticket', 'ticket-reply', 'german', 'Ticket Reply (Sent to Customer) [german]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(605, 'ticket', 'ticket-autoresponse', 'german', 'New Ticket Opened - Autoresponse [german]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(606, 'invoice', 'invoice-payment-recorded', 'german', 'Invoice Payment Recorded (Sent to Customer) [german]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(607, 'invoice', 'invoice-overdue-notice', 'german', 'Invoice Overdue Notice [german]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(608, 'invoice', 'invoice-already-send', 'german', 'Invoice Already Sent to Customer [german]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(609, 'ticket', 'new-ticket-created-staff', 'german', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [german]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(610, 'estimate', 'estimate-send-to-client', 'german', 'Send Estimate to Customer [german]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(611, 'ticket', 'ticket-reply-to-admin', 'german', 'Ticket Reply (Sent to Staff) [german]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(612, 'estimate', 'estimate-already-send', 'german', 'Estimate Already Sent to Customer [german]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(613, 'contract', 'contract-expiration', 'german', 'Contract Expiration Reminder (Sent to Customer Contacts) [german]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(614, 'tasks', 'task-assigned', 'german', 'New Task Assigned (Sent to Staff) [german]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(615, 'tasks', 'task-added-as-follower', 'german', 'Staff Member Added as Follower on Task (Sent to Staff) [german]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(616, 'tasks', 'task-commented', 'german', 'New Comment on Task (Sent to Staff) [german]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(617, 'tasks', 'task-added-attachment', 'german', 'New Attachment(s) on Task (Sent to Staff) [german]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(618, 'estimate', 'estimate-declined-to-staff', 'german', 'Estimate Declined (Sent to Staff) [german]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(619, 'estimate', 'estimate-accepted-to-staff', 'german', 'Estimate Accepted (Sent to Staff) [german]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(620, 'proposals', 'proposal-client-accepted', 'german', 'Customer Action - Accepted (Sent to Staff) [german]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(621, 'proposals', 'proposal-send-to-customer', 'german', 'Send Proposal to Customer [german]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(622, 'proposals', 'proposal-client-declined', 'german', 'Customer Action - Declined (Sent to Staff) [german]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(623, 'proposals', 'proposal-client-thank-you', 'german', 'Thank You Email (Sent to Customer After Accept) [german]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(624, 'proposals', 'proposal-comment-to-client', 'german', 'New Comment  (Sent to Customer/Lead) [german]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(625, 'proposals', 'proposal-comment-to-admin', 'german', 'New Comment (Sent to Staff)  [german]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(626, 'estimate', 'estimate-thank-you-to-customer', 'german', 'Thank You Email (Sent to Customer After Accept) [german]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(627, 'tasks', 'task-deadline-notification', 'german', 'Task Deadline Reminder - Sent to Assigned Members [german]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(628, 'contract', 'send-contract', 'german', 'Send Contract to Customer [german]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(629, 'invoice', 'invoice-payment-recorded-to-staff', 'german', 'Invoice Payment Recorded (Sent to Staff) [german]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(630, 'ticket', 'auto-close-ticket', 'german', 'Auto Close Ticket [german]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(631, 'project', 'new-project-discussion-created-to-staff', 'german', 'New Project Discussion (Sent to Project Members) [german]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(632, 'project', 'new-project-discussion-created-to-customer', 'german', 'New Project Discussion (Sent to Customer Contacts) [german]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(633, 'project', 'new-project-file-uploaded-to-customer', 'german', 'New Project File(s) Uploaded (Sent to Customer Contacts) [german]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(634, 'project', 'new-project-file-uploaded-to-staff', 'german', 'New Project File(s) Uploaded (Sent to Project Members) [german]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(635, 'project', 'new-project-discussion-comment-to-customer', 'german', 'New Discussion Comment  (Sent to Customer Contacts) [german]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(636, 'project', 'new-project-discussion-comment-to-staff', 'german', 'New Discussion Comment (Sent to Project Members) [german]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(637, 'project', 'staff-added-as-project-member', 'german', 'Staff Added as Project Member [german]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(638, 'estimate', 'estimate-expiry-reminder', 'german', 'Estimate Expiration Reminder [german]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(639, 'proposals', 'proposal-expiry-reminder', 'german', 'Proposal Expiration Reminder [german]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(640, 'staff', 'new-staff-created', 'german', 'New Staff Created (Welcome Email) [german]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(641, 'client', 'contact-forgot-password', 'german', 'Forgot Password [german]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(642, 'client', 'contact-password-reseted', 'german', 'Password Reset - Confirmation [german]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(643, 'client', 'contact-set-password', 'german', 'Set New Password [german]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(644, 'staff', 'staff-forgot-password', 'german', 'Forgot Password [german]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(645, 'staff', 'staff-password-reseted', 'german', 'Password Reset - Confirmation [german]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(646, 'project', 'assigned-to-project', 'german', 'New Project Created (Sent to Customer Contacts) [german]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(647, 'tasks', 'task-added-attachment-to-contacts', 'german', 'New Attachment(s) on Task (Sent to Customer Contacts) [german]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(648, 'tasks', 'task-commented-to-contacts', 'german', 'New Comment on Task (Sent to Customer Contacts) [german]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(649, 'leads', 'new-lead-assigned', 'german', 'New Lead Assigned to Staff Member [german]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(650, 'client', 'client-statement', 'german', 'Statement - Account Summary [german]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(651, 'ticket', 'ticket-assigned-to-admin', 'german', 'New Ticket Assigned (Sent to Staff) [german]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(652, 'client', 'new-client-registered-to-admin', 'german', 'New Customer Registration (Sent to admins) [german]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(653, 'leads', 'new-web-to-lead-form-submitted', 'german', 'Web to lead form submitted - Sent to lead [german]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(654, 'staff', 'two-factor-authentication', 'german', 'Two Factor Authentication [german]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(655, 'project', 'project-finished-to-customer', 'german', 'Project Marked as Finished (Sent to Customer Contacts) [german]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(656, 'credit_note', 'credit-note-send-to-client', 'german', 'Send Credit Note To Email [german]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(657, 'tasks', 'task-status-change-to-staff', 'german', 'Task Status Changed (Sent to Staff) [german]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(658, 'tasks', 'task-status-change-to-contacts', 'german', 'Task Status Changed (Sent to Customer Contacts) [german]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(659, 'staff', 'reminder-email-staff', 'german', 'Staff Reminder Email [german]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(660, 'contract', 'contract-comment-to-client', 'german', 'New Comment  (Sent to Customer Contacts) [german]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(661, 'contract', 'contract-comment-to-admin', 'german', 'New Comment (Sent to Staff)  [german]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(662, 'subscriptions', 'send-subscription', 'german', 'Send Subscription to Customer [german]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(663, 'subscriptions', 'subscription-payment-failed', 'german', 'Subscription Payment Failed [german]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(664, 'subscriptions', 'subscription-canceled', 'german', 'Subscription Canceled (Sent to customer primary contact) [german]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(665, 'subscriptions', 'subscription-payment-succeeded', 'german', 'Subscription Payment Succeeded (Sent to customer primary contact) [german]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(666, 'contract', 'contract-expiration-to-staff', 'german', 'Contract Expiration Reminder (Sent to Staff) [german]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(667, 'gdpr', 'gdpr-removal-request', 'german', 'Removal Request From Contact (Sent to administrators) [german]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(668, 'gdpr', 'gdpr-removal-request-lead', 'german', 'Removal Request From Lead (Sent to administrators) [german]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(669, 'client', 'client-registration-confirmed', 'german', 'Customer Registration Confirmed [german]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(670, 'contract', 'contract-signed-to-staff', 'german', 'Contract Signed (Sent to Staff) [german]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(671, 'subscriptions', 'customer-subscribed-to-staff', 'german', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [german]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(672, 'client', 'contact-verification-email', 'german', 'Email Verification (Sent to Contact After Registration) [german]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(673, 'client', 'new-customer-profile-file-uploaded-to-staff', 'german', 'New Customer Profile File(s) Uploaded (Sent to Staff) [german]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(674, 'staff', 'event-notification-to-staff', 'german', 'Event Notification (Calendar) [german]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(675, 'subscriptions', 'subscription-payment-requires-action', 'german', 'Credit Card Authorization Required - SCA [german]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(676, 'client', 'new-client-created', 'french', 'New Contact Added/Registered (Welcome Email) [french]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(677, 'invoice', 'invoice-send-to-client', 'french', 'Send Invoice to Customer [french]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(678, 'ticket', 'new-ticket-opened-admin', 'french', 'New Ticket Opened (Opened by Staff, Sent to Customer) [french]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(679, 'ticket', 'ticket-reply', 'french', 'Ticket Reply (Sent to Customer) [french]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(680, 'ticket', 'ticket-autoresponse', 'french', 'New Ticket Opened - Autoresponse [french]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(681, 'invoice', 'invoice-payment-recorded', 'french', 'Invoice Payment Recorded (Sent to Customer) [french]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(682, 'invoice', 'invoice-overdue-notice', 'french', 'Invoice Overdue Notice [french]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(683, 'invoice', 'invoice-already-send', 'french', 'Invoice Already Sent to Customer [french]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(684, 'ticket', 'new-ticket-created-staff', 'french', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [french]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(685, 'estimate', 'estimate-send-to-client', 'french', 'Send Estimate to Customer [french]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(686, 'ticket', 'ticket-reply-to-admin', 'french', 'Ticket Reply (Sent to Staff) [french]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(687, 'estimate', 'estimate-already-send', 'french', 'Estimate Already Sent to Customer [french]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(688, 'contract', 'contract-expiration', 'french', 'Contract Expiration Reminder (Sent to Customer Contacts) [french]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(689, 'tasks', 'task-assigned', 'french', 'New Task Assigned (Sent to Staff) [french]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(690, 'tasks', 'task-added-as-follower', 'french', 'Staff Member Added as Follower on Task (Sent to Staff) [french]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(691, 'tasks', 'task-commented', 'french', 'New Comment on Task (Sent to Staff) [french]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(692, 'tasks', 'task-added-attachment', 'french', 'New Attachment(s) on Task (Sent to Staff) [french]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(693, 'estimate', 'estimate-declined-to-staff', 'french', 'Estimate Declined (Sent to Staff) [french]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(694, 'estimate', 'estimate-accepted-to-staff', 'french', 'Estimate Accepted (Sent to Staff) [french]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(695, 'proposals', 'proposal-client-accepted', 'french', 'Customer Action - Accepted (Sent to Staff) [french]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(696, 'proposals', 'proposal-send-to-customer', 'french', 'Send Proposal to Customer [french]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(697, 'proposals', 'proposal-client-declined', 'french', 'Customer Action - Declined (Sent to Staff) [french]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(698, 'proposals', 'proposal-client-thank-you', 'french', 'Thank You Email (Sent to Customer After Accept) [french]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(699, 'proposals', 'proposal-comment-to-client', 'french', 'New Comment  (Sent to Customer/Lead) [french]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(700, 'proposals', 'proposal-comment-to-admin', 'french', 'New Comment (Sent to Staff)  [french]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(701, 'estimate', 'estimate-thank-you-to-customer', 'french', 'Thank You Email (Sent to Customer After Accept) [french]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(702, 'tasks', 'task-deadline-notification', 'french', 'Task Deadline Reminder - Sent to Assigned Members [french]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(703, 'contract', 'send-contract', 'french', 'Send Contract to Customer [french]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(704, 'invoice', 'invoice-payment-recorded-to-staff', 'french', 'Invoice Payment Recorded (Sent to Staff) [french]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(705, 'ticket', 'auto-close-ticket', 'french', 'Auto Close Ticket [french]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(706, 'project', 'new-project-discussion-created-to-staff', 'french', 'New Project Discussion (Sent to Project Members) [french]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(707, 'project', 'new-project-discussion-created-to-customer', 'french', 'New Project Discussion (Sent to Customer Contacts) [french]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(708, 'project', 'new-project-file-uploaded-to-customer', 'french', 'New Project File(s) Uploaded (Sent to Customer Contacts) [french]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(709, 'project', 'new-project-file-uploaded-to-staff', 'french', 'New Project File(s) Uploaded (Sent to Project Members) [french]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(710, 'project', 'new-project-discussion-comment-to-customer', 'french', 'New Discussion Comment  (Sent to Customer Contacts) [french]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(711, 'project', 'new-project-discussion-comment-to-staff', 'french', 'New Discussion Comment (Sent to Project Members) [french]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(712, 'project', 'staff-added-as-project-member', 'french', 'Staff Added as Project Member [french]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(713, 'estimate', 'estimate-expiry-reminder', 'french', 'Estimate Expiration Reminder [french]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(714, 'proposals', 'proposal-expiry-reminder', 'french', 'Proposal Expiration Reminder [french]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(715, 'staff', 'new-staff-created', 'french', 'New Staff Created (Welcome Email) [french]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(716, 'client', 'contact-forgot-password', 'french', 'Forgot Password [french]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(717, 'client', 'contact-password-reseted', 'french', 'Password Reset - Confirmation [french]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(718, 'client', 'contact-set-password', 'french', 'Set New Password [french]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(719, 'staff', 'staff-forgot-password', 'french', 'Forgot Password [french]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(720, 'staff', 'staff-password-reseted', 'french', 'Password Reset - Confirmation [french]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(721, 'project', 'assigned-to-project', 'french', 'New Project Created (Sent to Customer Contacts) [french]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(722, 'tasks', 'task-added-attachment-to-contacts', 'french', 'New Attachment(s) on Task (Sent to Customer Contacts) [french]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(723, 'tasks', 'task-commented-to-contacts', 'french', 'New Comment on Task (Sent to Customer Contacts) [french]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(724, 'leads', 'new-lead-assigned', 'french', 'New Lead Assigned to Staff Member [french]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(725, 'client', 'client-statement', 'french', 'Statement - Account Summary [french]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(726, 'ticket', 'ticket-assigned-to-admin', 'french', 'New Ticket Assigned (Sent to Staff) [french]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(727, 'client', 'new-client-registered-to-admin', 'french', 'New Customer Registration (Sent to admins) [french]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(728, 'leads', 'new-web-to-lead-form-submitted', 'french', 'Web to lead form submitted - Sent to lead [french]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(729, 'staff', 'two-factor-authentication', 'french', 'Two Factor Authentication [french]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(730, 'project', 'project-finished-to-customer', 'french', 'Project Marked as Finished (Sent to Customer Contacts) [french]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(731, 'credit_note', 'credit-note-send-to-client', 'french', 'Send Credit Note To Email [french]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(732, 'tasks', 'task-status-change-to-staff', 'french', 'Task Status Changed (Sent to Staff) [french]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(733, 'tasks', 'task-status-change-to-contacts', 'french', 'Task Status Changed (Sent to Customer Contacts) [french]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(734, 'staff', 'reminder-email-staff', 'french', 'Staff Reminder Email [french]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(735, 'contract', 'contract-comment-to-client', 'french', 'New Comment  (Sent to Customer Contacts) [french]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(736, 'contract', 'contract-comment-to-admin', 'french', 'New Comment (Sent to Staff)  [french]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(737, 'subscriptions', 'send-subscription', 'french', 'Send Subscription to Customer [french]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(738, 'subscriptions', 'subscription-payment-failed', 'french', 'Subscription Payment Failed [french]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(739, 'subscriptions', 'subscription-canceled', 'french', 'Subscription Canceled (Sent to customer primary contact) [french]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(740, 'subscriptions', 'subscription-payment-succeeded', 'french', 'Subscription Payment Succeeded (Sent to customer primary contact) [french]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(741, 'contract', 'contract-expiration-to-staff', 'french', 'Contract Expiration Reminder (Sent to Staff) [french]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(742, 'gdpr', 'gdpr-removal-request', 'french', 'Removal Request From Contact (Sent to administrators) [french]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(743, 'gdpr', 'gdpr-removal-request-lead', 'french', 'Removal Request From Lead (Sent to administrators) [french]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(744, 'client', 'client-registration-confirmed', 'french', 'Customer Registration Confirmed [french]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(745, 'contract', 'contract-signed-to-staff', 'french', 'Contract Signed (Sent to Staff) [french]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(746, 'subscriptions', 'customer-subscribed-to-staff', 'french', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [french]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(747, 'client', 'contact-verification-email', 'french', 'Email Verification (Sent to Contact After Registration) [french]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(748, 'client', 'new-customer-profile-file-uploaded-to-staff', 'french', 'New Customer Profile File(s) Uploaded (Sent to Staff) [french]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(749, 'staff', 'event-notification-to-staff', 'french', 'Event Notification (Calendar) [french]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(750, 'subscriptions', 'subscription-payment-requires-action', 'french', 'Credit Card Authorization Required - SCA [french]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(751, 'client', 'new-client-created', 'italian', 'New Contact Added/Registered (Welcome Email) [italian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(752, 'invoice', 'invoice-send-to-client', 'italian', 'Send Invoice to Customer [italian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(753, 'ticket', 'new-ticket-opened-admin', 'italian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [italian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(754, 'ticket', 'ticket-reply', 'italian', 'Ticket Reply (Sent to Customer) [italian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(755, 'ticket', 'ticket-autoresponse', 'italian', 'New Ticket Opened - Autoresponse [italian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(756, 'invoice', 'invoice-payment-recorded', 'italian', 'Invoice Payment Recorded (Sent to Customer) [italian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(757, 'invoice', 'invoice-overdue-notice', 'italian', 'Invoice Overdue Notice [italian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(758, 'invoice', 'invoice-already-send', 'italian', 'Invoice Already Sent to Customer [italian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(759, 'ticket', 'new-ticket-created-staff', 'italian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [italian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(760, 'estimate', 'estimate-send-to-client', 'italian', 'Send Estimate to Customer [italian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(761, 'ticket', 'ticket-reply-to-admin', 'italian', 'Ticket Reply (Sent to Staff) [italian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(762, 'estimate', 'estimate-already-send', 'italian', 'Estimate Already Sent to Customer [italian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(763, 'contract', 'contract-expiration', 'italian', 'Contract Expiration Reminder (Sent to Customer Contacts) [italian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(764, 'tasks', 'task-assigned', 'italian', 'New Task Assigned (Sent to Staff) [italian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(765, 'tasks', 'task-added-as-follower', 'italian', 'Staff Member Added as Follower on Task (Sent to Staff) [italian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(766, 'tasks', 'task-commented', 'italian', 'New Comment on Task (Sent to Staff) [italian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(767, 'tasks', 'task-added-attachment', 'italian', 'New Attachment(s) on Task (Sent to Staff) [italian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(768, 'estimate', 'estimate-declined-to-staff', 'italian', 'Estimate Declined (Sent to Staff) [italian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(769, 'estimate', 'estimate-accepted-to-staff', 'italian', 'Estimate Accepted (Sent to Staff) [italian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(770, 'proposals', 'proposal-client-accepted', 'italian', 'Customer Action - Accepted (Sent to Staff) [italian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(771, 'proposals', 'proposal-send-to-customer', 'italian', 'Send Proposal to Customer [italian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(772, 'proposals', 'proposal-client-declined', 'italian', 'Customer Action - Declined (Sent to Staff) [italian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(773, 'proposals', 'proposal-client-thank-you', 'italian', 'Thank You Email (Sent to Customer After Accept) [italian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(774, 'proposals', 'proposal-comment-to-client', 'italian', 'New Comment  (Sent to Customer/Lead) [italian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(775, 'proposals', 'proposal-comment-to-admin', 'italian', 'New Comment (Sent to Staff)  [italian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(776, 'estimate', 'estimate-thank-you-to-customer', 'italian', 'Thank You Email (Sent to Customer After Accept) [italian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(777, 'tasks', 'task-deadline-notification', 'italian', 'Task Deadline Reminder - Sent to Assigned Members [italian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(778, 'contract', 'send-contract', 'italian', 'Send Contract to Customer [italian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(779, 'invoice', 'invoice-payment-recorded-to-staff', 'italian', 'Invoice Payment Recorded (Sent to Staff) [italian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(780, 'ticket', 'auto-close-ticket', 'italian', 'Auto Close Ticket [italian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(781, 'project', 'new-project-discussion-created-to-staff', 'italian', 'New Project Discussion (Sent to Project Members) [italian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(782, 'project', 'new-project-discussion-created-to-customer', 'italian', 'New Project Discussion (Sent to Customer Contacts) [italian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(783, 'project', 'new-project-file-uploaded-to-customer', 'italian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [italian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(784, 'project', 'new-project-file-uploaded-to-staff', 'italian', 'New Project File(s) Uploaded (Sent to Project Members) [italian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(785, 'project', 'new-project-discussion-comment-to-customer', 'italian', 'New Discussion Comment  (Sent to Customer Contacts) [italian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(786, 'project', 'new-project-discussion-comment-to-staff', 'italian', 'New Discussion Comment (Sent to Project Members) [italian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(787, 'project', 'staff-added-as-project-member', 'italian', 'Staff Added as Project Member [italian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(788, 'estimate', 'estimate-expiry-reminder', 'italian', 'Estimate Expiration Reminder [italian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(789, 'proposals', 'proposal-expiry-reminder', 'italian', 'Proposal Expiration Reminder [italian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(790, 'staff', 'new-staff-created', 'italian', 'New Staff Created (Welcome Email) [italian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(791, 'client', 'contact-forgot-password', 'italian', 'Forgot Password [italian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(792, 'client', 'contact-password-reseted', 'italian', 'Password Reset - Confirmation [italian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(793, 'client', 'contact-set-password', 'italian', 'Set New Password [italian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(794, 'staff', 'staff-forgot-password', 'italian', 'Forgot Password [italian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(795, 'staff', 'staff-password-reseted', 'italian', 'Password Reset - Confirmation [italian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(796, 'project', 'assigned-to-project', 'italian', 'New Project Created (Sent to Customer Contacts) [italian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(797, 'tasks', 'task-added-attachment-to-contacts', 'italian', 'New Attachment(s) on Task (Sent to Customer Contacts) [italian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(798, 'tasks', 'task-commented-to-contacts', 'italian', 'New Comment on Task (Sent to Customer Contacts) [italian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(799, 'leads', 'new-lead-assigned', 'italian', 'New Lead Assigned to Staff Member [italian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(800, 'client', 'client-statement', 'italian', 'Statement - Account Summary [italian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(801, 'ticket', 'ticket-assigned-to-admin', 'italian', 'New Ticket Assigned (Sent to Staff) [italian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(802, 'client', 'new-client-registered-to-admin', 'italian', 'New Customer Registration (Sent to admins) [italian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(803, 'leads', 'new-web-to-lead-form-submitted', 'italian', 'Web to lead form submitted - Sent to lead [italian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(804, 'staff', 'two-factor-authentication', 'italian', 'Two Factor Authentication [italian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(805, 'project', 'project-finished-to-customer', 'italian', 'Project Marked as Finished (Sent to Customer Contacts) [italian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(806, 'credit_note', 'credit-note-send-to-client', 'italian', 'Send Credit Note To Email [italian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(807, 'tasks', 'task-status-change-to-staff', 'italian', 'Task Status Changed (Sent to Staff) [italian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(808, 'tasks', 'task-status-change-to-contacts', 'italian', 'Task Status Changed (Sent to Customer Contacts) [italian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(809, 'staff', 'reminder-email-staff', 'italian', 'Staff Reminder Email [italian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(810, 'contract', 'contract-comment-to-client', 'italian', 'New Comment  (Sent to Customer Contacts) [italian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(811, 'contract', 'contract-comment-to-admin', 'italian', 'New Comment (Sent to Staff)  [italian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(812, 'subscriptions', 'send-subscription', 'italian', 'Send Subscription to Customer [italian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(813, 'subscriptions', 'subscription-payment-failed', 'italian', 'Subscription Payment Failed [italian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(814, 'subscriptions', 'subscription-canceled', 'italian', 'Subscription Canceled (Sent to customer primary contact) [italian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(815, 'subscriptions', 'subscription-payment-succeeded', 'italian', 'Subscription Payment Succeeded (Sent to customer primary contact) [italian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(816, 'contract', 'contract-expiration-to-staff', 'italian', 'Contract Expiration Reminder (Sent to Staff) [italian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(817, 'gdpr', 'gdpr-removal-request', 'italian', 'Removal Request From Contact (Sent to administrators) [italian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(818, 'gdpr', 'gdpr-removal-request-lead', 'italian', 'Removal Request From Lead (Sent to administrators) [italian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(819, 'client', 'client-registration-confirmed', 'italian', 'Customer Registration Confirmed [italian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(820, 'contract', 'contract-signed-to-staff', 'italian', 'Contract Signed (Sent to Staff) [italian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(821, 'subscriptions', 'customer-subscribed-to-staff', 'italian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [italian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(822, 'client', 'contact-verification-email', 'italian', 'Email Verification (Sent to Contact After Registration) [italian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(823, 'client', 'new-customer-profile-file-uploaded-to-staff', 'italian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [italian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(824, 'staff', 'event-notification-to-staff', 'italian', 'Event Notification (Calendar) [italian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(825, 'subscriptions', 'subscription-payment-requires-action', 'italian', 'Credit Card Authorization Required - SCA [italian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(826, 'client', 'new-client-created', 'spanish', 'New Contact Added/Registered (Welcome Email) [spanish]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(827, 'invoice', 'invoice-send-to-client', 'spanish', 'Send Invoice to Customer [spanish]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(828, 'ticket', 'new-ticket-opened-admin', 'spanish', 'New Ticket Opened (Opened by Staff, Sent to Customer) [spanish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(829, 'ticket', 'ticket-reply', 'spanish', 'Ticket Reply (Sent to Customer) [spanish]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(830, 'ticket', 'ticket-autoresponse', 'spanish', 'New Ticket Opened - Autoresponse [spanish]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(831, 'invoice', 'invoice-payment-recorded', 'spanish', 'Invoice Payment Recorded (Sent to Customer) [spanish]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(832, 'invoice', 'invoice-overdue-notice', 'spanish', 'Invoice Overdue Notice [spanish]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(833, 'invoice', 'invoice-already-send', 'spanish', 'Invoice Already Sent to Customer [spanish]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(834, 'ticket', 'new-ticket-created-staff', 'spanish', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [spanish]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(835, 'estimate', 'estimate-send-to-client', 'spanish', 'Send Estimate to Customer [spanish]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(836, 'ticket', 'ticket-reply-to-admin', 'spanish', 'Ticket Reply (Sent to Staff) [spanish]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(837, 'estimate', 'estimate-already-send', 'spanish', 'Estimate Already Sent to Customer [spanish]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(838, 'contract', 'contract-expiration', 'spanish', 'Contract Expiration Reminder (Sent to Customer Contacts) [spanish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(839, 'tasks', 'task-assigned', 'spanish', 'New Task Assigned (Sent to Staff) [spanish]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(840, 'tasks', 'task-added-as-follower', 'spanish', 'Staff Member Added as Follower on Task (Sent to Staff) [spanish]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(841, 'tasks', 'task-commented', 'spanish', 'New Comment on Task (Sent to Staff) [spanish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(842, 'tasks', 'task-added-attachment', 'spanish', 'New Attachment(s) on Task (Sent to Staff) [spanish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(843, 'estimate', 'estimate-declined-to-staff', 'spanish', 'Estimate Declined (Sent to Staff) [spanish]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(844, 'estimate', 'estimate-accepted-to-staff', 'spanish', 'Estimate Accepted (Sent to Staff) [spanish]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(845, 'proposals', 'proposal-client-accepted', 'spanish', 'Customer Action - Accepted (Sent to Staff) [spanish]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(846, 'proposals', 'proposal-send-to-customer', 'spanish', 'Send Proposal to Customer [spanish]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(847, 'proposals', 'proposal-client-declined', 'spanish', 'Customer Action - Declined (Sent to Staff) [spanish]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(848, 'proposals', 'proposal-client-thank-you', 'spanish', 'Thank You Email (Sent to Customer After Accept) [spanish]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(849, 'proposals', 'proposal-comment-to-client', 'spanish', 'New Comment  (Sent to Customer/Lead) [spanish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(850, 'proposals', 'proposal-comment-to-admin', 'spanish', 'New Comment (Sent to Staff)  [spanish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(851, 'estimate', 'estimate-thank-you-to-customer', 'spanish', 'Thank You Email (Sent to Customer After Accept) [spanish]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(852, 'tasks', 'task-deadline-notification', 'spanish', 'Task Deadline Reminder - Sent to Assigned Members [spanish]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(853, 'contract', 'send-contract', 'spanish', 'Send Contract to Customer [spanish]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(854, 'invoice', 'invoice-payment-recorded-to-staff', 'spanish', 'Invoice Payment Recorded (Sent to Staff) [spanish]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(855, 'ticket', 'auto-close-ticket', 'spanish', 'Auto Close Ticket [spanish]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(856, 'project', 'new-project-discussion-created-to-staff', 'spanish', 'New Project Discussion (Sent to Project Members) [spanish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(857, 'project', 'new-project-discussion-created-to-customer', 'spanish', 'New Project Discussion (Sent to Customer Contacts) [spanish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(858, 'project', 'new-project-file-uploaded-to-customer', 'spanish', 'New Project File(s) Uploaded (Sent to Customer Contacts) [spanish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(859, 'project', 'new-project-file-uploaded-to-staff', 'spanish', 'New Project File(s) Uploaded (Sent to Project Members) [spanish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(860, 'project', 'new-project-discussion-comment-to-customer', 'spanish', 'New Discussion Comment  (Sent to Customer Contacts) [spanish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(861, 'project', 'new-project-discussion-comment-to-staff', 'spanish', 'New Discussion Comment (Sent to Project Members) [spanish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(862, 'project', 'staff-added-as-project-member', 'spanish', 'Staff Added as Project Member [spanish]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(863, 'estimate', 'estimate-expiry-reminder', 'spanish', 'Estimate Expiration Reminder [spanish]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(864, 'proposals', 'proposal-expiry-reminder', 'spanish', 'Proposal Expiration Reminder [spanish]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(865, 'staff', 'new-staff-created', 'spanish', 'New Staff Created (Welcome Email) [spanish]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(866, 'client', 'contact-forgot-password', 'spanish', 'Forgot Password [spanish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(867, 'client', 'contact-password-reseted', 'spanish', 'Password Reset - Confirmation [spanish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(868, 'client', 'contact-set-password', 'spanish', 'Set New Password [spanish]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(869, 'staff', 'staff-forgot-password', 'spanish', 'Forgot Password [spanish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(870, 'staff', 'staff-password-reseted', 'spanish', 'Password Reset - Confirmation [spanish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(871, 'project', 'assigned-to-project', 'spanish', 'New Project Created (Sent to Customer Contacts) [spanish]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(872, 'tasks', 'task-added-attachment-to-contacts', 'spanish', 'New Attachment(s) on Task (Sent to Customer Contacts) [spanish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(873, 'tasks', 'task-commented-to-contacts', 'spanish', 'New Comment on Task (Sent to Customer Contacts) [spanish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(874, 'leads', 'new-lead-assigned', 'spanish', 'New Lead Assigned to Staff Member [spanish]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(875, 'client', 'client-statement', 'spanish', 'Statement - Account Summary [spanish]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(876, 'ticket', 'ticket-assigned-to-admin', 'spanish', 'New Ticket Assigned (Sent to Staff) [spanish]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(877, 'client', 'new-client-registered-to-admin', 'spanish', 'New Customer Registration (Sent to admins) [spanish]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(878, 'leads', 'new-web-to-lead-form-submitted', 'spanish', 'Web to lead form submitted - Sent to lead [spanish]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(879, 'staff', 'two-factor-authentication', 'spanish', 'Two Factor Authentication [spanish]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(880, 'project', 'project-finished-to-customer', 'spanish', 'Project Marked as Finished (Sent to Customer Contacts) [spanish]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(881, 'credit_note', 'credit-note-send-to-client', 'spanish', 'Send Credit Note To Email [spanish]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(882, 'tasks', 'task-status-change-to-staff', 'spanish', 'Task Status Changed (Sent to Staff) [spanish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(883, 'tasks', 'task-status-change-to-contacts', 'spanish', 'Task Status Changed (Sent to Customer Contacts) [spanish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(884, 'staff', 'reminder-email-staff', 'spanish', 'Staff Reminder Email [spanish]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(885, 'contract', 'contract-comment-to-client', 'spanish', 'New Comment  (Sent to Customer Contacts) [spanish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(886, 'contract', 'contract-comment-to-admin', 'spanish', 'New Comment (Sent to Staff)  [spanish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(887, 'subscriptions', 'send-subscription', 'spanish', 'Send Subscription to Customer [spanish]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(888, 'subscriptions', 'subscription-payment-failed', 'spanish', 'Subscription Payment Failed [spanish]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(889, 'subscriptions', 'subscription-canceled', 'spanish', 'Subscription Canceled (Sent to customer primary contact) [spanish]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(890, 'subscriptions', 'subscription-payment-succeeded', 'spanish', 'Subscription Payment Succeeded (Sent to customer primary contact) [spanish]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(891, 'contract', 'contract-expiration-to-staff', 'spanish', 'Contract Expiration Reminder (Sent to Staff) [spanish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(892, 'gdpr', 'gdpr-removal-request', 'spanish', 'Removal Request From Contact (Sent to administrators) [spanish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(893, 'gdpr', 'gdpr-removal-request-lead', 'spanish', 'Removal Request From Lead (Sent to administrators) [spanish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(894, 'client', 'client-registration-confirmed', 'spanish', 'Customer Registration Confirmed [spanish]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(895, 'contract', 'contract-signed-to-staff', 'spanish', 'Contract Signed (Sent to Staff) [spanish]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(896, 'subscriptions', 'customer-subscribed-to-staff', 'spanish', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [spanish]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(897, 'client', 'contact-verification-email', 'spanish', 'Email Verification (Sent to Contact After Registration) [spanish]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(898, 'client', 'new-customer-profile-file-uploaded-to-staff', 'spanish', 'New Customer Profile File(s) Uploaded (Sent to Staff) [spanish]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(899, 'staff', 'event-notification-to-staff', 'spanish', 'Event Notification (Calendar) [spanish]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(900, 'subscriptions', 'subscription-payment-requires-action', 'spanish', 'Credit Card Authorization Required - SCA [spanish]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(901, 'client', 'new-client-created', 'russian', 'New Contact Added/Registered (Welcome Email) [russian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(902, 'invoice', 'invoice-send-to-client', 'russian', 'Send Invoice to Customer [russian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(903, 'ticket', 'new-ticket-opened-admin', 'russian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [russian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(904, 'ticket', 'ticket-reply', 'russian', 'Ticket Reply (Sent to Customer) [russian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(905, 'ticket', 'ticket-autoresponse', 'russian', 'New Ticket Opened - Autoresponse [russian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(906, 'invoice', 'invoice-payment-recorded', 'russian', 'Invoice Payment Recorded (Sent to Customer) [russian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(907, 'invoice', 'invoice-overdue-notice', 'russian', 'Invoice Overdue Notice [russian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(908, 'invoice', 'invoice-already-send', 'russian', 'Invoice Already Sent to Customer [russian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(909, 'ticket', 'new-ticket-created-staff', 'russian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [russian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(910, 'estimate', 'estimate-send-to-client', 'russian', 'Send Estimate to Customer [russian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(911, 'ticket', 'ticket-reply-to-admin', 'russian', 'Ticket Reply (Sent to Staff) [russian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(912, 'estimate', 'estimate-already-send', 'russian', 'Estimate Already Sent to Customer [russian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(913, 'contract', 'contract-expiration', 'russian', 'Contract Expiration Reminder (Sent to Customer Contacts) [russian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(914, 'tasks', 'task-assigned', 'russian', 'New Task Assigned (Sent to Staff) [russian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(915, 'tasks', 'task-added-as-follower', 'russian', 'Staff Member Added as Follower on Task (Sent to Staff) [russian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(916, 'tasks', 'task-commented', 'russian', 'New Comment on Task (Sent to Staff) [russian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(917, 'tasks', 'task-added-attachment', 'russian', 'New Attachment(s) on Task (Sent to Staff) [russian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(918, 'estimate', 'estimate-declined-to-staff', 'russian', 'Estimate Declined (Sent to Staff) [russian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(919, 'estimate', 'estimate-accepted-to-staff', 'russian', 'Estimate Accepted (Sent to Staff) [russian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(920, 'proposals', 'proposal-client-accepted', 'russian', 'Customer Action - Accepted (Sent to Staff) [russian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(921, 'proposals', 'proposal-send-to-customer', 'russian', 'Send Proposal to Customer [russian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(922, 'proposals', 'proposal-client-declined', 'russian', 'Customer Action - Declined (Sent to Staff) [russian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(923, 'proposals', 'proposal-client-thank-you', 'russian', 'Thank You Email (Sent to Customer After Accept) [russian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(924, 'proposals', 'proposal-comment-to-client', 'russian', 'New Comment  (Sent to Customer/Lead) [russian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(925, 'proposals', 'proposal-comment-to-admin', 'russian', 'New Comment (Sent to Staff)  [russian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(926, 'estimate', 'estimate-thank-you-to-customer', 'russian', 'Thank You Email (Sent to Customer After Accept) [russian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(927, 'tasks', 'task-deadline-notification', 'russian', 'Task Deadline Reminder - Sent to Assigned Members [russian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(928, 'contract', 'send-contract', 'russian', 'Send Contract to Customer [russian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(929, 'invoice', 'invoice-payment-recorded-to-staff', 'russian', 'Invoice Payment Recorded (Sent to Staff) [russian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(930, 'ticket', 'auto-close-ticket', 'russian', 'Auto Close Ticket [russian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(931, 'project', 'new-project-discussion-created-to-staff', 'russian', 'New Project Discussion (Sent to Project Members) [russian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(932, 'project', 'new-project-discussion-created-to-customer', 'russian', 'New Project Discussion (Sent to Customer Contacts) [russian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(933, 'project', 'new-project-file-uploaded-to-customer', 'russian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [russian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(934, 'project', 'new-project-file-uploaded-to-staff', 'russian', 'New Project File(s) Uploaded (Sent to Project Members) [russian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(935, 'project', 'new-project-discussion-comment-to-customer', 'russian', 'New Discussion Comment  (Sent to Customer Contacts) [russian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(936, 'project', 'new-project-discussion-comment-to-staff', 'russian', 'New Discussion Comment (Sent to Project Members) [russian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(937, 'project', 'staff-added-as-project-member', 'russian', 'Staff Added as Project Member [russian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(938, 'estimate', 'estimate-expiry-reminder', 'russian', 'Estimate Expiration Reminder [russian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(939, 'proposals', 'proposal-expiry-reminder', 'russian', 'Proposal Expiration Reminder [russian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(940, 'staff', 'new-staff-created', 'russian', 'New Staff Created (Welcome Email) [russian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(941, 'client', 'contact-forgot-password', 'russian', 'Forgot Password [russian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(942, 'client', 'contact-password-reseted', 'russian', 'Password Reset - Confirmation [russian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(943, 'client', 'contact-set-password', 'russian', 'Set New Password [russian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(944, 'staff', 'staff-forgot-password', 'russian', 'Forgot Password [russian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(945, 'staff', 'staff-password-reseted', 'russian', 'Password Reset - Confirmation [russian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(946, 'project', 'assigned-to-project', 'russian', 'New Project Created (Sent to Customer Contacts) [russian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(947, 'tasks', 'task-added-attachment-to-contacts', 'russian', 'New Attachment(s) on Task (Sent to Customer Contacts) [russian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(948, 'tasks', 'task-commented-to-contacts', 'russian', 'New Comment on Task (Sent to Customer Contacts) [russian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(949, 'leads', 'new-lead-assigned', 'russian', 'New Lead Assigned to Staff Member [russian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(950, 'client', 'client-statement', 'russian', 'Statement - Account Summary [russian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(951, 'ticket', 'ticket-assigned-to-admin', 'russian', 'New Ticket Assigned (Sent to Staff) [russian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(952, 'client', 'new-client-registered-to-admin', 'russian', 'New Customer Registration (Sent to admins) [russian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(953, 'leads', 'new-web-to-lead-form-submitted', 'russian', 'Web to lead form submitted - Sent to lead [russian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(954, 'staff', 'two-factor-authentication', 'russian', 'Two Factor Authentication [russian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(955, 'project', 'project-finished-to-customer', 'russian', 'Project Marked as Finished (Sent to Customer Contacts) [russian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(956, 'credit_note', 'credit-note-send-to-client', 'russian', 'Send Credit Note To Email [russian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(957, 'tasks', 'task-status-change-to-staff', 'russian', 'Task Status Changed (Sent to Staff) [russian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(958, 'tasks', 'task-status-change-to-contacts', 'russian', 'Task Status Changed (Sent to Customer Contacts) [russian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(959, 'staff', 'reminder-email-staff', 'russian', 'Staff Reminder Email [russian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(960, 'contract', 'contract-comment-to-client', 'russian', 'New Comment  (Sent to Customer Contacts) [russian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(961, 'contract', 'contract-comment-to-admin', 'russian', 'New Comment (Sent to Staff)  [russian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(962, 'subscriptions', 'send-subscription', 'russian', 'Send Subscription to Customer [russian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(963, 'subscriptions', 'subscription-payment-failed', 'russian', 'Subscription Payment Failed [russian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(964, 'subscriptions', 'subscription-canceled', 'russian', 'Subscription Canceled (Sent to customer primary contact) [russian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(965, 'subscriptions', 'subscription-payment-succeeded', 'russian', 'Subscription Payment Succeeded (Sent to customer primary contact) [russian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(966, 'contract', 'contract-expiration-to-staff', 'russian', 'Contract Expiration Reminder (Sent to Staff) [russian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(967, 'gdpr', 'gdpr-removal-request', 'russian', 'Removal Request From Contact (Sent to administrators) [russian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(968, 'gdpr', 'gdpr-removal-request-lead', 'russian', 'Removal Request From Lead (Sent to administrators) [russian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(969, 'client', 'client-registration-confirmed', 'russian', 'Customer Registration Confirmed [russian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(970, 'contract', 'contract-signed-to-staff', 'russian', 'Contract Signed (Sent to Staff) [russian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(971, 'subscriptions', 'customer-subscribed-to-staff', 'russian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [russian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(972, 'client', 'contact-verification-email', 'russian', 'Email Verification (Sent to Contact After Registration) [russian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(973, 'client', 'new-customer-profile-file-uploaded-to-staff', 'russian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [russian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(974, 'staff', 'event-notification-to-staff', 'russian', 'Event Notification (Calendar) [russian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(975, 'subscriptions', 'subscription-payment-requires-action', 'russian', 'Credit Card Authorization Required - SCA [russian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(976, 'client', 'new-client-created', 'catalan', 'New Contact Added/Registered (Welcome Email) [catalan]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(977, 'invoice', 'invoice-send-to-client', 'catalan', 'Send Invoice to Customer [catalan]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(978, 'ticket', 'new-ticket-opened-admin', 'catalan', 'New Ticket Opened (Opened by Staff, Sent to Customer) [catalan]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(979, 'ticket', 'ticket-reply', 'catalan', 'Ticket Reply (Sent to Customer) [catalan]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(980, 'ticket', 'ticket-autoresponse', 'catalan', 'New Ticket Opened - Autoresponse [catalan]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(981, 'invoice', 'invoice-payment-recorded', 'catalan', 'Invoice Payment Recorded (Sent to Customer) [catalan]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(982, 'invoice', 'invoice-overdue-notice', 'catalan', 'Invoice Overdue Notice [catalan]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(983, 'invoice', 'invoice-already-send', 'catalan', 'Invoice Already Sent to Customer [catalan]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(984, 'ticket', 'new-ticket-created-staff', 'catalan', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [catalan]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(985, 'estimate', 'estimate-send-to-client', 'catalan', 'Send Estimate to Customer [catalan]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(986, 'ticket', 'ticket-reply-to-admin', 'catalan', 'Ticket Reply (Sent to Staff) [catalan]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(987, 'estimate', 'estimate-already-send', 'catalan', 'Estimate Already Sent to Customer [catalan]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(988, 'contract', 'contract-expiration', 'catalan', 'Contract Expiration Reminder (Sent to Customer Contacts) [catalan]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(989, 'tasks', 'task-assigned', 'catalan', 'New Task Assigned (Sent to Staff) [catalan]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(990, 'tasks', 'task-added-as-follower', 'catalan', 'Staff Member Added as Follower on Task (Sent to Staff) [catalan]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(991, 'tasks', 'task-commented', 'catalan', 'New Comment on Task (Sent to Staff) [catalan]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(992, 'tasks', 'task-added-attachment', 'catalan', 'New Attachment(s) on Task (Sent to Staff) [catalan]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(993, 'estimate', 'estimate-declined-to-staff', 'catalan', 'Estimate Declined (Sent to Staff) [catalan]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(994, 'estimate', 'estimate-accepted-to-staff', 'catalan', 'Estimate Accepted (Sent to Staff) [catalan]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(995, 'proposals', 'proposal-client-accepted', 'catalan', 'Customer Action - Accepted (Sent to Staff) [catalan]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(996, 'proposals', 'proposal-send-to-customer', 'catalan', 'Send Proposal to Customer [catalan]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(997, 'proposals', 'proposal-client-declined', 'catalan', 'Customer Action - Declined (Sent to Staff) [catalan]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(998, 'proposals', 'proposal-client-thank-you', 'catalan', 'Thank You Email (Sent to Customer After Accept) [catalan]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(999, 'proposals', 'proposal-comment-to-client', 'catalan', 'New Comment  (Sent to Customer/Lead) [catalan]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1000, 'proposals', 'proposal-comment-to-admin', 'catalan', 'New Comment (Sent to Staff)  [catalan]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1001, 'estimate', 'estimate-thank-you-to-customer', 'catalan', 'Thank You Email (Sent to Customer After Accept) [catalan]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1002, 'tasks', 'task-deadline-notification', 'catalan', 'Task Deadline Reminder - Sent to Assigned Members [catalan]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1003, 'contract', 'send-contract', 'catalan', 'Send Contract to Customer [catalan]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1004, 'invoice', 'invoice-payment-recorded-to-staff', 'catalan', 'Invoice Payment Recorded (Sent to Staff) [catalan]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1005, 'ticket', 'auto-close-ticket', 'catalan', 'Auto Close Ticket [catalan]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1006, 'project', 'new-project-discussion-created-to-staff', 'catalan', 'New Project Discussion (Sent to Project Members) [catalan]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1007, 'project', 'new-project-discussion-created-to-customer', 'catalan', 'New Project Discussion (Sent to Customer Contacts) [catalan]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1008, 'project', 'new-project-file-uploaded-to-customer', 'catalan', 'New Project File(s) Uploaded (Sent to Customer Contacts) [catalan]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1009, 'project', 'new-project-file-uploaded-to-staff', 'catalan', 'New Project File(s) Uploaded (Sent to Project Members) [catalan]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1010, 'project', 'new-project-discussion-comment-to-customer', 'catalan', 'New Discussion Comment  (Sent to Customer Contacts) [catalan]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1011, 'project', 'new-project-discussion-comment-to-staff', 'catalan', 'New Discussion Comment (Sent to Project Members) [catalan]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1012, 'project', 'staff-added-as-project-member', 'catalan', 'Staff Added as Project Member [catalan]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1013, 'estimate', 'estimate-expiry-reminder', 'catalan', 'Estimate Expiration Reminder [catalan]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1014, 'proposals', 'proposal-expiry-reminder', 'catalan', 'Proposal Expiration Reminder [catalan]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1015, 'staff', 'new-staff-created', 'catalan', 'New Staff Created (Welcome Email) [catalan]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1016, 'client', 'contact-forgot-password', 'catalan', 'Forgot Password [catalan]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1017, 'client', 'contact-password-reseted', 'catalan', 'Password Reset - Confirmation [catalan]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1018, 'client', 'contact-set-password', 'catalan', 'Set New Password [catalan]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1019, 'staff', 'staff-forgot-password', 'catalan', 'Forgot Password [catalan]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1020, 'staff', 'staff-password-reseted', 'catalan', 'Password Reset - Confirmation [catalan]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1021, 'project', 'assigned-to-project', 'catalan', 'New Project Created (Sent to Customer Contacts) [catalan]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1022, 'tasks', 'task-added-attachment-to-contacts', 'catalan', 'New Attachment(s) on Task (Sent to Customer Contacts) [catalan]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1023, 'tasks', 'task-commented-to-contacts', 'catalan', 'New Comment on Task (Sent to Customer Contacts) [catalan]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1024, 'leads', 'new-lead-assigned', 'catalan', 'New Lead Assigned to Staff Member [catalan]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1025, 'client', 'client-statement', 'catalan', 'Statement - Account Summary [catalan]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1026, 'ticket', 'ticket-assigned-to-admin', 'catalan', 'New Ticket Assigned (Sent to Staff) [catalan]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1027, 'client', 'new-client-registered-to-admin', 'catalan', 'New Customer Registration (Sent to admins) [catalan]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1028, 'leads', 'new-web-to-lead-form-submitted', 'catalan', 'Web to lead form submitted - Sent to lead [catalan]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1029, 'staff', 'two-factor-authentication', 'catalan', 'Two Factor Authentication [catalan]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1030, 'project', 'project-finished-to-customer', 'catalan', 'Project Marked as Finished (Sent to Customer Contacts) [catalan]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1031, 'credit_note', 'credit-note-send-to-client', 'catalan', 'Send Credit Note To Email [catalan]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1032, 'tasks', 'task-status-change-to-staff', 'catalan', 'Task Status Changed (Sent to Staff) [catalan]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1033, 'tasks', 'task-status-change-to-contacts', 'catalan', 'Task Status Changed (Sent to Customer Contacts) [catalan]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1034, 'staff', 'reminder-email-staff', 'catalan', 'Staff Reminder Email [catalan]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1035, 'contract', 'contract-comment-to-client', 'catalan', 'New Comment  (Sent to Customer Contacts) [catalan]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1036, 'contract', 'contract-comment-to-admin', 'catalan', 'New Comment (Sent to Staff)  [catalan]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1037, 'subscriptions', 'send-subscription', 'catalan', 'Send Subscription to Customer [catalan]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1038, 'subscriptions', 'subscription-payment-failed', 'catalan', 'Subscription Payment Failed [catalan]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1039, 'subscriptions', 'subscription-canceled', 'catalan', 'Subscription Canceled (Sent to customer primary contact) [catalan]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1040, 'subscriptions', 'subscription-payment-succeeded', 'catalan', 'Subscription Payment Succeeded (Sent to customer primary contact) [catalan]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1041, 'contract', 'contract-expiration-to-staff', 'catalan', 'Contract Expiration Reminder (Sent to Staff) [catalan]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1042, 'gdpr', 'gdpr-removal-request', 'catalan', 'Removal Request From Contact (Sent to administrators) [catalan]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1043, 'gdpr', 'gdpr-removal-request-lead', 'catalan', 'Removal Request From Lead (Sent to administrators) [catalan]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1044, 'client', 'client-registration-confirmed', 'catalan', 'Customer Registration Confirmed [catalan]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1045, 'contract', 'contract-signed-to-staff', 'catalan', 'Contract Signed (Sent to Staff) [catalan]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1046, 'subscriptions', 'customer-subscribed-to-staff', 'catalan', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [catalan]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1047, 'client', 'contact-verification-email', 'catalan', 'Email Verification (Sent to Contact After Registration) [catalan]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1048, 'client', 'new-customer-profile-file-uploaded-to-staff', 'catalan', 'New Customer Profile File(s) Uploaded (Sent to Staff) [catalan]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1049, 'staff', 'event-notification-to-staff', 'catalan', 'Event Notification (Calendar) [catalan]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1050, 'subscriptions', 'subscription-payment-requires-action', 'catalan', 'Credit Card Authorization Required - SCA [catalan]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1051, 'client', 'new-client-created', 'persian', 'New Contact Added/Registered (Welcome Email) [persian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1052, 'invoice', 'invoice-send-to-client', 'persian', 'Send Invoice to Customer [persian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1053, 'ticket', 'new-ticket-opened-admin', 'persian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [persian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1054, 'ticket', 'ticket-reply', 'persian', 'Ticket Reply (Sent to Customer) [persian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1055, 'ticket', 'ticket-autoresponse', 'persian', 'New Ticket Opened - Autoresponse [persian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1056, 'invoice', 'invoice-payment-recorded', 'persian', 'Invoice Payment Recorded (Sent to Customer) [persian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1057, 'invoice', 'invoice-overdue-notice', 'persian', 'Invoice Overdue Notice [persian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1058, 'invoice', 'invoice-already-send', 'persian', 'Invoice Already Sent to Customer [persian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1059, 'ticket', 'new-ticket-created-staff', 'persian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [persian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1060, 'estimate', 'estimate-send-to-client', 'persian', 'Send Estimate to Customer [persian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1061, 'ticket', 'ticket-reply-to-admin', 'persian', 'Ticket Reply (Sent to Staff) [persian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1062, 'estimate', 'estimate-already-send', 'persian', 'Estimate Already Sent to Customer [persian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1063, 'contract', 'contract-expiration', 'persian', 'Contract Expiration Reminder (Sent to Customer Contacts) [persian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1064, 'tasks', 'task-assigned', 'persian', 'New Task Assigned (Sent to Staff) [persian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1065, 'tasks', 'task-added-as-follower', 'persian', 'Staff Member Added as Follower on Task (Sent to Staff) [persian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1066, 'tasks', 'task-commented', 'persian', 'New Comment on Task (Sent to Staff) [persian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1067, 'tasks', 'task-added-attachment', 'persian', 'New Attachment(s) on Task (Sent to Staff) [persian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1068, 'estimate', 'estimate-declined-to-staff', 'persian', 'Estimate Declined (Sent to Staff) [persian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1069, 'estimate', 'estimate-accepted-to-staff', 'persian', 'Estimate Accepted (Sent to Staff) [persian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1070, 'proposals', 'proposal-client-accepted', 'persian', 'Customer Action - Accepted (Sent to Staff) [persian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1071, 'proposals', 'proposal-send-to-customer', 'persian', 'Send Proposal to Customer [persian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1072, 'proposals', 'proposal-client-declined', 'persian', 'Customer Action - Declined (Sent to Staff) [persian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1073, 'proposals', 'proposal-client-thank-you', 'persian', 'Thank You Email (Sent to Customer After Accept) [persian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1074, 'proposals', 'proposal-comment-to-client', 'persian', 'New Comment  (Sent to Customer/Lead) [persian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1075, 'proposals', 'proposal-comment-to-admin', 'persian', 'New Comment (Sent to Staff)  [persian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1076, 'estimate', 'estimate-thank-you-to-customer', 'persian', 'Thank You Email (Sent to Customer After Accept) [persian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1077, 'tasks', 'task-deadline-notification', 'persian', 'Task Deadline Reminder - Sent to Assigned Members [persian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1078, 'contract', 'send-contract', 'persian', 'Send Contract to Customer [persian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1079, 'invoice', 'invoice-payment-recorded-to-staff', 'persian', 'Invoice Payment Recorded (Sent to Staff) [persian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1080, 'ticket', 'auto-close-ticket', 'persian', 'Auto Close Ticket [persian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1081, 'project', 'new-project-discussion-created-to-staff', 'persian', 'New Project Discussion (Sent to Project Members) [persian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1082, 'project', 'new-project-discussion-created-to-customer', 'persian', 'New Project Discussion (Sent to Customer Contacts) [persian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1083, 'project', 'new-project-file-uploaded-to-customer', 'persian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [persian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1084, 'project', 'new-project-file-uploaded-to-staff', 'persian', 'New Project File(s) Uploaded (Sent to Project Members) [persian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1085, 'project', 'new-project-discussion-comment-to-customer', 'persian', 'New Discussion Comment  (Sent to Customer Contacts) [persian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1086, 'project', 'new-project-discussion-comment-to-staff', 'persian', 'New Discussion Comment (Sent to Project Members) [persian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1087, 'project', 'staff-added-as-project-member', 'persian', 'Staff Added as Project Member [persian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1088, 'estimate', 'estimate-expiry-reminder', 'persian', 'Estimate Expiration Reminder [persian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1089, 'proposals', 'proposal-expiry-reminder', 'persian', 'Proposal Expiration Reminder [persian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1090, 'staff', 'new-staff-created', 'persian', 'New Staff Created (Welcome Email) [persian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1091, 'client', 'contact-forgot-password', 'persian', 'Forgot Password [persian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1092, 'client', 'contact-password-reseted', 'persian', 'Password Reset - Confirmation [persian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1093, 'client', 'contact-set-password', 'persian', 'Set New Password [persian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1094, 'staff', 'staff-forgot-password', 'persian', 'Forgot Password [persian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1095, 'staff', 'staff-password-reseted', 'persian', 'Password Reset - Confirmation [persian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1096, 'project', 'assigned-to-project', 'persian', 'New Project Created (Sent to Customer Contacts) [persian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1097, 'tasks', 'task-added-attachment-to-contacts', 'persian', 'New Attachment(s) on Task (Sent to Customer Contacts) [persian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1098, 'tasks', 'task-commented-to-contacts', 'persian', 'New Comment on Task (Sent to Customer Contacts) [persian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1099, 'leads', 'new-lead-assigned', 'persian', 'New Lead Assigned to Staff Member [persian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1100, 'client', 'client-statement', 'persian', 'Statement - Account Summary [persian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1101, 'ticket', 'ticket-assigned-to-admin', 'persian', 'New Ticket Assigned (Sent to Staff) [persian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1102, 'client', 'new-client-registered-to-admin', 'persian', 'New Customer Registration (Sent to admins) [persian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1103, 'leads', 'new-web-to-lead-form-submitted', 'persian', 'Web to lead form submitted - Sent to lead [persian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1104, 'staff', 'two-factor-authentication', 'persian', 'Two Factor Authentication [persian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1105, 'project', 'project-finished-to-customer', 'persian', 'Project Marked as Finished (Sent to Customer Contacts) [persian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1106, 'credit_note', 'credit-note-send-to-client', 'persian', 'Send Credit Note To Email [persian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1107, 'tasks', 'task-status-change-to-staff', 'persian', 'Task Status Changed (Sent to Staff) [persian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1108, 'tasks', 'task-status-change-to-contacts', 'persian', 'Task Status Changed (Sent to Customer Contacts) [persian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1109, 'staff', 'reminder-email-staff', 'persian', 'Staff Reminder Email [persian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1110, 'contract', 'contract-comment-to-client', 'persian', 'New Comment  (Sent to Customer Contacts) [persian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1111, 'contract', 'contract-comment-to-admin', 'persian', 'New Comment (Sent to Staff)  [persian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1112, 'subscriptions', 'send-subscription', 'persian', 'Send Subscription to Customer [persian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1113, 'subscriptions', 'subscription-payment-failed', 'persian', 'Subscription Payment Failed [persian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1114, 'subscriptions', 'subscription-canceled', 'persian', 'Subscription Canceled (Sent to customer primary contact) [persian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1115, 'subscriptions', 'subscription-payment-succeeded', 'persian', 'Subscription Payment Succeeded (Sent to customer primary contact) [persian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1116, 'contract', 'contract-expiration-to-staff', 'persian', 'Contract Expiration Reminder (Sent to Staff) [persian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1117, 'gdpr', 'gdpr-removal-request', 'persian', 'Removal Request From Contact (Sent to administrators) [persian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1118, 'gdpr', 'gdpr-removal-request-lead', 'persian', 'Removal Request From Lead (Sent to administrators) [persian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1119, 'client', 'client-registration-confirmed', 'persian', 'Customer Registration Confirmed [persian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1120, 'contract', 'contract-signed-to-staff', 'persian', 'Contract Signed (Sent to Staff) [persian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1121, 'subscriptions', 'customer-subscribed-to-staff', 'persian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [persian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1122, 'client', 'contact-verification-email', 'persian', 'Email Verification (Sent to Contact After Registration) [persian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1123, 'client', 'new-customer-profile-file-uploaded-to-staff', 'persian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [persian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1124, 'staff', 'event-notification-to-staff', 'persian', 'Event Notification (Calendar) [persian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1125, 'subscriptions', 'subscription-payment-requires-action', 'persian', 'Credit Card Authorization Required - SCA [persian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1126, 'client', 'new-client-created', 'turkish', 'New Contact Added/Registered (Welcome Email) [turkish]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1127, 'invoice', 'invoice-send-to-client', 'turkish', 'Send Invoice to Customer [turkish]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1128, 'ticket', 'new-ticket-opened-admin', 'turkish', 'New Ticket Opened (Opened by Staff, Sent to Customer) [turkish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1129, 'ticket', 'ticket-reply', 'turkish', 'Ticket Reply (Sent to Customer) [turkish]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1130, 'ticket', 'ticket-autoresponse', 'turkish', 'New Ticket Opened - Autoresponse [turkish]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1131, 'invoice', 'invoice-payment-recorded', 'turkish', 'Invoice Payment Recorded (Sent to Customer) [turkish]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1132, 'invoice', 'invoice-overdue-notice', 'turkish', 'Invoice Overdue Notice [turkish]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1133, 'invoice', 'invoice-already-send', 'turkish', 'Invoice Already Sent to Customer [turkish]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1134, 'ticket', 'new-ticket-created-staff', 'turkish', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [turkish]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1135, 'estimate', 'estimate-send-to-client', 'turkish', 'Send Estimate to Customer [turkish]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1136, 'ticket', 'ticket-reply-to-admin', 'turkish', 'Ticket Reply (Sent to Staff) [turkish]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1137, 'estimate', 'estimate-already-send', 'turkish', 'Estimate Already Sent to Customer [turkish]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1138, 'contract', 'contract-expiration', 'turkish', 'Contract Expiration Reminder (Sent to Customer Contacts) [turkish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1139, 'tasks', 'task-assigned', 'turkish', 'New Task Assigned (Sent to Staff) [turkish]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1140, 'tasks', 'task-added-as-follower', 'turkish', 'Staff Member Added as Follower on Task (Sent to Staff) [turkish]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1141, 'tasks', 'task-commented', 'turkish', 'New Comment on Task (Sent to Staff) [turkish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1142, 'tasks', 'task-added-attachment', 'turkish', 'New Attachment(s) on Task (Sent to Staff) [turkish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1143, 'estimate', 'estimate-declined-to-staff', 'turkish', 'Estimate Declined (Sent to Staff) [turkish]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1144, 'estimate', 'estimate-accepted-to-staff', 'turkish', 'Estimate Accepted (Sent to Staff) [turkish]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1145, 'proposals', 'proposal-client-accepted', 'turkish', 'Customer Action - Accepted (Sent to Staff) [turkish]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1146, 'proposals', 'proposal-send-to-customer', 'turkish', 'Send Proposal to Customer [turkish]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1147, 'proposals', 'proposal-client-declined', 'turkish', 'Customer Action - Declined (Sent to Staff) [turkish]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1148, 'proposals', 'proposal-client-thank-you', 'turkish', 'Thank You Email (Sent to Customer After Accept) [turkish]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1149, 'proposals', 'proposal-comment-to-client', 'turkish', 'New Comment  (Sent to Customer/Lead) [turkish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1150, 'proposals', 'proposal-comment-to-admin', 'turkish', 'New Comment (Sent to Staff)  [turkish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1151, 'estimate', 'estimate-thank-you-to-customer', 'turkish', 'Thank You Email (Sent to Customer After Accept) [turkish]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1152, 'tasks', 'task-deadline-notification', 'turkish', 'Task Deadline Reminder - Sent to Assigned Members [turkish]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1153, 'contract', 'send-contract', 'turkish', 'Send Contract to Customer [turkish]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1154, 'invoice', 'invoice-payment-recorded-to-staff', 'turkish', 'Invoice Payment Recorded (Sent to Staff) [turkish]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1155, 'ticket', 'auto-close-ticket', 'turkish', 'Auto Close Ticket [turkish]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1156, 'project', 'new-project-discussion-created-to-staff', 'turkish', 'New Project Discussion (Sent to Project Members) [turkish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1157, 'project', 'new-project-discussion-created-to-customer', 'turkish', 'New Project Discussion (Sent to Customer Contacts) [turkish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1158, 'project', 'new-project-file-uploaded-to-customer', 'turkish', 'New Project File(s) Uploaded (Sent to Customer Contacts) [turkish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1159, 'project', 'new-project-file-uploaded-to-staff', 'turkish', 'New Project File(s) Uploaded (Sent to Project Members) [turkish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1160, 'project', 'new-project-discussion-comment-to-customer', 'turkish', 'New Discussion Comment  (Sent to Customer Contacts) [turkish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1161, 'project', 'new-project-discussion-comment-to-staff', 'turkish', 'New Discussion Comment (Sent to Project Members) [turkish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1162, 'project', 'staff-added-as-project-member', 'turkish', 'Staff Added as Project Member [turkish]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1163, 'estimate', 'estimate-expiry-reminder', 'turkish', 'Estimate Expiration Reminder [turkish]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1164, 'proposals', 'proposal-expiry-reminder', 'turkish', 'Proposal Expiration Reminder [turkish]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1165, 'staff', 'new-staff-created', 'turkish', 'New Staff Created (Welcome Email) [turkish]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1166, 'client', 'contact-forgot-password', 'turkish', 'Forgot Password [turkish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1167, 'client', 'contact-password-reseted', 'turkish', 'Password Reset - Confirmation [turkish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1168, 'client', 'contact-set-password', 'turkish', 'Set New Password [turkish]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1169, 'staff', 'staff-forgot-password', 'turkish', 'Forgot Password [turkish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1170, 'staff', 'staff-password-reseted', 'turkish', 'Password Reset - Confirmation [turkish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1171, 'project', 'assigned-to-project', 'turkish', 'New Project Created (Sent to Customer Contacts) [turkish]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1172, 'tasks', 'task-added-attachment-to-contacts', 'turkish', 'New Attachment(s) on Task (Sent to Customer Contacts) [turkish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1173, 'tasks', 'task-commented-to-contacts', 'turkish', 'New Comment on Task (Sent to Customer Contacts) [turkish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1174, 'leads', 'new-lead-assigned', 'turkish', 'New Lead Assigned to Staff Member [turkish]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1175, 'client', 'client-statement', 'turkish', 'Statement - Account Summary [turkish]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1176, 'ticket', 'ticket-assigned-to-admin', 'turkish', 'New Ticket Assigned (Sent to Staff) [turkish]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1177, 'client', 'new-client-registered-to-admin', 'turkish', 'New Customer Registration (Sent to admins) [turkish]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1178, 'leads', 'new-web-to-lead-form-submitted', 'turkish', 'Web to lead form submitted - Sent to lead [turkish]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1179, 'staff', 'two-factor-authentication', 'turkish', 'Two Factor Authentication [turkish]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1180, 'project', 'project-finished-to-customer', 'turkish', 'Project Marked as Finished (Sent to Customer Contacts) [turkish]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1181, 'credit_note', 'credit-note-send-to-client', 'turkish', 'Send Credit Note To Email [turkish]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1182, 'tasks', 'task-status-change-to-staff', 'turkish', 'Task Status Changed (Sent to Staff) [turkish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1183, 'tasks', 'task-status-change-to-contacts', 'turkish', 'Task Status Changed (Sent to Customer Contacts) [turkish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1184, 'staff', 'reminder-email-staff', 'turkish', 'Staff Reminder Email [turkish]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1185, 'contract', 'contract-comment-to-client', 'turkish', 'New Comment  (Sent to Customer Contacts) [turkish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1186, 'contract', 'contract-comment-to-admin', 'turkish', 'New Comment (Sent to Staff)  [turkish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1187, 'subscriptions', 'send-subscription', 'turkish', 'Send Subscription to Customer [turkish]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1188, 'subscriptions', 'subscription-payment-failed', 'turkish', 'Subscription Payment Failed [turkish]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1189, 'subscriptions', 'subscription-canceled', 'turkish', 'Subscription Canceled (Sent to customer primary contact) [turkish]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1190, 'subscriptions', 'subscription-payment-succeeded', 'turkish', 'Subscription Payment Succeeded (Sent to customer primary contact) [turkish]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1191, 'contract', 'contract-expiration-to-staff', 'turkish', 'Contract Expiration Reminder (Sent to Staff) [turkish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1192, 'gdpr', 'gdpr-removal-request', 'turkish', 'Removal Request From Contact (Sent to administrators) [turkish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1193, 'gdpr', 'gdpr-removal-request-lead', 'turkish', 'Removal Request From Lead (Sent to administrators) [turkish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1194, 'client', 'client-registration-confirmed', 'turkish', 'Customer Registration Confirmed [turkish]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1195, 'contract', 'contract-signed-to-staff', 'turkish', 'Contract Signed (Sent to Staff) [turkish]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1196, 'subscriptions', 'customer-subscribed-to-staff', 'turkish', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [turkish]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1197, 'client', 'contact-verification-email', 'turkish', 'Email Verification (Sent to Contact After Registration) [turkish]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1198, 'client', 'new-customer-profile-file-uploaded-to-staff', 'turkish', 'New Customer Profile File(s) Uploaded (Sent to Staff) [turkish]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1199, 'staff', 'event-notification-to-staff', 'turkish', 'Event Notification (Calendar) [turkish]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1200, 'subscriptions', 'subscription-payment-requires-action', 'turkish', 'Credit Card Authorization Required - SCA [turkish]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1201, 'client', 'new-client-created', 'greek', 'New Contact Added/Registered (Welcome Email) [greek]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1202, 'invoice', 'invoice-send-to-client', 'greek', 'Send Invoice to Customer [greek]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1203, 'ticket', 'new-ticket-opened-admin', 'greek', 'New Ticket Opened (Opened by Staff, Sent to Customer) [greek]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1204, 'ticket', 'ticket-reply', 'greek', 'Ticket Reply (Sent to Customer) [greek]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1205, 'ticket', 'ticket-autoresponse', 'greek', 'New Ticket Opened - Autoresponse [greek]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1206, 'invoice', 'invoice-payment-recorded', 'greek', 'Invoice Payment Recorded (Sent to Customer) [greek]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1207, 'invoice', 'invoice-overdue-notice', 'greek', 'Invoice Overdue Notice [greek]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1208, 'invoice', 'invoice-already-send', 'greek', 'Invoice Already Sent to Customer [greek]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1209, 'ticket', 'new-ticket-created-staff', 'greek', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [greek]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1210, 'estimate', 'estimate-send-to-client', 'greek', 'Send Estimate to Customer [greek]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1211, 'ticket', 'ticket-reply-to-admin', 'greek', 'Ticket Reply (Sent to Staff) [greek]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1212, 'estimate', 'estimate-already-send', 'greek', 'Estimate Already Sent to Customer [greek]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1213, 'contract', 'contract-expiration', 'greek', 'Contract Expiration Reminder (Sent to Customer Contacts) [greek]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1214, 'tasks', 'task-assigned', 'greek', 'New Task Assigned (Sent to Staff) [greek]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1215, 'tasks', 'task-added-as-follower', 'greek', 'Staff Member Added as Follower on Task (Sent to Staff) [greek]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1216, 'tasks', 'task-commented', 'greek', 'New Comment on Task (Sent to Staff) [greek]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1217, 'tasks', 'task-added-attachment', 'greek', 'New Attachment(s) on Task (Sent to Staff) [greek]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1218, 'estimate', 'estimate-declined-to-staff', 'greek', 'Estimate Declined (Sent to Staff) [greek]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1219, 'estimate', 'estimate-accepted-to-staff', 'greek', 'Estimate Accepted (Sent to Staff) [greek]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1220, 'proposals', 'proposal-client-accepted', 'greek', 'Customer Action - Accepted (Sent to Staff) [greek]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1221, 'proposals', 'proposal-send-to-customer', 'greek', 'Send Proposal to Customer [greek]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1222, 'proposals', 'proposal-client-declined', 'greek', 'Customer Action - Declined (Sent to Staff) [greek]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1223, 'proposals', 'proposal-client-thank-you', 'greek', 'Thank You Email (Sent to Customer After Accept) [greek]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1224, 'proposals', 'proposal-comment-to-client', 'greek', 'New Comment  (Sent to Customer/Lead) [greek]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1225, 'proposals', 'proposal-comment-to-admin', 'greek', 'New Comment (Sent to Staff)  [greek]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1226, 'estimate', 'estimate-thank-you-to-customer', 'greek', 'Thank You Email (Sent to Customer After Accept) [greek]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1227, 'tasks', 'task-deadline-notification', 'greek', 'Task Deadline Reminder - Sent to Assigned Members [greek]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1228, 'contract', 'send-contract', 'greek', 'Send Contract to Customer [greek]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1229, 'invoice', 'invoice-payment-recorded-to-staff', 'greek', 'Invoice Payment Recorded (Sent to Staff) [greek]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1230, 'ticket', 'auto-close-ticket', 'greek', 'Auto Close Ticket [greek]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1231, 'project', 'new-project-discussion-created-to-staff', 'greek', 'New Project Discussion (Sent to Project Members) [greek]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1232, 'project', 'new-project-discussion-created-to-customer', 'greek', 'New Project Discussion (Sent to Customer Contacts) [greek]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1233, 'project', 'new-project-file-uploaded-to-customer', 'greek', 'New Project File(s) Uploaded (Sent to Customer Contacts) [greek]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1234, 'project', 'new-project-file-uploaded-to-staff', 'greek', 'New Project File(s) Uploaded (Sent to Project Members) [greek]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1235, 'project', 'new-project-discussion-comment-to-customer', 'greek', 'New Discussion Comment  (Sent to Customer Contacts) [greek]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1236, 'project', 'new-project-discussion-comment-to-staff', 'greek', 'New Discussion Comment (Sent to Project Members) [greek]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1237, 'project', 'staff-added-as-project-member', 'greek', 'Staff Added as Project Member [greek]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1238, 'estimate', 'estimate-expiry-reminder', 'greek', 'Estimate Expiration Reminder [greek]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1239, 'proposals', 'proposal-expiry-reminder', 'greek', 'Proposal Expiration Reminder [greek]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1240, 'staff', 'new-staff-created', 'greek', 'New Staff Created (Welcome Email) [greek]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1241, 'client', 'contact-forgot-password', 'greek', 'Forgot Password [greek]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1242, 'client', 'contact-password-reseted', 'greek', 'Password Reset - Confirmation [greek]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1243, 'client', 'contact-set-password', 'greek', 'Set New Password [greek]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1244, 'staff', 'staff-forgot-password', 'greek', 'Forgot Password [greek]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1245, 'staff', 'staff-password-reseted', 'greek', 'Password Reset - Confirmation [greek]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1246, 'project', 'assigned-to-project', 'greek', 'New Project Created (Sent to Customer Contacts) [greek]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1247, 'tasks', 'task-added-attachment-to-contacts', 'greek', 'New Attachment(s) on Task (Sent to Customer Contacts) [greek]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1248, 'tasks', 'task-commented-to-contacts', 'greek', 'New Comment on Task (Sent to Customer Contacts) [greek]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1249, 'leads', 'new-lead-assigned', 'greek', 'New Lead Assigned to Staff Member [greek]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1250, 'client', 'client-statement', 'greek', 'Statement - Account Summary [greek]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1251, 'ticket', 'ticket-assigned-to-admin', 'greek', 'New Ticket Assigned (Sent to Staff) [greek]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1252, 'client', 'new-client-registered-to-admin', 'greek', 'New Customer Registration (Sent to admins) [greek]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1253, 'leads', 'new-web-to-lead-form-submitted', 'greek', 'Web to lead form submitted - Sent to lead [greek]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1254, 'staff', 'two-factor-authentication', 'greek', 'Two Factor Authentication [greek]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1255, 'project', 'project-finished-to-customer', 'greek', 'Project Marked as Finished (Sent to Customer Contacts) [greek]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1256, 'credit_note', 'credit-note-send-to-client', 'greek', 'Send Credit Note To Email [greek]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1257, 'tasks', 'task-status-change-to-staff', 'greek', 'Task Status Changed (Sent to Staff) [greek]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1258, 'tasks', 'task-status-change-to-contacts', 'greek', 'Task Status Changed (Sent to Customer Contacts) [greek]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1259, 'staff', 'reminder-email-staff', 'greek', 'Staff Reminder Email [greek]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1260, 'contract', 'contract-comment-to-client', 'greek', 'New Comment  (Sent to Customer Contacts) [greek]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1261, 'contract', 'contract-comment-to-admin', 'greek', 'New Comment (Sent to Staff)  [greek]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1262, 'subscriptions', 'send-subscription', 'greek', 'Send Subscription to Customer [greek]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1263, 'subscriptions', 'subscription-payment-failed', 'greek', 'Subscription Payment Failed [greek]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1264, 'subscriptions', 'subscription-canceled', 'greek', 'Subscription Canceled (Sent to customer primary contact) [greek]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1265, 'subscriptions', 'subscription-payment-succeeded', 'greek', 'Subscription Payment Succeeded (Sent to customer primary contact) [greek]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1266, 'contract', 'contract-expiration-to-staff', 'greek', 'Contract Expiration Reminder (Sent to Staff) [greek]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1267, 'gdpr', 'gdpr-removal-request', 'greek', 'Removal Request From Contact (Sent to administrators) [greek]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1268, 'gdpr', 'gdpr-removal-request-lead', 'greek', 'Removal Request From Lead (Sent to administrators) [greek]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1269, 'client', 'client-registration-confirmed', 'greek', 'Customer Registration Confirmed [greek]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1270, 'contract', 'contract-signed-to-staff', 'greek', 'Contract Signed (Sent to Staff) [greek]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1271, 'subscriptions', 'customer-subscribed-to-staff', 'greek', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [greek]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1272, 'client', 'contact-verification-email', 'greek', 'Email Verification (Sent to Contact After Registration) [greek]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1273, 'client', 'new-customer-profile-file-uploaded-to-staff', 'greek', 'New Customer Profile File(s) Uploaded (Sent to Staff) [greek]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1274, 'staff', 'event-notification-to-staff', 'greek', 'Event Notification (Calendar) [greek]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1275, 'subscriptions', 'subscription-payment-requires-action', 'greek', 'Credit Card Authorization Required - SCA [greek]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1276, 'client', 'new-client-created', 'portuguese', 'New Contact Added/Registered (Welcome Email) [portuguese]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1277, 'invoice', 'invoice-send-to-client', 'portuguese', 'Send Invoice to Customer [portuguese]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1278, 'ticket', 'new-ticket-opened-admin', 'portuguese', 'New Ticket Opened (Opened by Staff, Sent to Customer) [portuguese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1279, 'ticket', 'ticket-reply', 'portuguese', 'Ticket Reply (Sent to Customer) [portuguese]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1280, 'ticket', 'ticket-autoresponse', 'portuguese', 'New Ticket Opened - Autoresponse [portuguese]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1281, 'invoice', 'invoice-payment-recorded', 'portuguese', 'Invoice Payment Recorded (Sent to Customer) [portuguese]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1282, 'invoice', 'invoice-overdue-notice', 'portuguese', 'Invoice Overdue Notice [portuguese]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1283, 'invoice', 'invoice-already-send', 'portuguese', 'Invoice Already Sent to Customer [portuguese]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1284, 'ticket', 'new-ticket-created-staff', 'portuguese', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [portuguese]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1285, 'estimate', 'estimate-send-to-client', 'portuguese', 'Send Estimate to Customer [portuguese]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1286, 'ticket', 'ticket-reply-to-admin', 'portuguese', 'Ticket Reply (Sent to Staff) [portuguese]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1287, 'estimate', 'estimate-already-send', 'portuguese', 'Estimate Already Sent to Customer [portuguese]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1288, 'contract', 'contract-expiration', 'portuguese', 'Contract Expiration Reminder (Sent to Customer Contacts) [portuguese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1289, 'tasks', 'task-assigned', 'portuguese', 'New Task Assigned (Sent to Staff) [portuguese]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1290, 'tasks', 'task-added-as-follower', 'portuguese', 'Staff Member Added as Follower on Task (Sent to Staff) [portuguese]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1291, 'tasks', 'task-commented', 'portuguese', 'New Comment on Task (Sent to Staff) [portuguese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1292, 'tasks', 'task-added-attachment', 'portuguese', 'New Attachment(s) on Task (Sent to Staff) [portuguese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1293, 'estimate', 'estimate-declined-to-staff', 'portuguese', 'Estimate Declined (Sent to Staff) [portuguese]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1294, 'estimate', 'estimate-accepted-to-staff', 'portuguese', 'Estimate Accepted (Sent to Staff) [portuguese]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1295, 'proposals', 'proposal-client-accepted', 'portuguese', 'Customer Action - Accepted (Sent to Staff) [portuguese]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1296, 'proposals', 'proposal-send-to-customer', 'portuguese', 'Send Proposal to Customer [portuguese]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1297, 'proposals', 'proposal-client-declined', 'portuguese', 'Customer Action - Declined (Sent to Staff) [portuguese]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1298, 'proposals', 'proposal-client-thank-you', 'portuguese', 'Thank You Email (Sent to Customer After Accept) [portuguese]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1299, 'proposals', 'proposal-comment-to-client', 'portuguese', 'New Comment  (Sent to Customer/Lead) [portuguese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1300, 'proposals', 'proposal-comment-to-admin', 'portuguese', 'New Comment (Sent to Staff)  [portuguese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1301, 'estimate', 'estimate-thank-you-to-customer', 'portuguese', 'Thank You Email (Sent to Customer After Accept) [portuguese]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1302, 'tasks', 'task-deadline-notification', 'portuguese', 'Task Deadline Reminder - Sent to Assigned Members [portuguese]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1303, 'contract', 'send-contract', 'portuguese', 'Send Contract to Customer [portuguese]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1304, 'invoice', 'invoice-payment-recorded-to-staff', 'portuguese', 'Invoice Payment Recorded (Sent to Staff) [portuguese]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1305, 'ticket', 'auto-close-ticket', 'portuguese', 'Auto Close Ticket [portuguese]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1306, 'project', 'new-project-discussion-created-to-staff', 'portuguese', 'New Project Discussion (Sent to Project Members) [portuguese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1307, 'project', 'new-project-discussion-created-to-customer', 'portuguese', 'New Project Discussion (Sent to Customer Contacts) [portuguese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1308, 'project', 'new-project-file-uploaded-to-customer', 'portuguese', 'New Project File(s) Uploaded (Sent to Customer Contacts) [portuguese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1309, 'project', 'new-project-file-uploaded-to-staff', 'portuguese', 'New Project File(s) Uploaded (Sent to Project Members) [portuguese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1310, 'project', 'new-project-discussion-comment-to-customer', 'portuguese', 'New Discussion Comment  (Sent to Customer Contacts) [portuguese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1311, 'project', 'new-project-discussion-comment-to-staff', 'portuguese', 'New Discussion Comment (Sent to Project Members) [portuguese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1312, 'project', 'staff-added-as-project-member', 'portuguese', 'Staff Added as Project Member [portuguese]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1313, 'estimate', 'estimate-expiry-reminder', 'portuguese', 'Estimate Expiration Reminder [portuguese]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1314, 'proposals', 'proposal-expiry-reminder', 'portuguese', 'Proposal Expiration Reminder [portuguese]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1315, 'staff', 'new-staff-created', 'portuguese', 'New Staff Created (Welcome Email) [portuguese]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1316, 'client', 'contact-forgot-password', 'portuguese', 'Forgot Password [portuguese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1317, 'client', 'contact-password-reseted', 'portuguese', 'Password Reset - Confirmation [portuguese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1318, 'client', 'contact-set-password', 'portuguese', 'Set New Password [portuguese]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1319, 'staff', 'staff-forgot-password', 'portuguese', 'Forgot Password [portuguese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1320, 'staff', 'staff-password-reseted', 'portuguese', 'Password Reset - Confirmation [portuguese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1321, 'project', 'assigned-to-project', 'portuguese', 'New Project Created (Sent to Customer Contacts) [portuguese]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1322, 'tasks', 'task-added-attachment-to-contacts', 'portuguese', 'New Attachment(s) on Task (Sent to Customer Contacts) [portuguese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1323, 'tasks', 'task-commented-to-contacts', 'portuguese', 'New Comment on Task (Sent to Customer Contacts) [portuguese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1324, 'leads', 'new-lead-assigned', 'portuguese', 'New Lead Assigned to Staff Member [portuguese]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1325, 'client', 'client-statement', 'portuguese', 'Statement - Account Summary [portuguese]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1326, 'ticket', 'ticket-assigned-to-admin', 'portuguese', 'New Ticket Assigned (Sent to Staff) [portuguese]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1327, 'client', 'new-client-registered-to-admin', 'portuguese', 'New Customer Registration (Sent to admins) [portuguese]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1328, 'leads', 'new-web-to-lead-form-submitted', 'portuguese', 'Web to lead form submitted - Sent to lead [portuguese]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1329, 'staff', 'two-factor-authentication', 'portuguese', 'Two Factor Authentication [portuguese]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1330, 'project', 'project-finished-to-customer', 'portuguese', 'Project Marked as Finished (Sent to Customer Contacts) [portuguese]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1331, 'credit_note', 'credit-note-send-to-client', 'portuguese', 'Send Credit Note To Email [portuguese]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1332, 'tasks', 'task-status-change-to-staff', 'portuguese', 'Task Status Changed (Sent to Staff) [portuguese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1333, 'tasks', 'task-status-change-to-contacts', 'portuguese', 'Task Status Changed (Sent to Customer Contacts) [portuguese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1334, 'staff', 'reminder-email-staff', 'portuguese', 'Staff Reminder Email [portuguese]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1335, 'contract', 'contract-comment-to-client', 'portuguese', 'New Comment  (Sent to Customer Contacts) [portuguese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1336, 'contract', 'contract-comment-to-admin', 'portuguese', 'New Comment (Sent to Staff)  [portuguese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1337, 'subscriptions', 'send-subscription', 'portuguese', 'Send Subscription to Customer [portuguese]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1338, 'subscriptions', 'subscription-payment-failed', 'portuguese', 'Subscription Payment Failed [portuguese]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1339, 'subscriptions', 'subscription-canceled', 'portuguese', 'Subscription Canceled (Sent to customer primary contact) [portuguese]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1340, 'subscriptions', 'subscription-payment-succeeded', 'portuguese', 'Subscription Payment Succeeded (Sent to customer primary contact) [portuguese]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1341, 'contract', 'contract-expiration-to-staff', 'portuguese', 'Contract Expiration Reminder (Sent to Staff) [portuguese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1342, 'gdpr', 'gdpr-removal-request', 'portuguese', 'Removal Request From Contact (Sent to administrators) [portuguese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1343, 'gdpr', 'gdpr-removal-request-lead', 'portuguese', 'Removal Request From Lead (Sent to administrators) [portuguese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1344, 'client', 'client-registration-confirmed', 'portuguese', 'Customer Registration Confirmed [portuguese]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1345, 'contract', 'contract-signed-to-staff', 'portuguese', 'Contract Signed (Sent to Staff) [portuguese]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1346, 'subscriptions', 'customer-subscribed-to-staff', 'portuguese', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [portuguese]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1347, 'client', 'contact-verification-email', 'portuguese', 'Email Verification (Sent to Contact After Registration) [portuguese]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1348, 'client', 'new-customer-profile-file-uploaded-to-staff', 'portuguese', 'New Customer Profile File(s) Uploaded (Sent to Staff) [portuguese]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1349, 'staff', 'event-notification-to-staff', 'portuguese', 'Event Notification (Calendar) [portuguese]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1350, 'subscriptions', 'subscription-payment-requires-action', 'portuguese', 'Credit Card Authorization Required - SCA [portuguese]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1351, 'client', 'new-client-created', 'ukrainian', 'New Contact Added/Registered (Welcome Email) [ukrainian]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1352, 'invoice', 'invoice-send-to-client', 'ukrainian', 'Send Invoice to Customer [ukrainian]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1353, 'ticket', 'new-ticket-opened-admin', 'ukrainian', 'New Ticket Opened (Opened by Staff, Sent to Customer) [ukrainian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1354, 'ticket', 'ticket-reply', 'ukrainian', 'Ticket Reply (Sent to Customer) [ukrainian]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1355, 'ticket', 'ticket-autoresponse', 'ukrainian', 'New Ticket Opened - Autoresponse [ukrainian]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1356, 'invoice', 'invoice-payment-recorded', 'ukrainian', 'Invoice Payment Recorded (Sent to Customer) [ukrainian]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1357, 'invoice', 'invoice-overdue-notice', 'ukrainian', 'Invoice Overdue Notice [ukrainian]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1358, 'invoice', 'invoice-already-send', 'ukrainian', 'Invoice Already Sent to Customer [ukrainian]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1359, 'ticket', 'new-ticket-created-staff', 'ukrainian', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [ukrainian]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1360, 'estimate', 'estimate-send-to-client', 'ukrainian', 'Send Estimate to Customer [ukrainian]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1361, 'ticket', 'ticket-reply-to-admin', 'ukrainian', 'Ticket Reply (Sent to Staff) [ukrainian]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1362, 'estimate', 'estimate-already-send', 'ukrainian', 'Estimate Already Sent to Customer [ukrainian]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1363, 'contract', 'contract-expiration', 'ukrainian', 'Contract Expiration Reminder (Sent to Customer Contacts) [ukrainian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1364, 'tasks', 'task-assigned', 'ukrainian', 'New Task Assigned (Sent to Staff) [ukrainian]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1365, 'tasks', 'task-added-as-follower', 'ukrainian', 'Staff Member Added as Follower on Task (Sent to Staff) [ukrainian]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1366, 'tasks', 'task-commented', 'ukrainian', 'New Comment on Task (Sent to Staff) [ukrainian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1367, 'tasks', 'task-added-attachment', 'ukrainian', 'New Attachment(s) on Task (Sent to Staff) [ukrainian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1368, 'estimate', 'estimate-declined-to-staff', 'ukrainian', 'Estimate Declined (Sent to Staff) [ukrainian]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1369, 'estimate', 'estimate-accepted-to-staff', 'ukrainian', 'Estimate Accepted (Sent to Staff) [ukrainian]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1370, 'proposals', 'proposal-client-accepted', 'ukrainian', 'Customer Action - Accepted (Sent to Staff) [ukrainian]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1371, 'proposals', 'proposal-send-to-customer', 'ukrainian', 'Send Proposal to Customer [ukrainian]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1372, 'proposals', 'proposal-client-declined', 'ukrainian', 'Customer Action - Declined (Sent to Staff) [ukrainian]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1373, 'proposals', 'proposal-client-thank-you', 'ukrainian', 'Thank You Email (Sent to Customer After Accept) [ukrainian]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1374, 'proposals', 'proposal-comment-to-client', 'ukrainian', 'New Comment  (Sent to Customer/Lead) [ukrainian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1375, 'proposals', 'proposal-comment-to-admin', 'ukrainian', 'New Comment (Sent to Staff)  [ukrainian]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1376, 'estimate', 'estimate-thank-you-to-customer', 'ukrainian', 'Thank You Email (Sent to Customer After Accept) [ukrainian]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1377, 'tasks', 'task-deadline-notification', 'ukrainian', 'Task Deadline Reminder - Sent to Assigned Members [ukrainian]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1378, 'contract', 'send-contract', 'ukrainian', 'Send Contract to Customer [ukrainian]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1379, 'invoice', 'invoice-payment-recorded-to-staff', 'ukrainian', 'Invoice Payment Recorded (Sent to Staff) [ukrainian]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1380, 'ticket', 'auto-close-ticket', 'ukrainian', 'Auto Close Ticket [ukrainian]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1381, 'project', 'new-project-discussion-created-to-staff', 'ukrainian', 'New Project Discussion (Sent to Project Members) [ukrainian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1382, 'project', 'new-project-discussion-created-to-customer', 'ukrainian', 'New Project Discussion (Sent to Customer Contacts) [ukrainian]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1383, 'project', 'new-project-file-uploaded-to-customer', 'ukrainian', 'New Project File(s) Uploaded (Sent to Customer Contacts) [ukrainian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1384, 'project', 'new-project-file-uploaded-to-staff', 'ukrainian', 'New Project File(s) Uploaded (Sent to Project Members) [ukrainian]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1385, 'project', 'new-project-discussion-comment-to-customer', 'ukrainian', 'New Discussion Comment  (Sent to Customer Contacts) [ukrainian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1386, 'project', 'new-project-discussion-comment-to-staff', 'ukrainian', 'New Discussion Comment (Sent to Project Members) [ukrainian]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1387, 'project', 'staff-added-as-project-member', 'ukrainian', 'Staff Added as Project Member [ukrainian]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1388, 'estimate', 'estimate-expiry-reminder', 'ukrainian', 'Estimate Expiration Reminder [ukrainian]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1389, 'proposals', 'proposal-expiry-reminder', 'ukrainian', 'Proposal Expiration Reminder [ukrainian]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1390, 'staff', 'new-staff-created', 'ukrainian', 'New Staff Created (Welcome Email) [ukrainian]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1391, 'client', 'contact-forgot-password', 'ukrainian', 'Forgot Password [ukrainian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1392, 'client', 'contact-password-reseted', 'ukrainian', 'Password Reset - Confirmation [ukrainian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1393, 'client', 'contact-set-password', 'ukrainian', 'Set New Password [ukrainian]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1394, 'staff', 'staff-forgot-password', 'ukrainian', 'Forgot Password [ukrainian]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1395, 'staff', 'staff-password-reseted', 'ukrainian', 'Password Reset - Confirmation [ukrainian]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1396, 'project', 'assigned-to-project', 'ukrainian', 'New Project Created (Sent to Customer Contacts) [ukrainian]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1397, 'tasks', 'task-added-attachment-to-contacts', 'ukrainian', 'New Attachment(s) on Task (Sent to Customer Contacts) [ukrainian]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1398, 'tasks', 'task-commented-to-contacts', 'ukrainian', 'New Comment on Task (Sent to Customer Contacts) [ukrainian]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1399, 'leads', 'new-lead-assigned', 'ukrainian', 'New Lead Assigned to Staff Member [ukrainian]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1400, 'client', 'client-statement', 'ukrainian', 'Statement - Account Summary [ukrainian]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1401, 'ticket', 'ticket-assigned-to-admin', 'ukrainian', 'New Ticket Assigned (Sent to Staff) [ukrainian]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1402, 'client', 'new-client-registered-to-admin', 'ukrainian', 'New Customer Registration (Sent to admins) [ukrainian]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1403, 'leads', 'new-web-to-lead-form-submitted', 'ukrainian', 'Web to lead form submitted - Sent to lead [ukrainian]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1404, 'staff', 'two-factor-authentication', 'ukrainian', 'Two Factor Authentication [ukrainian]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1405, 'project', 'project-finished-to-customer', 'ukrainian', 'Project Marked as Finished (Sent to Customer Contacts) [ukrainian]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1406, 'credit_note', 'credit-note-send-to-client', 'ukrainian', 'Send Credit Note To Email [ukrainian]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1407, 'tasks', 'task-status-change-to-staff', 'ukrainian', 'Task Status Changed (Sent to Staff) [ukrainian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1408, 'tasks', 'task-status-change-to-contacts', 'ukrainian', 'Task Status Changed (Sent to Customer Contacts) [ukrainian]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1409, 'staff', 'reminder-email-staff', 'ukrainian', 'Staff Reminder Email [ukrainian]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1410, 'contract', 'contract-comment-to-client', 'ukrainian', 'New Comment  (Sent to Customer Contacts) [ukrainian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1411, 'contract', 'contract-comment-to-admin', 'ukrainian', 'New Comment (Sent to Staff)  [ukrainian]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1412, 'subscriptions', 'send-subscription', 'ukrainian', 'Send Subscription to Customer [ukrainian]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1413, 'subscriptions', 'subscription-payment-failed', 'ukrainian', 'Subscription Payment Failed [ukrainian]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1414, 'subscriptions', 'subscription-canceled', 'ukrainian', 'Subscription Canceled (Sent to customer primary contact) [ukrainian]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1415, 'subscriptions', 'subscription-payment-succeeded', 'ukrainian', 'Subscription Payment Succeeded (Sent to customer primary contact) [ukrainian]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1416, 'contract', 'contract-expiration-to-staff', 'ukrainian', 'Contract Expiration Reminder (Sent to Staff) [ukrainian]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1417, 'gdpr', 'gdpr-removal-request', 'ukrainian', 'Removal Request From Contact (Sent to administrators) [ukrainian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1418, 'gdpr', 'gdpr-removal-request-lead', 'ukrainian', 'Removal Request From Lead (Sent to administrators) [ukrainian]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1419, 'client', 'client-registration-confirmed', 'ukrainian', 'Customer Registration Confirmed [ukrainian]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1420, 'contract', 'contract-signed-to-staff', 'ukrainian', 'Contract Signed (Sent to Staff) [ukrainian]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1421, 'subscriptions', 'customer-subscribed-to-staff', 'ukrainian', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [ukrainian]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1422, 'client', 'contact-verification-email', 'ukrainian', 'Email Verification (Sent to Contact After Registration) [ukrainian]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1423, 'client', 'new-customer-profile-file-uploaded-to-staff', 'ukrainian', 'New Customer Profile File(s) Uploaded (Sent to Staff) [ukrainian]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1424, 'staff', 'event-notification-to-staff', 'ukrainian', 'Event Notification (Calendar) [ukrainian]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1425, 'subscriptions', 'subscription-payment-requires-action', 'ukrainian', 'Credit Card Authorization Required - SCA [ukrainian]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1426, 'client', 'new-client-created', 'swedish', 'New Contact Added/Registered (Welcome Email) [swedish]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1427, 'invoice', 'invoice-send-to-client', 'swedish', 'Send Invoice to Customer [swedish]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1428, 'ticket', 'new-ticket-opened-admin', 'swedish', 'New Ticket Opened (Opened by Staff, Sent to Customer) [swedish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1429, 'ticket', 'ticket-reply', 'swedish', 'Ticket Reply (Sent to Customer) [swedish]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1430, 'ticket', 'ticket-autoresponse', 'swedish', 'New Ticket Opened - Autoresponse [swedish]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1431, 'invoice', 'invoice-payment-recorded', 'swedish', 'Invoice Payment Recorded (Sent to Customer) [swedish]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1432, 'invoice', 'invoice-overdue-notice', 'swedish', 'Invoice Overdue Notice [swedish]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1433, 'invoice', 'invoice-already-send', 'swedish', 'Invoice Already Sent to Customer [swedish]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1434, 'ticket', 'new-ticket-created-staff', 'swedish', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [swedish]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1435, 'estimate', 'estimate-send-to-client', 'swedish', 'Send Estimate to Customer [swedish]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1436, 'ticket', 'ticket-reply-to-admin', 'swedish', 'Ticket Reply (Sent to Staff) [swedish]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1437, 'estimate', 'estimate-already-send', 'swedish', 'Estimate Already Sent to Customer [swedish]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1438, 'contract', 'contract-expiration', 'swedish', 'Contract Expiration Reminder (Sent to Customer Contacts) [swedish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1439, 'tasks', 'task-assigned', 'swedish', 'New Task Assigned (Sent to Staff) [swedish]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1440, 'tasks', 'task-added-as-follower', 'swedish', 'Staff Member Added as Follower on Task (Sent to Staff) [swedish]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1441, 'tasks', 'task-commented', 'swedish', 'New Comment on Task (Sent to Staff) [swedish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1442, 'tasks', 'task-added-attachment', 'swedish', 'New Attachment(s) on Task (Sent to Staff) [swedish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1443, 'estimate', 'estimate-declined-to-staff', 'swedish', 'Estimate Declined (Sent to Staff) [swedish]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1444, 'estimate', 'estimate-accepted-to-staff', 'swedish', 'Estimate Accepted (Sent to Staff) [swedish]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1445, 'proposals', 'proposal-client-accepted', 'swedish', 'Customer Action - Accepted (Sent to Staff) [swedish]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1446, 'proposals', 'proposal-send-to-customer', 'swedish', 'Send Proposal to Customer [swedish]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1447, 'proposals', 'proposal-client-declined', 'swedish', 'Customer Action - Declined (Sent to Staff) [swedish]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1448, 'proposals', 'proposal-client-thank-you', 'swedish', 'Thank You Email (Sent to Customer After Accept) [swedish]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1449, 'proposals', 'proposal-comment-to-client', 'swedish', 'New Comment  (Sent to Customer/Lead) [swedish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1450, 'proposals', 'proposal-comment-to-admin', 'swedish', 'New Comment (Sent to Staff)  [swedish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1451, 'estimate', 'estimate-thank-you-to-customer', 'swedish', 'Thank You Email (Sent to Customer After Accept) [swedish]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1452, 'tasks', 'task-deadline-notification', 'swedish', 'Task Deadline Reminder - Sent to Assigned Members [swedish]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1453, 'contract', 'send-contract', 'swedish', 'Send Contract to Customer [swedish]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1454, 'invoice', 'invoice-payment-recorded-to-staff', 'swedish', 'Invoice Payment Recorded (Sent to Staff) [swedish]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1455, 'ticket', 'auto-close-ticket', 'swedish', 'Auto Close Ticket [swedish]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1456, 'project', 'new-project-discussion-created-to-staff', 'swedish', 'New Project Discussion (Sent to Project Members) [swedish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1457, 'project', 'new-project-discussion-created-to-customer', 'swedish', 'New Project Discussion (Sent to Customer Contacts) [swedish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1458, 'project', 'new-project-file-uploaded-to-customer', 'swedish', 'New Project File(s) Uploaded (Sent to Customer Contacts) [swedish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1459, 'project', 'new-project-file-uploaded-to-staff', 'swedish', 'New Project File(s) Uploaded (Sent to Project Members) [swedish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1460, 'project', 'new-project-discussion-comment-to-customer', 'swedish', 'New Discussion Comment  (Sent to Customer Contacts) [swedish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1461, 'project', 'new-project-discussion-comment-to-staff', 'swedish', 'New Discussion Comment (Sent to Project Members) [swedish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1462, 'project', 'staff-added-as-project-member', 'swedish', 'Staff Added as Project Member [swedish]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1463, 'estimate', 'estimate-expiry-reminder', 'swedish', 'Estimate Expiration Reminder [swedish]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1464, 'proposals', 'proposal-expiry-reminder', 'swedish', 'Proposal Expiration Reminder [swedish]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1465, 'staff', 'new-staff-created', 'swedish', 'New Staff Created (Welcome Email) [swedish]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1466, 'client', 'contact-forgot-password', 'swedish', 'Forgot Password [swedish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1467, 'client', 'contact-password-reseted', 'swedish', 'Password Reset - Confirmation [swedish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1468, 'client', 'contact-set-password', 'swedish', 'Set New Password [swedish]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1469, 'staff', 'staff-forgot-password', 'swedish', 'Forgot Password [swedish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1470, 'staff', 'staff-password-reseted', 'swedish', 'Password Reset - Confirmation [swedish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1471, 'project', 'assigned-to-project', 'swedish', 'New Project Created (Sent to Customer Contacts) [swedish]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1472, 'tasks', 'task-added-attachment-to-contacts', 'swedish', 'New Attachment(s) on Task (Sent to Customer Contacts) [swedish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1473, 'tasks', 'task-commented-to-contacts', 'swedish', 'New Comment on Task (Sent to Customer Contacts) [swedish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1474, 'leads', 'new-lead-assigned', 'swedish', 'New Lead Assigned to Staff Member [swedish]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1475, 'client', 'client-statement', 'swedish', 'Statement - Account Summary [swedish]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1476, 'ticket', 'ticket-assigned-to-admin', 'swedish', 'New Ticket Assigned (Sent to Staff) [swedish]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1477, 'client', 'new-client-registered-to-admin', 'swedish', 'New Customer Registration (Sent to admins) [swedish]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1478, 'leads', 'new-web-to-lead-form-submitted', 'swedish', 'Web to lead form submitted - Sent to lead [swedish]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1479, 'staff', 'two-factor-authentication', 'swedish', 'Two Factor Authentication [swedish]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1480, 'project', 'project-finished-to-customer', 'swedish', 'Project Marked as Finished (Sent to Customer Contacts) [swedish]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1481, 'credit_note', 'credit-note-send-to-client', 'swedish', 'Send Credit Note To Email [swedish]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1482, 'tasks', 'task-status-change-to-staff', 'swedish', 'Task Status Changed (Sent to Staff) [swedish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1483, 'tasks', 'task-status-change-to-contacts', 'swedish', 'Task Status Changed (Sent to Customer Contacts) [swedish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1484, 'staff', 'reminder-email-staff', 'swedish', 'Staff Reminder Email [swedish]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1485, 'contract', 'contract-comment-to-client', 'swedish', 'New Comment  (Sent to Customer Contacts) [swedish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1486, 'contract', 'contract-comment-to-admin', 'swedish', 'New Comment (Sent to Staff)  [swedish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1487, 'subscriptions', 'send-subscription', 'swedish', 'Send Subscription to Customer [swedish]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1488, 'subscriptions', 'subscription-payment-failed', 'swedish', 'Subscription Payment Failed [swedish]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1489, 'subscriptions', 'subscription-canceled', 'swedish', 'Subscription Canceled (Sent to customer primary contact) [swedish]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1490, 'subscriptions', 'subscription-payment-succeeded', 'swedish', 'Subscription Payment Succeeded (Sent to customer primary contact) [swedish]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1491, 'contract', 'contract-expiration-to-staff', 'swedish', 'Contract Expiration Reminder (Sent to Staff) [swedish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1492, 'gdpr', 'gdpr-removal-request', 'swedish', 'Removal Request From Contact (Sent to administrators) [swedish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1493, 'gdpr', 'gdpr-removal-request-lead', 'swedish', 'Removal Request From Lead (Sent to administrators) [swedish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1494, 'client', 'client-registration-confirmed', 'swedish', 'Customer Registration Confirmed [swedish]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1495, 'contract', 'contract-signed-to-staff', 'swedish', 'Contract Signed (Sent to Staff) [swedish]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1496, 'subscriptions', 'customer-subscribed-to-staff', 'swedish', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [swedish]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1497, 'client', 'contact-verification-email', 'swedish', 'Email Verification (Sent to Contact After Registration) [swedish]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1498, 'client', 'new-customer-profile-file-uploaded-to-staff', 'swedish', 'New Customer Profile File(s) Uploaded (Sent to Staff) [swedish]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1499, 'staff', 'event-notification-to-staff', 'swedish', 'Event Notification (Calendar) [swedish]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1500, 'subscriptions', 'subscription-payment-requires-action', 'swedish', 'Credit Card Authorization Required - SCA [swedish]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1501, 'client', 'new-client-created', 'polish', 'New Contact Added/Registered (Welcome Email) [polish]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1502, 'invoice', 'invoice-send-to-client', 'polish', 'Send Invoice to Customer [polish]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1503, 'ticket', 'new-ticket-opened-admin', 'polish', 'New Ticket Opened (Opened by Staff, Sent to Customer) [polish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1504, 'ticket', 'ticket-reply', 'polish', 'Ticket Reply (Sent to Customer) [polish]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1505, 'ticket', 'ticket-autoresponse', 'polish', 'New Ticket Opened - Autoresponse [polish]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1506, 'invoice', 'invoice-payment-recorded', 'polish', 'Invoice Payment Recorded (Sent to Customer) [polish]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1507, 'invoice', 'invoice-overdue-notice', 'polish', 'Invoice Overdue Notice [polish]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1508, 'invoice', 'invoice-already-send', 'polish', 'Invoice Already Sent to Customer [polish]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1509, 'ticket', 'new-ticket-created-staff', 'polish', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [polish]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1510, 'estimate', 'estimate-send-to-client', 'polish', 'Send Estimate to Customer [polish]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1511, 'ticket', 'ticket-reply-to-admin', 'polish', 'Ticket Reply (Sent to Staff) [polish]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1512, 'estimate', 'estimate-already-send', 'polish', 'Estimate Already Sent to Customer [polish]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1513, 'contract', 'contract-expiration', 'polish', 'Contract Expiration Reminder (Sent to Customer Contacts) [polish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1514, 'tasks', 'task-assigned', 'polish', 'New Task Assigned (Sent to Staff) [polish]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1515, 'tasks', 'task-added-as-follower', 'polish', 'Staff Member Added as Follower on Task (Sent to Staff) [polish]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1516, 'tasks', 'task-commented', 'polish', 'New Comment on Task (Sent to Staff) [polish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1517, 'tasks', 'task-added-attachment', 'polish', 'New Attachment(s) on Task (Sent to Staff) [polish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1518, 'estimate', 'estimate-declined-to-staff', 'polish', 'Estimate Declined (Sent to Staff) [polish]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1519, 'estimate', 'estimate-accepted-to-staff', 'polish', 'Estimate Accepted (Sent to Staff) [polish]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1520, 'proposals', 'proposal-client-accepted', 'polish', 'Customer Action - Accepted (Sent to Staff) [polish]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1521, 'proposals', 'proposal-send-to-customer', 'polish', 'Send Proposal to Customer [polish]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1522, 'proposals', 'proposal-client-declined', 'polish', 'Customer Action - Declined (Sent to Staff) [polish]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1523, 'proposals', 'proposal-client-thank-you', 'polish', 'Thank You Email (Sent to Customer After Accept) [polish]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1524, 'proposals', 'proposal-comment-to-client', 'polish', 'New Comment  (Sent to Customer/Lead) [polish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1525, 'proposals', 'proposal-comment-to-admin', 'polish', 'New Comment (Sent to Staff)  [polish]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1526, 'estimate', 'estimate-thank-you-to-customer', 'polish', 'Thank You Email (Sent to Customer After Accept) [polish]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1527, 'tasks', 'task-deadline-notification', 'polish', 'Task Deadline Reminder - Sent to Assigned Members [polish]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1528, 'contract', 'send-contract', 'polish', 'Send Contract to Customer [polish]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1529, 'invoice', 'invoice-payment-recorded-to-staff', 'polish', 'Invoice Payment Recorded (Sent to Staff) [polish]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1530, 'ticket', 'auto-close-ticket', 'polish', 'Auto Close Ticket [polish]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1531, 'project', 'new-project-discussion-created-to-staff', 'polish', 'New Project Discussion (Sent to Project Members) [polish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1532, 'project', 'new-project-discussion-created-to-customer', 'polish', 'New Project Discussion (Sent to Customer Contacts) [polish]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1533, 'project', 'new-project-file-uploaded-to-customer', 'polish', 'New Project File(s) Uploaded (Sent to Customer Contacts) [polish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1534, 'project', 'new-project-file-uploaded-to-staff', 'polish', 'New Project File(s) Uploaded (Sent to Project Members) [polish]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1535, 'project', 'new-project-discussion-comment-to-customer', 'polish', 'New Discussion Comment  (Sent to Customer Contacts) [polish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1536, 'project', 'new-project-discussion-comment-to-staff', 'polish', 'New Discussion Comment (Sent to Project Members) [polish]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1537, 'project', 'staff-added-as-project-member', 'polish', 'Staff Added as Project Member [polish]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1538, 'estimate', 'estimate-expiry-reminder', 'polish', 'Estimate Expiration Reminder [polish]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1539, 'proposals', 'proposal-expiry-reminder', 'polish', 'Proposal Expiration Reminder [polish]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1540, 'staff', 'new-staff-created', 'polish', 'New Staff Created (Welcome Email) [polish]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1541, 'client', 'contact-forgot-password', 'polish', 'Forgot Password [polish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1542, 'client', 'contact-password-reseted', 'polish', 'Password Reset - Confirmation [polish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1543, 'client', 'contact-set-password', 'polish', 'Set New Password [polish]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1544, 'staff', 'staff-forgot-password', 'polish', 'Forgot Password [polish]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1545, 'staff', 'staff-password-reseted', 'polish', 'Password Reset - Confirmation [polish]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1546, 'project', 'assigned-to-project', 'polish', 'New Project Created (Sent to Customer Contacts) [polish]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1547, 'tasks', 'task-added-attachment-to-contacts', 'polish', 'New Attachment(s) on Task (Sent to Customer Contacts) [polish]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1548, 'tasks', 'task-commented-to-contacts', 'polish', 'New Comment on Task (Sent to Customer Contacts) [polish]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1549, 'leads', 'new-lead-assigned', 'polish', 'New Lead Assigned to Staff Member [polish]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1550, 'client', 'client-statement', 'polish', 'Statement - Account Summary [polish]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1551, 'ticket', 'ticket-assigned-to-admin', 'polish', 'New Ticket Assigned (Sent to Staff) [polish]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1552, 'client', 'new-client-registered-to-admin', 'polish', 'New Customer Registration (Sent to admins) [polish]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1553, 'leads', 'new-web-to-lead-form-submitted', 'polish', 'Web to lead form submitted - Sent to lead [polish]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1554, 'staff', 'two-factor-authentication', 'polish', 'Two Factor Authentication [polish]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1555, 'project', 'project-finished-to-customer', 'polish', 'Project Marked as Finished (Sent to Customer Contacts) [polish]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1556, 'credit_note', 'credit-note-send-to-client', 'polish', 'Send Credit Note To Email [polish]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1557, 'tasks', 'task-status-change-to-staff', 'polish', 'Task Status Changed (Sent to Staff) [polish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1558, 'tasks', 'task-status-change-to-contacts', 'polish', 'Task Status Changed (Sent to Customer Contacts) [polish]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1559, 'staff', 'reminder-email-staff', 'polish', 'Staff Reminder Email [polish]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1560, 'contract', 'contract-comment-to-client', 'polish', 'New Comment  (Sent to Customer Contacts) [polish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1561, 'contract', 'contract-comment-to-admin', 'polish', 'New Comment (Sent to Staff)  [polish]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1562, 'subscriptions', 'send-subscription', 'polish', 'Send Subscription to Customer [polish]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1563, 'subscriptions', 'subscription-payment-failed', 'polish', 'Subscription Payment Failed [polish]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1564, 'subscriptions', 'subscription-canceled', 'polish', 'Subscription Canceled (Sent to customer primary contact) [polish]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1565, 'subscriptions', 'subscription-payment-succeeded', 'polish', 'Subscription Payment Succeeded (Sent to customer primary contact) [polish]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1566, 'contract', 'contract-expiration-to-staff', 'polish', 'Contract Expiration Reminder (Sent to Staff) [polish]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1567, 'gdpr', 'gdpr-removal-request', 'polish', 'Removal Request From Contact (Sent to administrators) [polish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1568, 'gdpr', 'gdpr-removal-request-lead', 'polish', 'Removal Request From Lead (Sent to administrators) [polish]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1569, 'client', 'client-registration-confirmed', 'polish', 'Customer Registration Confirmed [polish]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1570, 'contract', 'contract-signed-to-staff', 'polish', 'Contract Signed (Sent to Staff) [polish]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1571, 'subscriptions', 'customer-subscribed-to-staff', 'polish', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [polish]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1572, 'client', 'contact-verification-email', 'polish', 'Email Verification (Sent to Contact After Registration) [polish]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1573, 'client', 'new-customer-profile-file-uploaded-to-staff', 'polish', 'New Customer Profile File(s) Uploaded (Sent to Staff) [polish]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1574, 'staff', 'event-notification-to-staff', 'polish', 'Event Notification (Calendar) [polish]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1575, 'subscriptions', 'subscription-payment-requires-action', 'polish', 'Credit Card Authorization Required - SCA [polish]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1576, 'client', 'new-client-created', 'dutch', 'New Contact Added/Registered (Welcome Email) [dutch]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1577, 'invoice', 'invoice-send-to-client', 'dutch', 'Send Invoice to Customer [dutch]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1578, 'ticket', 'new-ticket-opened-admin', 'dutch', 'New Ticket Opened (Opened by Staff, Sent to Customer) [dutch]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1579, 'ticket', 'ticket-reply', 'dutch', 'Ticket Reply (Sent to Customer) [dutch]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1580, 'ticket', 'ticket-autoresponse', 'dutch', 'New Ticket Opened - Autoresponse [dutch]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1581, 'invoice', 'invoice-payment-recorded', 'dutch', 'Invoice Payment Recorded (Sent to Customer) [dutch]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1582, 'invoice', 'invoice-overdue-notice', 'dutch', 'Invoice Overdue Notice [dutch]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1583, 'invoice', 'invoice-already-send', 'dutch', 'Invoice Already Sent to Customer [dutch]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1584, 'ticket', 'new-ticket-created-staff', 'dutch', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [dutch]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1585, 'estimate', 'estimate-send-to-client', 'dutch', 'Send Estimate to Customer [dutch]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1586, 'ticket', 'ticket-reply-to-admin', 'dutch', 'Ticket Reply (Sent to Staff) [dutch]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1587, 'estimate', 'estimate-already-send', 'dutch', 'Estimate Already Sent to Customer [dutch]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1588, 'contract', 'contract-expiration', 'dutch', 'Contract Expiration Reminder (Sent to Customer Contacts) [dutch]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1589, 'tasks', 'task-assigned', 'dutch', 'New Task Assigned (Sent to Staff) [dutch]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1590, 'tasks', 'task-added-as-follower', 'dutch', 'Staff Member Added as Follower on Task (Sent to Staff) [dutch]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1591, 'tasks', 'task-commented', 'dutch', 'New Comment on Task (Sent to Staff) [dutch]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1592, 'tasks', 'task-added-attachment', 'dutch', 'New Attachment(s) on Task (Sent to Staff) [dutch]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1593, 'estimate', 'estimate-declined-to-staff', 'dutch', 'Estimate Declined (Sent to Staff) [dutch]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1594, 'estimate', 'estimate-accepted-to-staff', 'dutch', 'Estimate Accepted (Sent to Staff) [dutch]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1595, 'proposals', 'proposal-client-accepted', 'dutch', 'Customer Action - Accepted (Sent to Staff) [dutch]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1596, 'proposals', 'proposal-send-to-customer', 'dutch', 'Send Proposal to Customer [dutch]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1597, 'proposals', 'proposal-client-declined', 'dutch', 'Customer Action - Declined (Sent to Staff) [dutch]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1598, 'proposals', 'proposal-client-thank-you', 'dutch', 'Thank You Email (Sent to Customer After Accept) [dutch]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1599, 'proposals', 'proposal-comment-to-client', 'dutch', 'New Comment  (Sent to Customer/Lead) [dutch]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1600, 'proposals', 'proposal-comment-to-admin', 'dutch', 'New Comment (Sent to Staff)  [dutch]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1601, 'estimate', 'estimate-thank-you-to-customer', 'dutch', 'Thank You Email (Sent to Customer After Accept) [dutch]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1602, 'tasks', 'task-deadline-notification', 'dutch', 'Task Deadline Reminder - Sent to Assigned Members [dutch]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1603, 'contract', 'send-contract', 'dutch', 'Send Contract to Customer [dutch]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1604, 'invoice', 'invoice-payment-recorded-to-staff', 'dutch', 'Invoice Payment Recorded (Sent to Staff) [dutch]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1605, 'ticket', 'auto-close-ticket', 'dutch', 'Auto Close Ticket [dutch]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1606, 'project', 'new-project-discussion-created-to-staff', 'dutch', 'New Project Discussion (Sent to Project Members) [dutch]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1607, 'project', 'new-project-discussion-created-to-customer', 'dutch', 'New Project Discussion (Sent to Customer Contacts) [dutch]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1608, 'project', 'new-project-file-uploaded-to-customer', 'dutch', 'New Project File(s) Uploaded (Sent to Customer Contacts) [dutch]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1609, 'project', 'new-project-file-uploaded-to-staff', 'dutch', 'New Project File(s) Uploaded (Sent to Project Members) [dutch]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1610, 'project', 'new-project-discussion-comment-to-customer', 'dutch', 'New Discussion Comment  (Sent to Customer Contacts) [dutch]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1611, 'project', 'new-project-discussion-comment-to-staff', 'dutch', 'New Discussion Comment (Sent to Project Members) [dutch]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1612, 'project', 'staff-added-as-project-member', 'dutch', 'Staff Added as Project Member [dutch]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1613, 'estimate', 'estimate-expiry-reminder', 'dutch', 'Estimate Expiration Reminder [dutch]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1614, 'proposals', 'proposal-expiry-reminder', 'dutch', 'Proposal Expiration Reminder [dutch]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1615, 'staff', 'new-staff-created', 'dutch', 'New Staff Created (Welcome Email) [dutch]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1616, 'client', 'contact-forgot-password', 'dutch', 'Forgot Password [dutch]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1617, 'client', 'contact-password-reseted', 'dutch', 'Password Reset - Confirmation [dutch]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1618, 'client', 'contact-set-password', 'dutch', 'Set New Password [dutch]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1619, 'staff', 'staff-forgot-password', 'dutch', 'Forgot Password [dutch]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1620, 'staff', 'staff-password-reseted', 'dutch', 'Password Reset - Confirmation [dutch]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1621, 'project', 'assigned-to-project', 'dutch', 'New Project Created (Sent to Customer Contacts) [dutch]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1622, 'tasks', 'task-added-attachment-to-contacts', 'dutch', 'New Attachment(s) on Task (Sent to Customer Contacts) [dutch]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1623, 'tasks', 'task-commented-to-contacts', 'dutch', 'New Comment on Task (Sent to Customer Contacts) [dutch]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1624, 'leads', 'new-lead-assigned', 'dutch', 'New Lead Assigned to Staff Member [dutch]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1625, 'client', 'client-statement', 'dutch', 'Statement - Account Summary [dutch]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1626, 'ticket', 'ticket-assigned-to-admin', 'dutch', 'New Ticket Assigned (Sent to Staff) [dutch]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1627, 'client', 'new-client-registered-to-admin', 'dutch', 'New Customer Registration (Sent to admins) [dutch]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1628, 'leads', 'new-web-to-lead-form-submitted', 'dutch', 'Web to lead form submitted - Sent to lead [dutch]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1629, 'staff', 'two-factor-authentication', 'dutch', 'Two Factor Authentication [dutch]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1630, 'project', 'project-finished-to-customer', 'dutch', 'Project Marked as Finished (Sent to Customer Contacts) [dutch]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1631, 'credit_note', 'credit-note-send-to-client', 'dutch', 'Send Credit Note To Email [dutch]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1632, 'tasks', 'task-status-change-to-staff', 'dutch', 'Task Status Changed (Sent to Staff) [dutch]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1633, 'tasks', 'task-status-change-to-contacts', 'dutch', 'Task Status Changed (Sent to Customer Contacts) [dutch]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1634, 'staff', 'reminder-email-staff', 'dutch', 'Staff Reminder Email [dutch]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1635, 'contract', 'contract-comment-to-client', 'dutch', 'New Comment  (Sent to Customer Contacts) [dutch]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1636, 'contract', 'contract-comment-to-admin', 'dutch', 'New Comment (Sent to Staff)  [dutch]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1637, 'subscriptions', 'send-subscription', 'dutch', 'Send Subscription to Customer [dutch]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1638, 'subscriptions', 'subscription-payment-failed', 'dutch', 'Subscription Payment Failed [dutch]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1639, 'subscriptions', 'subscription-canceled', 'dutch', 'Subscription Canceled (Sent to customer primary contact) [dutch]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1640, 'subscriptions', 'subscription-payment-succeeded', 'dutch', 'Subscription Payment Succeeded (Sent to customer primary contact) [dutch]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1641, 'contract', 'contract-expiration-to-staff', 'dutch', 'Contract Expiration Reminder (Sent to Staff) [dutch]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1642, 'gdpr', 'gdpr-removal-request', 'dutch', 'Removal Request From Contact (Sent to administrators) [dutch]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1643, 'gdpr', 'gdpr-removal-request-lead', 'dutch', 'Removal Request From Lead (Sent to administrators) [dutch]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1644, 'client', 'client-registration-confirmed', 'dutch', 'Customer Registration Confirmed [dutch]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1645, 'contract', 'contract-signed-to-staff', 'dutch', 'Contract Signed (Sent to Staff) [dutch]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1646, 'subscriptions', 'customer-subscribed-to-staff', 'dutch', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [dutch]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1647, 'client', 'contact-verification-email', 'dutch', 'Email Verification (Sent to Contact After Registration) [dutch]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1648, 'client', 'new-customer-profile-file-uploaded-to-staff', 'dutch', 'New Customer Profile File(s) Uploaded (Sent to Staff) [dutch]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1649, 'staff', 'event-notification-to-staff', 'dutch', 'Event Notification (Calendar) [dutch]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1650, 'subscriptions', 'subscription-payment-requires-action', 'dutch', 'Credit Card Authorization Required - SCA [dutch]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1651, 'client', 'new-client-created', 'chinese', 'New Contact Added/Registered (Welcome Email) [chinese]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1652, 'invoice', 'invoice-send-to-client', 'chinese', 'Send Invoice to Customer [chinese]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1653, 'ticket', 'new-ticket-opened-admin', 'chinese', 'New Ticket Opened (Opened by Staff, Sent to Customer) [chinese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1654, 'ticket', 'ticket-reply', 'chinese', 'Ticket Reply (Sent to Customer) [chinese]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1655, 'ticket', 'ticket-autoresponse', 'chinese', 'New Ticket Opened - Autoresponse [chinese]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1656, 'invoice', 'invoice-payment-recorded', 'chinese', 'Invoice Payment Recorded (Sent to Customer) [chinese]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1657, 'invoice', 'invoice-overdue-notice', 'chinese', 'Invoice Overdue Notice [chinese]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1658, 'invoice', 'invoice-already-send', 'chinese', 'Invoice Already Sent to Customer [chinese]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1659, 'ticket', 'new-ticket-created-staff', 'chinese', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [chinese]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1660, 'estimate', 'estimate-send-to-client', 'chinese', 'Send Estimate to Customer [chinese]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1661, 'ticket', 'ticket-reply-to-admin', 'chinese', 'Ticket Reply (Sent to Staff) [chinese]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1662, 'estimate', 'estimate-already-send', 'chinese', 'Estimate Already Sent to Customer [chinese]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1663, 'contract', 'contract-expiration', 'chinese', 'Contract Expiration Reminder (Sent to Customer Contacts) [chinese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1664, 'tasks', 'task-assigned', 'chinese', 'New Task Assigned (Sent to Staff) [chinese]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1665, 'tasks', 'task-added-as-follower', 'chinese', 'Staff Member Added as Follower on Task (Sent to Staff) [chinese]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1666, 'tasks', 'task-commented', 'chinese', 'New Comment on Task (Sent to Staff) [chinese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1667, 'tasks', 'task-added-attachment', 'chinese', 'New Attachment(s) on Task (Sent to Staff) [chinese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1668, 'estimate', 'estimate-declined-to-staff', 'chinese', 'Estimate Declined (Sent to Staff) [chinese]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1669, 'estimate', 'estimate-accepted-to-staff', 'chinese', 'Estimate Accepted (Sent to Staff) [chinese]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1670, 'proposals', 'proposal-client-accepted', 'chinese', 'Customer Action - Accepted (Sent to Staff) [chinese]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1671, 'proposals', 'proposal-send-to-customer', 'chinese', 'Send Proposal to Customer [chinese]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1672, 'proposals', 'proposal-client-declined', 'chinese', 'Customer Action - Declined (Sent to Staff) [chinese]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1673, 'proposals', 'proposal-client-thank-you', 'chinese', 'Thank You Email (Sent to Customer After Accept) [chinese]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1674, 'proposals', 'proposal-comment-to-client', 'chinese', 'New Comment  (Sent to Customer/Lead) [chinese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1675, 'proposals', 'proposal-comment-to-admin', 'chinese', 'New Comment (Sent to Staff)  [chinese]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1676, 'estimate', 'estimate-thank-you-to-customer', 'chinese', 'Thank You Email (Sent to Customer After Accept) [chinese]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1677, 'tasks', 'task-deadline-notification', 'chinese', 'Task Deadline Reminder - Sent to Assigned Members [chinese]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1678, 'contract', 'send-contract', 'chinese', 'Send Contract to Customer [chinese]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1679, 'invoice', 'invoice-payment-recorded-to-staff', 'chinese', 'Invoice Payment Recorded (Sent to Staff) [chinese]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1680, 'ticket', 'auto-close-ticket', 'chinese', 'Auto Close Ticket [chinese]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1681, 'project', 'new-project-discussion-created-to-staff', 'chinese', 'New Project Discussion (Sent to Project Members) [chinese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1682, 'project', 'new-project-discussion-created-to-customer', 'chinese', 'New Project Discussion (Sent to Customer Contacts) [chinese]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1683, 'project', 'new-project-file-uploaded-to-customer', 'chinese', 'New Project File(s) Uploaded (Sent to Customer Contacts) [chinese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1684, 'project', 'new-project-file-uploaded-to-staff', 'chinese', 'New Project File(s) Uploaded (Sent to Project Members) [chinese]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1685, 'project', 'new-project-discussion-comment-to-customer', 'chinese', 'New Discussion Comment  (Sent to Customer Contacts) [chinese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1686, 'project', 'new-project-discussion-comment-to-staff', 'chinese', 'New Discussion Comment (Sent to Project Members) [chinese]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1687, 'project', 'staff-added-as-project-member', 'chinese', 'Staff Added as Project Member [chinese]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1688, 'estimate', 'estimate-expiry-reminder', 'chinese', 'Estimate Expiration Reminder [chinese]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1689, 'proposals', 'proposal-expiry-reminder', 'chinese', 'Proposal Expiration Reminder [chinese]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1690, 'staff', 'new-staff-created', 'chinese', 'New Staff Created (Welcome Email) [chinese]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1691, 'client', 'contact-forgot-password', 'chinese', 'Forgot Password [chinese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1692, 'client', 'contact-password-reseted', 'chinese', 'Password Reset - Confirmation [chinese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1693, 'client', 'contact-set-password', 'chinese', 'Set New Password [chinese]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1694, 'staff', 'staff-forgot-password', 'chinese', 'Forgot Password [chinese]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1695, 'staff', 'staff-password-reseted', 'chinese', 'Password Reset - Confirmation [chinese]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1696, 'project', 'assigned-to-project', 'chinese', 'New Project Created (Sent to Customer Contacts) [chinese]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1697, 'tasks', 'task-added-attachment-to-contacts', 'chinese', 'New Attachment(s) on Task (Sent to Customer Contacts) [chinese]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1698, 'tasks', 'task-commented-to-contacts', 'chinese', 'New Comment on Task (Sent to Customer Contacts) [chinese]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1699, 'leads', 'new-lead-assigned', 'chinese', 'New Lead Assigned to Staff Member [chinese]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1700, 'client', 'client-statement', 'chinese', 'Statement - Account Summary [chinese]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1701, 'ticket', 'ticket-assigned-to-admin', 'chinese', 'New Ticket Assigned (Sent to Staff) [chinese]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1702, 'client', 'new-client-registered-to-admin', 'chinese', 'New Customer Registration (Sent to admins) [chinese]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1703, 'leads', 'new-web-to-lead-form-submitted', 'chinese', 'Web to lead form submitted - Sent to lead [chinese]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1704, 'staff', 'two-factor-authentication', 'chinese', 'Two Factor Authentication [chinese]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1705, 'project', 'project-finished-to-customer', 'chinese', 'Project Marked as Finished (Sent to Customer Contacts) [chinese]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1706, 'credit_note', 'credit-note-send-to-client', 'chinese', 'Send Credit Note To Email [chinese]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1707, 'tasks', 'task-status-change-to-staff', 'chinese', 'Task Status Changed (Sent to Staff) [chinese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1708, 'tasks', 'task-status-change-to-contacts', 'chinese', 'Task Status Changed (Sent to Customer Contacts) [chinese]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1709, 'staff', 'reminder-email-staff', 'chinese', 'Staff Reminder Email [chinese]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1710, 'contract', 'contract-comment-to-client', 'chinese', 'New Comment  (Sent to Customer Contacts) [chinese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1711, 'contract', 'contract-comment-to-admin', 'chinese', 'New Comment (Sent to Staff)  [chinese]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1712, 'subscriptions', 'send-subscription', 'chinese', 'Send Subscription to Customer [chinese]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1713, 'subscriptions', 'subscription-payment-failed', 'chinese', 'Subscription Payment Failed [chinese]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1714, 'subscriptions', 'subscription-canceled', 'chinese', 'Subscription Canceled (Sent to customer primary contact) [chinese]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1715, 'subscriptions', 'subscription-payment-succeeded', 'chinese', 'Subscription Payment Succeeded (Sent to customer primary contact) [chinese]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1716, 'contract', 'contract-expiration-to-staff', 'chinese', 'Contract Expiration Reminder (Sent to Staff) [chinese]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1717, 'gdpr', 'gdpr-removal-request', 'chinese', 'Removal Request From Contact (Sent to administrators) [chinese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1718, 'gdpr', 'gdpr-removal-request-lead', 'chinese', 'Removal Request From Lead (Sent to administrators) [chinese]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1719, 'client', 'client-registration-confirmed', 'chinese', 'Customer Registration Confirmed [chinese]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1720, 'contract', 'contract-signed-to-staff', 'chinese', 'Contract Signed (Sent to Staff) [chinese]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1721, 'subscriptions', 'customer-subscribed-to-staff', 'chinese', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [chinese]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1722, 'client', 'contact-verification-email', 'chinese', 'Email Verification (Sent to Contact After Registration) [chinese]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1723, 'client', 'new-customer-profile-file-uploaded-to-staff', 'chinese', 'New Customer Profile File(s) Uploaded (Sent to Staff) [chinese]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1724, 'staff', 'event-notification-to-staff', 'chinese', 'Event Notification (Calendar) [chinese]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1725, 'subscriptions', 'subscription-payment-requires-action', 'chinese', 'Credit Card Authorization Required - SCA [chinese]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1726, 'client', 'new-client-created', 'portuguese_br', 'New Contact Added/Registered (Welcome Email) [portuguese_br]', 'Welcome aboard', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1727, 'invoice', 'invoice-send-to-client', 'portuguese_br', 'Send Invoice to Customer [portuguese_br]', 'Invoice with number {invoice_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1728, 'ticket', 'new-ticket-opened-admin', 'portuguese_br', 'New Ticket Opened (Opened by Staff, Sent to Customer) [portuguese_br]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1729, 'ticket', 'ticket-reply', 'portuguese_br', 'Ticket Reply (Sent to Customer) [portuguese_br]', 'New Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1730, 'ticket', 'ticket-autoresponse', 'portuguese_br', 'New Ticket Opened - Autoresponse [portuguese_br]', 'New Support Ticket Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1731, 'invoice', 'invoice-payment-recorded', 'portuguese_br', 'Invoice Payment Recorded (Sent to Customer) [portuguese_br]', 'Invoice Payment Recorded', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1732, 'invoice', 'invoice-overdue-notice', 'portuguese_br', 'Invoice Overdue Notice [portuguese_br]', 'Invoice Overdue Notice - {invoice_number}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1733, 'invoice', 'invoice-already-send', 'portuguese_br', 'Invoice Already Sent to Customer [portuguese_br]', 'Invoice # {invoice_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1734, 'ticket', 'new-ticket-created-staff', 'portuguese_br', 'New Ticket Created (Opened by Customer, Sent to Staff Members) [portuguese_br]', 'New Ticket Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1735, 'estimate', 'estimate-send-to-client', 'portuguese_br', 'Send Estimate to Customer [portuguese_br]', 'Estimate # {estimate_number} created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1736, 'ticket', 'ticket-reply-to-admin', 'portuguese_br', 'Ticket Reply (Sent to Staff) [portuguese_br]', 'New Support Ticket Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1737, 'estimate', 'estimate-already-send', 'portuguese_br', 'Estimate Already Sent to Customer [portuguese_br]', 'Estimate # {estimate_number} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1738, 'contract', 'contract-expiration', 'portuguese_br', 'Contract Expiration Reminder (Sent to Customer Contacts) [portuguese_br]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1739, 'tasks', 'task-assigned', 'portuguese_br', 'New Task Assigned (Sent to Staff) [portuguese_br]', 'New Task Assigned to You - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1740, 'tasks', 'task-added-as-follower', 'portuguese_br', 'Staff Member Added as Follower on Task (Sent to Staff) [portuguese_br]', 'You are added as follower on task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1741, 'tasks', 'task-commented', 'portuguese_br', 'New Comment on Task (Sent to Staff) [portuguese_br]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1742, 'tasks', 'task-added-attachment', 'portuguese_br', 'New Attachment(s) on Task (Sent to Staff) [portuguese_br]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1743, 'estimate', 'estimate-declined-to-staff', 'portuguese_br', 'Estimate Declined (Sent to Staff) [portuguese_br]', 'Customer Declined Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1744, 'estimate', 'estimate-accepted-to-staff', 'portuguese_br', 'Estimate Accepted (Sent to Staff) [portuguese_br]', 'Customer Accepted Estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1745, 'proposals', 'proposal-client-accepted', 'portuguese_br', 'Customer Action - Accepted (Sent to Staff) [portuguese_br]', 'Customer Accepted Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1746, 'proposals', 'proposal-send-to-customer', 'portuguese_br', 'Send Proposal to Customer [portuguese_br]', 'Proposal With Number {proposal_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1747, 'proposals', 'proposal-client-declined', 'portuguese_br', 'Customer Action - Declined (Sent to Staff) [portuguese_br]', 'Client Declined Proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1748, 'proposals', 'proposal-client-thank-you', 'portuguese_br', 'Thank You Email (Sent to Customer After Accept) [portuguese_br]', 'Thank for you accepting proposal', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1749, 'proposals', 'proposal-comment-to-client', 'portuguese_br', 'New Comment  (Sent to Customer/Lead) [portuguese_br]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1750, 'proposals', 'proposal-comment-to-admin', 'portuguese_br', 'New Comment (Sent to Staff)  [portuguese_br]', 'New Proposal Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1751, 'estimate', 'estimate-thank-you-to-customer', 'portuguese_br', 'Thank You Email (Sent to Customer After Accept) [portuguese_br]', 'Thank for you accepting estimate', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1752, 'tasks', 'task-deadline-notification', 'portuguese_br', 'Task Deadline Reminder - Sent to Assigned Members [portuguese_br]', 'Task Deadline Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1753, 'contract', 'send-contract', 'portuguese_br', 'Send Contract to Customer [portuguese_br]', 'Contract - {contract_subject}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1754, 'invoice', 'invoice-payment-recorded-to-staff', 'portuguese_br', 'Invoice Payment Recorded (Sent to Staff) [portuguese_br]', 'New Invoice Payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1755, 'ticket', 'auto-close-ticket', 'portuguese_br', 'Auto Close Ticket [portuguese_br]', 'Ticket Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1756, 'project', 'new-project-discussion-created-to-staff', 'portuguese_br', 'New Project Discussion (Sent to Project Members) [portuguese_br]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1757, 'project', 'new-project-discussion-created-to-customer', 'portuguese_br', 'New Project Discussion (Sent to Customer Contacts) [portuguese_br]', 'New Project Discussion Created - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1758, 'project', 'new-project-file-uploaded-to-customer', 'portuguese_br', 'New Project File(s) Uploaded (Sent to Customer Contacts) [portuguese_br]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1759, 'project', 'new-project-file-uploaded-to-staff', 'portuguese_br', 'New Project File(s) Uploaded (Sent to Project Members) [portuguese_br]', 'New Project File(s) Uploaded - {project_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1760, 'project', 'new-project-discussion-comment-to-customer', 'portuguese_br', 'New Discussion Comment  (Sent to Customer Contacts) [portuguese_br]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1761, 'project', 'new-project-discussion-comment-to-staff', 'portuguese_br', 'New Discussion Comment (Sent to Project Members) [portuguese_br]', 'New Discussion Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1762, 'project', 'staff-added-as-project-member', 'portuguese_br', 'Staff Added as Project Member [portuguese_br]', 'New project assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1763, 'estimate', 'estimate-expiry-reminder', 'portuguese_br', 'Estimate Expiration Reminder [portuguese_br]', 'Estimate Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1764, 'proposals', 'proposal-expiry-reminder', 'portuguese_br', 'Proposal Expiration Reminder [portuguese_br]', 'Proposal Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1765, 'staff', 'new-staff-created', 'portuguese_br', 'New Staff Created (Welcome Email) [portuguese_br]', 'You are added as staff member', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1766, 'client', 'contact-forgot-password', 'portuguese_br', 'Forgot Password [portuguese_br]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1767, 'client', 'contact-password-reseted', 'portuguese_br', 'Password Reset - Confirmation [portuguese_br]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1768, 'client', 'contact-set-password', 'portuguese_br', 'Set New Password [portuguese_br]', 'Set new password on {companyname} ', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1769, 'staff', 'staff-forgot-password', 'portuguese_br', 'Forgot Password [portuguese_br]', 'Create New Password', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1770, 'staff', 'staff-password-reseted', 'portuguese_br', 'Password Reset - Confirmation [portuguese_br]', 'Your password has been changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1771, 'project', 'assigned-to-project', 'portuguese_br', 'New Project Created (Sent to Customer Contacts) [portuguese_br]', 'New Project Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1772, 'tasks', 'task-added-attachment-to-contacts', 'portuguese_br', 'New Attachment(s) on Task (Sent to Customer Contacts) [portuguese_br]', 'New Attachment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1773, 'tasks', 'task-commented-to-contacts', 'portuguese_br', 'New Comment on Task (Sent to Customer Contacts) [portuguese_br]', 'New Comment on Task - {task_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1774, 'leads', 'new-lead-assigned', 'portuguese_br', 'New Lead Assigned to Staff Member [portuguese_br]', 'New lead assigned to you', '', '{companyname} | CRM', '', 0, 1, 0),
	(1775, 'client', 'client-statement', 'portuguese_br', 'Statement - Account Summary [portuguese_br]', 'Account Statement from {statement_from} to {statement_to}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1776, 'ticket', 'ticket-assigned-to-admin', 'portuguese_br', 'New Ticket Assigned (Sent to Staff) [portuguese_br]', 'New support ticket has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1777, 'client', 'new-client-registered-to-admin', 'portuguese_br', 'New Customer Registration (Sent to admins) [portuguese_br]', 'New Customer Registration', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1778, 'leads', 'new-web-to-lead-form-submitted', 'portuguese_br', 'Web to lead form submitted - Sent to lead [portuguese_br]', '{lead_name} - We Received Your Request', '', '{companyname} | CRM', NULL, 0, 0, 0),
	(1779, 'staff', 'two-factor-authentication', 'portuguese_br', 'Two Factor Authentication [portuguese_br]', 'Confirm Your Login', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1780, 'project', 'project-finished-to-customer', 'portuguese_br', 'Project Marked as Finished (Sent to Customer Contacts) [portuguese_br]', 'Project Marked as Finished', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1781, 'credit_note', 'credit-note-send-to-client', 'portuguese_br', 'Send Credit Note To Email [portuguese_br]', 'Credit Note With Number #{credit_note_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1782, 'tasks', 'task-status-change-to-staff', 'portuguese_br', 'Task Status Changed (Sent to Staff) [portuguese_br]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1783, 'tasks', 'task-status-change-to-contacts', 'portuguese_br', 'Task Status Changed (Sent to Customer Contacts) [portuguese_br]', 'Task Status Changed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1784, 'staff', 'reminder-email-staff', 'portuguese_br', 'Staff Reminder Email [portuguese_br]', 'You Have a New Reminder!', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1785, 'contract', 'contract-comment-to-client', 'portuguese_br', 'New Comment  (Sent to Customer Contacts) [portuguese_br]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1786, 'contract', 'contract-comment-to-admin', 'portuguese_br', 'New Comment (Sent to Staff)  [portuguese_br]', 'New Contract Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1787, 'subscriptions', 'send-subscription', 'portuguese_br', 'Send Subscription to Customer [portuguese_br]', 'Subscription Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1788, 'subscriptions', 'subscription-payment-failed', 'portuguese_br', 'Subscription Payment Failed [portuguese_br]', 'Your most recent invoice payment failed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1789, 'subscriptions', 'subscription-canceled', 'portuguese_br', 'Subscription Canceled (Sent to customer primary contact) [portuguese_br]', 'Your subscription has been canceled', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1790, 'subscriptions', 'subscription-payment-succeeded', 'portuguese_br', 'Subscription Payment Succeeded (Sent to customer primary contact) [portuguese_br]', 'Subscription  Payment Receipt - {subscription_name}', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1791, 'contract', 'contract-expiration-to-staff', 'portuguese_br', 'Contract Expiration Reminder (Sent to Staff) [portuguese_br]', 'Contract Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1792, 'gdpr', 'gdpr-removal-request', 'portuguese_br', 'Removal Request From Contact (Sent to administrators) [portuguese_br]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1793, 'gdpr', 'gdpr-removal-request-lead', 'portuguese_br', 'Removal Request From Lead (Sent to administrators) [portuguese_br]', 'Data Removal Request Received', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1794, 'client', 'client-registration-confirmed', 'portuguese_br', 'Customer Registration Confirmed [portuguese_br]', 'Your registration is confirmed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1795, 'contract', 'contract-signed-to-staff', 'portuguese_br', 'Contract Signed (Sent to Staff) [portuguese_br]', 'Customer Signed a Contract', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1796, 'subscriptions', 'customer-subscribed-to-staff', 'portuguese_br', 'Customer Subscribed to a Subscription (Sent to administrators and subscription creator) [portuguese_br]', 'Customer Subscribed to a Subscription', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1797, 'client', 'contact-verification-email', 'portuguese_br', 'Email Verification (Sent to Contact After Registration) [portuguese_br]', 'Verify Email Address', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1798, 'client', 'new-customer-profile-file-uploaded-to-staff', 'portuguese_br', 'New Customer Profile File(s) Uploaded (Sent to Staff) [portuguese_br]', 'Customer Uploaded New File(s) in Profile', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1799, 'staff', 'event-notification-to-staff', 'portuguese_br', 'Event Notification (Calendar) [portuguese_br]', 'Upcoming Event - {event_title}', '', '', NULL, 0, 1, 0),
	(1800, 'subscriptions', 'subscription-payment-requires-action', 'portuguese_br', 'Credit Card Authorization Required - SCA [portuguese_br]', 'Important: Confirm your subscription {subscription_name} payment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1801, 'ticket', 'new-ticket-opened-approval', 'english', 'New Ticket Opened (Opened by Staff, Sent to Approval authority)', 'Approve : New Support Ticket Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support ticket has been opened please login and approve.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}<br /></span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_public_url}">#{ticket_id}</a><br /><br />You can approve the ticket by login into admin panel: <a href="{login_page}">#{login_page}</a><br /><br /><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1802, 'ticket', 'new-ticket-opened-approval', 'slovak', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [slovak]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1803, 'ticket', 'new-ticket-opened-approval', 'czech', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [czech]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1804, 'ticket', 'new-ticket-opened-approval', 'romanian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [romanian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1805, 'ticket', 'new-ticket-opened-approval', 'japanese', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [japanese]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1806, 'ticket', 'new-ticket-opened-approval', 'indonesia', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [indonesia]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1807, 'ticket', 'new-ticket-opened-approval', 'bulgarian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [bulgarian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1808, 'ticket', 'new-ticket-opened-approval', 'vietnamese', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [vietnamese]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1809, 'ticket', 'new-ticket-opened-approval', 'german', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [german]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1810, 'ticket', 'new-ticket-opened-approval', 'french', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [french]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1811, 'ticket', 'new-ticket-opened-approval', 'italian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [italian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1812, 'ticket', 'new-ticket-opened-approval', 'spanish', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [spanish]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1813, 'ticket', 'new-ticket-opened-approval', 'russian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [russian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1814, 'ticket', 'new-ticket-opened-approval', 'catalan', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [catalan]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1815, 'ticket', 'new-ticket-opened-approval', 'persian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [persian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1816, 'ticket', 'new-ticket-opened-approval', 'turkish', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [turkish]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1817, 'ticket', 'new-ticket-opened-approval', 'greek', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [greek]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1818, 'ticket', 'new-ticket-opened-approval', 'portuguese', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [portuguese]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1819, 'ticket', 'new-ticket-opened-approval', 'ukrainian', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [ukrainian]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1820, 'ticket', 'new-ticket-opened-approval', 'swedish', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [swedish]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1821, 'ticket', 'new-ticket-opened-approval', 'polish', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [polish]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1822, 'ticket', 'new-ticket-opened-approval', 'dutch', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [dutch]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1823, 'ticket', 'new-ticket-opened-approval', 'chinese', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [chinese]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1824, 'ticket', 'new-ticket-opened-approval', 'portuguese_br', 'New Ticket Opened (Opened by Staff, Sent to Approval authority) [portuguese_br]', 'Approve : New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1825, 'service_request', 'new-service_request-opened-admin', 'english', 'New service_request Opened (Opened by Staff, Sent to Customer)', 'New Support Service Request Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support service request has been opened.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {service_request_priority}<br /></span><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service request on the following link: <a href="{service_request_public_url}">#{service_request_id}</a><br /><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1826, 'service_request', 'service_request-reply', 'english', 'service_request Reply (Sent to Customer)', 'New service_request Reply', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">You have a new service_request reply to service_request <a href="{service_request_public_url}">#{service_request_id}</a></span><br /><br /><span style="font-size: 12pt;"><strong>service_request Subject:</strong> {service_request_subject}<br /></span><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_public_url}">#{service_request_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1827, 'service_request', 'service_request-autoresponse', 'english', 'New service_request Opened - Autoresponse', 'New Support service_request Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">Thank you for contacting our support team. A support service_request has now been opened for your request. You will be notified when a response is made by email.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {service_request_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_public_url}">#{service_request_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1828, 'service_request', 'new-service_request-created-staff', 'english', 'New service_request Created (Opened by Customer, Sent to Staff Members)', 'New service_request Created', '<span style="font-size: 12pt;">A new support service_request has been opened.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {service_request_priority}<br /></span><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_url}">#{service_request_id}</a></span><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1829, 'service_request', 'service_request-reply-to-admin', 'english', 'service_request Reply (Sent to Staff)', 'New Support service_request Reply', '<span style="font-size: 12pt;">A new support service_request reply from {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {service_request_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_url}">#{service_request_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1830, 'service_request', 'auto-close-service_request', 'english', 'Auto Close service_request', 'service_request Auto Closed', '<p><span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">service_request {service_request_subject} has been auto close due to inactivity.</span><br /><br /><span style="font-size: 12pt;"><strong>service_request #</strong>: <a href="{service_request_public_url}">{service_request_id}</a></span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {service_request_priority}</span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(1831, 'service_request', 'service_request-assigned-to-admin', 'english', 'New service_request Assigned (Sent to Staff)', 'New support service_request has been assigned to you', '<p><span style="font-size: 12pt;">Hi</span></p>\r\n<p><span style="font-size: 12pt;">A new support service_request&nbsp;has been assigned to you.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {service_request_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_url}">#{service_request_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(1832, 'service_request', 'new-service_request-opened-approval', 'english', 'New service_request Opened (Opened by Staff, Sent to Approval authority)', 'Approve : New Support service_request Opened', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support service_request has been opened please login and approve.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {service_request_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {service_request_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {service_request_priority}<br /></span><br /><span style="font-size: 12pt;"><strong>service_request message:</strong></span><br /><span style="font-size: 12pt;">{service_request_message}</span><br /><br /><span style="font-size: 12pt;">You can view the service_request on the following link: <a href="{service_request_public_url}">#{service_request_id}</a><br /><br />You can approve the service_request by login into admin panel: <a href="{login_page}">#{login_page}</a><br /><br /><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(1833, 'service_request', 'new-service_request-opened-admin', 'slovak', 'New service_request Opened (Opened by Staff, Sent to Customer) [slovak]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1834, 'service_request', 'service_request-reply', 'slovak', 'service_request Reply (Sent to Customer) [slovak]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1835, 'service_request', 'service_request-autoresponse', 'slovak', 'New service_request Opened - Autoresponse [slovak]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1836, 'service_request', 'new-service_request-created-staff', 'slovak', 'New service_request Created (Opened by Customer, Sent to Staff Members) [slovak]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1837, 'service_request', 'service_request-reply-to-admin', 'slovak', 'service_request Reply (Sent to Staff) [slovak]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1838, 'service_request', 'auto-close-service_request', 'slovak', 'Auto Close service_request [slovak]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1839, 'service_request', 'service_request-assigned-to-admin', 'slovak', 'New service_request Assigned (Sent to Staff) [slovak]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1840, 'service_request', 'new-service_request-opened-approval', 'slovak', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [slovak]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1841, 'service_request', 'new-service_request-opened-admin', 'czech', 'New service_request Opened (Opened by Staff, Sent to Customer) [czech]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1842, 'service_request', 'service_request-reply', 'czech', 'service_request Reply (Sent to Customer) [czech]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1843, 'service_request', 'service_request-autoresponse', 'czech', 'New service_request Opened - Autoresponse [czech]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1844, 'service_request', 'new-service_request-created-staff', 'czech', 'New service_request Created (Opened by Customer, Sent to Staff Members) [czech]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1845, 'service_request', 'service_request-reply-to-admin', 'czech', 'service_request Reply (Sent to Staff) [czech]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1846, 'service_request', 'auto-close-service_request', 'czech', 'Auto Close service_request [czech]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1847, 'service_request', 'service_request-assigned-to-admin', 'czech', 'New service_request Assigned (Sent to Staff) [czech]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1848, 'service_request', 'new-service_request-opened-approval', 'czech', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [czech]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1849, 'service_request', 'new-service_request-opened-admin', 'romanian', 'New service_request Opened (Opened by Staff, Sent to Customer) [romanian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1850, 'service_request', 'service_request-reply', 'romanian', 'service_request Reply (Sent to Customer) [romanian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1851, 'service_request', 'service_request-autoresponse', 'romanian', 'New service_request Opened - Autoresponse [romanian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1852, 'service_request', 'new-service_request-created-staff', 'romanian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [romanian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1853, 'service_request', 'service_request-reply-to-admin', 'romanian', 'service_request Reply (Sent to Staff) [romanian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1854, 'service_request', 'auto-close-service_request', 'romanian', 'Auto Close service_request [romanian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1855, 'service_request', 'service_request-assigned-to-admin', 'romanian', 'New service_request Assigned (Sent to Staff) [romanian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1856, 'service_request', 'new-service_request-opened-approval', 'romanian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [romanian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1857, 'service_request', 'new-service_request-opened-admin', 'japanese', 'New service_request Opened (Opened by Staff, Sent to Customer) [japanese]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1858, 'service_request', 'service_request-reply', 'japanese', 'service_request Reply (Sent to Customer) [japanese]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1859, 'service_request', 'service_request-autoresponse', 'japanese', 'New service_request Opened - Autoresponse [japanese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1860, 'service_request', 'new-service_request-created-staff', 'japanese', 'New service_request Created (Opened by Customer, Sent to Staff Members) [japanese]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1861, 'service_request', 'service_request-reply-to-admin', 'japanese', 'service_request Reply (Sent to Staff) [japanese]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1862, 'service_request', 'auto-close-service_request', 'japanese', 'Auto Close service_request [japanese]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1863, 'service_request', 'service_request-assigned-to-admin', 'japanese', 'New service_request Assigned (Sent to Staff) [japanese]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1864, 'service_request', 'new-service_request-opened-approval', 'japanese', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [japanese]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1865, 'service_request', 'new-service_request-opened-admin', 'indonesia', 'New service_request Opened (Opened by Staff, Sent to Customer) [indonesia]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1866, 'service_request', 'service_request-reply', 'indonesia', 'service_request Reply (Sent to Customer) [indonesia]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1867, 'service_request', 'service_request-autoresponse', 'indonesia', 'New service_request Opened - Autoresponse [indonesia]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1868, 'service_request', 'new-service_request-created-staff', 'indonesia', 'New service_request Created (Opened by Customer, Sent to Staff Members) [indonesia]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1869, 'service_request', 'service_request-reply-to-admin', 'indonesia', 'service_request Reply (Sent to Staff) [indonesia]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1870, 'service_request', 'auto-close-service_request', 'indonesia', 'Auto Close service_request [indonesia]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1871, 'service_request', 'service_request-assigned-to-admin', 'indonesia', 'New service_request Assigned (Sent to Staff) [indonesia]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1872, 'service_request', 'new-service_request-opened-approval', 'indonesia', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [indonesia]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1873, 'service_request', 'new-service_request-opened-admin', 'bulgarian', 'New service_request Opened (Opened by Staff, Sent to Customer) [bulgarian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1874, 'service_request', 'service_request-reply', 'bulgarian', 'service_request Reply (Sent to Customer) [bulgarian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1875, 'service_request', 'service_request-autoresponse', 'bulgarian', 'New service_request Opened - Autoresponse [bulgarian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1876, 'service_request', 'new-service_request-created-staff', 'bulgarian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [bulgarian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1877, 'service_request', 'service_request-reply-to-admin', 'bulgarian', 'service_request Reply (Sent to Staff) [bulgarian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1878, 'service_request', 'auto-close-service_request', 'bulgarian', 'Auto Close service_request [bulgarian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1879, 'service_request', 'service_request-assigned-to-admin', 'bulgarian', 'New service_request Assigned (Sent to Staff) [bulgarian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1880, 'service_request', 'new-service_request-opened-approval', 'bulgarian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [bulgarian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1881, 'service_request', 'new-service_request-opened-admin', 'vietnamese', 'New service_request Opened (Opened by Staff, Sent to Customer) [vietnamese]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1882, 'service_request', 'service_request-reply', 'vietnamese', 'service_request Reply (Sent to Customer) [vietnamese]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1883, 'service_request', 'service_request-autoresponse', 'vietnamese', 'New service_request Opened - Autoresponse [vietnamese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1884, 'service_request', 'new-service_request-created-staff', 'vietnamese', 'New service_request Created (Opened by Customer, Sent to Staff Members) [vietnamese]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1885, 'service_request', 'service_request-reply-to-admin', 'vietnamese', 'service_request Reply (Sent to Staff) [vietnamese]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1886, 'service_request', 'auto-close-service_request', 'vietnamese', 'Auto Close service_request [vietnamese]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1887, 'service_request', 'service_request-assigned-to-admin', 'vietnamese', 'New service_request Assigned (Sent to Staff) [vietnamese]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1888, 'service_request', 'new-service_request-opened-approval', 'vietnamese', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [vietnamese]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1889, 'service_request', 'new-service_request-opened-admin', 'german', 'New service_request Opened (Opened by Staff, Sent to Customer) [german]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1890, 'service_request', 'service_request-reply', 'german', 'service_request Reply (Sent to Customer) [german]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1891, 'service_request', 'service_request-autoresponse', 'german', 'New service_request Opened - Autoresponse [german]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1892, 'service_request', 'new-service_request-created-staff', 'german', 'New service_request Created (Opened by Customer, Sent to Staff Members) [german]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1893, 'service_request', 'service_request-reply-to-admin', 'german', 'service_request Reply (Sent to Staff) [german]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1894, 'service_request', 'auto-close-service_request', 'german', 'Auto Close service_request [german]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1895, 'service_request', 'service_request-assigned-to-admin', 'german', 'New service_request Assigned (Sent to Staff) [german]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1896, 'service_request', 'new-service_request-opened-approval', 'german', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [german]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1897, 'service_request', 'new-service_request-opened-admin', 'french', 'New service_request Opened (Opened by Staff, Sent to Customer) [french]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1898, 'service_request', 'service_request-reply', 'french', 'service_request Reply (Sent to Customer) [french]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1899, 'service_request', 'service_request-autoresponse', 'french', 'New service_request Opened - Autoresponse [french]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1900, 'service_request', 'new-service_request-created-staff', 'french', 'New service_request Created (Opened by Customer, Sent to Staff Members) [french]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1901, 'service_request', 'service_request-reply-to-admin', 'french', 'service_request Reply (Sent to Staff) [french]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1902, 'service_request', 'auto-close-service_request', 'french', 'Auto Close service_request [french]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1903, 'service_request', 'service_request-assigned-to-admin', 'french', 'New service_request Assigned (Sent to Staff) [french]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1904, 'service_request', 'new-service_request-opened-approval', 'french', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [french]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1905, 'service_request', 'new-service_request-opened-admin', 'italian', 'New service_request Opened (Opened by Staff, Sent to Customer) [italian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1906, 'service_request', 'service_request-reply', 'italian', 'service_request Reply (Sent to Customer) [italian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1907, 'service_request', 'service_request-autoresponse', 'italian', 'New service_request Opened - Autoresponse [italian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1908, 'service_request', 'new-service_request-created-staff', 'italian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [italian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1909, 'service_request', 'service_request-reply-to-admin', 'italian', 'service_request Reply (Sent to Staff) [italian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1910, 'service_request', 'auto-close-service_request', 'italian', 'Auto Close service_request [italian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1911, 'service_request', 'service_request-assigned-to-admin', 'italian', 'New service_request Assigned (Sent to Staff) [italian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1912, 'service_request', 'new-service_request-opened-approval', 'italian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [italian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1913, 'service_request', 'new-service_request-opened-admin', 'spanish', 'New service_request Opened (Opened by Staff, Sent to Customer) [spanish]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1914, 'service_request', 'service_request-reply', 'spanish', 'service_request Reply (Sent to Customer) [spanish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1915, 'service_request', 'service_request-autoresponse', 'spanish', 'New service_request Opened - Autoresponse [spanish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1916, 'service_request', 'new-service_request-created-staff', 'spanish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [spanish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1917, 'service_request', 'service_request-reply-to-admin', 'spanish', 'service_request Reply (Sent to Staff) [spanish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1918, 'service_request', 'auto-close-service_request', 'spanish', 'Auto Close service_request [spanish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1919, 'service_request', 'service_request-assigned-to-admin', 'spanish', 'New service_request Assigned (Sent to Staff) [spanish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1920, 'service_request', 'new-service_request-opened-approval', 'spanish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [spanish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1921, 'service_request', 'new-service_request-opened-admin', 'russian', 'New service_request Opened (Opened by Staff, Sent to Customer) [russian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1922, 'service_request', 'service_request-reply', 'russian', 'service_request Reply (Sent to Customer) [russian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1923, 'service_request', 'service_request-autoresponse', 'russian', 'New service_request Opened - Autoresponse [russian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1924, 'service_request', 'new-service_request-created-staff', 'russian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [russian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1925, 'service_request', 'service_request-reply-to-admin', 'russian', 'service_request Reply (Sent to Staff) [russian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1926, 'service_request', 'auto-close-service_request', 'russian', 'Auto Close service_request [russian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1927, 'service_request', 'service_request-assigned-to-admin', 'russian', 'New service_request Assigned (Sent to Staff) [russian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1928, 'service_request', 'new-service_request-opened-approval', 'russian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [russian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1929, 'service_request', 'new-service_request-opened-admin', 'catalan', 'New service_request Opened (Opened by Staff, Sent to Customer) [catalan]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1930, 'service_request', 'service_request-reply', 'catalan', 'service_request Reply (Sent to Customer) [catalan]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1931, 'service_request', 'service_request-autoresponse', 'catalan', 'New service_request Opened - Autoresponse [catalan]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1932, 'service_request', 'new-service_request-created-staff', 'catalan', 'New service_request Created (Opened by Customer, Sent to Staff Members) [catalan]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1933, 'service_request', 'service_request-reply-to-admin', 'catalan', 'service_request Reply (Sent to Staff) [catalan]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1934, 'service_request', 'auto-close-service_request', 'catalan', 'Auto Close service_request [catalan]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1935, 'service_request', 'service_request-assigned-to-admin', 'catalan', 'New service_request Assigned (Sent to Staff) [catalan]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1936, 'service_request', 'new-service_request-opened-approval', 'catalan', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [catalan]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1937, 'service_request', 'new-service_request-opened-admin', 'persian', 'New service_request Opened (Opened by Staff, Sent to Customer) [persian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1938, 'service_request', 'service_request-reply', 'persian', 'service_request Reply (Sent to Customer) [persian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1939, 'service_request', 'service_request-autoresponse', 'persian', 'New service_request Opened - Autoresponse [persian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1940, 'service_request', 'new-service_request-created-staff', 'persian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [persian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1941, 'service_request', 'service_request-reply-to-admin', 'persian', 'service_request Reply (Sent to Staff) [persian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1942, 'service_request', 'auto-close-service_request', 'persian', 'Auto Close service_request [persian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1943, 'service_request', 'service_request-assigned-to-admin', 'persian', 'New service_request Assigned (Sent to Staff) [persian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1944, 'service_request', 'new-service_request-opened-approval', 'persian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [persian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1945, 'service_request', 'new-service_request-opened-admin', 'turkish', 'New service_request Opened (Opened by Staff, Sent to Customer) [turkish]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1946, 'service_request', 'service_request-reply', 'turkish', 'service_request Reply (Sent to Customer) [turkish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1947, 'service_request', 'service_request-autoresponse', 'turkish', 'New service_request Opened - Autoresponse [turkish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1948, 'service_request', 'new-service_request-created-staff', 'turkish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [turkish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1949, 'service_request', 'service_request-reply-to-admin', 'turkish', 'service_request Reply (Sent to Staff) [turkish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1950, 'service_request', 'auto-close-service_request', 'turkish', 'Auto Close service_request [turkish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1951, 'service_request', 'service_request-assigned-to-admin', 'turkish', 'New service_request Assigned (Sent to Staff) [turkish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1952, 'service_request', 'new-service_request-opened-approval', 'turkish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [turkish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1953, 'service_request', 'new-service_request-opened-admin', 'greek', 'New service_request Opened (Opened by Staff, Sent to Customer) [greek]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1954, 'service_request', 'service_request-reply', 'greek', 'service_request Reply (Sent to Customer) [greek]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1955, 'service_request', 'service_request-autoresponse', 'greek', 'New service_request Opened - Autoresponse [greek]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1956, 'service_request', 'new-service_request-created-staff', 'greek', 'New service_request Created (Opened by Customer, Sent to Staff Members) [greek]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1957, 'service_request', 'service_request-reply-to-admin', 'greek', 'service_request Reply (Sent to Staff) [greek]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1958, 'service_request', 'auto-close-service_request', 'greek', 'Auto Close service_request [greek]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1959, 'service_request', 'service_request-assigned-to-admin', 'greek', 'New service_request Assigned (Sent to Staff) [greek]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1960, 'service_request', 'new-service_request-opened-approval', 'greek', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [greek]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1961, 'service_request', 'new-service_request-opened-admin', 'portuguese', 'New service_request Opened (Opened by Staff, Sent to Customer) [portuguese]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1962, 'service_request', 'service_request-reply', 'portuguese', 'service_request Reply (Sent to Customer) [portuguese]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1963, 'service_request', 'service_request-autoresponse', 'portuguese', 'New service_request Opened - Autoresponse [portuguese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1964, 'service_request', 'new-service_request-created-staff', 'portuguese', 'New service_request Created (Opened by Customer, Sent to Staff Members) [portuguese]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1965, 'service_request', 'service_request-reply-to-admin', 'portuguese', 'service_request Reply (Sent to Staff) [portuguese]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1966, 'service_request', 'auto-close-service_request', 'portuguese', 'Auto Close service_request [portuguese]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1967, 'service_request', 'service_request-assigned-to-admin', 'portuguese', 'New service_request Assigned (Sent to Staff) [portuguese]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1968, 'service_request', 'new-service_request-opened-approval', 'portuguese', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [portuguese]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1969, 'service_request', 'new-service_request-opened-admin', 'ukrainian', 'New service_request Opened (Opened by Staff, Sent to Customer) [ukrainian]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1970, 'service_request', 'new-service_request-opened-admin', 'ukrainian', 'New service_request Opened (Opened by Staff, Sent to Customer) [ukrainian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1971, 'service_request', 'service_request-reply', 'ukrainian', 'service_request Reply (Sent to Customer) [ukrainian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1972, 'service_request', 'service_request-reply', 'ukrainian', 'service_request Reply (Sent to Customer) [ukrainian]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1973, 'service_request', 'service_request-autoresponse', 'ukrainian', 'New service_request Opened - Autoresponse [ukrainian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1974, 'service_request', 'service_request-autoresponse', 'ukrainian', 'New service_request Opened - Autoresponse [ukrainian]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1975, 'service_request', 'new-service_request-created-staff', 'ukrainian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [ukrainian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1976, 'service_request', 'new-service_request-created-staff', 'ukrainian', 'New service_request Created (Opened by Customer, Sent to Staff Members) [ukrainian]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1977, 'service_request', 'service_request-reply-to-admin', 'ukrainian', 'service_request Reply (Sent to Staff) [ukrainian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1978, 'service_request', 'service_request-reply-to-admin', 'ukrainian', 'service_request Reply (Sent to Staff) [ukrainian]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1979, 'service_request', 'auto-close-service_request', 'ukrainian', 'Auto Close service_request [ukrainian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1980, 'service_request', 'auto-close-service_request', 'ukrainian', 'Auto Close service_request [ukrainian]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1981, 'service_request', 'service_request-assigned-to-admin', 'ukrainian', 'New service_request Assigned (Sent to Staff) [ukrainian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1982, 'service_request', 'service_request-assigned-to-admin', 'ukrainian', 'New service_request Assigned (Sent to Staff) [ukrainian]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1983, 'service_request', 'new-service_request-opened-approval', 'ukrainian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [ukrainian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1984, 'service_request', 'new-service_request-opened-approval', 'ukrainian', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [ukrainian]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1985, 'service_request', 'new-service_request-opened-admin', 'swedish', 'New service_request Opened (Opened by Staff, Sent to Customer) [swedish]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(1986, 'service_request', 'new-service_request-opened-admin', 'swedish', 'New service_request Opened (Opened by Staff, Sent to Customer) [swedish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1987, 'service_request', 'service_request-reply', 'swedish', 'service_request Reply (Sent to Customer) [swedish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1988, 'service_request', 'service_request-reply', 'swedish', 'service_request Reply (Sent to Customer) [swedish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1989, 'service_request', 'service_request-autoresponse', 'swedish', 'New service_request Opened - Autoresponse [swedish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1990, 'service_request', 'service_request-autoresponse', 'swedish', 'New service_request Opened - Autoresponse [swedish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1991, 'service_request', 'new-service_request-created-staff', 'swedish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [swedish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1992, 'service_request', 'new-service_request-created-staff', 'swedish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [swedish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1993, 'service_request', 'service_request-reply-to-admin', 'swedish', 'service_request Reply (Sent to Staff) [swedish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1994, 'service_request', 'service_request-reply-to-admin', 'swedish', 'service_request Reply (Sent to Staff) [swedish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1995, 'service_request', 'auto-close-service_request', 'swedish', 'Auto Close service_request [swedish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1996, 'service_request', 'auto-close-service_request', 'swedish', 'Auto Close service_request [swedish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1997, 'service_request', 'service_request-assigned-to-admin', 'swedish', 'New service_request Assigned (Sent to Staff) [swedish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1998, 'service_request', 'service_request-assigned-to-admin', 'swedish', 'New service_request Assigned (Sent to Staff) [swedish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(1999, 'service_request', 'new-service_request-opened-approval', 'swedish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [swedish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2000, 'service_request', 'new-service_request-opened-approval', 'swedish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [swedish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2001, 'service_request', 'new-service_request-opened-admin', 'polish', 'New service_request Opened (Opened by Staff, Sent to Customer) [polish]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2002, 'service_request', 'new-service_request-opened-admin', 'polish', 'New service_request Opened (Opened by Staff, Sent to Customer) [polish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2003, 'service_request', 'service_request-reply', 'polish', 'service_request Reply (Sent to Customer) [polish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2004, 'service_request', 'service_request-reply', 'polish', 'service_request Reply (Sent to Customer) [polish]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2005, 'service_request', 'service_request-autoresponse', 'polish', 'New service_request Opened - Autoresponse [polish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2006, 'service_request', 'service_request-autoresponse', 'polish', 'New service_request Opened - Autoresponse [polish]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2007, 'service_request', 'new-service_request-created-staff', 'polish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [polish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2008, 'service_request', 'new-service_request-created-staff', 'polish', 'New service_request Created (Opened by Customer, Sent to Staff Members) [polish]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2009, 'service_request', 'service_request-reply-to-admin', 'polish', 'service_request Reply (Sent to Staff) [polish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2010, 'service_request', 'service_request-reply-to-admin', 'polish', 'service_request Reply (Sent to Staff) [polish]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2011, 'service_request', 'auto-close-service_request', 'polish', 'Auto Close service_request [polish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2012, 'service_request', 'auto-close-service_request', 'polish', 'Auto Close service_request [polish]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2013, 'service_request', 'service_request-assigned-to-admin', 'polish', 'New service_request Assigned (Sent to Staff) [polish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2014, 'service_request', 'service_request-assigned-to-admin', 'polish', 'New service_request Assigned (Sent to Staff) [polish]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2015, 'service_request', 'new-service_request-opened-approval', 'polish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [polish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2016, 'service_request', 'new-service_request-opened-approval', 'polish', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [polish]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2017, 'service_request', 'new-service_request-opened-admin', 'dutch', 'New service_request Opened (Opened by Staff, Sent to Customer) [dutch]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2018, 'service_request', 'new-service_request-opened-admin', 'dutch', 'New service_request Opened (Opened by Staff, Sent to Customer) [dutch]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2019, 'service_request', 'service_request-reply', 'dutch', 'service_request Reply (Sent to Customer) [dutch]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2020, 'service_request', 'service_request-reply', 'dutch', 'service_request Reply (Sent to Customer) [dutch]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2021, 'service_request', 'service_request-autoresponse', 'dutch', 'New service_request Opened - Autoresponse [dutch]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2022, 'service_request', 'service_request-autoresponse', 'dutch', 'New service_request Opened - Autoresponse [dutch]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2023, 'service_request', 'new-service_request-created-staff', 'dutch', 'New service_request Created (Opened by Customer, Sent to Staff Members) [dutch]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2024, 'service_request', 'new-service_request-created-staff', 'dutch', 'New service_request Created (Opened by Customer, Sent to Staff Members) [dutch]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2025, 'service_request', 'service_request-reply-to-admin', 'dutch', 'service_request Reply (Sent to Staff) [dutch]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2026, 'service_request', 'service_request-reply-to-admin', 'dutch', 'service_request Reply (Sent to Staff) [dutch]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2027, 'service_request', 'auto-close-service_request', 'dutch', 'Auto Close service_request [dutch]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2028, 'service_request', 'auto-close-service_request', 'dutch', 'Auto Close service_request [dutch]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2029, 'service_request', 'service_request-assigned-to-admin', 'dutch', 'New service_request Assigned (Sent to Staff) [dutch]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2030, 'service_request', 'service_request-assigned-to-admin', 'dutch', 'New service_request Assigned (Sent to Staff) [dutch]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2031, 'service_request', 'new-service_request-opened-approval', 'dutch', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [dutch]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2032, 'service_request', 'new-service_request-opened-approval', 'dutch', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [dutch]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2033, 'service_request', 'new-service_request-opened-admin', 'chinese', 'New service_request Opened (Opened by Staff, Sent to Customer) [chinese]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2034, 'service_request', 'new-service_request-opened-admin', 'chinese', 'New service_request Opened (Opened by Staff, Sent to Customer) [chinese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2035, 'service_request', 'service_request-reply', 'chinese', 'service_request Reply (Sent to Customer) [chinese]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2036, 'service_request', 'service_request-reply', 'chinese', 'service_request Reply (Sent to Customer) [chinese]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2037, 'service_request', 'service_request-autoresponse', 'chinese', 'New service_request Opened - Autoresponse [chinese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2038, 'service_request', 'service_request-autoresponse', 'chinese', 'New service_request Opened - Autoresponse [chinese]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2039, 'service_request', 'new-service_request-created-staff', 'chinese', 'New service_request Created (Opened by Customer, Sent to Staff Members) [chinese]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2040, 'service_request', 'new-service_request-created-staff', 'chinese', 'New service_request Created (Opened by Customer, Sent to Staff Members) [chinese]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2041, 'service_request', 'service_request-reply-to-admin', 'chinese', 'service_request Reply (Sent to Staff) [chinese]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2042, 'service_request', 'service_request-reply-to-admin', 'chinese', 'service_request Reply (Sent to Staff) [chinese]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2043, 'service_request', 'auto-close-service_request', 'chinese', 'Auto Close service_request [chinese]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2044, 'service_request', 'auto-close-service_request', 'chinese', 'Auto Close service_request [chinese]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2045, 'service_request', 'service_request-assigned-to-admin', 'chinese', 'New service_request Assigned (Sent to Staff) [chinese]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2046, 'service_request', 'service_request-assigned-to-admin', 'chinese', 'New service_request Assigned (Sent to Staff) [chinese]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2047, 'service_request', 'new-service_request-opened-approval', 'chinese', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [chinese]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2048, 'service_request', 'new-service_request-opened-approval', 'chinese', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [chinese]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2049, 'service_request', 'new-service_request-opened-admin', 'portuguese_br', 'New service_request Opened (Opened by Staff, Sent to Customer) [portuguese_br]', 'New Support service_request Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2050, 'service_request', 'new-service_request-opened-admin', 'portuguese_br', 'New service_request Opened (Opened by Staff, Sent to Customer) [portuguese_br]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2051, 'service_request', 'service_request-reply', 'portuguese_br', 'service_request Reply (Sent to Customer) [portuguese_br]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2052, 'service_request', 'service_request-reply', 'portuguese_br', 'service_request Reply (Sent to Customer) [portuguese_br]', 'New service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2053, 'service_request', 'service_request-autoresponse', 'portuguese_br', 'New service_request Opened - Autoresponse [portuguese_br]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2054, 'service_request', 'service_request-autoresponse', 'portuguese_br', 'New service_request Opened - Autoresponse [portuguese_br]', 'New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2055, 'service_request', 'new-service_request-created-staff', 'portuguese_br', 'New service_request Created (Opened by Customer, Sent to Staff Members) [portuguese_br]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2056, 'service_request', 'new-service_request-created-staff', 'portuguese_br', 'New service_request Created (Opened by Customer, Sent to Staff Members) [portuguese_br]', 'New service_request Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2057, 'service_request', 'service_request-reply-to-admin', 'portuguese_br', 'service_request Reply (Sent to Staff) [portuguese_br]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2058, 'service_request', 'service_request-reply-to-admin', 'portuguese_br', 'service_request Reply (Sent to Staff) [portuguese_br]', 'New Support service_request Reply', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2059, 'service_request', 'auto-close-service_request', 'portuguese_br', 'Auto Close service_request [portuguese_br]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2060, 'service_request', 'auto-close-service_request', 'portuguese_br', 'Auto Close service_request [portuguese_br]', 'service_request Auto Closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2061, 'service_request', 'service_request-assigned-to-admin', 'portuguese_br', 'New service_request Assigned (Sent to Staff) [portuguese_br]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2062, 'service_request', 'service_request-assigned-to-admin', 'portuguese_br', 'New service_request Assigned (Sent to Staff) [portuguese_br]', 'New support service_request has been assigned to you', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2063, 'service_request', 'new-service_request-opened-approval', 'portuguese_br', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [portuguese_br]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2064, 'service_request', 'new-service_request-opened-approval', 'portuguese_br', 'New service_request Opened (Opened by Staff, Sent to Approval authority) [portuguese_br]', 'Approve : New Support service_request Opened', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2065, 'ticket', 'ticket-assigned-to-customer', 'english', 'New Ticket Assigned (Sent to Customer)', 'New support ticket has been assigned to staff', '<p><span style="font-size: 12pt;">Hi</span></p>\r\n<p><span style="font-size: 12pt;">A new support ticket has been assigned to {contact_firstname} {contact_lastname}</span><span class="pull-right"></span></p>\r\n<p><span style="font-size: 12pt;"></span><br /><br /><span style="font-size: 12pt;"><strong>Subject</strong>: {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department</strong>: {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority</strong>: {ticket_priority}</span><br /><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_url}">#{ticket_id}</a></span><br /><br /><span style="font-size: 12pt;">Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span></p>', '{companyname} | CRM', '', 0, 1, 0),
	(2066, 'ticket', 'ticket-assigned-to-customer', 'slovak', 'New Ticket Assigned (Sent to Customer) [slovak]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2067, 'ticket', 'ticket-assigned-to-customer', 'czech', 'New Ticket Assigned (Sent to Customer) [czech]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2068, 'ticket', 'ticket-assigned-to-customer', 'romanian', 'New Ticket Assigned (Sent to Customer) [romanian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2069, 'ticket', 'ticket-assigned-to-customer', 'japanese', 'New Ticket Assigned (Sent to Customer) [japanese]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2070, 'ticket', 'ticket-assigned-to-customer', 'indonesia', 'New Ticket Assigned (Sent to Customer) [indonesia]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2071, 'ticket', 'ticket-assigned-to-customer', 'bulgarian', 'New Ticket Assigned (Sent to Customer) [bulgarian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2072, 'ticket', 'ticket-assigned-to-customer', 'vietnamese', 'New Ticket Assigned (Sent to Customer) [vietnamese]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2073, 'ticket', 'ticket-assigned-to-customer', 'german', 'New Ticket Assigned (Sent to Customer) [german]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2074, 'ticket', 'ticket-assigned-to-customer', 'french', 'New Ticket Assigned (Sent to Customer) [french]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2075, 'ticket', 'ticket-assigned-to-customer', 'italian', 'New Ticket Assigned (Sent to Customer) [italian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2076, 'ticket', 'ticket-assigned-to-customer', 'spanish', 'New Ticket Assigned (Sent to Customer) [spanish]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2077, 'ticket', 'ticket-assigned-to-customer', 'russian', 'New Ticket Assigned (Sent to Customer) [russian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2078, 'ticket', 'ticket-assigned-to-customer', 'catalan', 'New Ticket Assigned (Sent to Customer) [catalan]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2079, 'ticket', 'ticket-assigned-to-customer', 'persian', 'New Ticket Assigned (Sent to Customer) [persian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2080, 'ticket', 'ticket-assigned-to-customer', 'turkish', 'New Ticket Assigned (Sent to Customer) [turkish]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2081, 'ticket', 'ticket-assigned-to-customer', 'greek', 'New Ticket Assigned (Sent to Customer) [greek]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2082, 'ticket', 'ticket-assigned-to-customer', 'portuguese', 'New Ticket Assigned (Sent to Customer) [portuguese]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2083, 'ticket', 'ticket-assigned-to-customer', 'ukrainian', 'New Ticket Assigned (Sent to Customer) [ukrainian]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2084, 'ticket', 'ticket-assigned-to-customer', 'swedish', 'New Ticket Assigned (Sent to Customer) [swedish]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2085, 'ticket', 'ticket-assigned-to-customer', 'polish', 'New Ticket Assigned (Sent to Customer) [polish]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2086, 'ticket', 'ticket-assigned-to-customer', 'dutch', 'New Ticket Assigned (Sent to Customer) [dutch]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2087, 'ticket', 'ticket-assigned-to-customer', 'chinese', 'New Ticket Assigned (Sent to Customer) [chinese]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2088, 'ticket', 'ticket-assigned-to-customer', 'portuguese_br', 'New Ticket Assigned (Sent to Customer) [portuguese_br]', 'New support ticket has been assigned to staff', '', '{companyname} | CRM', '', 0, 1, 0),
	(2089, 'ticket', 'ticket-exceded-slato-staff', 'english', 'Ticket Exceded Sla time mail to Staff member', 'Ticket SLA Time is Exceed', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br />Ticket SLA Time Is Exceed.<br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}<br /></span><span style="font-size: 12pt;"><strong><br />Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;">You can view the ticket on the following link: <a href="{ticket_public_url}">#{ticket_id}</a><br /><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(2090, 'ticket', 'ticket-exceded-slato-staff', 'slovak', 'Ticket Exceded Sla time mail to Staff member [slovak]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2091, 'ticket', 'ticket-exceded-slato-staff', 'czech', 'Ticket Exceded Sla time mail to Staff member [czech]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2092, 'ticket', 'ticket-exceded-slato-staff', 'romanian', 'Ticket Exceded Sla time mail to Staff member [romanian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2093, 'ticket', 'ticket-exceded-slato-staff', 'japanese', 'Ticket Exceded Sla time mail to Staff member [japanese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2094, 'ticket', 'ticket-exceded-slato-staff', 'indonesia', 'Ticket Exceded Sla time mail to Staff member [indonesia]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2095, 'ticket', 'ticket-exceded-slato-staff', 'bulgarian', 'Ticket Exceded Sla time mail to Staff member [bulgarian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2096, 'ticket', 'ticket-exceded-slato-staff', 'vietnamese', 'Ticket Exceded Sla time mail to Staff member [vietnamese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2097, 'ticket', 'ticket-exceded-slato-staff', 'german', 'Ticket Exceded Sla time mail to Staff member [german]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2098, 'ticket', 'ticket-exceded-slato-staff', 'french', 'Ticket Exceded Sla time mail to Staff member [french]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2099, 'ticket', 'ticket-exceded-slato-staff', 'italian', 'Ticket Exceded Sla time mail to Staff member [italian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2100, 'ticket', 'ticket-exceded-slato-staff', 'spanish', 'Ticket Exceded Sla time mail to Staff member [spanish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2101, 'ticket', 'ticket-exceded-slato-staff', 'russian', 'Ticket Exceded Sla time mail to Staff member [russian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2102, 'ticket', 'ticket-exceded-slato-staff', 'catalan', 'Ticket Exceded Sla time mail to Staff member [catalan]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2103, 'ticket', 'ticket-exceded-slato-staff', 'persian', 'Ticket Exceded Sla time mail to Staff member [persian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2104, 'ticket', 'ticket-exceded-slato-staff', 'turkish', 'Ticket Exceded Sla time mail to Staff member [turkish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2105, 'ticket', 'ticket-exceded-slato-staff', 'greek', 'Ticket Exceded Sla time mail to Staff member [greek]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2106, 'ticket', 'ticket-exceded-slato-staff', 'portuguese', 'Ticket Exceded Sla time mail to Staff member [portuguese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2107, 'ticket', 'ticket-exceded-slato-staff', 'ukrainian', 'Ticket Exceded Sla time mail to Staff member [ukrainian]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2108, 'ticket', 'ticket-exceded-slato-staff', 'swedish', 'Ticket Exceded Sla time mail to Staff member [swedish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2109, 'ticket', 'ticket-exceded-slato-staff', 'polish', 'Ticket Exceded Sla time mail to Staff member [polish]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2110, 'ticket', 'ticket-exceded-slato-staff', 'dutch', 'Ticket Exceded Sla time mail to Staff member [dutch]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2111, 'ticket', 'ticket-exceded-slato-staff', 'chinese', 'Ticket Exceded Sla time mail to Staff member [chinese]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2112, 'ticket', 'ticket-exceded-slato-staff', 'portuguese_br', 'Ticket Exceded Sla time mail to Staff member [portuguese_br]', 'New Support Ticket Opened', '', '{companyname} | CRM', '', 0, 1, 0),
	(2113, 'ticket', 'new-ticket-closed-admin', 'english', 'Ticket Closed (Opened by Staff, Sent to Customer)', 'Support Ticket closed', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support ticket has closed.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}<br /></span><strong><strong>Billing Type : </strong></strong><span style="font-size: 12pt;">{ticket_billing_type}<br /><strong>Contract : </strong>{ticket_contract}</span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;"><a href="{ticket_public_url}"></a><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(2114, 'ticket', 'new-ticket-closed-admin', 'slovak', 'Ticket Closed (Opened by Staff, Sent to Customer) [slovak]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2115, 'ticket', 'new-ticket-closed-admin', 'czech', 'Ticket Closed (Opened by Staff, Sent to Customer) [czech]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2116, 'ticket', 'new-ticket-closed-admin', 'romanian', 'Ticket Closed (Opened by Staff, Sent to Customer) [romanian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2117, 'ticket', 'new-ticket-closed-admin', 'japanese', 'Ticket Closed (Opened by Staff, Sent to Customer) [japanese]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2118, 'ticket', 'new-ticket-closed-admin', 'indonesia', 'Ticket Closed (Opened by Staff, Sent to Customer) [indonesia]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2119, 'ticket', 'new-ticket-closed-admin', 'bulgarian', 'Ticket Closed (Opened by Staff, Sent to Customer) [bulgarian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2120, 'ticket', 'new-ticket-closed-admin', 'vietnamese', 'Ticket Closed (Opened by Staff, Sent to Customer) [vietnamese]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2121, 'ticket', 'new-ticket-closed-admin', 'german', 'Ticket Closed (Opened by Staff, Sent to Customer) [german]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2122, 'ticket', 'new-ticket-closed-admin', 'french', 'Ticket Closed (Opened by Staff, Sent to Customer) [french]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2123, 'ticket', 'new-ticket-closed-admin', 'italian', 'Ticket Closed (Opened by Staff, Sent to Customer) [italian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2124, 'ticket', 'new-ticket-closed-admin', 'spanish', 'Ticket Closed (Opened by Staff, Sent to Customer) [spanish]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2125, 'ticket', 'new-ticket-closed-admin', 'russian', 'Ticket Closed (Opened by Staff, Sent to Customer) [russian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2126, 'ticket', 'new-ticket-closed-admin', 'catalan', 'Ticket Closed (Opened by Staff, Sent to Customer) [catalan]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2127, 'ticket', 'new-ticket-closed-admin', 'persian', 'Ticket Closed (Opened by Staff, Sent to Customer) [persian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2128, 'ticket', 'new-ticket-closed-admin', 'turkish', 'Ticket Closed (Opened by Staff, Sent to Customer) [turkish]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2129, 'ticket', 'new-ticket-closed-admin', 'greek', 'Ticket Closed (Opened by Staff, Sent to Customer) [greek]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2130, 'ticket', 'new-ticket-closed-admin', 'portuguese', 'Ticket Closed (Opened by Staff, Sent to Customer) [portuguese]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2131, 'ticket', 'new-ticket-closed-admin', 'ukrainian', 'Ticket Closed (Opened by Staff, Sent to Customer) [ukrainian]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2132, 'ticket', 'new-ticket-closed-admin', 'swedish', 'Ticket Closed (Opened by Staff, Sent to Customer) [swedish]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2133, 'ticket', 'new-ticket-closed-admin', 'polish', 'Ticket Closed (Opened by Staff, Sent to Customer) [polish]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2134, 'ticket', 'new-ticket-closed-admin', 'dutch', 'Ticket Closed (Opened by Staff, Sent to Customer) [dutch]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2135, 'ticket', 'new-ticket-closed-admin', 'chinese', 'Ticket Closed (Opened by Staff, Sent to Customer) [chinese]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2136, 'ticket', 'new-ticket-closed-admin', 'portuguese_br', 'Ticket Closed (Opened by Staff, Sent to Customer) [portuguese_br]', 'Support Ticket closed', '', '{companyname} | CRM', '', 0, 1, 0),
	(2137, 'service_request', 'new-service_request-closed-admin', 'english', 'Service request Closed (Opened by Staff, Sent to Customer)', 'Support Service Request closed', '<span style="font-size: 12pt;">Hi {contact_firstname} {contact_lastname}</span><br /><br /><span style="font-size: 12pt;">New support ticket has closed.</span><br /><br /><span style="font-size: 12pt;"><strong>Subject:</strong> {ticket_subject}</span><br /><span style="font-size: 12pt;"><strong>Department:</strong> {ticket_department}</span><br /><span style="font-size: 12pt;"><strong>Priority:</strong> {ticket_priority}<br /></span><strong><strong>Billing Type : </strong></strong><span style="font-size: 12pt;">{ticket_billing_type}<br /><strong>Contract : </strong>{ticket_contract}</span><br /><span style="font-size: 12pt;"><strong>Ticket message:</strong></span><br /><span style="font-size: 12pt;">{ticket_message}</span><br /><br /><span style="font-size: 12pt;"><a href="{ticket_public_url}"></a><br />Kind Regards,</span><br /><span style="font-size: 12pt;">{email_signature}</span>', '{companyname} | CRM', '', 0, 1, 0),
	(2138, 'service_request', 'new-service_request-closed-admin', 'slovak', 'Service request Closed (Opened by Staff, Sent to Customer) [slovak]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2139, 'service_request', 'new-service_request-closed-admin', 'czech', 'Service request Closed (Opened by Staff, Sent to Customer) [czech]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2140, 'service_request', 'new-service_request-closed-admin', 'romanian', 'Service request Closed (Opened by Staff, Sent to Customer) [romanian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2141, 'service_request', 'new-service_request-closed-admin', 'japanese', 'Service request Closed (Opened by Staff, Sent to Customer) [japanese]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2142, 'service_request', 'new-service_request-closed-admin', 'indonesia', 'Service request Closed (Opened by Staff, Sent to Customer) [indonesia]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2143, 'service_request', 'new-service_request-closed-admin', 'bulgarian', 'Service request Closed (Opened by Staff, Sent to Customer) [bulgarian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2144, 'service_request', 'new-service_request-closed-admin', 'vietnamese', 'Service request Closed (Opened by Staff, Sent to Customer) [vietnamese]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2145, 'service_request', 'new-service_request-closed-admin', 'german', 'Service request Closed (Opened by Staff, Sent to Customer) [german]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2146, 'service_request', 'new-service_request-closed-admin', 'french', 'Service request Closed (Opened by Staff, Sent to Customer) [french]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2147, 'service_request', 'new-service_request-closed-admin', 'italian', 'Service request Closed (Opened by Staff, Sent to Customer) [italian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2148, 'service_request', 'new-service_request-closed-admin', 'spanish', 'Service request Closed (Opened by Staff, Sent to Customer) [spanish]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2149, 'service_request', 'new-service_request-closed-admin', 'russian', 'Service request Closed (Opened by Staff, Sent to Customer) [russian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2150, 'service_request', 'new-service_request-closed-admin', 'catalan', 'Service request Closed (Opened by Staff, Sent to Customer) [catalan]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2151, 'service_request', 'new-service_request-closed-admin', 'persian', 'Service request Closed (Opened by Staff, Sent to Customer) [persian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2152, 'service_request', 'new-service_request-closed-admin', 'turkish', 'Service request Closed (Opened by Staff, Sent to Customer) [turkish]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2153, 'service_request', 'new-service_request-closed-admin', 'greek', 'Service request Closed (Opened by Staff, Sent to Customer) [greek]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2154, 'service_request', 'new-service_request-closed-admin', 'portuguese', 'Service request Closed (Opened by Staff, Sent to Customer) [portuguese]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2155, 'service_request', 'new-service_request-closed-admin', 'ukrainian', 'Service request Closed (Opened by Staff, Sent to Customer) [ukrainian]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2156, 'service_request', 'new-service_request-closed-admin', 'swedish', 'Service request Closed (Opened by Staff, Sent to Customer) [swedish]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2157, 'service_request', 'new-service_request-closed-admin', 'polish', 'Service request Closed (Opened by Staff, Sent to Customer) [polish]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2158, 'service_request', 'new-service_request-closed-admin', 'dutch', 'Service request Closed (Opened by Staff, Sent to Customer) [dutch]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2159, 'service_request', 'new-service_request-closed-admin', 'chinese', 'Service request Closed (Opened by Staff, Sent to Customer) [chinese]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2160, 'service_request', 'new-service_request-closed-admin', 'portuguese_br', 'Service request Closed (Opened by Staff, Sent to Customer) [portuguese_br]', 'Support Service Request closed', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2161, 'ticket', 'reports', 'english', 'Report', 'Report', 'Dear {contact_firstname} {contact_lastname}<br /><br />Thank you for registering on the <strong>{companyname}</strong> ITSM System.<br /><br />We just wanted to say welcome.<br /><br />Please contact us if you need any help.<br /><br />Click here to view your profile: <a href="{crm_url}">{crm_url}</a><br /><br />Kind Regards, <br />{email_signature}<br /><br />(This is an automated email, so please don\'t reply to this email address)', '{companyname} | ITSM', '', 0, 1, 0),
	(2162, 'ticket', 'reports', 'bulgarian', 'Report [bulgarian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2163, 'ticket', 'reports', 'catalan', 'Report [catalan]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2164, 'ticket', 'reports', 'chinese', 'Report [chinese]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2165, 'ticket', 'reports', 'czech', 'Report [czech]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2166, 'ticket', 'reports', 'dutch', 'Report [dutch]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2167, 'ticket', 'reports', 'french', 'Report [french]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2168, 'ticket', 'reports', 'german', 'Report [german]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2169, 'ticket', 'reports', 'greek', 'Report [greek]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2170, 'ticket', 'reports', 'indonesia', 'Report [indonesia]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2171, 'ticket', 'reports', 'italian', 'Report [italian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2172, 'ticket', 'reports', 'japanese', 'Report [japanese]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2173, 'ticket', 'reports', 'persian', 'Report [persian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2174, 'ticket', 'reports', 'polish', 'Report [polish]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2175, 'ticket', 'reports', 'portuguese', 'Report [portuguese]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2176, 'ticket', 'reports', 'portuguese_br', 'Report [portuguese_br]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2177, 'ticket', 'reports', 'romanian', 'Report [romanian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2178, 'ticket', 'reports', 'russian', 'Report [russian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2179, 'ticket', 'reports', 'slovak', 'Report [slovak]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2180, 'ticket', 'reports', 'spanish', 'Report [spanish]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2181, 'ticket', 'reports', 'swedish', 'Report [swedish]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2182, 'ticket', 'reports', 'turkish', 'Report [turkish]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2183, 'ticket', 'reports', 'ukrainian', 'Report [ukrainian]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2184, 'ticket', 'reports', 'vietnamese', 'Report [vietnamese]', 'Report', '', '{companyname} | ITSM', NULL, 0, 1, 0),
	(2185, 'order_tracker', 'send-contact-order-note', 'english', 'Send Order Note to Client', 'Order Note Created', 'Hello&nbsp;{contact_firstname}&nbsp;{contact_lastname}<br /><br />We have added new order note for your order {so_number}.<br /><br />Order Status : {order_status}<br /><br />Order Note : {order_note}<br /><br />Please login to check order note.<br /><br />Best Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2186, 'order_tracker', 'send-contact-order-note', 'bulgarian', 'Send Order Note to Client [bulgarian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2187, 'order_tracker', 'send-contact-order-note', 'catalan', 'Send Order Note to Client [catalan]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2188, 'order_tracker', 'send-contact-order-note', 'chinese', 'Send Order Note to Client [chinese]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2189, 'order_tracker', 'send-contact-order-note', 'czech', 'Send Order Note to Client [czech]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2190, 'order_tracker', 'send-contact-order-note', 'dutch', 'Send Order Note to Client [dutch]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2191, 'order_tracker', 'send-contact-order-note', 'french', 'Send Order Note to Client [french]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2192, 'order_tracker', 'send-contact-order-note', 'german', 'Send Order Note to Client [german]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2193, 'order_tracker', 'send-contact-order-note', 'greek', 'Send Order Note to Client [greek]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2194, 'order_tracker', 'send-contact-order-note', 'indonesia', 'Send Order Note to Client [indonesia]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2195, 'order_tracker', 'send-contact-order-note', 'italian', 'Send Order Note to Client [italian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2196, 'order_tracker', 'send-contact-order-note', 'japanese', 'Send Order Note to Client [japanese]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2197, 'order_tracker', 'send-contact-order-note', 'persian', 'Send Order Note to Client [persian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2198, 'order_tracker', 'send-contact-order-note', 'polish', 'Send Order Note to Client [polish]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2199, 'order_tracker', 'send-contact-order-note', 'portuguese', 'Send Order Note to Client [portuguese]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2200, 'order_tracker', 'send-contact-order-note', 'portuguese_br', 'Send Order Note to Client [portuguese_br]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2201, 'order_tracker', 'send-contact-order-note', 'romanian', 'Send Order Note to Client [romanian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2202, 'order_tracker', 'send-contact-order-note', 'russian', 'Send Order Note to Client [russian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2203, 'order_tracker', 'send-contact-order-note', 'slovak', 'Send Order Note to Client [slovak]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2204, 'order_tracker', 'send-contact-order-note', 'spanish', 'Send Order Note to Client [spanish]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2205, 'order_tracker', 'send-contact-order-note', 'swedish', 'Send Order Note to Client [swedish]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2206, 'order_tracker', 'send-contact-order-note', 'turkish', 'Send Order Note to Client [turkish]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2207, 'order_tracker', 'send-contact-order-note', 'ukrainian', 'Send Order Note to Client [ukrainian]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2208, 'order_tracker', 'send-contact-order-note', 'vietnamese', 'Send Order Note to Client [vietnamese]', 'Order Note Created', '', '{companyname} | CRM', '', 0, 1, 0),
	(2209, 'boqs', 'boq-client-accepted', 'english', 'Customer Action - Accepted (Sent to Staff)', 'Customer Accepted Boq', '<div>Hi<br /> <br />Client <strong>{boq_boq_to}</strong> accepted the following boq:<br /> <br /><strong>Number:</strong> {boq_number}<br /><strong>Subject</strong>: {boq_subject}<br /><strong>Total</strong>: {boq_total}<br /> <br />View the boq on the following link: <a href="{boq_link}">{boq_number}</a><br /> <br />Kind Regards,<br />{email_signature}</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>', '{companyname} | CRM', '', 0, 1, 0),
	(2210, 'boqs', 'boq-send-to-customer', 'english', 'Send Boq to Customer', 'Boq With Number {boq_number} Created', 'Dear {boq_boq_to}<br /><br />Please find our attached boq.<br /><br />This boq is valid until: {boq_open_till}<br />You can view the boq on the following link: <a href="{boq_link}">{boq_number}</a><br /><br />Please don\'t hesitate to comment online if you have any questions.<br /><br />We look forward to your communication.<br /><br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2211, 'boqs', 'boq-client-declined', 'english', 'Customer Action - Declined (Sent to Staff)', 'Client Declined Boq', 'Hi<br /> <br />Customer <strong>{boq_boq_to}</strong> declined the boq <strong>{boq_subject}</strong><br /> <br />View the boq on the following link <a href="{boq_link}">{boq_number}</a>&nbsp;or from the admin area.<br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2212, 'boqs', 'boq-client-thank-you', 'english', 'Thank You Email (Sent to Customer After Accept)', 'Thank for you accepting boq', 'Dear {boq_boq_to}<br /> <br />Thank for for accepting the boq.<br /> <br />We look forward to doing business with you.<br /> <br />We will contact you as soon as possible<br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2213, 'boqs', 'boq-comment-to-client', 'english', 'New Comment  (Sent to Customer/Lead)', 'New Boq Comment', 'Dear {boq_boq_to}<br /> <br />A new comment has been made on the following boq: <strong>{boq_number}</strong><br /> <br />You can view and reply to the comment on the following link: <a href="{boq_link}">{boq_number}</a><br /> <br />Kind Regards,<br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2214, 'boqs', 'boq-comment-to-admin', 'english', 'New Comment (Sent to Staff) ', 'New Boq Comment', 'Hi<br /> <br />A new comment has been made to the boq <strong>{boq_subject}</strong><br /> <br />You can view and reply to the comment on the following link: <a href="{boq_link}">{boq_number}</a>&nbsp;or from the admin area.<br /> <br />{email_signature}', '{companyname} | CRM', '', 0, 1, 0),
	(2215, 'boqs', 'boq-expiry-reminder', 'english', 'Boq Expiration Reminder', 'Boq Expiration Reminder', '<p>Hello {boq_boq_to}<br /><br />The boq {boq_number}&nbsp;will expire on <strong>{boq_open_till}</strong><br /><br />You can view the boq on the following link: <a href="{boq_link}">{boq_number}</a><br /><br />Kind Regards,<br />{email_signature}</p>', '{companyname} | CRM', '', 0, 1, 0),
	(2216, 'boqs', 'boq-client-accepted', 'bulgarian', 'Customer Action - Accepted (Sent to Staff) [bulgarian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2217, 'boqs', 'boq-send-to-customer', 'bulgarian', 'Send Boq to Customer [bulgarian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2218, 'boqs', 'boq-client-declined', 'bulgarian', 'Customer Action - Declined (Sent to Staff) [bulgarian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2219, 'boqs', 'boq-client-thank-you', 'bulgarian', 'Thank You Email (Sent to Customer After Accept) [bulgarian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2220, 'boqs', 'boq-comment-to-client', 'bulgarian', 'New Comment  (Sent to Customer/Lead) [bulgarian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2221, 'boqs', 'boq-comment-to-admin', 'bulgarian', 'New Comment (Sent to Staff)  [bulgarian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2222, 'boqs', 'boq-expiry-reminder', 'bulgarian', 'Boq Expiration Reminder [bulgarian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2223, 'boqs', 'boq-client-accepted', 'catalan', 'Customer Action - Accepted (Sent to Staff) [catalan]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2224, 'boqs', 'boq-send-to-customer', 'catalan', 'Send Boq to Customer [catalan]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2225, 'boqs', 'boq-client-declined', 'catalan', 'Customer Action - Declined (Sent to Staff) [catalan]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2226, 'boqs', 'boq-client-thank-you', 'catalan', 'Thank You Email (Sent to Customer After Accept) [catalan]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2227, 'boqs', 'boq-comment-to-client', 'catalan', 'New Comment  (Sent to Customer/Lead) [catalan]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2228, 'boqs', 'boq-comment-to-admin', 'catalan', 'New Comment (Sent to Staff)  [catalan]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2229, 'boqs', 'boq-expiry-reminder', 'catalan', 'Boq Expiration Reminder [catalan]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2230, 'boqs', 'boq-client-accepted', 'chinese', 'Customer Action - Accepted (Sent to Staff) [chinese]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2231, 'boqs', 'boq-send-to-customer', 'chinese', 'Send Boq to Customer [chinese]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2232, 'boqs', 'boq-client-declined', 'chinese', 'Customer Action - Declined (Sent to Staff) [chinese]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2233, 'boqs', 'boq-client-thank-you', 'chinese', 'Thank You Email (Sent to Customer After Accept) [chinese]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2234, 'boqs', 'boq-comment-to-client', 'chinese', 'New Comment  (Sent to Customer/Lead) [chinese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2235, 'boqs', 'boq-comment-to-admin', 'chinese', 'New Comment (Sent to Staff)  [chinese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2236, 'boqs', 'boq-expiry-reminder', 'chinese', 'Boq Expiration Reminder [chinese]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2237, 'boqs', 'boq-client-accepted', 'czech', 'Customer Action - Accepted (Sent to Staff) [czech]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2238, 'boqs', 'boq-send-to-customer', 'czech', 'Send Boq to Customer [czech]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2239, 'boqs', 'boq-client-declined', 'czech', 'Customer Action - Declined (Sent to Staff) [czech]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2240, 'boqs', 'boq-client-thank-you', 'czech', 'Thank You Email (Sent to Customer After Accept) [czech]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2241, 'boqs', 'boq-comment-to-client', 'czech', 'New Comment  (Sent to Customer/Lead) [czech]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2242, 'boqs', 'boq-comment-to-admin', 'czech', 'New Comment (Sent to Staff)  [czech]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2243, 'boqs', 'boq-expiry-reminder', 'czech', 'Boq Expiration Reminder [czech]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2244, 'boqs', 'boq-client-accepted', 'dutch', 'Customer Action - Accepted (Sent to Staff) [dutch]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2245, 'boqs', 'boq-send-to-customer', 'dutch', 'Send Boq to Customer [dutch]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2246, 'boqs', 'boq-client-declined', 'dutch', 'Customer Action - Declined (Sent to Staff) [dutch]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2247, 'boqs', 'boq-client-thank-you', 'dutch', 'Thank You Email (Sent to Customer After Accept) [dutch]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2248, 'boqs', 'boq-comment-to-client', 'dutch', 'New Comment  (Sent to Customer/Lead) [dutch]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2249, 'boqs', 'boq-comment-to-admin', 'dutch', 'New Comment (Sent to Staff)  [dutch]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2250, 'boqs', 'boq-expiry-reminder', 'dutch', 'Boq Expiration Reminder [dutch]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2251, 'boqs', 'boq-client-accepted', 'french', 'Customer Action - Accepted (Sent to Staff) [french]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2252, 'boqs', 'boq-send-to-customer', 'french', 'Send Boq to Customer [french]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2253, 'boqs', 'boq-client-declined', 'french', 'Customer Action - Declined (Sent to Staff) [french]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2254, 'boqs', 'boq-client-thank-you', 'french', 'Thank You Email (Sent to Customer After Accept) [french]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2255, 'boqs', 'boq-comment-to-client', 'french', 'New Comment  (Sent to Customer/Lead) [french]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2256, 'boqs', 'boq-comment-to-admin', 'french', 'New Comment (Sent to Staff)  [french]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2257, 'boqs', 'boq-expiry-reminder', 'french', 'Boq Expiration Reminder [french]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2258, 'boqs', 'boq-client-accepted', 'german', 'Customer Action - Accepted (Sent to Staff) [german]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2259, 'boqs', 'boq-send-to-customer', 'german', 'Send Boq to Customer [german]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2260, 'boqs', 'boq-client-declined', 'german', 'Customer Action - Declined (Sent to Staff) [german]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2261, 'boqs', 'boq-client-thank-you', 'german', 'Thank You Email (Sent to Customer After Accept) [german]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2262, 'boqs', 'boq-comment-to-client', 'german', 'New Comment  (Sent to Customer/Lead) [german]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2263, 'boqs', 'boq-comment-to-admin', 'german', 'New Comment (Sent to Staff)  [german]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2264, 'boqs', 'boq-expiry-reminder', 'german', 'Boq Expiration Reminder [german]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2265, 'boqs', 'boq-client-accepted', 'greek', 'Customer Action - Accepted (Sent to Staff) [greek]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2266, 'boqs', 'boq-send-to-customer', 'greek', 'Send Boq to Customer [greek]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2267, 'boqs', 'boq-client-declined', 'greek', 'Customer Action - Declined (Sent to Staff) [greek]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2268, 'boqs', 'boq-client-thank-you', 'greek', 'Thank You Email (Sent to Customer After Accept) [greek]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2269, 'boqs', 'boq-comment-to-client', 'greek', 'New Comment  (Sent to Customer/Lead) [greek]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2270, 'boqs', 'boq-comment-to-admin', 'greek', 'New Comment (Sent to Staff)  [greek]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2271, 'boqs', 'boq-expiry-reminder', 'greek', 'Boq Expiration Reminder [greek]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2272, 'boqs', 'boq-client-accepted', 'indonesia', 'Customer Action - Accepted (Sent to Staff) [indonesia]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2273, 'boqs', 'boq-send-to-customer', 'indonesia', 'Send Boq to Customer [indonesia]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2274, 'boqs', 'boq-client-declined', 'indonesia', 'Customer Action - Declined (Sent to Staff) [indonesia]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2275, 'boqs', 'boq-client-thank-you', 'indonesia', 'Thank You Email (Sent to Customer After Accept) [indonesia]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2276, 'boqs', 'boq-comment-to-client', 'indonesia', 'New Comment  (Sent to Customer/Lead) [indonesia]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2277, 'boqs', 'boq-comment-to-admin', 'indonesia', 'New Comment (Sent to Staff)  [indonesia]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2278, 'boqs', 'boq-expiry-reminder', 'indonesia', 'Boq Expiration Reminder [indonesia]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2279, 'boqs', 'boq-client-accepted', 'italian', 'Customer Action - Accepted (Sent to Staff) [italian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2280, 'boqs', 'boq-send-to-customer', 'italian', 'Send Boq to Customer [italian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2281, 'boqs', 'boq-client-declined', 'italian', 'Customer Action - Declined (Sent to Staff) [italian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2282, 'boqs', 'boq-client-thank-you', 'italian', 'Thank You Email (Sent to Customer After Accept) [italian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2283, 'boqs', 'boq-comment-to-client', 'italian', 'New Comment  (Sent to Customer/Lead) [italian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2284, 'boqs', 'boq-comment-to-admin', 'italian', 'New Comment (Sent to Staff)  [italian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2285, 'boqs', 'boq-expiry-reminder', 'italian', 'Boq Expiration Reminder [italian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2286, 'boqs', 'boq-client-accepted', 'japanese', 'Customer Action - Accepted (Sent to Staff) [japanese]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2287, 'boqs', 'boq-send-to-customer', 'japanese', 'Send Boq to Customer [japanese]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2288, 'boqs', 'boq-client-declined', 'japanese', 'Customer Action - Declined (Sent to Staff) [japanese]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2289, 'boqs', 'boq-client-thank-you', 'japanese', 'Thank You Email (Sent to Customer After Accept) [japanese]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2290, 'boqs', 'boq-comment-to-client', 'japanese', 'New Comment  (Sent to Customer/Lead) [japanese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2291, 'boqs', 'boq-comment-to-admin', 'japanese', 'New Comment (Sent to Staff)  [japanese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2292, 'boqs', 'boq-expiry-reminder', 'japanese', 'Boq Expiration Reminder [japanese]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2293, 'boqs', 'boq-client-accepted', 'persian', 'Customer Action - Accepted (Sent to Staff) [persian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2294, 'boqs', 'boq-send-to-customer', 'persian', 'Send Boq to Customer [persian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2295, 'boqs', 'boq-client-declined', 'persian', 'Customer Action - Declined (Sent to Staff) [persian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2296, 'boqs', 'boq-client-thank-you', 'persian', 'Thank You Email (Sent to Customer After Accept) [persian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2297, 'boqs', 'boq-comment-to-client', 'persian', 'New Comment  (Sent to Customer/Lead) [persian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2298, 'boqs', 'boq-comment-to-admin', 'persian', 'New Comment (Sent to Staff)  [persian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2299, 'boqs', 'boq-expiry-reminder', 'persian', 'Boq Expiration Reminder [persian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2300, 'boqs', 'boq-client-accepted', 'polish', 'Customer Action - Accepted (Sent to Staff) [polish]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2301, 'boqs', 'boq-send-to-customer', 'polish', 'Send Boq to Customer [polish]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2302, 'boqs', 'boq-client-declined', 'polish', 'Customer Action - Declined (Sent to Staff) [polish]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2303, 'boqs', 'boq-client-thank-you', 'polish', 'Thank You Email (Sent to Customer After Accept) [polish]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2304, 'boqs', 'boq-comment-to-client', 'polish', 'New Comment  (Sent to Customer/Lead) [polish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2305, 'boqs', 'boq-comment-to-admin', 'polish', 'New Comment (Sent to Staff)  [polish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2306, 'boqs', 'boq-expiry-reminder', 'polish', 'Boq Expiration Reminder [polish]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2307, 'boqs', 'boq-client-accepted', 'portuguese', 'Customer Action - Accepted (Sent to Staff) [portuguese]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2308, 'boqs', 'boq-send-to-customer', 'portuguese', 'Send Boq to Customer [portuguese]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2309, 'boqs', 'boq-client-declined', 'portuguese', 'Customer Action - Declined (Sent to Staff) [portuguese]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2310, 'boqs', 'boq-client-thank-you', 'portuguese', 'Thank You Email (Sent to Customer After Accept) [portuguese]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2311, 'boqs', 'boq-comment-to-client', 'portuguese', 'New Comment  (Sent to Customer/Lead) [portuguese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2312, 'boqs', 'boq-comment-to-admin', 'portuguese', 'New Comment (Sent to Staff)  [portuguese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2313, 'boqs', 'boq-expiry-reminder', 'portuguese', 'Boq Expiration Reminder [portuguese]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2314, 'boqs', 'boq-client-accepted', 'portuguese_br', 'Customer Action - Accepted (Sent to Staff) [portuguese_br]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2315, 'boqs', 'boq-send-to-customer', 'portuguese_br', 'Send Boq to Customer [portuguese_br]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2316, 'boqs', 'boq-client-declined', 'portuguese_br', 'Customer Action - Declined (Sent to Staff) [portuguese_br]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2317, 'boqs', 'boq-client-thank-you', 'portuguese_br', 'Thank You Email (Sent to Customer After Accept) [portuguese_br]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2318, 'boqs', 'boq-comment-to-client', 'portuguese_br', 'New Comment  (Sent to Customer/Lead) [portuguese_br]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2319, 'boqs', 'boq-comment-to-admin', 'portuguese_br', 'New Comment (Sent to Staff)  [portuguese_br]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2320, 'boqs', 'boq-expiry-reminder', 'portuguese_br', 'Boq Expiration Reminder [portuguese_br]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2321, 'boqs', 'boq-client-accepted', 'romanian', 'Customer Action - Accepted (Sent to Staff) [romanian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2322, 'boqs', 'boq-send-to-customer', 'romanian', 'Send Boq to Customer [romanian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2323, 'boqs', 'boq-client-declined', 'romanian', 'Customer Action - Declined (Sent to Staff) [romanian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2324, 'boqs', 'boq-client-thank-you', 'romanian', 'Thank You Email (Sent to Customer After Accept) [romanian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2325, 'boqs', 'boq-comment-to-client', 'romanian', 'New Comment  (Sent to Customer/Lead) [romanian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2326, 'boqs', 'boq-comment-to-admin', 'romanian', 'New Comment (Sent to Staff)  [romanian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2327, 'boqs', 'boq-expiry-reminder', 'romanian', 'Boq Expiration Reminder [romanian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2328, 'boqs', 'boq-client-accepted', 'russian', 'Customer Action - Accepted (Sent to Staff) [russian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2329, 'boqs', 'boq-send-to-customer', 'russian', 'Send Boq to Customer [russian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2330, 'boqs', 'boq-client-declined', 'russian', 'Customer Action - Declined (Sent to Staff) [russian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2331, 'boqs', 'boq-client-thank-you', 'russian', 'Thank You Email (Sent to Customer After Accept) [russian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2332, 'boqs', 'boq-comment-to-client', 'russian', 'New Comment  (Sent to Customer/Lead) [russian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2333, 'boqs', 'boq-comment-to-admin', 'russian', 'New Comment (Sent to Staff)  [russian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2334, 'boqs', 'boq-expiry-reminder', 'russian', 'Boq Expiration Reminder [russian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2335, 'boqs', 'boq-client-accepted', 'slovak', 'Customer Action - Accepted (Sent to Staff) [slovak]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2336, 'boqs', 'boq-send-to-customer', 'slovak', 'Send Boq to Customer [slovak]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2337, 'boqs', 'boq-client-declined', 'slovak', 'Customer Action - Declined (Sent to Staff) [slovak]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2338, 'boqs', 'boq-client-thank-you', 'slovak', 'Thank You Email (Sent to Customer After Accept) [slovak]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2339, 'boqs', 'boq-comment-to-client', 'slovak', 'New Comment  (Sent to Customer/Lead) [slovak]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2340, 'boqs', 'boq-comment-to-admin', 'slovak', 'New Comment (Sent to Staff)  [slovak]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2341, 'boqs', 'boq-expiry-reminder', 'slovak', 'Boq Expiration Reminder [slovak]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2342, 'boqs', 'boq-client-accepted', 'spanish', 'Customer Action - Accepted (Sent to Staff) [spanish]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2343, 'boqs', 'boq-send-to-customer', 'spanish', 'Send Boq to Customer [spanish]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2344, 'boqs', 'boq-client-declined', 'spanish', 'Customer Action - Declined (Sent to Staff) [spanish]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2345, 'boqs', 'boq-client-thank-you', 'spanish', 'Thank You Email (Sent to Customer After Accept) [spanish]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2346, 'boqs', 'boq-comment-to-client', 'spanish', 'New Comment  (Sent to Customer/Lead) [spanish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2347, 'boqs', 'boq-comment-to-admin', 'spanish', 'New Comment (Sent to Staff)  [spanish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2348, 'boqs', 'boq-expiry-reminder', 'spanish', 'Boq Expiration Reminder [spanish]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2349, 'boqs', 'boq-client-accepted', 'swedish', 'Customer Action - Accepted (Sent to Staff) [swedish]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2350, 'boqs', 'boq-send-to-customer', 'swedish', 'Send Boq to Customer [swedish]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2351, 'boqs', 'boq-client-declined', 'swedish', 'Customer Action - Declined (Sent to Staff) [swedish]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2352, 'boqs', 'boq-client-thank-you', 'swedish', 'Thank You Email (Sent to Customer After Accept) [swedish]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2353, 'boqs', 'boq-comment-to-client', 'swedish', 'New Comment  (Sent to Customer/Lead) [swedish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2354, 'boqs', 'boq-comment-to-admin', 'swedish', 'New Comment (Sent to Staff)  [swedish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2355, 'boqs', 'boq-expiry-reminder', 'swedish', 'Boq Expiration Reminder [swedish]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2356, 'boqs', 'boq-client-accepted', 'turkish', 'Customer Action - Accepted (Sent to Staff) [turkish]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2357, 'boqs', 'boq-send-to-customer', 'turkish', 'Send Boq to Customer [turkish]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2358, 'boqs', 'boq-client-declined', 'turkish', 'Customer Action - Declined (Sent to Staff) [turkish]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2359, 'boqs', 'boq-client-thank-you', 'turkish', 'Thank You Email (Sent to Customer After Accept) [turkish]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2360, 'boqs', 'boq-comment-to-client', 'turkish', 'New Comment  (Sent to Customer/Lead) [turkish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2361, 'boqs', 'boq-comment-to-admin', 'turkish', 'New Comment (Sent to Staff)  [turkish]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2362, 'boqs', 'boq-expiry-reminder', 'turkish', 'Boq Expiration Reminder [turkish]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2363, 'boqs', 'boq-client-accepted', 'ukrainian', 'Customer Action - Accepted (Sent to Staff) [ukrainian]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2364, 'boqs', 'boq-send-to-customer', 'ukrainian', 'Send Boq to Customer [ukrainian]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2365, 'boqs', 'boq-client-declined', 'ukrainian', 'Customer Action - Declined (Sent to Staff) [ukrainian]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2366, 'boqs', 'boq-client-thank-you', 'ukrainian', 'Thank You Email (Sent to Customer After Accept) [ukrainian]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2367, 'boqs', 'boq-comment-to-client', 'ukrainian', 'New Comment  (Sent to Customer/Lead) [ukrainian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2368, 'boqs', 'boq-comment-to-admin', 'ukrainian', 'New Comment (Sent to Staff)  [ukrainian]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2369, 'boqs', 'boq-expiry-reminder', 'ukrainian', 'Boq Expiration Reminder [ukrainian]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2370, 'boqs', 'boq-client-accepted', 'vietnamese', 'Customer Action - Accepted (Sent to Staff) [vietnamese]', 'Customer Accepted Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2371, 'boqs', 'boq-send-to-customer', 'vietnamese', 'Send Boq to Customer [vietnamese]', 'Boq With Number {boq_number} Created', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2372, 'boqs', 'boq-client-declined', 'vietnamese', 'Customer Action - Declined (Sent to Staff) [vietnamese]', 'Client Declined Boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2373, 'boqs', 'boq-client-thank-you', 'vietnamese', 'Thank You Email (Sent to Customer After Accept) [vietnamese]', 'Thank for you accepting boq', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2374, 'boqs', 'boq-comment-to-client', 'vietnamese', 'New Comment  (Sent to Customer/Lead) [vietnamese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2375, 'boqs', 'boq-comment-to-admin', 'vietnamese', 'New Comment (Sent to Staff)  [vietnamese]', 'New Boq Comment', '', '{companyname} | CRM', NULL, 0, 1, 0),
	(2376, 'boqs', 'boq-expiry-reminder', 'vietnamese', 'Boq Expiration Reminder [vietnamese]', 'Boq Expiration Reminder', '', '{companyname} | CRM', NULL, 0, 1, 0);
/*!40000 ALTER TABLE `tblemailtemplates` ENABLE KEYS */;

-- Dumping structure for table crm.tblestimates
CREATE TABLE IF NOT EXISTS `tblestimates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  `datesend` datetime DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `deleted_customer_name` varchar(100) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '0',
  `hash` varchar(32) DEFAULT NULL,
  `datecreated` datetime NOT NULL,
  `date` date NOT NULL,
  `expirydate` date DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `total_tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL,
  `adjustment` decimal(15,2) DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `clientnote` text,
  `adminnote` text,
  `discount_percent` decimal(15,2) DEFAULT '0.00',
  `discount_total` decimal(15,2) DEFAULT '0.00',
  `discount_type` varchar(30) DEFAULT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `invoiced_date` datetime DEFAULT NULL,
  `terms` text,
  `reference_no` varchar(100) DEFAULT NULL,
  `sale_agent` int(11) NOT NULL DEFAULT '0',
  `billing_street` varchar(200) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_state` varchar(100) DEFAULT NULL,
  `billing_zip` varchar(100) DEFAULT NULL,
  `billing_country` int(11) DEFAULT NULL,
  `shipping_street` varchar(200) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_zip` varchar(100) DEFAULT NULL,
  `shipping_country` int(11) DEFAULT NULL,
  `include_shipping` tinyint(1) NOT NULL,
  `show_shipping_on_estimate` tinyint(1) NOT NULL DEFAULT '1',
  `show_quantity_as` int(11) NOT NULL DEFAULT '1',
  `pipeline_order` int(11) NOT NULL DEFAULT '0',
  `is_expiry_notified` int(11) NOT NULL DEFAULT '0',
  `acceptance_firstname` varchar(50) DEFAULT NULL,
  `acceptance_lastname` varchar(50) DEFAULT NULL,
  `acceptance_email` varchar(100) DEFAULT NULL,
  `acceptance_date` datetime DEFAULT NULL,
  `acceptance_ip` varchar(40) DEFAULT NULL,
  `signature` varchar(40) DEFAULT NULL,
  `short_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientid` (`clientid`),
  KEY `currency` (`currency`),
  KEY `project_id` (`project_id`),
  KEY `sale_agent` (`sale_agent`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblestimates: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblestimates` DISABLE KEYS */;
INSERT INTO `tblestimates` (`id`, `sent`, `datesend`, `clientid`, `deleted_customer_name`, `project_id`, `number`, `prefix`, `number_format`, `hash`, `datecreated`, `date`, `expirydate`, `currency`, `subtotal`, `total_tax`, `total`, `adjustment`, `addedfrom`, `status`, `clientnote`, `adminnote`, `discount_percent`, `discount_total`, `discount_type`, `invoiceid`, `invoiced_date`, `terms`, `reference_no`, `sale_agent`, `billing_street`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `shipping_street`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `include_shipping`, `show_shipping_on_estimate`, `show_quantity_as`, `pipeline_order`, `is_expiry_notified`, `acceptance_firstname`, `acceptance_lastname`, `acceptance_email`, `acceptance_date`, `acceptance_ip`, `signature`, `short_link`) VALUES
	(1, 0, NULL, 1, NULL, 0, 1, 'EST-', 1, '0e7acc8c8a0972d43e31c99ae3bd8f50', '2021-08-04 11:58:29', '2021-08-04', '2021-08-11', 3, 2034389.17, 0.00, 1830950.25, 0.00, 1, 2, 'NA', 'NA', 10.00, 203438.92, 'before_tax', NULL, NULL, 'NA', 'BOQ', 1, '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 0, NULL, 6, NULL, 0, 2, 'EST-', 1, '29d6dc263412c0049164037f764c5a5b', '2021-08-24 15:34:20', '2021-08-24', '2021-08-31', 1, 546.00, 0.00, 546.00, 0.00, 1, 1, '', 'NA', 0.00, 0.00, '', NULL, NULL, '', '', 5, 'B-10, DEVAMANI SOCIETY, SHIVAJI UDYAN', 'PALSE', 'Maharashtra', '422102', 0, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblestimates` ENABLE KEYS */;

-- Dumping structure for table crm.tblevents
CREATE TABLE IF NOT EXISTS `tblevents` (
  `eventid` int(11) NOT NULL AUTO_INCREMENT,
  `title` mediumtext NOT NULL,
  `description` text,
  `userid` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `public` int(11) NOT NULL DEFAULT '0',
  `color` varchar(10) DEFAULT NULL,
  `isstartnotified` tinyint(1) NOT NULL DEFAULT '0',
  `reminder_before` int(11) NOT NULL DEFAULT '0',
  `reminder_before_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`eventid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblevents: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblevents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblevents` ENABLE KEYS */;

-- Dumping structure for table crm.tblexpenses
CREATE TABLE IF NOT EXISTS `tblexpenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `tax` int(11) DEFAULT NULL,
  `tax2` int(11) NOT NULL DEFAULT '0',
  `reference_no` varchar(100) DEFAULT NULL,
  `note` text,
  `expense_name` varchar(191) DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `billable` int(11) DEFAULT '0',
  `invoiceid` int(11) DEFAULT NULL,
  `paymentmode` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `recurring_type` varchar(10) DEFAULT NULL,
  `repeat_every` int(11) DEFAULT NULL,
  `recurring` int(11) NOT NULL DEFAULT '0',
  `cycles` int(11) NOT NULL DEFAULT '0',
  `total_cycles` int(11) NOT NULL DEFAULT '0',
  `custom_recurring` int(11) NOT NULL DEFAULT '0',
  `last_recurring_date` date DEFAULT NULL,
  `create_invoice_billable` tinyint(1) DEFAULT NULL,
  `send_invoice_to_customer` tinyint(1) NOT NULL,
  `recurring_from` int(11) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  `addedfrom` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientid` (`clientid`),
  KEY `project_id` (`project_id`),
  KEY `category` (`category`),
  KEY `currency` (`currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblexpenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblexpenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblexpenses` ENABLE KEYS */;

-- Dumping structure for table crm.tblexpenses_categories
CREATE TABLE IF NOT EXISTS `tblexpenses_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblexpenses_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblexpenses_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblexpenses_categories` ENABLE KEYS */;

-- Dumping structure for table crm.tblfiles
CREATE TABLE IF NOT EXISTS `tblfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) NOT NULL,
  `file_name` varchar(191) NOT NULL,
  `filetype` varchar(40) DEFAULT NULL,
  `visible_to_customer` int(11) NOT NULL DEFAULT '0',
  `attachment_key` varchar(32) DEFAULT NULL,
  `external` varchar(40) DEFAULT NULL,
  `external_link` text,
  `thumbnail_link` text COMMENT 'For external usage',
  `staffid` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT '0',
  `task_comment_id` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblfiles: ~9 rows (approximately)
/*!40000 ALTER TABLE `tblfiles` DISABLE KEYS */;
INSERT INTO `tblfiles` (`id`, `rel_id`, `rel_type`, `file_name`, `filetype`, `visible_to_customer`, `attachment_key`, `external`, `external_link`, `thumbnail_link`, `staffid`, `contact_id`, `task_comment_id`, `dateadded`) VALUES
	(1, 2, 'customer', 'Desktop.png', 'image/png', 1, 'bf137f50212e53c79b81f49634e01916', NULL, NULL, NULL, 0, 4, 0, '2021-03-13 12:09:07'),
	(2, 2, 'lead', 'BackUp.pdf', 'application/pdf', 0, '38c35d962d44dc93e1a858e02f0c1d16', NULL, NULL, NULL, 1, 0, 0, '2021-05-11 11:54:39'),
	(3, 2, 'contract', 'NCDEX-homepage_banner__20211028.jpg', 'image/jpeg', 0, '15beb7f32c3bc4aae965a0d71049474d', NULL, NULL, NULL, 1, 0, 0, '2021-11-08 19:22:12'),
	(4, 15, 'task', 'Guidelines for submission of Aadhaar Card and Utility Bills.pdf', 'application/pdf', 0, 'c7e0cd29c55c6c56d05a630a4c6fad91', NULL, NULL, NULL, 1, 0, 0, '2021-11-08 19:25:13'),
	(5, 15, 'task', 'Banner.jpg', 'image/jpeg', 0, '6944657224ee9c6f4464524cb312d0b7', NULL, NULL, NULL, 1, 0, 0, '2021-11-09 12:45:30'),
	(6, 5, 'customer', 'NCDEX-homepage_banner_20211028.jpg', 'image/jpeg', 1, 'e18834ea7b5e4e07fc9a4abe05bff04b', NULL, NULL, NULL, 0, 37, 0, '2021-11-17 12:51:57'),
	(7, 5, 'customer', 'NCDEX-homepage_banner-wiw.jpg', 'image/jpeg', 1, '84392086491851adfa513d345d0eb33d', NULL, NULL, NULL, 0, 37, 0, '2021-11-17 17:08:46'),
	(8, 5, 'customer', 'CTA-01.png', 'image/png', 1, 'f191aa9a1e2465a3c90fb6871bff98cb', NULL, NULL, NULL, 0, 37, 0, '2021-12-06 14:31:10'),
	(9, 5, 'customer', 'Banner.jpg', 'image/jpeg', 1, '5a1e0d041e1e510105e3fda37c862315', NULL, NULL, NULL, 0, 37, 0, '2021-12-06 17:50:22');
/*!40000 ALTER TABLE `tblfiles` ENABLE KEYS */;

-- Dumping structure for table crm.tblform_questions
CREATE TABLE IF NOT EXISTS `tblform_questions` (
  `questionid` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) DEFAULT NULL,
  `question` mediumtext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `question_order` int(11) NOT NULL,
  PRIMARY KEY (`questionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblform_questions: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblform_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblform_questions` ENABLE KEYS */;

-- Dumping structure for table crm.tblform_question_box
CREATE TABLE IF NOT EXISTS `tblform_question_box` (
  `boxid` int(11) NOT NULL AUTO_INCREMENT,
  `boxtype` varchar(10) NOT NULL,
  `questionid` int(11) NOT NULL,
  PRIMARY KEY (`boxid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblform_question_box: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblform_question_box` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblform_question_box` ENABLE KEYS */;

-- Dumping structure for table crm.tblform_question_box_description
CREATE TABLE IF NOT EXISTS `tblform_question_box_description` (
  `questionboxdescriptionid` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext NOT NULL,
  `boxid` mediumtext NOT NULL,
  `questionid` int(11) NOT NULL,
  PRIMARY KEY (`questionboxdescriptionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblform_question_box_description: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblform_question_box_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblform_question_box_description` ENABLE KEYS */;

-- Dumping structure for table crm.tblform_results
CREATE TABLE IF NOT EXISTS `tblform_results` (
  `resultid` int(11) NOT NULL AUTO_INCREMENT,
  `boxid` int(11) NOT NULL,
  `boxdescriptionid` int(11) DEFAULT NULL,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) DEFAULT NULL,
  `questionid` int(11) NOT NULL,
  `answer` text,
  `resultsetid` int(11) NOT NULL,
  PRIMARY KEY (`resultid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblform_results: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblform_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblform_results` ENABLE KEYS */;

-- Dumping structure for table crm.tblgaps
CREATE TABLE IF NOT EXISTS `tblgaps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `staffid` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `impact_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblgaps: ~1 rows (approximately)
/*!40000 ALTER TABLE `tblgaps` DISABLE KEYS */;
INSERT INTO `tblgaps` (`id`, `clientid`, `name`, `staffid`, `category_id`, `impact_id`, `status_id`, `description`, `created_at`, `updated_at`) VALUES
	(1, 5, 'TestGap', 0, 4, 4, 4, '', '2021-04-04 14:37:31', '2021-10-21 12:40:42'),
	(2, 2, 'New Gap', 0, 4, 3, 3, '', '2021-10-20 19:13:30', '2021-10-20 19:13:30');
/*!40000 ALTER TABLE `tblgaps` ENABLE KEYS */;

-- Dumping structure for table crm.tblgap_category
CREATE TABLE IF NOT EXISTS `tblgap_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblgap_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgap_category` DISABLE KEYS */;
INSERT INTO `tblgap_category` (`id`, `category_title`, `created_at`, `updated_at`) VALUES
	(4, 'GAP Category', '2021-03-31 15:29:53', '2021-04-05 11:13:26');
/*!40000 ALTER TABLE `tblgap_category` ENABLE KEYS */;

-- Dumping structure for table crm.tblgap_impact
CREATE TABLE IF NOT EXISTS `tblgap_impact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `impact_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblgap_impact: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgap_impact` DISABLE KEYS */;
INSERT INTO `tblgap_impact` (`id`, `impact_title`, `created_at`, `updated_at`) VALUES
	(4, 'Risk', '2021-03-31 15:30:02', '2021-04-05 11:13:27');
/*!40000 ALTER TABLE `tblgap_impact` ENABLE KEYS */;

-- Dumping structure for table crm.tblgap_notes
CREATE TABLE IF NOT EXISTS `tblgap_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gap_id` int(11) DEFAULT NULL,
  `gap_note` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblgap_notes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgap_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblgap_notes` ENABLE KEYS */;

-- Dumping structure for table crm.tblgap_status
CREATE TABLE IF NOT EXISTS `tblgap_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblgap_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgap_status` DISABLE KEYS */;
INSERT INTO `tblgap_status` (`id`, `status_title`, `created_at`, `updated_at`) VALUES
	(4, 'Low', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tblgap_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblgdpr_requests
CREATE TABLE IF NOT EXISTS `tblgdpr_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `lead_id` int(11) NOT NULL DEFAULT '0',
  `request_type` varchar(191) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `request_date` datetime NOT NULL,
  `request_from` varchar(150) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblgdpr_requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgdpr_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblgdpr_requests` ENABLE KEYS */;

-- Dumping structure for table crm.tblgoals
CREATE TABLE IF NOT EXISTS `tblgoals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `goal_type` int(11) NOT NULL,
  `contract_type` int(11) NOT NULL DEFAULT '0',
  `achievement` int(11) NOT NULL,
  `notify_when_fail` tinyint(1) NOT NULL DEFAULT '1',
  `notify_when_achieve` tinyint(1) NOT NULL DEFAULT '1',
  `notified` int(11) NOT NULL DEFAULT '0',
  `staff_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblgoals: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblgoals` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblgoals` ENABLE KEYS */;

-- Dumping structure for table crm.tblguest_contacts
CREATE TABLE IF NOT EXISTS `tblguest_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `country_code` varchar(20) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `comment` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblguest_contacts: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblguest_contacts` DISABLE KEYS */;
INSERT INTO `tblguest_contacts` (`id`, `subject`, `name`, `company`, `email`, `country_code`, `contact`, `country`, `comment`, `created_at`, `updated_at`) VALUES
	(1, 'Need Urgent Support', 'Akshay Dange', 'Leometric Technology', 'akshayd@leometric.com', NULL, '9405310077', NULL, 'NA', '2021-12-09 14:53:04', '2021-12-09 14:53:04'),
	(2, 'Need Urgent Support', 'Akshay Dange', 'Leometric Technology', 'akshayd@leometric.com', NULL, '9405310077', NULL, 'NA', '2021-12-09 16:48:17', '2021-12-09 16:48:17'),
	(3, 'Need Urgent Support', 'Akshay Dange', 'Leometric Technology', 'akshayd@leometric.com', NULL, '9405310077', NULL, 'NA', '2021-12-09 17:04:18', '2021-12-09 17:04:18'),
	(4, 'Ask For a Quote', 'Sudarshan Sinha', 'Innowyat', 'sudarshan@inno.com', NULL, '9090908989', NULL, '', '2021-12-10 12:45:38', '2021-12-10 12:45:38'),
	(5, 'Create My Account', 'Sudarshan Sinha', 'Innowyat', 'sudarshan@inno.com', NULL, '9090908989', NULL, 'Need to create my account on decoding my it portal.', '2021-12-10 12:50:12', '2021-12-10 12:50:12');
/*!40000 ALTER TABLE `tblguest_contacts` ENABLE KEYS */;

-- Dumping structure for table crm.tblinventory_details
CREATE TABLE IF NOT EXISTS `tblinventory_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `customer_prefix` varchar(50) NOT NULL,
  `sub_type` int(11) DEFAULT NULL,
  `manufacture` int(11) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `processor` varchar(200) DEFAULT NULL,
  `ram` varchar(100) DEFAULT NULL,
  `storage` varchar(200) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `hostname` varchar(200) DEFAULT NULL,
  `operating_system` varchar(100) DEFAULT NULL,
  `sw_valid_from` date DEFAULT NULL,
  `sw_valid_to` date DEFAULT NULL,
  `sw_amount` float DEFAULT NULL,
  `sw_renewal` date DEFAULT NULL,
  `support_contract` int(11) DEFAULT NULL,
  `backup_required` int(11) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `assigned_staff` int(11) NOT NULL,
  `oem_support` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `contact` int(11) NOT NULL,
  `clientid` int(11) NOT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `notes` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblinventory_details: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblinventory_details` DISABLE KEYS */;
INSERT INTO `tblinventory_details` (`id`, `type`, `customer_prefix`, `sub_type`, `manufacture`, `product_name`, `processor`, `ram`, `storage`, `ip_address`, `serial_no`, `hostname`, `operating_system`, `sw_valid_from`, `sw_valid_to`, `sw_amount`, `sw_renewal`, `support_contract`, `backup_required`, `customer_id`, `location`, `assigned_staff`, `oem_support`, `status`, `contact`, `clientid`, `purpose`, `notes`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 2, '', 11, 3, '4', '2GHz', '2GB', '256GB', '89.98.87.66', '78946123', 'godady', 'linux', '0000-00-00', '0000-00-00', 0, '0000-00-00', 2, 1, 0, 5, 0, 0, 3, 37, 5, 'NA', '                                                                                    ', '2021-04-05 14:55:55', NULL, '2021-10-28 13:32:12', NULL),
	(2, 4, '', 13, 2, '2', '2GHz', '2GB', '256GB', '222.222.222.222', '78946123', 'tst', 'linux', '0000-00-00', '0000-00-00', 0, '0000-00-00', NULL, 1, 0, 2, 0, 0, 3, 7, 2, NULL, NULL, '2021-04-13 12:01:24', NULL, '2021-04-13 12:01:24', NULL),
	(3, 1, '', 7, 1, '1', '33', '2GB', '34', '3433', '545454', 'NA', 'NA', '0000-00-00', '0000-00-00', 0, '0000-00-00', 3, 1, 0, 5, 0, 0, 5, 29, 1, NULL, NULL, '2021-04-13 12:02:43', NULL, '2021-04-13 12:03:08', NULL);
/*!40000 ALTER TABLE `tblinventory_details` ENABLE KEYS */;

-- Dumping structure for table crm.tblinventory_manufacture
CREATE TABLE IF NOT EXISTS `tblinventory_manufacture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacture_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblinventory_manufacture: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblinventory_manufacture` DISABLE KEYS */;
INSERT INTO `tblinventory_manufacture` (`id`, `manufacture_name`, `created_at`) VALUES
	(1, 'Car', '2021-04-13 11:59:58'),
	(2, 'Datapoint', '2021-04-13 12:00:14'),
	(3, 'Godaddy', '2021-04-05 14:39:29');
/*!40000 ALTER TABLE `tblinventory_manufacture` ENABLE KEYS */;

-- Dumping structure for table crm.tblinventory_products
CREATE TABLE IF NOT EXISTS `tblinventory_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacture_id` int(11) DEFAULT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblinventory_products: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblinventory_products` DISABLE KEYS */;
INSERT INTO `tblinventory_products` (`id`, `manufacture_id`, `product_name`, `updated_at`) VALUES
	(1, 1, 'Tata Mouse', '2021-03-30 11:56:30'),
	(2, 2, 'Slack', '2021-03-30 18:49:29'),
	(3, 1, 'Text', '2021-03-30 22:08:15'),
	(4, 3, 'Premium', '2021-04-05 14:41:00');
/*!40000 ALTER TABLE `tblinventory_products` ENABLE KEYS */;

-- Dumping structure for table crm.tblinventory_statuses
CREATE TABLE IF NOT EXISTS `tblinventory_statuses` (
  `id` int(11) NOT NULL,
  `status_name` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblinventory_statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblinventory_statuses` DISABLE KEYS */;
INSERT INTO `tblinventory_statuses` (`id`, `status_name`, `created_at`, `updated_at`) VALUES
	(3, 'Opened', '2021-04-01 11:58:13', '2021-04-05 11:22:53'),
	(5, 'Resolved', '2021-04-01 12:12:31', '2021-04-05 11:22:53');
/*!40000 ALTER TABLE `tblinventory_statuses` ENABLE KEYS */;

-- Dumping structure for table crm.tblinventory_types
CREATE TABLE IF NOT EXISTS `tblinventory_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblinventory_types: ~16 rows (approximately)
/*!40000 ALTER TABLE `tblinventory_types` DISABLE KEYS */;
INSERT INTO `tblinventory_types` (`id`, `type_name`, `parent_id`, `created_at`) VALUES
	(1, 'Network', 0, '2021-03-04 15:33:42'),
	(2, 'Servers', 0, '2021-03-04 15:33:42'),
	(3, 'Storage', 0, '2021-03-04 15:34:20'),
	(4, 'Endpoints', 0, '2021-03-04 15:34:20'),
	(5, 'Software', 0, '2021-03-04 15:35:00'),
	(6, 'Switch', 1, '2021-03-04 15:35:00'),
	(7, 'Firewall', 1, '2021-03-04 15:52:35'),
	(8, 'Router', 1, '2021-03-04 15:52:35'),
	(9, 'Physical', 2, '2021-03-04 15:53:10'),
	(10, 'Virtual ', 2, '2021-03-04 15:53:10'),
	(11, 'Cloud', 2, '2021-03-04 15:53:34'),
	(12, 'Desktop', 4, '2021-03-04 15:53:34'),
	(13, 'Laptop', 4, '2021-03-04 15:53:51'),
	(14, 'Mobile', 4, '2021-03-04 15:53:51'),
	(15, 'Microsoft', 5, '2021-03-04 15:56:04'),
	(16, 'Oracle', 5, '2021-03-04 15:56:04');
/*!40000 ALTER TABLE `tblinventory_types` ENABLE KEYS */;

-- Dumping structure for table crm.tblinvoicepaymentrecords
CREATE TABLE IF NOT EXISTS `tblinvoicepaymentrecords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceid` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `paymentmode` varchar(40) DEFAULT NULL,
  `paymentmethod` varchar(191) DEFAULT NULL,
  `date` date NOT NULL,
  `daterecorded` datetime NOT NULL,
  `note` text NOT NULL,
  `transactionid` mediumtext,
  PRIMARY KEY (`id`),
  KEY `invoiceid` (`invoiceid`),
  KEY `paymentmethod` (`paymentmethod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblinvoicepaymentrecords: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblinvoicepaymentrecords` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblinvoicepaymentrecords` ENABLE KEYS */;

-- Dumping structure for table crm.tblinvoices
CREATE TABLE IF NOT EXISTS `tblinvoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  `datesend` datetime DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `deleted_customer_name` varchar(100) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '0',
  `datecreated` datetime NOT NULL,
  `date` date NOT NULL,
  `duedate` date DEFAULT NULL,
  `currency` int(11) NOT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `total_tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL,
  `adjustment` decimal(15,2) DEFAULT NULL,
  `addedfrom` int(11) DEFAULT NULL,
  `hash` varchar(32) NOT NULL,
  `status` int(11) DEFAULT '1',
  `clientnote` text,
  `adminnote` text,
  `last_overdue_reminder` date DEFAULT NULL,
  `cancel_overdue_reminders` int(11) NOT NULL DEFAULT '0',
  `allowed_payment_modes` mediumtext,
  `token` mediumtext,
  `discount_percent` decimal(15,2) DEFAULT '0.00',
  `discount_total` decimal(15,2) DEFAULT '0.00',
  `discount_type` varchar(30) NOT NULL,
  `recurring` int(11) NOT NULL DEFAULT '0',
  `recurring_type` varchar(10) DEFAULT NULL,
  `custom_recurring` tinyint(1) NOT NULL DEFAULT '0',
  `cycles` int(11) NOT NULL DEFAULT '0',
  `total_cycles` int(11) NOT NULL DEFAULT '0',
  `is_recurring_from` int(11) DEFAULT NULL,
  `last_recurring_date` date DEFAULT NULL,
  `terms` text,
  `sale_agent` int(11) NOT NULL DEFAULT '0',
  `billing_street` varchar(200) DEFAULT NULL,
  `billing_city` varchar(100) DEFAULT NULL,
  `billing_state` varchar(100) DEFAULT NULL,
  `billing_zip` varchar(100) DEFAULT NULL,
  `billing_country` int(11) DEFAULT NULL,
  `shipping_street` varchar(200) DEFAULT NULL,
  `shipping_city` varchar(100) DEFAULT NULL,
  `shipping_state` varchar(100) DEFAULT NULL,
  `shipping_zip` varchar(100) DEFAULT NULL,
  `shipping_country` int(11) DEFAULT NULL,
  `include_shipping` tinyint(1) NOT NULL,
  `show_shipping_on_invoice` tinyint(1) NOT NULL DEFAULT '1',
  `show_quantity_as` int(11) NOT NULL DEFAULT '1',
  `project_id` int(11) DEFAULT '0',
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `short_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currency` (`currency`),
  KEY `clientid` (`clientid`),
  KEY `project_id` (`project_id`),
  KEY `sale_agent` (`sale_agent`),
  KEY `total` (`total`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblinvoices: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblinvoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblinvoices` ENABLE KEYS */;

-- Dumping structure for table crm.tblitemable
CREATE TABLE IF NOT EXISTS `tblitemable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(15) NOT NULL,
  `description` mediumtext NOT NULL,
  `long_description` mediumtext,
  `qty` decimal(15,2) NOT NULL,
  `rate` decimal(15,2) NOT NULL,
  `margin` float NOT NULL,
  `sell_price` decimal(15,2) NOT NULL,
  `unit` varchar(40) DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`),
  KEY `qty` (`qty`),
  KEY `rate` (`rate`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblitemable: ~11 rows (approximately)
/*!40000 ALTER TABLE `tblitemable` DISABLE KEYS */;
INSERT INTO `tblitemable` (`id`, `rel_id`, `rel_type`, `description`, `long_description`, `qty`, `rate`, `margin`, `sell_price`, `unit`, `item_order`) VALUES
	(1, 1, 'boq', 'Test 1', 'test one', 1.00, 100.00, 0, 0.00, '', 1),
	(2, 1, 'boq', 'Test 2', 'test two', 1.00, 200.00, 0, 0.00, '', 2),
	(3, 2, 'boq', 'Test 1', 'test one', 47.00, 0.00, 0, 0.00, '', 0),
	(4, 2, 'boq', 'Test 2', 'test two', 74.00, 0.00, 0, 0.00, '', 0),
	(5, 2, 'boq', 'Test 2', 'test two', 97.00, 0.00, 0, 0.00, '', 0),
	(6, 1, 'estimate', 'Test 1', 'test one', 1.00, 110.00, 12, 0.00, '', 1),
	(7, 1, 'estimate', 'Test 2', 'test two', 74.00, 80.00, 7, 0.00, '', 2),
	(8, 1, 'estimate', 'Test 2', 'test two', 97.00, 40.00, 5, 0.00, '', 3),
	(9, 2, 'estimate', 'Test Item New', 'Test Description of test item', 2.00, 100.00, 5, 0.00, '', 1),
	(10, 3, 'boq', 'Test Item New', 'Test Description of test item', 5.00, 100.00, 0, 0.00, '', 0),
	(13, 3, 'boq', 'Test Item New', 'Test Description of test item', 1.00, 100.00, 0, 0.00, '', 1);
/*!40000 ALTER TABLE `tblitemable` ENABLE KEYS */;

-- Dumping structure for table crm.tblitems
CREATE TABLE IF NOT EXISTS `tblitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` mediumtext NOT NULL,
  `long_description` text,
  `sku` varchar(50) DEFAULT NULL,
  `rate` decimal(15,2) NOT NULL,
  `converted_rate` decimal(15,2) NOT NULL,
  `default_currency` int(11) DEFAULT NULL,
  `region` int(11) DEFAULT NULL,
  `manufacturer` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `tax2` int(11) DEFAULT NULL,
  `unit` varchar(40) DEFAULT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sku` (`sku`),
  KEY `tax` (`tax`),
  KEY `tax2` (`tax2`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblitems: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblitems` DISABLE KEYS */;
INSERT INTO `tblitems` (`id`, `description`, `long_description`, `sku`, `rate`, `converted_rate`, `default_currency`, `region`, `manufacturer`, `tax`, `tax2`, `unit`, `group_id`) VALUES
	(1, 'Test 1', 'test one', 'ITM007', 0.00, 0.00, 0, 1, 1, NULL, NULL, '', 0),
	(2, 'Test 2', 'test two', NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, NULL, 0),
	(3, 'Test Item New', 'Test Description of test item', 'ITM001', 100.00, 0.00, 2, NULL, NULL, NULL, NULL, NULL, 0),
	(4, 'Pen Drive', 'PD', 'ITM009', 210.00, 0.00, 3, 2, 1, NULL, NULL, '', 0),
	(5, 'CFL', 'Bajaj CFL', 'ITM002', 0.23, 16.83, 3, 2, 1, NULL, NULL, '', 0),
	(6, 'Maruti WagonR Tyer', 'NA', 'CD001', 109.33, 8000.00, 3, 1, 1, NULL, NULL, '', 0);
/*!40000 ALTER TABLE `tblitems` ENABLE KEYS */;

-- Dumping structure for table crm.tblitems_groups
CREATE TABLE IF NOT EXISTS `tblitems_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblitems_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblitems_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblitems_groups` ENABLE KEYS */;

-- Dumping structure for table crm.tblitems_manufacturers
CREATE TABLE IF NOT EXISTS `tblitems_manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblitems_manufacturers: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblitems_manufacturers` DISABLE KEYS */;
INSERT INTO `tblitems_manufacturers` (`id`, `manufacturer_title`, `created_at`, `updated_at`) VALUES
	(1, 'EKC D', '2021-08-27 14:39:22', '2021-08-27 14:39:33');
/*!40000 ALTER TABLE `tblitems_manufacturers` ENABLE KEYS */;

-- Dumping structure for table crm.tblitems_regions
CREATE TABLE IF NOT EXISTS `tblitems_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblitems_regions: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblitems_regions` DISABLE KEYS */;
INSERT INTO `tblitems_regions` (`id`, `region_title`, `created_at`, `updated_at`) VALUES
	(1, 'Nashik Road', '2021-08-27 14:32:11', '2021-08-27 14:32:23'),
	(2, 'Pune', '2021-08-27 14:34:26', '2021-08-27 14:34:26');
/*!40000 ALTER TABLE `tblitems_regions` ENABLE KEYS */;

-- Dumping structure for table crm.tblitem_tax
CREATE TABLE IF NOT EXISTS `tblitem_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) NOT NULL,
  `taxrate` decimal(15,2) NOT NULL,
  `taxname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemid` (`itemid`),
  KEY `rel_id` (`rel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblitem_tax: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblitem_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblitem_tax` ENABLE KEYS */;

-- Dumping structure for table crm.tblknowedge_base_article_feedback
CREATE TABLE IF NOT EXISTS `tblknowedge_base_article_feedback` (
  `articleanswerid` int(11) NOT NULL AUTO_INCREMENT,
  `articleid` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`articleanswerid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblknowedge_base_article_feedback: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblknowedge_base_article_feedback` DISABLE KEYS */;
INSERT INTO `tblknowedge_base_article_feedback` (`articleanswerid`, `articleid`, `answer`, `ip`, `date`) VALUES
	(1, 1, 0, '::1', '2021-06-23 12:49:41'),
	(2, 1, 1, '::1', '2021-11-09 18:12:05'),
	(3, 1, 1, '::1', '2021-11-16 15:22:10');
/*!40000 ALTER TABLE `tblknowedge_base_article_feedback` ENABLE KEYS */;

-- Dumping structure for table crm.tblknowledge_base
CREATE TABLE IF NOT EXISTS `tblknowledge_base` (
  `articleid` int(11) NOT NULL AUTO_INCREMENT,
  `articlegroup` int(11) NOT NULL,
  `clients` text NOT NULL,
  `staffs` text NOT NULL,
  `subject` mediumtext NOT NULL,
  `description` text NOT NULL,
  `slug` mediumtext NOT NULL,
  `active` tinyint(4) NOT NULL,
  `datecreated` datetime NOT NULL,
  `article_order` int(11) NOT NULL DEFAULT '0',
  `staff_article` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`articleid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblknowledge_base: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblknowledge_base` DISABLE KEYS */;
INSERT INTO `tblknowledge_base` (`articleid`, `articlegroup`, `clients`, `staffs`, `subject`, `description`, `slug`, `active`, `datecreated`, `article_order`, `staff_article`) VALUES
	(1, 1, '1,3,6,5', '1,2', 'Test', 'Testing', 'test', 1, '2021-06-23 12:48:42', 0, 0),
	(2, 1, '1,3,6', '1,5', 'Test Article', 'Testing', 'test-article', 1, '2021-06-23 14:32:34', 0, 0);
/*!40000 ALTER TABLE `tblknowledge_base` ENABLE KEYS */;

-- Dumping structure for table crm.tblknowledge_base_groups
CREATE TABLE IF NOT EXISTS `tblknowledge_base_groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `group_slug` text,
  `description` mediumtext,
  `active` tinyint(4) NOT NULL,
  `color` varchar(10) DEFAULT '#28B8DA',
  `group_order` int(11) DEFAULT '0',
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblknowledge_base_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblknowledge_base_groups` DISABLE KEYS */;
INSERT INTO `tblknowledge_base_groups` (`groupid`, `name`, `group_slug`, `description`, `active`, `color`, `group_order`) VALUES
	(1, 'Article Group', 'article-group', 'NA', 1, '#28b8da', 1),
	(2, 'Test Group', 'test-group', 'NA', 1, '#28B8DA', 2);
/*!40000 ALTER TABLE `tblknowledge_base_groups` ENABLE KEYS */;

-- Dumping structure for table crm.tblleads
CREATE TABLE IF NOT EXISTS `tblleads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(65) DEFAULT NULL,
  `deal_name` varchar(65) DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `company` varchar(191) DEFAULT NULL,
  `description` text,
  `country` int(11) NOT NULL DEFAULT '0',
  `zip` varchar(15) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `assigned` int(11) NOT NULL DEFAULT '0',
  `pre_sale_engineer` text,
  `generated_by` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL,
  `dateprojectedclosed` date DEFAULT NULL,
  `from_form_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `source` int(11) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `approval_by` int(11) NOT NULL,
  `lastcontact` datetime DEFAULT NULL,
  `dateassigned` date DEFAULT NULL,
  `last_status_change` datetime DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `leadorder` int(11) DEFAULT '1',
  `phonenumber` varchar(50) DEFAULT NULL,
  `date_converted` datetime DEFAULT NULL,
  `lost` tinyint(1) NOT NULL DEFAULT '0',
  `junk` int(11) NOT NULL DEFAULT '0',
  `last_lead_status` int(11) NOT NULL DEFAULT '0',
  `is_imported_from_email_integration` tinyint(1) NOT NULL DEFAULT '0',
  `email_integration_uid` varchar(30) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `default_language` varchar(40) DEFAULT NULL,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `lead_value` decimal(15,2) DEFAULT NULL,
  `gross_margin_value` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `company` (`company`),
  KEY `email` (`email`),
  KEY `assigned` (`assigned`),
  KEY `status` (`status`),
  KEY `source` (`source`),
  KEY `lastcontact` (`lastcontact`),
  KEY `dateadded` (`dateadded`),
  KEY `leadorder` (`leadorder`),
  KEY `from_form_id` (`from_form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblleads: ~8 rows (approximately)
/*!40000 ALTER TABLE `tblleads` DISABLE KEYS */;
INSERT INTO `tblleads` (`id`, `hash`, `deal_name`, `name`, `title`, `company`, `description`, `country`, `zip`, `city`, `state`, `address`, `assigned`, `pre_sale_engineer`, `generated_by`, `dateadded`, `dateprojectedclosed`, `from_form_id`, `status`, `source`, `approval_status`, `approval_by`, `lastcontact`, `dateassigned`, `last_status_change`, `addedfrom`, `email`, `website`, `leadorder`, `phonenumber`, `date_converted`, `lost`, `junk`, `last_lead_status`, `is_imported_from_email_integration`, `email_integration_uid`, `is_public`, `default_language`, `client_id`, `contact_id`, `lead_value`, `gross_margin_value`) VALUES
	(1, '4b4e7b142faf7de084515e03d99898ff-b0b234e37ed42736cbea6a27949d4294', 'Aarti Akshay', 'Aarti Dange', 'HR Manager', 'aDcoders Technologies', 'NA', 102, '422102', 'Nashik', 'Maharashtra', 'Nashik', 1, '0', 0, '2021-03-06 15:26:05', '2021-05-08', 0, 2, 2, 1, 0, '2021-05-06 18:00:00', NULL, '2021-05-27 11:10:18', 1, 'aarti@gmail.com', 'www.aartidange.com', 1, '9405310077', '2021-05-06 16:27:47', 0, 0, 0, 0, NULL, 0, 'english', 0, 0, 1200.00, NULL),
	(2, '266ee16cba861af7bdb105a74b105d39-eaf3f8bbec3d2c3fcbc7d90a39b7603d', 'Test Deal', 'Akshay B Dange', 'Team Lead', 'Akshay Technologies', 'NA', 102, '422102', 'Nashik', 'Maharashtra', 'Nashik', 5, '4', 1, '2021-05-07 16:29:33', '2021-05-27', 0, 1, 1, 1, 0, '2021-05-11 11:54:00', '2021-05-07', '2021-05-10 16:42:24', 1, 'akshay.dange74@gmail.com', 'www.akshay.com', 1, '09405310077', '2021-05-07 17:22:17', 0, 0, 0, 0, NULL, 0, 'english', 0, 37, 400.00, 700.00),
	(3, '440719390096e17331b0f8484923dc85-25f5df016d4ac9ad89d7ad54b767631d', 'Vidhi', 'Vidhi Landge', 'Cutie Pie', 'Vidhi Group of Companies', 'NA', 0, '422102', 'PALSE', 'Maharashtra', 'B-10, DEVAMANI SOCIETY, SHIVAJI UDYAN', 5, '1', 1, '2021-05-11 15:23:00', '2021-05-31', 0, 1, 1, 1, 0, '2021-05-11 15:23:00', '2021-05-11', NULL, 1, 'vidhi@gmail.com', 'www.cutiepievidhi.com', 1, '9405310077', '2021-05-11 19:04:56', 0, 0, 0, 0, NULL, 0, '', 6, 38, 74.00, 47.00),
	(4, '402247cefb711afdfd14314ab36ed34e-8049b779f41c017b5089e23939acbc7d', 'Test Existing Deal', 'Akshay Dange', 'Lead', 'Leometric', 'NA', 0, '', '', '', '', 4, '5', 1, '2021-07-11 15:26:01', '2021-05-31', 0, 1, 2, 0, 5, '2021-05-11 15:26:01', '2021-05-11', NULL, 1, 'akshaydange@leometric.com', '', 1, '9405310077', NULL, 0, 0, 0, 0, NULL, 0, '', 1, 3, 47.00, 27.00),
	(5, '175d49e1abeeb8236442b0ceac0afc20-443bd4e445f36b7ff3ce5f9c9ee5a50d', 'Pusher Test', 'Rohit Saluja', '', 'Decoding IT', '', 0, '', '', '', '', 4, '1', 1, '2021-05-18 16:25:10', '2021-05-31', 0, 1, 2, 0, 5, '2021-05-18 16:25:10', '2021-05-18', '2021-05-19 15:02:48', 1, 'rohit@gmail.com', '', 1, '', NULL, 0, 0, 0, 0, NULL, 0, '', 2, 7, 2345.00, 123.00),
	(6, 'a6bb667edf97fae8a057267d028bbffd-b9a04fa2606b48f36d456298968e6a86', 'Pusher Test', 'Akshay B Dange', '', 'Akshay Technologies', 'NA', 102, '422102', 'Nashik', 'Maharashtra', 'Nashik', 4, '5', 5, '2021-11-19 18:43:03', '2021-05-19', 0, 2, 2, 0, 5, '2021-05-19 18:43:03', '2021-05-19', NULL, 1, 'akshay.dange@gmail.com', '', 1, '09405310077', NULL, 0, 0, 0, 0, NULL, 0, '', 0, 0, 2111.00, 121.00),
	(7, '1cd0d7eacd3b5248990c023f847dcdb5-8fc28e123cbe989e39cffdb45c5f2aca', 'Test Deal', 'Leometric | CRM ', '', 'Leometric', 'NA', 0, '', '', '', '', 4, '1', 1, '2021-05-19 18:47:07', '2021-05-27', 0, 2, 2, 0, 5, '2021-05-19 18:47:07', '2021-05-19', NULL, 1, 'sudarshan@leometric.com', '', 1, '', NULL, 0, 0, 0, 0, NULL, 0, '', 1, 22, 12121.00, 222.00),
	(8, 'dd7187deeb387288c0c2a931134d2140-ee5bb4764a756cb94cd5aa87a4ed3de9', 'Test Deal', 'Akshay Dange', 'Lead', 'Leometric', 'NA', 0, '', '', '', '', 4, '4', 1, '2021-05-26 12:45:26', '2021-06-04', 0, 1, 2, 0, 0, '2021-05-26 12:45:26', '2021-05-26', NULL, 1, 'akshaydange@leometric.com', '', 1, '9405310077', NULL, 0, 0, 0, 0, NULL, 0, '', 1, 3, 23232.00, 223.00);
/*!40000 ALTER TABLE `tblleads` ENABLE KEYS */;

-- Dumping structure for table crm.tblleads_email_integration
CREATE TABLE IF NOT EXISTS `tblleads_email_integration` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'the ID always must be 1',
  `active` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `imap_server` varchar(100) NOT NULL,
  `password` mediumtext NOT NULL,
  `check_every` int(11) NOT NULL DEFAULT '5',
  `responsible` int(11) NOT NULL,
  `lead_source` int(11) NOT NULL,
  `lead_status` int(11) NOT NULL,
  `encryption` varchar(3) DEFAULT NULL,
  `folder` varchar(100) NOT NULL,
  `last_run` varchar(50) DEFAULT NULL,
  `notify_lead_imported` tinyint(1) NOT NULL DEFAULT '1',
  `notify_lead_contact_more_times` tinyint(1) NOT NULL DEFAULT '1',
  `notify_type` varchar(20) DEFAULT NULL,
  `notify_ids` mediumtext,
  `mark_public` int(11) NOT NULL DEFAULT '0',
  `only_loop_on_unseen_emails` tinyint(1) NOT NULL DEFAULT '1',
  `delete_after_import` int(11) NOT NULL DEFAULT '0',
  `create_task_if_customer` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblleads_email_integration: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblleads_email_integration` DISABLE KEYS */;
INSERT INTO `tblleads_email_integration` (`id`, `active`, `email`, `imap_server`, `password`, `check_every`, `responsible`, `lead_source`, `lead_status`, `encryption`, `folder`, `last_run`, `notify_lead_imported`, `notify_lead_contact_more_times`, `notify_type`, `notify_ids`, `mark_public`, `only_loop_on_unseen_emails`, `delete_after_import`, `create_task_if_customer`) VALUES
	(1, 0, '', '', '', 10, 0, 0, 0, 'tls', 'INBOX', '', 1, 1, 'assigned', '', 0, 1, 0, 1);
/*!40000 ALTER TABLE `tblleads_email_integration` ENABLE KEYS */;

-- Dumping structure for table crm.tblleads_sources
CREATE TABLE IF NOT EXISTS `tblleads_sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblleads_sources: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblleads_sources` DISABLE KEYS */;
INSERT INTO `tblleads_sources` (`id`, `name`) VALUES
	(2, 'Facebook'),
	(1, 'Google');
/*!40000 ALTER TABLE `tblleads_sources` ENABLE KEYS */;

-- Dumping structure for table crm.tblleads_stage
CREATE TABLE IF NOT EXISTS `tblleads_stage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage` varchar(50) NOT NULL,
  `stageorder` int(11) DEFAULT NULL,
  `color` varchar(10) DEFAULT '#28B8DA',
  `isdefault` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stage` (`stage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblleads_stage: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblleads_stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblleads_stage` ENABLE KEYS */;

-- Dumping structure for table crm.tblleads_status
CREATE TABLE IF NOT EXISTS `tblleads_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `statusorder` int(11) DEFAULT NULL,
  `color` varchar(10) DEFAULT '#28B8DA',
  `isdefault` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblleads_status: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblleads_status` DISABLE KEYS */;
INSERT INTO `tblleads_status` (`id`, `name`, `category`, `statusorder`, `color`, `isdefault`) VALUES
	(1, 'Customer', NULL, 1000, '#7cb342', 1),
	(2, 'New Lead', 'Open', 2, '#28b8da', 0),
	(3, 'Open Won', 'Open', 3, '#28b8da', 0),
	(4, 'Closed Won', 'Closed', 4, '#00a110', 0),
	(6, 'Closed Lost', 'Closed', 5, '#ff6b00', 0);
/*!40000 ALTER TABLE `tblleads_status` ENABLE KEYS */;

-- Dumping structure for table crm.tbllead_activity_log
CREATE TABLE IF NOT EXISTS `tbllead_activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leadid` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `additional_data` text,
  `date` datetime NOT NULL,
  `staffid` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `custom_activity` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbllead_activity_log: ~34 rows (approximately)
/*!40000 ALTER TABLE `tbllead_activity_log` DISABLE KEYS */;
INSERT INTO `tbllead_activity_log` (`id`, `leadid`, `description`, `additional_data`, `date`, `staffid`, `full_name`, `custom_activity`) VALUES
	(1, 1, 'not_lead_activity_created', '', '2021-05-06 15:26:06', 1, 'Admin Admin', 0),
	(2, 1, 'not_lead_activity_converted', 'a:1:{i:0;s:11:"Admin Admin";}', '2021-05-06 15:27:35', 1, 'Admin Admin', 0),
	(3, 1, 'not_lead_activity_converted', 'a:1:{i:0;s:11:"Admin Admin";}', '2021-05-06 16:27:47', 1, 'Admin Admin', 0),
	(4, 1, 'not_lead_activity_converted_email', 'a:2:{i:0;s:15:"aarti@gmail.com";i:1;s:18:"aarti123@gmail.com";}', '2021-05-06 16:27:47', 1, 'Admin Admin', 0),
	(5, 2, 'not_lead_activity_created', '', '2021-05-07 16:29:33', 1, 'Admin Admin', 0),
	(6, 2, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:76:"<a href="http://localhost/crm/admin/profile/5" target="_blank">User User</a>";}', '2021-05-07 16:29:40', 1, 'Admin Admin', 0),
	(7, 2, 'not_lead_activity_converted', 'a:1:{i:0;s:11:"Admin Admin";}', '2021-05-07 17:22:17', 1, 'Admin Admin', 0),
	(8, 2, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"Customer";i:2;s:8:"New Lead";}', '2021-05-10 16:21:08', 1, 'Admin Admin', 0),
	(9, 2, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"New Lead";i:2;s:8:"Customer";}', '2021-05-10 16:42:24', 1, 'Admin Admin', 0),
	(10, 2, 'not_lead_activity_approval_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:1:"1";i:2;s:1:"2";}', '2021-05-10 17:49:07', 1, 'Admin Admin', 0),
	(11, 2, 'not_lead_activity_approval_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:1:"2";i:2;s:1:"1";}', '2021-05-10 17:49:14', 1, 'Admin Admin', 0),
	(12, 2, 'not_lead_activity_contacted', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:19:"2021-05-11 11:54:00";}', '2021-05-11 11:54:18', 1, 'Admin Admin', 0),
	(13, 2, 'not_lead_activity_added_attachment', '', '2021-05-11 11:54:39', 1, 'Admin Admin', 0),
	(14, 3, 'not_lead_activity_created', '', '2021-05-11 15:23:00', 1, 'Admin Admin', 0),
	(15, 3, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:76:"<a href="http://localhost/crm/admin/profile/5" target="_blank">User User</a>";}', '2021-05-11 15:23:08', 1, 'Admin Admin', 0),
	(16, 4, 'not_lead_activity_created', '', '2021-05-11 15:26:01', 1, 'Admin Admin', 0),
	(17, 4, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:75:"<a href="http://localhost/crm/admin/profile/4" target="_blank">Deepak G</a>";}', '2021-05-11 15:26:12', 1, 'Admin Admin', 0),
	(18, 4, 'not_lead_activity_approval_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:1:"0";i:2;s:1:"1";}', '2021-05-11 15:27:14', 1, 'Admin Admin', 0),
	(19, 3, 'not_lead_activity_approval_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:1:"0";i:2;s:1:"1";}', '2021-05-11 15:28:15', 1, 'Admin Admin', 0),
	(20, 3, 'not_lead_activity_converted', 'a:1:{i:0;s:11:"Admin Admin";}', '2021-05-11 19:04:56', 1, 'Admin Admin', 0),
	(21, 1, 'not_lead_activity_status_updated', 'a:3:{i:0;s:9:"User User";i:1;s:8:"Customer";i:2;s:8:"New Lead";}', '2021-05-12 14:50:26', 5, 'User User', 0),
	(22, 5, 'not_lead_activity_created', '', '2021-05-18 16:25:10', 1, 'Admin Admin', 0),
	(23, 5, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:75:"<a href="http://localhost/crm/admin/profile/4" target="_blank">Deepak G</a>";}', '2021-05-18 16:25:31', 1, 'Admin Admin', 0),
	(24, 5, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"New Lead";i:2;s:8:"Customer";}', '2021-05-19 15:02:48', 1, 'Admin Admin', 0),
	(25, 6, 'not_lead_activity_created', '', '2021-05-19 18:43:04', 1, 'Admin Admin', 0),
	(26, 6, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:75:"<a href="http://localhost/crm/admin/profile/4" target="_blank">Deepak G</a>";}', '2021-05-19 18:43:12', 1, 'Admin Admin', 0),
	(27, 7, 'not_lead_activity_created', '', '2021-05-19 18:47:08', 1, 'Admin Admin', 0),
	(28, 7, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:75:"<a href="http://localhost/crm/admin/profile/4" target="_blank">Deepak G</a>";}', '2021-05-19 18:47:11', 1, 'Admin Admin', 0),
	(29, 8, 'not_lead_activity_created', '', '2021-05-26 12:45:27', 1, 'Admin Admin', 0),
	(30, 8, 'not_lead_activity_assigned_to', 'a:2:{i:0;s:11:"Admin Admin";i:1;s:75:"<a href="http://localhost/crm/admin/profile/4" target="_blank">Deepak G</a>";}', '2021-05-26 12:45:37', 1, 'Admin Admin', 0),
	(31, 1, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"New Lead";i:2;s:8:"Open Won";}', '2021-05-26 13:05:51', 1, 'Admin Admin', 0),
	(32, 1, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"Open Won";i:2;s:8:"New Lead";}', '2021-05-26 13:08:47', 1, 'Admin Admin', 0),
	(33, 1, 'not_lead_activity_approval_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:1:"0";i:2;s:1:"1";}', '2021-05-26 13:09:02', 1, 'Admin Admin', 0),
	(34, 1, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"New Lead";i:2;s:8:"Open Won";}', '2021-05-26 13:09:14', 1, 'Admin Admin', 0),
	(35, 1, 'not_lead_activity_status_updated', 'a:3:{i:0;s:11:"Admin Admin";i:1;s:8:"Open Won";i:2;s:8:"New Lead";}', '2021-05-27 11:10:18', 1, 'Admin Admin', 0),
	(36, 1, 'not_activity_new_task_created', 'a:1:{i:0;s:108:"<a href="http://localhost/crm/admin/tasks/view/13" onclick="init_task_modal(13);return false;">Test Task</a>";}', '2021-07-16 17:13:38', 1, 'Admin Admin', 0),
	(37, 2, 'not_activity_new_task_created', 'a:1:{i:0;s:117:"<a href="http://localhost/crm/admin/tasks/view/14" onclick="init_task_modal(14);return false;">New Checklist Task</a>";}', '2021-07-16 17:17:54', 1, 'Admin Admin', 0);
/*!40000 ALTER TABLE `tbllead_activity_log` ENABLE KEYS */;

-- Dumping structure for table crm.tbllead_integration_emails
CREATE TABLE IF NOT EXISTS `tbllead_integration_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` mediumtext,
  `body` mediumtext,
  `dateadded` datetime NOT NULL,
  `leadid` int(11) NOT NULL,
  `emailid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbllead_integration_emails: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbllead_integration_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbllead_integration_emails` ENABLE KEYS */;

-- Dumping structure for table crm.tblmail_attachment
CREATE TABLE IF NOT EXISTS `tblmail_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL,
  `file_name` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'inbox',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmail_attachment: ~37 rows (approximately)
/*!40000 ALTER TABLE `tblmail_attachment` DISABLE KEYS */;
INSERT INTO `tblmail_attachment` (`id`, `mail_id`, `file_name`, `file_type`, `type`) VALUES
	(8, 49, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(9, 49, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(10, 49, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(11, 49, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(12, 49, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(13, 49, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox'),
	(14, 51, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(15, 51, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(16, 51, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(17, 51, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(18, 51, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(19, 51, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox'),
	(20, 52, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(21, 52, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(22, 52, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(23, 52, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(24, 52, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(25, 52, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox'),
	(26, 67, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(27, 67, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(28, 67, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(29, 67, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(30, 67, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(31, 67, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox'),
	(32, 32, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(33, 32, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(34, 32, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(35, 32, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(36, 32, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(37, 32, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox'),
	(38, 32, 'Tickets Pending tasks.xlsx', 'application/vnd.openxmlformats-officedocument.spre', 'inbox'),
	(39, 33, 'DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png', 'image/png', 'inbox'),
	(40, 33, '5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png', 'image/png', 'inbox'),
	(41, 33, '5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png', 'image/png', 'inbox'),
	(42, 33, '5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png', 'image/png', 'inbox'),
	(43, 33, '5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png', 'image/png', 'inbox'),
	(44, 33, 'SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png', 'image/png', 'inbox');
/*!40000 ALTER TABLE `tblmail_attachment` ENABLE KEYS */;

-- Dumping structure for table crm.tblmail_inbox
CREATE TABLE IF NOT EXISTS `tblmail_inbox` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_staff_id` int(11) NOT NULL DEFAULT '0',
  `to_staff_id` int(11) NOT NULL DEFAULT '0',
  `to` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cc` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcc` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `body` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `has_attachment` tinyint(1) NOT NULL DEFAULT '0',
  `date_received` datetime NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `folder` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inbox',
  `stared` tinyint(1) NOT NULL DEFAULT '0',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `from_email` varchar(150) DEFAULT NULL,
  `converted_incident` int(11) NOT NULL DEFAULT '0',
  `converted_sr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmail_inbox: ~32 rows (approximately)
/*!40000 ALTER TABLE `tblmail_inbox` DISABLE KEYS */;
INSERT INTO `tblmail_inbox` (`id`, `from_staff_id`, `to_staff_id`, `to`, `cc`, `bcc`, `sender_name`, `subject`, `body`, `has_attachment`, `date_received`, `read`, `folder`, `stared`, `important`, `trash`, `from_email`, `converted_incident`, `converted_sr`) VALUES
	(1, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Sudarshan Gadekar', 'Test', 'TestTestTestTestTestTestTest', 0, '2021-03-13 19:05:32', 0, 'inbox', 0, 0, 0, 'Sudarshan Gadekar <sudarshangadekar35@gmail.com>', 0, 0),
	(4, 0, 1, 'sudarshan@leometric.com', '', NULL, 'techlead@gmail.com', 'New Support Service Request Opened [service_request ID: 2]', 'Hi Sudarshan <br>New support service request has been opened.<br>Subject: Network Failure<br>Department: Technical Lead<br>Priority: Medium<br>service_request message:<br>Network Failure<br>You can view the service request on the following link: #2<br>Kind Regards,', 0, '2021-03-15 11:35:03', 1, 'inbox', 0, 0, 0, '"techlead@gmail.com" <techlead@gmail.com>', 0, 0),
	(8, 0, 1, 'sudarshan@leometric.com', '', NULL, 'akshaydange@leometric.com', 'Testing', 'Testing', 0, '2021-03-15 12:40:51', 1, 'inbox', 0, 0, 0, 'akshaydange@leometric.com', 0, 0),
	(9, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Kiran', 'Testing', 'Test<br><br><br>Thanks and regards,<br>Kiran G', 0, '2021-03-15 14:14:43', 0, 'inbox', 0, 0, 0, 'Kiran <kiran@leometric.com>', 0, 0),
	(10, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Kiran', 'Tesing', 'Test<br><br><br>Thanks and regards,<br>Kiran G', 0, '2021-03-15 14:18:04', 0, 'inbox', 0, 0, 0, 'Kiran <kiran@leometric.com>', 0, 1),
	(22, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy11.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser l16si13293768oii.183 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-15 15:29:35', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy11.mail.unifiedlayer.com>', 0, 0),
	(23, 0, 1, 'rohit@decodingit.com', 'monami@decodingit.com,gadekardhanesh@leometric.com,amolwale@leometric.com,sudarshan@leometric.com', NULL, 'akshaydange@leometric.com', 'Daily Update on ITSM project 15 March 2021', 'Hello Rohit,<br>Update on ITSM Project 15-March-2021:<br>- Added & showed Approve/Reject status in DataTable on SR listing page.<br>- Resolved approval status selection Bug in SR Edit(Setting tab).<br>- Display customer name on top and when on click name button redirect  <br>to customer view from SR Near status.<br>- Setting Tab Renamed as Details.<br>- Resolved contact name change issue in SR and Incident.<br>- Added interface for team role.<br>Thanks & Regards,<br>Akshay B. Dange<br>Leometric Technology Pvt. Ltd.', 0, '2021-03-15 22:18:10', 0, 'inbox', 0, 0, 0, 'akshaydange@leometric.com', 0, 0),
	(24, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Leometric Technology | CRM', 'New Project Discussion Created - ITSM', 'Hi Sudarshan<br>New project discussion created from Dhanesh Gadekar<br>Subject: Client Feedback<br>Description: Below are some requirements in Ticketing modules. Some already<br>discussed and few customer fields which not discussed before. <br>1<br>------------------------------------------<br>Change the Status Name Answered to Resolved<br>Notification required when status is marked to Resolved<br>2<br>------------------------------------------<br>Customer Company Name visibility required somehere in ticket form<br>3<br>------------------------------------------<br>Below Custom Fieds Required in Service Requests (not required in Incidents)<br>Title - Request For<br>Options List - Add, Remove, Change,Upgrade<br>Delivery -<br>Options List- Remove, Onsite , Remote/Onsite<br>Estimated Efforts<br>Options List : 15 Mins, 30 Mins, 60 Mins, 4 hours and so on<br>4<br>---------------------------------------<br>Below Customer Field required in Incident and Service Request Both.<br>Billing Type-<br>Options List - Contract, Paid ,Purchase Order<br>Contract - Choose Contract from Contract Data of Customer<br>Paid -  Choose Billing form Billing data of Customer<br>5<br>----------------------------------------------<br>Email send name should be created as new contact if it does not exist<br>earlier, If domain name is matching then it should go under company , if not<br>matching then create general.<br>6<br>---------------------------------------------<br>Under Customer Area Need new Section called Team<br>In this we need to create fields - Remote Support Engineer , Network<br>Engineer, Server Engineer and assign the persons from Staff.<br>In Ticket Settings<br>We need to display the designated Team Members<br>In Ticket Settings  also need to show the Approvers Name from customer side.<br>You can view the discussion on the following link: Client Feedback<br>Kind Regards,<br />                            Leometric Technology', 0, '2021-03-16 17:37:06', 0, 'inbox', 0, 0, 0, 'Leometric Technology | CRM <admin@leometric.com>', 0, 0),
	(25, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Leometric Technology | CRM', 'New Project File(s) Uploaded - ITSM', 'Hello Sudarshan<br>New project file is uploaded on ITSM from Dhanesh Gadekar<br>You can view the project on the following link: ITSM<br>To view the file you can click on the following link: Contract<br>details.xlsx<br>Kind Regards,<br />                            Leometric Technology', 0, '2021-03-16 17:42:31', 0, 'inbox', 0, 0, 0, 'Leometric Technology | CRM <admin@leometric.com>', 0, 0),
	(26, 1, 1, 'sudarshan@leometric.com', '', NULL, 'Leometric | CRM', 'Contract Expiration Reminder', 'Hi Admin<br>This is a reminder that the following contract will expire soon:<br>Subject: Test Contract<br>Description: Test <br>Date Start: 2021-03-16<br>Date End: 2021-03-18<br>Kind Regards,', 0, '2021-03-16 23:16:35', 1, 'inbox', 0, 0, 0, 'Leometric | CRM <sudarshan@leometric.com>', 0, 0),
	(27, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy9.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser i21si16956590otj.220 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-16 23:16:35', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy9.mail.unifiedlayer.com>', 0, 0),
	(28, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy13.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser k20si15921067ots.254 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-16 23:38:10', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy13.mail.unifiedlayer.com>', 0, 0),
	(29, 0, 1, 'sudarshan@leometric.com', '', NULL, 'techlead@gmail.com', 'New Support Ticket Opened [Ticket ID: 1]', 'Hi Leometric | CRM <br>New support ticket has been opened.<br>Subject: New Test Incident<br>Department: Technical Lead<br>Priority: Medium<br>Ticket message:<br>Test<br>You can view the ticket on the following link: #1<br>Kind Regards,', 0, '2021-03-17 13:08:33', 0, 'inbox', 0, 0, 0, '"techlead@gmail.com" <techlead@gmail.com>', 0, 0),
	(30, 1, 1, 'sudarshan@leometric.com', '', NULL, 'Leometric | CRM', 'Contract Expiration Reminder', 'Hi Admin<br>This is a reminder that the following contract will expire soon:<br>Subject: Test Contact<br>Description: tET<br>Date Start: 2021-03-17<br>Date End: 2021-03-19<br>Kind Regards,', 0, '2021-03-17 16:30:35', 0, 'inbox', 0, 0, 0, 'Leometric | CRM <sudarshan@leometric.com>', 0, 0),
	(31, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy7.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.115.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser s13si16552163oth.221 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-17 16:36:02', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy7.mail.unifiedlayer.com>', 0, 0),
	(32, 0, 1, 'akshaydange@leometric.com', 'monami@decodingit.com,gadekardhanesh@leometric.com,amolwale@leometric.com,sudarshan@leometric.com', NULL, 'Rohit Saluja', 'Re: Daily Update on ITSM project 15 March 2021', 'Dear Dhanesh<br>As discussed, Please find attached the list of pending tasks/bugs in Ticket module. It is application to incident and SR both.<br>Kind Regards,<br>Rohit Saluja<br>Managing Partner<br>M: +968 96016785 * O: +968 22844785<br>E: rohit@decodingit.com<br>A: PB C2921, PC 900. Al Udhaiba, Sultanate of Oman<br>[cid:DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png]<br>W: www.decodingit.com<br>[cid:5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png]  [cid:5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png]    [cid:5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png]    [cid:5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png] <br>[cid:SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png]<br>Network and Security           Datacenter Management and Business Continuity           Managed Services<br>________________________________<br>From: Rohit Saluja<br>Sent: Tuesday, March 16, 2021 12:44 PM<br>To: akshaydange@leometric.com <br>Cc: Monami B. Saluja ; gadekardhanesh ; amolwale ; sudarshan <br>Subject: RE: Daily Update on ITSM project 15 March 2021<br>Dear Akshay<br>Thanks for the update.<br>Can you make one list of pending activities in Incident/SR and estimated completion date, I will review from my side and confirm.<br>Please do a testing also for everything done so far.<br>-----Original Message-----<br>From: akshaydange@leometric.com <br>Sent: 15 March 2021 20:45<br>To: Rohit Saluja <br>Cc: Monami B. Saluja ; gadekardhanesh ; amolwale ; sudarshan <br>Subject: Daily Update on ITSM project 15 March 2021<br>Hello Rohit,<br>Update on ITSM Project 15-March-2021:<br>- Added & showed Approve/Reject status in DataTable on SR listing page.<br>- Resolved approval status selection Bug in SR Edit(Setting tab).<br>- Display customer name on top and when on click name button redirect to customer view from SR Near status.<br>- Setting Tab Renamed as Details.<br>- Resolved contact name change issue in SR and Incident.<br>- Added interface for team role.<br>Thanks & Regards,<br>Akshay B. Dange<br>Leometric Technology Pvt. Ltd.', 1, '2021-03-17 17:14:33', 1, 'inbox', 0, 0, 0, 'Rohit Saluja <rohit@decodingit.com>', 0, 0),
	(33, 0, 1, 'akshaydange@leometric.com', 'monami@decodingit.com,gadekardhanesh@leometric.com,amolwale@leometric.com,sudarshan@leometric.com', NULL, 'Rohit Saluja', 'Re: Daily Update on ITSM project 17 March 2021', 'Dear Akshay<br>Thanks.<br>please use the excel sheet which  I have shared yesterday for incident/SR  to give updates from today.<br>You can create one sheet for each date.<br>You can mark items as complete which is done but after taking confirmation.<br>Kind Regards,<br>Rohit Saluja<br>Managing Partner<br>M: +968 96016785 * O: +968 22844785<br>E: rohit@decodingit.com<br>A: PB C2921, PC 900. Al Udhaiba, Sultanate of Oman<br>[cid:DIT_logo_sign_186x77px_dee5dcd6-9e54-48f0-8f26-f36a8208d904.png]<br>W: www.decodingit.com<br>[cid:5363785-internetlinkedinmediasocialuiux_3ad53aa6-471c-445d-86b3-0f8c5fbe674d.png]  [cid:5363778-internetmediasocialtwitteruiux_7177ae99-a6ca-4132-8cfa-28cb50e60d4f.png]    [cid:5363775-facebookinternetmediasocialuiux_a652afc5-417d-428d-ba74-12cd5107736b.png]    [cid:5363784-instagraminternetmediasocialuiux_bd1b9fe0-8760-48c4-8001-346cedc70af5.png] <br>[cid:SignatureBannerDec20_25846570-5e29-4f67-a94e-ccf6a75c5e63.png]<br>Network and Security           Datacenter Management and Business Continuity           Managed Services<br>________________________________<br>From: akshaydange@leometric.com <br>Sent: Wednesday, March 17, 2021 5:58 PM<br>To: Rohit Saluja<br>Cc: Monami B. Saluja; gadekardhanesh; amolwale; sudarshan<br>Subject: Daily Update on ITSM project 17 March 2021<br>Hello Rohit,<br>Update on ITSM Project 17-March-2021:<br>- Added Additional service charges flow in incident.<br>- Added Contract field under Customer as per requirements.<br>- Done SLA Time in Incident and SR under customer.<br>- Added 2 new tabs in customer section i.e. SLA time & Additional Billing.<br>Thanks & Regards,<br>Akshay B. Dange<br>Leometric Technology Pvt. Ltd.', 1, '2021-03-18 10:41:34', 0, 'inbox', 0, 0, 0, 'Rohit Saluja <rohit@decodingit.com>', 0, 0),
	(34, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy6.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser i21si1128108otj.220 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 11:53:02', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy6.mail.unifiedlayer.com>', 0, 0),
	(35, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy13.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser a85si996749oib.3 - gsmtp (in<br>    reply to RCPT TO command)', 0, '2021-03-18 11:53:02', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy13.mail.unifiedlayer.com>', 0, 0),
	(36, 1, 1, 'sudarshan@leometric.com', '', NULL, 'sudarshan@leometric.com', 'Re: New Support Ticket Opened [Ticket ID: 4]', 'WORKING ON sOME iiSUE TESTING<br>On 2021-03-18 11:54 AM, Leometric | CRM wrote:<br>> Hi Leometric | CRM<br>> <br>> New support ticket has been opened.<br>> <br>> Subject: Test<br>> Department: Support and Maintence<br>> Priority: Medium<br>> <br>> Ticket message:<br>> Test<br>> <br>> You can view the ticket on the following link: #4 [1]<br>> <br>> Kind Regards,<br>> <br>> <br>> Links:<br>> ------<br>> [1] http://localhost/crm/forms/tickets/fa2e1983d664648dcbc177c11aaebf7a', 0, '2021-03-18 11:58:32', 0, 'inbox', 0, 0, 0, 'sudarshan@leometric.com', 0, 0),
	(37, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy7.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser m27si1189648otj.104 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 12:42:32', 0, 'inbox', 0, 0, 0, 'Mail Delivery System <MAILER-DAEMON@soproxy7.mail.unifiedlayer.com>', 0, 0),
	(38, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Sudarshan Gadekar', 'Re: New Support Ticket Opened [Ticket ID: 5]', 'Test Done<br>On Thu, Mar 18, 2021 at 2:22 PM Leometric | CRM <br>wrote:<br>> Hi Test<br>><br>> New support ticket has been opened.<br>><br>> *Subject:* Test<br>> *Department:* Support and Maintence<br>> *Priority:* Medium<br>><br>> *Ticket message:*<br>> Test<br>><br>> You can view the ticket on the following link: #5<br>> <br>><br>> Kind Regards,<br>>', 0, '2021-03-18 14:23:47', 0, 'inbox', 0, 0, 0, 'Sudarshan Gadekar <sudarshangadekar35@gmail.com>', 0, 0),
	(39, 0, 1, 'sudarshan@leometric.com', '', NULL, 'techlead@gmail.com', 'New Ticket Reply [Ticket ID: 1]', 'Hi Leometric | CRM <br>You have a new ticket reply to ticket #1<br>Ticket Subject: Test Contract In Mail<br>Ticket message:<br>You can view the ticket on the following link: #1<br>Kind Regards,', 0, '2021-03-18 14:56:33', 0, 'inbox', 0, 0, 0, '"techlead@gmail.com" <techlead@gmail.com>', 0, 0),
	(40, 0, 1, 'sudarshan@leometric.com', '', NULL, 'techlead@gmail.com', 'New Ticket Reply [Ticket ID: 1]', 'Hi Leometric | CRM <br>You have a new ticket reply to ticket #1<br>Ticket Subject: Test Contract In Mail<br>Ticket message:<br>You can view the ticket on the following link: #1<br>Kind Regards,', 0, '2021-03-18 14:56:33', 0, 'inbox', 0, 0, 0, '"techlead@gmail.com" <techlead@gmail.com>', 0, 0),
	(41, 0, 1, 'sudarshan@leometric.com', '', NULL, 'techlead@gmail.com', 'New Support Ticket Opened [Ticket ID: 2]', 'Hi Leometric | CRM <br>New support ticket has been opened.<br>Subject: Test<br>Department: Technical Lead<br>Priority: Medium<br>Billing Type : contract<br>Contract : Test Contract<br>Ticket message:<br>Test<br>You can view the ticket on the following link: #2<br>Kind Regards,', 0, '2021-03-18 15:02:04', 1, 'inbox', 0, 0, 0, '"techlead@gmail.com" <techlead@gmail.com>', 0, 0),
	(42, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Sudarshan Gadekar', 'Re: New Support Service Request Opened [service_request ID: 1]', 'I haven\'t received it yet.<br>On Thu, 18 Mar 2021, 4:04 pm Leometric | CRM, <br>wrote:<br>> Hi Sudarshan<br>><br>> New support service request has been opened.<br>><br>> *Subject:* Test SR<br>> *Department:* Support and Maintence<br>> *Priority:* Low<br>><br>> *service_request message:*<br>> Tetsing mail<br>><br>> You can view the service request on the following link: #1<br>> <br>><br>> Kind Regards,<br>>', 0, '2021-03-18 16:30:28', 1, 'inbox', 0, 0, 0, 'Sudarshan Gadekar <sudarshangadekar35@gmail.com>', 0, 0),
	(43, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy13.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.115.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser w131si1265754oie.53 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 16:58:02', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy13.mail.unifiedlayer.com>', 0, 0),
	(44, 0, 1, 'sudarshan@leometric.com', '', NULL, 'leocrmtest@gmail.com', 'New Support Ticket Opened [Ticket ID: 1]', 'Hi Leometric | CRM <br>New support ticket has been opened.<br>Subject: Test Incident To Detect issue of worklogs<br>Department: Support and Maintence<br>Priority: Low<br>Billing Type : contract<br>Contract : Test Contact<br>Ticket message:<br>Test<br>You can view the ticket on the following link: #1<br>Kind Regards,', 0, '2021-03-18 17:03:32', 0, 'inbox', 0, 0, 1, '"leocrmtest@gmail.com" <leocrmtest@gmail.com>', 0, 0),
	(45, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy4.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.113.27] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser v11si1414793oig.104 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 17:03:32', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy4.mail.unifiedlayer.com>', 0, 0),
	(46, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy14.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.138.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser f12si1329377oti.320 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 17:09:03', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy14.mail.unifiedlayer.com>', 0, 0),
	(47, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy3.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.114.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser 60si1320096ots.3 - gsmtp (in<br>    reply to RCPT TO command)', 0, '2021-03-18 18:14:32', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy3.mail.unifiedlayer.com>', 0, 0),
	(48, 0, 1, 'sudarshan@leometric.com', '', NULL, 'Mail Delivery System', 'Undelivered Mail Returned to Sender', 'This is the mail system at host soproxy6.mail.unifiedlayer.com.<br>I\'m sorry to have to inform you that your message could not<br>be delivered to one or more recipients. It\'s attached below.<br>For further assistance, please send mail to postmaster.<br>If you do so, please include this problem report. You can<br>delete your own text from the attached returned message.<br>                   The mail system<br>: host gmail-smtp-in.l.google.com[142.250.114.26] said:<br>    550-5.1.1 The email account that you tried to reach does not exist. Please<br>    try 550-5.1.1 double-checking the recipient\'s email address for typos or<br>    550-5.1.1 unnecessary spaces. Learn more at 550 5.1.1<br>    https://support.google.com/mail/?p=NoSuchUser b12si1445244oiy.52 - gsmtp<br>    (in reply to RCPT TO command)', 0, '2021-03-18 18:14:33', 0, 'inbox', 0, 0, 1, 'Mail Delivery System <MAILER-DAEMON@soproxy6.mail.unifiedlayer.com>', 0, 0);
/*!40000 ALTER TABLE `tblmail_inbox` ENABLE KEYS */;

-- Dumping structure for table crm.tblmail_outbox
CREATE TABLE IF NOT EXISTS `tblmail_outbox` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_staff_id` int(11) NOT NULL DEFAULT '0',
  `to` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cc` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcc` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `body` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `has_attachment` tinyint(1) NOT NULL DEFAULT '0',
  `date_sent` datetime NOT NULL,
  `stared` tinyint(1) NOT NULL DEFAULT '0',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `reply_from_id` int(11) DEFAULT NULL,
  `reply_type` varchar(45) NOT NULL DEFAULT 'inbox',
  `draft` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmail_outbox: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblmail_outbox` DISABLE KEYS */;
INSERT INTO `tblmail_outbox` (`id`, `sender_staff_id`, `to`, `cc`, `bcc`, `sender_name`, `subject`, `body`, `has_attachment`, `date_sent`, `stared`, `important`, `trash`, `reply_from_id`, `reply_type`, `draft`) VALUES
	(5, 3, 'akshaydange@leometric.com', 'sudarshan@leometric.com', NULL, 'Sudarshan Gadekar', 'Test Msg From staff', '', 0, '2021-02-17 15:18:45', 0, 0, 0, NULL, 'inbox', 0),
	(10, 1, 'itsm@powermyit.com', '', NULL, 'Admin Admin', 'Test Mail ', 'This is an test mail', 0, '2021-03-01 11:15:18', 0, 0, 0, NULL, 'inbox', 0),
	(11, 1, 'sudarshan@leometric.com', '', NULL, 'Admin Admin', 'test', 'test', 0, '2021-03-03 14:04:38', 0, 0, 0, NULL, 'inbox', 0),
	(12, 1, 'akshay.dange74@gmail.com', 'akshay.dange74@gmail.com', NULL, 'Admin Admin', 'Test', 'Test', 0, '2021-04-09 15:38:31', 0, 0, 0, NULL, 'inbox', 0);
/*!40000 ALTER TABLE `tblmail_outbox` ENABLE KEYS */;

-- Dumping structure for table crm.tblmail_queue
CREATE TABLE IF NOT EXISTS `tblmail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engine` varchar(40) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `cc` text,
  `bcc` text,
  `message` mediumtext NOT NULL,
  `alt_message` mediumtext,
  `status` enum('pending','sending','sent','failed') DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `headers` text,
  `attachments` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmail_queue: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblmail_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblmail_queue` ENABLE KEYS */;

-- Dumping structure for table crm.tblmigrations
CREATE TABLE IF NOT EXISTS `tblmigrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmigrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblmigrations` DISABLE KEYS */;
INSERT INTO `tblmigrations` (`version`) VALUES
	(273);
/*!40000 ALTER TABLE `tblmigrations` ENABLE KEYS */;

-- Dumping structure for table crm.tblmilestones
CREATE TABLE IF NOT EXISTS `tblmilestones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `description` text,
  `description_visible_to_customer` tinyint(1) DEFAULT '0',
  `due_date` date NOT NULL,
  `project_id` int(11) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `milestone_order` int(11) NOT NULL DEFAULT '0',
  `datecreated` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmilestones: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblmilestones` DISABLE KEYS */;
INSERT INTO `tblmilestones` (`id`, `name`, `description`, `description_visible_to_customer`, `due_date`, `project_id`, `color`, `milestone_order`, `datecreated`) VALUES
	(1, 'Test', 'Testing', 0, '2021-11-11', 2, '#4554', 0, '2021-11-11'),
	(2, 'sdkj', 'kjdjd', 0, '2021-11-11', 2, '#GH778', 0, '2021-11-11'),
	(3, 'ldldldld', 'dxchdhdhdhd', 0, '2021-11-11', 1, NULL, 0, '2021-11-11');
/*!40000 ALTER TABLE `tblmilestones` ENABLE KEYS */;

-- Dumping structure for table crm.tblmodules
CREATE TABLE IF NOT EXISTS `tblmodules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(55) NOT NULL,
  `installed_version` varchar(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblmodules: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblmodules` DISABLE KEYS */;
INSERT INTO `tblmodules` (`id`, `module_name`, `installed_version`, `active`) VALUES
	(1, 'menu_setup', '2.3.0', 1),
	(3, 'mailbox', '1.0.0', 1),
	(4, 'theme_style', '2.3.0', 1),
	(5, 'api', '1.0.2', 0),
	(6, 'goals', '2.3.0', 1);
/*!40000 ALTER TABLE `tblmodules` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetworks
CREATE TABLE IF NOT EXISTS `tblnetworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(25) DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblnetworks: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblnetworks` DISABLE KEYS */;
INSERT INTO `tblnetworks` (`id`, `type`, `clientid`, `title`, `created_at`, `updated_at`) VALUES
	(1, 1, 5, 'Prosperous Farmer', '2021-04-05 14:52:17', '2021-10-21 13:31:08'),
	(2, 14, 5, 'Test Report File', '2021-04-10 18:22:43', '2021-10-25 19:05:35'),
	(3, 1, 3, 'Server Test', '2021-04-12 18:44:23', '2021-10-28 14:02:11'),
	(4, 1, 3, 'Testing', '2021-10-21 13:30:39', '2021-10-21 13:30:39');
/*!40000 ALTER TABLE `tblnetworks` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetwork_assets
CREATE TABLE IF NOT EXISTS `tblnetwork_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblnetwork_assets: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnetwork_assets` DISABLE KEYS */;
INSERT INTO `tblnetwork_assets` (`id`, `network_id`, `inventory_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2021-04-05 14:56:23', '2021-04-05 14:56:23'),
	(2, 1, 2, '2021-10-26 16:39:02', '2021-10-26 16:39:02');
/*!40000 ALTER TABLE `tblnetwork_assets` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetwork_diagrams
CREATE TABLE IF NOT EXISTS `tblnetwork_diagrams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `diagram_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblnetwork_diagrams: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnetwork_diagrams` DISABLE KEYS */;
INSERT INTO `tblnetwork_diagrams` (`id`, `network_id`, `diagram_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2021-04-05 14:52:31', '2021-04-05 14:52:31');
/*!40000 ALTER TABLE `tblnetwork_diagrams` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetwork_gaps
CREATE TABLE IF NOT EXISTS `tblnetwork_gaps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `gap_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblnetwork_gaps: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnetwork_gaps` DISABLE KEYS */;
INSERT INTO `tblnetwork_gaps` (`id`, `network_id`, `gap_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2021-04-05 14:54:19', '2021-04-05 14:54:19');
/*!40000 ALTER TABLE `tblnetwork_gaps` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetwork_software_licenses
CREATE TABLE IF NOT EXISTS `tblnetwork_software_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `renewal_id` int(11) NOT NULL COMMENT 'renewal renamed as software and licenses',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblnetwork_software_licenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnetwork_software_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblnetwork_software_licenses` ENABLE KEYS */;

-- Dumping structure for table crm.tblnetwork_urls
CREATE TABLE IF NOT EXISTS `tblnetwork_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `monitoring_title` varchar(50) DEFAULT NULL,
  `url` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblnetwork_urls: ~1 rows (approximately)
/*!40000 ALTER TABLE `tblnetwork_urls` DISABLE KEYS */;
INSERT INTO `tblnetwork_urls` (`id`, `network_id`, `monitoring_title`, `url`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, 'https://youtu.be/lV6QKINJgNs', '2021-04-05 14:53:55', '2021-04-05 14:53:55'),
	(2, 1, 'Test Monitoring', 'https://nms.decodingit.com:9090/public/mapshow.htm?id=2725&mapid=F7516F67-8885-4310-8807-3FEC61F549B9', '2021-12-07 18:38:09', '2021-12-07 18:38:09');
/*!40000 ALTER TABLE `tblnetwork_urls` ENABLE KEYS */;

-- Dumping structure for table crm.tblnewsfeed_comment_likes
CREATE TABLE IF NOT EXISTS `tblnewsfeed_comment_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(11) NOT NULL,
  `commentid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `dateliked` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnewsfeed_comment_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnewsfeed_comment_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblnewsfeed_comment_likes` ENABLE KEYS */;

-- Dumping structure for table crm.tblnewsfeed_posts
CREATE TABLE IF NOT EXISTS `tblnewsfeed_posts` (
  `postid` int(11) NOT NULL AUTO_INCREMENT,
  `creator` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `visibility` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `pinned` int(11) NOT NULL,
  `datepinned` datetime DEFAULT NULL,
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnewsfeed_posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnewsfeed_posts` DISABLE KEYS */;
INSERT INTO `tblnewsfeed_posts` (`postid`, `creator`, `datecreated`, `visibility`, `content`, `pinned`, `datepinned`) VALUES
	(1, 1, '2021-02-15 17:30:28', 'all', '', 0, NULL);
/*!40000 ALTER TABLE `tblnewsfeed_posts` ENABLE KEYS */;

-- Dumping structure for table crm.tblnewsfeed_post_comments
CREATE TABLE IF NOT EXISTS `tblnewsfeed_post_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `userid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnewsfeed_post_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnewsfeed_post_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblnewsfeed_post_comments` ENABLE KEYS */;

-- Dumping structure for table crm.tblnewsfeed_post_likes
CREATE TABLE IF NOT EXISTS `tblnewsfeed_post_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `dateliked` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnewsfeed_post_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnewsfeed_post_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblnewsfeed_post_likes` ENABLE KEYS */;

-- Dumping structure for table crm.tblnewsletters
CREATE TABLE IF NOT EXISTS `tblnewsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `clientid` int(11) NOT NULL,
  `nl_file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblnewsletters: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblnewsletters` DISABLE KEYS */;
INSERT INTO `tblnewsletters` (`id`, `type`, `title`, `clientid`, `nl_file`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 1, 'Test File Upload', 0, 'contract-base-decoding-it-solutions.pdf', '2021-12-08 12:37:30', 1, '2021-12-08 12:37:30', NULL),
	(2, 2, 'New Educational Contents Edited', 0, 'CTA-01 (1).png', '2021-12-08 12:38:45', 1, '2021-12-08 12:42:32', NULL);
/*!40000 ALTER TABLE `tblnewsletters` ENABLE KEYS */;

-- Dumping structure for table crm.tblnotes
CREATE TABLE IF NOT EXISTS `tblnotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) NOT NULL,
  `description` text,
  `date_contacted` datetime DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnotes: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblnotes` DISABLE KEYS */;
INSERT INTO `tblnotes` (`id`, `rel_id`, `rel_type`, `description`, `date_contacted`, `addedfrom`, `dateadded`) VALUES
	(2, 2, 'customer', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 1, '2021-03-13 11:50:26'),
	(3, 11, 'customer', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', NULL, 1, '2021-03-13 11:50:56'),
	(4, 2, 'customer', 'Test', NULL, 1, '2021-03-13 12:04:59'),
	(5, 2, 'lead', 'Test Note', NULL, 1, '2021-05-11 11:53:58'),
	(6, 2, 'lead', 'Testing notes', '2021-05-11 11:54:00', 1, '2021-05-11 11:54:18'),
	(7, 1, 'service_request', 'Test', NULL, 1, '2021-07-20 12:59:49');
/*!40000 ALTER TABLE `tblnotes` ENABLE KEYS */;

-- Dumping structure for table crm.tblnotifications
CREATE TABLE IF NOT EXISTS `tblnotifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isread` int(11) NOT NULL DEFAULT '0',
  `isread_inline` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `description` text NOT NULL,
  `fromuserid` int(11) NOT NULL,
  `fromclientid` int(11) NOT NULL DEFAULT '0',
  `from_fullname` varchar(100) NOT NULL,
  `touserid` int(11) NOT NULL,
  `toclientid` int(11) DEFAULT NULL,
  `fromcompany` int(11) DEFAULT NULL,
  `link` mediumtext,
  `additional_data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblnotifications: ~19 rows (approximately)
/*!40000 ALTER TABLE `tblnotifications` DISABLE KEYS */;
INSERT INTO `tblnotifications` (`id`, `isread`, `isread_inline`, `date`, `description`, `fromuserid`, `fromclientid`, `from_fullname`, `touserid`, `toclientid`, `fromcompany`, `link`, `additional_data`) VALUES
	(1, 0, 0, '2021-05-18 16:25:10', 'not_assigned_lead_to_you', 1, 0, 'Admin Admin', 4, NULL, NULL, '#leadid=5', 'a:1:{i:0;s:12:"Rohit Saluja";}'),
	(2, 1, 1, '2021-05-18 16:25:31', 'New deal added. please check & approve', 0, 0, '', 5, NULL, 1, 'leads', 'a:1:{i:0;i:5;}'),
	(3, 0, 0, '2021-05-19 18:43:04', 'not_assigned_lead_to_you', 1, 0, 'Admin Admin', 4, NULL, NULL, '#leadid=6', 'a:1:{i:0;s:14:"Akshay B Dange";}'),
	(4, 0, 0, '2021-05-19 18:43:12', 'New deal added. please check & approve', 0, 0, '', 5, NULL, 1, 'leads', 'a:1:{i:0;i:6;}'),
	(5, 0, 0, '2021-05-19 18:47:08', 'not_assigned_lead_to_you', 1, 0, 'Admin Admin', 4, NULL, NULL, '#leadid=7', 'a:1:{i:0;s:16:"Leometric | CRM ";}'),
	(6, 0, 0, '2021-05-19 18:47:11', 'New deal added. please check & approve', 0, 0, '', 5, NULL, 1, 'leads', 'a:1:{i:0;i:7;}'),
	(7, 0, 0, '2021-05-26 12:45:27', 'not_assigned_lead_to_you', 1, 0, 'Admin Admin', 4, NULL, NULL, '#leadid=8', 'a:1:{i:0;s:12:"Akshay Dange";}'),
	(8, 0, 0, '2021-05-26 12:45:37', 'New deal added. please check & approve', 0, 0, '', 0, NULL, 1, 'leads', 'a:1:{i:0;i:8;}'),
	(9, 0, 0, '2021-05-26 18:06:19', 'new_task_assigned_non_user', 0, 0, '', 4, NULL, 1, '#taskid=11', 'a:1:{i:0;s:21:"Test Ticket  01 Task ";}'),
	(10, 1, 0, '2021-09-21 13:12:23', 'not_customer_viewed_estimate', 0, 3, 'Akshay Dange', 1, NULL, NULL, 'estimates/list_estimates/1', 'a:1:{i:0;s:10:"EST-000001";}'),
	(11, 0, 0, '2021-11-09 12:54:43', 'not_contract_comment_from_client', 0, 37, '', 1, 5, 1, 'contracts/contract/2', 'a:1:{i:0;s:8:"Apprisal";}'),
	(12, 0, 0, '2021-11-09 12:55:09', 'not_contract_comment_from_client', 0, 37, '', 1, 5, 1, 'contracts/contract/2', 'a:1:{i:0;s:8:"Apprisal";}'),
	(13, 0, 0, '2021-11-13 13:27:08', 'not_task_assigned_to_you', 1, 0, 'Admin Admin', 5, NULL, NULL, '#taskid=15', 'a:1:{i:0;s:14:"Akshay B Dange";}'),
	(14, 0, 0, '2021-11-13 13:27:19', 'not_task_assigned_to_you', 1, 0, 'Admin Admin', 4, NULL, NULL, '#taskid=15', 'a:1:{i:0;s:14:"Akshay B Dange";}'),
	(15, 0, 0, '2021-11-13 13:27:22', 'not_task_assigned_someone', 1, 0, 'Admin Admin', 5, 5, NULL, '#taskid=15', 'a:2:{i:0;s:8:"Deepak G";i:1;s:14:"Akshay B Dange";}'),
	(16, 0, 0, '2021-11-17 12:52:15', 'not_customer_uploaded_file', 0, 37, 'Akki Dange', 1, NULL, NULL, 'clients/client/5?group=attachments', NULL),
	(17, 0, 0, '2021-11-17 17:08:56', 'not_customer_uploaded_file', 0, 37, 'Akki Dange', 1, NULL, NULL, 'clients/client/5?group=attachments', NULL),
	(18, 0, 0, '2021-11-25 13:58:49', 'Ticket 2 status marked to Resolved', 0, 0, '', 0, 0, 1, 'clients/ticket/2', NULL),
	(19, 0, 0, '2021-12-06 14:31:32', 'not_customer_uploaded_file', 0, 37, 'Akki Dange', 1, NULL, NULL, 'clients/client/5?group=attachments', NULL);
/*!40000 ALTER TABLE `tblnotifications` ENABLE KEYS */;

-- Dumping structure for table crm.tblnotification_members
CREATE TABLE IF NOT EXISTS `tblnotification_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblnotification_members: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblnotification_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblnotification_members` ENABLE KEYS */;

-- Dumping structure for table crm.tbloptions
CREATE TABLE IF NOT EXISTS `tbloptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `value` longtext NOT NULL,
  `autoload` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbloptions: ~418 rows (approximately)
/*!40000 ALTER TABLE `tbloptions` DISABLE KEYS */;
INSERT INTO `tbloptions` (`id`, `name`, `value`, `autoload`) VALUES
	(1, 'dateformat', 'Y-m-d|%Y-%m-%d', 1),
	(2, 'companyname', 'Leometric', 1),
	(3, 'services', '1', 1),
	(4, 'maximum_allowed_ticket_attachments', '4', 1),
	(5, 'ticket_attachments_file_extensions', '.jpg,.png,.pdf,.doc,.zip,.rar', 1),
	(6, 'staff_access_only_assigned_departments', '1', 1),
	(7, 'use_knowledge_base', '1', 1),
	(8, 'smtp_email', 'akshaydange@leometric.com', 1),
	(9, 'smtp_password', '3f7a74b382d9195ee3c5935a25c294bdd1a287460f0d3df73189f035c2f25ad3ce7f7b5f09a8be8ae0100a67d86778420be3a083df041785b44a0f3bfb033a9aiHZnyWMCyVLWeuNWdEIMXUedOmTZmLR7sfL+UyHGAzg=', 1),
	(10, 'company_info_format', '{company_name}<br />\r\r\n{address}<br />\r\r\n{city} {state}<br />\r\r\n{country_code} {zip_code}<br />\r\r\n{vat_number_with_label}', 0),
	(11, 'smtp_port', '465', 1),
	(12, 'smtp_host', 'leometric.com', 1),
	(13, 'smtp_email_charset', 'utf-8', 1),
	(14, 'default_timezone', 'Asia/Kolkata', 1),
	(15, 'clients_default_theme', 'leometric', 1),
	(16, 'company_logo', 'c1eecc062fa266ee3fc8f8c7436312cf.png', 1),
	(17, 'tables_pagination_limit', '25', 1),
	(18, 'main_domain', 'leometric.com', 1),
	(19, 'allow_registration', '0', 1),
	(20, 'knowledge_base_without_registration', '1', 1),
	(21, 'email_signature', '', 1),
	(22, 'default_staff_role', '1', 1),
	(23, 'newsfeed_maximum_files_upload', '10', 1),
	(24, 'contract_expiration_before', '4', 1),
	(25, 'invoice_prefix', 'INV-', 1),
	(26, 'decimal_separator', '.', 1),
	(27, 'thousand_separator', ',', 1),
	(28, 'invoice_company_name', '', 1),
	(29, 'invoice_company_address', '', 1),
	(30, 'invoice_company_city', '', 1),
	(31, 'invoice_company_country_code', '', 1),
	(32, 'invoice_company_postal_code', '', 1),
	(33, 'invoice_company_phonenumber', '', 1),
	(34, 'view_invoice_only_logged_in', '0', 1),
	(35, 'invoice_number_format', '1', 1),
	(36, 'next_invoice_number', '1', 0),
	(37, 'active_language', 'english', 1),
	(38, 'invoice_number_decrement_on_delete', '1', 1),
	(39, 'automatically_send_invoice_overdue_reminder_after', '1', 1),
	(40, 'automatically_resend_invoice_overdue_reminder_after', '3', 1),
	(41, 'expenses_auto_operations_hour', '21', 1),
	(42, 'delete_only_on_last_invoice', '1', 1),
	(43, 'delete_only_on_last_estimate', '1', 1),
	(44, 'create_invoice_from_recurring_only_on_paid_invoices', '0', 1),
	(45, 'allow_payment_amount_to_be_modified', '1', 1),
	(46, 'rtl_support_client', '0', 1),
	(47, 'limit_top_search_bar_results_to', '10', 1),
	(48, 'estimate_prefix', 'EST-', 1),
	(49, 'next_estimate_number', '3', 0),
	(50, 'estimate_number_decrement_on_delete', '1', 1),
	(51, 'estimate_number_format', '1', 1),
	(52, 'estimate_auto_convert_to_invoice_on_client_accept', '1', 1),
	(53, 'exclude_estimate_from_client_area_with_draft_status', '1', 1),
	(54, 'rtl_support_admin', '0', 1),
	(55, 'last_cron_run', '1622032577', 1),
	(56, 'show_sale_agent_on_estimates', '1', 1),
	(57, 'show_sale_agent_on_invoices', '1', 1),
	(58, 'predefined_terms_invoice', '', 1),
	(59, 'predefined_terms_estimate', '', 1),
	(60, 'default_task_priority', '2', 1),
	(61, 'dropbox_app_key', '', 1),
	(62, 'show_expense_reminders_on_calendar', '1', 1),
	(63, 'only_show_contact_tickets', '1', 1),
	(64, 'predefined_clientnote_invoice', '', 1),
	(65, 'predefined_clientnote_estimate', '', 1),
	(66, 'custom_pdf_logo_image_url', '', 1),
	(67, 'favicon', 'favicon.png', 1),
	(68, 'invoice_due_after', '30', 1),
	(69, 'google_api_key', '', 1),
	(70, 'google_calendar_main_calendar', '', 1),
	(71, 'default_tax', 'a:0:{}', 1),
	(72, 'show_invoices_on_calendar', '1', 1),
	(73, 'show_estimates_on_calendar', '1', 1),
	(74, 'show_contracts_on_calendar', '1', 1),
	(75, 'show_tasks_on_calendar', '1', 1),
	(76, 'show_customer_reminders_on_calendar', '1', 1),
	(77, 'output_client_pdfs_from_admin_area_in_client_language', '0', 1),
	(78, 'show_lead_reminders_on_calendar', '1', 1),
	(79, 'send_estimate_expiry_reminder_before', '4', 1),
	(80, 'leads_default_source', '', 1),
	(81, 'leads_default_status', '', 1),
	(82, 'proposal_expiry_reminder_enabled', '1', 1),
	(83, 'send_proposal_expiry_reminder_before', '4', 1),
	(84, 'default_contact_permissions', 'a:6:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";i:5;s:1:"6";}', 1),
	(85, 'pdf_logo_width', '150', 1),
	(86, 'access_tickets_to_none_staff_members', '0', 1),
	(87, 'customer_default_country', '', 1),
	(88, 'view_estimate_only_logged_in', '0', 1),
	(89, 'show_status_on_pdf_ei', '1', 1),
	(90, 'email_piping_only_replies', '0', 1),
	(91, 'email_piping_only_registered', '0', 1),
	(92, 'default_view_calendar', 'month', 1),
	(93, 'email_piping_default_priority', '2', 1),
	(94, 'total_to_words_lowercase', '0', 1),
	(95, 'show_tax_per_item', '1', 1),
	(96, 'total_to_words_enabled', '0', 1),
	(97, 'receive_notification_on_new_ticket', '1', 0),
	(98, 'autoclose_tickets_after', '0', 1),
	(99, 'media_max_file_size_upload', '10', 1),
	(100, 'client_staff_add_edit_delete_task_comments_first_hour', '0', 1),
	(101, 'show_projects_on_calendar', '1', 1),
	(102, 'leads_kanban_limit', '50', 1),
	(103, 'tasks_reminder_notification_before', '2', 1),
	(104, 'pdf_font', 'freesans', 1),
	(105, 'pdf_table_heading_color', '#323a45', 1),
	(106, 'pdf_table_heading_text_color', '#ffffff', 1),
	(107, 'pdf_font_size', '10', 1),
	(108, 'default_leads_kanban_sort', 'leadorder', 1),
	(109, 'default_leads_kanban_sort_type', 'asc', 1),
	(110, 'allowed_files', '.png,.jpg,.pdf,.doc,.docx,.xls,.xlsx,.zip,.rar,.txt', 1),
	(111, 'show_all_tasks_for_project_member', '1', 1),
	(112, 'email_protocol', 'smtp', 1),
	(113, 'calendar_first_day', '0', 1),
	(114, 'recaptcha_secret_key', '', 1),
	(115, 'show_help_on_setup_menu', '1', 1),
	(116, 'show_proposals_on_calendar', '1', 1),
	(117, 'smtp_encryption', 'ssl', 1),
	(118, 'recaptcha_site_key', '', 1),
	(119, 'smtp_username', 'akshaydange@leometric.com', 1),
	(120, 'auto_stop_tasks_timers_on_new_timer', '1', 1),
	(121, 'notification_when_customer_pay_invoice', '1', 1),
	(122, 'calendar_invoice_color', '#FF6F00', 1),
	(123, 'calendar_estimate_color', '#FF6F00', 1),
	(124, 'calendar_proposal_color', '#84c529', 1),
	(125, 'new_task_auto_assign_current_member', '1', 1),
	(126, 'calendar_reminder_color', '#03A9F4', 1),
	(127, 'calendar_contract_color', '#B72974', 1),
	(128, 'calendar_project_color', '#B72974', 1),
	(129, 'update_info_message', '', 1),
	(130, 'show_estimate_reminders_on_calendar', '1', 1),
	(131, 'show_invoice_reminders_on_calendar', '1', 1),
	(132, 'show_proposal_reminders_on_calendar', '1', 1),
	(133, 'proposal_due_after', '7', 1),
	(134, 'allow_customer_to_change_ticket_status', '0', 1),
	(135, 'lead_lock_after_convert_to_customer', '0', 1),
	(136, 'default_proposals_pipeline_sort', 'pipeline_order', 1),
	(137, 'default_proposals_pipeline_sort_type', 'asc', 1),
	(138, 'default_estimates_pipeline_sort', 'pipeline_order', 1),
	(139, 'default_estimates_pipeline_sort_type', 'asc', 1),
	(140, 'use_recaptcha_customers_area', '0', 1),
	(141, 'remove_decimals_on_zero', '0', 1),
	(142, 'remove_tax_name_from_item_table', '0', 1),
	(143, 'pdf_format_invoice', 'A4-PORTRAIT', 1),
	(144, 'pdf_format_estimate', 'A4-PORTRAIT', 1),
	(145, 'pdf_format_proposal', 'A4-PORTRAIT', 1),
	(146, 'pdf_format_payment', 'A4-PORTRAIT', 1),
	(147, 'pdf_format_contract', 'A4-PORTRAIT', 1),
	(148, 'swap_pdf_info', '0', 1),
	(149, 'exclude_invoice_from_client_area_with_draft_status', '1', 1),
	(150, 'cron_has_run_from_cli', '1', 1),
	(151, 'hide_cron_is_required_message', '0', 0),
	(152, 'auto_assign_customer_admin_after_lead_convert', '1', 1),
	(153, 'show_transactions_on_invoice_pdf', '1', 1),
	(154, 'show_pay_link_to_invoice_pdf', '1', 1),
	(155, 'tasks_kanban_limit', '50', 1),
	(156, 'purchase_key', '', 1),
	(157, 'estimates_pipeline_limit', '50', 1),
	(158, 'proposals_pipeline_limit', '50', 1),
	(159, 'proposal_number_prefix', 'PRO-', 1),
	(160, 'number_padding_prefixes', '6', 1),
	(161, 'show_page_number_on_pdf', '0', 1),
	(162, 'calendar_events_limit', '4', 1),
	(163, 'show_setup_menu_item_only_on_hover', '0', 1),
	(164, 'company_requires_vat_number_field', '1', 1),
	(165, 'company_is_required', '1', 1),
	(166, 'allow_contact_to_delete_files', '0', 1),
	(167, 'company_vat', '', 1),
	(168, 'di', '1613129487', 1),
	(169, 'invoice_auto_operations_hour', '21', 1),
	(170, 'use_minified_files', '1', 1),
	(171, 'only_own_files_contacts', '0', 1),
	(172, 'allow_primary_contact_to_view_edit_billing_and_shipping', '0', 1),
	(173, 'estimate_due_after', '7', 1),
	(174, 'staff_members_open_tickets_to_all_contacts', '1', 1),
	(175, 'time_format', '24', 1),
	(176, 'delete_activity_log_older_then', '1', 1),
	(177, 'disable_language', '0', 1),
	(178, 'company_state', '', 1),
	(179, 'email_header', '', 1),
	(180, 'show_pdf_signature_invoice', '1', 0),
	(181, 'show_pdf_signature_estimate', '1', 0),
	(182, 'signature_image', '', 0),
	(183, 'scroll_responsive_tables', '0', 1),
	(184, 'email_footer', '', 1),
	(185, 'exclude_proposal_from_client_area_with_draft_status', '1', 1),
	(186, 'pusher_app_key', '', 1),
	(187, 'pusher_app_secret', '', 1),
	(188, 'pusher_app_id', '', 1),
	(189, 'pusher_realtime_notifications', '0', 1),
	(190, 'pdf_format_statement', 'A4-PORTRAIT', 1),
	(191, 'pusher_cluster', '', 1),
	(192, 'show_table_export_button', 'to_all', 1),
	(193, 'allow_staff_view_proposals_assigned', '1', 1),
	(194, 'show_cloudflare_notice', '1', 0),
	(195, 'task_modal_class', 'modal-lg', 1),
	(196, 'lead_modal_class', 'modal-lg', 1),
	(197, 'show_timesheets_overview_all_members_notice_admins', '1', 0),
	(198, 'desktop_notifications', '0', 1),
	(199, 'hide_notified_reminders_from_calendar', '1', 0),
	(200, 'customer_info_format', '{company_name}<br />\r\r\n{street}<br />\r\r\n{city} {state}<br />\r\r\n{country_code} {zip_code}<br />\r\r\n{vat_number_with_label}', 0),
	(201, 'timer_started_change_status_in_progress', '1', 0),
	(202, 'default_ticket_reply_status', '3', 1),
	(203, 'default_task_status', 'auto', 1),
	(204, 'email_queue_skip_with_attachments', '1', 1),
	(205, 'email_queue_enabled', '0', 1),
	(206, 'last_email_queue_retry', '1616074771', 1),
	(207, 'auto_dismiss_desktop_notifications_after', '0', 1),
	(208, 'proposal_info_format', '{proposal_to}<br />\r\r\n{address}<br />\r\r\n{city} {state}<br />\r\r\n{country_code} {zip_code}<br />\r\r\n{phone}<br />\r\r\n{email}', 0),
	(209, 'ticket_replies_order', 'asc', 1),
	(210, 'new_recurring_invoice_action', 'generate_and_send', 0),
	(211, 'bcc_emails', '', 0),
	(212, 'email_templates_language_checks', 'a:2231:{s:25:"new-client-created-slovak";i:1;s:29:"invoice-send-to-client-slovak";i:1;s:30:"new-ticket-opened-admin-slovak";i:1;s:19:"ticket-reply-slovak";i:1;s:26:"ticket-autoresponse-slovak";i:1;s:31:"invoice-payment-recorded-slovak";i:1;s:29:"invoice-overdue-notice-slovak";i:1;s:27:"invoice-already-send-slovak";i:1;s:31:"new-ticket-created-staff-slovak";i:1;s:30:"estimate-send-to-client-slovak";i:1;s:28:"ticket-reply-to-admin-slovak";i:1;s:28:"estimate-already-send-slovak";i:1;s:26:"contract-expiration-slovak";i:1;s:20:"task-assigned-slovak";i:1;s:29:"task-added-as-follower-slovak";i:1;s:21:"task-commented-slovak";i:1;s:28:"task-added-attachment-slovak";i:1;s:33:"estimate-declined-to-staff-slovak";i:1;s:33:"estimate-accepted-to-staff-slovak";i:1;s:31:"proposal-client-accepted-slovak";i:1;s:32:"proposal-send-to-customer-slovak";i:1;s:31:"proposal-client-declined-slovak";i:1;s:32:"proposal-client-thank-you-slovak";i:1;s:33:"proposal-comment-to-client-slovak";i:1;s:32:"proposal-comment-to-admin-slovak";i:1;s:37:"estimate-thank-you-to-customer-slovak";i:1;s:33:"task-deadline-notification-slovak";i:1;s:20:"send-contract-slovak";i:1;s:40:"invoice-payment-recorded-to-staff-slovak";i:1;s:24:"auto-close-ticket-slovak";i:1;s:46:"new-project-discussion-created-to-staff-slovak";i:1;s:49:"new-project-discussion-created-to-customer-slovak";i:1;s:44:"new-project-file-uploaded-to-customer-slovak";i:1;s:41:"new-project-file-uploaded-to-staff-slovak";i:1;s:49:"new-project-discussion-comment-to-customer-slovak";i:1;s:46:"new-project-discussion-comment-to-staff-slovak";i:1;s:36:"staff-added-as-project-member-slovak";i:1;s:31:"estimate-expiry-reminder-slovak";i:1;s:31:"proposal-expiry-reminder-slovak";i:1;s:24:"new-staff-created-slovak";i:1;s:30:"contact-forgot-password-slovak";i:1;s:31:"contact-password-reseted-slovak";i:1;s:27:"contact-set-password-slovak";i:1;s:28:"staff-forgot-password-slovak";i:1;s:29:"staff-password-reseted-slovak";i:1;s:26:"assigned-to-project-slovak";i:1;s:40:"task-added-attachment-to-contacts-slovak";i:1;s:33:"task-commented-to-contacts-slovak";i:1;s:24:"new-lead-assigned-slovak";i:1;s:23:"client-statement-slovak";i:1;s:31:"ticket-assigned-to-admin-slovak";i:1;s:37:"new-client-registered-to-admin-slovak";i:1;s:37:"new-web-to-lead-form-submitted-slovak";i:1;s:32:"two-factor-authentication-slovak";i:1;s:35:"project-finished-to-customer-slovak";i:1;s:33:"credit-note-send-to-client-slovak";i:1;s:34:"task-status-change-to-staff-slovak";i:1;s:37:"task-status-change-to-contacts-slovak";i:1;s:27:"reminder-email-staff-slovak";i:1;s:33:"contract-comment-to-client-slovak";i:1;s:32:"contract-comment-to-admin-slovak";i:1;s:24:"send-subscription-slovak";i:1;s:34:"subscription-payment-failed-slovak";i:1;s:28:"subscription-canceled-slovak";i:1;s:37:"subscription-payment-succeeded-slovak";i:1;s:35:"contract-expiration-to-staff-slovak";i:1;s:27:"gdpr-removal-request-slovak";i:1;s:32:"gdpr-removal-request-lead-slovak";i:1;s:36:"client-registration-confirmed-slovak";i:1;s:31:"contract-signed-to-staff-slovak";i:1;s:35:"customer-subscribed-to-staff-slovak";i:1;s:33:"contact-verification-email-slovak";i:1;s:50:"new-customer-profile-file-uploaded-to-staff-slovak";i:1;s:34:"event-notification-to-staff-slovak";i:1;s:43:"subscription-payment-requires-action-slovak";i:1;s:24:"new-client-created-czech";i:1;s:28:"invoice-send-to-client-czech";i:1;s:29:"new-ticket-opened-admin-czech";i:1;s:18:"ticket-reply-czech";i:1;s:25:"ticket-autoresponse-czech";i:1;s:30:"invoice-payment-recorded-czech";i:1;s:28:"invoice-overdue-notice-czech";i:1;s:26:"invoice-already-send-czech";i:1;s:30:"new-ticket-created-staff-czech";i:1;s:29:"estimate-send-to-client-czech";i:1;s:27:"ticket-reply-to-admin-czech";i:1;s:27:"estimate-already-send-czech";i:1;s:25:"contract-expiration-czech";i:1;s:19:"task-assigned-czech";i:1;s:28:"task-added-as-follower-czech";i:1;s:20:"task-commented-czech";i:1;s:27:"task-added-attachment-czech";i:1;s:32:"estimate-declined-to-staff-czech";i:1;s:32:"estimate-accepted-to-staff-czech";i:1;s:30:"proposal-client-accepted-czech";i:1;s:31:"proposal-send-to-customer-czech";i:1;s:30:"proposal-client-declined-czech";i:1;s:31:"proposal-client-thank-you-czech";i:1;s:32:"proposal-comment-to-client-czech";i:1;s:31:"proposal-comment-to-admin-czech";i:1;s:36:"estimate-thank-you-to-customer-czech";i:1;s:32:"task-deadline-notification-czech";i:1;s:19:"send-contract-czech";i:1;s:39:"invoice-payment-recorded-to-staff-czech";i:1;s:23:"auto-close-ticket-czech";i:1;s:45:"new-project-discussion-created-to-staff-czech";i:1;s:48:"new-project-discussion-created-to-customer-czech";i:1;s:43:"new-project-file-uploaded-to-customer-czech";i:1;s:40:"new-project-file-uploaded-to-staff-czech";i:1;s:48:"new-project-discussion-comment-to-customer-czech";i:1;s:45:"new-project-discussion-comment-to-staff-czech";i:1;s:35:"staff-added-as-project-member-czech";i:1;s:30:"estimate-expiry-reminder-czech";i:1;s:30:"proposal-expiry-reminder-czech";i:1;s:23:"new-staff-created-czech";i:1;s:29:"contact-forgot-password-czech";i:1;s:30:"contact-password-reseted-czech";i:1;s:26:"contact-set-password-czech";i:1;s:27:"staff-forgot-password-czech";i:1;s:28:"staff-password-reseted-czech";i:1;s:25:"assigned-to-project-czech";i:1;s:39:"task-added-attachment-to-contacts-czech";i:1;s:32:"task-commented-to-contacts-czech";i:1;s:23:"new-lead-assigned-czech";i:1;s:22:"client-statement-czech";i:1;s:30:"ticket-assigned-to-admin-czech";i:1;s:36:"new-client-registered-to-admin-czech";i:1;s:36:"new-web-to-lead-form-submitted-czech";i:1;s:31:"two-factor-authentication-czech";i:1;s:34:"project-finished-to-customer-czech";i:1;s:32:"credit-note-send-to-client-czech";i:1;s:33:"task-status-change-to-staff-czech";i:1;s:36:"task-status-change-to-contacts-czech";i:1;s:26:"reminder-email-staff-czech";i:1;s:32:"contract-comment-to-client-czech";i:1;s:31:"contract-comment-to-admin-czech";i:1;s:23:"send-subscription-czech";i:1;s:33:"subscription-payment-failed-czech";i:1;s:27:"subscription-canceled-czech";i:1;s:36:"subscription-payment-succeeded-czech";i:1;s:34:"contract-expiration-to-staff-czech";i:1;s:26:"gdpr-removal-request-czech";i:1;s:31:"gdpr-removal-request-lead-czech";i:1;s:35:"client-registration-confirmed-czech";i:1;s:30:"contract-signed-to-staff-czech";i:1;s:34:"customer-subscribed-to-staff-czech";i:1;s:32:"contact-verification-email-czech";i:1;s:49:"new-customer-profile-file-uploaded-to-staff-czech";i:1;s:33:"event-notification-to-staff-czech";i:1;s:42:"subscription-payment-requires-action-czech";i:1;s:27:"new-client-created-romanian";i:1;s:31:"invoice-send-to-client-romanian";i:1;s:32:"new-ticket-opened-admin-romanian";i:1;s:21:"ticket-reply-romanian";i:1;s:28:"ticket-autoresponse-romanian";i:1;s:33:"invoice-payment-recorded-romanian";i:1;s:31:"invoice-overdue-notice-romanian";i:1;s:29:"invoice-already-send-romanian";i:1;s:33:"new-ticket-created-staff-romanian";i:1;s:32:"estimate-send-to-client-romanian";i:1;s:30:"ticket-reply-to-admin-romanian";i:1;s:30:"estimate-already-send-romanian";i:1;s:28:"contract-expiration-romanian";i:1;s:22:"task-assigned-romanian";i:1;s:31:"task-added-as-follower-romanian";i:1;s:23:"task-commented-romanian";i:1;s:30:"task-added-attachment-romanian";i:1;s:35:"estimate-declined-to-staff-romanian";i:1;s:35:"estimate-accepted-to-staff-romanian";i:1;s:33:"proposal-client-accepted-romanian";i:1;s:34:"proposal-send-to-customer-romanian";i:1;s:33:"proposal-client-declined-romanian";i:1;s:34:"proposal-client-thank-you-romanian";i:1;s:35:"proposal-comment-to-client-romanian";i:1;s:34:"proposal-comment-to-admin-romanian";i:1;s:39:"estimate-thank-you-to-customer-romanian";i:1;s:35:"task-deadline-notification-romanian";i:1;s:22:"send-contract-romanian";i:1;s:42:"invoice-payment-recorded-to-staff-romanian";i:1;s:26:"auto-close-ticket-romanian";i:1;s:48:"new-project-discussion-created-to-staff-romanian";i:1;s:51:"new-project-discussion-created-to-customer-romanian";i:1;s:46:"new-project-file-uploaded-to-customer-romanian";i:1;s:43:"new-project-file-uploaded-to-staff-romanian";i:1;s:51:"new-project-discussion-comment-to-customer-romanian";i:1;s:48:"new-project-discussion-comment-to-staff-romanian";i:1;s:38:"staff-added-as-project-member-romanian";i:1;s:33:"estimate-expiry-reminder-romanian";i:1;s:33:"proposal-expiry-reminder-romanian";i:1;s:26:"new-staff-created-romanian";i:1;s:32:"contact-forgot-password-romanian";i:1;s:33:"contact-password-reseted-romanian";i:1;s:29:"contact-set-password-romanian";i:1;s:30:"staff-forgot-password-romanian";i:1;s:31:"staff-password-reseted-romanian";i:1;s:28:"assigned-to-project-romanian";i:1;s:42:"task-added-attachment-to-contacts-romanian";i:1;s:35:"task-commented-to-contacts-romanian";i:1;s:26:"new-lead-assigned-romanian";i:1;s:25:"client-statement-romanian";i:1;s:33:"ticket-assigned-to-admin-romanian";i:1;s:39:"new-client-registered-to-admin-romanian";i:1;s:39:"new-web-to-lead-form-submitted-romanian";i:1;s:34:"two-factor-authentication-romanian";i:1;s:37:"project-finished-to-customer-romanian";i:1;s:35:"credit-note-send-to-client-romanian";i:1;s:36:"task-status-change-to-staff-romanian";i:1;s:39:"task-status-change-to-contacts-romanian";i:1;s:29:"reminder-email-staff-romanian";i:1;s:35:"contract-comment-to-client-romanian";i:1;s:34:"contract-comment-to-admin-romanian";i:1;s:26:"send-subscription-romanian";i:1;s:36:"subscription-payment-failed-romanian";i:1;s:30:"subscription-canceled-romanian";i:1;s:39:"subscription-payment-succeeded-romanian";i:1;s:37:"contract-expiration-to-staff-romanian";i:1;s:29:"gdpr-removal-request-romanian";i:1;s:34:"gdpr-removal-request-lead-romanian";i:1;s:38:"client-registration-confirmed-romanian";i:1;s:33:"contract-signed-to-staff-romanian";i:1;s:37:"customer-subscribed-to-staff-romanian";i:1;s:35:"contact-verification-email-romanian";i:1;s:52:"new-customer-profile-file-uploaded-to-staff-romanian";i:1;s:36:"event-notification-to-staff-romanian";i:1;s:45:"subscription-payment-requires-action-romanian";i:1;s:27:"new-client-created-japanese";i:1;s:31:"invoice-send-to-client-japanese";i:1;s:32:"new-ticket-opened-admin-japanese";i:1;s:21:"ticket-reply-japanese";i:1;s:28:"ticket-autoresponse-japanese";i:1;s:33:"invoice-payment-recorded-japanese";i:1;s:31:"invoice-overdue-notice-japanese";i:1;s:29:"invoice-already-send-japanese";i:1;s:33:"new-ticket-created-staff-japanese";i:1;s:32:"estimate-send-to-client-japanese";i:1;s:30:"ticket-reply-to-admin-japanese";i:1;s:30:"estimate-already-send-japanese";i:1;s:28:"contract-expiration-japanese";i:1;s:22:"task-assigned-japanese";i:1;s:31:"task-added-as-follower-japanese";i:1;s:23:"task-commented-japanese";i:1;s:30:"task-added-attachment-japanese";i:1;s:35:"estimate-declined-to-staff-japanese";i:1;s:35:"estimate-accepted-to-staff-japanese";i:1;s:33:"proposal-client-accepted-japanese";i:1;s:34:"proposal-send-to-customer-japanese";i:1;s:33:"proposal-client-declined-japanese";i:1;s:34:"proposal-client-thank-you-japanese";i:1;s:35:"proposal-comment-to-client-japanese";i:1;s:34:"proposal-comment-to-admin-japanese";i:1;s:39:"estimate-thank-you-to-customer-japanese";i:1;s:35:"task-deadline-notification-japanese";i:1;s:22:"send-contract-japanese";i:1;s:42:"invoice-payment-recorded-to-staff-japanese";i:1;s:26:"auto-close-ticket-japanese";i:1;s:48:"new-project-discussion-created-to-staff-japanese";i:1;s:51:"new-project-discussion-created-to-customer-japanese";i:1;s:46:"new-project-file-uploaded-to-customer-japanese";i:1;s:43:"new-project-file-uploaded-to-staff-japanese";i:1;s:51:"new-project-discussion-comment-to-customer-japanese";i:1;s:48:"new-project-discussion-comment-to-staff-japanese";i:1;s:38:"staff-added-as-project-member-japanese";i:1;s:33:"estimate-expiry-reminder-japanese";i:1;s:33:"proposal-expiry-reminder-japanese";i:1;s:26:"new-staff-created-japanese";i:1;s:32:"contact-forgot-password-japanese";i:1;s:33:"contact-password-reseted-japanese";i:1;s:29:"contact-set-password-japanese";i:1;s:30:"staff-forgot-password-japanese";i:1;s:31:"staff-password-reseted-japanese";i:1;s:28:"assigned-to-project-japanese";i:1;s:42:"task-added-attachment-to-contacts-japanese";i:1;s:35:"task-commented-to-contacts-japanese";i:1;s:26:"new-lead-assigned-japanese";i:1;s:25:"client-statement-japanese";i:1;s:33:"ticket-assigned-to-admin-japanese";i:1;s:39:"new-client-registered-to-admin-japanese";i:1;s:39:"new-web-to-lead-form-submitted-japanese";i:1;s:34:"two-factor-authentication-japanese";i:1;s:37:"project-finished-to-customer-japanese";i:1;s:35:"credit-note-send-to-client-japanese";i:1;s:36:"task-status-change-to-staff-japanese";i:1;s:39:"task-status-change-to-contacts-japanese";i:1;s:29:"reminder-email-staff-japanese";i:1;s:35:"contract-comment-to-client-japanese";i:1;s:34:"contract-comment-to-admin-japanese";i:1;s:26:"send-subscription-japanese";i:1;s:36:"subscription-payment-failed-japanese";i:1;s:30:"subscription-canceled-japanese";i:1;s:39:"subscription-payment-succeeded-japanese";i:1;s:37:"contract-expiration-to-staff-japanese";i:1;s:29:"gdpr-removal-request-japanese";i:1;s:34:"gdpr-removal-request-lead-japanese";i:1;s:38:"client-registration-confirmed-japanese";i:1;s:33:"contract-signed-to-staff-japanese";i:1;s:37:"customer-subscribed-to-staff-japanese";i:1;s:35:"contact-verification-email-japanese";i:1;s:52:"new-customer-profile-file-uploaded-to-staff-japanese";i:1;s:36:"event-notification-to-staff-japanese";i:1;s:45:"subscription-payment-requires-action-japanese";i:1;s:28:"new-client-created-indonesia";i:1;s:32:"invoice-send-to-client-indonesia";i:1;s:33:"new-ticket-opened-admin-indonesia";i:1;s:22:"ticket-reply-indonesia";i:1;s:29:"ticket-autoresponse-indonesia";i:1;s:34:"invoice-payment-recorded-indonesia";i:1;s:32:"invoice-overdue-notice-indonesia";i:1;s:30:"invoice-already-send-indonesia";i:1;s:34:"new-ticket-created-staff-indonesia";i:1;s:33:"estimate-send-to-client-indonesia";i:1;s:31:"ticket-reply-to-admin-indonesia";i:1;s:31:"estimate-already-send-indonesia";i:1;s:29:"contract-expiration-indonesia";i:1;s:23:"task-assigned-indonesia";i:1;s:32:"task-added-as-follower-indonesia";i:1;s:24:"task-commented-indonesia";i:1;s:31:"task-added-attachment-indonesia";i:1;s:36:"estimate-declined-to-staff-indonesia";i:1;s:36:"estimate-accepted-to-staff-indonesia";i:1;s:34:"proposal-client-accepted-indonesia";i:1;s:35:"proposal-send-to-customer-indonesia";i:1;s:34:"proposal-client-declined-indonesia";i:1;s:35:"proposal-client-thank-you-indonesia";i:1;s:36:"proposal-comment-to-client-indonesia";i:1;s:35:"proposal-comment-to-admin-indonesia";i:1;s:40:"estimate-thank-you-to-customer-indonesia";i:1;s:36:"task-deadline-notification-indonesia";i:1;s:23:"send-contract-indonesia";i:1;s:43:"invoice-payment-recorded-to-staff-indonesia";i:1;s:27:"auto-close-ticket-indonesia";i:1;s:49:"new-project-discussion-created-to-staff-indonesia";i:1;s:52:"new-project-discussion-created-to-customer-indonesia";i:1;s:47:"new-project-file-uploaded-to-customer-indonesia";i:1;s:44:"new-project-file-uploaded-to-staff-indonesia";i:1;s:52:"new-project-discussion-comment-to-customer-indonesia";i:1;s:49:"new-project-discussion-comment-to-staff-indonesia";i:1;s:39:"staff-added-as-project-member-indonesia";i:1;s:34:"estimate-expiry-reminder-indonesia";i:1;s:34:"proposal-expiry-reminder-indonesia";i:1;s:27:"new-staff-created-indonesia";i:1;s:33:"contact-forgot-password-indonesia";i:1;s:34:"contact-password-reseted-indonesia";i:1;s:30:"contact-set-password-indonesia";i:1;s:31:"staff-forgot-password-indonesia";i:1;s:32:"staff-password-reseted-indonesia";i:1;s:29:"assigned-to-project-indonesia";i:1;s:43:"task-added-attachment-to-contacts-indonesia";i:1;s:36:"task-commented-to-contacts-indonesia";i:1;s:27:"new-lead-assigned-indonesia";i:1;s:26:"client-statement-indonesia";i:1;s:34:"ticket-assigned-to-admin-indonesia";i:1;s:40:"new-client-registered-to-admin-indonesia";i:1;s:40:"new-web-to-lead-form-submitted-indonesia";i:1;s:35:"two-factor-authentication-indonesia";i:1;s:38:"project-finished-to-customer-indonesia";i:1;s:36:"credit-note-send-to-client-indonesia";i:1;s:37:"task-status-change-to-staff-indonesia";i:1;s:40:"task-status-change-to-contacts-indonesia";i:1;s:30:"reminder-email-staff-indonesia";i:1;s:36:"contract-comment-to-client-indonesia";i:1;s:35:"contract-comment-to-admin-indonesia";i:1;s:27:"send-subscription-indonesia";i:1;s:37:"subscription-payment-failed-indonesia";i:1;s:31:"subscription-canceled-indonesia";i:1;s:40:"subscription-payment-succeeded-indonesia";i:1;s:38:"contract-expiration-to-staff-indonesia";i:1;s:30:"gdpr-removal-request-indonesia";i:1;s:35:"gdpr-removal-request-lead-indonesia";i:1;s:39:"client-registration-confirmed-indonesia";i:1;s:34:"contract-signed-to-staff-indonesia";i:1;s:38:"customer-subscribed-to-staff-indonesia";i:1;s:36:"contact-verification-email-indonesia";i:1;s:53:"new-customer-profile-file-uploaded-to-staff-indonesia";i:1;s:37:"event-notification-to-staff-indonesia";i:1;s:46:"subscription-payment-requires-action-indonesia";i:1;s:28:"new-client-created-bulgarian";i:1;s:32:"invoice-send-to-client-bulgarian";i:1;s:33:"new-ticket-opened-admin-bulgarian";i:1;s:22:"ticket-reply-bulgarian";i:1;s:29:"ticket-autoresponse-bulgarian";i:1;s:34:"invoice-payment-recorded-bulgarian";i:1;s:32:"invoice-overdue-notice-bulgarian";i:1;s:30:"invoice-already-send-bulgarian";i:1;s:34:"new-ticket-created-staff-bulgarian";i:1;s:33:"estimate-send-to-client-bulgarian";i:1;s:31:"ticket-reply-to-admin-bulgarian";i:1;s:31:"estimate-already-send-bulgarian";i:1;s:29:"contract-expiration-bulgarian";i:1;s:23:"task-assigned-bulgarian";i:1;s:32:"task-added-as-follower-bulgarian";i:1;s:24:"task-commented-bulgarian";i:1;s:31:"task-added-attachment-bulgarian";i:1;s:36:"estimate-declined-to-staff-bulgarian";i:1;s:36:"estimate-accepted-to-staff-bulgarian";i:1;s:34:"proposal-client-accepted-bulgarian";i:1;s:35:"proposal-send-to-customer-bulgarian";i:1;s:34:"proposal-client-declined-bulgarian";i:1;s:35:"proposal-client-thank-you-bulgarian";i:1;s:36:"proposal-comment-to-client-bulgarian";i:1;s:35:"proposal-comment-to-admin-bulgarian";i:1;s:40:"estimate-thank-you-to-customer-bulgarian";i:1;s:36:"task-deadline-notification-bulgarian";i:1;s:23:"send-contract-bulgarian";i:1;s:43:"invoice-payment-recorded-to-staff-bulgarian";i:1;s:27:"auto-close-ticket-bulgarian";i:1;s:49:"new-project-discussion-created-to-staff-bulgarian";i:1;s:52:"new-project-discussion-created-to-customer-bulgarian";i:1;s:47:"new-project-file-uploaded-to-customer-bulgarian";i:1;s:44:"new-project-file-uploaded-to-staff-bulgarian";i:1;s:52:"new-project-discussion-comment-to-customer-bulgarian";i:1;s:49:"new-project-discussion-comment-to-staff-bulgarian";i:1;s:39:"staff-added-as-project-member-bulgarian";i:1;s:34:"estimate-expiry-reminder-bulgarian";i:1;s:34:"proposal-expiry-reminder-bulgarian";i:1;s:27:"new-staff-created-bulgarian";i:1;s:33:"contact-forgot-password-bulgarian";i:1;s:34:"contact-password-reseted-bulgarian";i:1;s:30:"contact-set-password-bulgarian";i:1;s:31:"staff-forgot-password-bulgarian";i:1;s:32:"staff-password-reseted-bulgarian";i:1;s:29:"assigned-to-project-bulgarian";i:1;s:43:"task-added-attachment-to-contacts-bulgarian";i:1;s:36:"task-commented-to-contacts-bulgarian";i:1;s:27:"new-lead-assigned-bulgarian";i:1;s:26:"client-statement-bulgarian";i:1;s:34:"ticket-assigned-to-admin-bulgarian";i:1;s:40:"new-client-registered-to-admin-bulgarian";i:1;s:40:"new-web-to-lead-form-submitted-bulgarian";i:1;s:35:"two-factor-authentication-bulgarian";i:1;s:38:"project-finished-to-customer-bulgarian";i:1;s:36:"credit-note-send-to-client-bulgarian";i:1;s:37:"task-status-change-to-staff-bulgarian";i:1;s:40:"task-status-change-to-contacts-bulgarian";i:1;s:30:"reminder-email-staff-bulgarian";i:1;s:36:"contract-comment-to-client-bulgarian";i:1;s:35:"contract-comment-to-admin-bulgarian";i:1;s:27:"send-subscription-bulgarian";i:1;s:37:"subscription-payment-failed-bulgarian";i:1;s:31:"subscription-canceled-bulgarian";i:1;s:40:"subscription-payment-succeeded-bulgarian";i:1;s:38:"contract-expiration-to-staff-bulgarian";i:1;s:30:"gdpr-removal-request-bulgarian";i:1;s:35:"gdpr-removal-request-lead-bulgarian";i:1;s:39:"client-registration-confirmed-bulgarian";i:1;s:34:"contract-signed-to-staff-bulgarian";i:1;s:38:"customer-subscribed-to-staff-bulgarian";i:1;s:36:"contact-verification-email-bulgarian";i:1;s:53:"new-customer-profile-file-uploaded-to-staff-bulgarian";i:1;s:37:"event-notification-to-staff-bulgarian";i:1;s:46:"subscription-payment-requires-action-bulgarian";i:1;s:29:"new-client-created-vietnamese";i:1;s:33:"invoice-send-to-client-vietnamese";i:1;s:34:"new-ticket-opened-admin-vietnamese";i:1;s:23:"ticket-reply-vietnamese";i:1;s:30:"ticket-autoresponse-vietnamese";i:1;s:35:"invoice-payment-recorded-vietnamese";i:1;s:33:"invoice-overdue-notice-vietnamese";i:1;s:31:"invoice-already-send-vietnamese";i:1;s:35:"new-ticket-created-staff-vietnamese";i:1;s:34:"estimate-send-to-client-vietnamese";i:1;s:32:"ticket-reply-to-admin-vietnamese";i:1;s:32:"estimate-already-send-vietnamese";i:1;s:30:"contract-expiration-vietnamese";i:1;s:24:"task-assigned-vietnamese";i:1;s:33:"task-added-as-follower-vietnamese";i:1;s:25:"task-commented-vietnamese";i:1;s:32:"task-added-attachment-vietnamese";i:1;s:37:"estimate-declined-to-staff-vietnamese";i:1;s:37:"estimate-accepted-to-staff-vietnamese";i:1;s:35:"proposal-client-accepted-vietnamese";i:1;s:36:"proposal-send-to-customer-vietnamese";i:1;s:35:"proposal-client-declined-vietnamese";i:1;s:36:"proposal-client-thank-you-vietnamese";i:1;s:37:"proposal-comment-to-client-vietnamese";i:1;s:36:"proposal-comment-to-admin-vietnamese";i:1;s:41:"estimate-thank-you-to-customer-vietnamese";i:1;s:37:"task-deadline-notification-vietnamese";i:1;s:24:"send-contract-vietnamese";i:1;s:44:"invoice-payment-recorded-to-staff-vietnamese";i:1;s:28:"auto-close-ticket-vietnamese";i:1;s:50:"new-project-discussion-created-to-staff-vietnamese";i:1;s:53:"new-project-discussion-created-to-customer-vietnamese";i:1;s:48:"new-project-file-uploaded-to-customer-vietnamese";i:1;s:45:"new-project-file-uploaded-to-staff-vietnamese";i:1;s:53:"new-project-discussion-comment-to-customer-vietnamese";i:1;s:50:"new-project-discussion-comment-to-staff-vietnamese";i:1;s:40:"staff-added-as-project-member-vietnamese";i:1;s:35:"estimate-expiry-reminder-vietnamese";i:1;s:35:"proposal-expiry-reminder-vietnamese";i:1;s:28:"new-staff-created-vietnamese";i:1;s:34:"contact-forgot-password-vietnamese";i:1;s:35:"contact-password-reseted-vietnamese";i:1;s:31:"contact-set-password-vietnamese";i:1;s:32:"staff-forgot-password-vietnamese";i:1;s:33:"staff-password-reseted-vietnamese";i:1;s:30:"assigned-to-project-vietnamese";i:1;s:44:"task-added-attachment-to-contacts-vietnamese";i:1;s:37:"task-commented-to-contacts-vietnamese";i:1;s:28:"new-lead-assigned-vietnamese";i:1;s:27:"client-statement-vietnamese";i:1;s:35:"ticket-assigned-to-admin-vietnamese";i:1;s:41:"new-client-registered-to-admin-vietnamese";i:1;s:41:"new-web-to-lead-form-submitted-vietnamese";i:1;s:36:"two-factor-authentication-vietnamese";i:1;s:39:"project-finished-to-customer-vietnamese";i:1;s:37:"credit-note-send-to-client-vietnamese";i:1;s:38:"task-status-change-to-staff-vietnamese";i:1;s:41:"task-status-change-to-contacts-vietnamese";i:1;s:31:"reminder-email-staff-vietnamese";i:1;s:37:"contract-comment-to-client-vietnamese";i:1;s:36:"contract-comment-to-admin-vietnamese";i:1;s:28:"send-subscription-vietnamese";i:1;s:38:"subscription-payment-failed-vietnamese";i:1;s:32:"subscription-canceled-vietnamese";i:1;s:41:"subscription-payment-succeeded-vietnamese";i:1;s:39:"contract-expiration-to-staff-vietnamese";i:1;s:31:"gdpr-removal-request-vietnamese";i:1;s:36:"gdpr-removal-request-lead-vietnamese";i:1;s:40:"client-registration-confirmed-vietnamese";i:1;s:35:"contract-signed-to-staff-vietnamese";i:1;s:39:"customer-subscribed-to-staff-vietnamese";i:1;s:37:"contact-verification-email-vietnamese";i:1;s:54:"new-customer-profile-file-uploaded-to-staff-vietnamese";i:1;s:38:"event-notification-to-staff-vietnamese";i:1;s:47:"subscription-payment-requires-action-vietnamese";i:1;s:25:"new-client-created-german";i:1;s:29:"invoice-send-to-client-german";i:1;s:30:"new-ticket-opened-admin-german";i:1;s:19:"ticket-reply-german";i:1;s:26:"ticket-autoresponse-german";i:1;s:31:"invoice-payment-recorded-german";i:1;s:29:"invoice-overdue-notice-german";i:1;s:27:"invoice-already-send-german";i:1;s:31:"new-ticket-created-staff-german";i:1;s:30:"estimate-send-to-client-german";i:1;s:28:"ticket-reply-to-admin-german";i:1;s:28:"estimate-already-send-german";i:1;s:26:"contract-expiration-german";i:1;s:20:"task-assigned-german";i:1;s:29:"task-added-as-follower-german";i:1;s:21:"task-commented-german";i:1;s:28:"task-added-attachment-german";i:1;s:33:"estimate-declined-to-staff-german";i:1;s:33:"estimate-accepted-to-staff-german";i:1;s:31:"proposal-client-accepted-german";i:1;s:32:"proposal-send-to-customer-german";i:1;s:31:"proposal-client-declined-german";i:1;s:32:"proposal-client-thank-you-german";i:1;s:33:"proposal-comment-to-client-german";i:1;s:32:"proposal-comment-to-admin-german";i:1;s:37:"estimate-thank-you-to-customer-german";i:1;s:33:"task-deadline-notification-german";i:1;s:20:"send-contract-german";i:1;s:40:"invoice-payment-recorded-to-staff-german";i:1;s:24:"auto-close-ticket-german";i:1;s:46:"new-project-discussion-created-to-staff-german";i:1;s:49:"new-project-discussion-created-to-customer-german";i:1;s:44:"new-project-file-uploaded-to-customer-german";i:1;s:41:"new-project-file-uploaded-to-staff-german";i:1;s:49:"new-project-discussion-comment-to-customer-german";i:1;s:46:"new-project-discussion-comment-to-staff-german";i:1;s:36:"staff-added-as-project-member-german";i:1;s:31:"estimate-expiry-reminder-german";i:1;s:31:"proposal-expiry-reminder-german";i:1;s:24:"new-staff-created-german";i:1;s:30:"contact-forgot-password-german";i:1;s:31:"contact-password-reseted-german";i:1;s:27:"contact-set-password-german";i:1;s:28:"staff-forgot-password-german";i:1;s:29:"staff-password-reseted-german";i:1;s:26:"assigned-to-project-german";i:1;s:40:"task-added-attachment-to-contacts-german";i:1;s:33:"task-commented-to-contacts-german";i:1;s:24:"new-lead-assigned-german";i:1;s:23:"client-statement-german";i:1;s:31:"ticket-assigned-to-admin-german";i:1;s:37:"new-client-registered-to-admin-german";i:1;s:37:"new-web-to-lead-form-submitted-german";i:1;s:32:"two-factor-authentication-german";i:1;s:35:"project-finished-to-customer-german";i:1;s:33:"credit-note-send-to-client-german";i:1;s:34:"task-status-change-to-staff-german";i:1;s:37:"task-status-change-to-contacts-german";i:1;s:27:"reminder-email-staff-german";i:1;s:33:"contract-comment-to-client-german";i:1;s:32:"contract-comment-to-admin-german";i:1;s:24:"send-subscription-german";i:1;s:34:"subscription-payment-failed-german";i:1;s:28:"subscription-canceled-german";i:1;s:37:"subscription-payment-succeeded-german";i:1;s:35:"contract-expiration-to-staff-german";i:1;s:27:"gdpr-removal-request-german";i:1;s:32:"gdpr-removal-request-lead-german";i:1;s:36:"client-registration-confirmed-german";i:1;s:31:"contract-signed-to-staff-german";i:1;s:35:"customer-subscribed-to-staff-german";i:1;s:33:"contact-verification-email-german";i:1;s:50:"new-customer-profile-file-uploaded-to-staff-german";i:1;s:34:"event-notification-to-staff-german";i:1;s:43:"subscription-payment-requires-action-german";i:1;s:25:"new-client-created-french";i:1;s:29:"invoice-send-to-client-french";i:1;s:30:"new-ticket-opened-admin-french";i:1;s:19:"ticket-reply-french";i:1;s:26:"ticket-autoresponse-french";i:1;s:31:"invoice-payment-recorded-french";i:1;s:29:"invoice-overdue-notice-french";i:1;s:27:"invoice-already-send-french";i:1;s:31:"new-ticket-created-staff-french";i:1;s:30:"estimate-send-to-client-french";i:1;s:28:"ticket-reply-to-admin-french";i:1;s:28:"estimate-already-send-french";i:1;s:26:"contract-expiration-french";i:1;s:20:"task-assigned-french";i:1;s:29:"task-added-as-follower-french";i:1;s:21:"task-commented-french";i:1;s:28:"task-added-attachment-french";i:1;s:33:"estimate-declined-to-staff-french";i:1;s:33:"estimate-accepted-to-staff-french";i:1;s:31:"proposal-client-accepted-french";i:1;s:32:"proposal-send-to-customer-french";i:1;s:31:"proposal-client-declined-french";i:1;s:32:"proposal-client-thank-you-french";i:1;s:33:"proposal-comment-to-client-french";i:1;s:32:"proposal-comment-to-admin-french";i:1;s:37:"estimate-thank-you-to-customer-french";i:1;s:33:"task-deadline-notification-french";i:1;s:20:"send-contract-french";i:1;s:40:"invoice-payment-recorded-to-staff-french";i:1;s:24:"auto-close-ticket-french";i:1;s:46:"new-project-discussion-created-to-staff-french";i:1;s:49:"new-project-discussion-created-to-customer-french";i:1;s:44:"new-project-file-uploaded-to-customer-french";i:1;s:41:"new-project-file-uploaded-to-staff-french";i:1;s:49:"new-project-discussion-comment-to-customer-french";i:1;s:46:"new-project-discussion-comment-to-staff-french";i:1;s:36:"staff-added-as-project-member-french";i:1;s:31:"estimate-expiry-reminder-french";i:1;s:31:"proposal-expiry-reminder-french";i:1;s:24:"new-staff-created-french";i:1;s:30:"contact-forgot-password-french";i:1;s:31:"contact-password-reseted-french";i:1;s:27:"contact-set-password-french";i:1;s:28:"staff-forgot-password-french";i:1;s:29:"staff-password-reseted-french";i:1;s:26:"assigned-to-project-french";i:1;s:40:"task-added-attachment-to-contacts-french";i:1;s:33:"task-commented-to-contacts-french";i:1;s:24:"new-lead-assigned-french";i:1;s:23:"client-statement-french";i:1;s:31:"ticket-assigned-to-admin-french";i:1;s:37:"new-client-registered-to-admin-french";i:1;s:37:"new-web-to-lead-form-submitted-french";i:1;s:32:"two-factor-authentication-french";i:1;s:35:"project-finished-to-customer-french";i:1;s:33:"credit-note-send-to-client-french";i:1;s:34:"task-status-change-to-staff-french";i:1;s:37:"task-status-change-to-contacts-french";i:1;s:27:"reminder-email-staff-french";i:1;s:33:"contract-comment-to-client-french";i:1;s:32:"contract-comment-to-admin-french";i:1;s:24:"send-subscription-french";i:1;s:34:"subscription-payment-failed-french";i:1;s:28:"subscription-canceled-french";i:1;s:37:"subscription-payment-succeeded-french";i:1;s:35:"contract-expiration-to-staff-french";i:1;s:27:"gdpr-removal-request-french";i:1;s:32:"gdpr-removal-request-lead-french";i:1;s:36:"client-registration-confirmed-french";i:1;s:31:"contract-signed-to-staff-french";i:1;s:35:"customer-subscribed-to-staff-french";i:1;s:33:"contact-verification-email-french";i:1;s:50:"new-customer-profile-file-uploaded-to-staff-french";i:1;s:34:"event-notification-to-staff-french";i:1;s:43:"subscription-payment-requires-action-french";i:1;s:26:"new-client-created-italian";i:1;s:30:"invoice-send-to-client-italian";i:1;s:31:"new-ticket-opened-admin-italian";i:1;s:20:"ticket-reply-italian";i:1;s:27:"ticket-autoresponse-italian";i:1;s:32:"invoice-payment-recorded-italian";i:1;s:30:"invoice-overdue-notice-italian";i:1;s:28:"invoice-already-send-italian";i:1;s:32:"new-ticket-created-staff-italian";i:1;s:31:"estimate-send-to-client-italian";i:1;s:29:"ticket-reply-to-admin-italian";i:1;s:29:"estimate-already-send-italian";i:1;s:27:"contract-expiration-italian";i:1;s:21:"task-assigned-italian";i:1;s:30:"task-added-as-follower-italian";i:1;s:22:"task-commented-italian";i:1;s:29:"task-added-attachment-italian";i:1;s:34:"estimate-declined-to-staff-italian";i:1;s:34:"estimate-accepted-to-staff-italian";i:1;s:32:"proposal-client-accepted-italian";i:1;s:33:"proposal-send-to-customer-italian";i:1;s:32:"proposal-client-declined-italian";i:1;s:33:"proposal-client-thank-you-italian";i:1;s:34:"proposal-comment-to-client-italian";i:1;s:33:"proposal-comment-to-admin-italian";i:1;s:38:"estimate-thank-you-to-customer-italian";i:1;s:34:"task-deadline-notification-italian";i:1;s:21:"send-contract-italian";i:1;s:41:"invoice-payment-recorded-to-staff-italian";i:1;s:25:"auto-close-ticket-italian";i:1;s:47:"new-project-discussion-created-to-staff-italian";i:1;s:50:"new-project-discussion-created-to-customer-italian";i:1;s:45:"new-project-file-uploaded-to-customer-italian";i:1;s:42:"new-project-file-uploaded-to-staff-italian";i:1;s:50:"new-project-discussion-comment-to-customer-italian";i:1;s:47:"new-project-discussion-comment-to-staff-italian";i:1;s:37:"staff-added-as-project-member-italian";i:1;s:32:"estimate-expiry-reminder-italian";i:1;s:32:"proposal-expiry-reminder-italian";i:1;s:25:"new-staff-created-italian";i:1;s:31:"contact-forgot-password-italian";i:1;s:32:"contact-password-reseted-italian";i:1;s:28:"contact-set-password-italian";i:1;s:29:"staff-forgot-password-italian";i:1;s:30:"staff-password-reseted-italian";i:1;s:27:"assigned-to-project-italian";i:1;s:41:"task-added-attachment-to-contacts-italian";i:1;s:34:"task-commented-to-contacts-italian";i:1;s:25:"new-lead-assigned-italian";i:1;s:24:"client-statement-italian";i:1;s:32:"ticket-assigned-to-admin-italian";i:1;s:38:"new-client-registered-to-admin-italian";i:1;s:38:"new-web-to-lead-form-submitted-italian";i:1;s:33:"two-factor-authentication-italian";i:1;s:36:"project-finished-to-customer-italian";i:1;s:34:"credit-note-send-to-client-italian";i:1;s:35:"task-status-change-to-staff-italian";i:1;s:38:"task-status-change-to-contacts-italian";i:1;s:28:"reminder-email-staff-italian";i:1;s:34:"contract-comment-to-client-italian";i:1;s:33:"contract-comment-to-admin-italian";i:1;s:25:"send-subscription-italian";i:1;s:35:"subscription-payment-failed-italian";i:1;s:29:"subscription-canceled-italian";i:1;s:38:"subscription-payment-succeeded-italian";i:1;s:36:"contract-expiration-to-staff-italian";i:1;s:28:"gdpr-removal-request-italian";i:1;s:33:"gdpr-removal-request-lead-italian";i:1;s:37:"client-registration-confirmed-italian";i:1;s:32:"contract-signed-to-staff-italian";i:1;s:36:"customer-subscribed-to-staff-italian";i:1;s:34:"contact-verification-email-italian";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-italian";i:1;s:35:"event-notification-to-staff-italian";i:1;s:44:"subscription-payment-requires-action-italian";i:1;s:26:"new-client-created-spanish";i:1;s:30:"invoice-send-to-client-spanish";i:1;s:31:"new-ticket-opened-admin-spanish";i:1;s:20:"ticket-reply-spanish";i:1;s:27:"ticket-autoresponse-spanish";i:1;s:32:"invoice-payment-recorded-spanish";i:1;s:30:"invoice-overdue-notice-spanish";i:1;s:28:"invoice-already-send-spanish";i:1;s:32:"new-ticket-created-staff-spanish";i:1;s:31:"estimate-send-to-client-spanish";i:1;s:29:"ticket-reply-to-admin-spanish";i:1;s:29:"estimate-already-send-spanish";i:1;s:27:"contract-expiration-spanish";i:1;s:21:"task-assigned-spanish";i:1;s:30:"task-added-as-follower-spanish";i:1;s:22:"task-commented-spanish";i:1;s:29:"task-added-attachment-spanish";i:1;s:34:"estimate-declined-to-staff-spanish";i:1;s:34:"estimate-accepted-to-staff-spanish";i:1;s:32:"proposal-client-accepted-spanish";i:1;s:33:"proposal-send-to-customer-spanish";i:1;s:32:"proposal-client-declined-spanish";i:1;s:33:"proposal-client-thank-you-spanish";i:1;s:34:"proposal-comment-to-client-spanish";i:1;s:33:"proposal-comment-to-admin-spanish";i:1;s:38:"estimate-thank-you-to-customer-spanish";i:1;s:34:"task-deadline-notification-spanish";i:1;s:21:"send-contract-spanish";i:1;s:41:"invoice-payment-recorded-to-staff-spanish";i:1;s:25:"auto-close-ticket-spanish";i:1;s:47:"new-project-discussion-created-to-staff-spanish";i:1;s:50:"new-project-discussion-created-to-customer-spanish";i:1;s:45:"new-project-file-uploaded-to-customer-spanish";i:1;s:42:"new-project-file-uploaded-to-staff-spanish";i:1;s:50:"new-project-discussion-comment-to-customer-spanish";i:1;s:47:"new-project-discussion-comment-to-staff-spanish";i:1;s:37:"staff-added-as-project-member-spanish";i:1;s:32:"estimate-expiry-reminder-spanish";i:1;s:32:"proposal-expiry-reminder-spanish";i:1;s:25:"new-staff-created-spanish";i:1;s:31:"contact-forgot-password-spanish";i:1;s:32:"contact-password-reseted-spanish";i:1;s:28:"contact-set-password-spanish";i:1;s:29:"staff-forgot-password-spanish";i:1;s:30:"staff-password-reseted-spanish";i:1;s:27:"assigned-to-project-spanish";i:1;s:41:"task-added-attachment-to-contacts-spanish";i:1;s:34:"task-commented-to-contacts-spanish";i:1;s:25:"new-lead-assigned-spanish";i:1;s:24:"client-statement-spanish";i:1;s:32:"ticket-assigned-to-admin-spanish";i:1;s:38:"new-client-registered-to-admin-spanish";i:1;s:38:"new-web-to-lead-form-submitted-spanish";i:1;s:33:"two-factor-authentication-spanish";i:1;s:36:"project-finished-to-customer-spanish";i:1;s:34:"credit-note-send-to-client-spanish";i:1;s:35:"task-status-change-to-staff-spanish";i:1;s:38:"task-status-change-to-contacts-spanish";i:1;s:28:"reminder-email-staff-spanish";i:1;s:34:"contract-comment-to-client-spanish";i:1;s:33:"contract-comment-to-admin-spanish";i:1;s:25:"send-subscription-spanish";i:1;s:35:"subscription-payment-failed-spanish";i:1;s:29:"subscription-canceled-spanish";i:1;s:38:"subscription-payment-succeeded-spanish";i:1;s:36:"contract-expiration-to-staff-spanish";i:1;s:28:"gdpr-removal-request-spanish";i:1;s:33:"gdpr-removal-request-lead-spanish";i:1;s:37:"client-registration-confirmed-spanish";i:1;s:32:"contract-signed-to-staff-spanish";i:1;s:36:"customer-subscribed-to-staff-spanish";i:1;s:34:"contact-verification-email-spanish";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-spanish";i:1;s:35:"event-notification-to-staff-spanish";i:1;s:44:"subscription-payment-requires-action-spanish";i:1;s:26:"new-client-created-russian";i:1;s:30:"invoice-send-to-client-russian";i:1;s:31:"new-ticket-opened-admin-russian";i:1;s:20:"ticket-reply-russian";i:1;s:27:"ticket-autoresponse-russian";i:1;s:32:"invoice-payment-recorded-russian";i:1;s:30:"invoice-overdue-notice-russian";i:1;s:28:"invoice-already-send-russian";i:1;s:32:"new-ticket-created-staff-russian";i:1;s:31:"estimate-send-to-client-russian";i:1;s:29:"ticket-reply-to-admin-russian";i:1;s:29:"estimate-already-send-russian";i:1;s:27:"contract-expiration-russian";i:1;s:21:"task-assigned-russian";i:1;s:30:"task-added-as-follower-russian";i:1;s:22:"task-commented-russian";i:1;s:29:"task-added-attachment-russian";i:1;s:34:"estimate-declined-to-staff-russian";i:1;s:34:"estimate-accepted-to-staff-russian";i:1;s:32:"proposal-client-accepted-russian";i:1;s:33:"proposal-send-to-customer-russian";i:1;s:32:"proposal-client-declined-russian";i:1;s:33:"proposal-client-thank-you-russian";i:1;s:34:"proposal-comment-to-client-russian";i:1;s:33:"proposal-comment-to-admin-russian";i:1;s:38:"estimate-thank-you-to-customer-russian";i:1;s:34:"task-deadline-notification-russian";i:1;s:21:"send-contract-russian";i:1;s:41:"invoice-payment-recorded-to-staff-russian";i:1;s:25:"auto-close-ticket-russian";i:1;s:47:"new-project-discussion-created-to-staff-russian";i:1;s:50:"new-project-discussion-created-to-customer-russian";i:1;s:45:"new-project-file-uploaded-to-customer-russian";i:1;s:42:"new-project-file-uploaded-to-staff-russian";i:1;s:50:"new-project-discussion-comment-to-customer-russian";i:1;s:47:"new-project-discussion-comment-to-staff-russian";i:1;s:37:"staff-added-as-project-member-russian";i:1;s:32:"estimate-expiry-reminder-russian";i:1;s:32:"proposal-expiry-reminder-russian";i:1;s:25:"new-staff-created-russian";i:1;s:31:"contact-forgot-password-russian";i:1;s:32:"contact-password-reseted-russian";i:1;s:28:"contact-set-password-russian";i:1;s:29:"staff-forgot-password-russian";i:1;s:30:"staff-password-reseted-russian";i:1;s:27:"assigned-to-project-russian";i:1;s:41:"task-added-attachment-to-contacts-russian";i:1;s:34:"task-commented-to-contacts-russian";i:1;s:25:"new-lead-assigned-russian";i:1;s:24:"client-statement-russian";i:1;s:32:"ticket-assigned-to-admin-russian";i:1;s:38:"new-client-registered-to-admin-russian";i:1;s:38:"new-web-to-lead-form-submitted-russian";i:1;s:33:"two-factor-authentication-russian";i:1;s:36:"project-finished-to-customer-russian";i:1;s:34:"credit-note-send-to-client-russian";i:1;s:35:"task-status-change-to-staff-russian";i:1;s:38:"task-status-change-to-contacts-russian";i:1;s:28:"reminder-email-staff-russian";i:1;s:34:"contract-comment-to-client-russian";i:1;s:33:"contract-comment-to-admin-russian";i:1;s:25:"send-subscription-russian";i:1;s:35:"subscription-payment-failed-russian";i:1;s:29:"subscription-canceled-russian";i:1;s:38:"subscription-payment-succeeded-russian";i:1;s:36:"contract-expiration-to-staff-russian";i:1;s:28:"gdpr-removal-request-russian";i:1;s:33:"gdpr-removal-request-lead-russian";i:1;s:37:"client-registration-confirmed-russian";i:1;s:32:"contract-signed-to-staff-russian";i:1;s:36:"customer-subscribed-to-staff-russian";i:1;s:34:"contact-verification-email-russian";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-russian";i:1;s:35:"event-notification-to-staff-russian";i:1;s:44:"subscription-payment-requires-action-russian";i:1;s:26:"new-client-created-catalan";i:1;s:30:"invoice-send-to-client-catalan";i:1;s:31:"new-ticket-opened-admin-catalan";i:1;s:20:"ticket-reply-catalan";i:1;s:27:"ticket-autoresponse-catalan";i:1;s:32:"invoice-payment-recorded-catalan";i:1;s:30:"invoice-overdue-notice-catalan";i:1;s:28:"invoice-already-send-catalan";i:1;s:32:"new-ticket-created-staff-catalan";i:1;s:31:"estimate-send-to-client-catalan";i:1;s:29:"ticket-reply-to-admin-catalan";i:1;s:29:"estimate-already-send-catalan";i:1;s:27:"contract-expiration-catalan";i:1;s:21:"task-assigned-catalan";i:1;s:30:"task-added-as-follower-catalan";i:1;s:22:"task-commented-catalan";i:1;s:29:"task-added-attachment-catalan";i:1;s:34:"estimate-declined-to-staff-catalan";i:1;s:34:"estimate-accepted-to-staff-catalan";i:1;s:32:"proposal-client-accepted-catalan";i:1;s:33:"proposal-send-to-customer-catalan";i:1;s:32:"proposal-client-declined-catalan";i:1;s:33:"proposal-client-thank-you-catalan";i:1;s:34:"proposal-comment-to-client-catalan";i:1;s:33:"proposal-comment-to-admin-catalan";i:1;s:38:"estimate-thank-you-to-customer-catalan";i:1;s:34:"task-deadline-notification-catalan";i:1;s:21:"send-contract-catalan";i:1;s:41:"invoice-payment-recorded-to-staff-catalan";i:1;s:25:"auto-close-ticket-catalan";i:1;s:47:"new-project-discussion-created-to-staff-catalan";i:1;s:50:"new-project-discussion-created-to-customer-catalan";i:1;s:45:"new-project-file-uploaded-to-customer-catalan";i:1;s:42:"new-project-file-uploaded-to-staff-catalan";i:1;s:50:"new-project-discussion-comment-to-customer-catalan";i:1;s:47:"new-project-discussion-comment-to-staff-catalan";i:1;s:37:"staff-added-as-project-member-catalan";i:1;s:32:"estimate-expiry-reminder-catalan";i:1;s:32:"proposal-expiry-reminder-catalan";i:1;s:25:"new-staff-created-catalan";i:1;s:31:"contact-forgot-password-catalan";i:1;s:32:"contact-password-reseted-catalan";i:1;s:28:"contact-set-password-catalan";i:1;s:29:"staff-forgot-password-catalan";i:1;s:30:"staff-password-reseted-catalan";i:1;s:27:"assigned-to-project-catalan";i:1;s:41:"task-added-attachment-to-contacts-catalan";i:1;s:34:"task-commented-to-contacts-catalan";i:1;s:25:"new-lead-assigned-catalan";i:1;s:24:"client-statement-catalan";i:1;s:32:"ticket-assigned-to-admin-catalan";i:1;s:38:"new-client-registered-to-admin-catalan";i:1;s:38:"new-web-to-lead-form-submitted-catalan";i:1;s:33:"two-factor-authentication-catalan";i:1;s:36:"project-finished-to-customer-catalan";i:1;s:34:"credit-note-send-to-client-catalan";i:1;s:35:"task-status-change-to-staff-catalan";i:1;s:38:"task-status-change-to-contacts-catalan";i:1;s:28:"reminder-email-staff-catalan";i:1;s:34:"contract-comment-to-client-catalan";i:1;s:33:"contract-comment-to-admin-catalan";i:1;s:25:"send-subscription-catalan";i:1;s:35:"subscription-payment-failed-catalan";i:1;s:29:"subscription-canceled-catalan";i:1;s:38:"subscription-payment-succeeded-catalan";i:1;s:36:"contract-expiration-to-staff-catalan";i:1;s:28:"gdpr-removal-request-catalan";i:1;s:33:"gdpr-removal-request-lead-catalan";i:1;s:37:"client-registration-confirmed-catalan";i:1;s:32:"contract-signed-to-staff-catalan";i:1;s:36:"customer-subscribed-to-staff-catalan";i:1;s:34:"contact-verification-email-catalan";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-catalan";i:1;s:35:"event-notification-to-staff-catalan";i:1;s:44:"subscription-payment-requires-action-catalan";i:1;s:26:"new-client-created-persian";i:1;s:30:"invoice-send-to-client-persian";i:1;s:31:"new-ticket-opened-admin-persian";i:1;s:20:"ticket-reply-persian";i:1;s:27:"ticket-autoresponse-persian";i:1;s:32:"invoice-payment-recorded-persian";i:1;s:30:"invoice-overdue-notice-persian";i:1;s:28:"invoice-already-send-persian";i:1;s:32:"new-ticket-created-staff-persian";i:1;s:31:"estimate-send-to-client-persian";i:1;s:29:"ticket-reply-to-admin-persian";i:1;s:29:"estimate-already-send-persian";i:1;s:27:"contract-expiration-persian";i:1;s:21:"task-assigned-persian";i:1;s:30:"task-added-as-follower-persian";i:1;s:22:"task-commented-persian";i:1;s:29:"task-added-attachment-persian";i:1;s:34:"estimate-declined-to-staff-persian";i:1;s:34:"estimate-accepted-to-staff-persian";i:1;s:32:"proposal-client-accepted-persian";i:1;s:33:"proposal-send-to-customer-persian";i:1;s:32:"proposal-client-declined-persian";i:1;s:33:"proposal-client-thank-you-persian";i:1;s:34:"proposal-comment-to-client-persian";i:1;s:33:"proposal-comment-to-admin-persian";i:1;s:38:"estimate-thank-you-to-customer-persian";i:1;s:34:"task-deadline-notification-persian";i:1;s:21:"send-contract-persian";i:1;s:41:"invoice-payment-recorded-to-staff-persian";i:1;s:25:"auto-close-ticket-persian";i:1;s:47:"new-project-discussion-created-to-staff-persian";i:1;s:50:"new-project-discussion-created-to-customer-persian";i:1;s:45:"new-project-file-uploaded-to-customer-persian";i:1;s:42:"new-project-file-uploaded-to-staff-persian";i:1;s:50:"new-project-discussion-comment-to-customer-persian";i:1;s:47:"new-project-discussion-comment-to-staff-persian";i:1;s:37:"staff-added-as-project-member-persian";i:1;s:32:"estimate-expiry-reminder-persian";i:1;s:32:"proposal-expiry-reminder-persian";i:1;s:25:"new-staff-created-persian";i:1;s:31:"contact-forgot-password-persian";i:1;s:32:"contact-password-reseted-persian";i:1;s:28:"contact-set-password-persian";i:1;s:29:"staff-forgot-password-persian";i:1;s:30:"staff-password-reseted-persian";i:1;s:27:"assigned-to-project-persian";i:1;s:41:"task-added-attachment-to-contacts-persian";i:1;s:34:"task-commented-to-contacts-persian";i:1;s:25:"new-lead-assigned-persian";i:1;s:24:"client-statement-persian";i:1;s:32:"ticket-assigned-to-admin-persian";i:1;s:38:"new-client-registered-to-admin-persian";i:1;s:38:"new-web-to-lead-form-submitted-persian";i:1;s:33:"two-factor-authentication-persian";i:1;s:36:"project-finished-to-customer-persian";i:1;s:34:"credit-note-send-to-client-persian";i:1;s:35:"task-status-change-to-staff-persian";i:1;s:38:"task-status-change-to-contacts-persian";i:1;s:28:"reminder-email-staff-persian";i:1;s:34:"contract-comment-to-client-persian";i:1;s:33:"contract-comment-to-admin-persian";i:1;s:25:"send-subscription-persian";i:1;s:35:"subscription-payment-failed-persian";i:1;s:29:"subscription-canceled-persian";i:1;s:38:"subscription-payment-succeeded-persian";i:1;s:36:"contract-expiration-to-staff-persian";i:1;s:28:"gdpr-removal-request-persian";i:1;s:33:"gdpr-removal-request-lead-persian";i:1;s:37:"client-registration-confirmed-persian";i:1;s:32:"contract-signed-to-staff-persian";i:1;s:36:"customer-subscribed-to-staff-persian";i:1;s:34:"contact-verification-email-persian";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-persian";i:1;s:35:"event-notification-to-staff-persian";i:1;s:44:"subscription-payment-requires-action-persian";i:1;s:26:"new-client-created-turkish";i:1;s:30:"invoice-send-to-client-turkish";i:1;s:31:"new-ticket-opened-admin-turkish";i:1;s:20:"ticket-reply-turkish";i:1;s:27:"ticket-autoresponse-turkish";i:1;s:32:"invoice-payment-recorded-turkish";i:1;s:30:"invoice-overdue-notice-turkish";i:1;s:28:"invoice-already-send-turkish";i:1;s:32:"new-ticket-created-staff-turkish";i:1;s:31:"estimate-send-to-client-turkish";i:1;s:29:"ticket-reply-to-admin-turkish";i:1;s:29:"estimate-already-send-turkish";i:1;s:27:"contract-expiration-turkish";i:1;s:21:"task-assigned-turkish";i:1;s:30:"task-added-as-follower-turkish";i:1;s:22:"task-commented-turkish";i:1;s:29:"task-added-attachment-turkish";i:1;s:34:"estimate-declined-to-staff-turkish";i:1;s:34:"estimate-accepted-to-staff-turkish";i:1;s:32:"proposal-client-accepted-turkish";i:1;s:33:"proposal-send-to-customer-turkish";i:1;s:32:"proposal-client-declined-turkish";i:1;s:33:"proposal-client-thank-you-turkish";i:1;s:34:"proposal-comment-to-client-turkish";i:1;s:33:"proposal-comment-to-admin-turkish";i:1;s:38:"estimate-thank-you-to-customer-turkish";i:1;s:34:"task-deadline-notification-turkish";i:1;s:21:"send-contract-turkish";i:1;s:41:"invoice-payment-recorded-to-staff-turkish";i:1;s:25:"auto-close-ticket-turkish";i:1;s:47:"new-project-discussion-created-to-staff-turkish";i:1;s:50:"new-project-discussion-created-to-customer-turkish";i:1;s:45:"new-project-file-uploaded-to-customer-turkish";i:1;s:42:"new-project-file-uploaded-to-staff-turkish";i:1;s:50:"new-project-discussion-comment-to-customer-turkish";i:1;s:47:"new-project-discussion-comment-to-staff-turkish";i:1;s:37:"staff-added-as-project-member-turkish";i:1;s:32:"estimate-expiry-reminder-turkish";i:1;s:32:"proposal-expiry-reminder-turkish";i:1;s:25:"new-staff-created-turkish";i:1;s:31:"contact-forgot-password-turkish";i:1;s:32:"contact-password-reseted-turkish";i:1;s:28:"contact-set-password-turkish";i:1;s:29:"staff-forgot-password-turkish";i:1;s:30:"staff-password-reseted-turkish";i:1;s:27:"assigned-to-project-turkish";i:1;s:41:"task-added-attachment-to-contacts-turkish";i:1;s:34:"task-commented-to-contacts-turkish";i:1;s:25:"new-lead-assigned-turkish";i:1;s:24:"client-statement-turkish";i:1;s:32:"ticket-assigned-to-admin-turkish";i:1;s:38:"new-client-registered-to-admin-turkish";i:1;s:38:"new-web-to-lead-form-submitted-turkish";i:1;s:33:"two-factor-authentication-turkish";i:1;s:36:"project-finished-to-customer-turkish";i:1;s:34:"credit-note-send-to-client-turkish";i:1;s:35:"task-status-change-to-staff-turkish";i:1;s:38:"task-status-change-to-contacts-turkish";i:1;s:28:"reminder-email-staff-turkish";i:1;s:34:"contract-comment-to-client-turkish";i:1;s:33:"contract-comment-to-admin-turkish";i:1;s:25:"send-subscription-turkish";i:1;s:35:"subscription-payment-failed-turkish";i:1;s:29:"subscription-canceled-turkish";i:1;s:38:"subscription-payment-succeeded-turkish";i:1;s:36:"contract-expiration-to-staff-turkish";i:1;s:28:"gdpr-removal-request-turkish";i:1;s:33:"gdpr-removal-request-lead-turkish";i:1;s:37:"client-registration-confirmed-turkish";i:1;s:32:"contract-signed-to-staff-turkish";i:1;s:36:"customer-subscribed-to-staff-turkish";i:1;s:34:"contact-verification-email-turkish";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-turkish";i:1;s:35:"event-notification-to-staff-turkish";i:1;s:44:"subscription-payment-requires-action-turkish";i:1;s:24:"new-client-created-greek";i:1;s:28:"invoice-send-to-client-greek";i:1;s:29:"new-ticket-opened-admin-greek";i:1;s:18:"ticket-reply-greek";i:1;s:25:"ticket-autoresponse-greek";i:1;s:30:"invoice-payment-recorded-greek";i:1;s:28:"invoice-overdue-notice-greek";i:1;s:26:"invoice-already-send-greek";i:1;s:30:"new-ticket-created-staff-greek";i:1;s:29:"estimate-send-to-client-greek";i:1;s:27:"ticket-reply-to-admin-greek";i:1;s:27:"estimate-already-send-greek";i:1;s:25:"contract-expiration-greek";i:1;s:19:"task-assigned-greek";i:1;s:28:"task-added-as-follower-greek";i:1;s:20:"task-commented-greek";i:1;s:27:"task-added-attachment-greek";i:1;s:32:"estimate-declined-to-staff-greek";i:1;s:32:"estimate-accepted-to-staff-greek";i:1;s:30:"proposal-client-accepted-greek";i:1;s:31:"proposal-send-to-customer-greek";i:1;s:30:"proposal-client-declined-greek";i:1;s:31:"proposal-client-thank-you-greek";i:1;s:32:"proposal-comment-to-client-greek";i:1;s:31:"proposal-comment-to-admin-greek";i:1;s:36:"estimate-thank-you-to-customer-greek";i:1;s:32:"task-deadline-notification-greek";i:1;s:19:"send-contract-greek";i:1;s:39:"invoice-payment-recorded-to-staff-greek";i:1;s:23:"auto-close-ticket-greek";i:1;s:45:"new-project-discussion-created-to-staff-greek";i:1;s:48:"new-project-discussion-created-to-customer-greek";i:1;s:43:"new-project-file-uploaded-to-customer-greek";i:1;s:40:"new-project-file-uploaded-to-staff-greek";i:1;s:48:"new-project-discussion-comment-to-customer-greek";i:1;s:45:"new-project-discussion-comment-to-staff-greek";i:1;s:35:"staff-added-as-project-member-greek";i:1;s:30:"estimate-expiry-reminder-greek";i:1;s:30:"proposal-expiry-reminder-greek";i:1;s:23:"new-staff-created-greek";i:1;s:29:"contact-forgot-password-greek";i:1;s:30:"contact-password-reseted-greek";i:1;s:26:"contact-set-password-greek";i:1;s:27:"staff-forgot-password-greek";i:1;s:28:"staff-password-reseted-greek";i:1;s:25:"assigned-to-project-greek";i:1;s:39:"task-added-attachment-to-contacts-greek";i:1;s:32:"task-commented-to-contacts-greek";i:1;s:23:"new-lead-assigned-greek";i:1;s:22:"client-statement-greek";i:1;s:30:"ticket-assigned-to-admin-greek";i:1;s:36:"new-client-registered-to-admin-greek";i:1;s:36:"new-web-to-lead-form-submitted-greek";i:1;s:31:"two-factor-authentication-greek";i:1;s:34:"project-finished-to-customer-greek";i:1;s:32:"credit-note-send-to-client-greek";i:1;s:33:"task-status-change-to-staff-greek";i:1;s:36:"task-status-change-to-contacts-greek";i:1;s:26:"reminder-email-staff-greek";i:1;s:32:"contract-comment-to-client-greek";i:1;s:31:"contract-comment-to-admin-greek";i:1;s:23:"send-subscription-greek";i:1;s:33:"subscription-payment-failed-greek";i:1;s:27:"subscription-canceled-greek";i:1;s:36:"subscription-payment-succeeded-greek";i:1;s:34:"contract-expiration-to-staff-greek";i:1;s:26:"gdpr-removal-request-greek";i:1;s:31:"gdpr-removal-request-lead-greek";i:1;s:35:"client-registration-confirmed-greek";i:1;s:30:"contract-signed-to-staff-greek";i:1;s:34:"customer-subscribed-to-staff-greek";i:1;s:32:"contact-verification-email-greek";i:1;s:49:"new-customer-profile-file-uploaded-to-staff-greek";i:1;s:33:"event-notification-to-staff-greek";i:1;s:42:"subscription-payment-requires-action-greek";i:1;s:29:"new-client-created-portuguese";i:1;s:33:"invoice-send-to-client-portuguese";i:1;s:34:"new-ticket-opened-admin-portuguese";i:1;s:23:"ticket-reply-portuguese";i:1;s:30:"ticket-autoresponse-portuguese";i:1;s:35:"invoice-payment-recorded-portuguese";i:1;s:33:"invoice-overdue-notice-portuguese";i:1;s:31:"invoice-already-send-portuguese";i:1;s:35:"new-ticket-created-staff-portuguese";i:1;s:34:"estimate-send-to-client-portuguese";i:1;s:32:"ticket-reply-to-admin-portuguese";i:1;s:32:"estimate-already-send-portuguese";i:1;s:30:"contract-expiration-portuguese";i:1;s:24:"task-assigned-portuguese";i:1;s:33:"task-added-as-follower-portuguese";i:1;s:25:"task-commented-portuguese";i:1;s:32:"task-added-attachment-portuguese";i:1;s:37:"estimate-declined-to-staff-portuguese";i:1;s:37:"estimate-accepted-to-staff-portuguese";i:1;s:35:"proposal-client-accepted-portuguese";i:1;s:36:"proposal-send-to-customer-portuguese";i:1;s:35:"proposal-client-declined-portuguese";i:1;s:36:"proposal-client-thank-you-portuguese";i:1;s:37:"proposal-comment-to-client-portuguese";i:1;s:36:"proposal-comment-to-admin-portuguese";i:1;s:41:"estimate-thank-you-to-customer-portuguese";i:1;s:37:"task-deadline-notification-portuguese";i:1;s:24:"send-contract-portuguese";i:1;s:44:"invoice-payment-recorded-to-staff-portuguese";i:1;s:28:"auto-close-ticket-portuguese";i:1;s:50:"new-project-discussion-created-to-staff-portuguese";i:1;s:53:"new-project-discussion-created-to-customer-portuguese";i:1;s:48:"new-project-file-uploaded-to-customer-portuguese";i:1;s:45:"new-project-file-uploaded-to-staff-portuguese";i:1;s:53:"new-project-discussion-comment-to-customer-portuguese";i:1;s:50:"new-project-discussion-comment-to-staff-portuguese";i:1;s:40:"staff-added-as-project-member-portuguese";i:1;s:35:"estimate-expiry-reminder-portuguese";i:1;s:35:"proposal-expiry-reminder-portuguese";i:1;s:28:"new-staff-created-portuguese";i:1;s:34:"contact-forgot-password-portuguese";i:1;s:35:"contact-password-reseted-portuguese";i:1;s:31:"contact-set-password-portuguese";i:1;s:32:"staff-forgot-password-portuguese";i:1;s:33:"staff-password-reseted-portuguese";i:1;s:30:"assigned-to-project-portuguese";i:1;s:44:"task-added-attachment-to-contacts-portuguese";i:1;s:37:"task-commented-to-contacts-portuguese";i:1;s:28:"new-lead-assigned-portuguese";i:1;s:27:"client-statement-portuguese";i:1;s:35:"ticket-assigned-to-admin-portuguese";i:1;s:41:"new-client-registered-to-admin-portuguese";i:1;s:41:"new-web-to-lead-form-submitted-portuguese";i:1;s:36:"two-factor-authentication-portuguese";i:1;s:39:"project-finished-to-customer-portuguese";i:1;s:37:"credit-note-send-to-client-portuguese";i:1;s:38:"task-status-change-to-staff-portuguese";i:1;s:41:"task-status-change-to-contacts-portuguese";i:1;s:31:"reminder-email-staff-portuguese";i:1;s:37:"contract-comment-to-client-portuguese";i:1;s:36:"contract-comment-to-admin-portuguese";i:1;s:28:"send-subscription-portuguese";i:1;s:38:"subscription-payment-failed-portuguese";i:1;s:32:"subscription-canceled-portuguese";i:1;s:41:"subscription-payment-succeeded-portuguese";i:1;s:39:"contract-expiration-to-staff-portuguese";i:1;s:31:"gdpr-removal-request-portuguese";i:1;s:36:"gdpr-removal-request-lead-portuguese";i:1;s:40:"client-registration-confirmed-portuguese";i:1;s:35:"contract-signed-to-staff-portuguese";i:1;s:39:"customer-subscribed-to-staff-portuguese";i:1;s:37:"contact-verification-email-portuguese";i:1;s:54:"new-customer-profile-file-uploaded-to-staff-portuguese";i:1;s:38:"event-notification-to-staff-portuguese";i:1;s:47:"subscription-payment-requires-action-portuguese";i:1;s:28:"new-client-created-ukrainian";i:1;s:32:"invoice-send-to-client-ukrainian";i:1;s:33:"new-ticket-opened-admin-ukrainian";i:1;s:22:"ticket-reply-ukrainian";i:1;s:29:"ticket-autoresponse-ukrainian";i:1;s:34:"invoice-payment-recorded-ukrainian";i:1;s:32:"invoice-overdue-notice-ukrainian";i:1;s:30:"invoice-already-send-ukrainian";i:1;s:34:"new-ticket-created-staff-ukrainian";i:1;s:33:"estimate-send-to-client-ukrainian";i:1;s:31:"ticket-reply-to-admin-ukrainian";i:1;s:31:"estimate-already-send-ukrainian";i:1;s:29:"contract-expiration-ukrainian";i:1;s:23:"task-assigned-ukrainian";i:1;s:32:"task-added-as-follower-ukrainian";i:1;s:24:"task-commented-ukrainian";i:1;s:31:"task-added-attachment-ukrainian";i:1;s:36:"estimate-declined-to-staff-ukrainian";i:1;s:36:"estimate-accepted-to-staff-ukrainian";i:1;s:34:"proposal-client-accepted-ukrainian";i:1;s:35:"proposal-send-to-customer-ukrainian";i:1;s:34:"proposal-client-declined-ukrainian";i:1;s:35:"proposal-client-thank-you-ukrainian";i:1;s:36:"proposal-comment-to-client-ukrainian";i:1;s:35:"proposal-comment-to-admin-ukrainian";i:1;s:40:"estimate-thank-you-to-customer-ukrainian";i:1;s:36:"task-deadline-notification-ukrainian";i:1;s:23:"send-contract-ukrainian";i:1;s:43:"invoice-payment-recorded-to-staff-ukrainian";i:1;s:27:"auto-close-ticket-ukrainian";i:1;s:49:"new-project-discussion-created-to-staff-ukrainian";i:1;s:52:"new-project-discussion-created-to-customer-ukrainian";i:1;s:47:"new-project-file-uploaded-to-customer-ukrainian";i:1;s:44:"new-project-file-uploaded-to-staff-ukrainian";i:1;s:52:"new-project-discussion-comment-to-customer-ukrainian";i:1;s:49:"new-project-discussion-comment-to-staff-ukrainian";i:1;s:39:"staff-added-as-project-member-ukrainian";i:1;s:34:"estimate-expiry-reminder-ukrainian";i:1;s:34:"proposal-expiry-reminder-ukrainian";i:1;s:27:"new-staff-created-ukrainian";i:1;s:33:"contact-forgot-password-ukrainian";i:1;s:34:"contact-password-reseted-ukrainian";i:1;s:30:"contact-set-password-ukrainian";i:1;s:31:"staff-forgot-password-ukrainian";i:1;s:32:"staff-password-reseted-ukrainian";i:1;s:29:"assigned-to-project-ukrainian";i:1;s:43:"task-added-attachment-to-contacts-ukrainian";i:1;s:36:"task-commented-to-contacts-ukrainian";i:1;s:27:"new-lead-assigned-ukrainian";i:1;s:26:"client-statement-ukrainian";i:1;s:34:"ticket-assigned-to-admin-ukrainian";i:1;s:40:"new-client-registered-to-admin-ukrainian";i:1;s:40:"new-web-to-lead-form-submitted-ukrainian";i:1;s:35:"two-factor-authentication-ukrainian";i:1;s:38:"project-finished-to-customer-ukrainian";i:1;s:36:"credit-note-send-to-client-ukrainian";i:1;s:37:"task-status-change-to-staff-ukrainian";i:1;s:40:"task-status-change-to-contacts-ukrainian";i:1;s:30:"reminder-email-staff-ukrainian";i:1;s:36:"contract-comment-to-client-ukrainian";i:1;s:35:"contract-comment-to-admin-ukrainian";i:1;s:27:"send-subscription-ukrainian";i:1;s:37:"subscription-payment-failed-ukrainian";i:1;s:31:"subscription-canceled-ukrainian";i:1;s:40:"subscription-payment-succeeded-ukrainian";i:1;s:38:"contract-expiration-to-staff-ukrainian";i:1;s:30:"gdpr-removal-request-ukrainian";i:1;s:35:"gdpr-removal-request-lead-ukrainian";i:1;s:39:"client-registration-confirmed-ukrainian";i:1;s:34:"contract-signed-to-staff-ukrainian";i:1;s:38:"customer-subscribed-to-staff-ukrainian";i:1;s:36:"contact-verification-email-ukrainian";i:1;s:53:"new-customer-profile-file-uploaded-to-staff-ukrainian";i:1;s:37:"event-notification-to-staff-ukrainian";i:1;s:46:"subscription-payment-requires-action-ukrainian";i:1;s:26:"new-client-created-swedish";i:1;s:30:"invoice-send-to-client-swedish";i:1;s:31:"new-ticket-opened-admin-swedish";i:1;s:20:"ticket-reply-swedish";i:1;s:27:"ticket-autoresponse-swedish";i:1;s:32:"invoice-payment-recorded-swedish";i:1;s:30:"invoice-overdue-notice-swedish";i:1;s:28:"invoice-already-send-swedish";i:1;s:32:"new-ticket-created-staff-swedish";i:1;s:31:"estimate-send-to-client-swedish";i:1;s:29:"ticket-reply-to-admin-swedish";i:1;s:29:"estimate-already-send-swedish";i:1;s:27:"contract-expiration-swedish";i:1;s:21:"task-assigned-swedish";i:1;s:30:"task-added-as-follower-swedish";i:1;s:22:"task-commented-swedish";i:1;s:29:"task-added-attachment-swedish";i:1;s:34:"estimate-declined-to-staff-swedish";i:1;s:34:"estimate-accepted-to-staff-swedish";i:1;s:32:"proposal-client-accepted-swedish";i:1;s:33:"proposal-send-to-customer-swedish";i:1;s:32:"proposal-client-declined-swedish";i:1;s:33:"proposal-client-thank-you-swedish";i:1;s:34:"proposal-comment-to-client-swedish";i:1;s:33:"proposal-comment-to-admin-swedish";i:1;s:38:"estimate-thank-you-to-customer-swedish";i:1;s:34:"task-deadline-notification-swedish";i:1;s:21:"send-contract-swedish";i:1;s:41:"invoice-payment-recorded-to-staff-swedish";i:1;s:25:"auto-close-ticket-swedish";i:1;s:47:"new-project-discussion-created-to-staff-swedish";i:1;s:50:"new-project-discussion-created-to-customer-swedish";i:1;s:45:"new-project-file-uploaded-to-customer-swedish";i:1;s:42:"new-project-file-uploaded-to-staff-swedish";i:1;s:50:"new-project-discussion-comment-to-customer-swedish";i:1;s:47:"new-project-discussion-comment-to-staff-swedish";i:1;s:37:"staff-added-as-project-member-swedish";i:1;s:32:"estimate-expiry-reminder-swedish";i:1;s:32:"proposal-expiry-reminder-swedish";i:1;s:25:"new-staff-created-swedish";i:1;s:31:"contact-forgot-password-swedish";i:1;s:32:"contact-password-reseted-swedish";i:1;s:28:"contact-set-password-swedish";i:1;s:29:"staff-forgot-password-swedish";i:1;s:30:"staff-password-reseted-swedish";i:1;s:27:"assigned-to-project-swedish";i:1;s:41:"task-added-attachment-to-contacts-swedish";i:1;s:34:"task-commented-to-contacts-swedish";i:1;s:25:"new-lead-assigned-swedish";i:1;s:24:"client-statement-swedish";i:1;s:32:"ticket-assigned-to-admin-swedish";i:1;s:38:"new-client-registered-to-admin-swedish";i:1;s:38:"new-web-to-lead-form-submitted-swedish";i:1;s:33:"two-factor-authentication-swedish";i:1;s:36:"project-finished-to-customer-swedish";i:1;s:34:"credit-note-send-to-client-swedish";i:1;s:35:"task-status-change-to-staff-swedish";i:1;s:38:"task-status-change-to-contacts-swedish";i:1;s:28:"reminder-email-staff-swedish";i:1;s:34:"contract-comment-to-client-swedish";i:1;s:33:"contract-comment-to-admin-swedish";i:1;s:25:"send-subscription-swedish";i:1;s:35:"subscription-payment-failed-swedish";i:1;s:29:"subscription-canceled-swedish";i:1;s:38:"subscription-payment-succeeded-swedish";i:1;s:36:"contract-expiration-to-staff-swedish";i:1;s:28:"gdpr-removal-request-swedish";i:1;s:33:"gdpr-removal-request-lead-swedish";i:1;s:37:"client-registration-confirmed-swedish";i:1;s:32:"contract-signed-to-staff-swedish";i:1;s:36:"customer-subscribed-to-staff-swedish";i:1;s:34:"contact-verification-email-swedish";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-swedish";i:1;s:35:"event-notification-to-staff-swedish";i:1;s:44:"subscription-payment-requires-action-swedish";i:1;s:25:"new-client-created-polish";i:1;s:29:"invoice-send-to-client-polish";i:1;s:30:"new-ticket-opened-admin-polish";i:1;s:19:"ticket-reply-polish";i:1;s:26:"ticket-autoresponse-polish";i:1;s:31:"invoice-payment-recorded-polish";i:1;s:29:"invoice-overdue-notice-polish";i:1;s:27:"invoice-already-send-polish";i:1;s:31:"new-ticket-created-staff-polish";i:1;s:30:"estimate-send-to-client-polish";i:1;s:28:"ticket-reply-to-admin-polish";i:1;s:28:"estimate-already-send-polish";i:1;s:26:"contract-expiration-polish";i:1;s:20:"task-assigned-polish";i:1;s:29:"task-added-as-follower-polish";i:1;s:21:"task-commented-polish";i:1;s:28:"task-added-attachment-polish";i:1;s:33:"estimate-declined-to-staff-polish";i:1;s:33:"estimate-accepted-to-staff-polish";i:1;s:31:"proposal-client-accepted-polish";i:1;s:32:"proposal-send-to-customer-polish";i:1;s:31:"proposal-client-declined-polish";i:1;s:32:"proposal-client-thank-you-polish";i:1;s:33:"proposal-comment-to-client-polish";i:1;s:32:"proposal-comment-to-admin-polish";i:1;s:37:"estimate-thank-you-to-customer-polish";i:1;s:33:"task-deadline-notification-polish";i:1;s:20:"send-contract-polish";i:1;s:40:"invoice-payment-recorded-to-staff-polish";i:1;s:24:"auto-close-ticket-polish";i:1;s:46:"new-project-discussion-created-to-staff-polish";i:1;s:49:"new-project-discussion-created-to-customer-polish";i:1;s:44:"new-project-file-uploaded-to-customer-polish";i:1;s:41:"new-project-file-uploaded-to-staff-polish";i:1;s:49:"new-project-discussion-comment-to-customer-polish";i:1;s:46:"new-project-discussion-comment-to-staff-polish";i:1;s:36:"staff-added-as-project-member-polish";i:1;s:31:"estimate-expiry-reminder-polish";i:1;s:31:"proposal-expiry-reminder-polish";i:1;s:24:"new-staff-created-polish";i:1;s:30:"contact-forgot-password-polish";i:1;s:31:"contact-password-reseted-polish";i:1;s:27:"contact-set-password-polish";i:1;s:28:"staff-forgot-password-polish";i:1;s:29:"staff-password-reseted-polish";i:1;s:26:"assigned-to-project-polish";i:1;s:40:"task-added-attachment-to-contacts-polish";i:1;s:33:"task-commented-to-contacts-polish";i:1;s:24:"new-lead-assigned-polish";i:1;s:23:"client-statement-polish";i:1;s:31:"ticket-assigned-to-admin-polish";i:1;s:37:"new-client-registered-to-admin-polish";i:1;s:37:"new-web-to-lead-form-submitted-polish";i:1;s:32:"two-factor-authentication-polish";i:1;s:35:"project-finished-to-customer-polish";i:1;s:33:"credit-note-send-to-client-polish";i:1;s:34:"task-status-change-to-staff-polish";i:1;s:37:"task-status-change-to-contacts-polish";i:1;s:27:"reminder-email-staff-polish";i:1;s:33:"contract-comment-to-client-polish";i:1;s:32:"contract-comment-to-admin-polish";i:1;s:24:"send-subscription-polish";i:1;s:34:"subscription-payment-failed-polish";i:1;s:28:"subscription-canceled-polish";i:1;s:37:"subscription-payment-succeeded-polish";i:1;s:35:"contract-expiration-to-staff-polish";i:1;s:27:"gdpr-removal-request-polish";i:1;s:32:"gdpr-removal-request-lead-polish";i:1;s:36:"client-registration-confirmed-polish";i:1;s:31:"contract-signed-to-staff-polish";i:1;s:35:"customer-subscribed-to-staff-polish";i:1;s:33:"contact-verification-email-polish";i:1;s:50:"new-customer-profile-file-uploaded-to-staff-polish";i:1;s:34:"event-notification-to-staff-polish";i:1;s:43:"subscription-payment-requires-action-polish";i:1;s:24:"new-client-created-dutch";i:1;s:28:"invoice-send-to-client-dutch";i:1;s:29:"new-ticket-opened-admin-dutch";i:1;s:18:"ticket-reply-dutch";i:1;s:25:"ticket-autoresponse-dutch";i:1;s:30:"invoice-payment-recorded-dutch";i:1;s:28:"invoice-overdue-notice-dutch";i:1;s:26:"invoice-already-send-dutch";i:1;s:30:"new-ticket-created-staff-dutch";i:1;s:29:"estimate-send-to-client-dutch";i:1;s:27:"ticket-reply-to-admin-dutch";i:1;s:27:"estimate-already-send-dutch";i:1;s:25:"contract-expiration-dutch";i:1;s:19:"task-assigned-dutch";i:1;s:28:"task-added-as-follower-dutch";i:1;s:20:"task-commented-dutch";i:1;s:27:"task-added-attachment-dutch";i:1;s:32:"estimate-declined-to-staff-dutch";i:1;s:32:"estimate-accepted-to-staff-dutch";i:1;s:30:"proposal-client-accepted-dutch";i:1;s:31:"proposal-send-to-customer-dutch";i:1;s:30:"proposal-client-declined-dutch";i:1;s:31:"proposal-client-thank-you-dutch";i:1;s:32:"proposal-comment-to-client-dutch";i:1;s:31:"proposal-comment-to-admin-dutch";i:1;s:36:"estimate-thank-you-to-customer-dutch";i:1;s:32:"task-deadline-notification-dutch";i:1;s:19:"send-contract-dutch";i:1;s:39:"invoice-payment-recorded-to-staff-dutch";i:1;s:23:"auto-close-ticket-dutch";i:1;s:45:"new-project-discussion-created-to-staff-dutch";i:1;s:48:"new-project-discussion-created-to-customer-dutch";i:1;s:43:"new-project-file-uploaded-to-customer-dutch";i:1;s:40:"new-project-file-uploaded-to-staff-dutch";i:1;s:48:"new-project-discussion-comment-to-customer-dutch";i:1;s:45:"new-project-discussion-comment-to-staff-dutch";i:1;s:35:"staff-added-as-project-member-dutch";i:1;s:30:"estimate-expiry-reminder-dutch";i:1;s:30:"proposal-expiry-reminder-dutch";i:1;s:23:"new-staff-created-dutch";i:1;s:29:"contact-forgot-password-dutch";i:1;s:30:"contact-password-reseted-dutch";i:1;s:26:"contact-set-password-dutch";i:1;s:27:"staff-forgot-password-dutch";i:1;s:28:"staff-password-reseted-dutch";i:1;s:25:"assigned-to-project-dutch";i:1;s:39:"task-added-attachment-to-contacts-dutch";i:1;s:32:"task-commented-to-contacts-dutch";i:1;s:23:"new-lead-assigned-dutch";i:1;s:22:"client-statement-dutch";i:1;s:30:"ticket-assigned-to-admin-dutch";i:1;s:36:"new-client-registered-to-admin-dutch";i:1;s:36:"new-web-to-lead-form-submitted-dutch";i:1;s:31:"two-factor-authentication-dutch";i:1;s:34:"project-finished-to-customer-dutch";i:1;s:32:"credit-note-send-to-client-dutch";i:1;s:33:"task-status-change-to-staff-dutch";i:1;s:36:"task-status-change-to-contacts-dutch";i:1;s:26:"reminder-email-staff-dutch";i:1;s:32:"contract-comment-to-client-dutch";i:1;s:31:"contract-comment-to-admin-dutch";i:1;s:23:"send-subscription-dutch";i:1;s:33:"subscription-payment-failed-dutch";i:1;s:27:"subscription-canceled-dutch";i:1;s:36:"subscription-payment-succeeded-dutch";i:1;s:34:"contract-expiration-to-staff-dutch";i:1;s:26:"gdpr-removal-request-dutch";i:1;s:31:"gdpr-removal-request-lead-dutch";i:1;s:35:"client-registration-confirmed-dutch";i:1;s:30:"contract-signed-to-staff-dutch";i:1;s:34:"customer-subscribed-to-staff-dutch";i:1;s:32:"contact-verification-email-dutch";i:1;s:49:"new-customer-profile-file-uploaded-to-staff-dutch";i:1;s:33:"event-notification-to-staff-dutch";i:1;s:42:"subscription-payment-requires-action-dutch";i:1;s:26:"new-client-created-chinese";i:1;s:30:"invoice-send-to-client-chinese";i:1;s:31:"new-ticket-opened-admin-chinese";i:1;s:20:"ticket-reply-chinese";i:1;s:27:"ticket-autoresponse-chinese";i:1;s:32:"invoice-payment-recorded-chinese";i:1;s:30:"invoice-overdue-notice-chinese";i:1;s:28:"invoice-already-send-chinese";i:1;s:32:"new-ticket-created-staff-chinese";i:1;s:31:"estimate-send-to-client-chinese";i:1;s:29:"ticket-reply-to-admin-chinese";i:1;s:29:"estimate-already-send-chinese";i:1;s:27:"contract-expiration-chinese";i:1;s:21:"task-assigned-chinese";i:1;s:30:"task-added-as-follower-chinese";i:1;s:22:"task-commented-chinese";i:1;s:29:"task-added-attachment-chinese";i:1;s:34:"estimate-declined-to-staff-chinese";i:1;s:34:"estimate-accepted-to-staff-chinese";i:1;s:32:"proposal-client-accepted-chinese";i:1;s:33:"proposal-send-to-customer-chinese";i:1;s:32:"proposal-client-declined-chinese";i:1;s:33:"proposal-client-thank-you-chinese";i:1;s:34:"proposal-comment-to-client-chinese";i:1;s:33:"proposal-comment-to-admin-chinese";i:1;s:38:"estimate-thank-you-to-customer-chinese";i:1;s:34:"task-deadline-notification-chinese";i:1;s:21:"send-contract-chinese";i:1;s:41:"invoice-payment-recorded-to-staff-chinese";i:1;s:25:"auto-close-ticket-chinese";i:1;s:47:"new-project-discussion-created-to-staff-chinese";i:1;s:50:"new-project-discussion-created-to-customer-chinese";i:1;s:45:"new-project-file-uploaded-to-customer-chinese";i:1;s:42:"new-project-file-uploaded-to-staff-chinese";i:1;s:50:"new-project-discussion-comment-to-customer-chinese";i:1;s:47:"new-project-discussion-comment-to-staff-chinese";i:1;s:37:"staff-added-as-project-member-chinese";i:1;s:32:"estimate-expiry-reminder-chinese";i:1;s:32:"proposal-expiry-reminder-chinese";i:1;s:25:"new-staff-created-chinese";i:1;s:31:"contact-forgot-password-chinese";i:1;s:32:"contact-password-reseted-chinese";i:1;s:28:"contact-set-password-chinese";i:1;s:29:"staff-forgot-password-chinese";i:1;s:30:"staff-password-reseted-chinese";i:1;s:27:"assigned-to-project-chinese";i:1;s:41:"task-added-attachment-to-contacts-chinese";i:1;s:34:"task-commented-to-contacts-chinese";i:1;s:25:"new-lead-assigned-chinese";i:1;s:24:"client-statement-chinese";i:1;s:32:"ticket-assigned-to-admin-chinese";i:1;s:38:"new-client-registered-to-admin-chinese";i:1;s:38:"new-web-to-lead-form-submitted-chinese";i:1;s:33:"two-factor-authentication-chinese";i:1;s:36:"project-finished-to-customer-chinese";i:1;s:34:"credit-note-send-to-client-chinese";i:1;s:35:"task-status-change-to-staff-chinese";i:1;s:38:"task-status-change-to-contacts-chinese";i:1;s:28:"reminder-email-staff-chinese";i:1;s:34:"contract-comment-to-client-chinese";i:1;s:33:"contract-comment-to-admin-chinese";i:1;s:25:"send-subscription-chinese";i:1;s:35:"subscription-payment-failed-chinese";i:1;s:29:"subscription-canceled-chinese";i:1;s:38:"subscription-payment-succeeded-chinese";i:1;s:36:"contract-expiration-to-staff-chinese";i:1;s:28:"gdpr-removal-request-chinese";i:1;s:33:"gdpr-removal-request-lead-chinese";i:1;s:37:"client-registration-confirmed-chinese";i:1;s:32:"contract-signed-to-staff-chinese";i:1;s:36:"customer-subscribed-to-staff-chinese";i:1;s:34:"contact-verification-email-chinese";i:1;s:51:"new-customer-profile-file-uploaded-to-staff-chinese";i:1;s:35:"event-notification-to-staff-chinese";i:1;s:44:"subscription-payment-requires-action-chinese";i:1;s:32:"new-client-created-portuguese_br";i:1;s:36:"invoice-send-to-client-portuguese_br";i:1;s:37:"new-ticket-opened-admin-portuguese_br";i:1;s:26:"ticket-reply-portuguese_br";i:1;s:33:"ticket-autoresponse-portuguese_br";i:1;s:38:"invoice-payment-recorded-portuguese_br";i:1;s:36:"invoice-overdue-notice-portuguese_br";i:1;s:34:"invoice-already-send-portuguese_br";i:1;s:38:"new-ticket-created-staff-portuguese_br";i:1;s:37:"estimate-send-to-client-portuguese_br";i:1;s:35:"ticket-reply-to-admin-portuguese_br";i:1;s:35:"estimate-already-send-portuguese_br";i:1;s:33:"contract-expiration-portuguese_br";i:1;s:27:"task-assigned-portuguese_br";i:1;s:36:"task-added-as-follower-portuguese_br";i:1;s:28:"task-commented-portuguese_br";i:1;s:35:"task-added-attachment-portuguese_br";i:1;s:40:"estimate-declined-to-staff-portuguese_br";i:1;s:40:"estimate-accepted-to-staff-portuguese_br";i:1;s:38:"proposal-client-accepted-portuguese_br";i:1;s:39:"proposal-send-to-customer-portuguese_br";i:1;s:38:"proposal-client-declined-portuguese_br";i:1;s:39:"proposal-client-thank-you-portuguese_br";i:1;s:40:"proposal-comment-to-client-portuguese_br";i:1;s:39:"proposal-comment-to-admin-portuguese_br";i:1;s:44:"estimate-thank-you-to-customer-portuguese_br";i:1;s:40:"task-deadline-notification-portuguese_br";i:1;s:27:"send-contract-portuguese_br";i:1;s:47:"invoice-payment-recorded-to-staff-portuguese_br";i:1;s:31:"auto-close-ticket-portuguese_br";i:1;s:53:"new-project-discussion-created-to-staff-portuguese_br";i:1;s:56:"new-project-discussion-created-to-customer-portuguese_br";i:1;s:51:"new-project-file-uploaded-to-customer-portuguese_br";i:1;s:48:"new-project-file-uploaded-to-staff-portuguese_br";i:1;s:56:"new-project-discussion-comment-to-customer-portuguese_br";i:1;s:53:"new-project-discussion-comment-to-staff-portuguese_br";i:1;s:43:"staff-added-as-project-member-portuguese_br";i:1;s:38:"estimate-expiry-reminder-portuguese_br";i:1;s:38:"proposal-expiry-reminder-portuguese_br";i:1;s:31:"new-staff-created-portuguese_br";i:1;s:37:"contact-forgot-password-portuguese_br";i:1;s:38:"contact-password-reseted-portuguese_br";i:1;s:34:"contact-set-password-portuguese_br";i:1;s:35:"staff-forgot-password-portuguese_br";i:1;s:36:"staff-password-reseted-portuguese_br";i:1;s:33:"assigned-to-project-portuguese_br";i:1;s:47:"task-added-attachment-to-contacts-portuguese_br";i:1;s:40:"task-commented-to-contacts-portuguese_br";i:1;s:31:"new-lead-assigned-portuguese_br";i:1;s:30:"client-statement-portuguese_br";i:1;s:38:"ticket-assigned-to-admin-portuguese_br";i:1;s:44:"new-client-registered-to-admin-portuguese_br";i:1;s:44:"new-web-to-lead-form-submitted-portuguese_br";i:1;s:39:"two-factor-authentication-portuguese_br";i:1;s:42:"project-finished-to-customer-portuguese_br";i:1;s:40:"credit-note-send-to-client-portuguese_br";i:1;s:41:"task-status-change-to-staff-portuguese_br";i:1;s:44:"task-status-change-to-contacts-portuguese_br";i:1;s:34:"reminder-email-staff-portuguese_br";i:1;s:40:"contract-comment-to-client-portuguese_br";i:1;s:39:"contract-comment-to-admin-portuguese_br";i:1;s:31:"send-subscription-portuguese_br";i:1;s:41:"subscription-payment-failed-portuguese_br";i:1;s:35:"subscription-canceled-portuguese_br";i:1;s:44:"subscription-payment-succeeded-portuguese_br";i:1;s:42:"contract-expiration-to-staff-portuguese_br";i:1;s:34:"gdpr-removal-request-portuguese_br";i:1;s:39:"gdpr-removal-request-lead-portuguese_br";i:1;s:43:"client-registration-confirmed-portuguese_br";i:1;s:38:"contract-signed-to-staff-portuguese_br";i:1;s:42:"customer-subscribed-to-staff-portuguese_br";i:1;s:40:"contact-verification-email-portuguese_br";i:1;s:57:"new-customer-profile-file-uploaded-to-staff-portuguese_br";i:1;s:41:"event-notification-to-staff-portuguese_br";i:1;s:50:"subscription-payment-requires-action-portuguese_br";i:1;s:33:"new-ticket-opened-approval-slovak";i:1;s:32:"new-ticket-opened-approval-czech";i:1;s:35:"new-ticket-opened-approval-romanian";i:1;s:35:"new-ticket-opened-approval-japanese";i:1;s:36:"new-ticket-opened-approval-indonesia";i:1;s:36:"new-ticket-opened-approval-bulgarian";i:1;s:37:"new-ticket-opened-approval-vietnamese";i:1;s:33:"new-ticket-opened-approval-german";i:1;s:33:"new-ticket-opened-approval-french";i:1;s:34:"new-ticket-opened-approval-italian";i:1;s:34:"new-ticket-opened-approval-spanish";i:1;s:34:"new-ticket-opened-approval-russian";i:1;s:34:"new-ticket-opened-approval-catalan";i:1;s:34:"new-ticket-opened-approval-persian";i:1;s:34:"new-ticket-opened-approval-turkish";i:1;s:32:"new-ticket-opened-approval-greek";i:1;s:37:"new-ticket-opened-approval-portuguese";i:1;s:36:"new-ticket-opened-approval-ukrainian";i:1;s:34:"new-ticket-opened-approval-swedish";i:1;s:33:"new-ticket-opened-approval-polish";i:1;s:32:"new-ticket-opened-approval-dutch";i:1;s:34:"new-ticket-opened-approval-chinese";i:1;s:40:"new-ticket-opened-approval-portuguese_br";i:1;s:39:"new-service_request-opened-admin-slovak";i:1;s:28:"service_request-reply-slovak";i:1;s:35:"service_request-autoresponse-slovak";i:1;s:40:"new-service_request-created-staff-slovak";i:1;s:37:"service_request-reply-to-admin-slovak";i:1;s:33:"auto-close-service_request-slovak";i:1;s:40:"service_request-assigned-to-admin-slovak";i:1;s:42:"new-service_request-opened-approval-slovak";i:1;s:38:"new-service_request-opened-admin-czech";i:1;s:27:"service_request-reply-czech";i:1;s:34:"service_request-autoresponse-czech";i:1;s:39:"new-service_request-created-staff-czech";i:1;s:36:"service_request-reply-to-admin-czech";i:1;s:32:"auto-close-service_request-czech";i:1;s:39:"service_request-assigned-to-admin-czech";i:1;s:41:"new-service_request-opened-approval-czech";i:1;s:41:"new-service_request-opened-admin-romanian";i:1;s:30:"service_request-reply-romanian";i:1;s:37:"service_request-autoresponse-romanian";i:1;s:42:"new-service_request-created-staff-romanian";i:1;s:39:"service_request-reply-to-admin-romanian";i:1;s:35:"auto-close-service_request-romanian";i:1;s:42:"service_request-assigned-to-admin-romanian";i:1;s:44:"new-service_request-opened-approval-romanian";i:1;s:41:"new-service_request-opened-admin-japanese";i:1;s:30:"service_request-reply-japanese";i:1;s:37:"service_request-autoresponse-japanese";i:1;s:42:"new-service_request-created-staff-japanese";i:1;s:39:"service_request-reply-to-admin-japanese";i:1;s:35:"auto-close-service_request-japanese";i:1;s:42:"service_request-assigned-to-admin-japanese";i:1;s:44:"new-service_request-opened-approval-japanese";i:1;s:42:"new-service_request-opened-admin-indonesia";i:1;s:31:"service_request-reply-indonesia";i:1;s:38:"service_request-autoresponse-indonesia";i:1;s:43:"new-service_request-created-staff-indonesia";i:1;s:40:"service_request-reply-to-admin-indonesia";i:1;s:36:"auto-close-service_request-indonesia";i:1;s:43:"service_request-assigned-to-admin-indonesia";i:1;s:45:"new-service_request-opened-approval-indonesia";i:1;s:42:"new-service_request-opened-admin-bulgarian";i:1;s:31:"service_request-reply-bulgarian";i:1;s:38:"service_request-autoresponse-bulgarian";i:1;s:43:"new-service_request-created-staff-bulgarian";i:1;s:40:"service_request-reply-to-admin-bulgarian";i:1;s:36:"auto-close-service_request-bulgarian";i:1;s:43:"service_request-assigned-to-admin-bulgarian";i:1;s:45:"new-service_request-opened-approval-bulgarian";i:1;s:43:"new-service_request-opened-admin-vietnamese";i:1;s:32:"service_request-reply-vietnamese";i:1;s:39:"service_request-autoresponse-vietnamese";i:1;s:44:"new-service_request-created-staff-vietnamese";i:1;s:41:"service_request-reply-to-admin-vietnamese";i:1;s:37:"auto-close-service_request-vietnamese";i:1;s:44:"service_request-assigned-to-admin-vietnamese";i:1;s:46:"new-service_request-opened-approval-vietnamese";i:1;s:39:"new-service_request-opened-admin-german";i:1;s:28:"service_request-reply-german";i:1;s:35:"service_request-autoresponse-german";i:1;s:40:"new-service_request-created-staff-german";i:1;s:37:"service_request-reply-to-admin-german";i:1;s:33:"auto-close-service_request-german";i:1;s:40:"service_request-assigned-to-admin-german";i:1;s:42:"new-service_request-opened-approval-german";i:1;s:39:"new-service_request-opened-admin-french";i:1;s:28:"service_request-reply-french";i:1;s:35:"service_request-autoresponse-french";i:1;s:40:"new-service_request-created-staff-french";i:1;s:37:"service_request-reply-to-admin-french";i:1;s:33:"auto-close-service_request-french";i:1;s:40:"service_request-assigned-to-admin-french";i:1;s:42:"new-service_request-opened-approval-french";i:1;s:40:"new-service_request-opened-admin-italian";i:1;s:29:"service_request-reply-italian";i:1;s:36:"service_request-autoresponse-italian";i:1;s:41:"new-service_request-created-staff-italian";i:1;s:38:"service_request-reply-to-admin-italian";i:1;s:34:"auto-close-service_request-italian";i:1;s:41:"service_request-assigned-to-admin-italian";i:1;s:43:"new-service_request-opened-approval-italian";i:1;s:40:"new-service_request-opened-admin-spanish";i:1;s:29:"service_request-reply-spanish";i:1;s:36:"service_request-autoresponse-spanish";i:1;s:41:"new-service_request-created-staff-spanish";i:1;s:38:"service_request-reply-to-admin-spanish";i:1;s:34:"auto-close-service_request-spanish";i:1;s:41:"service_request-assigned-to-admin-spanish";i:1;s:43:"new-service_request-opened-approval-spanish";i:1;s:40:"new-service_request-opened-admin-russian";i:1;s:29:"service_request-reply-russian";i:1;s:36:"service_request-autoresponse-russian";i:1;s:41:"new-service_request-created-staff-russian";i:1;s:38:"service_request-reply-to-admin-russian";i:1;s:34:"auto-close-service_request-russian";i:1;s:41:"service_request-assigned-to-admin-russian";i:1;s:43:"new-service_request-opened-approval-russian";i:1;s:40:"new-service_request-opened-admin-catalan";i:1;s:29:"service_request-reply-catalan";i:1;s:36:"service_request-autoresponse-catalan";i:1;s:41:"new-service_request-created-staff-catalan";i:1;s:38:"service_request-reply-to-admin-catalan";i:1;s:34:"auto-close-service_request-catalan";i:1;s:41:"service_request-assigned-to-admin-catalan";i:1;s:43:"new-service_request-opened-approval-catalan";i:1;s:40:"new-service_request-opened-admin-persian";i:1;s:29:"service_request-reply-persian";i:1;s:36:"service_request-autoresponse-persian";i:1;s:41:"new-service_request-created-staff-persian";i:1;s:38:"service_request-reply-to-admin-persian";i:1;s:34:"auto-close-service_request-persian";i:1;s:41:"service_request-assigned-to-admin-persian";i:1;s:43:"new-service_request-opened-approval-persian";i:1;s:40:"new-service_request-opened-admin-turkish";i:1;s:29:"service_request-reply-turkish";i:1;s:36:"service_request-autoresponse-turkish";i:1;s:41:"new-service_request-created-staff-turkish";i:1;s:38:"service_request-reply-to-admin-turkish";i:1;s:34:"auto-close-service_request-turkish";i:1;s:41:"service_request-assigned-to-admin-turkish";i:1;s:43:"new-service_request-opened-approval-turkish";i:1;s:38:"new-service_request-opened-admin-greek";i:1;s:27:"service_request-reply-greek";i:1;s:34:"service_request-autoresponse-greek";i:1;s:39:"new-service_request-created-staff-greek";i:1;s:36:"service_request-reply-to-admin-greek";i:1;s:32:"auto-close-service_request-greek";i:1;s:39:"service_request-assigned-to-admin-greek";i:1;s:41:"new-service_request-opened-approval-greek";i:1;s:43:"new-service_request-opened-admin-portuguese";i:1;s:32:"service_request-reply-portuguese";i:1;s:39:"service_request-autoresponse-portuguese";i:1;s:44:"new-service_request-created-staff-portuguese";i:1;s:41:"service_request-reply-to-admin-portuguese";i:1;s:37:"auto-close-service_request-portuguese";i:1;s:44:"service_request-assigned-to-admin-portuguese";i:1;s:46:"new-service_request-opened-approval-portuguese";i:1;s:42:"new-service_request-opened-admin-ukrainian";i:1;s:31:"service_request-reply-ukrainian";i:1;s:38:"service_request-autoresponse-ukrainian";i:1;s:43:"new-service_request-created-staff-ukrainian";i:1;s:40:"service_request-reply-to-admin-ukrainian";i:1;s:36:"auto-close-service_request-ukrainian";i:1;s:43:"service_request-assigned-to-admin-ukrainian";i:1;s:45:"new-service_request-opened-approval-ukrainian";i:1;s:40:"new-service_request-opened-admin-swedish";i:1;s:29:"service_request-reply-swedish";i:1;s:36:"service_request-autoresponse-swedish";i:1;s:41:"new-service_request-created-staff-swedish";i:1;s:38:"service_request-reply-to-admin-swedish";i:1;s:34:"auto-close-service_request-swedish";i:1;s:41:"service_request-assigned-to-admin-swedish";i:1;s:43:"new-service_request-opened-approval-swedish";i:1;s:39:"new-service_request-opened-admin-polish";i:1;s:28:"service_request-reply-polish";i:1;s:35:"service_request-autoresponse-polish";i:1;s:40:"new-service_request-created-staff-polish";i:1;s:37:"service_request-reply-to-admin-polish";i:1;s:33:"auto-close-service_request-polish";i:1;s:40:"service_request-assigned-to-admin-polish";i:1;s:42:"new-service_request-opened-approval-polish";i:1;s:38:"new-service_request-opened-admin-dutch";i:1;s:27:"service_request-reply-dutch";i:1;s:34:"service_request-autoresponse-dutch";i:1;s:39:"new-service_request-created-staff-dutch";i:1;s:36:"service_request-reply-to-admin-dutch";i:1;s:32:"auto-close-service_request-dutch";i:1;s:39:"service_request-assigned-to-admin-dutch";i:1;s:41:"new-service_request-opened-approval-dutch";i:1;s:40:"new-service_request-opened-admin-chinese";i:1;s:29:"service_request-reply-chinese";i:1;s:36:"service_request-autoresponse-chinese";i:1;s:41:"new-service_request-created-staff-chinese";i:1;s:38:"service_request-reply-to-admin-chinese";i:1;s:34:"auto-close-service_request-chinese";i:1;s:41:"service_request-assigned-to-admin-chinese";i:1;s:43:"new-service_request-opened-approval-chinese";i:1;s:46:"new-service_request-opened-admin-portuguese_br";i:1;s:35:"service_request-reply-portuguese_br";i:1;s:42:"service_request-autoresponse-portuguese_br";i:1;s:47:"new-service_request-created-staff-portuguese_br";i:1;s:44:"service_request-reply-to-admin-portuguese_br";i:1;s:40:"auto-close-service_request-portuguese_br";i:1;s:47:"service_request-assigned-to-admin-portuguese_br";i:1;s:49:"new-service_request-opened-approval-portuguese_br";i:1;s:34:"ticket-assigned-to-customer-slovak";i:1;s:33:"ticket-assigned-to-customer-czech";i:1;s:36:"ticket-assigned-to-customer-romanian";i:1;s:36:"ticket-assigned-to-customer-japanese";i:1;s:37:"ticket-assigned-to-customer-indonesia";i:1;s:37:"ticket-assigned-to-customer-bulgarian";i:1;s:38:"ticket-assigned-to-customer-vietnamese";i:1;s:34:"ticket-assigned-to-customer-german";i:1;s:34:"ticket-assigned-to-customer-french";i:1;s:35:"ticket-assigned-to-customer-italian";i:1;s:35:"ticket-assigned-to-customer-spanish";i:1;s:35:"ticket-assigned-to-customer-russian";i:1;s:35:"ticket-assigned-to-customer-catalan";i:1;s:35:"ticket-assigned-to-customer-persian";i:1;s:35:"ticket-assigned-to-customer-turkish";i:1;s:33:"ticket-assigned-to-customer-greek";i:1;s:38:"ticket-assigned-to-customer-portuguese";i:1;s:37:"ticket-assigned-to-customer-ukrainian";i:1;s:35:"ticket-assigned-to-customer-swedish";i:1;s:34:"ticket-assigned-to-customer-polish";i:1;s:33:"ticket-assigned-to-customer-dutch";i:1;s:35:"ticket-assigned-to-customer-chinese";i:1;s:41:"ticket-assigned-to-customer-portuguese_br";i:1;s:36:"ticket-exceded-slato-staff-bulgarian";i:1;s:33:"new-ticket-closed-admin-bulgarian";i:1;s:42:"new-service_request-closed-admin-bulgarian";i:1;s:34:"ticket-exceded-slato-staff-catalan";i:1;s:31:"new-ticket-closed-admin-catalan";i:1;s:40:"new-service_request-closed-admin-catalan";i:1;s:34:"ticket-exceded-slato-staff-chinese";i:1;s:31:"new-ticket-closed-admin-chinese";i:1;s:40:"new-service_request-closed-admin-chinese";i:1;s:32:"ticket-exceded-slato-staff-czech";i:1;s:29:"new-ticket-closed-admin-czech";i:1;s:38:"new-service_request-closed-admin-czech";i:1;s:32:"ticket-exceded-slato-staff-dutch";i:1;s:29:"new-ticket-closed-admin-dutch";i:1;s:38:"new-service_request-closed-admin-dutch";i:1;s:33:"ticket-exceded-slato-staff-french";i:1;s:30:"new-ticket-closed-admin-french";i:1;s:39:"new-service_request-closed-admin-french";i:1;s:33:"ticket-exceded-slato-staff-german";i:1;s:30:"new-ticket-closed-admin-german";i:1;s:39:"new-service_request-closed-admin-german";i:1;s:32:"ticket-exceded-slato-staff-greek";i:1;s:29:"new-ticket-closed-admin-greek";i:1;s:38:"new-service_request-closed-admin-greek";i:1;s:36:"ticket-exceded-slato-staff-indonesia";i:1;s:33:"new-ticket-closed-admin-indonesia";i:1;s:42:"new-service_request-closed-admin-indonesia";i:1;s:34:"ticket-exceded-slato-staff-italian";i:1;s:31:"new-ticket-closed-admin-italian";i:1;s:40:"new-service_request-closed-admin-italian";i:1;s:35:"ticket-exceded-slato-staff-japanese";i:1;s:32:"new-ticket-closed-admin-japanese";i:1;s:41:"new-service_request-closed-admin-japanese";i:1;s:34:"ticket-exceded-slato-staff-persian";i:1;s:31:"new-ticket-closed-admin-persian";i:1;s:40:"new-service_request-closed-admin-persian";i:1;s:33:"ticket-exceded-slato-staff-polish";i:1;s:30:"new-ticket-closed-admin-polish";i:1;s:39:"new-service_request-closed-admin-polish";i:1;s:37:"ticket-exceded-slato-staff-portuguese";i:1;s:34:"new-ticket-closed-admin-portuguese";i:1;s:43:"new-service_request-closed-admin-portuguese";i:1;s:40:"ticket-exceded-slato-staff-portuguese_br";i:1;s:37:"new-ticket-closed-admin-portuguese_br";i:1;s:46:"new-service_request-closed-admin-portuguese_br";i:1;s:35:"ticket-exceded-slato-staff-romanian";i:1;s:32:"new-ticket-closed-admin-romanian";i:1;s:41:"new-service_request-closed-admin-romanian";i:1;s:34:"ticket-exceded-slato-staff-russian";i:1;s:31:"new-ticket-closed-admin-russian";i:1;s:40:"new-service_request-closed-admin-russian";i:1;s:33:"ticket-exceded-slato-staff-slovak";i:1;s:30:"new-ticket-closed-admin-slovak";i:1;s:39:"new-service_request-closed-admin-slovak";i:1;s:34:"ticket-exceded-slato-staff-spanish";i:1;s:31:"new-ticket-closed-admin-spanish";i:1;s:40:"new-service_request-closed-admin-spanish";i:1;s:34:"ticket-exceded-slato-staff-swedish";i:1;s:31:"new-ticket-closed-admin-swedish";i:1;s:40:"new-service_request-closed-admin-swedish";i:1;s:34:"ticket-exceded-slato-staff-turkish";i:1;s:31:"new-ticket-closed-admin-turkish";i:1;s:40:"new-service_request-closed-admin-turkish";i:1;s:36:"ticket-exceded-slato-staff-ukrainian";i:1;s:33:"new-ticket-closed-admin-ukrainian";i:1;s:42:"new-service_request-closed-admin-ukrainian";i:1;s:37:"ticket-exceded-slato-staff-vietnamese";i:1;s:34:"new-ticket-closed-admin-vietnamese";i:1;s:43:"new-service_request-closed-admin-vietnamese";i:1;s:17:"reports-bulgarian";i:1;s:15:"reports-catalan";i:1;s:15:"reports-chinese";i:1;s:13:"reports-czech";i:1;s:13:"reports-dutch";i:1;s:14:"reports-french";i:1;s:14:"reports-german";i:1;s:13:"reports-greek";i:1;s:17:"reports-indonesia";i:1;s:15:"reports-italian";i:1;s:16:"reports-japanese";i:1;s:15:"reports-persian";i:1;s:14:"reports-polish";i:1;s:18:"reports-portuguese";i:1;s:21:"reports-portuguese_br";i:1;s:16:"reports-romanian";i:1;s:15:"reports-russian";i:1;s:14:"reports-slovak";i:1;s:15:"reports-spanish";i:1;s:15:"reports-swedish";i:1;s:15:"reports-turkish";i:1;s:17:"reports-ukrainian";i:1;s:18:"reports-vietnamese";i:1;s:33:"send-contact-order-note-bulgarian";i:1;s:31:"send-contact-order-note-catalan";i:1;s:31:"send-contact-order-note-chinese";i:1;s:29:"send-contact-order-note-czech";i:1;s:29:"send-contact-order-note-dutch";i:1;s:30:"send-contact-order-note-french";i:1;s:30:"send-contact-order-note-german";i:1;s:29:"send-contact-order-note-greek";i:1;s:33:"send-contact-order-note-indonesia";i:1;s:31:"send-contact-order-note-italian";i:1;s:32:"send-contact-order-note-japanese";i:1;s:31:"send-contact-order-note-persian";i:1;s:30:"send-contact-order-note-polish";i:1;s:34:"send-contact-order-note-portuguese";i:1;s:37:"send-contact-order-note-portuguese_br";i:1;s:32:"send-contact-order-note-romanian";i:1;s:31:"send-contact-order-note-russian";i:1;s:30:"send-contact-order-note-slovak";i:1;s:31:"send-contact-order-note-spanish";i:1;s:31:"send-contact-order-note-swedish";i:1;s:31:"send-contact-order-note-turkish";i:1;s:33:"send-contact-order-note-ukrainian";i:1;s:34:"send-contact-order-note-vietnamese";i:1;s:29:"boq-client-accepted-bulgarian";i:1;s:30:"boq-send-to-customer-bulgarian";i:1;s:29:"boq-client-declined-bulgarian";i:1;s:30:"boq-client-thank-you-bulgarian";i:1;s:31:"boq-comment-to-client-bulgarian";i:1;s:30:"boq-comment-to-admin-bulgarian";i:1;s:29:"boq-expiry-reminder-bulgarian";i:1;s:27:"boq-client-accepted-catalan";i:1;s:28:"boq-send-to-customer-catalan";i:1;s:27:"boq-client-declined-catalan";i:1;s:28:"boq-client-thank-you-catalan";i:1;s:29:"boq-comment-to-client-catalan";i:1;s:28:"boq-comment-to-admin-catalan";i:1;s:27:"boq-expiry-reminder-catalan";i:1;s:27:"boq-client-accepted-chinese";i:1;s:28:"boq-send-to-customer-chinese";i:1;s:27:"boq-client-declined-chinese";i:1;s:28:"boq-client-thank-you-chinese";i:1;s:29:"boq-comment-to-client-chinese";i:1;s:28:"boq-comment-to-admin-chinese";i:1;s:27:"boq-expiry-reminder-chinese";i:1;s:25:"boq-client-accepted-czech";i:1;s:26:"boq-send-to-customer-czech";i:1;s:25:"boq-client-declined-czech";i:1;s:26:"boq-client-thank-you-czech";i:1;s:27:"boq-comment-to-client-czech";i:1;s:26:"boq-comment-to-admin-czech";i:1;s:25:"boq-expiry-reminder-czech";i:1;s:25:"boq-client-accepted-dutch";i:1;s:26:"boq-send-to-customer-dutch";i:1;s:25:"boq-client-declined-dutch";i:1;s:26:"boq-client-thank-you-dutch";i:1;s:27:"boq-comment-to-client-dutch";i:1;s:26:"boq-comment-to-admin-dutch";i:1;s:25:"boq-expiry-reminder-dutch";i:1;s:26:"boq-client-accepted-french";i:1;s:27:"boq-send-to-customer-french";i:1;s:26:"boq-client-declined-french";i:1;s:27:"boq-client-thank-you-french";i:1;s:28:"boq-comment-to-client-french";i:1;s:27:"boq-comment-to-admin-french";i:1;s:26:"boq-expiry-reminder-french";i:1;s:26:"boq-client-accepted-german";i:1;s:27:"boq-send-to-customer-german";i:1;s:26:"boq-client-declined-german";i:1;s:27:"boq-client-thank-you-german";i:1;s:28:"boq-comment-to-client-german";i:1;s:27:"boq-comment-to-admin-german";i:1;s:26:"boq-expiry-reminder-german";i:1;s:25:"boq-client-accepted-greek";i:1;s:26:"boq-send-to-customer-greek";i:1;s:25:"boq-client-declined-greek";i:1;s:26:"boq-client-thank-you-greek";i:1;s:27:"boq-comment-to-client-greek";i:1;s:26:"boq-comment-to-admin-greek";i:1;s:25:"boq-expiry-reminder-greek";i:1;s:29:"boq-client-accepted-indonesia";i:1;s:30:"boq-send-to-customer-indonesia";i:1;s:29:"boq-client-declined-indonesia";i:1;s:30:"boq-client-thank-you-indonesia";i:1;s:31:"boq-comment-to-client-indonesia";i:1;s:30:"boq-comment-to-admin-indonesia";i:1;s:29:"boq-expiry-reminder-indonesia";i:1;s:27:"boq-client-accepted-italian";i:1;s:28:"boq-send-to-customer-italian";i:1;s:27:"boq-client-declined-italian";i:1;s:28:"boq-client-thank-you-italian";i:1;s:29:"boq-comment-to-client-italian";i:1;s:28:"boq-comment-to-admin-italian";i:1;s:27:"boq-expiry-reminder-italian";i:1;s:28:"boq-client-accepted-japanese";i:1;s:29:"boq-send-to-customer-japanese";i:1;s:28:"boq-client-declined-japanese";i:1;s:29:"boq-client-thank-you-japanese";i:1;s:30:"boq-comment-to-client-japanese";i:1;s:29:"boq-comment-to-admin-japanese";i:1;s:28:"boq-expiry-reminder-japanese";i:1;s:27:"boq-client-accepted-persian";i:1;s:28:"boq-send-to-customer-persian";i:1;s:27:"boq-client-declined-persian";i:1;s:28:"boq-client-thank-you-persian";i:1;s:29:"boq-comment-to-client-persian";i:1;s:28:"boq-comment-to-admin-persian";i:1;s:27:"boq-expiry-reminder-persian";i:1;s:26:"boq-client-accepted-polish";i:1;s:27:"boq-send-to-customer-polish";i:1;s:26:"boq-client-declined-polish";i:1;s:27:"boq-client-thank-you-polish";i:1;s:28:"boq-comment-to-client-polish";i:1;s:27:"boq-comment-to-admin-polish";i:1;s:26:"boq-expiry-reminder-polish";i:1;s:30:"boq-client-accepted-portuguese";i:1;s:31:"boq-send-to-customer-portuguese";i:1;s:30:"boq-client-declined-portuguese";i:1;s:31:"boq-client-thank-you-portuguese";i:1;s:32:"boq-comment-to-client-portuguese";i:1;s:31:"boq-comment-to-admin-portuguese";i:1;s:30:"boq-expiry-reminder-portuguese";i:1;s:33:"boq-client-accepted-portuguese_br";i:1;s:34:"boq-send-to-customer-portuguese_br";i:1;s:33:"boq-client-declined-portuguese_br";i:1;s:34:"boq-client-thank-you-portuguese_br";i:1;s:35:"boq-comment-to-client-portuguese_br";i:1;s:34:"boq-comment-to-admin-portuguese_br";i:1;s:33:"boq-expiry-reminder-portuguese_br";i:1;s:28:"boq-client-accepted-romanian";i:1;s:29:"boq-send-to-customer-romanian";i:1;s:28:"boq-client-declined-romanian";i:1;s:29:"boq-client-thank-you-romanian";i:1;s:30:"boq-comment-to-client-romanian";i:1;s:29:"boq-comment-to-admin-romanian";i:1;s:28:"boq-expiry-reminder-romanian";i:1;s:27:"boq-client-accepted-russian";i:1;s:28:"boq-send-to-customer-russian";i:1;s:27:"boq-client-declined-russian";i:1;s:28:"boq-client-thank-you-russian";i:1;s:29:"boq-comment-to-client-russian";i:1;s:28:"boq-comment-to-admin-russian";i:1;s:27:"boq-expiry-reminder-russian";i:1;s:26:"boq-client-accepted-slovak";i:1;s:27:"boq-send-to-customer-slovak";i:1;s:26:"boq-client-declined-slovak";i:1;s:27:"boq-client-thank-you-slovak";i:1;s:28:"boq-comment-to-client-slovak";i:1;s:27:"boq-comment-to-admin-slovak";i:1;s:26:"boq-expiry-reminder-slovak";i:1;s:27:"boq-client-accepted-spanish";i:1;s:28:"boq-send-to-customer-spanish";i:1;s:27:"boq-client-declined-spanish";i:1;s:28:"boq-client-thank-you-spanish";i:1;s:29:"boq-comment-to-client-spanish";i:1;s:28:"boq-comment-to-admin-spanish";i:1;s:27:"boq-expiry-reminder-spanish";i:1;s:27:"boq-client-accepted-swedish";i:1;s:28:"boq-send-to-customer-swedish";i:1;s:27:"boq-client-declined-swedish";i:1;s:28:"boq-client-thank-you-swedish";i:1;s:29:"boq-comment-to-client-swedish";i:1;s:28:"boq-comment-to-admin-swedish";i:1;s:27:"boq-expiry-reminder-swedish";i:1;s:27:"boq-client-accepted-turkish";i:1;s:28:"boq-send-to-customer-turkish";i:1;s:27:"boq-client-declined-turkish";i:1;s:28:"boq-client-thank-you-turkish";i:1;s:29:"boq-comment-to-client-turkish";i:1;s:28:"boq-comment-to-admin-turkish";i:1;s:27:"boq-expiry-reminder-turkish";i:1;s:29:"boq-client-accepted-ukrainian";i:1;s:30:"boq-send-to-customer-ukrainian";i:1;s:29:"boq-client-declined-ukrainian";i:1;s:30:"boq-client-thank-you-ukrainian";i:1;s:31:"boq-comment-to-client-ukrainian";i:1;s:30:"boq-comment-to-admin-ukrainian";i:1;s:29:"boq-expiry-reminder-ukrainian";i:1;s:30:"boq-client-accepted-vietnamese";i:1;s:31:"boq-send-to-customer-vietnamese";i:1;s:30:"boq-client-declined-vietnamese";i:1;s:31:"boq-client-thank-you-vietnamese";i:1;s:32:"boq-comment-to-client-vietnamese";i:1;s:31:"boq-comment-to-admin-vietnamese";i:1;s:30:"boq-expiry-reminder-vietnamese";i:1;}', 0),
	(213, 'proposal_accept_identity_confirmation', '1', 0),
	(214, 'estimate_accept_identity_confirmation', '1', 0),
	(215, 'new_task_auto_follower_current_member', '0', 1),
	(216, 'task_biillable_checked_on_creation', '1', 1),
	(217, 'predefined_clientnote_credit_note', '', 1),
	(218, 'predefined_terms_credit_note', '', 1),
	(219, 'next_credit_note_number', '1', 1),
	(220, 'credit_note_prefix', 'CN-', 1),
	(221, 'credit_note_number_decrement_on_delete', '1', 1),
	(222, 'pdf_format_credit_note', 'A4-PORTRAIT', 1),
	(223, 'show_pdf_signature_credit_note', '1', 0),
	(224, 'show_credit_note_reminders_on_calendar', '1', 1),
	(225, 'show_amount_due_on_invoice', '1', 1),
	(226, 'show_total_paid_on_invoice', '1', 1),
	(227, 'show_credits_applied_on_invoice', '1', 1),
	(228, 'staff_members_create_inline_lead_status', '1', 1),
	(229, 'staff_members_create_inline_customer_groups', '1', 1),
	(230, 'staff_members_create_inline_ticket_services', '1', 1),
	(231, 'staff_members_save_tickets_predefined_replies', '1', 1),
	(232, 'staff_members_create_inline_contract_types', '1', 1),
	(233, 'staff_members_create_inline_expense_categories', '1', 1),
	(234, 'show_project_on_credit_note', '1', 1),
	(235, 'proposals_auto_operations_hour', '21', 1),
	(236, 'estimates_auto_operations_hour', '21', 1),
	(237, 'contracts_auto_operations_hour', '21', 1),
	(238, 'credit_note_number_format', '1', 1),
	(239, 'allow_non_admin_members_to_import_leads', '0', 1),
	(240, 'e_sign_legal_text', 'By clicking on "Sign", I consent to be legally bound by this electronic representation of my signature.', 1),
	(241, 'show_pdf_signature_contract', '1', 1),
	(242, 'view_contract_only_logged_in', '0', 1),
	(243, 'show_subscriptions_in_customers_area', '1', 1),
	(244, 'calendar_only_assigned_tasks', '0', 1),
	(245, 'after_subscription_payment_captured', 'send_invoice_and_receipt', 1),
	(246, 'mail_engine', 'phpmailer', 1),
	(247, 'gdpr_enable_terms_and_conditions', '0', 1),
	(248, 'privacy_policy', '', 1),
	(249, 'terms_and_conditions', '', 1),
	(250, 'gdpr_enable_terms_and_conditions_lead_form', '0', 1),
	(251, 'gdpr_enable_terms_and_conditions_ticket_form', '0', 1),
	(252, 'gdpr_contact_enable_right_to_be_forgotten', '0', 1),
	(253, 'show_gdpr_in_customers_menu', '1', 1),
	(254, 'show_gdpr_link_in_footer', '1', 1),
	(255, 'enable_gdpr', '0', 1),
	(256, 'gdpr_on_forgotten_remove_invoices_credit_notes', '0', 1),
	(257, 'gdpr_on_forgotten_remove_estimates', '0', 1),
	(258, 'gdpr_enable_consent_for_contacts', '0', 1),
	(259, 'gdpr_consent_public_page_top_block', '', 1),
	(260, 'gdpr_page_top_information_block', '', 1),
	(261, 'gdpr_enable_lead_public_form', '0', 1),
	(262, 'gdpr_show_lead_custom_fields_on_public_form', '0', 1),
	(263, 'gdpr_lead_attachments_on_public_form', '0', 1),
	(264, 'gdpr_enable_consent_for_leads', '0', 1),
	(265, 'gdpr_lead_enable_right_to_be_forgotten', '0', 1),
	(266, 'allow_staff_view_invoices_assigned', '1', 1),
	(267, 'gdpr_data_portability_leads', '0', 1),
	(268, 'gdpr_lead_data_portability_allowed', '', 1),
	(269, 'gdpr_contact_data_portability_allowed', '', 1),
	(270, 'gdpr_data_portability_contacts', '0', 1),
	(271, 'allow_staff_view_estimates_assigned', '1', 1),
	(272, 'gdpr_after_lead_converted_delete', '0', 1),
	(273, 'gdpr_show_terms_and_conditions_in_footer', '0', 1),
	(274, 'save_last_order_for_tables', '0', 1),
	(275, 'company_logo_dark', '', 1),
	(276, 'customers_register_require_confirmation', '0', 1),
	(277, 'allow_non_admin_staff_to_delete_ticket_attachments', '0', 1),
	(278, 'receive_notification_on_new_ticket_replies', '1', 0),
	(279, 'google_client_id', '', 1),
	(280, 'enable_google_picker', '1', 1),
	(281, 'show_ticket_reminders_on_calendar', '1', 1),
	(282, 'ticket_import_reply_only', '0', 1),
	(283, 'visible_customer_profile_tabs', 'all', 0),
	(284, 'show_project_on_invoice', '1', 1),
	(285, 'show_project_on_estimate', '1', 1),
	(286, 'staff_members_create_inline_lead_source', '1', 1),
	(287, 'lead_unique_validation', '["email"]', 1),
	(288, 'last_upgrade_copy_data', '', 1),
	(289, 'custom_js_admin_scripts', '', 1),
	(290, 'custom_js_customer_scripts', '0', 1),
	(291, 'stripe_webhook_id', '', 1),
	(292, 'stripe_webhook_signing_secret', '', 1),
	(293, 'stripe_ideal_webhook_id', '', 1),
	(294, 'stripe_ideal_webhook_signing_secret', '', 1),
	(295, 'show_php_version_notice', '1', 0),
	(296, 'recaptcha_ignore_ips', '', 1),
	(297, 'show_task_reminders_on_calendar', '1', 1),
	(298, 'customer_settings', 'true', 1),
	(299, 'tasks_reminder_notification_hour', '21', 1),
	(300, 'allow_primary_contact_to_manage_other_contacts', '0', 1),
	(301, 'items_table_amounts_exclude_currency_symbol', '1', 1),
	(302, 'round_off_task_timer_option', '0', 1),
	(303, 'round_off_task_timer_time', '5', 1),
	(304, 'bitly_access_token', '', 1),
	(305, 'upgraded_from_version', '', 0),
	(306, 'sms_clickatell_api_key', '', 1),
	(307, 'sms_clickatell_active', '0', 1),
	(308, 'sms_clickatell_initialized', '1', 1),
	(309, 'sms_msg91_sender_id', '', 1),
	(310, 'sms_msg91_api_type', 'api', 1),
	(311, 'sms_msg91_auth_key', '', 1),
	(312, 'sms_msg91_active', '0', 1),
	(313, 'sms_msg91_initialized', '1', 1),
	(314, 'sms_twilio_account_sid', '', 1),
	(315, 'sms_twilio_auth_token', '', 1),
	(316, 'sms_twilio_phone_number', '', 1),
	(317, 'sms_twilio_active', '0', 1),
	(318, 'sms_twilio_initialized', '1', 1),
	(319, 'paymentmethod_authorize_acceptjs_active', '0', 1),
	(320, 'paymentmethod_authorize_acceptjs_label', 'Authorize.net Accept.js', 1),
	(321, 'paymentmethod_authorize_acceptjs_public_key', '', 0),
	(322, 'paymentmethod_authorize_acceptjs_api_login_id', '', 0),
	(323, 'paymentmethod_authorize_acceptjs_api_transaction_key', '', 0),
	(324, 'paymentmethod_authorize_acceptjs_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(325, 'paymentmethod_authorize_acceptjs_currencies', 'USD', 0),
	(326, 'paymentmethod_authorize_acceptjs_test_mode_enabled', '0', 0),
	(327, 'paymentmethod_authorize_acceptjs_default_selected', '1', 1),
	(328, 'paymentmethod_authorize_acceptjs_initialized', '1', 1),
	(329, 'paymentmethod_instamojo_active', '0', 1),
	(330, 'paymentmethod_instamojo_label', 'Instamojo', 1),
	(331, 'paymentmethod_instamojo_api_key', '', 0),
	(332, 'paymentmethod_instamojo_auth_token', '', 0),
	(333, 'paymentmethod_instamojo_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(334, 'paymentmethod_instamojo_currencies', 'INR', 0),
	(335, 'paymentmethod_instamojo_test_mode_enabled', '1', 0),
	(336, 'paymentmethod_instamojo_default_selected', '1', 1),
	(337, 'paymentmethod_instamojo_initialized', '1', 1),
	(338, 'paymentmethod_mollie_active', '0', 1),
	(339, 'paymentmethod_mollie_label', 'Mollie', 1),
	(340, 'paymentmethod_mollie_api_key', '', 0),
	(341, 'paymentmethod_mollie_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(342, 'paymentmethod_mollie_currencies', 'EUR', 0),
	(343, 'paymentmethod_mollie_test_mode_enabled', '1', 0),
	(344, 'paymentmethod_mollie_default_selected', '1', 1),
	(345, 'paymentmethod_mollie_initialized', '1', 1),
	(346, 'paymentmethod_paypal_braintree_active', '0', 1),
	(347, 'paymentmethod_paypal_braintree_label', 'Braintree', 1),
	(348, 'paymentmethod_paypal_braintree_merchant_id', '', 0),
	(349, 'paymentmethod_paypal_braintree_api_public_key', '', 0),
	(350, 'paymentmethod_paypal_braintree_api_private_key', '', 0),
	(351, 'paymentmethod_paypal_braintree_currencies', 'USD', 0),
	(352, 'paymentmethod_paypal_braintree_paypal_enabled', '1', 0),
	(353, 'paymentmethod_paypal_braintree_test_mode_enabled', '1', 0),
	(354, 'paymentmethod_paypal_braintree_default_selected', '1', 1),
	(355, 'paymentmethod_paypal_braintree_initialized', '1', 1),
	(356, 'paymentmethod_paypal_checkout_active', '0', 1),
	(357, 'paymentmethod_paypal_checkout_label', 'Paypal Smart Checkout', 1),
	(358, 'paymentmethod_paypal_checkout_client_id', '', 0),
	(359, 'paymentmethod_paypal_checkout_secret', '', 0),
	(360, 'paymentmethod_paypal_checkout_payment_description', 'Payment for Invoice {invoice_number}', 0),
	(361, 'paymentmethod_paypal_checkout_currencies', 'USD,CAD,EUR', 0),
	(362, 'paymentmethod_paypal_checkout_test_mode_enabled', '1', 0),
	(363, 'paymentmethod_paypal_checkout_default_selected', '1', 1),
	(364, 'paymentmethod_paypal_checkout_initialized', '1', 1),
	(365, 'paymentmethod_paypal_active', '0', 1),
	(366, 'paymentmethod_paypal_label', 'Paypal', 1),
	(367, 'paymentmethod_paypal_username', '', 0),
	(368, 'paymentmethod_paypal_password', '', 0),
	(369, 'paymentmethod_paypal_signature', '', 0),
	(370, 'paymentmethod_paypal_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(371, 'paymentmethod_paypal_currencies', 'EUR,USD', 0),
	(372, 'paymentmethod_paypal_test_mode_enabled', '1', 0),
	(373, 'paymentmethod_paypal_default_selected', '1', 1),
	(374, 'paymentmethod_paypal_initialized', '1', 1),
	(375, 'paymentmethod_payu_money_active', '0', 1),
	(376, 'paymentmethod_payu_money_label', 'PayU Money', 1),
	(377, 'paymentmethod_payu_money_key', '', 0),
	(378, 'paymentmethod_payu_money_salt', '', 0),
	(379, 'paymentmethod_payu_money_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(380, 'paymentmethod_payu_money_currencies', 'INR', 0),
	(381, 'paymentmethod_payu_money_test_mode_enabled', '1', 0),
	(382, 'paymentmethod_payu_money_default_selected', '1', 1),
	(383, 'paymentmethod_payu_money_initialized', '1', 1),
	(384, 'paymentmethod_stripe_active', '0', 1),
	(385, 'paymentmethod_stripe_label', 'Stripe Checkout', 1),
	(386, 'paymentmethod_stripe_api_secret_key', '', 0),
	(387, 'paymentmethod_stripe_api_publishable_key', '', 0),
	(388, 'paymentmethod_stripe_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(389, 'paymentmethod_stripe_currencies', 'USD,CAD', 0),
	(390, 'paymentmethod_stripe_allow_primary_contact_to_update_credit_card', '1', 0),
	(391, 'paymentmethod_stripe_default_selected', '1', 1),
	(392, 'paymentmethod_stripe_initialized', '1', 1),
	(393, 'paymentmethod_stripe_ideal_active', '0', 1),
	(394, 'paymentmethod_stripe_ideal_label', 'Stripe iDEAL', 1),
	(395, 'paymentmethod_stripe_ideal_api_secret_key', '', 0),
	(396, 'paymentmethod_stripe_ideal_api_publishable_key', '', 0),
	(397, 'paymentmethod_stripe_ideal_description_dashboard', 'Payment for Invoice {invoice_number}', 0),
	(398, 'paymentmethod_stripe_ideal_statement_descriptor', 'Payment for Invoice {invoice_number}', 0),
	(399, 'paymentmethod_stripe_ideal_currencies', 'EUR', 0),
	(400, 'paymentmethod_stripe_ideal_default_selected', '1', 1),
	(401, 'paymentmethod_stripe_ideal_initialized', '1', 1),
	(402, 'paymentmethod_two_checkout_active', '0', 1),
	(403, 'paymentmethod_two_checkout_label', '2Checkout', 1),
	(404, 'paymentmethod_two_checkout_merchant_code', '', 0),
	(405, 'paymentmethod_two_checkout_secret_key', '', 0),
	(406, 'paymentmethod_two_checkout_description', 'Payment for Invoice {invoice_number}', 0),
	(407, 'paymentmethod_two_checkout_currencies', 'USD, EUR, GBP', 0),
	(408, 'paymentmethod_two_checkout_test_mode_enabled', '1', 0),
	(409, 'paymentmethod_two_checkout_default_selected', '1', 1),
	(410, 'paymentmethod_two_checkout_initialized', '1', 1),
	(411, 'aside_menu_active', '{"dashboard":{"id":"dashboard","icon":"","disabled":"false","position":1},"customers":{"id":"customers","icon":"","disabled":"false","position":"10"},"mailbox":{"id":"mailbox","icon":"","disabled":"false","position":"15"},"incident":{"id":"incident","icon":"","disabled":"false","position":"20"},"service_request":{"id":"service_request","icon":"","disabled":"false","position":"25"},"assets":{"id":"assets","icon":"","disabled":"false","position":"30"},"projects":{"id":"projects","icon":"","disabled":"false","position":"35"},"tasks":{"id":"tasks","icon":"","disabled":"false","position":"40"},"subscriptions":{"id":"subscriptions","icon":"","disabled":"false","position":"45"},"expenses":{"id":"expenses","icon":"","disabled":"false","position":"50"},"contracts":{"id":"contracts","icon":"","disabled":"false","position":"55"},"leads":{"id":"leads","icon":"","disabled":"false","position":"60"},"knowledge-base":{"id":"knowledge-base","icon":"","disabled":"false","position":"65"},"reports":{"id":"reports","icon":"","disabled":"false","position":"70","children":{"sales-reports":{"disabled":"false","id":"sales-reports","icon":"","position":"5"},"expenses-reports":{"disabled":"false","id":"expenses-reports","icon":"","position":"10"},"expenses-vs-income-reports":{"disabled":"false","id":"expenses-vs-income-reports","icon":"","position":"15"},"leads-reports":{"disabled":"false","id":"leads-reports","icon":"","position":"20"},"timesheets-reports":{"disabled":"false","id":"timesheets-reports","icon":"","position":"25"},"knowledge-base-reports":{"disabled":"false","id":"knowledge-base-reports","icon":"","position":"30"}}}}', 1),
	(412, 'setup_menu_active', '[]', 1),
	(413, 'mailbox_enabled', '1', 1),
	(414, 'mailbox_imap_server', 'leometric.com', 1),
	(415, 'mailbox_encryption', 'ssl', 1),
	(416, 'mailbox_folder_scan', 'Inbox', 1),
	(417, 'mailbox_check_every', '3', 1),
	(418, 'mailbox_only_loop_on_unseen_emails', '1', 1),
	(419, 'theme_style', '[{"id":"admin-menu","color":"#262761"},{"id":"user-welcome-text-color","color":"#ffffff"},{"id":"top-header","color":"#ffffff"},{"id":"top-header-links","color":"#262761"},{"id":"customers-navigation","color":"#ffffff"},{"id":"admin-menu","color":"#262761"},{"id":"customers-navigation-links","color":"#000000"},{"id":"btn-info","color":"#262761"}]', 1),
	(420, 'theme_style_custom_admin_area', '', 1),
	(421, 'theme_style_custom_clients_area', '', 1),
	(422, 'theme_style_custom_clients_and_admin_area', '', 1),
	(423, 'chat_url', 'https://connect.decodingit.com:1443/callus/#powermyit', 1);
/*!40000 ALTER TABLE `tbloptions` ENABLE KEYS */;

-- Dumping structure for table crm.tblorder_status
CREATE TABLE IF NOT EXISTS `tblorder_status` (
  `statusid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `statuscolor` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`statusid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblorder_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblorder_status` DISABLE KEYS */;
INSERT INTO `tblorder_status` (`statusid`, `name`, `statuscolor`) VALUES
	(1, 'Placed', '#107935'),
	(2, 'Shipped', '#1563e1');
/*!40000 ALTER TABLE `tblorder_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblorder_tracker
CREATE TABLE IF NOT EXISTS `tblorder_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `so_number` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `contact_id` text,
  `customer_name` varchar(255) DEFAULT NULL,
  `lpo_no` varchar(100) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `po_no` varchar(100) DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `items_received` varchar(20) DEFAULT NULL,
  `expected_delivery` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `items_delivered` varchar(20) DEFAULT NULL,
  `bill_created` varchar(20) DEFAULT NULL,
  `pi_attached` varchar(20) DEFAULT NULL,
  `service_request` varchar(20) DEFAULT NULL,
  `sr_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `order_completed` varchar(20) DEFAULT NULL,
  `renewal_required` varchar(20) DEFAULT NULL,
  `renewal_db_updated` varchar(20) DEFAULT NULL,
  `sales_staff_id` int(11) DEFAULT NULL,
  `invoice_submited` varchar(20) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `amount` float(12,2) DEFAULT NULL,
  `inv_submission_date` date DEFAULT NULL,
  `invoice_acknowledge` varchar(10) DEFAULT NULL,
  `payment_received` varchar(10) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_received_in` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblorder_tracker: ~7 rows (approximately)
/*!40000 ALTER TABLE `tblorder_tracker` DISABLE KEYS */;
INSERT INTO `tblorder_tracker` (`id`, `so_number`, `description`, `client_id`, `contact_id`, `customer_name`, `lpo_no`, `order_date`, `po_no`, `vendor_name`, `items_received`, `expected_delivery`, `delivery_date`, `items_delivered`, `bill_created`, `pi_attached`, `service_request`, `sr_id`, `project_id`, `order_completed`, `renewal_required`, `renewal_db_updated`, `sales_staff_id`, `invoice_submited`, `invoice_no`, `amount`, `inv_submission_date`, `invoice_acknowledge`, `payment_received`, `payment_date`, `payment_received_in`, `status_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 'SAT-099 12', 'Testing Edited 12', 5, '3', 'Akshay', '78771', '2021-09-10', '8999', 'aDcoders', 'Yes', '2021-06-29', '2021-06-29', 'Yes', 'Yes', 'Yes', 'Project', 2, NULL, 'Yes', 'Yes', 'No', 4, 'No', '677771', 890001.00, '2021-09-09', 'Yes', 'No', '2021-09-08', '51', 1, '2021-06-29 10:15:17', NULL, '2021-11-03 16:24:04', NULL),
	(2, 'AAKR-07', 'Test Description', 5, '37', NULL, '1234567', '2021-06-15', 'PO0099', 'Leometric Technology', 'Yes', '2021-06-24', '2021-06-24', 'Yes', 'Yes', 'No', 'Yes', 2, NULL, 'Yes', 'Yes', 'No', 4, 'Yes', 'INV0877', 20000.00, '2021-06-29', 'NA', NULL, '2021-06-28', '3', 2, '2021-06-29 18:52:26', NULL, '2021-09-15 10:23:57', NULL),
	(3, 'AAKR-09', 'Test Description New', 6, NULL, NULL, '1234567', '2021-06-19', 'PO0077', 'Vidhi Technologies', 'Yes', '2021-06-23', '2021-06-22', 'Yes', 'Yes', 'Yes', 'No', NULL, NULL, 'No', 'Yes', 'Yes', 4, 'Yes', 'INV0879', 34555.00, '2021-06-28', 'NA', NULL, '2021-06-27', '2', 2, '2021-06-29 19:31:05', NULL, '2021-08-26 12:53:39', NULL),
	(4, 'AAKR-47', 'Test Description New', 6, NULL, NULL, '1234567', '2021-07-19', NULL, NULL, 'Partial', '2021-07-22', '2021-07-20', 'Yes', 'Yes', 'Yes', 'Service Request', 1, NULL, 'Yes', 'Yes', 'No', 4, 'Yes', 'INV0477', 250.00, '2021-07-20', 'Yes', 'Yes', '2021-07-20', '1', 2, '2021-07-20 11:21:01', NULL, '2021-07-20 11:21:01', NULL),
	(5, 'AAKR-47', 'Testing Edited', 2, NULL, NULL, '1234567', '2021-07-20', NULL, NULL, 'Yes', '2021-07-13', '2021-07-19', 'Yes', 'Yes', 'No', 'Service Request', NULL, NULL, 'Yes', 'Yes', 'No', 4, 'Yes', 'INV0877', 20000.00, '2021-07-20', 'Yes', 'No', '2021-07-13', '3', 1, '2021-07-20 12:43:22', NULL, '2021-07-20 12:43:22', NULL),
	(6, 'AAKR-47', 'Test Description New', 1, NULL, NULL, '1234567', '2021-07-04', NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', 'Yes', 'Project', 1, NULL, 'No', 'Yes', 'Yes', 4, 'Yes', 'INV0477', 0.00, '2021-07-13', 'Yes', 'Yes', '2021-07-05', '1', 1, '2021-07-20 16:21:51', NULL, '2021-08-26 12:53:38', NULL),
	(7, 'AS-0074', 'AS47', 1, '3', NULL, '47741', '2021-09-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, '', 0.00, '1970-01-01', NULL, NULL, '1970-01-01', '', 2, '2021-09-14 16:54:24', NULL, '2021-09-14 16:55:53', NULL);
/*!40000 ALTER TABLE `tblorder_tracker` ENABLE KEYS */;

-- Dumping structure for table crm.tblorder_tracker_items
CREATE TABLE IF NOT EXISTS `tblorder_tracker_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `expected_delivery_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `item_received` varchar(10) DEFAULT NULL,
  `item_delivered` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblorder_tracker_items: ~9 rows (approximately)
/*!40000 ALTER TABLE `tblorder_tracker_items` DISABLE KEYS */;
INSERT INTO `tblorder_tracker_items` (`id`, `order_id`, `item_id`, `quantity`, `vendor_name`, `expected_delivery_date`, `delivery_date`, `item_received`, `item_delivered`, `created_at`, `created_by`) VALUES
	(9, 4, 1, 7, 'Vidhi', '2021-07-22', '2021-07-20', 'Yes', 'Yes', '2021-07-20 11:21:01', NULL),
	(25, 5, 1, 35, 'Vidhi', '2021-07-20', '2021-07-18', 'Yes', 'No', '2021-07-20 16:19:13', NULL),
	(26, 5, 2, 55, 'Darshana', '2021-07-20', '2021-07-18', 'No', 'No', '2021-07-20 16:19:13', NULL),
	(27, 5, 1, 67, 'Akshay', '2021-07-20', '2021-07-20', 'No', 'No', '2021-07-20 16:19:13', NULL),
	(28, 6, 1, 7, 'Vidhi', '2021-07-21', '2021-07-18', 'Yes', 'No', '2021-07-20 16:21:51', NULL),
	(29, 6, 2, 34, 'www', '2021-07-21', '2021-07-21', 'No', 'No', '2021-07-20 16:21:51', NULL),
	(42, 1, 2, 15, 'XYZ', '2021-07-19', '2021-07-19', 'No', 'Yes', '2021-09-14 16:41:26', NULL),
	(43, 1, 1, 10, 'ABC', '2021-07-17', '2021-07-19', 'Yes', 'No', '2021-09-14 16:41:26', NULL),
	(44, 7, 1, 7, 'Vidhi', '2021-07-28', '2021-07-27', 'No', 'Yes', '2021-09-14 16:55:53', NULL),
	(45, 2, 2, 15, 'XYZ', '2021-07-19', '2021-07-19', 'No', 'Yes', '2021-09-15 10:23:57', NULL);
/*!40000 ALTER TABLE `tblorder_tracker_items` ENABLE KEYS */;

-- Dumping structure for table crm.tblorder_tracker_notes
CREATE TABLE IF NOT EXISTS `tblorder_tracker_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_note` text,
  `share_to_customer` int(11) DEFAULT '0',
  `added_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblorder_tracker_notes: ~25 rows (approximately)
/*!40000 ALTER TABLE `tblorder_tracker_notes` DISABLE KEYS */;
INSERT INTO `tblorder_tracker_notes` (`id`, `order_id`, `order_note`, `share_to_customer`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Testing', 0, NULL, NULL, '2021-07-20 13:21:49', '2021-07-20 13:41:27'),
	(2, 1, 'Test Note', 1, NULL, NULL, '2021-07-20 13:22:29', '2021-07-22 16:42:03'),
	(3, 1, 'Edited New', 0, NULL, 1, '2021-07-20 13:23:12', '2021-08-12 15:41:11'),
	(4, 6, 'test', 1, NULL, NULL, '2021-07-20 16:22:15', '2021-07-22 16:40:37'),
	(16, 1, 'Test', 1, NULL, NULL, '2021-08-09 12:03:53', '2021-08-09 12:03:53'),
	(17, 1, 'Testing', 1, NULL, NULL, '2021-08-09 12:05:31', '2021-08-09 12:05:31'),
	(18, 1, 'Testing', 1, NULL, NULL, '2021-08-09 12:07:05', '2021-08-09 12:07:05'),
	(19, 1, 'Testing', 1, NULL, NULL, '2021-08-09 12:21:21', '2021-08-09 12:21:21'),
	(20, 1, 'Testing mail', 1, NULL, NULL, '2021-08-09 12:22:19', '2021-08-09 12:22:19'),
	(21, 1, 'Testing Note', 1, NULL, NULL, '2021-08-09 12:27:02', '2021-08-09 12:27:02'),
	(22, 1, 'Mail Test', 1, NULL, NULL, '2021-08-09 12:28:00', '2021-08-09 12:28:00'),
	(23, 1, 'sjadhjs', 1, NULL, NULL, '2021-08-09 12:28:56', '2021-08-09 12:28:56'),
	(24, 1, 'sdkjs', 1, NULL, NULL, '2021-08-09 12:29:26', '2021-08-09 12:29:26'),
	(25, 1, 'akskjk', 1, NULL, NULL, '2021-08-09 12:30:41', '2021-08-09 12:30:41'),
	(26, 1, 'noted', 1, NULL, NULL, '2021-08-09 16:40:47', '2021-08-09 16:40:47'),
	(27, 1, 'Tets', 1, NULL, NULL, '2021-08-09 16:45:28', '2021-08-09 16:45:28'),
	(28, 1, 'yysys', 1, NULL, NULL, '2021-08-09 16:46:27', '2021-08-09 16:46:27'),
	(29, 1, 'sdks', 1, NULL, NULL, '2021-08-09 16:47:44', '2021-08-09 16:47:44'),
	(30, 1, 'sdks', 1, NULL, NULL, '2021-08-09 16:48:11', '2021-08-09 16:48:11'),
	(31, 1, 'skjsj', 1, NULL, NULL, '2021-08-09 16:49:21', '2021-08-09 16:49:21'),
	(32, 1, 'New Note', 0, 1, NULL, '2021-08-12 15:41:44', '2021-08-12 15:41:44'),
	(33, 1, 'Testing', 1, 1, NULL, '2021-09-14 16:23:07', '2021-09-14 16:23:07'),
	(34, 1, 'Test', 1, 1, NULL, '2021-09-14 16:25:06', '2021-09-14 16:25:06'),
	(35, 1, 'ghjkhjk', 1, 1, NULL, '2021-09-14 16:31:26', '2021-09-14 16:31:26'),
	(36, 1, 'Testing of Note', 1, 1, NULL, '2021-09-14 16:41:57', '2021-09-14 16:41:57'),
	(37, 2, 'Testing', 1, 1, NULL, '2021-09-15 10:24:52', '2021-09-15 10:24:52');
/*!40000 ALTER TABLE `tblorder_tracker_notes` ENABLE KEYS */;

-- Dumping structure for table crm.tblorder_tracking_status
CREATE TABLE IF NOT EXISTS `tblorder_tracking_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblorder_tracking_status: ~24 rows (approximately)
/*!40000 ALTER TABLE `tblorder_tracking_status` DISABLE KEYS */;
INSERT INTO `tblorder_tracking_status` (`id`, `order_id`, `status_id`, `created_at`, `created_by`) VALUES
	(1, 3, 1, '2021-06-29 19:31:05', NULL),
	(2, 3, 2, '2021-06-29 19:32:01', NULL),
	(3, 1, 2, '2021-07-20 10:52:07', NULL),
	(4, 1, 2, '2021-07-20 11:03:08', NULL),
	(5, 1, 2, '2021-07-20 11:11:03', NULL),
	(6, 1, 2, '2021-07-20 11:12:10', NULL),
	(7, 4, 2, '2021-07-20 11:21:01', NULL),
	(8, 5, 1, '2021-07-20 12:43:22', NULL),
	(9, 5, 1, '2021-07-20 12:47:04', NULL),
	(10, 5, 1, '2021-07-20 12:47:40', NULL),
	(11, 5, 1, '2021-07-20 16:16:55', NULL),
	(12, 5, 1, '2021-07-20 16:19:13', NULL),
	(13, 6, 1, '2021-07-20 16:21:51', NULL),
	(14, 7, 1, '2021-07-26 10:34:14', NULL),
	(15, 7, 1, '2021-07-26 16:11:59', NULL),
	(16, 1, 2, '2021-08-09 12:02:22', NULL),
	(17, 8, 1, '2021-08-26 15:20:05', NULL),
	(18, 1, 1, '2021-09-10 10:44:05', NULL),
	(19, 1, 1, '2021-09-10 10:56:34', NULL),
	(20, 1, 1, '2021-09-10 13:10:26', NULL),
	(21, 1, 1, '2021-09-10 16:01:10', NULL),
	(22, 1, 1, '2021-09-14 16:41:26', NULL),
	(23, 7, 1, '2021-09-14 16:54:25', NULL),
	(24, 7, 2, '2021-09-14 16:55:53', NULL),
	(25, 2, 2, '2021-09-15 10:23:57', NULL);
/*!40000 ALTER TABLE `tblorder_tracking_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblpayment_modes
CREATE TABLE IF NOT EXISTS `tblpayment_modes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `show_on_pdf` int(11) NOT NULL DEFAULT '0',
  `invoices_only` int(11) NOT NULL DEFAULT '0',
  `expenses_only` int(11) NOT NULL DEFAULT '0',
  `selected_by_default` int(11) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblpayment_modes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblpayment_modes` DISABLE KEYS */;
INSERT INTO `tblpayment_modes` (`id`, `name`, `description`, `show_on_pdf`, `invoices_only`, `expenses_only`, `selected_by_default`, `active`) VALUES
	(1, 'Bank', NULL, 0, 0, 0, 1, 1);
/*!40000 ALTER TABLE `tblpayment_modes` ENABLE KEYS */;

-- Dumping structure for table crm.tblpinned_projects
CREATE TABLE IF NOT EXISTS `tblpinned_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblpinned_projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblpinned_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblpinned_projects` ENABLE KEYS */;

-- Dumping structure for table crm.tblprojectdiscussioncomments
CREATE TABLE IF NOT EXISTS `tblprojectdiscussioncomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `discussion_type` varchar(10) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `content` text NOT NULL,
  `staff_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT '0',
  `fullname` varchar(191) DEFAULT NULL,
  `file_name` varchar(191) DEFAULT NULL,
  `file_mime_type` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblprojectdiscussioncomments: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblprojectdiscussioncomments` DISABLE KEYS */;
INSERT INTO `tblprojectdiscussioncomments` (`id`, `discussion_id`, `discussion_type`, `parent`, `created`, `modified`, `content`, `staff_id`, `contact_id`, `fullname`, `file_name`, `file_mime_type`) VALUES
	(1, 1, 'regular', NULL, '2021-11-13 18:21:39', '2021-11-13 18:21:39', 'skdj sidhsdh', 1, 37, NULL, NULL, NULL),
	(2, 1, 'file', NULL, '2021-11-13 18:22:01', '2021-11-13 18:22:02', 'skhj sdji', 5, 37, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblprojectdiscussioncomments` ENABLE KEYS */;

-- Dumping structure for table crm.tblprojectdiscussions
CREATE TABLE IF NOT EXISTS `tblprojectdiscussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `show_to_customer` tinyint(1) NOT NULL DEFAULT '0',
  `datecreated` datetime NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `staff_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblprojectdiscussions: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblprojectdiscussions` DISABLE KEYS */;
INSERT INTO `tblprojectdiscussions` (`id`, `project_id`, `subject`, `description`, `show_to_customer`, `datecreated`, `last_activity`, `staff_id`, `contact_id`) VALUES
	(1, 5, 'Test Discussion', 'shd sdjsdjjs sjd', 1, '2021-11-13 18:12:41', '2021-11-13 18:12:43', 0, 37);
/*!40000 ALTER TABLE `tblprojectdiscussions` ENABLE KEYS */;

-- Dumping structure for table crm.tblprojects
CREATE TABLE IF NOT EXISTS `tblprojects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `clientid` int(11) NOT NULL,
  `billing_type` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `deadline` date DEFAULT NULL,
  `project_created` date NOT NULL,
  `date_finished` datetime DEFAULT NULL,
  `progress` int(11) DEFAULT '0',
  `progress_from_tasks` int(11) NOT NULL DEFAULT '1',
  `project_cost` decimal(15,2) DEFAULT NULL,
  `project_rate_per_hour` decimal(15,2) DEFAULT NULL,
  `estimated_hours` decimal(15,2) DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientid` (`clientid`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblprojects: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblprojects` DISABLE KEYS */;
INSERT INTO `tblprojects` (`id`, `name`, `description`, `status`, `clientid`, `billing_type`, `start_date`, `deadline`, `project_created`, `date_finished`, `progress`, `progress_from_tasks`, `project_cost`, `project_rate_per_hour`, `estimated_hours`, `addedfrom`) VALUES
	(1, 'Project', 'Pro', 2, 5, 1, '2021-11-20', '2021-11-28', '2021-10-27', '2021-10-27 11:43:19', 3, 1, NULL, NULL, NULL, 0),
	(2, 'New Project', 'Project', 1, 3, 2, '2021-10-27', '2021-10-27', '2021-10-27', '2021-10-27 17:40:52', 1, 1, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `tblprojects` ENABLE KEYS */;

-- Dumping structure for table crm.tblproject_activity
CREATE TABLE IF NOT EXISTS `tblproject_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL DEFAULT '0',
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `fullname` varchar(100) DEFAULT NULL,
  `visible_to_customer` int(11) NOT NULL DEFAULT '0',
  `description_key` varchar(191) NOT NULL COMMENT 'Language file key',
  `additional_data` text,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproject_activity: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblproject_activity` DISABLE KEYS */;
INSERT INTO `tblproject_activity` (`id`, `project_id`, `staff_id`, `contact_id`, `fullname`, `visible_to_customer`, `description_key`, `additional_data`, `dateadded`) VALUES
	(1, 1, 5, 0, NULL, 0, 'project_activity_new_task_comment', 'API Testing Task', '2021-11-13 13:38:54'),
	(2, 1, 0, 37, NULL, 0, 'project_activity_new_task_comment', 'API Testing Task', '2021-11-13 13:41:28');
/*!40000 ALTER TABLE `tblproject_activity` ENABLE KEYS */;

-- Dumping structure for table crm.tblproject_files
CREATE TABLE IF NOT EXISTS `tblproject_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(191) NOT NULL,
  `subject` varchar(191) DEFAULT NULL,
  `description` text,
  `filetype` varchar(50) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `visible_to_customer` tinyint(1) DEFAULT '0',
  `staffid` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `external` varchar(40) DEFAULT NULL,
  `external_link` text,
  `thumbnail_link` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproject_files: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproject_files` DISABLE KEYS */;
INSERT INTO `tblproject_files` (`id`, `file_name`, `subject`, `description`, `filetype`, `dateadded`, `last_activity`, `project_id`, `visible_to_customer`, `staffid`, `contact_id`, `external`, `external_link`, `thumbnail_link`) VALUES
	(1, 'akjak.jpg', 'dsjdls', 'kddl', 'kd/jpg', '2021-11-11 14:05:07', '2021-11-11 14:05:08', 2, 1, 0, 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblproject_files` ENABLE KEYS */;

-- Dumping structure for table crm.tblproject_members
CREATE TABLE IF NOT EXISTS `tblproject_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproject_members: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproject_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblproject_members` ENABLE KEYS */;

-- Dumping structure for table crm.tblproject_notes
CREATE TABLE IF NOT EXISTS `tblproject_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproject_notes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproject_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblproject_notes` ENABLE KEYS */;

-- Dumping structure for table crm.tblproject_settings
CREATE TABLE IF NOT EXISTS `tblproject_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproject_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproject_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblproject_settings` ENABLE KEYS */;

-- Dumping structure for table crm.tblproposals
CREATE TABLE IF NOT EXISTS `tblproposals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) DEFAULT NULL,
  `content` longtext,
  `addedfrom` int(11) NOT NULL,
  `datecreated` datetime NOT NULL,
  `total` decimal(15,2) DEFAULT NULL,
  `subtotal` decimal(15,2) NOT NULL,
  `total_tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `adjustment` decimal(15,2) DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL,
  `discount_total` decimal(15,2) NOT NULL,
  `discount_type` varchar(30) DEFAULT NULL,
  `show_quantity_as` int(11) NOT NULL DEFAULT '1',
  `currency` int(11) NOT NULL,
  `open_till` date DEFAULT NULL,
  `date` date NOT NULL,
  `rel_id` int(11) DEFAULT NULL,
  `rel_type` varchar(40) DEFAULT NULL,
  `assigned` int(11) DEFAULT NULL,
  `hash` varchar(32) NOT NULL,
  `proposal_to` varchar(191) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '0',
  `zip` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `allow_comments` tinyint(1) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL,
  `estimate_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `date_converted` datetime DEFAULT NULL,
  `pipeline_order` int(11) NOT NULL DEFAULT '0',
  `is_expiry_notified` int(11) NOT NULL DEFAULT '0',
  `acceptance_firstname` varchar(50) DEFAULT NULL,
  `acceptance_lastname` varchar(50) DEFAULT NULL,
  `acceptance_email` varchar(100) DEFAULT NULL,
  `acceptance_date` datetime DEFAULT NULL,
  `acceptance_ip` varchar(40) DEFAULT NULL,
  `signature` varchar(40) DEFAULT NULL,
  `short_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproposals: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproposals` DISABLE KEYS */;
INSERT INTO `tblproposals` (`id`, `subject`, `content`, `addedfrom`, `datecreated`, `total`, `subtotal`, `total_tax`, `adjustment`, `discount_percent`, `discount_total`, `discount_type`, `show_quantity_as`, `currency`, `open_till`, `date`, `rel_id`, `rel_type`, `assigned`, `hash`, `proposal_to`, `country`, `zip`, `state`, `city`, `address`, `email`, `phone`, `allow_comments`, `status`, `estimate_id`, `invoice_id`, `date_converted`, `pipeline_order`, `is_expiry_notified`, `acceptance_firstname`, `acceptance_lastname`, `acceptance_email`, `acceptance_date`, `acceptance_ip`, `signature`, `short_link`) VALUES
	(1, 'Work Report : 05/06/2020', '{proposal_items}', 1, '2021-08-02 11:33:03', 300.00, 300.00, 0.00, 0.00, 0.00, 0.00, '', 1, 1, '2021-08-09', '2021-08-02', 5, 'customer', 1, '3fa88037bd59fe9eab892ef959e8eb3e', 'Akshay B Dange', 102, '422102', 'Maharashtra', 'Nashik', 'Nashik', 'akshay.dange74@gmail.com', '09405310077', 1, 6, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblproposals` ENABLE KEYS */;

-- Dumping structure for table crm.tblproposal_comments
CREATE TABLE IF NOT EXISTS `tblproposal_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` mediumtext,
  `proposalid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblproposal_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblproposal_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblproposal_comments` ENABLE KEYS */;

-- Dumping structure for table crm.tblrelated_items
CREATE TABLE IF NOT EXISTS `tblrelated_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblrelated_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblrelated_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblrelated_items` ENABLE KEYS */;

-- Dumping structure for table crm.tblreminders
CREATE TABLE IF NOT EXISTS `tblreminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `date` datetime NOT NULL,
  `isnotified` int(11) NOT NULL DEFAULT '0',
  `rel_id` int(11) NOT NULL,
  `staff` int(11) NOT NULL,
  `rel_type` varchar(40) NOT NULL,
  `notify_by_email` int(11) NOT NULL DEFAULT '1',
  `creator` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`),
  KEY `staff` (`staff`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblreminders: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblreminders` DISABLE KEYS */;
INSERT INTO `tblreminders` (`id`, `description`, `date`, `isnotified`, `rel_id`, `staff`, `rel_type`, `notify_by_email`, `creator`) VALUES
	(1, 'Test', '2021-03-13 12:07:00', 1, 2, 10, 'customer', 0, 1),
	(2, 'Testing', '2021-08-03 11:35:00', 0, 1, 1, 'boq', 0, 1);
/*!40000 ALTER TABLE `tblreminders` ENABLE KEYS */;

-- Dumping structure for table crm.tblrenewals
CREATE TABLE IF NOT EXISTS `tblrenewals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `renewal_type` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` datetime NOT NULL,
  `vendor` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `notify_to` varchar(50) NOT NULL,
  `already_notify` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblrenewals: ~1 rows (approximately)
/*!40000 ALTER TABLE `tblrenewals` DISABLE KEYS */;
INSERT INTO `tblrenewals` (`id`, `clientid`, `name`, `renewal_type`, `start_date`, `end_date`, `vendor`, `note`, `notify_to`, `already_notify`, `created_at`, `updated_at`) VALUES
	(1, 5, 'Akshay B Dange', 1, '2021-04-09', '2021-04-09 00:00:00', 'Sudarshan', '                            Testing                          ', 'test@mail.com', 0, '2021-04-09 14:30:33', '2021-10-28 13:57:37');
/*!40000 ALTER TABLE `tblrenewals` ENABLE KEYS */;

-- Dumping structure for table crm.tblrenewal_types
CREATE TABLE IF NOT EXISTS `tblrenewal_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblrenewal_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblrenewal_types` DISABLE KEYS */;
INSERT INTO `tblrenewal_types` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
	(1, 'Test State', '2021-04-09 14:29:28', '2021-04-09 14:29:28'),
	(2, 'Test Type', '2021-04-09 14:29:38', '2021-04-09 14:29:38');
/*!40000 ALTER TABLE `tblrenewal_types` ENABLE KEYS */;

-- Dumping structure for table crm.tblrenewal_vendors
CREATE TABLE IF NOT EXISTS `tblrenewal_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(25) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tblrenewal_vendors: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblrenewal_vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblrenewal_vendors` ENABLE KEYS */;

-- Dumping structure for table crm.tblroles
CREATE TABLE IF NOT EXISTS `tblroles` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `permissions` longtext,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblroles: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblroles` DISABLE KEYS */;
INSERT INTO `tblroles` (`roleid`, `name`, `permissions`) VALUES
	(1, 'Employee', NULL),
	(3, 'User', 'a:4:{s:9:"customers";a:2:{i:0;s:4:"view";i:1;s:6:"create";}s:7:"backups";a:2:{i:0;s:4:"view";i:1;s:6:"create";}s:8:"renewals";a:1:{i:0;s:4:"view";}s:8:"projects";a:1:{i:0;s:4:"edit";}}'),
	(6, 'Sales Manager', 'a:1:{s:5:"leads";a:2:{i:0;s:4:"view";i:1;s:6:"delete";}}');
/*!40000 ALTER TABLE `tblroles` ENABLE KEYS */;

-- Dumping structure for table crm.tblsales_activity
CREATE TABLE IF NOT EXISTS `tblsales_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_type` varchar(20) DEFAULT NULL,
  `rel_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `additional_data` text,
  `staffid` varchar(11) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblsales_activity: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblsales_activity` DISABLE KEYS */;
INSERT INTO `tblsales_activity` (`id`, `rel_type`, `rel_id`, `description`, `additional_data`, `staffid`, `full_name`, `date`) VALUES
	(1, 'estimate', 1, 'estimate_activity_created', '', '1', 'Admin Admin', '2021-08-04 11:58:30'),
	(2, 'estimate', 1, 'invoice_estimate_activity_updated_qty_item', 'a:3:{i:0;s:6:"Test 1";i:1;s:5:"47.00";i:2;s:1:"1";}', '1', 'Admin Admin', '2021-08-19 14:18:11'),
	(3, 'estimate', 2, 'estimate_activity_created', '', '1', 'Admin Admin', '2021-08-24 15:34:22');
/*!40000 ALTER TABLE `tblsales_activity` ENABLE KEYS */;

-- Dumping structure for table crm.tblscheduled_emails
CREATE TABLE IF NOT EXISTS `tblscheduled_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(15) NOT NULL,
  `scheduled_at` datetime NOT NULL,
  `contacts` varchar(197) NOT NULL,
  `cc` text,
  `attach_pdf` tinyint(1) NOT NULL DEFAULT '1',
  `template` varchar(197) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblscheduled_emails: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblscheduled_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblscheduled_emails` ENABLE KEYS */;

-- Dumping structure for table crm.tblservices
CREATE TABLE IF NOT EXISTS `tblservices` (
  `serviceid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`serviceid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservices: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblservices` DISABLE KEYS */;
INSERT INTO `tblservices` (`serviceid`, `name`) VALUES
	(1, 'Test'),
	(2, 'New Service'),
	(3, 'Old Service');
/*!40000 ALTER TABLE `tblservices` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_overview_attachments
CREATE TABLE IF NOT EXISTS `tblservice_overview_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_overviewid` int(11) NOT NULL,
  `summary` text,
  `file_name` varchar(191) DEFAULT NULL,
  `filetype` varchar(50) DEFAULT NULL,
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblservice_overview_attachments: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblservice_overview_attachments` DISABLE KEYS */;
INSERT INTO `tblservice_overview_attachments` (`id`, `service_overviewid`, `summary`, `file_name`, `filetype`, `dateadded`) VALUES
	(1, 1, 'Testing', 'PRO-000764.pdf', NULL, '2021-10-26 16:48:55'),
	(2, 1, 'Test 2', 'PRO-000764.pdf', NULL, '2021-10-27 16:25:06'),
	(3, 1, 'Testing   Overview                         ', 'NCDEX-homepage_banner__20211028.jpg', NULL, '2021-11-08 11:42:30');
/*!40000 ALTER TABLE `tblservice_overview_attachments` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_requests
CREATE TABLE IF NOT EXISTS `tblservice_requests` (
  `servicerequestpattern` varchar(10) NOT NULL DEFAULT 'SR',
  `service_requestid` int(11) NOT NULL AUTO_INCREMENT,
  `adminreplying` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `email` text,
  `name` text,
  `department` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `service` int(11) DEFAULT NULL,
  `service_requestkey` varchar(32) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `message` text,
  `admin` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `lastreply` datetime DEFAULT NULL,
  `clientread` int(11) NOT NULL DEFAULT '0',
  `adminread` int(11) NOT NULL DEFAULT '0',
  `assigned` int(11) NOT NULL DEFAULT '0',
  `is_approved` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `service_request_category` int(11) NOT NULL DEFAULT '0',
  `user_acceptance` int(11) NOT NULL DEFAULT '0',
  `worklog` int(11) NOT NULL DEFAULT '0',
  `take_approval` int(11) DEFAULT NULL,
  `approval_assigned_to_staffid` int(11) DEFAULT NULL,
  `additional_charges` int(11) DEFAULT NULL,
  `request_for` varchar(20) DEFAULT NULL,
  `delivery` varchar(20) DEFAULT NULL,
  `estimated_efforts` varchar(20) DEFAULT NULL,
  `billing_type` varchar(50) DEFAULT NULL,
  `contract` int(11) DEFAULT NULL,
  PRIMARY KEY (`service_requestid`),
  KEY `service` (`service`),
  KEY `department` (`department`),
  KEY `status` (`status`),
  KEY `userid` (`userid`),
  KEY `priority` (`priority`),
  KEY `project_id` (`project_id`),
  KEY `contactid` (`contactid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_requests: ~7 rows (approximately)
/*!40000 ALTER TABLE `tblservice_requests` DISABLE KEYS */;
INSERT INTO `tblservice_requests` (`servicerequestpattern`, `service_requestid`, `adminreplying`, `userid`, `contactid`, `email`, `name`, `department`, `priority`, `status`, `service`, `service_requestkey`, `subject`, `message`, `admin`, `date`, `project_id`, `lastreply`, `clientread`, `adminread`, `assigned`, `is_approved`, `approved_by`, `service_request_category`, `user_acceptance`, `worklog`, `take_approval`, `approval_assigned_to_staffid`, `additional_charges`, `request_for`, `delivery`, `estimated_efforts`, `billing_type`, `contract`) VALUES
	('SR', 1, 0, 1, 20, 'kshaydange@leometric.com', 'akshaydange@leometric.com dange', 1, 1, 4, 1, 'b6108e6a30a4e709e3f7b9d57b8e01eb', 'Test SR', 'Tetsing mail', 1, '2021-03-18 16:04:03', 0, NULL, 1, 1, 1, NULL, NULL, 0, 0, 0, 1, 20, NULL, '3', '2', '30 Mins', 'contract', 1),
	('SR', 2, 0, 5, 37, 'akshay.dange74@gmail.com', 'Akshay B Dange', 1, 2, 2, 1, '366cd4144af1dede81a87ca05c472c2d', 'Work Report : 05/06/2020', 'testing', 1, '2021-04-24 15:40:58', 0, NULL, 1, 1, 5, NULL, NULL, 0, 0, 0, 1, 3, 2, '1', '1', '30 Mins', 'free', 1),
	('SR', 3, 0, 5, 37, NULL, NULL, 0, 0, 1, 2, '38be3915755b36bee960c589aab2c8ba', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 16:00:20', 0, '2021-11-11 12:53:55', 1, 0, 0, 0, NULL, 0, 0, 0, 1, 37, NULL, NULL, NULL, NULL, NULL, NULL),
	('SR', 4, 0, 5, 37, NULL, NULL, 0, 0, 1, 2, '5332a97da77e1982ddc5b84b616962a3', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 16:03:13', 0, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 37, NULL, NULL, NULL, NULL, NULL, NULL),
	('SR', 5, 0, 5, 37, NULL, NULL, 0, 0, 1, 2, '7b0c606c97e25afa40ef4f40f95a28de', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 16:03:41', 0, NULL, 1, 0, 0, 0, NULL, 0, 0, 0, 1, 37, NULL, '4', NULL, NULL, NULL, NULL),
	('SR', 6, 0, 5, 37, NULL, NULL, 0, 0, 1, 2, '6b6378877bde5b6431548bc4c2b0f59a', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 16:06:38', 0, NULL, 0, 0, 0, NULL, NULL, 0, 0, 0, 1, NULL, NULL, '4', NULL, NULL, NULL, NULL),
	('SR', 7, 0, 5, 37, NULL, NULL, 0, 0, 1, 3, '9eb8a15d89bf5d17d450d5afc1b1c878', 'Test Article', 'sdkjsdji sdiouis', NULL, '2021-10-28 16:10:26', 0, NULL, 1, 0, 0, NULL, NULL, 0, 0, 0, 1, NULL, NULL, '3', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tblservice_requests` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_requests_pipe_log
CREATE TABLE IF NOT EXISTS `tblservice_requests_pipe_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `email_to` varchar(100) NOT NULL,
  `name` varchar(191) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `message` mediumtext NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_requests_pipe_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblservice_requests_pipe_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblservice_requests_pipe_log` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_requests_predefined_replies
CREATE TABLE IF NOT EXISTS `tblservice_requests_predefined_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_requests_predefined_replies: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblservice_requests_predefined_replies` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblservice_requests_predefined_replies` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_requests_priorities
CREATE TABLE IF NOT EXISTS `tblservice_requests_priorities` (
  `priorityid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`priorityid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_requests_priorities: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblservice_requests_priorities` DISABLE KEYS */;
INSERT INTO `tblservice_requests_priorities` (`priorityid`, `name`) VALUES
	(1, 'Lows'),
	(2, 'Medium'),
	(3, 'High');
/*!40000 ALTER TABLE `tblservice_requests_priorities` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_requests_status
CREATE TABLE IF NOT EXISTS `tblservice_requests_status` (
  `service_requeststatusid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `isdefault` int(11) NOT NULL DEFAULT '0',
  `statuscolor` varchar(7) DEFAULT NULL,
  `statusorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`service_requeststatusid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_requests_status: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblservice_requests_status` DISABLE KEYS */;
INSERT INTO `tblservice_requests_status` (`service_requeststatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`) VALUES
	(1, 'Open', 1, '#ff2d42', 1),
	(2, 'In progress', 1, '#84c529', 2),
	(3, 'Answered', 1, '#0000ff', 3),
	(4, 'On Hold', 1, '#c0c0c0', 4),
	(5, 'Closed', 1, '#03a9f4', 5);
/*!40000 ALTER TABLE `tblservice_requests_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_request_attachments
CREATE TABLE IF NOT EXISTS `tblservice_request_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_requestid` int(11) NOT NULL,
  `replyid` int(11) DEFAULT NULL,
  `file_name` varchar(191) NOT NULL,
  `filetype` varchar(50) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_request_attachments: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblservice_request_attachments` DISABLE KEYS */;
INSERT INTO `tblservice_request_attachments` (`id`, `service_requestid`, `replyid`, `file_name`, `filetype`, `dateadded`) VALUES
	(1, 3, 4, 'git-cheat-sheet-pdf_0.png', 'image/png', '2021-12-13 14:50:17'),
	(2, 3, 5, 'CTA-01 (2).png', 'image/png', '2021-12-13 14:51:10'),
	(3, 5, 7, 'CTA-01 (2).png', 'image/png', '2021-12-13 15:06:55');
/*!40000 ALTER TABLE `tblservice_request_attachments` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_request_replies
CREATE TABLE IF NOT EXISTS `tblservice_request_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_requestid` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `email` text,
  `date` datetime NOT NULL,
  `message` text,
  `attachment` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblservice_request_replies: ~7 rows (approximately)
/*!40000 ALTER TABLE `tblservice_request_replies` DISABLE KEYS */;
INSERT INTO `tblservice_request_replies` (`id`, `service_requestid`, `userid`, `contactid`, `name`, `email`, `date`, `message`, `attachment`, `admin`) VALUES
	(1, 500017, 2, 1, NULL, NULL, '2021-03-11 19:08:11', 'Hello Akshay ,<br />\r\n<br />\r\nYour service is in Progrss', NULL, NULL),
	(2, 3, 5, 37, NULL, NULL, '2021-11-09 19:46:04', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, NULL),
	(3, 3, 5, 37, NULL, NULL, '2021-11-11 12:53:54', 'shsd isis susususu', NULL, NULL),
	(4, 3, 5, 37, NULL, NULL, '2021-12-13 14:50:16', 'attachments postman test with log', NULL, NULL),
	(5, 3, 5, 37, NULL, NULL, '2021-12-13 14:51:10', 'attachments postman test', NULL, NULL),
	(6, 5, 5, 37, NULL, NULL, '2021-12-13 15:06:05', 'attachments postman test', NULL, NULL),
	(7, 5, 5, 37, NULL, NULL, '2021-12-13 15:06:54', 'attachments postman test', NULL, NULL);
/*!40000 ALTER TABLE `tblservice_request_replies` ENABLE KEYS */;

-- Dumping structure for table crm.tblservice_request_status_logs
CREATE TABLE IF NOT EXISTS `tblservice_request_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_requestid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblservice_request_status_logs: ~17 rows (approximately)
/*!40000 ALTER TABLE `tblservice_request_status_logs` DISABLE KEYS */;
INSERT INTO `tblservice_request_status_logs` (`id`, `service_requestid`, `status`, `datetime`) VALUES
	(1, 1, 2, '2021-04-05 08:31:12'),
	(2, 1, 3, '2021-04-05 08:31:16'),
	(3, 1, 4, '2021-04-05 08:31:32'),
	(4, 1, 5, '2021-04-05 08:31:37'),
	(5, 1, 3, '2021-04-05 08:32:56'),
	(6, 1, 2, '2021-04-05 08:32:59'),
	(7, 1, 5, '2021-04-05 08:33:06'),
	(8, 1, 1, '2021-04-05 08:35:50'),
	(9, 1, 5, '2021-04-05 08:36:01'),
	(10, 1, 5, '2021-04-05 08:50:27'),
	(11, 1, 1, '2021-04-05 09:41:52'),
	(12, 1, 4, '2021-04-24 15:15:30'),
	(13, 2, 1, '2021-04-24 15:40:58'),
	(14, 2, 2, '2021-04-24 15:41:13'),
	(15, 3, 1, '2021-10-28 16:00:20'),
	(16, 4, 1, '2021-10-28 16:03:13'),
	(17, 5, 1, '2021-10-28 16:03:41'),
	(18, 6, 1, '2021-10-28 16:06:38'),
	(19, 7, 1, '2021-10-28 16:10:26');
/*!40000 ALTER TABLE `tblservice_request_status_logs` ENABLE KEYS */;

-- Dumping structure for table crm.tblsessions
CREATE TABLE IF NOT EXISTS `tblsessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblsessions: ~17 rows (approximately)
/*!40000 ALTER TABLE `tblsessions` DISABLE KEYS */;
INSERT INTO `tblsessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('3cp696c3aj6fb6v4olfvrsrc9s64nktk', '::1', 1646052308, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035323239363B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B),
	('4ana08b1ncda4hb0pkes0qaaavrs05qd', '::1', 1646052705, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035323730353B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('4sg35hdbad05do3ktr5ft9k06ch3j7jn', '::1', 1646053615, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035333631353B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('52t7njs0gjcapct2rv7drgfd8gmdjats', '::1', 1646054634, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035343633343B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('ats2v7a4ueb8d9d16iuvskcjb53qs8tu', '::1', 1646057005, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035373030353B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('c1g5pil7300cphvgsqcui7s5k4heirng', '::1', 1646027539, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363032373533393B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B),
	('cs5pkjqkehh4u4vtbk2sdd9lem9p6g8r', '::1', 1646029722, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363032393731393B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B),
	('cu2ml65c3d18okg7apuadq978cigo5c5', '::1', 1646054005, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035343030353B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('dqtg1vkf9sudk832f5rcr4se8jt0g88q', '::1', 1646057014, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035373030353B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('dte6oi017omkaneopqb9j5dt1g6dr6ib', '::1', 1646053182, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035333138323B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('e95l51alk5v5tpo9ac6s0ut53deofj5b', '::1', 1646024242, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363032343234323B7265645F75726C7C733A34303A22687474703A2F2F6C6F63616C686F73742F63726D2F61646D696E2F6F726465725F747261636B6572223B),
	('eb64v94lg9i0jk2j665s6remruorrp1l', '::1', 1646056050, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035363035303B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('g2l46v000kl7kkqceakftevuk0ncoktn', '::1', 1646211837, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363231313736323B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('ga791jn54o1sso7ggsnv4oek4biqmd5a', '::1', 1646054309, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035343330393B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('i10epi28v4fdl74hgmfts55gomij1jo4', '::1', 1646211768, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363231313736323B),
	('i9rvn4g2dprd74n3md2f4n0uavdqguvr', '::1', 1646028903, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363032383930333B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B),
	('iq2cume4k9et361ltpji40lqurpg40s2', '::1', 1646055606, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363035353630363B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B636C69656E745F757365725F69647C733A313A2235223B636F6E746163745F757365725F69647C733A323A223337223B636C69656E745F6C6F676765645F696E7C623A313B),
	('rkep105lcvp8bii7r990tfuudin11g8e', '::1', 1646031617, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363033313631373B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B),
	('s6pr5u4nrutlbqpjjnlsc9l3b99m1bhl', '::1', 1646040372, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313634363034303335393B73746166665F757365725F69647C733A313A2231223B73746166665F6C6F676765645F696E7C623A313B73657475702D6D656E752D6F70656E7C733A303A22223B);
/*!40000 ALTER TABLE `tblsessions` ENABLE KEYS */;

-- Dumping structure for table crm.tblshared_customer_files
CREATE TABLE IF NOT EXISTS `tblshared_customer_files` (
  `file_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblshared_customer_files: ~5 rows (approximately)
/*!40000 ALTER TABLE `tblshared_customer_files` DISABLE KEYS */;
INSERT INTO `tblshared_customer_files` (`file_id`, `contact_id`) VALUES
	(1, 1),
	(1, 4),
	(1, 8),
	(6, 37),
	(7, 37),
	(8, 37);
/*!40000 ALTER TABLE `tblshared_customer_files` ENABLE KEYS */;

-- Dumping structure for table crm.tblslatimes
CREATE TABLE IF NOT EXISTS `tblslatimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `high_incident_sla` varchar(50) DEFAULT NULL,
  `medium_incident_sla` varchar(50) DEFAULT NULL,
  `low_incident_sla` varchar(50) DEFAULT NULL,
  `high_service_request_sla` varchar(50) DEFAULT NULL,
  `medium_service_request_sla` varchar(50) DEFAULT NULL,
  `low_service_request_sla` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblslatimes: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblslatimes` DISABLE KEYS */;
INSERT INTO `tblslatimes` (`id`, `customer_id`, `high_incident_sla`, `medium_incident_sla`, `low_incident_sla`, `high_service_request_sla`, `medium_service_request_sla`, `low_service_request_sla`) VALUES
	(1, 1, '1 Hour', '2 Hour', '3 Hour', '24 Hour', '48 Hour', '24 Hour'),
	(2, 2, '1 Hour', '1 Hour', '3 Hour', '24 Hour', '76 Hour', '76 Hour');
/*!40000 ALTER TABLE `tblslatimes` ENABLE KEYS */;

-- Dumping structure for table crm.tblspam_filters
CREATE TABLE IF NOT EXISTS `tblspam_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(40) NOT NULL,
  `rel_type` varchar(10) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblspam_filters: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblspam_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblspam_filters` ENABLE KEYS */;

-- Dumping structure for table crm.tblstaff
CREATE TABLE IF NOT EXISTS `tblstaff` (
  `staffid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `facebook` mediumtext,
  `linkedin` mediumtext,
  `phonenumber` varchar(30) DEFAULT NULL,
  `skype` varchar(50) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `datecreated` datetime NOT NULL,
  `profile_image` varchar(191) DEFAULT NULL,
  `last_ip` varchar(40) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `last_password_change` datetime DEFAULT NULL,
  `new_pass_key` varchar(32) DEFAULT NULL,
  `new_pass_key_requested` datetime DEFAULT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  `role` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `default_language` varchar(40) DEFAULT NULL,
  `direction` varchar(3) DEFAULT NULL,
  `media_path_slug` varchar(191) DEFAULT NULL,
  `is_not_staff` int(11) NOT NULL DEFAULT '0',
  `hourly_rate` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_sale_value_q1` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_sale_value_q2` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_sale_value_q3` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_sale_value_q4` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_gp_value` decimal(15,2) NOT NULL DEFAULT '0.00',
  `target_cust_value` decimal(15,2) NOT NULL DEFAULT '0.00',
  `two_factor_auth_enabled` tinyint(1) DEFAULT '0',
  `two_factor_auth_code` varchar(100) DEFAULT NULL,
  `two_factor_auth_code_requested` datetime DEFAULT NULL,
  `email_signature` text,
  `google_auth_secret` text,
  `mail_password` varchar(250) DEFAULT NULL,
  `mail_signature` varchar(250) DEFAULT NULL,
  `last_email_check` varchar(50) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`staffid`),
  KEY `firstname` (`firstname`),
  KEY `lastname` (`lastname`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblstaff: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblstaff` DISABLE KEYS */;
INSERT INTO `tblstaff` (`staffid`, `email`, `firstname`, `lastname`, `facebook`, `linkedin`, `phonenumber`, `skype`, `password`, `datecreated`, `profile_image`, `last_ip`, `last_login`, `last_activity`, `last_password_change`, `new_pass_key`, `new_pass_key_requested`, `admin`, `role`, `active`, `default_language`, `direction`, `media_path_slug`, `is_not_staff`, `hourly_rate`, `target_sale_value_q1`, `target_sale_value_q2`, `target_sale_value_q3`, `target_sale_value_q4`, `target_gp_value`, `target_cust_value`, `two_factor_auth_enabled`, `two_factor_auth_code`, `two_factor_auth_code_requested`, `email_signature`, `google_auth_secret`, `mail_password`, `mail_signature`, `last_email_check`, `country_id`) VALUES
	(1, 'akshaydange@leometric.com', 'Admin', 'Admin', '', '', '', '', '$2a$08$Z/zN/8PSTR9wHjgvWZ6Z0uRhnp/ut.NNouqJhUv8zYoqzjXUsFM1u', '2021-02-12 17:01:27', NULL, '::1', '2022-02-28 10:27:33', '2022-02-28 18:15:04', NULL, NULL, NULL, 1, 0, 1, '', '', NULL, 0, 0.00, 12600.00, 12900.00, 15000.00, 12200.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, 'Leometric@123', '', '1616075101', 0),
	(4, 'deepak@gmail.com', 'Deepak', 'G', '', '', '8446301196', '', '$2a$08$9JxGA17hhnXH.yzfEl2f6eAijIOxNa1LqPsfeFbFQWjVuzT8QLqJS', '2021-03-15 10:32:04', NULL, '::1', '2021-03-15 11:07:01', '2021-03-15 11:07:06', '2021-05-18 12:43:33', NULL, NULL, 0, 1, 1, '', '', 'deepak-g', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, NULL, NULL, NULL, 102),
	(5, 'user@user.com', 'User', 'User', '', '', '', '', '$2a$08$Pq.axQ7Pxn00lrfZAyEixuaevg2XwsPYvYq9.iA6JYH3gWdCueLSq', '2021-04-21 14:27:30', NULL, '::1', '2022-01-25 16:28:59', '2022-01-25 16:31:25', '2022-01-25 16:28:38', NULL, NULL, 0, 0, 1, '', '', 'user-user', 0, 0.00, 10000.00, 0.00, 0.00, 0.00, 2500.00, 5000.00, 0, NULL, NULL, '', NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `tblstaff` ENABLE KEYS */;

-- Dumping structure for table crm.tblstaff_departments
CREATE TABLE IF NOT EXISTS `tblstaff_departments` (
  `staffdepartmentid` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `departmentid` int(11) NOT NULL,
  PRIMARY KEY (`staffdepartmentid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblstaff_departments: ~4 rows (approximately)
/*!40000 ALTER TABLE `tblstaff_departments` DISABLE KEYS */;
INSERT INTO `tblstaff_departments` (`staffdepartmentid`, `staffid`, `departmentid`) VALUES
	(2, 9, 1),
	(8, 10, 2),
	(9, 11, 2),
	(12, 4, 6);
/*!40000 ALTER TABLE `tblstaff_departments` ENABLE KEYS */;

-- Dumping structure for table crm.tblstaff_permissions
CREATE TABLE IF NOT EXISTS `tblstaff_permissions` (
  `staff_id` int(11) NOT NULL,
  `feature` varchar(40) NOT NULL,
  `capability` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblstaff_permissions: ~18 rows (approximately)
/*!40000 ALTER TABLE `tblstaff_permissions` DISABLE KEYS */;
INSERT INTO `tblstaff_permissions` (`staff_id`, `feature`, `capability`) VALUES
	(5, 'incidents', 'view_own'),
	(5, 'incidents', 'create'),
	(5, 'service_requests', 'view_own'),
	(5, 'service_requests', 'create'),
	(5, 'service_requests', 'edit'),
	(5, 'inventories', 'view'),
	(5, 'inventories', 'create'),
	(5, 'backups', 'view'),
	(5, 'backups', 'create'),
	(5, 'renewals', 'view'),
	(5, 'sales_order', 'view'),
	(5, 'sales_order', 'create'),
	(5, 'sales_order', 'edit'),
	(5, 'projects', 'edit'),
	(5, 'boqs', 'view'),
	(5, 'boqs', 'create'),
	(5, 'boqs', 'edit'),
	(5, 'leads', 'view');
/*!40000 ALTER TABLE `tblstaff_permissions` ENABLE KEYS */;

-- Dumping structure for table crm.tblsubscriptions
CREATE TABLE IF NOT EXISTS `tblsubscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `description` text,
  `description_in_item` tinyint(1) NOT NULL DEFAULT '0',
  `clientid` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `terms` text,
  `currency` int(11) NOT NULL,
  `tax_id` int(11) NOT NULL DEFAULT '0',
  `stripe_tax_id` varchar(50) DEFAULT NULL,
  `tax_id_2` int(11) NOT NULL DEFAULT '0',
  `stripe_tax_id_2` varchar(50) DEFAULT NULL,
  `stripe_plan_id` text,
  `stripe_subscription_id` text NOT NULL,
  `next_billing_cycle` bigint(20) DEFAULT NULL,
  `ends_at` bigint(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `created_from` int(11) NOT NULL,
  `date_subscribed` datetime DEFAULT NULL,
  `in_test_environment` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientid` (`clientid`),
  KEY `currency` (`currency`),
  KEY `tax_id` (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblsubscriptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblsubscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblsubscriptions` ENABLE KEYS */;

-- Dumping structure for table crm.tbltaggables
CREATE TABLE IF NOT EXISTS `tbltaggables` (
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(20) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `tag_order` int(11) NOT NULL DEFAULT '0',
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltaggables: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbltaggables` DISABLE KEYS */;
INSERT INTO `tbltaggables` (`rel_id`, `rel_type`, `tag_id`, `tag_order`) VALUES
	(1, 'lead', 1, 1),
	(3, 'lead', 2, 1),
	(4, 'lead', 1, 1),
	(4, 'lead', 3, 2),
	(4, 'lead', 4, 3);
/*!40000 ALTER TABLE `tbltaggables` ENABLE KEYS */;

-- Dumping structure for table crm.tbltags
CREATE TABLE IF NOT EXISTS `tbltags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltags: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbltags` DISABLE KEYS */;
INSERT INTO `tbltags` (`id`, `name`) VALUES
	(3, 'akshay'),
	(1, 'citrix'),
	(4, 'new lead'),
	(2, 'vidhi');
/*!40000 ALTER TABLE `tbltags` ENABLE KEYS */;

-- Dumping structure for table crm.tbltasks
CREATE TABLE IF NOT EXISTS `tbltasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` mediumtext,
  `description` text,
  `priority` int(11) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  `startdate` date NOT NULL,
  `duedate` date DEFAULT NULL,
  `datefinished` datetime DEFAULT NULL,
  `addedfrom` int(11) NOT NULL,
  `is_added_from_contact` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `recurring_type` varchar(10) DEFAULT NULL,
  `repeat_every` int(11) DEFAULT NULL,
  `recurring` int(11) NOT NULL DEFAULT '0',
  `is_recurring_from` int(11) DEFAULT NULL,
  `cycles` int(11) NOT NULL DEFAULT '0',
  `total_cycles` int(11) NOT NULL DEFAULT '0',
  `custom_recurring` tinyint(1) NOT NULL DEFAULT '0',
  `last_recurring_date` date DEFAULT NULL,
  `rel_id` int(11) DEFAULT NULL,
  `rel_type` varchar(30) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `billable` tinyint(1) NOT NULL DEFAULT '0',
  `billed` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `hourly_rate` decimal(15,2) NOT NULL DEFAULT '0.00',
  `milestone` int(11) DEFAULT '0',
  `kanban_order` int(11) NOT NULL DEFAULT '0',
  `milestone_order` int(11) NOT NULL DEFAULT '0',
  `visible_to_client` tinyint(1) NOT NULL DEFAULT '0',
  `deadline_notified` int(11) NOT NULL DEFAULT '0',
  `time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rel_id` (`rel_id`),
  KEY `rel_type` (`rel_type`),
  KEY `milestone` (`milestone`),
  KEY `kanban_order` (`kanban_order`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltasks: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbltasks` DISABLE KEYS */;
INSERT INTO `tbltasks` (`id`, `name`, `description`, `priority`, `clientid`, `dateadded`, `startdate`, `duedate`, `datefinished`, `addedfrom`, `is_added_from_contact`, `status`, `recurring_type`, `repeat_every`, `recurring`, `is_recurring_from`, `cycles`, `total_cycles`, `custom_recurring`, `last_recurring_date`, `rel_id`, `rel_type`, `is_public`, `billable`, `billed`, `invoice_id`, `hourly_rate`, `milestone`, `kanban_order`, `milestone_order`, `visible_to_client`, `deadline_notified`, `time`) VALUES
	(5, 'Test Ticket  01 Task ', 'Please work on Test Incident', 2, NULL, '2021-03-18 17:03:11', '2021-03-18', '2021-03-20', NULL, 1, 0, 4, 'week', 1, 1, NULL, 0, 2, 0, '2021-04-01', 1, 'ticket', 0, 1, 0, 0, 2500.00, 0, 0, 0, 0, 0, NULL),
	(6, 'Test', 'Test', 2, NULL, '2021-03-18 18:02:22', '2021-03-18', '2021-03-20', NULL, 1, 0, 4, 'week', 1, 1, NULL, 0, 2, 0, '2021-04-01', NULL, NULL, 0, 1, 0, 0, 0.00, 0, 0, 0, 0, 0, NULL),
	(8, 'Test', '', 2, NULL, '2021-03-18 18:11:34', '2021-03-18', '2021-03-20', NULL, 1, 0, 4, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, 0, 1, 0, 0, 45.00, 0, 0, 0, 0, 0, NULL),
	(9, 'Test Ticket  01 Task ', 'Please work on Test Incident', 2, NULL, '2021-04-27 10:59:02', '2021-03-25', '2021-03-27', NULL, 1, 0, 1, NULL, 0, 0, 5, 0, 0, 0, NULL, 1, 'ticket', 0, 1, 0, 0, 2500.00, 0, 0, 0, 0, 0, NULL),
	(10, 'Test', 'Test', 2, NULL, '2021-04-27 10:59:11', '2021-03-25', '2021-03-27', NULL, 1, 0, 1, NULL, 0, 0, 6, 0, 0, 0, NULL, NULL, NULL, 0, 1, 0, 0, 0.00, 0, 0, 0, 0, 0, NULL),
	(11, 'Test Ticket  01 Task ', 'Please work on Test Incident', 2, NULL, '2021-05-26 18:06:18', '2021-04-01', '2021-04-03', NULL, 1, 0, 1, NULL, 0, 0, 5, 0, 0, 0, NULL, 1, 'ticket', 0, 1, 0, 0, 2500.00, 0, 0, 0, 0, 0, NULL),
	(12, 'Test', 'Test', 2, NULL, '2021-05-26 18:06:26', '2021-04-01', '2021-04-03', NULL, 1, 0, 1, NULL, 0, 0, 6, 0, 0, 0, NULL, NULL, NULL, 0, 1, 0, 0, 0.00, 0, 0, 0, 0, 0, NULL),
	(13, 'Test Task', 'Test', 2, NULL, '2021-07-16 17:13:37', '2021-07-16', '2021-07-19', NULL, 1, 0, 4, 'week', 1, 1, NULL, 0, 0, 0, NULL, 2, 'project', 0, 1, 0, 0, 100.00, 1, 0, 0, 0, 0, NULL),
	(14, 'New Checklist Task', '', 2, NULL, '2021-07-16 17:17:54', '2021-07-16', '2021-07-21', NULL, 1, 0, 4, 'week', 1, 1, NULL, 0, 0, 0, NULL, 2, 'project', 0, 1, 0, 0, 0.00, 2, 0, 0, 0, 0, NULL),
	(15, 'Akshay B Dange', '', 2, NULL, '2021-07-16 17:58:21', '2021-07-16', NULL, NULL, 1, 0, 4, NULL, NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, 0, 1, 0, 0, 0.00, 0, 0, 0, 0, 0, NULL),
	(16, 'API Testing Task', 'shksj sdjhsnd sidhisdh sidih', 3, 5, '2021-11-13 13:06:51', '2021-11-13', '2021-12-08', NULL, 1, 1, 4, NULL, NULL, 0, NULL, 0, 0, 0, NULL, 3, 'ticket', 0, 1, 0, 0, 0.00, 2, 0, 0, 0, 0, NULL),
	(17, 'Test timesheet', '', 2, NULL, '2021-12-06 15:01:49', '2021-12-05', '2021-12-08', '2021-12-06 15:25:59', 1, 0, 5, NULL, NULL, 0, NULL, 0, 0, 0, NULL, 3, 'ticket', 0, 1, 0, 0, 0.00, 0, 0, 0, 0, 0, NULL);
/*!40000 ALTER TABLE `tbltasks` ENABLE KEYS */;

-- Dumping structure for table crm.tbltaskstimers
CREATE TABLE IF NOT EXISTS `tbltaskstimers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `start_time` varchar(64) NOT NULL,
  `end_time` varchar(64) DEFAULT NULL,
  `staff_id` int(11) NOT NULL,
  `hourly_rate` decimal(15,2) NOT NULL DEFAULT '0.00',
  `note` text,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltaskstimers: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbltaskstimers` DISABLE KEYS */;
INSERT INTO `tbltaskstimers` (`id`, `task_id`, `start_time`, `end_time`, `staff_id`, `hourly_rate`, `note`) VALUES
	(1, 17, '1617588579', '1617597579', 1, 0.00, NULL),
	(2, 16, '1617592203', '1617597603', 1, 0.00, NULL),
	(3, 17, '1638783126', '1638784550', 1, 0.00, NULL);
/*!40000 ALTER TABLE `tbltaskstimers` ENABLE KEYS */;

-- Dumping structure for table crm.tbltasks_checklist_templates
CREATE TABLE IF NOT EXISTS `tbltasks_checklist_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltasks_checklist_templates: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltasks_checklist_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltasks_checklist_templates` ENABLE KEYS */;

-- Dumping structure for table crm.tbltask_assigned
CREATE TABLE IF NOT EXISTS `tbltask_assigned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `taskid` int(11) NOT NULL,
  `assigned_from` int(11) NOT NULL DEFAULT '0',
  `is_assigned_from_contact` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `taskid` (`taskid`),
  KEY `staffid` (`staffid`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltask_assigned: ~17 rows (approximately)
/*!40000 ALTER TABLE `tbltask_assigned` DISABLE KEYS */;
INSERT INTO `tbltask_assigned` (`id`, `staffid`, `taskid`, `assigned_from`, `is_assigned_from_contact`) VALUES
	(7, 1, 5, 1, 0),
	(8, 4, 5, 1, 0),
	(9, 1, 6, 1, 0),
	(12, 1, 8, 1, 0),
	(13, 4, 8, 1, 0),
	(14, 1, 9, 1, 0),
	(15, 4, 9, 1, 0),
	(16, 1, 10, 1, 0),
	(17, 1, 11, 1, 0),
	(18, 4, 11, 1, 0),
	(19, 1, 12, 1, 0),
	(20, 1, 13, 1, 0),
	(21, 1, 14, 1, 0),
	(22, 1, 15, 1, 0),
	(23, 5, 15, 1, 0),
	(24, 4, 15, 1, 0),
	(25, 1, 17, 1, 0);
/*!40000 ALTER TABLE `tbltask_assigned` ENABLE KEYS */;

-- Dumping structure for table crm.tbltask_checklist_items
CREATE TABLE IF NOT EXISTS `tbltask_checklist_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskid` int(11) NOT NULL,
  `description` text NOT NULL,
  `finished` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL,
  `addedfrom` int(11) NOT NULL,
  `finished_from` int(11) DEFAULT '0',
  `list_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltask_checklist_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltask_checklist_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltask_checklist_items` ENABLE KEYS */;

-- Dumping structure for table crm.tbltask_comments
CREATE TABLE IF NOT EXISTS `tbltask_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `taskid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_id` (`file_id`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltask_comments: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbltask_comments` DISABLE KEYS */;
INSERT INTO `tbltask_comments` (`id`, `content`, `taskid`, `staffid`, `contact_id`, `file_id`, `dateadded`) VALUES
	(1, 'dskhj sdjhsd', 5, 0, 1, 0, '0000-00-00 00:00:00'),
	(2, 'API Testing Task Comment', 16, 0, 0, 0, '2021-11-13 13:38:54'),
	(3, 'New API Testing Task Comment', 16, 0, 37, 0, '2021-11-13 13:41:28');
/*!40000 ALTER TABLE `tbltask_comments` ENABLE KEYS */;

-- Dumping structure for table crm.tbltask_followers
CREATE TABLE IF NOT EXISTS `tbltask_followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `taskid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltask_followers: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltask_followers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltask_followers` ENABLE KEYS */;

-- Dumping structure for table crm.tbltaxes
CREATE TABLE IF NOT EXISTS `tbltaxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `taxrate` decimal(15,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltaxes: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltaxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltaxes` ENABLE KEYS */;

-- Dumping structure for table crm.tblteamroles
CREATE TABLE IF NOT EXISTS `tblteamroles` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblteamroles: ~2 rows (approximately)
/*!40000 ALTER TABLE `tblteamroles` DISABLE KEYS */;
INSERT INTO `tblteamroles` (`roleid`, `name`) VALUES
	(1, 'UI'),
	(2, 'Tester');
/*!40000 ALTER TABLE `tblteamroles` ENABLE KEYS */;

-- Dumping structure for table crm.tblteams
CREATE TABLE IF NOT EXISTS `tblteams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `staffid` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblteams: ~7 rows (approximately)
/*!40000 ALTER TABLE `tblteams` DISABLE KEYS */;
INSERT INTO `tblteams` (`id`, `customer_id`, `staffid`, `role`) VALUES
	(11, 2, 1, 1),
	(12, 1, 4, 1),
	(13, 2, 4, 2),
	(14, 3, 4, 1),
	(15, 3, 4, 1),
	(16, 5, 5, 1),
	(17, 5, 4, 1),
	(18, 5, 1, 2);
/*!40000 ALTER TABLE `tblteams` ENABLE KEYS */;

-- Dumping structure for table crm.tblteam_availiable
CREATE TABLE IF NOT EXISTS `tblteam_availiable` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `leave_type` varchar(50) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `ticket_subject` int(11) DEFAULT NULL COMMENT 'status id =3 then ticket sub from incident and status id = 4 then ticjet sub from SR',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblteam_availiable: ~3 rows (approximately)
/*!40000 ALTER TABLE `tblteam_availiable` DISABLE KEYS */;
INSERT INTO `tblteam_availiable` (`id`, `staff_id`, `leave_type`, `status_id`, `from_date`, `to_date`, `project_id`, `ticket_subject`, `created_at`, `updated_at`) VALUES
	(5, 1, 'Sick', 1, '2021-06-11', '2021-06-17', 0, 0, '2021-06-17 12:39:18', '2021-11-09 11:56:25'),
	(6, 4, '', 5, '0000-00-00', '0000-00-00', NULL, NULL, '2021-06-17 14:05:16', '2021-06-17 14:05:16'),
	(7, 5, '', 4, '2021-06-30', '2021-06-18', NULL, 2, '2021-06-17 14:16:25', '2021-11-09 11:56:28');
/*!40000 ALTER TABLE `tblteam_availiable` ENABLE KEYS */;

-- Dumping structure for table crm.tbltemplates
CREATE TABLE IF NOT EXISTS `tbltemplates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `addedfrom` int(11) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltemplates: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltemplates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltemplates` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets
CREATE TABLE IF NOT EXISTS `tbltickets` (
  `ticketpattern` varchar(10) NOT NULL DEFAULT 'IN',
  `ticketid` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminreplying` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `email` text,
  `name` text,
  `department` int(11) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `service` int(11) DEFAULT NULL,
  `ticketkey` varchar(32) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `message` text,
  `admin` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `lastreply` datetime DEFAULT NULL,
  `clientread` int(11) NOT NULL DEFAULT '0',
  `adminread` int(11) NOT NULL DEFAULT '0',
  `assigned` int(11) NOT NULL DEFAULT '0',
  `is_approved` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL COMMENT 'Approved or rejected based on staff id',
  `attach_service_report` int(11) NOT NULL DEFAULT '0',
  `worklog` int(11) NOT NULL DEFAULT '0',
  `user_acceptance` int(11) NOT NULL DEFAULT '0',
  `take_approval` int(11) DEFAULT NULL,
  `approval_assigned_to_staffid` int(11) DEFAULT NULL,
  `incident_mail_flag` int(11) DEFAULT NULL,
  `incident_time` varchar(50) DEFAULT NULL,
  `billing_type` varchar(50) DEFAULT NULL,
  `contract` int(11) DEFAULT NULL,
  `additional_charges` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ticketid`),
  KEY `service` (`service`),
  KEY `department` (`department`),
  KEY `status` (`status`),
  KEY `userid` (`userid`),
  KEY `priority` (`priority`),
  KEY `project_id` (`project_id`),
  KEY `contactid` (`contactid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltickets: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbltickets` DISABLE KEYS */;
INSERT INTO `tbltickets` (`ticketpattern`, `ticketid`, `adminreplying`, `userid`, `contactid`, `email`, `name`, `department`, `company`, `priority`, `status`, `service`, `ticketkey`, `subject`, `message`, `admin`, `date`, `project_id`, `lastreply`, `clientread`, `adminread`, `assigned`, `is_approved`, `approved_by`, `attach_service_report`, `worklog`, `user_acceptance`, `take_approval`, `approval_assigned_to_staffid`, `incident_mail_flag`, `incident_time`, `billing_type`, `contract`, `additional_charges`) VALUES
	('IN', 1, 0, 5, 37, NULL, NULL, 0, NULL, 0, 3, 1, '620bca877dea4c9ddb44dbceeded950b', 'Test Ticket', 'asdjh asiodu asiduiasd iasdiuasi', NULL, '2021-10-28 10:56:36', 0, '2021-11-25 14:51:30', 0, 1, 1, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020368', NULL, NULL, NULL),
	('IN', 2, 0, 5, 27, NULL, NULL, 0, NULL, 0, 3, 1, '287a00298f1f203526bb02a161777430', 'Work Report : 05/06/2020', 'asdj asdio0asd aisdo0asid', NULL, '2021-10-28 11:00:57', 0, NULL, 1, 1, 4, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 3, 0, 1, 2, NULL, NULL, 0, NULL, 0, 1, 2, 'be9321feb7f14371326e0c4338fa0222', 'New API Ticket', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 15:48:25', 0, '2021-11-11 19:25:21', 0, 1, 4, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 4, 0, 1, 3, NULL, NULL, 0, NULL, 0, 1, 2, 'b4df16e8728769ea6e14a1c9ea4ecf38', 'New API Ticket', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 15:49:47', 0, NULL, 0, 1, 1, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 5, 0, 5, 37, NULL, NULL, 0, NULL, 0, 1, 2, '378d474fc6dd82457132cba4bc94a00c', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 15:53:36', 0, '2021-12-13 12:08:01', 1, 1, 5, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 6, 0, 5, 37, NULL, NULL, 0, NULL, 0, 1, 2, '54d61c88042f03af40240652d4e7222f', 'New API Ticket Auth', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, '2021-10-28 16:12:45', 0, NULL, 0, 1, 5, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 7, 0, 5, 37, NULL, NULL, 0, NULL, 0, 1, NULL, '9b9e8572378e31792aff5c0d28ea1c0e', 'Test Article', '', NULL, '2022-01-08 11:14:03', 0, NULL, 1, 1, 0, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3020399', NULL, NULL, NULL),
	('IN', 8, 0, 5, 40, 'vidhi123@gmail.com', 'Vidhi Landge', 6, NULL, 1, 1, 2, '478fc7072cf5520e684fd8f2774e09b9', 'Work Report : 05/06/2020', 'Testing<a href="http://localhost/crm/knowledge_base/test-article">Test Article</a>', 1, '2022-02-26 17:19:56', 1, NULL, 0, 1, 1, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '3573', 'free', 2, NULL);
/*!40000 ALTER TABLE `tbltickets` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_deliveries
CREATE TABLE IF NOT EXISTS `tbltickets_deliveries` (
  `deliveryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`deliveryid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tbltickets_deliveries: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_deliveries` DISABLE KEYS */;
INSERT INTO `tbltickets_deliveries` (`deliveryid`, `name`) VALUES
	(1, 'Remote'),
	(2, 'Onsite');
/*!40000 ALTER TABLE `tbltickets_deliveries` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_pipe_log
CREATE TABLE IF NOT EXISTS `tbltickets_pipe_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `email_to` varchar(100) NOT NULL,
  `name` varchar(191) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `message` mediumtext NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltickets_pipe_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_pipe_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltickets_pipe_log` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_predefined_replies
CREATE TABLE IF NOT EXISTS `tbltickets_predefined_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltickets_predefined_replies: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_predefined_replies` DISABLE KEYS */;
INSERT INTO `tbltickets_predefined_replies` (`id`, `name`, `message`) VALUES
	(1, 'Test reply', 'Testing');
/*!40000 ALTER TABLE `tbltickets_predefined_replies` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_priorities
CREATE TABLE IF NOT EXISTS `tbltickets_priorities` (
  `priorityid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`priorityid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltickets_priorities: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_priorities` DISABLE KEYS */;
INSERT INTO `tbltickets_priorities` (`priorityid`, `name`) VALUES
	(1, 'Low'),
	(2, 'Medium'),
	(3, 'High');
/*!40000 ALTER TABLE `tbltickets_priorities` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_request_for
CREATE TABLE IF NOT EXISTS `tbltickets_request_for` (
  `requestid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`requestid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tbltickets_request_for: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_request_for` DISABLE KEYS */;
INSERT INTO `tbltickets_request_for` (`requestid`, `name`) VALUES
	(1, 'Add'),
	(2, 'Remove'),
	(3, 'Change'),
	(4, 'Upgrade');
/*!40000 ALTER TABLE `tbltickets_request_for` ENABLE KEYS */;

-- Dumping structure for table crm.tbltickets_status
CREATE TABLE IF NOT EXISTS `tbltickets_status` (
  `ticketstatusid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `isdefault` int(11) NOT NULL DEFAULT '0',
  `statuscolor` varchar(7) DEFAULT NULL,
  `statusorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticketstatusid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltickets_status: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbltickets_status` DISABLE KEYS */;
INSERT INTO `tbltickets_status` (`ticketstatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`) VALUES
	(1, 'New', 1, '#ff2d42', 1),
	(2, 'In progress', 1, '#84c529', 2),
	(3, 'Resolved', 1, '#0000ff', 3),
	(4, 'On Hold', 1, '#c0c0c0', 4),
	(5, 'Closed Edit', 1, '#03a9f4', 5),
	(6, 'Good', 0, '#019009', NULL);
/*!40000 ALTER TABLE `tbltickets_status` ENABLE KEYS */;

-- Dumping structure for table crm.tblticket_attachments
CREATE TABLE IF NOT EXISTS `tblticket_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticketid` int(11) NOT NULL,
  `replyid` int(11) DEFAULT NULL,
  `file_name` varchar(191) NOT NULL,
  `filetype` varchar(50) DEFAULT NULL,
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblticket_attachments: ~9 rows (approximately)
/*!40000 ALTER TABLE `tblticket_attachments` DISABLE KEYS */;
INSERT INTO `tblticket_attachments` (`id`, `ticketid`, `replyid`, `file_name`, `filetype`, `dateadded`) VALUES
	(1, 1, NULL, 'CE-20-21-010002.pdf', 'application/pdf', '2021-10-28 10:56:37'),
	(2, 1, NULL, 'PRO-000764.pdf', 'application/pdf', '2021-10-28 10:56:37'),
	(3, 2, NULL, 'ConfirmationPage-210411122062.pdf', 'application/pdf', '2021-10-28 11:00:57'),
	(4, 6, NULL, 'CE-20-21-010002.pdf', 'application/pdf', '2021-10-28 16:12:45'),
	(5, 1, 11, 'NCDEX-homepage_banner__20211028 (1).jpg', 'image/jpeg', '2021-11-09 19:28:52'),
	(6, 5, 16, 'CTA-01 (1).png', 'image/png', '2021-12-09 12:35:03'),
	(7, 5, 18, 'IDH  NCDEX IPFT Banner.jpg', 'image/jpeg', '2021-12-09 12:41:20'),
	(8, 5, 25, 'IMG20200504.jpg', 'image/jpeg', '2021-12-13 12:08:01'),
	(9, 5, 26, 'git-cheat-sheet-pdf_0.png', 'image/png', '2021-12-13 12:12:09'),
	(10, 5, 28, 'git-cheat-sheet-pdf_0.png', 'image/png', '2021-12-13 12:15:54');
/*!40000 ALTER TABLE `tblticket_attachments` ENABLE KEYS */;

-- Dumping structure for table crm.tblticket_replies
CREATE TABLE IF NOT EXISTS `tblticket_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticketid` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `email` text,
  `date` datetime NOT NULL,
  `message` text,
  `attachment` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblticket_replies: ~14 rows (approximately)
/*!40000 ALTER TABLE `tblticket_replies` DISABLE KEYS */;
INSERT INTO `tblticket_replies` (`id`, `ticketid`, `userid`, `contactid`, `name`, `email`, `date`, `message`, `attachment`, `admin`) VALUES
	(8, 100045, 2, 4, NULL, NULL, '2021-03-13 12:11:59', 'Test', NULL, NULL),
	(10, 3, 0, 0, NULL, NULL, '2021-03-18 11:52:08', 'Testing Done', NULL, 1),
	(11, 1, 5, 37, NULL, NULL, '2021-11-09 19:28:50', 'Testing of app reply', NULL, NULL),
	(12, 1, 5, 37, NULL, NULL, '2021-11-09 19:36:50', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, NULL),
	(13, 3, 5, 37, NULL, NULL, '2021-11-09 19:40:42', 'sdkjjsdkjsk ihsidhy sdihsui', NULL, NULL),
	(14, 3, 5, 37, NULL, NULL, '2021-11-11 19:25:19', 'Akshay Dange ihsidhy', NULL, NULL),
	(15, 1, 0, 0, NULL, NULL, '2021-11-25 14:51:30', 'sjkjhsd sjsj', NULL, 1),
	(16, 5, 5, 37, NULL, NULL, '2021-12-09 12:35:03', 'sdkjjsdkjsk ihsidhy sdihsui attachments', NULL, NULL),
	(17, 5, 5, 37, NULL, NULL, '2021-12-09 12:39:52', 'sdkjjsdkjsk ihsidhy sdihsui attachments test', NULL, NULL),
	(18, 5, 5, 37, NULL, NULL, '2021-12-09 12:41:20', 'sdkjjsdkjsk ihsidhy sdihsui attachments test new', NULL, NULL),
	(24, 5, 5, 37, NULL, NULL, '2021-12-13 11:49:42', 'attachments postman test', NULL, NULL),
	(25, 5, 5, 37, NULL, NULL, '2021-12-13 12:08:01', 'Test ticket reply from web', NULL, NULL),
	(26, 5, 5, 37, NULL, NULL, '2021-12-13 12:12:08', 'attachments postman test', NULL, NULL),
	(27, 5, 5, 37, NULL, NULL, '2021-12-13 12:12:53', 'attachments postman test', NULL, NULL),
	(28, 5, 5, 37, NULL, NULL, '2021-12-13 12:15:53', 'attachments postman test with log', NULL, NULL);
/*!40000 ALTER TABLE `tblticket_replies` ENABLE KEYS */;

-- Dumping structure for table crm.tblticket_status_logs
CREATE TABLE IF NOT EXISTS `tblticket_status_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticketid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table crm.tblticket_status_logs: ~28 rows (approximately)
/*!40000 ALTER TABLE `tblticket_status_logs` DISABLE KEYS */;
INSERT INTO `tblticket_status_logs` (`id`, `ticketid`, `status`, `datetime`, `updatetime`) VALUES
	(1, 1, 1, '2021-03-18 14:38:15', '2022-02-26 17:18:34'),
	(2, 2, 1, '2021-03-18 14:40:53', '2022-02-26 17:18:34'),
	(3, 1, 1, '2021-03-18 14:48:46', '2022-02-26 17:18:34'),
	(4, 1, 1, '2021-03-18 14:50:42', '2022-02-26 17:18:34'),
	(5, 1, 5, '2021-03-18 14:54:33', '2022-02-26 17:18:34'),
	(6, 1, 3, '2021-03-18 14:56:01', '2022-02-26 17:18:34'),
	(7, 1, 4, '2021-03-18 14:56:07', '2022-02-26 17:18:34'),
	(8, 1, 5, '2021-03-18 14:56:38', '2022-02-26 17:18:34'),
	(9, 1, 1, '2021-03-18 14:56:42', '2022-02-26 17:18:34'),
	(10, 2, 1, '2021-03-18 15:00:13', '2022-02-26 17:18:34'),
	(11, 3, 1, '2021-03-18 15:43:51', '2022-02-26 18:19:30'),
	(12, 1, 1, '2021-03-18 17:02:14', '2022-02-26 17:18:34'),
	(13, 1, 5, '2021-03-18 18:58:16', '2022-02-26 17:18:34'),
	(14, 1, 5, '2021-03-18 19:00:11', '2022-02-26 17:18:34'),
	(15, 1, 1, '2021-03-19 12:00:31', '2022-02-26 17:18:34'),
	(16, 1, 2, '2021-03-19 13:24:26', '2022-02-26 17:18:34'),
	(17, 1, 3, '2021-03-19 13:24:43', '2022-02-26 17:18:34'),
	(18, 2, 1, '2021-04-13 18:49:43', '2022-02-26 17:18:34'),
	(19, 3, 1, '2021-10-20 17:17:03', '2022-02-26 18:19:30'),
	(20, 1, 1, '2021-10-28 10:56:36', '2022-02-26 17:18:34'),
	(21, 2, 1, '2021-10-28 11:00:57', '2022-02-26 17:18:34'),
	(22, 3, 1, '2021-10-28 15:48:25', '2022-02-26 18:19:30'),
	(23, 4, 1, '2021-10-28 15:49:47', '2022-02-26 18:19:30'),
	(24, 5, 1, '2021-10-28 15:53:36', '2022-02-26 18:19:30'),
	(25, 6, 1, '2021-10-28 16:12:45', '2022-02-26 18:19:29'),
	(26, 2, 3, '2021-11-25 13:58:57', '2022-02-26 17:18:34'),
	(27, 7, 1, '2022-01-08 11:14:03', '2022-02-26 18:19:29'),
	(28, 8, 1, '2022-02-26 17:19:56', '2022-02-26 18:19:29');
/*!40000 ALTER TABLE `tblticket_status_logs` ENABLE KEYS */;

-- Dumping structure for table crm.tbltodos
CREATE TABLE IF NOT EXISTS `tbltodos` (
  `todoid` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `staffid` int(11) NOT NULL,
  `dateadded` datetime NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `datefinished` datetime DEFAULT NULL,
  `item_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`todoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltodos: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltodos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltodos` ENABLE KEYS */;

-- Dumping structure for table crm.tbltracked_mails
CREATE TABLE IF NOT EXISTS `tbltracked_mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) NOT NULL,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(40) NOT NULL,
  `date` datetime NOT NULL,
  `email` varchar(100) NOT NULL,
  `opened` tinyint(1) NOT NULL DEFAULT '0',
  `date_opened` datetime DEFAULT NULL,
  `subject` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltracked_mails: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltracked_mails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltracked_mails` ENABLE KEYS */;

-- Dumping structure for table crm.tbltwocheckout_log
CREATE TABLE IF NOT EXISTS `tbltwocheckout_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(64) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id` (`invoice_id`),
  CONSTRAINT `tbltwocheckout_log_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `tblinvoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbltwocheckout_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbltwocheckout_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltwocheckout_log` ENABLE KEYS */;

-- Dumping structure for table crm.tbluser_api
CREATE TABLE IF NOT EXISTS `tbluser_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expiration_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table crm.tbluser_api: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbluser_api` DISABLE KEYS */;
INSERT INTO `tbluser_api` (`id`, `user`, `name`, `token`, `expiration_date`) VALUES
	(1, 'leometric', 'Leometric | CRM ', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibGVvbWV0cmljIiwibmFtZSI6Ikxlb21ldHJpYyB8IENSTSAiLCJBUElfVElNRSI6MTYzNDE5ODE2MX0.JfpcVlmui2olDhvx-YpPEcZKZ7DGY5-sP30RZV9PIBs', '2022-11-14 13:25:00');
/*!40000 ALTER TABLE `tbluser_api` ENABLE KEYS */;

-- Dumping structure for table crm.tbluser_auto_login
CREATE TABLE IF NOT EXISTS `tbluser_auto_login` (
  `key_id` char(32) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_agent` varchar(150) NOT NULL,
  `last_ip` varchar(40) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `staff` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbluser_auto_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbluser_auto_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbluser_auto_login` ENABLE KEYS */;

-- Dumping structure for table crm.tbluser_meta
CREATE TABLE IF NOT EXISTS `tbluser_meta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `client_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `contact_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(191) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tbluser_meta: ~13 rows (approximately)
/*!40000 ALTER TABLE `tbluser_meta` DISABLE KEYS */;
INSERT INTO `tbluser_meta` (`umeta_id`, `staff_id`, `client_id`, `contact_id`, `meta_key`, `meta_value`) VALUES
	(1, 1, 0, 0, 'recent_searches', '[]'),
	(2, 0, 0, 1, 'consent_key', '2903c2061492d75362bd71501d5a8d7e-3c101331d04b94e21a1fe9d700715bfc'),
	(4, 1, 0, 0, 'dashboard_widgets_visibility', 'a:10:{i:0;a:2:{s:2:"id";s:9:"top_stats";s:7:"visible";s:1:"1";}i:1;a:2:{s:2:"id";s:16:"finance_overview";s:7:"visible";s:1:"1";}i:2;a:2:{s:2:"id";s:9:"user_data";s:7:"visible";s:1:"1";}i:3;a:2:{s:2:"id";s:8:"calendar";s:7:"visible";s:1:"1";}i:4;a:2:{s:2:"id";s:21:"weekly_payments_chart";s:7:"visible";s:1:"0";}i:5;a:2:{s:2:"id";s:18:"contracts_expiring";s:7:"visible";s:1:"1";}i:6;a:2:{s:2:"id";s:5:"todos";s:7:"visible";s:1:"1";}i:7;a:2:{s:2:"id";s:11:"leads_chart";s:7:"visible";s:1:"1";}i:8;a:2:{s:2:"id";s:14:"projects_chart";s:7:"visible";s:1:"1";}i:9;a:2:{s:2:"id";s:17:"projects_activity";s:7:"visible";s:1:"1";}}'),
	(6, 0, 0, 4, 'consent_key', 'b26dc521f9226b0ef72ff9a45683ab42-bdc3149f00c358dafb6653ca3946c4f6'),
	(9, 0, 0, 9, 'consent_key', 'cdb58745ae64583584cac39d22db3e82-dffc2d581ca8ef42f088593ce5af54b5'),
	(10, 0, 0, 8, 'consent_key', '813224923c7055d86f3f67d99b15bf04-8bbf6947971f1b5a3e502e899ff155b3'),
	(11, 0, 0, 2, 'consent_key', 'dc139695bc1d132c1e0180fcaed1e9f4-7db0511084bfd1e1718d7907e7902fb6'),
	(12, 0, 0, 3, 'consent_key', '16c72645c3189214b66ecb0499afe4d5-f1db359b53d26fe03db4a61c98d455ee'),
	(13, 0, 0, 7, 'consent_key', '1c0fc91b5c597ecd6ab809e30b9ce121-7bc3403a282125c215f42110cb4560eb'),
	(14, 0, 0, 12, 'consent_key', '3d4bc83885a238846035066eda8e7111-78aeb67889cef1d5bf74ea4a47fc80a3'),
	(15, 0, 0, 20, 'consent_key', '869e8eee9c33e4a6917e7dc674907e0a-c4e35ba50974a63c60821da90828e92c'),
	(16, 0, 0, 21, 'consent_key', '816eb7791621ab2eda0a496327dd1ed0-e793cefcccf64e227aa03d752d43306e'),
	(17, 0, 0, 22, 'consent_key', '013ba4d8d46290c6881808fe0dcf5267-b126c1a2376d95f051c8958a3d04714e'),
	(18, 0, 0, 39, 'consent_key', 'ad2813d0e323aba88bb082b19aa4eba4-f9ae606b32129a500b1d7b76ac2ea7dc'),
	(19, 0, 0, 37, 'consent_key', '1fd75b82d8037a4fe96df18c83e3fe4f-511521b34fad6bcc6693f086869cd719'),
	(20, 0, 0, 40, 'consent_key', 'fd618a38f8c1bdc45d404c2eb76ad447-3094ae980c73a047bf0f5e1411c0bf1e');
/*!40000 ALTER TABLE `tbluser_meta` ENABLE KEYS */;

-- Dumping structure for table crm.tblvault
CREATE TABLE IF NOT EXISTS `tblvault` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `server_address` varchar(191) NOT NULL,
  `port` int(11) DEFAULT NULL,
  `username` varchar(191) NOT NULL,
  `password` text NOT NULL,
  `description` text,
  `creator` int(11) NOT NULL,
  `creator_name` varchar(100) DEFAULT NULL,
  `visibility` tinyint(1) NOT NULL DEFAULT '1',
  `share_in_projects` tinyint(1) NOT NULL DEFAULT '0',
  `last_updated` datetime DEFAULT NULL,
  `last_updated_from` varchar(100) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblvault: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblvault` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblvault` ENABLE KEYS */;

-- Dumping structure for table crm.tblviews_tracking
CREATE TABLE IF NOT EXISTS `tblviews_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_id` int(11) NOT NULL,
  `rel_type` varchar(40) NOT NULL,
  `date` datetime NOT NULL,
  `view_ip` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblviews_tracking: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblviews_tracking` DISABLE KEYS */;
INSERT INTO `tblviews_tracking` (`id`, `rel_id`, `rel_type`, `date`, `view_ip`) VALUES
	(1, 1, 'estimate', '2021-09-21 13:12:23', '::1'),
	(2, 1, 'kb_article', '2021-11-09 17:56:42', '::1');
/*!40000 ALTER TABLE `tblviews_tracking` ENABLE KEYS */;

-- Dumping structure for table crm.tblweb_to_lead
CREATE TABLE IF NOT EXISTS `tblweb_to_lead` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_key` varchar(32) NOT NULL,
  `lead_source` int(11) NOT NULL,
  `lead_status` int(11) NOT NULL,
  `notify_lead_imported` int(11) NOT NULL DEFAULT '1',
  `notify_type` varchar(20) DEFAULT NULL,
  `notify_ids` mediumtext,
  `responsible` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) NOT NULL,
  `form_data` mediumtext,
  `recaptcha` int(11) NOT NULL DEFAULT '0',
  `submit_btn_name` varchar(40) DEFAULT NULL,
  `success_submit_msg` text,
  `language` varchar(40) DEFAULT NULL,
  `allow_duplicate` int(11) NOT NULL DEFAULT '1',
  `mark_public` int(11) NOT NULL DEFAULT '0',
  `track_duplicate_field` varchar(20) DEFAULT NULL,
  `track_duplicate_field_and` varchar(20) DEFAULT NULL,
  `create_task_on_duplicate` int(11) NOT NULL DEFAULT '0',
  `dateadded` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crm.tblweb_to_lead: ~0 rows (approximately)
/*!40000 ALTER TABLE `tblweb_to_lead` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblweb_to_lead` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
